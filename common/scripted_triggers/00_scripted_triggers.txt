is_available_to_collaboration_government = { # occupied_countries.1 should be updated along with this
	NOT = {
		any_country_with_original_tag = {
			original_tag_to_check = FROM
			is_subject_of = PREV # if already created one do not create another
			#has_autonomy_state = autonomy_collaboration_government
		}
	}
}

countries_with_custom_ship_companies = {
	original_tag = AST
}

countries_with_naval_manufacturer = {
	original_tag = AST
}

no_building_slots = {
	custom_trigger_tooltip = {
		tooltip = TT_NO_BUILDING_SLOTS
		NOT = {
			OR = {
				any_owned_state = {
					free_building_slots = {
						building = industrial_complex
						size > 0
						include_locked = no
					}
				}
				any_owned_state = {
					free_building_slots = {
						building = arms_factory
						size > 0
						include_locked = no
					}
				}
				any_owned_state = {
					free_building_slots = {
						building = dockyard
						size > 0
						include_locked = no
					}
				}
				any_owned_state = {
					free_building_slots = {
						building = synthetic_refinery
						size > 0
						include_locked = no
					}
				}
				#any_owned_state = {
				#	free_building_slots = {
				#		building = fuel_silo
				#		size > 0
				#		include_locked = no
				#	}
				#}
				any_owned_state = {
					free_building_slots = {
						building = nuclear_reactor
						size > 0
						include_locked = no
					}
				}
			}
		}
	}
}

max_1_building_slots = {
	custom_trigger_tooltip = {
		tooltip = TT_MAX_1_BUILDING_SLOTS
		NOT = {
			OR = {
				any_owned_state = {
					free_building_slots = {
						building = industrial_complex
						size > 1
						include_locked = no
					}
				}
				any_owned_state = {
					free_building_slots = {
						building = arms_factory
						size > 1
						include_locked = no
					}
				}
				any_owned_state = {
					free_building_slots = {
						building = dockyard
						size > 1
						include_locked = no
					}
				}
				any_owned_state = {
					free_building_slots = {
						building = synthetic_refinery
						size > 1
						include_locked = no
					}
				}
				#any_owned_state = {
				#	free_building_slots = {
				#		building = fuel_silo
				#		size > 1
				#		include_locked = no
				#	}
				#}
				any_owned_state = {
					free_building_slots = {
						building = nuclear_reactor
						size > 1
						include_locked = no
					}
				}
			}
		}
	}
}

# Actively Signed Trade Agreements
has_more_than_one_trade_agreement = {
	custom_trigger_tooltip = {
		tooltip = more_than_1_signed_trade_agreement
		check_variable = { signed_trade_agreements > 0  }
	}
}

has_more_than_two_trade_agreement = {
	custom_trigger_tooltip = {
		tooltip = more_than_1_signed_trade_agreement
		check_variable = { signed_trade_agreements > 1  }
	}
}

has_more_than_three_trade_agreement = {
	custom_trigger_tooltip = {
		tooltip = more_than_1_signed_trade_agreement
		check_variable = { signed_trade_agreements > 2  }
	}
}

has_more_than_four_trade_agreement = {
	custom_trigger_tooltip = {
		tooltip = more_than_1_signed_trade_agreement
		check_variable = { signed_trade_agreements > 3  }
	}
}

has_more_than_five_trade_agreement = {
	custom_trigger_tooltip = {
		tooltip = more_than_5_signed_trade_agreement
		check_variable = { signed_trade_agreements > 4  }
	}
}

# Check Treasury Amount
has_more_than_temp_treasury = {
	custom_trigger_tooltip = {
		tooltip = has_more_than_temp_treasury_tt
		check_variable = { treasury > temp_treasury }
	}
}
