####Author: Angriest Bird
####Ukrainian Trigger Conditions

UKR_nato_major_influence_greater_than_10_percent = {
	OR = {
		UKR = {
			OR = {
				AND = {
					check_variable = { influence_array^0 = USA }
					check_variable = { influence_array_val^0 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^1 = USA }
					check_variable = { influence_array_val^1 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^2 = USA }
					check_variable = { influence_array_val^2 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^3 = USA }
					check_variable = { influence_array_val^3 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^4 = USA }
					check_variable = { influence_array_val^4 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^5 = USA }
					check_variable = { influence_array_val^5 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^6 = USA }
					check_variable = { influence_array_val^6 > 9.999 }
				}
			}
		}
		UKR = {
			OR = {
				AND = {
					check_variable = { influence_array^0 = ENG }
					check_variable = { influence_array_val^0 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^1 = ENG }
					check_variable = { influence_array_val^1 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^2 = ENG }
					check_variable = { influence_array_val^2 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^3 = ENG }
					check_variable = { influence_array_val^3 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^4 = ENG }
					check_variable = { influence_array_val^4 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^5 = ENG }
					check_variable = { influence_array_val^5 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^6 = ENG }
					check_variable = { influence_array_val^6 > 9.999 }
				}
			}
		}
		UKR = {
			OR = {
				AND = {
					check_variable = { influence_array^0 = FRA }
					check_variable = { influence_array_val^0 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^1 = FRA }
					check_variable = { influence_array_val^1 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^2 = FRA }
					check_variable = { influence_array_val^2 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^3 = FRA }
					check_variable = { influence_array_val^3 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^4 = FRA }
					check_variable = { influence_array_val^4 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^5 = FRA }
					check_variable = { influence_array_val^5 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^6 = FRA }
					check_variable = { influence_array_val^6 > 9.999 }
				}
			}
		}
		UKR = {
			OR = {
				AND = {
					check_variable = { influence_array^0 = GER }
					check_variable = { influence_array_val^0 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^1 = GER }
					check_variable = { influence_array_val^1 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^2 = GER }
					check_variable = { influence_array_val^2 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^3 = GER }
					check_variable = { influence_array_val^3 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^4 = GER }
					check_variable = { influence_array_val^4 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^5 = GER }
					check_variable = { influence_array_val^5 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^6 = GER }
					check_variable = { influence_array_val^6 > 9.999 }
				}
			}
		}
	}
}
UKR_targeted_country_has_more_than_70_percent_influence = {
	check_variable = { influence_array^0 = UKR }
	check_variable = { influence_array_val^0 > 79.999 }
}
UKR_east_major_influence_greater_than_10_perecnt = {
	OR = {
		UKR = {
			OR = {
				AND = {
					check_variable = { influence_array^0 = CHI }
					check_variable = { influence_array_val^0 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^1 = CHI }
					check_variable = { influence_array_val^1 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^2 = CHI }
					check_variable = { influence_array_val^2 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^3 = CHI }
					check_variable = { influence_array_val^3 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^4 = CHI }
					check_variable = { influence_array_val^4 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^5 = CHI }
					check_variable = { influence_array_val^5 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^6 = CHI }
					check_variable = { influence_array_val^6 > 9.999 }
				}
			}
		}
		UKR = {
			OR = {
				AND = {
					check_variable = { influence_array^0 = SOV }
					check_variable = { influence_array_val^0 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^1 = SOV }
					check_variable = { influence_array_val^1 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^2 = SOV }
					check_variable = { influence_array_val^2 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^3 = SOV }
					check_variable = { influence_array_val^3 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^4 = SOV }
					check_variable = { influence_array_val^4 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^5 = SOV }
					check_variable = { influence_array_val^5 > 9.999 }
				}
				AND = {
					check_variable = { influence_array^6 = SOV }
					check_variable = { influence_array_val^6 > 9.999 }
				}
			}
		}
	}
}
UKR_is_pro_west = {
	custom_trigger_tooltip = {
		tooltip = UKR_is_pro_west_TT
		OR = {
			is_in_array = { ruling_party = 0 }
			is_in_array = { ruling_party = 1 }
			is_in_array = { ruling_party = 2 }
			is_in_array = { ruling_party = 3 }
			is_in_array = { ruling_party = 14 }
			is_in_array = { ruling_party = 15 }
			is_in_array = { ruling_party = 16 }
		}
	}
}

UKR_is_pro_east = {
	custom_trigger_tooltip = {
		tooltip = UKR_is_pro_east_TT
		OR = {
			is_in_array = { ruling_party = 4 }
			is_in_array = { ruling_party = 5 }
			is_in_array = { ruling_party = 6 }
			is_in_array = { ruling_party = 7 }
			is_in_array = { ruling_party = 8 }
			is_in_array = { ruling_party = 9 }
			is_in_array = { ruling_party = 10 }
			is_in_array = { ruling_party = 11 }
			is_in_array = { ruling_party = 18 }
			is_in_array = { ruling_party = 19 }
		}
	}
}
UKR_subjects_available = {
	OR = {
		DPR = {
			has_autonomy_state = autonomy_republic_ukr
			exists = yes
			influence_higher_40 = yes
		}
		LPR = {
			has_autonomy_state = autonomy_republic_ukr
			exists = yes
			influence_higher_40 = yes
		}
		VRP = {
			has_autonomy_state = autonomy_republic_ukr
			exists = yes
			influence_higher_40 = yes
		}
		LRP = {
			has_autonomy_state = autonomy_republic_ukr
			exists = yes
			influence_higher_40 = yes
		}
		CRM = {
			has_autonomy_state = autonomy_republic_ukr
			exists = yes
			influence_higher_40 = yes
		}
	}
}
UKR_subjects_disable = {
	NOT = {
		OR = {
			DPR = {
				has_autonomy_state = autonomy_republic_ukr
				exists = yes
			}
			LPR = {
				has_autonomy_state = autonomy_republic_ukr
				exists = yes
			}
			VRP = {
				has_autonomy_state = autonomy_republic_ukr
				exists = yes
			}
			LRP = {
				has_autonomy_state = autonomy_republic_ukr
				exists = yes
			}
			CRM = {
				has_autonomy_state = autonomy_republic_ukr
				exists = yes
			}
		}
	}
}
UKR_is_neutral = {
	custom_trigger_tooltip = {
		tooltip = UKR_is_neutral_TT
		OR = {
			is_in_array = { ruling_party = 12 }
			is_in_array = { ruling_party = 13 }
			is_in_array = { ruling_party = 17 }
			is_in_array = { ruling_party = 20 }
			is_in_array = { ruling_party = 21 }
			is_in_array = { ruling_party = 22 }
			is_in_array = { ruling_party = 23 }
		}
	}
}

UKR_TEST1 = {
	OR = {
		check_variable = { influence_array^0 = USA }
		check_variable = { influence_array^1 = USA }
		check_variable = { influence_array^2 = USA }
		check_variable = { influence_array^3 = USA }
		check_variable = { influence_array^4 = USA }
		check_variable = { influence_array^5 = USA }
		check_variable = { influence_array^6 = USA }
	}
}

UKR_TEST2 = {
	OR = {
		check_variable = { influence_array^0 = SOV }
		check_variable = { influence_array^1 = SOV }
		check_variable = { influence_array^2 = SOV }
		check_variable = { influence_array^3 = SOV }
		check_variable = { influence_array^4 = SOV }
		check_variable = { influence_array^5 = SOV }
		check_variable = { influence_array^6 = SOV }
	}
}

UKR_communists_in_coalition_with_socialists = {
	custom_trigger_tooltip = {
		tooltip = UKR_Communists_socialists_TT
		OR = {
			is_in_array = { ruling_party = 4 }
			is_in_array = { gov_coalition_array = 18 }
		}
	}
}