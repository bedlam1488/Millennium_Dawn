is_major_company = {
	OR = {
		has_country_flag = blackwater_sel
		has_country_flag = wagner_sel
		has_country_flag = aegis_sel
		has_country_flag = constellis_sel
		has_country_flag = md_sel
	}
}