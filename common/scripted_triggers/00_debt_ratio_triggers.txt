gdp_debt_ratio_lower_5 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_5_tt
		check_variable = { THIS.debt_ratio < 0.05 }
	}
}
gdp_debt_ratio_higher_5 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_5_tt
		check_variable = { THIS.debt_ratio > 0.05 }
	}
}
gdp_debt_ratio_lower_10 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_10_tt
		check_variable = { THIS.debt_ratio < 0.1 }
	}
}
gdp_debt_ratio_higher_10 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_10_tt
		check_variable = { THIS.debt_ratio > 0.1 }
	}
}
gdp_debt_ratio_lower_15 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_15_tt
		check_variable = { THIS.debt_ratio < 0.15 }
	}
}
gdp_debt_ratio_higher_15 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_15_tt
		check_variable = { THIS.debt_ratio > 0.15 }
	}
}
gdp_debt_ratio_lower_20 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_20_tt
		check_variable = { THIS.debt_ratio < 0.2 }
	}
}
gdp_debt_ratio_higher_20 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_20_tt
		check_variable = { THIS.debt_ratio > 0.2 }
	}
}
gdp_debt_ratio_lower_25 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_25_tt
		check_variable = { THIS.debt_ratio < 0.25 }
	}
}
gdp_debt_ratio_higher_25 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_25_tt
		check_variable = { THIS.debt_ratio > 0.25 }
	}
}
gdp_debt_ratio_lower_30 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_30_tt
		check_variable = { THIS.debt_ratio < 0.3 }
	}
}
gdp_debt_ratio_higher_30 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_30_tt
		check_variable = { THIS.debt_ratio > 0.3 }
	}
}
gdp_debt_ratio_lower_35 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_35_tt
		check_variable = { THIS.debt_ratio < 0.35 }
	}
}
gdp_debt_ratio_higher_35 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_35_tt
		check_variable = { THIS.debt_ratio > 0.35 }
	}
}
gdp_debt_ratio_lower_40 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_40_tt
		check_variable = { THIS.debt_ratio < 0.4 }
	}
}
gdp_debt_ratio_higher_40 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_40_tt
		check_variable = { THIS.debt_ratio > 0.4 }
	}
}
gdp_debt_ratio_lower_45 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_45_tt
		check_variable = { THIS.debt_ratio < 0.45 }
	}
}
gdp_debt_ratio_higher_45 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_45_tt
		check_variable = { THIS.debt_ratio > 0.45 }
	}
}
gdp_debt_ratio_lower_50 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_50_tt
		check_variable = { THIS.debt_ratio < 0.5 }
	}
}
gdp_debt_ratio_higher_50 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_50_tt
		check_variable = { THIS.debt_ratio > 0.5 }
	}
}
gdp_debt_ratio_lower_55 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_55_tt
		check_variable = { THIS.debt_ratio < 0.55 }
	}
}
gdp_debt_ratio_higher_55 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_55_tt
		check_variable = { THIS.debt_ratio > 0.55 }
	}
}
gdp_debt_ratio_lower_60 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_60_tt
		check_variable = { THIS.debt_ratio < 0.6 }
	}
}
gdp_debt_ratio_higher_60 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_60_tt
		check_variable = { THIS.debt_ratio > 0.6 }
	}
}
gdp_debt_ratio_lower_65 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_65_tt
		check_variable = { THIS.debt_ratio < 0.65 }
	}
}
gdp_debt_ratio_higher_65 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_65_tt
		check_variable = { THIS.debt_ratio > 0.65 }
	}
}
gdp_debt_ratio_lower_70 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_70_tt
		check_variable = { THIS.debt_ratio < 0.7 }
	}
}
gdp_debt_ratio_higher_70 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_70_tt
		check_variable = { THIS.debt_ratio > 0.7 }
	}
}
gdp_debt_ratio_lower_75 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_75_tt
		check_variable = { THIS.debt_ratio < 0.75 }
	}
}
gdp_debt_ratio_higher_75 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_75_tt
		check_variable = { THIS.debt_ratio > 0.75 }
	}
}
gdp_debt_ratio_lower_80 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_80_tt
		check_variable = { THIS.debt_ratio < 0.8 }
	}
}
gdp_debt_ratio_higher_80 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_80_tt
		check_variable = { THIS.debt_ratio > 0.8 }
	}
}
gdp_debt_ratio_lower_85 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_85_tt
		check_variable = { THIS.debt_ratio < 0.85 }
	}
}
gdp_debt_ratio_higher_85 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_85_tt
		check_variable = { THIS.debt_ratio > 0.85 }
	}
}
gdp_debt_ratio_lower_90 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_90_tt
		check_variable = { THIS.debt_ratio < 0.9 }
	}
}
gdp_debt_ratio_higher_90 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_90_tt
		check_variable = { THIS.debt_ratio > 0.9 }
	}
}
gdp_debt_ratio_lower_95 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_95_tt
		check_variable = { THIS.debt_ratio < 0.95 }
	}
}
gdp_debt_ratio_higher_95 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_95_tt
		check_variable = { THIS.debt_ratio > 0.95 }
	}
}
gdp_debt_ratio_lower_100 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_100_tt
		check_variable = { THIS.debt_ratio < 1.0 }
	}
}
gdp_debt_ratio_higher_100 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_100_tt
		check_variable = { THIS.debt_ratio > 1.0 }
	}
}
gdp_debt_ratio_lower_105 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_105_tt
		check_variable = { THIS.debt_ratio < 1.05 }
	}
}
gdp_debt_ratio_higher_105 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_105_tt
		check_variable = { THIS.debt_ratio > 1.05 }
	}
}
gdp_debt_ratio_lower_110 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_110_tt
		check_variable = { THIS.debt_ratio < 1.1 }
	}
}
gdp_debt_ratio_higher_110 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_110_tt
		check_variable = { THIS.debt_ratio > 1.1 }
	}
}
gdp_debt_ratio_lower_115 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_115_tt
		check_variable = { THIS.debt_ratio < 1.15 }
	}
}
gdp_debt_ratio_higher_115 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_115_tt
		check_variable = { THIS.debt_ratio > 1.15 }
	}
}
gdp_debt_ratio_lower_120 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_120_tt
		check_variable = { THIS.debt_ratio < 1.2 }
	}
}
gdp_debt_ratio_higher_120 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_120_tt
		check_variable = { THIS.debt_ratio > 1.2 }
	}
}
gdp_debt_ratio_lower_125 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_125_tt
		check_variable = { THIS.debt_ratio < 1.25 }
	}
}
gdp_debt_ratio_higher_125 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_125_tt
		check_variable = { THIS.debt_ratio > 1.25 }
	}
}
gdp_debt_ratio_lower_130 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_130_tt
		check_variable = { THIS.debt_ratio < 1.3 }
	}
}
gdp_debt_ratio_higher_130 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_130_tt
		check_variable = { THIS.debt_ratio > 1.3 }
	}
}
gdp_debt_ratio_lower_135 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_135_tt
		check_variable = { THIS.debt_ratio < 1.35 }
	}
}
gdp_debt_ratio_higher_135 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_135_tt
		check_variable = { THIS.debt_ratio > 1.35 }
	}
}
gdp_debt_ratio_lower_140 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_140_tt
		check_variable = { THIS.debt_ratio < 1.4 }
	}
}
gdp_debt_ratio_higher_140 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_140_tt
		check_variable = { THIS.debt_ratio > 1.4 }
	}
}
gdp_debt_ratio_lower_145 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_145_tt
		check_variable = { THIS.debt_ratio < 1.45 }
	}
}
gdp_debt_ratio_higher_145 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_145_tt
		check_variable = { THIS.debt_ratio > 1.45 }
	}
}
gdp_debt_ratio_lower_150 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_150_tt
		check_variable = { THIS.debt_ratio < 1.5 }
	}
}
gdp_debt_ratio_higher_150 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_150_tt
		check_variable = { THIS.debt_ratio > 1.5 }
	}
}
gdp_debt_ratio_lower_155 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_155_tt
		check_variable = { THIS.debt_ratio < 1.55 }
	}
}
gdp_debt_ratio_higher_155 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_155_tt
		check_variable = { THIS.debt_ratio > 1.55 }
	}
}
gdp_debt_ratio_lower_160 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_160_tt
		check_variable = { THIS.debt_ratio < 1.6 }
	}
}
gdp_debt_ratio_higher_160 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_160_tt
		check_variable = { THIS.debt_ratio > 1.6 }
	}
}
gdp_debt_ratio_lower_165 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_165_tt
		check_variable = { THIS.debt_ratio < 1.65 }
	}
}
gdp_debt_ratio_higher_165 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_165_tt
		check_variable = { THIS.debt_ratio > 1.65 }
	}
}
gdp_debt_ratio_lower_170 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_170_tt
		check_variable = { THIS.debt_ratio < 1.7 }
	}
}
gdp_debt_ratio_higher_170 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_170_tt
		check_variable = { THIS.debt_ratio > 1.7 }
	}
}
gdp_debt_ratio_lower_175 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_175_tt
		check_variable = { THIS.debt_ratio < 1.75 }
	}
}
gdp_debt_ratio_higher_175 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_175_tt
		check_variable = { THIS.debt_ratio > 1.75 }
	}
}
gdp_debt_ratio_lower_180 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_180_tt
		check_variable = { THIS.debt_ratio < 1.8 }
	}
}
gdp_debt_ratio_higher_180 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_180_tt
		check_variable = { THIS.debt_ratio > 1.8 }
	}
}
gdp_debt_ratio_lower_185 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_185_tt
		check_variable = { THIS.debt_ratio < 1.85 }
	}
}
gdp_debt_ratio_higher_185 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_185_tt
		check_variable = { THIS.debt_ratio > 1.85 }
	}
}
gdp_debt_ratio_lower_190 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_190_tt
		check_variable = { THIS.debt_ratio < 1.9 }
	}
}
gdp_debt_ratio_higher_190 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_190_tt
		check_variable = { THIS.debt_ratio > 1.9 }
	}
}
gdp_debt_ratio_lower_195 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_195_tt
		check_variable = { THIS.debt_ratio < 1.95 }
	}
}
gdp_debt_ratio_higher_195 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_195_tt
		check_variable = { THIS.debt_ratio > 1.95 }
	}
}
gdp_debt_ratio_lower_200 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_lower_200_tt
		check_variable = { THIS.debt_ratio < 2.0 }
	}
}
gdp_debt_ratio_higher_200 = {
	custom_trigger_tooltip = {
		tooltip = gdp_debt_ratio_higher_200_tt
		check_variable = { THIS.debt_ratio > 2.0 }
	}
}
