has_nuclear_reactors = {
	custom_trigger_tooltip = {
		tooltip = has_nuclear_reactors_tt
		check_variable = { nuclear_reactors > 0 }
	}
}
has_enrichment_facilities = {
	custom_trigger_tooltip = {
		tooltip = has_enrichment_facilities_tt
		check_variable = { enrichment_facilities > 0 }
	}
}
has_reactor_material_stockpile = {
	custom_trigger_tooltip = {
		tooltip = has_reactor_material_stockpile_tt
		check_variable = { var_reactor_material_stockpile > 500 }
	}
}
