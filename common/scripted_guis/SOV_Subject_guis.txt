scripted_gui = {
	union_state_gui = {
		context_type = decision_category
		window_name = "union_state_ui_window"
		visible = { always = yes }
	}
	wagner_ui = {
		context_type = decision_category
		window_name = "wagner_ui_window"
		visible = { always = yes }
	}
}
