PER_clergy_balance_of_power_category = {
	initial_value = 0.25
	left_side = PER_secularists_bop_left_side
	right_side = PER_clergy_right_side
	decision_category = PER_clergy_balance_of_power_category
	range = {
		id = PER_secularists_bop_mid_range
		min = -0.1
		max = 0.1
		modifier = {
			neutrality_drift = 0.02
			army_core_defence_factor = 0.10
			stability_factor = 0.05
		}
		on_activate = {

		}
		on_deactivate = {

		}
	}

	side = {
		id = PER_secularists_bop_left_side
		icon = GFX_bop_PER_secular
		range = {
			id = PER_secularists_bop_left_win_range
			min = -1
			max = -0.9
			modifier = {
				hidden_modifier = {
					productivity_growth_modifier = 1.0
					production_speed_buildings_factor = 0.15
					foreign_influence_defense_modifier = 0.10
					research_speed_factor = 0.10
					corruption_cost_factor = -0.20
					# Debuffs
					conscription_factor = -0.05
					justify_war_goal_time = 0.15
					education_cost_multiplier_modifier = 0.10
					army_morale_factor = -0.025
				}
				custom_modifier_tooltip = PER_secular_state_buffs
			}
			on_activate = {
			}
			on_deactivate = {

			}
		}

		range = {
			id = PER_secularists_bop_left_advanced_range
			min = -0.9
			max = -0.6
			modifier = {
				hidden_modifier = {
					production_speed_buildings_factor = 0.10
					foreign_influence_defense_modifier = 0.05
					research_speed_factor = 0.05
					corruption_cost_factor = -0.15
					# Debuffs
					conscription_factor = -0.03
					justify_war_goal_time = 0.10
					education_cost_multiplier_modifier = 0.05
				}
				custom_modifier_tooltip = PER_secular_dominance_buffs
			}
			on_activate = {
			}
			on_deactivate = {

			}
		}

		range = {
			id = PER_secularists_bop_left_medium_range
			min = -0.6
			max = -0.4
			modifier = {
				hidden_modifier = {
					production_speed_buildings_factor = 0.05
					foreign_influence_defense_modifier = 0.05
					corruption_cost_factor = -0.05
					# Debuffs
					conscription_factor = -0.01
					justify_war_goal_time = 0.05
				}
				custom_modifier_tooltip = PER_secular_influence_buffs
			}
			on_activate = {

			}
			on_deactivate = {

			}
		}

		range = {
			id = PER_secularists_bop_left_low_range
			min = -0.4
			max = -0.1
			modifier = {
				hidden_modifier = {
					production_speed_buildings_factor = 0.05
					# Debuffs
					justify_war_goal_time = 0.05
				}
				custom_modifier_tooltip = PER_secular_leaning_buffs
			}
			on_activate = {

			}
			on_deactivate = {

			}
		}
	}

	side = {
		id = PER_clergy_right_side
		icon = GFX_bop_PER_conservative
		range = {
			id = PER_clergy_bop_right_win_range
			min = 0.9
			max = 1
			modifier = {
				hidden_modifier = {
					conscription_factor = 0.10
					army_attack_factor = 0.05
					war_support_factor = 0.15
					surrender_limit = 0.20
					send_volunteer_size = 2
					# Debuffs
					production_speed_buildings_factor = -0.15
					research_speed_factor = -0.10
					tax_gain_multiplier_modifier = -0.10
					corruption_cost_factor = 0.50
				}
				custom_modifier_tooltip = PER_defacto_theocratic_state_buffs
			}
			on_activate = {
			}
			on_deactivate = {

			}
		}

		range = {
			id = PER_clergy_bop_right_advanced_range
			min = 0.6
			max = 0.9
			modifier = {
				hidden_modifier = {
					conscription_factor = 0.05
					army_attack_factor = 0.03
					war_support_factor = 0.10
					surrender_limit = 0.10
					# Debuffs
					production_speed_buildings_factor = -0.10
					research_speed_factor = -0.10
					tax_gain_multiplier_modifier = -0.05
					corruption_cost_factor = 0.20
				}
				custom_modifier_tooltip = PER_islamist_dominance_buffs
			}
			on_activate = {

			}
			on_deactivate = {

			}
		}

		range = {
			id = PER_clergy_bop_right_medium_range
			min = 0.4
			max = 0.6
			modifier = {
				hidden_modifier = {
					conscription_factor = 0.05
					war_support_factor = 0.10
					# Debuffs
					production_speed_buildings_factor = -0.05
					research_speed_factor = -0.05
					corruption_cost_factor = 0.10
				}
				custom_modifier_tooltip = PER_islamist_influence_buffs
			}
			on_activate = {

			}
			on_deactivate = {

			}
		}

		range = {
			id = PER_clergy_bop_right_low_range
			min = 0.1
			max = 0.4
			modifier = {
				hidden_modifier = {
					conscription_factor = 0.05
					# Debuffs
					corruption_cost_factor = 0.05
				}
				custom_modifier_tooltip = PER_islamist_leaning_buffs
			}
			on_activate = {

			}
			on_deactivate = {

			}
		}
	}
}