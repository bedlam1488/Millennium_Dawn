WAG_wagner_category = {
	WAG_foreign_army = {
		cost = 45
		icon = GFX_decision_wagner
		days_remove = 320
		available = {
			has_completed_focus = WAG_wagner_outher_heaven
		}
		ai_will_do = {
			factor = 100
		}
		visible = {
			has_completed_focus = WAG_wagner_outher_heaven
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision WAG_foreign_army"
			set_temp_variable = { modify_wagner = 1700 }
			modify_wagner_support = yes
			set_temp_variable = { treasury_change = -5 }
			modify_treasury_effect = yes
		}

	}
	WAG_local_recruit = {
		cost = 35
		icon = GFX_decision_wagner
		days_remove = 220
		available = {
			has_completed_focus = WAG_wagner_outher_heaven
		}
		ai_will_do = {
			factor = 100
		}
		visible = {
			has_completed_focus = WAG_wagner_centers

		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision WAG_local_recruit"
			set_temp_variable = { modify_wagner = 800 }
			modify_wagner_support = yes
			set_temp_variable = { treasury_change = -2 }
			modify_treasury_effect = yes
		}

	}
	WAG_new_type_brigade = {
		cost = 50
		icon = GFX_decision_wagner
		days_remove = 45
		ai_will_do = {
			factor = 100
		}
		available = {
			custom_trigger_tooltip = {
				tooltip = SOV_wagner_recruit_2000
				check_variable = { WAG.wagner > 1999 }
			}
		}

		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove WAG_decision_new_type_brigade"
			add_stability = -0.06
			set_temp_variable = { modify_wagner = -2000 }
			modify_wagner_support = yes
			set_temp_variable = { treasury_change = -15 }
			modify_treasury_effect = yes
			custom_effect_tooltip = BLR_wagner_recruit_TT
			hidden_effect = {
				division_template = {
					name = "Wagner Light Unit"
					is_locked = yes
					regiments = {
						L_Inf_Bat = { x = 0 y = 0 }
						L_Inf_Bat = { x = 0 y = 1 }
						L_Inf_Bat = { x = 0 y = 2 }
						L_Inf_Bat = { x = 0 y = 3 }
						Arty_Bat = { x = 0 y = 4 }
						L_Inf_Bat = { x = 1 y = 0 }
						L_Inf_Bat = { x = 1 y = 1 }
						L_Inf_Bat = { x = 1 y = 2 }
						L_Inf_Bat = { x = 1 y = 3 }
						Arty_Bat = { x = 1 y = 4 }
					}
					support = {
						L_Recce_Comp = { x = 0 y = 0 }
						L_Engi_Comp = { x = 0 y = 1 }
					}
					priority = 2
				}
				spawn_light_infantry_wagner = yes
			}
		}
	}
	WAG_passports = {
		cost = 50
		icon = GFX_decision_wagner
		days_remove = 180
		ai_will_do = {
			factor = 100
		}
		available = {
			has_completed_focus = WAG_passport
		}

		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove WAG_passports"
			WAG_passports_expansion = yes
		}
		}
}
WAG_wagner_mother_base_category = {
	WAG_recruit_center = {
		custom_cost_trigger = {
			check_variable = { treasury > 10.0 }
		}
		custom_cost_text = cost_10_0
		icon = GFX_decision_mother_baza_button
		days_remove = 120
		fire_only_once = yes
		
		ai_will_do = {
			factor = 100
		}

		visible = {
			has_completed_focus = WAG_wagner_outher_heaven
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision WAG_recruit_center"
			set_country_flag = SOV_wagner_recruit_center
		}
		
	}
	WAG_recruit1_center = {
		custom_cost_trigger = {
			check_variable = { treasury > 10.0 }
		}
		custom_cost_text = cost_10_0
		icon = GFX_decision_mother_baza_button
		days_remove = 120
		fire_only_once = yes
		
		ai_will_do = {
			factor = 100
		}

		visible = {
			has_completed_focus = WAG_wagner_outher_heaven
			has_country_flag = SOV_wagner_recruit_center
		}

		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove WAG_recruit1_center"
			add_ideas = WAG_pmcs_army
		}
	}
	WAG_recruit2_center = {
		custom_cost_trigger = {
			check_variable = { treasury > 10.0 }
		}
		custom_cost_text = cost_10_0
		icon = GFX_decision_mother_baza_button
		days_remove = 180
		fire_only_once = yes
		
		ai_will_do = {
			factor = 100
		}

		visible = {
			has_completed_focus = WAG_wagner_outher_heaven
			has_idea = WAG_pmcs_army
		}

		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove WAG_recruit2_center"
			swap_ideas = {
				remove_idea = WAG_pmcs_army
				add_idea = WAG_pmcs_army1
			}
		}
	}
	WAG_recruit3_center = {
		custom_cost_trigger = {
			check_variable = { treasury > 10.0 }
		}
		custom_cost_text = cost_10_0
		icon = GFX_decision_mother_baza_button
		days_remove = 190
		fire_only_once = yes
		
		ai_will_do = {
			factor = 100
		}

		visible = {
			has_completed_focus = WAG_wagner_outher_heaven
			has_idea = WAG_pmcs_army1
		}

		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove WAG_recruit3_center"
			swap_ideas = {
				remove_idea = WAG_pmcs_army1
				add_idea = WAG_pmcs_army2
			}
		}
	}
	WAG_recruit4_center = {
		custom_cost_trigger = {
			check_variable = { treasury > 10.0 }
		}
		custom_cost_text = cost_10_0
		icon = GFX_decision_mother_baza_button
		days_remove = 210
		fire_only_once = yes
		
		ai_will_do = {
			factor = 100
		}

		visible = {
			has_completed_focus = WAG_wagner_outher_heaven
			has_idea = WAG_pmcs_army2
		}

		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove WAG_recruit4_center"
			swap_ideas = {
				remove_idea = WAG_pmcs_army2
				add_idea = WAG_pmcs_army3
			}
		}
	}
	WAG_recruit5_center = {
		custom_cost_trigger = {
			check_variable = { treasury > 12.5 }
		}
		custom_cost_text = cost_12_5
		icon = GFX_decision_mother_baza_button
		days_remove = 210
		fire_only_once = yes
		
		ai_will_do = {
			factor = 100
		}

		visible = {
			has_completed_focus = WAG_wagner_outher_heaven
			has_idea = WAG_pmcs_army3
		}

		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove WAG_recruit5_center"
			swap_ideas = {
				remove_idea = WAG_pmcs_army3
				add_idea = WAG_pmcs_army4
			}
		}
	}
	WAG_science_center = {
		custom_cost_trigger = {
			check_variable = { treasury > 12.5 }
		}
		custom_cost_text = cost_12_5
		icon = GFX_decision_mother_baza_button
		days_remove = 220
		fire_only_once = yes
		
		ai_will_do = {
			factor = 100
		}

		visible = {
			has_completed_focus = WAG_wagner_outher_heaven
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove WAG_science_center"
			set_country_flag = SOV_wagner_science_center
			add_ideas = WAG_centers
		}
		
	}
	WAG_medical_center = {
		custom_cost_trigger = {
			check_variable = { treasury > 12.5 }
		}
		custom_cost_text = cost_12_5
		icon = GFX_decision_mother_baza_button
		days_remove = 300
		fire_only_once = yes
		
		ai_will_do = {
			factor = 100
		}

		visible = {
			has_completed_focus = WAG_wagner_outher_heaven
			has_country_flag = SOV_wagner_science_center
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove WAG_medical_center"
			set_country_flag = SOV_wagner_medical_center
			swap_ideas = {
				remove_idea = WAG_centers
				add_idea = WAG_centers1
			}
		}
		
	}
	WAG_resource_center = {
		cost = 45
		icon = GFX_decision_mother_baza_button
		days_remove = 120
		fire_only_once = yes
		
		ai_will_do = {
			factor = 100
		}

		visible = {
			has_country_flag = SOV_wagner_medical_center
			has_completed_focus = WAG_wagner_outher_heaven
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove WAG_resource_center"
			swap_ideas = {
				remove_idea = WAG_centers1
				add_idea = WAG_centers2
			}
		}
		
	}
}
WAG_Bunt_cate = {
	WAG_The_March_Justice_dec = {
		activation = {
			has_war_with = SOV
		}
		available = {
			WAG = {
				controls_province = 6380
			}
		}
		cancel_trigger = {
		}
		icon = GFX_decision_mother_baza_button
		is_good = no
		days_mission_timeout = 50
		selectable_mission = no
		complete_effect = {
			SOV = {
				change_tag_from = WAG
			}
			transfer_state = 667
			SOV = {
			white_peace = WAG
			set_country_flag = SOV_wagner_parties
			set_temp_variable = { rul_party_temp = 22 }
			change_ruling_party_effect = yes
			set_politics = {
				ruling_party = nationalist
				elections_allowed = no
			}
		}
		}
		timeout_effect = {
			delete_unit_template_and_units = {
				division_template = "Wagner Light Unit"
				disband = yes
			}
			delete_unit_template_and_units = {
				division_template = "Wagner Group PMC"
				disband = yes
			}
			delete_unit_template_and_units = {
				division_template = "Wagner Tank Unit"
				disband = yes
			}
			delete_unit_template_and_units = {
				division_template = "Wagner Heavy Mechanized Unit"
				disband = yes
			}	
			SOV = {
				clr_country_flag = SOV_wagner_parties
				set_country_flag = SOV_wagner_rebeliion_flag
				add_stability = -0.05
				white_peace = WAG
				transfer_state = 667
				country_event = {
					id = russia_wagner.33
					days = 61
				}
			}
			news_event = {
				id = russia_wag_news.2
				hours = 1
			}
		}
	} 
}
