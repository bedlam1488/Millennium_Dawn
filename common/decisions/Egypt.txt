EGY_evergreen_category = {
	EGY_expand_evergreen_production = {
		visible = { check_variable = { ROOT.current_max_evergreen_capacity < ROOT.max_evergreen_capacity } }

		days_remove = 100
		days_re_enable = 150
		cost = 50

		remove_effect = {
			custom_effect_tooltip = evergreen_cap_tt
			set_temp_variable = { treasury_change = -5 }
			modify_treasury_effect = yes
			add_to_variable = { ROOT.current_max_evergreen_capacity = 1 }
		}

		ai_will_do = { factor = 20 }
		complete_effect = { log = "[GetDateText]: [Root.GetName]: Decision EGY_expand_evergreen_production" }
	}

	EGY_mission_to_Israel = {
		fire_only_once = no
		available = {
			custom_trigger_tooltip = {
				tooltip = EGY_can_send_evergreen_mission_TT
				check_variable = { ROOT.current_evergreen_capacity < ROOT.current_max_evergreen_capacity }
			}
			NOT = { has_decision = EGY_mission_to_Sudan }
			NOT = { has_decision = EGY_mission_to_Libya }
			NOT = { has_decision = EGY_mission_to_Algeria }
			NOT = { has_decision = EGY_mission_to_Jordan }
			NOT = { has_decision = EGY_mission_to_Syria }
			NOT = { has_decision = EGY_mission_to_Saudia }
			NOT = { has_decision = EGY_mission_to_Iraq }
			NOT = { has_decision = EGY_mission_to_Lebanon }
			NOT = { has_decision = EGY_mission_to_uae }
			NOT = { has_decision = EGY_mission_to_Turkey }
			NOT = { has_country_flag = EGY_pending_offer }
			NOT = { ISR = { has_idea = EGY_evergreen_fertilizers_idea } }
		}
		visible = { country_exists = ISR }

		days_remove = 10
		days_re_enable = 250
		cost = 25

		remove_effect = {
			set_country_flag = EGY_pending_offer
			ISR = { country_event = egypt_evergreen.1 }
		}

		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				ROOT = { has_opinion = { target = ISR value < -20 } }
			}
		}
		complete_effect = { log = "[GetDateText]: [Root.GetName]: Decision EGY_mission_to_Israel" }
	}

	EGY_mission_to_Sudan = {
		fire_only_once = no
		available = {
			custom_trigger_tooltip = {
				tooltip = EGY_can_send_evergreen_mission_TT
				check_variable = { ROOT.current_evergreen_capacity < ROOT.current_max_evergreen_capacity }
			}
			NOT = { has_decision = EGY_mission_to_Israel }
			NOT = { has_decision = EGY_mission_to_Libya }
			NOT = { has_decision = EGY_mission_to_Algeria }
			NOT = { has_decision = EGY_mission_to_Jordan }
			NOT = { has_decision = EGY_mission_to_Syria }
			NOT = { has_decision = EGY_mission_to_Saudia }
			NOT = { has_decision = EGY_mission_to_Iraq }
			NOT = { has_decision = EGY_mission_to_Lebanon }
			NOT = { has_decision = EGY_mission_to_uae }
			NOT = { has_decision = EGY_mission_to_Turkey }
			NOT = { has_country_flag = EGY_pending_offer }
			NOT = { SUD = { has_idea = EGY_evergreen_fertilizers_idea } }
		}
		visible = { country_exists = SUD }

		days_remove = 10
		days_re_enable = 250
		cost = 25

		remove_effect = {
			set_country_flag = EGY_pending_offer
			SUD = { country_event = egypt_evergreen.1 }
		}

		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				ROOT = { has_opinion = { target = SUD value < -20 } }
			}
		}
		complete_effect = { log = "[GetDateText]: [Root.GetName]: Decision EGY_mission_to_Sudan" }
	}

	EGY_mission_to_Libya = {
		fire_only_once = no
		available = {
			custom_trigger_tooltip = {
				tooltip = EGY_can_send_evergreen_mission_TT
				check_variable = { ROOT.current_evergreen_capacity < ROOT.current_max_evergreen_capacity }
			}
			NOT = { has_decision = EGY_mission_to_Israel }
			NOT = { has_decision = EGY_mission_to_Sudan }
			NOT = { has_decision = EGY_mission_to_Algeria }
			NOT = { has_decision = EGY_mission_to_Jordan }
			NOT = { has_decision = EGY_mission_to_Syria }
			NOT = { has_decision = EGY_mission_to_Saudia }
			NOT = { has_decision = EGY_mission_to_Iraq }
			NOT = { has_decision = EGY_mission_to_Lebanon }
			NOT = { has_decision = EGY_mission_to_uae }
			NOT = { has_decision = EGY_mission_to_Turkey }
			NOT = { has_country_flag = EGY_pending_offer }
			NOT = { LBA = { has_idea = EGY_evergreen_fertilizers_idea } }
		}
		visible = { country_exists = LBA }

		days_remove = 10
		days_re_enable = 250
		cost = 25

		remove_effect = {
			set_country_flag = EGY_pending_offer
			LBA = { country_event = egypt_evergreen.1 }
		}

		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				ROOT = { has_opinion = { target = LBA value < -20 } }
			}
		}
		complete_effect = { log = "[GetDateText]: [Root.GetName]: Decision EGY_mission_to_Libya" }
	}

	EGY_mission_to_Algeria = {
		fire_only_once = no
		available = {
			custom_trigger_tooltip = {
				tooltip = EGY_can_send_evergreen_mission_TT
				check_variable = { ROOT.current_evergreen_capacity < ROOT.current_max_evergreen_capacity }
			}
			NOT = { has_decision = EGY_mission_to_Israel }
			NOT = { has_decision = EGY_mission_to_Sudan }
			NOT = { has_decision = EGY_mission_to_Libya }
			NOT = { has_decision = EGY_mission_to_Jordan }
			NOT = { has_decision = EGY_mission_to_Syria }
			NOT = { has_decision = EGY_mission_to_Saudia }
			NOT = { has_decision = EGY_mission_to_Iraq }
			NOT = { has_decision = EGY_mission_to_Lebanon }
			NOT = { has_decision = EGY_mission_to_uae }
			NOT = { has_decision = EGY_mission_to_Turkey }
			NOT = { has_country_flag = EGY_pending_offer }
			NOT = { ALG = { has_idea = EGY_evergreen_fertilizers_idea } }
		}
		visible = { country_exists = ALG }

		days_remove = 10
		days_re_enable = 250
		cost = 25

		remove_effect = {
			set_country_flag = EGY_pending_offer
			ALG = { country_event = egypt_evergreen.1 }
		}

		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				ROOT = { has_opinion = { target = ALG value < -20 } }
			}
		}
		complete_effect = { log = "[GetDateText]: [Root.GetName]: Decision EGY_mission_to_Algeria" }
	}

	EGY_mission_to_Jordan = {
		fire_only_once = no
		available = {
			custom_trigger_tooltip = {
				tooltip = EGY_can_send_evergreen_mission_TT
				check_variable = { ROOT.current_evergreen_capacity < ROOT.current_max_evergreen_capacity }
			}
			NOT = { has_decision = EGY_mission_to_Israel }
			NOT = { has_decision = EGY_mission_to_Sudan }
			NOT = { has_decision = EGY_mission_to_Libya }
			NOT = { has_decision = EGY_mission_to_Algeria }
			NOT = { has_decision = EGY_mission_to_Syria }
			NOT = { has_decision = EGY_mission_to_Saudia }
			NOT = { has_decision = EGY_mission_to_Iraq }
			NOT = { has_decision = EGY_mission_to_Lebanon }
			NOT = { has_decision = EGY_mission_to_uae }
			NOT = { has_decision = EGY_mission_to_Turkey }
			NOT = { has_country_flag = EGY_pending_offer }
			NOT = { JOR = { has_idea = EGY_evergreen_fertilizers_idea } }
		}
		visible = { country_exists = JOR }

		days_remove = 10
		days_re_enable = 250
		cost = 25

		remove_effect = {
			set_country_flag = EGY_pending_offer
			JOR = { country_event = egypt_evergreen.1 }
		}

		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				ROOT = { has_opinion = { target = JOR value < -20 } }
			}
		}
		complete_effect = { log = "[GetDateText]: [Root.GetName]: Decision EGY_mission_to_Jordan" }
	}

	EGY_mission_to_Syria = {
		fire_only_once = no
		available = {
			custom_trigger_tooltip = {
				tooltip = EGY_can_send_evergreen_mission_TT
				check_variable = { ROOT.current_evergreen_capacity < ROOT.current_max_evergreen_capacity }
			}
			NOT = { has_decision = EGY_mission_to_Israel }
			NOT = { has_decision = EGY_mission_to_Sudan }
			NOT = { has_decision = EGY_mission_to_Libya }
			NOT = { has_decision = EGY_mission_to_Algeria }
			NOT = { has_decision = EGY_mission_to_Jordan }
			NOT = { has_decision = EGY_mission_to_Saudia }
			NOT = { has_decision = EGY_mission_to_Iraq }
			NOT = { has_decision = EGY_mission_to_Lebanon }
			NOT = { has_decision = EGY_mission_to_uae }
			NOT = { has_decision = EGY_mission_to_Turkey }
			NOT = { has_country_flag = EGY_pending_offer }
			NOT = { SYR = { has_idea = EGY_evergreen_fertilizers_idea } }
		}
		visible = { country_exists = SYR }

		days_remove = 10
		days_re_enable = 250
		cost = 25

		remove_effect = {
			set_country_flag = EGY_pending_offer
			SYR = { country_event = egypt_evergreen.1 }
		}

		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				ROOT = { has_opinion = { target = SYR value < -20 } }
			}
		}
		complete_effect = { log = "[GetDateText]: [Root.GetName]: Decision EGY_mission_to_Jordan" }
	}

	EGY_mission_to_Saudia = {
		fire_only_once = no
		available = {
			custom_trigger_tooltip = {
				tooltip = EGY_can_send_evergreen_mission_TT
				check_variable = { ROOT.current_evergreen_capacity < ROOT.current_max_evergreen_capacity }
			}
			NOT = { has_decision = EGY_mission_to_Israel }
			NOT = { has_decision = EGY_mission_to_Sudan }
			NOT = { has_decision = EGY_mission_to_Libya }
			NOT = { has_decision = EGY_mission_to_Algeria }
			NOT = { has_decision = EGY_mission_to_Jordan }
			NOT = { has_decision = EGY_mission_to_Syria }
			NOT = { has_decision = EGY_mission_to_Iraq }
			NOT = { has_decision = EGY_mission_to_Lebanon }
			NOT = { has_decision = EGY_mission_to_uae }
			NOT = { has_decision = EGY_mission_to_Turkey }
			NOT = { has_country_flag = EGY_pending_offer }
			NOT = { SAU = { has_idea = EGY_evergreen_fertilizers_idea } }
		}
		visible = { country_exists = SAU }

		days_remove = 10
		days_re_enable = 250
		cost = 25

		remove_effect = {
			set_country_flag = EGY_pending_offer
			SAU = { country_event = egypt_evergreen.1 }
		}

		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				ROOT = { has_opinion = { target = SAU value < -20 } }
			}
		}
		complete_effect = { log = "[GetDateText]: [Root.GetName]: Decision EGY_mission_to_Saudia" }
	}

	EGY_mission_to_Iraq = {
		fire_only_once = no
		available = {
			custom_trigger_tooltip = {
				tooltip = EGY_can_send_evergreen_mission_TT
				check_variable = { ROOT.current_evergreen_capacity < ROOT.current_max_evergreen_capacity }
			}
			NOT = { has_decision = EGY_mission_to_Israel }
			NOT = { has_decision = EGY_mission_to_Sudan }
			NOT = { has_decision = EGY_mission_to_Libya }
			NOT = { has_decision = EGY_mission_to_Algeria }
			NOT = { has_decision = EGY_mission_to_Jordan }
			NOT = { has_decision = EGY_mission_to_Syria }
			NOT = { has_decision = EGY_mission_to_Saudia }
			NOT = { has_decision = EGY_mission_to_Lebanon }
			NOT = { has_decision = EGY_mission_to_uae }
			NOT = { has_decision = EGY_mission_to_Turkey }
			NOT = { has_country_flag = EGY_pending_offer }
			NOT = { IRQ = { has_idea = EGY_evergreen_fertilizers_idea } }
		}
		visible = { country_exists = IRQ }

		days_remove = 10
		days_re_enable = 250
		cost = 25

		remove_effect = {
			set_country_flag = EGY_pending_offer
			IRQ = { country_event = egypt_evergreen.1 }
		}

		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				ROOT = { has_opinion = { target = IRQ value < -20 } }
			}
		}
		complete_effect = { log = "[GetDateText]: [Root.GetName]: Decision EGY_mission_to_Iraq" }
	}

	EGY_mission_to_Lebanon = {
		fire_only_once = no
		available = {
			custom_trigger_tooltip = {
				tooltip = EGY_can_send_evergreen_mission_TT
				check_variable = { ROOT.current_evergreen_capacity < ROOT.current_max_evergreen_capacity }
			}
			NOT = { has_decision = EGY_mission_to_Israel }
			NOT = { has_decision = EGY_mission_to_Sudan }
			NOT = { has_decision = EGY_mission_to_Libya }
			NOT = { has_decision = EGY_mission_to_Algeria }
			NOT = { has_decision = EGY_mission_to_Jordan }
			NOT = { has_decision = EGY_mission_to_Syria }
			NOT = { has_decision = EGY_mission_to_Saudia }
			NOT = { has_decision = EGY_mission_to_Iraq }
			NOT = { has_decision = EGY_mission_to_uae }
			NOT = { has_decision = EGY_mission_to_Turkey }
			NOT = { has_country_flag = EGY_pending_offer }
			NOT = { LEB = { has_idea = EGY_evergreen_fertilizers_idea } }
		}
		visible = { country_exists = LEB }

		days_remove = 10
		days_re_enable = 250
		cost = 25

		remove_effect = {
			set_country_flag = EGY_pending_offer
			LEB = { country_event = egypt_evergreen.1 }
		}

		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				ROOT = { has_opinion = { target = LEB value < -20 } }
			}
		}
		complete_effect = { log = "[GetDateText]: [Root.GetName]: Decision EGY_mission_to_Lebanon" }
	}

	EGY_mission_to_uae = {
		fire_only_once = no
		available = {
			custom_trigger_tooltip = {
				tooltip = EGY_can_send_evergreen_mission_TT
				check_variable = { ROOT.current_evergreen_capacity < ROOT.current_max_evergreen_capacity }
			}
			NOT = { has_decision = EGY_mission_to_Israel }
			NOT = { has_decision = EGY_mission_to_Sudan }
			NOT = { has_decision = EGY_mission_to_Libya }
			NOT = { has_decision = EGY_mission_to_Algeria }
			NOT = { has_decision = EGY_mission_to_Jordan }
			NOT = { has_decision = EGY_mission_to_Syria }
			NOT = { has_decision = EGY_mission_to_Saudia }
			NOT = { has_decision = EGY_mission_to_Iraq }
			NOT = { has_decision = EGY_mission_to_Lebanon }
			NOT = { has_decision = EGY_mission_to_Turkey }
			NOT = { has_country_flag = EGY_pending_offer }
			NOT = { UAE = { has_idea = EGY_evergreen_fertilizers_idea } }
		}
		visible = { country_exists = UAE }

		days_remove = 10
		days_re_enable = 250
		cost = 25

		remove_effect = {
			set_country_flag = EGY_pending_offer
			UAE = { country_event = egypt_evergreen.1 }
		}

		ai_will_do = {
			factor = 20
			modifier = { factor = 20 }
		}
		complete_effect = { log = "[GetDateText]: [Root.GetName]: Decision EGY_mission_to_uae" }
	}
	EGY_mission_to_Turkey = {
		fire_only_once = no
		available = {
			custom_trigger_tooltip = {
				tooltip = EGY_can_send_evergreen_mission_TT
				check_variable = { ROOT.current_evergreen_capacity < ROOT.current_max_evergreen_capacity }
			}
			NOT = { has_decision = EGY_mission_to_Israel }
			NOT = { has_decision = EGY_mission_to_Sudan }
			NOT = { has_decision = EGY_mission_to_Libya }
			NOT = { has_decision = EGY_mission_to_Algeria }
			NOT = { has_decision = EGY_mission_to_Jordan }
			NOT = { has_decision = EGY_mission_to_Syria }
			NOT = { has_decision = EGY_mission_to_Saudia }
			NOT = { has_decision = EGY_mission_to_Iraq }
			NOT = { has_decision = EGY_mission_to_Lebanon }
			NOT = { has_decision = EGY_mission_to_uae }
			NOT = { has_country_flag = EGY_pending_offer }
			NOT = { TUR = { has_idea = EGY_evergreen_fertilizers_idea } }
		}
		visible = { country_exists = TUR }

		days_remove = 10
		days_re_enable = 250
		cost = 25

		remove_effect = {
			set_country_flag = EGY_pending_offer
			TUR = { country_event = egypt_evergreen.1 }
		}

		ai_will_do = {
			factor = 20
			modifier = { factor = 20 }
		}
		complete_effect = { log = "[GetDateText]: [Root.GetName]: Decision EGY_mission_to_Turkey" }
	}
}

EGY_coptic_decision = {
	EGY_appease_coptic_people = {
		visible = { original_tag = EGY }
		icon = GFX_decision_copts_speaker_button
		cost = 65
		days_remove = 65
		fire_only_once = no
		available = {
			NOT = { has_decision = EGY_promote_coptic_politicians }
			NOT = { has_decision = EGY_raids_on_coptic_people }
			NOT = { has_decision = EGY_governmental_pressure }
			NOT = { has_decision = EGY_copt_coalition }
			NOT = { has_decision = EGY_appease_coptic_people }
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision EGY_appease_coptic_people"
			set_temp_variable = { coptic_opinion_change = 4 }
			modify_coptic_opinion_effect = yes
		}
	}
	#
	EGY_promote_coptic_politicians = {
		visible = { original_tag = EGY }
		icon = GFX_decision_copts_propa_button
		cost = 45
		days_remove = 73
		fire_only_once = no
		available = {
			NOT = { has_decision = EGY_appease_coptic_people }
			NOT = { has_decision = EGY_raids_on_coptic_people }
			NOT = { has_decision = EGY_governmental_pressure }
			NOT = { has_decision = EGY_copt_coalition }
			NOT = { has_decision = EGY_promote_coptic_politicians }
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision EGY_promote_coptic_politicians"
			set_temp_variable = { coptic_opinion_change = 7 }
			modify_coptic_opinion_effect = yes
		}
	}
	#
	EGY_raids_on_coptic_people = {
		visible = { original_tag = EGY }
		icon = GFX_decision_copts_raid_button
		cost = 55
		days_remove = 67
		fire_only_once = no
		available = {
			NOT = { has_decision = EGY_appease_coptic_people }
			NOT = { has_decision = EGY_raids_on_coptic_people }
			NOT = { has_decision = EGY_governmental_pressure }
			NOT = { has_decision = EGY_copt_coalition }
			NOT = { has_decision = EGY_promote_coptic_politicians }
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision EGY_raids_on_coptic_people"
			set_temp_variable = { coptic_opinion_change = -8 }
			modify_coptic_opinion_effect = yes
		}
	}
	#
	EGY_governmental_pressure = {
		visible = { original_tag = EGY }
		icon = GFX_decision_copts_pressure_button
		cost = 60
		days_remove = 75
		fire_only_once = no
		available = {
			NOT = { has_decision = EGY_appease_coptic_people }
			NOT = { has_decision = EGY_raids_on_coptic_people }
			NOT = { has_decision = EGY_governmental_pressure }
			NOT = { has_decision = EGY_copt_coalition }
			NOT = { has_decision = EGY_promote_coptic_politicians }
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision EGY_governmental_pressure"
			set_temp_variable = { coptic_opinion_change = -3 }
			modify_coptic_opinion_effect = yes
			set_temp_variable = { coptic_pol_influence_change = -5 }
			modify_coptic_pol_influence_effect = yes
		}
	}
	EGY_copt_coalition = {
		visible = { original_tag = EGY }
		icon = GFX_decision_copts_govern_button
		cost = 60
		days_remove = 75
		available = {
			NOT = { has_decision = EGY_appease_coptic_people }
			NOT = { has_decision = EGY_raids_on_coptic_people }
			NOT = { has_decision = EGY_governmental_pressure }
			NOT = { has_decision = EGY_copt_coalition }
			NOT = { has_decision = EGY_promote_coptic_politicians }
		}
		visible = {
			NOT = { neutrality_neutral_autocracy_are_in_power = yes }
		}

		available = {
			NOT = { is_in_array = { gov_coalition_array = 13 } }
			custom_trigger_tooltip = {
				check_variable = { EGY_coptic_people_opinion > 80 }
				check_variable = { EGY_copts_political_influence > 60 }
				tooltip = copt_coalition_decision_tt
			}
		}
		fire_only_once = no

		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision EGY_copt_coalition"
			custom_effect_tooltip = EGY_copt_coalition_create
			hidden_effect = {
				add_to_array = { gov_coalition_array = 13 }
				subtract_from_variable = { party_pop_array^13 = current_cons_popularity }
				recalculate_party = yes
				update_government_coalition_strength = yes
				set_coalition_drift = yes
			}
			clr_country_flag = party13_banned
			set_temp_variable = { coptic_opinion_change = 8 }
			modify_coptic_opinion_effect = yes
			set_temp_variable = { coptic_pol_influence_change = 5 }
			modify_coptic_pol_influence_effect = yes
			set_party_index_to_ruling_party = yes
			set_temp_variable = { party_popularity_increase = 0.05 }
			set_temp_variable = { temp_outlook_increase = 0.05 }
			add_relative_party_popularity = yes
			set_temp_variable = { party_index = 13 }
			set_temp_variable = { party_popularity_increase = 0.05 }
			set_temp_variable = { temp_outlook_increase = 0.05 }
			add_relative_party_popularity = yes
		}
	}
}

EGY_slums_decision = {
	EGY_remove_slums_in_alexandria = {
		visible = {
			original_tag = EGY
			owns_state = 216
			216 = {
				OR = {
					has_dynamic_modifier = { modifier = EGY_slums_4_level }
					has_dynamic_modifier = { modifier = EGY_slums_3_level }
					has_dynamic_modifier = { modifier = EGY_slums_2_level }
					has_dynamic_modifier = { modifier = EGY_slums_1_level }
				}
			}
		}
		available = { NOT = { has_country_flag = EGY_already_remove_slums } }
		highlight_states = { highlight_states_trigger = { state = 216 } }
		icon = GFX_decision_slums_up_button
		days_remove = 120
		fire_only_once = no

		modifier = { consumer_goods_factor = 0.05 }

		custom_cost_text = cost_4_0

		custom_cost_trigger = {
			check_variable = { treasury > 3.99 }
		}

		complete_effect = {
			set_country_flag = EGY_already_remove_slums
			set_temp_variable = { treasury_change = -4 }
			modify_treasury_effect = yes
		}

		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision EGY_remove_slums_in_alexandria"
			clr_country_flag = EGY_already_remove_slums
			if = {
				limit = { 216 = { has_dynamic_modifier = { modifier = EGY_slums_4_level } } }
				216 = { remove_dynamic_modifier = { modifier = EGY_slums_4_level } }
				216 = { add_dynamic_modifier = { modifier = EGY_slums_3_level } }
			}
			else_if = {
				limit = { 216 = { has_dynamic_modifier = { modifier = EGY_slums_3_level } } }
				216 = { remove_dynamic_modifier = { modifier = EGY_slums_3_level } }
				216 = { add_dynamic_modifier = { modifier = EGY_slums_2_level } }
			}
			else_if = {
				limit = { 216 = { has_dynamic_modifier = { modifier = EGY_slums_2_level } } }
				216 = { remove_dynamic_modifier = { modifier = EGY_slums_2_level } }
				216 = { add_dynamic_modifier = { modifier = EGY_slums_1_level } }
			}
			else_if = {
				limit = { 216 = { has_dynamic_modifier = { modifier = EGY_slums_1_level } } }
				216 = { remove_dynamic_modifier = { modifier = EGY_slums_1_level } }
			}
			subtract_from_variable = { EGY_slums_left = 1 }
			check_broken_economy_conditions_for_reducing = yes
			add_stability = 0.005
			hidden_effect = { country_event = egypt.149 }
		}
		ai_will_do = {
			factor = 10
			modifier = {
				216 = { has_dynamic_modifier = { modifier = EGY_slums_1_level } }
				add = 10
			}
			modifier = {
				216 = { has_dynamic_modifier = { modifier = EGY_slums_2_level } }
				add = 10
			}
			modifier = {
				216 = { has_dynamic_modifier = { modifier = EGY_slums_3_level } }
				add = 15
			}
			modifier = {
				216 = { has_dynamic_modifier = { modifier = EGY_slums_4_level } }
				add = 20
			}
		}
	}
	EGY_remove_slums_in_cairo = {
		visible = {
			original_tag = EGY
			owns_state = 215
			215 = {
				OR = {
					has_dynamic_modifier = { modifier = EGY_slums_5_level }
					has_dynamic_modifier = { modifier = EGY_slums_4_level }
					has_dynamic_modifier = { modifier = EGY_slums_3_level }
					has_dynamic_modifier = { modifier = EGY_slums_2_level }
					has_dynamic_modifier = { modifier = EGY_slums_1_level }
				}
			}
		}
		available = { NOT = { has_country_flag = EGY_already_remove_slums } }
		highlight_states = { highlight_states_trigger = { state = 215 } }
		icon = GFX_decision_slums_up_button
		days_remove = 180
		fire_only_once = no

		modifier = { consumer_goods_factor = 0.1 }

		custom_cost_text = cost_6_0

		custom_cost_trigger = {
			check_variable = { treasury > 5.99 }
		}

		complete_effect = {
			set_country_flag = EGY_already_remove_slums
			set_temp_variable = { treasury_change = -6 }
			modify_treasury_effect = yes
		}

		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision EGY_remove_slums_in_alexandria"
			clr_country_flag = EGY_already_remove_slums
			if = {
				limit = { 215 = { has_dynamic_modifier = { modifier = EGY_slums_5_level } } }
				215 = { remove_dynamic_modifier = { modifier = EGY_slums_5_level } }
				215 = { add_dynamic_modifier = { modifier = EGY_slums_4_level } }
			}
			else_if = {
				limit = { 215 = { has_dynamic_modifier = { modifier = EGY_slums_4_level } } }
				215 = { remove_dynamic_modifier = { modifier = EGY_slums_4_level } }
				215 = { add_dynamic_modifier = { modifier = EGY_slums_3_level } }
			}
			else_if = {
				limit = { 215 = { has_dynamic_modifier = { modifier = EGY_slums_3_level } } }
				215 = { remove_dynamic_modifier = { modifier = EGY_slums_3_level } }
				215 = { add_dynamic_modifier = { modifier = EGY_slums_2_level } }
			}
			else_if = {
				limit = { 215 = { has_dynamic_modifier = { modifier = EGY_slums_2_level } } }
				215 = { remove_dynamic_modifier = { modifier = EGY_slums_2_level } }
				215 = { add_dynamic_modifier = { modifier = EGY_slums_1_level } }
			}
			else_if = {
				limit = { 215 = { has_dynamic_modifier = { modifier = EGY_slums_1_level } } }
				215 = { remove_dynamic_modifier = { modifier = EGY_slums_1_level } }
			}
			subtract_from_variable = { EGY_slums_left = 1 }
			check_broken_economy_conditions_for_reducing = yes
			add_stability = 0.005
			hidden_effect = { country_event = egypt.149 }
		}
		ai_will_do = {
			factor = 10
			modifier = {
				215 = { has_dynamic_modifier = { modifier = EGY_slums_1_level } }
				add = 10
			}
			modifier = {
				215 = { has_dynamic_modifier = { modifier = EGY_slums_2_level } }
				add = 15
			}
			modifier = {
				215 = { has_dynamic_modifier = { modifier = EGY_slums_3_level } }
				add = 20
			}
			modifier = {
				215 = { has_dynamic_modifier = { modifier = EGY_slums_4_level } }
				add = 25
			}
			modifier = {
				215 = { has_dynamic_modifier = { modifier = EGY_slums_5_level } }
				add = 30
			}
		}
	}
	EGY_remove_slums_in_suez = {
		visible = {
			original_tag = EGY
			owns_state = 214
			214 = {
				OR = {
					has_dynamic_modifier = { modifier = EGY_slums_4_level }
					has_dynamic_modifier = { modifier = EGY_slums_3_level }
					has_dynamic_modifier = { modifier = EGY_slums_2_level }
					has_dynamic_modifier = { modifier = EGY_slums_1_level }
				}
			}
		}
		available = { NOT = { has_country_flag = EGY_already_remove_slums } }
		highlight_states = { highlight_states_trigger = { state = 214 } }
		icon = GFX_decision_slums_up_button
		days_remove = 150
		fire_only_once = no

		custom_cost_text = cost_5_0

		custom_cost_trigger = {
			check_variable = { treasury > 4.99 }
		}

		modifier = { consumer_goods_factor = 0.08 }

		complete_effect = {
			set_country_flag = EGY_already_remove_slums
			set_temp_variable = { treasury_change = -5 }
			modify_treasury_effect = yes
		}

		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision EGY_remove_slums_in_alexandria"
			clr_country_flag = EGY_already_remove_slums
			if = {
				limit = { 214 = { has_dynamic_modifier = { modifier = EGY_slums_4_level } } }
				214 = { remove_dynamic_modifier = { modifier = EGY_slums_4_level } }
				214 = { add_dynamic_modifier = { modifier = EGY_slums_3_level } }
			}
			else_if = {
				limit = { 214 = { has_dynamic_modifier = { modifier = EGY_slums_3_level } } }
				214 = { remove_dynamic_modifier = { modifier = EGY_slums_3_level } }
				214 = { add_dynamic_modifier = { modifier = EGY_slums_2_level } }
			}
			else_if = {
				limit = { 214 = { has_dynamic_modifier = { modifier = EGY_slums_2_level } } }
				214 = { remove_dynamic_modifier = { modifier = EGY_slums_2_level } }
				214 = { add_dynamic_modifier = { modifier = EGY_slums_1_level } }
			}
			else_if = {
				limit = { 214 = { has_dynamic_modifier = { modifier = EGY_slums_1_level } } }
				214 = { remove_dynamic_modifier = { modifier = EGY_slums_1_level } }
			}
			subtract_from_variable = { EGY_slums_left = 1 }
			check_broken_economy_conditions_for_reducing = yes
			add_stability = 0.005
			hidden_effect = { country_event = egypt.149 }
		}
		ai_will_do = {
			factor = 10
			modifier = {
				214 = { has_dynamic_modifier = { modifier = EGY_slums_1_level } }
				add = 10
			}
			modifier = {
				214 = { has_dynamic_modifier = { modifier = EGY_slums_2_level } }
				add = 15
			}
			modifier = {
				214 = { has_dynamic_modifier = { modifier = EGY_slums_3_level } }
				add = 20
			}
			modifier = {
				214 = { has_dynamic_modifier = { modifier = EGY_slums_4_level } }
				add = 25
			}
		}
	}
	EGY_remove_slums_in_nile_valley = {
		visible = {
			original_tag = EGY
			owns_state = 217
			217 = {
				OR = {
					has_dynamic_modifier = { modifier = EGY_slums_4_level }
					has_dynamic_modifier = { modifier = EGY_slums_3_level }
					has_dynamic_modifier = { modifier = EGY_slums_2_level }
					has_dynamic_modifier = { modifier = EGY_slums_1_level }
				}
			}
		}
		available = { NOT = { has_country_flag = EGY_already_remove_slums } }
		highlight_states = { highlight_states_trigger = { state = 217 } }
		icon = GFX_decision_slums_up_button
		days_remove = 120
		fire_only_once = no

		custom_cost_text = cost_3_5

		custom_cost_trigger = {
			check_variable = { treasury > 3.49 }
		}

		modifier = { consumer_goods_factor = 0.05 }

		complete_effect = {
			set_country_flag = EGY_already_remove_slums
			set_temp_variable = { treasury_change = -3.5 }
			modify_treasury_effect = yes
		}

		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision EGY_remove_slums_in_alexandria"
			clr_country_flag = EGY_already_remove_slums
			if = {
				limit = { 217 = { has_dynamic_modifier = { modifier = EGY_slums_3_level } } }
				217 = { remove_dynamic_modifier = { modifier = EGY_slums_3_level } }
				217 = { add_dynamic_modifier = { modifier = EGY_slums_2_level } }
			}
			else_if = {
				limit = { 217 = { has_dynamic_modifier = { modifier = EGY_slums_2_level } } }
				217 = { remove_dynamic_modifier = { modifier = EGY_slums_2_level } }
				217 = { add_dynamic_modifier = { modifier = EGY_slums_1_level } }
			}
			else_if = {
				limit = { 217 = { has_dynamic_modifier = { modifier = EGY_slums_1_level } } }
				217 = { remove_dynamic_modifier = { modifier = EGY_slums_1_level } }
			}
			subtract_from_variable = { EGY_slums_left = 1 }
			check_broken_economy_conditions_for_reducing = yes
			add_stability = 0.005
			hidden_effect = { country_event = egypt.149 }
		}
		ai_will_do = {
			factor = 10
			modifier = {
				217 = { has_dynamic_modifier = { modifier = EGY_slums_1_level } }
				add = 10
			}
			modifier = {
				217 = { has_dynamic_modifier = { modifier = EGY_slums_2_level } }
				add = 15
			}
			modifier = {
				217 = { has_dynamic_modifier = { modifier = EGY_slums_3_level } }
				add = 20
			}
		}
	}
}