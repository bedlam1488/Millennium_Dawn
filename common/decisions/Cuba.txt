#ter defence
CUB_ter_defence_category = {

	CUB_ter_oboronaup_start = {
		cost = 15
		icon = CUB_traien_usa
		days_remove = 10
		fire_only_once = yes

		available = {
			has_war = yes
		}
		visible = {
				has_completed_focus = CUB_create_ter_defense
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove CUB_ter_oboronaup_start"
			add_manpower = -5000
			set_temp_variable = { treasury_change = -6 }
			modify_treasury_effect = yes
			division_template = {
				name = "Territorial Defense Brigade"
				is_locked = yes
				regiments = {
					Militia_Bat = { x = 0 y = 0 }
					Militia_Bat = { x = 0 y = 1 }
					Militia_Bat = { x = 1 y = 0 }
					Militia_Bat = { x = 1 y = 1 }
					Militia_Bat = { x = 1 y = 2 }
					Militia_Bat = { x = 2 y = 1 }
					Arty_Bat = { x = 2 y = 0 }
					Arty_Bat = { x = 2 y = 1 }
					Arty_Bat = { x = 2 y = 2 }
				}
				support = {
					Mot_Recce_Comp = { x = 0 y = 0 }
					L_Engi_Comp = { x = 0 y = 1 }
				}
			}
			random_owned_controlled_state = {
				limit = { ROOT = { has_full_control_of_state = PREV } }
				prioritize = { 854 }
				create_unit = {
					division = "name = \"Territorial Defense Brigade\" division_template = \"Territorial Defense Brigade\" start_experience_factor = 1"
					owner = CUB
				}
			}
		}
	}
	CUB_ter_oboronaup = {
		cost = 15
		icon = CUB_traien_usa
		days_remove = 10
		fire_only_once = yes

		available = {
			has_war = yes
		}
		visible = {
			has_completed_focus = CUB_create_ter_defense
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove CUB_ter_oboronaup_start"
			add_manpower = -5000
			set_temp_variable = { treasury_change = -6 }
			modify_treasury_effect = yes
			division_template = {
				name = "Territorial Defense Brigade"
				is_locked = yes
		regiments = {
			Militia_Bat = { x = 0 y = 0 }
			Militia_Bat = { x = 0 y = 1 }
			Militia_Bat = { x = 1 y = 0 }
			Militia_Bat = { x = 1 y = 1 }
			Militia_Bat = { x = 1 y = 2 }
			Militia_Bat = { x = 2 y = 1 }
			Arty_Bat = { x = 2 y = 0 }
			Arty_Bat = { x = 2 y = 1 }
			Arty_Bat = { x = 2 y = 2 }
		}
		support = {
			Mot_Recce_Comp = { x = 0 y = 0 }
			L_Engi_Comp = { x = 0 y = 1 }
			}
		}
		random_owned_controlled_state = {
			limit = { ROOT = { has_full_control_of_state = PREV } }
			prioritize = { 855 }
			create_unit = {
				division = "name = \"Territorial Defense Brigade\" division_template = \"Territorial Defense Brigade\" start_experience_factor = 1"
				owner = CUB
				}
			}
		}
	}
}

# Blockade Mechanics
CUB_blockade_fight_category = {
	CUB_buy_healthcare = {
		icon = GFX_decision_cub_health_button
		days_remove = 105
		fire_only_once = no
		cost = 150
		modifier = {
			health_cost_multiplier_modifier = 0.05
		}
		ai_will_do = { factor = 355 }
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: CUB_us_citizens_propoganda"
				add_stability = 0.05
			set_temp_variable = { treasury_change = -10 }
			modify_treasury_effect = yes
			set_temp_variable = { percent_change = -5 }
			set_temp_variable = { tag_index = USA }
			set_temp_variable = { influence_target = CUB }
			change_influence_percentage = yes
		}
	}
	CUB_attract_eastern_inv = {
		days_remove = 90
		icon = GFX_decision_cub_rusmoney_button
		fire_only_once = no
		cost = 120
		available = {
			SOV = {
				has_opinion = {
					target = CUB
					value >19
				}
			}
			country_exists = SOV
			NOT = {
				has_war_with = SOV
			}
			CHI = {
				has_opinion = {
					target = CUB
					value >19
				}
			}
			country_exists = CHI
			NOT = {
				has_war_with = CHI
			}
			RAJ = {
				has_opinion = {
					target = CUB
					value >19
				}
			}
			country_exists = RAJ
			NOT = {
				has_war_with = RAJ
			}
		}
		ai_will_do = { factor = 355 }
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: CUB_attract_eastern_inv"
			set_temp_variable = { treasury_change = 10 }
			modify_treasury_effect = yes
			set_temp_variable = { percent_change = -3 }
			set_temp_variable = { tag_index = USA }
			set_temp_variable = { influence_target = CUB }
			change_influence_percentage = yes
			set_temp_variable = { percent_change = 4 }
			set_temp_variable = { tag_index = SOV }
			set_temp_variable = { influence_target = CUB }
			change_influence_percentage = yes
			set_temp_variable = { percent_change = 2 }
			set_temp_variable = { tag_index = RAJ }
			set_temp_variable = { influence_target = CUB }
			change_influence_percentage = yes
			set_temp_variable = { percent_change = 3 }
			set_temp_variable = { tag_index = CHI }
			set_temp_variable = { influence_target = CUB }
			change_influence_percentage = yes
		}
	}
	#USA
	CUB_meeting_of_USA = {
		days_remove = 45
		icon = GFX_decision_cub_usa_button
		fire_only_once = no
		cost = 100
		available = {
			USA = {
				has_opinion = {
					target = CUB
					value >-21
				}
			}
			country_exists = USA
			NOT = {
				has_war_with = USA
			}
		}
		ai_will_do = { factor = 355 }
		visible = {
			USA = {
				has_country_flag = blockade_cuba_yes
			}
		}
	remove_effect = {
		log = "[GetDateText]: [Root.GetName]: CUB_meeting_of_USA"
		USA = {
			add_opinion_modifier = {
				target = CUB
				modifier = small_increase
			}
		}
		CUB = {
			add_opinion_modifier = {
				target = USA
				modifier = small_increase
				}
			}
		}
	}
	CUB_normalisation_with_USA = {
		icon = GFX_decision_usa
		days_remove = 45
		fire_only_once = no
		cost = 100
		ai_will_do = { factor = 355 }
		available = {
			USA = {
				has_opinion = {
					target = CUB
					value > 10
				}
			}
		}
		visible = {
			USA = {
				has_country_flag = blockade_cuba_yes
			}
			CUB = { has_completed_focus = CUB_demand_from_us }
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: CUB_meeting_of_USA"
			USA = {
				add_opinion_modifier = {
					target = CUB
					modifier = medium_increase
				}
			}
			CUB = {
				add_opinion_modifier = {
				target = USA
				modifier = medium_increase
				}
			}
		}
	}
	CUB_diplomacy_with_USA = {
		icon = GFX_decision_usa
		days_remove = 45
		fire_only_once = no
		cost = 100
		ai_will_do = { factor = 355 }
		available = {
			USA = {
				has_opinion = {
					target = CUB
					value > 30
				}
			}
		}
		visible = {
			USA = {
				has_country_flag = blockade_cuba_yes
			}
			CUB = { has_completed_focus = CUB_demand_from_us }
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: CUB_diplomacy_with_USA"
			USA = {
				add_opinion_modifier = {
					target = CUB
					modifier = large_increase
				}
			}
			CUB = {
				add_opinion_modifier = {
					target = USA
					modifier = large_increase
				}
			}
		}
	}
	CUB_open_bank_and_trade_with_USA = {
		icon = GFX_decision_usa
		days_remove = 45
		fire_only_once = no
		cost = 100
		ai_will_do = { factor = 355 }
		available = {
			USA = {
				has_opinion = {
					target = CUB
					value > 50
				}
			}
		}
		visible = {
			USA = {
				has_country_flag = blockade_cuba_yes
			}
			CUB = { has_completed_focus = CUB_demand_from_us }
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: CUB_open_bank_and_trade_with_USA"
			USA = {
				add_opinion_modifier = {
					target = CUB
					modifier = large_commercial_relations
				}
			}
			CUB = {
				add_opinion_modifier = {
					target = USA
					modifier = large_commercial_relations
				}
			}
		}
	}
	CUB_negotations_with_USA = {
		icon = GFX_decision_usa
		days_remove = 45
		fire_only_once = no
		cost = 100
		ai_will_do = { factor = 355 }
		available = {
			USA = {
				has_opinion = {
					target = CUB
					value > 60
				}
			}
		}
		visible = {
			USA = {
				has_country_flag = blockade_cuba_yes
			}
			CUB = { has_completed_focus = CUB_demand_from_us }
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: CUB_open_bank_and_trade_with_USA"
			USA = {
				add_opinion_modifier = {
					target = CUB
					modifier = small_increase
				}
			}
			CUB = {
				add_opinion_modifier = {
					target = USA
					modifier = small_increase
				}
			}
			USA = { country_event = { id =cuba.66 days =  1 } }
		}
	}
	#STARTING DECISIONS
	CUB_open_borders_inv = {
		days_remove = 120
		fire_only_once = yes
		icon = GFX_decision_cub_tourism_button
		cost = 135
		ai_will_do = { factor = 355 }
		modifier = {
			political_power_factor = 0.05
			bureaucracy_cost_multiplier_modifier = 0.05
			police_cost_multiplier_modifier = 0.03
			foreign_influence_defense_modifier = 0.02
			influence_coup_modifier = 0.02
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: CUB_open_borders_inv"
			add_ideas = open_borders
			set_temp_variable = { treasury_change = 10 }
			modify_treasury_effect = yes
			set_temp_variable = { percent_change = -3 }
			set_temp_variable = { tag_index = USA }
			set_temp_variable = { influence_target = CUB }
			change_influence_percentage = yes
			set_temp_variable = { percent_change = 2 }
			set_temp_variable = { tag_index = SOV }
			set_temp_variable = { influence_target = CUB }
			change_influence_percentage = yes
			set_temp_variable = { percent_change = 2 }
			set_temp_variable = { tag_index = CHI }
			set_temp_variable = { influence_target = CUB }
			change_influence_percentage = yes
			set_temp_variable = { percent_change = 2 }
			set_temp_variable = { tag_index = CAN }
			set_temp_variable = { influence_target = CUB }
			change_influence_percentage = yes
			set_country_flag = open_borders
			add_manpower = 3000
		}
	}
	CUB_border_measures_inv = {
		icon = GFX_decision_antiinfl_button
		days_remove = 70
		fire_only_once = yes
		cost = 100
		ai_will_do = { factor = 355 }
		modifier = {
			political_power_factor = 0.05
			bureaucracy_cost_multiplier_modifier = -0.05
			police_cost_multiplier_modifier = -0.02
			receiving_investment_duration_modifier = -0.04
			investment_cost_modifier = -0.04
			foreign_influence_defense_modifier = 0.02
			influence_coup_modifier = 0.02
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: CUB_open_borders_inv"
			set_temp_variable = { treasury_change = 10 }
			modify_treasury_effect = yes
			set_temp_variable = { percent_change = -5 }
			set_temp_variable = { tag_index = USA }
			set_temp_variable = { influence_target = CUB }
			change_influence_percentage = yes
			set_temp_variable = { percent_change = 2 }
			set_temp_variable = { tag_index = SOV }
			set_temp_variable = { influence_target = CUB }
			change_influence_percentage = yes
			set_temp_variable = { percent_change = 2 }
			set_temp_variable = { tag_index = CHI }
			set_temp_variable = { influence_target = CUB }
			change_influence_percentage = yes
			set_temp_variable = { percent_change = 2 }
			set_temp_variable = { tag_index = CAN }
			set_temp_variable = { influence_target = CUB }
			change_influence_percentage = yes
			set_temp_variable = { percent_change = 2 }
			set_temp_variable = { tag_index = ENG }
			set_temp_variable = { influence_target = CUB }
			change_influence_percentage = yes
		}
	}
	CUB_propoganda_of_time = {
		icon = GFX_decision_inlfpropaganda_button
		days_remove = 150
		fire_only_once = no
		cost = 150
		ai_will_do = { factor = 355 }
		modifier = {
			political_power_factor = -0.05
			bureaucracy_cost_multiplier_modifier = 0.05
			propaganda_campaign_cost_modifier = 0.03
			foreign_influence_defense_modifier = 0.02
			influence_coup_modifier = 0.02
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: CUB_propoganda_of_time_inv"
			set_party_index_to_ruling_party = yes
			set_temp_variable = { party_popularity_increase = 0.003 }
			add_relative_party_popularity = yes
			add_stability = 0.05
		}
	}
	CUB_ask_for_chinese_money = {
		icon = GFX_decision_recogn_china_button
		days_remove = 120
		fire_only_once = no
		cost = 80
		available = {
			CHI = {
				has_opinion = {
					target = CUB
					value >19
				}
			}
		}
		ai_will_do = { factor = 355 }
		modifier = {
			political_power_factor = -0.05
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: CUB_ask_for_chinese_money"
			set_party_index_to_ruling_party = yes
			set_temp_variable = { party_popularity_increase = 0.015 }
			add_relative_party_popularity = yes
			add_stability = 0.05
			set_temp_variable = { treasury_change = 7 }
			modify_treasury_effect = yes
			set_temp_variable = { percent_change = 8 }
			set_temp_variable = { tag_index = CHI }
			set_temp_variable = { influence_target = CUB }
			change_influence_percentage = yes
		}
	}
	CUB_develop_projects_on_DOM = {
		days_remove = 45
		fire_only_once = yes
		cost = 165
		icon = GFX_decision_cub_dom_button
		ai_will_do = { factor = 355 }
		available = {
			DOM = { influence_higher_40 = yes }
			DOM = {
				has_opinion = {
					target = CUB
					value >19
				}
			}
			country_exists = DOM
			NOT = {
				has_war_with = DOM
			}
		}
		visible = {
			has_country_flag = open_borders
		}
		modifier = {
			bureaucracy_cost_multiplier_modifier = 0.05
			propaganda_campaign_cost_modifier = 0.03
			foreign_influence_defense_modifier = 0.02
			influence_coup_modifier = 0.02
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: CUB_develop_projects_on_DOM"
			DOM = {
				862 = {
					add_extra_state_shared_building_slots = 1
					add_building_construction = {
						type = infrastructure
						level = 1
						instant_build = yes
					}
				}
			}
			set_temp_variable = { percent_change = 5 }
			set_temp_variable = { tag_index = CUB }
			set_temp_variable = { influence_target = DOM }
			change_influence_percentage = yes
			set_temp_variable = { percent_change = -5 }
			set_temp_variable = { tag_index = USA }
			set_temp_variable = { influence_target = DOM }
			change_influence_percentage = yes
			set_temp_variable = { int_investment_change = 2 }
			modify_international_investment_effect = yes
		}
	}
	CUB_develop_projects_on_HAI = {
		days_remove = 165
		fire_only_once = yes
		cost = 150
		icon = GFX_decision_cub_hai_button
		ai_will_do = { factor = 355 }
		available = {
			HAI = { influence_higher_40 = yes }
			HAI = {
				has_opinion = {
					target = CUB
					value >19
				}
			}
			country_exists = HAI
			NOT = {
				has_war_with = HAI
			}
		}
		visible = {
			has_country_flag = open_borders
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: CUB_develop_projects_on_HAI"
			HAI = {
				859 = {
					add_extra_state_shared_building_slots = 1
					add_building_construction = {
						type = infrastructure
						level = 1
						instant_build = yes
					}
				}
			}
			set_temp_variable = { percent_change = 5 }
			set_temp_variable = { tag_index = CUB }
			set_temp_variable = { influence_target = HAI }
			change_influence_percentage = yes
			set_temp_variable = { percent_change = -5 }
			set_temp_variable = { tag_index = USA }
			set_temp_variable = { influence_target = HAI }
			change_influence_percentage = yes
			set_temp_variable = { int_investment_change = 3 }
			modify_international_investment_effect = yes
		}
	}
	#FIX
	CUB_tourism_with_CAN = {
		days_remove = 150
		fire_only_once = no
		cost = 120
		icon = GFX_decision_cub_canada_button
		ai_will_do = { factor = 355 }
		available = {
			CAN = {
				has_opinion = {
					target = CUB
					value >19
				}
			}
			country_exists = CAN
			NOT = {
				has_war_with = CAN
			}
		}
		visible = {
			has_country_flag = open_borders
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: CUB_open_bank_and_trade_with_USA"
			set_temp_variable = { treasury_change = 5 }
			modify_treasury_effect = yes
			set_temp_variable = { percent_change = 3 }
			set_temp_variable = { tag_index = CUB }
			set_temp_variable = { influence_target = CAN }
			change_influence_percentage = yes
			set_temp_variable = { percent_change = -6 }
			set_temp_variable = { tag_index = USA }
			set_temp_variable = { influence_target = CAN }
			change_influence_percentage = yes
		}
	}
	CUB_trade_with_CAN = {
		days_remove = 150
		fire_only_once = yes
		cost = 120
		ai_will_do = { factor = 355 }
		icon = GFX_decision_cub_canada_button
		available = {
			CAN = {
				has_opinion = {
					target = CUB
					value >29
				}
			}
			country_exists = CAN
			NOT = {
				has_war_with = CAN
			}
		}
		visible = {
			has_country_flag = open_borders
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: CUB_trade_with_CAN"
			CAN = {
				add_opinion_modifier = {
					target = CUB
					modifier = large_commercial_relations
				}
			}
			CUB = {
				add_opinion_modifier = {
					target = CAN
					modifier = large_commercial_relations
				}
			}
			set_temp_variable = { receiver_nation = CAN.id }
			set_temp_variable = { sender_nation = CUB }
			set_improved_trade_agreement = yes
			set_temp_variable = { percent_change = 5 }
			set_temp_variable = { tag_index = CUB }
			set_temp_variable = { influence_target = CAN }
			change_influence_percentage = yes
			set_temp_variable = { percent_change = -5 }
			set_temp_variable = { tag_index = USA }
			set_temp_variable = { influence_target = CAN }
			change_influence_percentage = yes
		}
	}
	CUB_trade_with_MEX = {
		days_remove = 150
		fire_only_once = yes
		icon = GFX_decision_cub_mexico_button
		cost = 120
		ai_will_do = { factor = 355 }
		available = {
			MEX = {
				has_opinion = {
					target = CUB
					value >19
				}
			}
			country_exists = MEX
			NOT = {
				has_war_with = MEX
			}
		}
		visible = {
			has_country_flag = open_borders
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: CUB_trade_with_MEX"
			set_temp_variable = { receiver_nation = MEX.id }
			set_temp_variable = { sender_nation = CUB }
			set_improved_trade_agreement = yes
		}
	}
	CUB_dolg_with_MEX = {
		days_remove = 150
		fire_only_once = yes
		cost = 150
		icon = GFX_decision_cub_mexico_button
		ai_will_do = { factor = 355 }
		available = {
			MEX = {
				has_opinion = {
					target = CUB
					value >19
				}
			}
			country_exists = MEX
			NOT = {
				has_war_with = MEX
			}
		}
		visible = {
			has_country_flag = open_borders
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: CUB_dolg_with_MEX"
			set_temp_variable = { debt_change = -5 }
			modify_debt_effect = yes
			MEX = {
				add_opinion_modifier = {
					target = CUB
					modifier = large_commercial_relations
				}
			}
			CUB = {
				add_opinion_modifier = {
					target = MEX
					modifier = large_commercial_relations
				}
			}
			set_temp_variable = { percent_change = -5 }
			set_temp_variable = { tag_index = USA }
			set_temp_variable = { influence_target = CUB }
			change_influence_percentage = yes
		}
	}
}

CUB_alba_category = {
	CUB_alba_healthcare = {
		cost = 195
		days_remove = 365
		fire_only_once = yes
		icon = cuba_healtalba_button
		modifier = {
			consumer_goods_factor = 0.01
			monthly_population = 0.05
			stability_factor = 0.01
			health_cost_multiplier_modifier = 0.05
		}
		ai_will_do = { factor = 100 }
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove CUB_alba_healthcare"
			add_stability = 0.06
			set_temp_variable = { treasury_change = -7 }
			modify_treasury_effect = yes
			add_to_variable = {
				var = alba_completed
				value = 1
			}
		}
	}
	CUB_alba_schools = {
		cost = 150
		days_remove = 210
		fire_only_once = yes
		icon = cuba_education_button
		modifier = {
			consumer_goods_factor = 0.01
			political_power_factor = 0.03
			research_speed_factor = 0.05
			education_cost_multiplier_modifier = 0.05
			production_speed_internet_station_factor = 0.05
			production_speed_offices_factor = 0.10
			bureaucracy_cost_multiplier_modifier = 0.03

		}
		ai_will_do = { factor = 100 }
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove CUB_alba_schools"
			add_stability = 0.02
			set_temp_variable = { treasury_change = -7 }
			modify_treasury_effect = yes
			add_to_variable = {
				var = alba_completed
				value = 1
			}
		}
	}
	CUB_own_currency = {
		cost = 150
		days_remove = 210
		fire_only_once = yes
		icon = cuba_currency_button
		modifier = {
			political_power_factor = 0.03
			research_speed_factor = 0.05
			consumer_goods_factor = 0.02
			production_speed_infrastructure_factor = 0.10
			production_speed_internet_station_factor = 0.05
			bureaucracy_cost_multiplier_modifier = 0.05
			education_cost_multiplier_modifier = 0.05
		}
		ai_will_do = { factor = 100 }
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove CUB_own_currency"
			add_stability = 0.02
			set_temp_variable = { treasury_change = -7 }
			modify_treasury_effect = yes
			add_to_variable = {
				var = alba_completed
				value = 1
			}
		}
	}
	CUB_alba_petrosur = {
		cost = 150
		days_remove = 250
		fire_only_once = yes
		icon = cuba_oil_button
		modifier = {
			local_resources_factor = 0.15
			research_speed_factor = 0.05
			bureaucracy_cost_multiplier_modifier = -0.02
		}
		ai_will_do = { factor = 100 }
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove CUB_alba_petrosur"
			add_stability = 0.06
			set_temp_variable = { treasury_change = -10 }
			modify_treasury_effect = yes
			add_to_variable = {
				var = alba_completed
				value = 1
			}
		}
	}
	CUB_alba_telesur = {
		cost = 120
		days_remove = 180
		fire_only_once = yes
		icon = cuba_tv_button
		modifier = {
			political_power_factor = 0.03
			industrial_capacity_factory = 0.1
			research_speed_factor = 0.05
		}
		ai_will_do = { factor = 100 }
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove CUB_alba_telesur"
			add_stability = 0.06
			set_temp_variable = { treasury_change = -5 }
			modify_treasury_effect = yes
			add_to_variable = {
				var = alba_completed
				value = 1
			}
		}
	}
	CUB_alba_sport = {
		cost = 100
		days_remove = 180
		fire_only_once = yes
		icon = cuba_sports_button
		modifier = {
			political_power_factor = 0.03
			research_speed_factor = 0.05
		}
		ai_will_do = { factor = 100 }
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove CUB_alba_sport"
			add_stability = 0.03
			set_temp_variable = { treasury_change = -3 }
			modify_treasury_effect = yes
			add_to_variable = {
				var = alba_completed
				value = 1
			}
		}
	}
	CUB_alba_ticker_mission = {
		available = { always = no }
		activation = { has_idea = CUB_alba_idea }
		days_mission_timeout = 800
		is_good = no
		fire_only_once = yes
		icon = none
		cancel_trigger = {
			custom_trigger_tooltip = {
				tooltip = CUB_alba_completion_trigger_TT
				OR = {
					check_variable = { alba_completed = 5 }
					check_variable = { alba_completed > 5 }
				}
			}
		}
		visible = { check_variable = { alba_completed < 5 } }
		cancel_effect = {
			add_stability = 0.12
			country_event = { id = cuba.21 days = 1 }
		}
		timeout_effect = {
			add_stability = -0.12
			add_political_power = -100
			country_event = { id = cuba.22 days = 1 }
		}
	}
}

CUB_confederation_category = {
	CUB_invite_haiti = {
		icon = GFX_decision_cuba_antilean_button
		cost = 80
		fire_only_once = no
		days_remove = 365
		ai_will_do = {
			factor = 355
		}
		available = {
			country_exists = HAI
			NOT = {
				has_war_with = HAI
			}
			HAI = {
				influence_higher_25 = yes
			}
		}
		visible = {
			CUB = { has_completed_focus = CUB_invite_haiti }
			tag = CUB
			HAI = {
				NOT = {
					has_country_flag = antillies_conf_agree
				}
			}
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision SER_invite_bosnia"
			HAI = {	country_event = { id = CubaConf.50 days = 1 } }
		}
	}
	CUB_invite_dominicana = {
		icon = GFX_decision_cuba_antilean_button
		cost = 80
		fire_only_once = no
		days_remove = 365
		ai_will_do = {
			factor = 355
		}
		available = {
			country_exists = DOM
			NOT = {
				has_war_with = DOM
			}
			DOM = {
				influence_higher_25 = yes
			}
		}
		visible = {
			CUB = { has_completed_focus = CUB_invite_dominicana }
			tag = CUB
			DOM = {
				NOT = {
					has_country_flag = antillies_conf_agree
				}
			}
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision SER_invite_bosnia"
			DOM = {	country_event = { id = CubaConf.50 days = 1 } }
		}
	}
	CUB_invite_puerto_rico = {
		icon = GFX_decision_cuba_antilean_button
		cost = 80
		fire_only_once = no
		days_remove = 365
		ai_will_do = {
			factor = 355
		}
		available = {
			country_exists = PTR
			NOT = {
				has_war_with = PTR
			}
			PTR = {
				influence_higher_25 = yes
			}
		}
		visible = {
			CUB = { has_completed_focus = CUB_invite_dominicana }
			tag = CUB
			country_exists = PTR
			PTR = {
				NOT = {
					has_country_flag = antillies_conf_agree
				}
			}
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision SER_invite_bosnia"
			PTR = {	country_event = { id = CubaConf.50 days = 1 } }
		}
	}
	CUB_invite_columbia = {
		icon = GFX_decision_cuba_antilean_button
		cost = 80
		fire_only_once = no
		days_remove = 365
		ai_will_do = {
			factor = 355
		}
		available = {
			country_exists = COL
			NOT = {
				has_war_with = COL
			}
			COL = {
				influence_higher_25 = yes
			}
		}
		visible = {
			CUB = { has_completed_focus = CUB_talsks_with_columbia }
			tag = CUB
			country_exists = COL
			COL = {
				NOT = {
					has_country_flag = antillies_conf_agree
				}
			}
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision SER_invite_bosnia"
			COL = {	country_event = { id = CubaConf.50 days = 1 } }
		}
	}
	CUB_invite_jamaica = {
		cost = 80
		fire_only_once = no
		icon = GFX_decision_cuba_antilean_button
		days_remove = 365
		ai_will_do = {
			factor = 355
		}
		available = {
			country_exists = JAM
			NOT = {
				has_war_with = JAM
			}
			JAM = {
				influence_higher_25 = yes
			}
		}
		visible = {
			CUB = { has_completed_focus = CUB_talsks_with_jamaica }
			tag = CUB
			country_exists = JAM
			JAM = {
				NOT = {
					has_country_flag = antillies_conf_agree
				}
			}
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision CUB_invite_jamaica"
			JAM = {	country_event = { id = CubaConf.50 days = 1 } }
		}
	}
	CUB_ask_puerto = {
		cost = 80
		fire_only_once = no
		icon = GFX_decision_cuba_antilean_button
		days_remove = 40
		ai_will_do = {
			factor = 355
		}
		available = {
			NOT = {
			country_exists = PTR
			}
		}
		visible = {
			CUB = { has_completed_focus = CUB_liberate_puerto_rico }
			tag = CUB
			NOT = {
				country_exists = PTR
			}
			USA = {
				has_country_flag = puerto_disagree
			}
			PTR = {
				NOT = {
					has_country_flag = antillies_conf_agree
				}
			}
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision CUB_ask_puerto"
			USA = {	country_event = { id = cuba.40 days = 1 } }
		}
	}
	CUB_form_confederation = {
		cost = 80
		fire_only_once = yes
		icon = cuba_antileancreate_button
		days_remove = 1
		ai_will_do = {
			factor = 355
		}
		available = {
			country_exists = CUB
			HAI = { is_subject_of = CUB }
			DOM = { is_subject_of = CUB }
			COL = { is_subject_of = CUB }
			JAM = { is_subject_of = CUB }
			PTR = { is_subject_of = CUB }
		}
		visible = {
			tag = CUB
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision CUB_form_confederation"
			confederation_creation = yes
			}
	}
}
