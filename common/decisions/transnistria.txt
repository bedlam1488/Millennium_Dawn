###Created by Lord Bogdanoff
PMR_eastern_europe_operations = {
	PMR_romania_operation = {
		available = {
			NOT = {
				has_country_flag = doing_operation
			}
			has_capitulated = no
		}
		visible = {
			tag = PMR
			country_exists = ROM
			check_variable = { PMR.kolbasna > 700 }
			has_completed_focus = PMR_sell_weapon
		}
		ai_will_do = {
			factor = 80
		}

		icon = GFX_decision_pmr_cobasna_button

		custom_cost_trigger = {
			command_power > 20
		}
		custom_cost_text = command_power_more_than_20

		days_remove = 30
		days_re_enable = 0
		
		
		complete_effect = {
			set_country_flag = doing_operation
			add_command_power = -20
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove PMR_romania_operation: [From.GetName]"
			PMR = { country_event = { id = transnistria.1 days = 1 } }
			clr_country_flag = doing_operation
		}
	}
	PMR_serbia_operation = {
		available = {
			NOT = {
				has_country_flag = doing_operation
			}
			has_capitulated = no
		}
		visible = {
			tag = PMR
			country_exists = SER
			check_variable = { PMR.kolbasna > 700 }
			has_completed_focus = PMR_sell_weapon
		}
		ai_will_do = {
			factor = 80
		}

		icon = GFX_decision_pmr_cobasna_button

		custom_cost_trigger = {
			command_power > 20
		}
		custom_cost_text = command_power_more_than_20
		
		days_remove = 30
		days_re_enable = 0
		
		
		complete_effect = {
			set_country_flag = doing_operation
			add_command_power = -20
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove PMR_serbia_operation: [From.GetName]"
			PMR = { country_event = { id = transnistria.1 days = 1 } }
			clr_country_flag = doing_operation
		}
	}
	PMR_bulgaria_operation = {
		available = {
			NOT = {
				has_country_flag = doing_operation
			}
			has_capitulated = no
		}
		visible = {
			tag = PMR
			country_exists = BUL
			check_variable = { PMR.kolbasna > 700 }
			has_completed_focus = PMR_sell_weapon
		}
		ai_will_do = {
			factor = 80
		}

		icon = GFX_decision_pmr_cobasna_button

		custom_cost_trigger = {
			command_power > 20
		}
		custom_cost_text = command_power_more_than_20
		
		days_remove = 30
		days_re_enable = 0
		
		
		complete_effect = {
			set_country_flag = doing_operation
			add_command_power = -20
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove PMR_bulgaria_operation: [From.GetName]"
			PMR = { country_event = { id = transnistria.1 days = 1 } }
			clr_country_flag = doing_operation
		}
	}
	PMR_montenegro_operation = {
		available = {
			NOT = {
				has_country_flag = doing_operation
			}
			has_capitulated = no
		}
		visible = {
			tag = PMR
			check_variable = { PMR.kolbasna > 700 }
			has_completed_focus = PMR_sell_weapon
		}
		ai_will_do = {
			factor = 80
		}

		icon = GFX_decision_pmr_cobasna_button

		custom_cost_trigger = {
			command_power > 20
		}
		custom_cost_text = command_power_more_than_20
		
		days_remove = 30
		days_re_enable = 0
		
		
		complete_effect = {
			set_country_flag = doing_operation
			add_command_power = -20
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove PMR_montenegro_operation: [From.GetName]"
			PMR = { country_event = { id = transnistria.1 days = 1 } }
			clr_country_flag = doing_operation
		}
	}
	PMR_bosnia_operation = {
		available = {
			NOT = {
				has_country_flag = doing_operation
			}
			has_capitulated = no
		}
		visible = {
			tag = PMR
			country_exists = BOS
			check_variable = { PMR.kolbasna > 700 }
			has_completed_focus = PMR_sell_weapon
		}
		ai_will_do = {
			factor = 80
		}

		icon = GFX_decision_pmr_cobasna_button

		custom_cost_trigger = {
			command_power > 20
		}
		custom_cost_text = command_power_more_than_20
		
		days_remove = 30
		days_re_enable = 0
		
		
		complete_effect = {
			set_country_flag = doing_operation
			add_command_power = -20
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove PMR_bosnia_operation: [From.GetName]"
			PMR = { country_event = { id = transnistria.1 days = 1 } }
			clr_country_flag = doing_operation
		}
	}
	PMR_croatia_operation = {
		available = {
			NOT = {
				has_country_flag = doing_operation
			}
			has_capitulated = no
		}
		visible = {
			tag = PMR
			country_exists = CRO
			check_variable = { PMR.kolbasna > 700 }
			has_completed_focus = PMR_sell_weapon
		}
		ai_will_do = {
			factor = 80
		}

		icon = GFX_decision_pmr_cobasna_button

		custom_cost_trigger = {
			command_power > 20
		}
		custom_cost_text = command_power_more_than_20
		
		days_remove = 30
		days_re_enable = 0
		
		
		complete_effect = {
			set_country_flag = doing_operation
			add_command_power = -20
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove PMR_croatia_operation: [From.GetName]"
			PMR = { country_event = { id = transnistria.1 days = 1 } }
			clr_country_flag = doing_operation
		}
	}
	PMR_greece_operation = {
		available = {
			NOT = {
				has_country_flag = doing_operation
			}
			has_capitulated = no
		}
		visible = {
			tag = PMR
			country_exists = GRE
			check_variable = { PMR.kolbasna > 700 }
			has_completed_focus = PMR_sell_weapon
		}
		ai_will_do = {
			factor = 80
		}

		icon = GFX_decision_pmr_cobasna_button

		custom_cost_trigger = {
			command_power > 20
		}
		custom_cost_text = command_power_more_than_20
		
		days_remove = 30
		days_re_enable = 0
		
		
		complete_effect = {
			set_country_flag = doing_operation
			add_command_power = -20
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove PMR_greece_operation: [From.GetName]"
			PMR = { country_event = { id = transnistria.1 days = 1 } }
			clr_country_flag = doing_operation
		}
	}
	PMR_macedonia_operation = {
		available = {
			NOT = {
				has_country_flag = doing_operation
			}
			has_capitulated = no
		}
		visible = {
			tag = PMR
			check_variable = { PMR.kolbasna > 700 }
			has_completed_focus = PMR_sell_weapon
		}
		ai_will_do = {
			factor = 80
		}

		icon = GFX_decision_pmr_cobasna_button

		custom_cost_trigger = {
			command_power > 20
		}
		custom_cost_text = command_power_more_than_20
		
		days_remove = 30
		days_re_enable = 0
		
		
		complete_effect = {
			set_country_flag = doing_operation
			add_command_power = -20
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove PMR_macedonia_operation: [From.GetName]"
			PMR = { country_event = { id = transnistria.1 days = 1 } }
			clr_country_flag = doing_operation
		}
	}
	PMR_albania_operation = {
		available = {
			NOT = {
				has_country_flag = doing_operation
			}
			has_capitulated = no
		}
		visible = {
			tag = PMR
			country_exists = ALB
			check_variable = { PMR.kolbasna > 700 }
			has_completed_focus = PMR_sell_weapon
		}
		ai_will_do = {
			factor = 80
		}

		icon = GFX_decision_pmr_cobasna_button

		custom_cost_trigger = {
			command_power > 20
		}
		custom_cost_text = command_power_more_than_20
		
		days_remove = 30
		days_re_enable = 0
		
		
		complete_effect = {
			set_country_flag = doing_operation
			add_command_power = -20
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove PMR_albania_operation: [From.GetName]"
			PMR = { country_event = { id = transnistria.1 days = 1 } }
			clr_country_flag = doing_operation
		}
	}
	PMR_russia_operation = {
		available = {
			NOT = {
				has_country_flag = doing_operation
			}
			has_capitulated = no
		}
		visible = {
			tag = PMR
			country_exists = SOV
			check_variable = { PMR.kolbasna > 700 }
			has_completed_focus = PMR_sell_weapon
		}
		ai_will_do = {
			factor = 80
		}

		icon = GFX_decision_pmr_cobasna_button

		custom_cost_trigger = {
			command_power > 50
		}
		custom_cost_text = command_power_more_than_50
		
		days_remove = 45
		days_re_enable = 0
		
		
		complete_effect = {
			set_country_flag = doing_operation
			add_command_power = -50
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove PMR_russia_operation: [From.GetName]"
			PMR = { country_event = { id = transnistria.3 days = 1 } }
			clr_country_flag = doing_operation
		}
	}
	PMR_france_operation = {
		available = {
			NOT = {
				has_country_flag = doing_operation
			}
			has_capitulated = no
		}
		visible = {
			tag = PMR
			country_exists = FRA
			check_variable = { PMR.kolbasna > 700 }
			has_completed_focus = PMR_sell_weapon
		}
		ai_will_do = {
			factor = 80
		}

		icon = GFX_decision_pmr_cobasna_button

		custom_cost_trigger = {
			command_power > 50
		}
		custom_cost_text = command_power_more_than_50
		
		days_remove = 45
		days_re_enable = 0
		
		
		complete_effect = {
			set_country_flag = doing_operation
			add_command_power = -50
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove PMR_france_operation: [From.GetName]"
			PMR = { country_event = { id = transnistria.3 days = 1 } }
			clr_country_flag = doing_operation
		}
	}
	PMR_luxemburg_operation = {
		available = {
			NOT = {
				has_country_flag = doing_operation
			}
			has_capitulated = no
		}
		visible = {
			tag = PMR
			country_exists = LUX
			check_variable = { PMR.kolbasna > 700 }
			has_completed_focus = PMR_sell_weapon
		}
		ai_will_do = {
			factor = 80
		}

		icon = GFX_decision_pmr_cobasna_button

		custom_cost_trigger = {
			command_power > 50
		}
		custom_cost_text = command_power_more_than_50
		
		days_remove = 45
		days_re_enable = 0
		
		
		complete_effect = {
			set_country_flag = doing_operation
			add_command_power = -50
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove PMR_luxemburg_operation: [From.GetName]"
			PMR = { country_event = { id = transnistria.3 days = 1 } }
			clr_country_flag = doing_operation
		}
	}
	PMR_belgium_operation = {
		available = {
			NOT = {
				has_country_flag = doing_operation
			}
			has_capitulated = no
		}
		visible = {
			tag = PMR
			country_exists = BEL
			check_variable = { PMR.kolbasna > 700 }
			has_completed_focus = PMR_sell_weapon
		}
		ai_will_do = {
			factor = 80
		}

		icon = GFX_decision_pmr_cobasna_button

		custom_cost_trigger = {
			command_power > 50
		}
		custom_cost_text = command_power_more_than_50
		
		days_remove = 45
		days_re_enable = 0
		
		
		complete_effect = {
			set_country_flag = doing_operation
			add_command_power = -50
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove PMR_belgium_operation: [From.GetName]"
			PMR = { country_event = { id = transnistria.3 days = 1 } }
			clr_country_flag = doing_operation
		}
	}
	PMR_netherlands_operation = {
		available = {
			NOT = {
				has_country_flag = doing_operation
			}
			has_capitulated = no
		}
		visible = {
			tag = PMR
			country_exists = HOL
			check_variable = { PMR.kolbasna > 700 }
			has_completed_focus = PMR_sell_weapon
		}
		ai_will_do = {
			factor = 80
		}

		icon = GFX_decision_pmr_cobasna_button

		custom_cost_trigger = {
			command_power > 50
		}
		custom_cost_text = command_power_more_than_50
		
		days_remove = 45
		days_re_enable = 0
		
		
		complete_effect = {
			set_country_flag = doing_operation
			add_command_power = -50
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove PMR_netherlands_operation: [From.GetName]"
			PMR = { country_event = { id = transnistria.3 days = 1 } }
			clr_country_flag = doing_operation
		}
	}
	PMR_germany_operation = {
		available = {
			NOT = {
				has_country_flag = doing_operation
			}
			has_capitulated = no
		}
		visible = {
			tag = PMR
			country_exists = GER
			check_variable = { PMR.kolbasna > 700 }
			has_completed_focus = PMR_sell_weapon
		}
		ai_will_do = {
			factor = 80
		}

		icon = GFX_decision_pmr_cobasna_button

		custom_cost_trigger = {
			command_power > 50
		}
		custom_cost_text = command_power_more_than_50
		
		days_remove = 45
		days_re_enable = 0
		
		
		complete_effect = {
			set_country_flag = doing_operation
			add_command_power = -50
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove PMR_germany_operation: [From.GetName]"
			PMR = { country_event = { id = transnistria.3 days = 1 } }
			clr_country_flag = doing_operation
		}
	}
	PMR_use_kobasna = {
		cost = 150
		visible = {
			OR = {
				original_tag = UKR
				original_tag = SOV
			}
			ROOT = { owns_state = 149 }
			check_variable = { PMR.kolbasna > 700 }
		}
		ai_will_do = {
			factor = 80
		}
		icon = GFX_decision_pmr_cobasna_button
		days_remove = 45
		days_re_enable = 0
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove PMR_use_kobasna"
			hidden_effect = {
				if = {
					limit = {
						check_variable = {
							PMR.kolbasna > 40000
						}
						check_variable = {
							PMR.kolbasna < 80000
						}
					}
					PMR = {
						set_temp_variable = { modify_kolbasna = -70000 }
						modify_kolbasna_support = yes
					}
					add_equipment_to_stockpile = {
						type = infantry_weapons
						amount = 50000
						producer = PMR
					}
				}
				else_if = {
					limit = {
						check_variable = {
							PMR.kolbasna > 20000
						}
						check_variable = {
							PMR.kolbasna < 40000
						}
					}
					PMR = {
						set_temp_variable = { modify_kolbasna = -30000 }
						modify_kolbasna_support = yes
					}
					add_equipment_to_stockpile = {
						type = infantry_weapons
						amount = 30000
						producer = PMR
					}
				}
				else_if = {
					limit = {
						check_variable = {
							PMR.kolbasna > 5000
						}
						check_variable = {
							PMR.kolbasna < 20000
						}
					}
					PMR = {
						set_temp_variable = { modify_kolbasna = -20000 }
						modify_kolbasna_support = yes
					}
					add_equipment_to_stockpile = {
						type = infantry_weapons
						amount = 10000
						producer = PMR
					}
				}
				else_if = {
					limit = {
						check_variable = {
							PMR.kolbasna > 700
						}
						check_variable = {
							PMR.kolbasna < 4900
						}
					}
					PMR = {
						set_temp_variable = { modify_kolbasna = -4000 }
						modify_kolbasna_support = yes
					}
					add_equipment_to_stockpile = {
						type = infantry_weapons
						amount = 3000
						producer = PMR
					}
				}
			}
		}
	}
}

PMR_russia_moldovian_way = {
	PMR_russia_way = {
		cost = 50
		visible = {
			country_exists = SOV
			SOV = { is_subject = no }
			is_subject = no
			NOT = {
				OR = {
					has_autonomy_state = autonomy_republic_rf
					has_autonomy_state = autonomy_moldovian_federation1
					has_completed_focus = PMR_federation
					has_completed_focus = PMR_referendum
					
				}
			}
		}
		ai_will_do = {
			factor = 0
			modifier = {
				has_global_flag = PMR_HISTORIC_DEFAULT_PATH
				add = 355
			}
			modifier = {
				has_global_flag = PMR_COMMUNISM_PATH
				add = 355
			}
			modifier = {
				has_global_flag = PMR_PATRIOTIC_PATH
				add = 355
			}
			modifier = {
				has_global_flag = PMR_SHERIFF_PATH
				add = 355
			}
		}
		icon = GFX_decision_pmr_sov_button
		days_remove = 200
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove PMR_russia_way"
			set_temp_variable = { modify_moldovans = -5 }
			modify_moldovans_support = yes
			set_temp_variable = { modify_russians = 5 }
			modify_russians_support = yes
			set_temp_variable = { percent_change = 0.5 }
			set_temp_variable = { tag_index = SOV }
			set_temp_variable = { influence_target = PMR }
			change_influence_percentage = yes
		}
	}
	PMR_russia_refendum = {
		cost = 50
		available = {
			custom_trigger_tooltip = {
				tooltip = PMR_russian_accept_80_tt
				check_variable = { PMR.russians > 79 }
			}
			OR = {
				is_subject = no
			}
			country_exists = SOV
		}
		ai_will_do = {
			factor = 0
			modifier = {
				has_global_flag = PMR_HISTORIC_DEFAULT_PATH
				add = 355
			}
			modifier = {
				has_global_flag = PMR_COMMUNISM_PATH
				add = 355
			}
			modifier = {
				has_global_flag = PMR_PATRIOTIC_PATH
				add = 355
			}
			modifier = {
				has_global_flag = PMR_SHERIFF_PATH
				add = 355
			}
		}
		icon = GFX_decision_pmr_sov_button
		days_remove = 600
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove PMR_russia_refendum"
			SOV = { country_event = transnistria.16 }
		}
	}
	PMR_moldovian_way = {
		cost = 50
		visible = {
			country_exists = MLV
			MLV = { is_subject = no }
			is_subject = no
			NOT = {
				OR = {
					has_autonomy_state = autonomy_republic_rf
					has_autonomy_state = autonomy_moldovian_federation1
					has_completed_focus = PMR_federation
					has_completed_focus = PMR_referendum
				}
			}
		}
		ai_will_do = {
			factor = 0
			modifier = {
				has_global_flag = PMR_LIBERAL_PATH
				add = 355
			}
		}
		icon = GFX_decision_pmr_mlv_button
		days_remove = 200
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove PMR_moldovian_way"
			set_temp_variable = { modify_moldovans = 5 }
			modify_moldovans_support = yes
			set_temp_variable = { modify_russians = -5 }
			modify_russians_support = yes
			set_temp_variable = { percent_change = 0.5 }
			set_temp_variable = { tag_index = MLV }
			set_temp_variable = { influence_target = PMR }
			change_influence_percentage = yes
		}
	}
	PMR_moldova_refendum = {
		cost = 50
		available = {
			custom_trigger_tooltip = {
				tooltip = PMR_moldovans_accept_80_tt
				check_variable = { PMR.moldovans > 79 }
			}
			OR = {
				PMR = {	is_subject_of = MLV	}
				is_subject = no
			}
			country_exists = MLV
		}
		ai_will_do = {
			factor = 0
			modifier = {
				has_global_flag = PMR_LIBERAL_PATH
				add = 355
			}
		}
		icon = GFX_decision_pmr_mlv_button
		days_remove = 600
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove PMR_moldova_refendum"
			MLV = { country_event = transnistria.17 }
		}
	}
}
PMR_sheriff_domination = {
	PMR_anti_sheriff = {
		cost = 45
		visible = {
			NOT = {
				has_completed_focus = PMR_sheriff_last
			}
		}
		ai_will_do = {
			factor = 1
			modifier = {
				has_global_flag = PMR_SHERIFF_PATH
				add = 355
			}
		}
		icon = GFX_decision_anti_sheriff_button
		days_remove = 100
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove PMR_anti_sheriff"
			set_temp_variable = { modify_sheriffs = -5 }
			modify_sheriffs_support = yes
		}
	}
}
PMR_kazaks_prepare = {
	PMR_kazak_kamensk = {
		custom_cost_trigger = {
			command_power > 20
		}
		custom_cost_text = command_power_more_than_20
		visible = {
			NOT = {
				has_completed_focus = PMR_kazaks
			}
		}
		ai_will_do = {
			factor = 355
		}
		fire_only_once = yes
		icon = GFX_decision_kazaki_button
		days_remove = 50
		complete_effect = {
			add_command_power = -20
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove PMR_kazak_kamensk"
			set_country_flag = kazak1
		}
	}
	PMR_kazak_ryibninsk = {
		custom_cost_trigger = {
			command_power > 20
		}
		custom_cost_text = command_power_more_than_20
		visible = {
			NOT = {
				has_completed_focus = PMR_kazaks
			}
		}
		ai_will_do = {
			factor = 355
		}
		fire_only_once = yes
		icon = GFX_decision_kazaki_button
		days_remove = 50
		complete_effect = {
			add_command_power = -20
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove PMR_kazak_ryibninsk"
			set_country_flag = kazak2
		}
	}
	PMR_kazak_dubossar = {
		custom_cost_trigger = {
			command_power > 20
		}
		custom_cost_text = command_power_more_than_20
		visible = {
			NOT = {
				has_completed_focus = PMR_kazaks
			}
		}
		ai_will_do = {
			factor = 355
		}
		fire_only_once = yes
		icon = GFX_decision_kazaki_button
		days_remove = 50
		complete_effect = {
			add_command_power = -20
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove PMR_kazak_dubossar"
			set_country_flag = kazak3
		}
	}
	PMR_kazak_slobodze = {
		custom_cost_trigger = {
			command_power > 20
		}
		custom_cost_text = command_power_more_than_20
		visible = {
			NOT = {
				has_completed_focus = PMR_kazaks
			}
		}
		ai_will_do = {
			factor = 355
		}
		fire_only_once = yes
		icon = GFX_decision_kazaki_button
		days_remove = 50
		complete_effect = {
			add_command_power = -20
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove PMR_kazak_slobodze"
			set_country_flag = kazak4
		}
	}
	PMR_kazak_benderi = {
		custom_cost_trigger = {
			command_power > 20
		}
		custom_cost_text = command_power_more_than_20
		visible = {
			NOT = {
				has_completed_focus = PMR_kazaks
			}
		}
		ai_will_do = {
			factor = 355
		}
		fire_only_once = yes
		icon = GFX_decision_kazaki_button
		days_remove = 50
		complete_effect = {
			add_command_power = -20
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove PMR_kazak_benderi"
			set_country_flag = kazak5
		}
	}
}
PMR_Federation_category = {
	PMR_annex = {
		icon = GFX_decision_pmr_federation_button
		days_remove = 300
		cost = 15
		visible = {
			original_tag = PMR
			OR = {
				GGZ = {
					has_autonomy_state = autonomy_moldovian_federation
				}
				OPR = {
					has_autonomy_state = autonomy_moldovian_federation
				}
			}
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision PMR_annex"
			if = {
				limit = { 
					GGZ = {
						has_autonomy_state = autonomy_moldovian_federation
						is_subject_of = PMR
					}
					country_exists = GGZ
				}
				set_temp_variable = { treasury_change = -2 }
				modify_treasury_effect = yes
				add_political_power = -150
				set_party_index_to_ruling_party = yes
				set_temp_variable = { party_popularity_increase = -0.08 }
				set_temp_variable = { temp_outlook_increase = -0.08 }
				add_relative_party_popularity = yes
				annex_country = { target = GGZ transfer_troops = yes }
				GGZ = { every_core_state = { add_core_of = PMR } }
				hidden_effect = {
					add_to_variable = { treasury = GGZ.treasury }
					add_to_variable = { debt = GGZ.debt }
					add_to_variable = { int_investments = GGZ.int_investments }
				}
			}	
			if = {
				limit = { 
					OPR = {
						has_autonomy_state = autonomy_moldovian_federation
						is_subject_of = PMR
					}
					country_exists = OPR
				}
				set_temp_variable = { treasury_change = -5 }
				modify_treasury_effect = yes
				add_political_power = -300
				set_party_index_to_ruling_party = yes
				set_temp_variable = { party_popularity_increase = -0.10 }
				set_temp_variable = { temp_outlook_increase = -0.10 }
				add_relative_party_popularity = yes
				annex_country = { target = OPR transfer_troops = yes }
				OPR = { every_core_state = { add_core_of = PMR } }
				hidden_effect = {
					add_to_variable = { treasury = OPR.treasury }
					add_to_variable = { debt = OPR.debt }
					add_to_variable = { int_investments = OPR.int_investments }
				}
			}	
		}
	}
	PMR_doctrines = {
		icon = GFX_decision_pmr_federation_button
		days_remove = 300
		cost = 35
		visible = {
			original_tag = PMR
			has_completed_focus = PMR_doctrine
			NOT = { has_completed_focus = PMR_pro_ukraine }
			OR = {
				GGZ = {
					has_autonomy_state = autonomy_moldovian_federation
				}
				OPR = {
					has_autonomy_state = autonomy_moldovian_federation
				}
			}
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision PMR_doctrines"
			every_other_country = {
				limit = {
					has_autonomy_state = autonomy_moldovian_federation
					NOT = { has_idea = PMR_doctrine_subject_romania }
					PMR = { has_idea = PMR_doctrine_romania }
				}
				add_ideas = PMR_doctrine_subject_romania
			}	
			every_other_country = {
				limit = {
					has_autonomy_state = autonomy_moldovian_federation
					NOT = { has_idea = PMR_subject_doctrine_ukraine }
					PMR = { has_idea = PMR_doctrine_ukraine }
				}
				add_ideas = PMR_subject_doctrine_ukraine
			}
		}
	}
}