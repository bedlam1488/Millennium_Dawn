BUL_rebellion_category = {

	BUL_start_probulgarian_propaganda = {
		icon = GFX_decision_bul_fyr_politic
		allowed = {
			original_tag = BUL
		}

		cost = 60

		days_remove = 30
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_start_probulgarian_propaganda"
			set_temp_variable = { percent_change = 2 }
			set_temp_variable = { tag_index = BUL }
			set_temp_variable = { influence_target = FYR }
			change_influence_percentage = yes
			country_event = bul_mech.7
			FYR = {
				country_event = bul_mech.7
				add_stability = -0.05
				add_opinion_modifier = {
					target = BUL
					modifier = drama
				}
				add_popularity = {
					ideology = nationalist
					popularity = 0.05
				}
				recalculate_party = yes
			}
		}
	}

	BUL_arrange_a_pro_bulgarian_protest = {
		icon = GFX_decision_bul_fyr_politic
		allowed = {
			original_tag = BUL
		}
		visible = {
			always = yes
		}
		cost = 110

		days_remove = 15
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_arrange_a_pro_bulgarian_protest"
			set_temp_variable = { percent_change = 5 }
			set_temp_variable = { tag_index = BUL }
			set_temp_variable = { influence_target = FYR }
			change_influence_percentage = yes
			country_event = bul_mech.8
			FYR = {
				country_event = bul_mech.8
				add_stability = -0.10
				add_opinion_modifier = {
					target = BUL
					modifier = drama
				}
				add_popularity = {
					ideology = nationalist
					popularity = 0.05
				}
			}
		}
	}

	BUL_send_weapons_to_separatists = {
		icon = GFX_decision_bul_fyr_politic
		allowed = {
			original_tag = BUL
		}
		visible = {
			always = yes
		}

		cost = 45

		days_remove = 30
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_send_weapons_to_separatists"
			set_temp_variable = { percent_change = 0.5 }
			set_temp_variable = { tag_index = BUL }
			set_temp_variable = { influence_target = FYR }
			change_influence_percentage = yes
			add_equipment_to_stockpile = {
				type = Inf_equipment
				amount = -1000
				producer = ROOT
			}
			country_event = bul_mech.9
			BUL = {
				add_war_support = 0.05
			}
			FYR = {
				add_stability = -0.05
				add_war_support = 0.05
				add_opinion_modifier = {
					target = BUL
					modifier = hostile_status
				}
			}
		}
	}

	BUL_friendship_diplomacy = {
		icon = GFX_decision_bul_fyr_politic
		allowed = {
			original_tag = BUL
		}

		cost = 25

		days_remove = 10
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_friendship_diplomacy"
			add_opinion_modifier = {
				target = FYR
				modifier = diplomatic_support
			}
			reverse_add_opinion_modifier = {
				target = FYR
				modifier = diplomatic_support
			}
			set_temp_variable = { percent_change = 0.5 }
			set_temp_variable = { tag_index = BUL }
			set_temp_variable = { influence_target = FYR }
			change_influence_percentage = yes
			country_event = bul_mech.10
			FYR = {
				country_event = bul_mech.10
			}
		}
	}

	BUL_invest_in_macedonia = {
		icon = GFX_decision_bul_fyr_politic
		allowed = {
			original_tag = BUL
		}

		cost = 10

		days_remove = 60
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_invest_in_macedonia"
			FYR = {
				country_event = bul_mech.11
			}
		}
	}

	BUL_purge_nationalists_fyr = {
		icon = GFX_decision_bul_fyr_politic
		allowed = {
			original_tag = BUL
		}

		cost = 75

		days_remove = 30
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_purge_nationalists_fyr"
			set_temp_variable = { percent_change = 5 }
			set_temp_variable = { tag_index = BUL }
			set_temp_variable = { influence_target = FYR }
			change_influence_percentage = yes
			add_popularity = {
				ideology = nationalist
				popularity = -0.05
			}
			recalculate_party = yes
			country_event = bul_mech.14
			FYR = {
				country_event = bul_mech.14
				add_popularity = {
					ideology = nationalist
					popularity = -0.05
				}
				recalculate_party = yes
			}
		}
	}

	BUL_start_antibulgarian_propaganda = {
		icon = GFX_decision_bul_fyr_politic
		allowed = {
			original_tag = FYR
		}

		cost = 75

		days_remove = 30
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_start_antibulgarian_propaganda"
			set_temp_variable = { percent_change = 2 }
			set_temp_variable = { tag_index = FYR }
			set_temp_variable = { influence_target = BUL }
			change_influence_percentage = yes
			country_event = bul_mech.20
			BUL = {
				country_event = bul_mech.20
				add_stability = -0.05
				add_opinion_modifier = {
					target = BUL
					modifier = drama
				}
				add_popularity = {
					ideology = nationalist
					popularity = 0.05
				}
				recalculate_party = yes
			}
		}
	}

	BUL_arrange_a_anti_bulgarian_protest = {
		icon = GFX_decision_bul_fyr_politic
		allowed = {
			original_tag = FYR
		}

		cost = 10

		days_remove = 15
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision 	BUL_arrange_a_anti_bulgarian_protest"
			set_temp_variable = {
				percent_change = 5
			}
			change_domestic_influence_percentage = yes
			change_influence_percentage = yes
			country_event = bul_mech.21
			BUL = {
				country_event = bul_mech.21
				add_stability = -0.10
				add_opinion_modifier = {
					target = BUL
					modifier = drama
				}
				add_popularity = {
					ideology = nationalist
					popularity = 0.05
				}
				recalculate_party = yes
			}
		}
	}

	BUL_meeting_with_bulgarian_officials = {
		icon = GFX_decision_bul_fyr_politic
		allowed = {
			OR = {
				original_tag = BUL
				original_tag = FYR
			}
		}

		cost = 100

		days_remove = 10
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_meeting_with_bulgarian_officials"
			ROOT = {
				country_event = bul_mech.16
			}
		}
	}

	BUL_conduct_training_on_border = {
		icon = GFX_decision_bul_fyr_politic
		allowed = {
			OR = {
				original_tag = BUL
				original_tag = FYR
			}
		}

		cost = 100

		days_remove = 30
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_conduct_training_on_border"
			country_event = bul_mech.17
		}
	}


}
BUL_Bolgarisation_category = {
	BUL_132_return = {
		icon = GFX_decision_bulgarisation
		allowed = { original_tag = BUL }
		cost = 70
		fire_only_once = yes
		days_remove = 120

		available = {
			owns_state = 132
		}

		visible = {
			tag = BUL
			owns_state = 132
		}
		remove_effect = {
			add_state_core = 132
			add_stability = -0.05
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_132_return"
		}
	}
	BUL_133_return = {
		icon = GFX_decision_bulgarisation
		allowed = { original_tag = BUL }
		cost = 80
		fire_only_once = yes
		days_remove = 180

		available = {
			owns_state = 133
		}

		visible = {
			tag = BUL
			owns_state = 133
		}
		remove_effect = {
			add_state_core = 133
			add_stability = -0.05
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_133_return"
		}
	}
	BUL_998_return = {
		icon = GFX_decision_bulgarisation
		allowed = { original_tag = BUL }
		cost = 70
		fire_only_once = yes
		days_remove = 120

		available = {
			owns_state = 998
		}

		visible = {
			tag = BUL
			owns_state = 998
		}
		remove_effect = {
			add_state_core = 998
			add_stability = -0.05
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_998_return"
		}
	}
	BUL_130_return = {
		icon = GFX_decision_bulgarisation
		allowed = { original_tag = BUL }
		cost = 70
		fire_only_once = yes
		days_remove = 120

		available = {
			owns_state = 130
		}

		visible = {
			tag = BUL
			owns_state = 130
		}
		remove_effect = {
			add_state_core = 130
			add_stability = -0.05
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_130_return"
		}
	}
	BUL_131_return = {
		icon = GFX_decision_bulgarisation
		allowed = { original_tag = BUL }
		cost = 70
		fire_only_once = yes
		days_remove = 120

		available = {
			owns_state = 131
		}

		visible = {
			tag = BUL
			owns_state = 131
		}
		remove_effect = {
			add_state_core = 131
			add_stability = -0.05
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_131_return"
		}
	}
	BUL_134_return = {
		icon = GFX_decision_bulgarisation
		allowed = { original_tag = BUL }
		cost = 70
		fire_only_once = yes
		days_remove = 120

		available = {
			owns_state = 134 
		}

		visible = {
			tag = BUL
			owns_state = 134
		}
		remove_effect = {
			add_state_core = 134
			add_stability = -0.05
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_134_return"
		}
	}
	BUL_934_return = {
		icon = GFX_decision_bulgarisation
		allowed = { original_tag = BUL }
		cost = 70
		fire_only_once = yes
		days_remove = 120

		available = {
			owns_state = 934
		}

		visible = {
			tag = BUL
			owns_state = 934
		}
		remove_effect = {
			add_state_core = 934
			add_stability = -0.05
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_934_return"
		}
	}
	BUL_137_return = {
		icon = GFX_decision_bulgarisation
		allowed = { original_tag = BUL }
		cost = 75
		fire_only_once = yes
		days_remove = 160

		available = {
			owns_state = 137
		}

		visible = {
			tag = BUL
			owns_state = 137
		}
		remove_effect = {
			add_state_core = 137
			add_stability = -0.05
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_137_return"
		}
	}
	BUL_138_return = {
		icon = GFX_decision_bulgarisation
		allowed = { original_tag = BUL }
		cost = 75
		fire_only_once = yes
		days_remove = 160

		available = {
			owns_state = 138 
		}

		visible = {
			tag = BUL
			owns_state = 138 
		}
		remove_effect = {
			
			add_state_core = 138
			add_stability = -0.05
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_138_return"
		}
	}
	BUL_974_return = {
		icon = GFX_decision_bulgarisation
		allowed = { original_tag = BUL }
		cost = 70
		fire_only_once = yes
		days_remove = 120

		available = {
			owns_state = 974 
		}

		visible = {
			tag = BUL
			owns_state = 974
		}
		remove_effect = {
			add_state_core = 974
			add_stability = -0.05
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_974_return"
		}
	}
	BUL_140_return = {
		icon = GFX_decision_bulgarisation
		allowed = { original_tag = BUL }
		cost = 75
		fire_only_once = yes
		days_remove = 140

		available = {
			owns_state = 140
		}

		visible = {
			tag = BUL
			owns_state = 140
		}
		remove_effect = {
			add_state_core = 140
			add_stability = -0.05
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_140_return"
		}
	}
	BUL_139_return = {
		icon = GFX_decision_bulgarisation
		allowed = { original_tag = BUL }
		cost = 75
		fire_only_once = yes
		days_remove = 140

		available = {
			owns_state = 139
		}

		visible = {
			tag = BUL
			owns_state = 139
		}
		remove_effect = {
			add_state_core = 139
			add_stability = -0.05
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_139_return"
		}
	}
	BUL_136_return = {
		icon = GFX_decision_bulgarisation
		allowed = { original_tag = BUL }
		cost = 70
		fire_only_once = yes
		days_remove = 120

		available = {
			owns_state = 136
		}

		visible = {
			tag = BUL
			owns_state = 136
		}
		remove_effect = {
			add_state_core = 136
			add_stability = -0.05
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_136_return"
		}
	}
	BUL_135_return = {
		icon = GFX_decision_bulgarisation
		allowed = { original_tag = BUL }
		cost = 70
		fire_only_once = yes
		days_remove = 120

		available = {
			owns_state = 135
		}

		visible = {
			tag = BUL
			owns_state = 135
		}
		remove_effect = {
			add_state_core = 135
			add_stability = -0.05
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_135_return"
		}
	}
}
BUL_Export_category = {
	BUL_Export_category = {
		icon = GFX_decision_decree8
		allowed = { original_tag = BUL }
		cost = 75
		days_remove = 300

		available = {
			BLR = {
				has_opinion = { target = BUL value > 19 }
			}
			BUL = {
				has_opinion = { target = BLR value > 19 }
			}
		}

		visible = {
			has_completed_focus = BUL_fighter_focus_3
		}
		remove_effect = {
			BUL = {
				country_event = { id = belarus_export.2	days = 1 }
			}
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_Export_categor"
		}
	}
}
###EXPORT WEAPON
BUL_Export_category = {
	BUL_geo_export = {
		icon = GFX_decision_arsenal_decision
		allowed = { original_tag = BUL }
		cost = 65
		days_remove = 300

		available = {
			has_country_flag = BUL_georgia_export_weapon_flag
		}

		visible = {
			tag = BUL
			has_completed_focus = BUL_expanding_arsenal
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_geo_export"
			GEO = {
				country_event = { id = bulg_export.1 days = 1 }
			}
		}
	}
	BUL_iraq_export = {
		icon = GFX_decision_arsenal_decision
		allowed = { original_tag = BUL }
		cost = 65
		days_remove = 300

		available = {
			has_country_flag = BUL_iraq_export_weapon_flag
		}

		visible = {
			tag = BUL
			has_completed_focus = BUL_expanding_arsenal
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_iraq_export"
			IRQ = {
				country_event = { id = bulg_export.1 days = 1 }
			}
		}
	}
	BUL_afgan_export = {
		icon = GFX_decision_arsenal_decision
		allowed = { original_tag = BUL }
		cost = 65
		days_remove = 300

		available = {
			has_country_flag = BUL_afgan_export_weapon_flag
		}

		visible = {
			tag = BUL
			has_completed_focus = BUL_expanding_arsenal
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_afgan_export"
			AFG = {
				country_event = { id = bulg_export.1 days = 1 }
			}
		}
	}
	BUL_india_export = {
		icon = GFX_decision_arsenal_decision
		allowed = { original_tag = BUL }
		cost = 75
		days_remove = 300

		available = {
			has_country_flag = BUL_india_export_weapon_flag
		}

		visible = {
			tag = BUL
			has_completed_focus = BUL_expanding_arsenal
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_india_export"
			RAJ = {
				country_event = { id = bulg_export.1 days = 1 }
			}
		}
	}
	BUL_ukraine_export = {
		icon = GFX_decision_arsenal_decision
		allowed = { original_tag = BUL }
		cost = 60
		days_remove = 300

		available = {
			has_country_flag = BUL_ukraine_export_weapon_flag
		}

		visible = {
			tag = BUL
			has_completed_focus = BUL_expanding_arsenal
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_ukraine_export"
			UKR = {
				country_event = { id = bulg_export.1 days = 1 }
			}
		}
	}
	BUL_azer_export = {
		icon = GFX_decision_arsenal_decision
		allowed = { original_tag = BUL }
		cost = 60
		days_remove = 300

		available = {
			has_country_flag = BUL_azer_export_weapon_flag
		}

		visible = {
			tag = BUL
			has_completed_focus = BUL_expanding_arsenal
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_azer_export"
			AZE = {
				country_event = { id = bulg_export.1 days = 1 }
			}
		}
	}
	BUL_arm_export = {
		icon = GFX_decision_arsenal_decision
		allowed = { original_tag = BUL }
		cost = 60
		days_remove = 300

		available = {
			has_country_flag = BUL_arm_export_weapon_flag
		}

		visible = {
			tag = BUL
			has_completed_focus = BUL_expanding_arsenal
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_arm_export"
			ARM = {
				country_event = { id = bulg_export.1 days = 1 }
			}
		}
	}
	BUL_serb_export = {
		icon = GFX_decision_arsenal_decision
		allowed = { original_tag = BUL }
		cost = 60
		days_remove = 300

		available = {
			has_country_flag = BUL_serb_export_weapon_flag
		}

		visible = {
			tag = BUL
			has_completed_focus = BUL_expanding_arsenal
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_serb_export"
			SER = {
				country_event = { id = bulg_export.1 days = 1 }
			}
		}
	}
	BUL_indonesia_export = {
		icon = GFX_decision_arsenal_decision
		allowed = { original_tag = BUL }
		cost = 60
		days_remove = 300

		available = {
			has_country_flag = BUL_indo_export_weapon_flag
		}

		visible = {
			tag = BUL
			has_completed_focus = BUL_expanding_arsenal
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_indonesia_export"
			IND = {
				country_event = { id = bulg_export.1 days = 1 }
			}
		}
	}
	BUL_mac_dac_export = {
		icon = GFX_decision_arsenal_decision
		allowed = { original_tag = BUL }
		cost = 60
		days_remove = 300

		available = {
			has_country_flag = BUL_mac_export_weapon_flag
		}

		visible = {
			tag = BUL
			has_completed_focus = BUL_expanding_arsenal
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BUL_mac_dac_export"
			FYR = {
				country_event = { id = bulg_export.1 days = 1 }
			}
		}
	}
}
