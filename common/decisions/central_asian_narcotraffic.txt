central_asian_narcotraffic = {
	KAZ_central_asian_narcotraffic_1 = {

		icon = GFX_decision_drug_prohibition

		cost = 100

		allowed = {
			tag = KAZ
		}

		fire_only_once = yes

		available = {
			has_war = no
			custom_trigger_tooltip = {
				tooltip = KAZ_central_asian_border_less_than_three_fort_tt
				718 = {
					any_province_building_level = {
						province = {
							id = 1517
							id = 14373
							id = 14347
						}
						building = bunker
						level > 0
					}
				}
				717 = {
					any_province_building_level = {
						province = {
							id = 12640
							id = 10603
							id = 4773
						}
						building = bunker
						level > 0
					}
				}
				719 = {
					any_province_building_level = {
						province = {
							id = 10146
							id = 13210
							id = 1840
						}
						building = bunker
						level > 0
					}
				}
			}
		}

		remove_effect = {
			swap_ideas = {
				remove_idea = KAZ_narcotraffic_1
				add_idea = KAZ_narcotraffic_2
			}
			set_country_flag = central_asian_narcotraffic_1
		}

		days_remove = 180
	}
	UZB_central_asian_narcotraffic_1 = {

		icon = GFX_decision_drug_prohibition

		cost = 100

		allowed = {
			tag = UZB
		}

		fire_only_once = yes

		available = {
			has_war = no
			custom_trigger_tooltip = {
				tooltip = UZB_central_asian_border_less_than_three_fort_tt
				726 = {
					any_province_building_level = {
						province = {
							id = 12239
							id = 10205
							id = 1216
							id = 10671
							id = 4629
							id = 3007
							id = 11687
						}
						building = bunker
						level > 0
					}
				}
				725 = {
					any_province_building_level = {
						province = {
							id = 1459
							id = 10488
							id = 10530
							id = 2058
						}
						building = bunker
						level > 0
					}
				}
			}
		}

		remove_effect = {
			swap_ideas = {
				remove_idea = UZB_narcotraffic_1
				add_idea = UZB_narcotraffic_2
			}
			set_country_flag = central_asian_narcotraffic_1
		}
	}
	KYR_central_asian_narcotraffic_1 = {

		icon = GFX_decision_drug_prohibition

		cost = 100

		allowed = {
			tag = KYR
		}

		fire_only_once = yes

		available = {
			has_war = no
			custom_trigger_tooltip = {
				tooltip = KYR_central_asian_border_less_than_three_fort_tt
				721 = {
					any_province_building_level = {
						province = {
							id = 12581
							id = 14625
							id = 14132
							id = 13214
						}
						building = bunker
						level > 0
					}
				}
			}
		}

		remove_effect = {
			swap_ideas = {
				remove_idea = KYR_narcotraffic_1
				add_idea = KYR_narcotraffic_2
			}
			set_country_flag = central_asian_narcotraffic_1
		}
	}
	TAJ_central_asian_narcotraffic_1 = {

		icon = GFX_decision_drug_prohibition

		cost = 100

		allowed = {
			tag = TAJ
		}

		fire_only_once = yes

		available = {
			has_war = no
			custom_trigger_tooltip = {
				tooltip = TAJ_central_asian_border_less_than_three_fort_tt
				722 = {
					any_province_building_level = {
						province = {
							id = 12805
							id = 10417
							id = 1307
							id = 14130
						}
						building = bunker
						level > 0
					}
				}
				724 = {
					any_province_building_level = {
						province = {
							id = 7224
							id = 12218
							id = 14131
						}
						building = bunker
						level > 0
					}
				}
			}
		}

		remove_effect = {
			swap_ideas = {
				remove_idea = TAJ_narcotraffic_1
				add_idea = TAJ_narcotraffic_2
			}
			set_country_flag = central_asian_narcotraffic_1
		}
	}
#################2222222
	KAZ_central_asian_narcotraffic_2 = {

		icon = GFX_decision_drug_prohibition

		cost = 100

		visible = {
			has_country_flag = central_asian_narcotraffic_1
		}

		allowed = {
			tag = KAZ
		}

		fire_only_once = yes

		available = {
			has_war = no
			custom_trigger_tooltip = {
				tooltip = KAZ_central_asian_border_less_than_three_fort_tt_2
				718 = {
					any_province_building_level = {
						province = {
							id = 1517
							id = 14373
							id = 14347
						}
						building = bunker
						level > 1
					}
				}
				717 = {
					any_province_building_level = {
						province = {
							id = 12640
							id = 10603
							id = 4773
						}
						building = bunker
						level > 1
					}
				}
				719 = {
					any_province_building_level = {
						province = {
							id = 10146
							id = 13210
							id = 1840
						}
						building = bunker
						level > 1
					}
				}
			}
		}

		remove_effect = {
			swap_ideas = {
				remove_idea = KAZ_narcotraffic_2
				add_idea = KAZ_narcotraffic_3
			}
			increase_policing_budget = yes
			set_country_flag = central_asian_narcotraffic_2
		}

		days_remove = 180
	}
	UZB_central_asian_narcotraffic_2 = {

		icon = GFX_decision_drug_prohibition

		cost = 100

		visible = {
			has_country_flag = central_asian_narcotraffic_1
		}

		allowed = {
			tag = UZB
		}

		fire_only_once = yes

		available = {
			has_war = no
			custom_trigger_tooltip = {
				tooltip = UZB_central_asian_border_less_than_three_fort_tt_2
				726 = {
					any_province_building_level = {
						province = {
							id = 12239
							id = 10205
							id = 1216
							id = 10671
							id = 4629
							id = 3007
							id = 11687
						}
						building = bunker
						level > 1
					}
				}
				725 = {
					any_province_building_level = {
						province = {
							id = 1459
							id = 10488
							id = 10530
							id = 2058
						}
						building = bunker
						level > 1
					}
				}
			}
		}

		remove_effect = {
			swap_ideas = {
				remove_idea = UZB_narcotraffic_2
				add_idea = UZB_narcotraffic_3
			}
			increase_policing_budget = yes
			set_country_flag = central_asian_narcotraffic_2
		}
	}
	KYR_central_asian_narcotraffic_2 = {

		icon = GFX_decision_drug_prohibition

		cost = 100

		visible = {
			has_country_flag = central_asian_narcotraffic_1
		}

		allowed = {
			tag = KYR
		}

		fire_only_once = yes

		available = {
			has_war = no
			custom_trigger_tooltip = {
				tooltip = KYR_central_asian_border_less_than_three_fort_tt_2
				721 = {
					any_province_building_level = {
						province = {
							id = 12581
							id = 14625
							id = 14132
							id = 13214
						}
						building = bunker
						level > 1
					}
				}
			}
		}

		remove_effect = {
			swap_ideas = {
				remove_idea = KYR_narcotraffic_2
				add_idea = KYR_narcotraffic_3
			}
			increase_policing_budget = yes
			set_country_flag = central_asian_narcotraffic_2
		}
	}
	TAJ_central_asian_narcotraffic_2 = {

		icon = GFX_decision_drug_prohibition

		cost = 100

		visible = {
			has_country_flag = central_asian_narcotraffic_1
		}

		allowed = {
			tag = TAJ
		}

		fire_only_once = yes

		available = {
			has_war = no
			custom_trigger_tooltip = {
				tooltip = TAJ_central_asian_border_less_than_three_fort_tt_2
				722 = {
					any_province_building_level = {
						province = {
							id = 12805
							id = 10417
							id = 1307
							id = 14130
						}
						building = bunker
						level > 1
					}
				}
				724 = {
					any_province_building_level = {
						province = {
							id = 7224
							id = 12218
							id = 14131
						}
						building = bunker
						level > 1
					}
				}
			}
		}

		remove_effect = {
			swap_ideas = {
				remove_idea = TAJ_narcotraffic_2
				add_idea = TAJ_narcotraffic_3
			}
			increase_policing_budget = yes
			set_country_flag = central_asian_narcotraffic_2
		}
	}
#################33333333
	KAZ_central_asian_narcotraffic_3 = {

		icon = GFX_decision_drug_prohibition

		cost = 100

		visible = {
			has_country_flag = central_asian_narcotraffic_2
		}

		allowed = {
			tag = KAZ
		}

		fire_only_once = yes

		available = {
			has_war = no
			custom_trigger_tooltip = {
				tooltip = KAZ_central_asian_border_less_than_three_fort_tt_3
				718 = {
					any_province_building_level = {
						province = {
							id = 1517
							id = 14373
							id = 14347
						}
						building = bunker
						level > 2
					}
				}
				717 = {
					any_province_building_level = {
						province = {
							id = 12640
							id = 10603
							id = 4773
						}
						building = bunker
						level > 2
					}
				}
				719 = {
					any_province_building_level = {
						province = {
							id = 10146
							id = 13210
							id = 1840
						}
						building = bunker
						level > 2
					}
				}
			}
		}

		remove_effect = {
			random_list = {
				50 = {
					country_event = { id = drugs.1 }
				}
				50 = {
					country_event = { id = drugs.2 }
				}
			}
		}

		days_remove = 180
	}
	UZB_central_asian_narcotraffic_3 = {

		icon = GFX_decision_drug_prohibition

		cost = 100

		visible = {
			has_country_flag = central_asian_narcotraffic_2
		}

		allowed = {
			tag = UZB
		}

		fire_only_once = yes

		available = {
			has_war = no
			custom_trigger_tooltip = {
				tooltip = UZB_central_asian_border_less_than_three_fort_tt_3
				726 = {
					any_province_building_level = {
						province = {
							id = 12239
							id = 10205
							id = 1216
							id = 10671
							id = 4629
							id = 3007
							id = 11687
						}
						building = bunker
						level > 2
					}
				}
				725 = {
					any_province_building_level = {
						province = {
							id = 1459
							id = 10488
							id = 10530
							id = 2058
						}
						building = bunker
						level > 2
					}
				}
			}
		}

		remove_effect = {
			random_list = {
				50 = {
					country_event = { id = drugs.1 }
				}
				50 = {
					country_event = { id = drugs.2 }
				}
			}
		}
	}
	KYR_central_asian_narcotraffic_3 = {

		icon = GFX_decision_drug_prohibition

		cost = 100

		visible = {
			has_country_flag = central_asian_narcotraffic_2
		}

		allowed = {
			tag = KYR
		}

		fire_only_once = yes

		available = {
			has_war = no
			custom_trigger_tooltip = {
				tooltip = KYR_central_asian_border_less_than_three_fort_tt_3
				721 = {
					any_province_building_level = {
						province = {
							id = 12581
							id = 14625
							id = 14132
							id = 13214
						}
						building = bunker
						level > 2
					}
				}
			}
		}

		remove_effect = {
			random_list = {
				50 = {
					country_event = { id = drugs.1 }
				}
				50 = {
					country_event = { id = drugs.2 }
				}
			}
		}
	}
	TAJ_central_asian_narcotraffic_3 = {

		icon = GFX_decision_drug_prohibition

		cost = 100

		visible = {
			has_country_flag = central_asian_narcotraffic_2
		}

		allowed = {
			tag = TAJ
		}

		fire_only_once = yes

		available = {
			has_war = no
			custom_trigger_tooltip = {
				tooltip = TAJ_central_asian_border_less_than_three_fort_tt_3
				722 = {
					any_province_building_level = {
						province = {
							id = 12805
							id = 10417
							id = 1307
							id = 14130
						}
						building = bunker
						level > 2
					}
				}
				724 = {
					any_province_building_level = {
						province = {
							id = 7224
							id = 12218
							id = 14131
						}
						building = bunker
						level > 2
					}
				}
			}
		}

		remove_effect = {
			random_list = {
				50 = {
					country_event = { id = drugs.1 }
				}
				50 = {
					country_event = { id = drugs.2 }
				}
			}
		}
	}
#################4444444
	KAZ_central_asian_narcotraffic_4 = {

		icon = GFX_decision_drug_prohibition

		cost = 100

		visible = {
			has_country_flag = no_narcotraffic
		}

		allowed = {
			tag = KAZ
		}

		fire_only_once = yes

		available = {
			has_war = no
			custom_trigger_tooltip = {
				tooltip = KAZ_central_asian_border_less_than_three_fort_tt_3
				718 = {
					any_province_building_level = {
						province = {
							id = 1517
							id = 14373
							id = 14347
						}
						building = bunker
						level > 2
					}
				}
				717 = {
					any_province_building_level = {
						province = {
							id = 12640
							id = 10603
							id = 4773
						}
						building = bunker
						level > 2
					}
				}
				719 = {
					any_province_building_level = {
						province = {
							id = 10146
							id = 13210
							id = 1840
						}
						building = bunker
						level > 2
					}
				}
			}
		}

		remove_effect = {
			random_list = {
				50 = {
					country_event = { id = drugs.1 }
				}
				50 = {
					country_event = { id = drugs.3 }
				}
			}
		}

		days_remove = 180
	}
	UZB_central_asian_narcotraffic_4 = {

		icon = GFX_decision_drug_prohibition

		cost = 100

		visible = {
			has_country_flag = no_narcotraffic
		}

		allowed = {
			tag = UZB
		}

		fire_only_once = yes

		available = {
			has_war = no
			custom_trigger_tooltip = {
				tooltip = UZB_central_asian_border_less_than_three_fort_tt_3
				726 = {
					any_province_building_level = {
						province = {
							id = 12239
							id = 10205
							id = 1216
							id = 10671
							id = 4629
							id = 3007
							id = 11687
						}
						building = bunker
						level > 2
					}
				}
				725 = {
					any_province_building_level = {
						province = {
							id = 1459
							id = 10488
							id = 10530
							id = 2058
						}
						building = bunker
						level > 2
					}
				}
			}
		}

		remove_effect = {
			random_list = {
				50 = {
					country_event = { id = drugs.1 }
				}
				50 = {
					country_event = { id = drugs.3 }
				}
			}
		}
	}
	KYR_central_asian_narcotraffic_4 = {

		icon = GFX_decision_drug_prohibition

		cost = 100

		visible = {
			has_country_flag = no_narcotraffic
		}

		allowed = {
			tag = KYR
		}

		fire_only_once = yes

		available = {
			has_war = no
			custom_trigger_tooltip = {
				tooltip = KYR_central_asian_border_less_than_three_fort_tt_3
				721 = {
					any_province_building_level = {
						province = {
							id = 12581
							id = 14625
							id = 14132
							id = 13214
						}
						building = bunker
						level > 2
					}
				}
			}
		}

		remove_effect = {
			random_list = {
				50 = {
					country_event = { id = drugs.1 }
				}
				50 = {
					country_event = { id = drugs.3 }
				}
			}
		}
	}
	TAJ_central_asian_narcotraffic_4 = {

		icon = GFX_decision_drug_prohibition

		cost = 100

		visible = {
			has_country_flag = no_narcotraffic
		}

		allowed = {
			tag = TAJ
		}

		fire_only_once = yes

		available = {
			has_war = no
			custom_trigger_tooltip = {
				tooltip = TAJ_central_asian_border_less_than_three_fort_tt_3
				722 = {
					any_province_building_level = {
						province = {
							id = 12805
							id = 10417
							id = 1307
							id = 14130
						}
						building = bunker
						level > 2
					}
				}
				724 = {
					any_province_building_level = {
						province = {
							id = 7224
							id = 12218
							id = 14131
						}
						building = bunker
						level > 2
					}
				}
			}
		}

		remove_effect = {
			random_list = {
				50 = {
					country_event = { id = drugs.1 }
				}
				50 = {
					country_event = { id = drugs.3 }
				}
			}
		}
	}
	#################5555555
	KAZ_central_asian_narcotraffic_5 = {

		icon = GFX_decision_drug_prohibition

		cost = 100

		visible = {
			has_country_flag = no_narcotraffic_2
		}

		allowed = {
			tag = KAZ
		}

		fire_only_once = yes

		available = {
			has_war = no
			custom_trigger_tooltip = {
				tooltip = KAZ_central_asian_border_less_than_three_fort_tt_3
				718 = {
					any_province_building_level = {
						province = {
							id = 1517
							id = 14373
							id = 14347
						}
						building = bunker
						level > 2
					}
				}
				717 = {
					any_province_building_level = {
						province = {
							id = 12640
							id = 10603
							id = 4773
						}
						building = bunker
						level > 2
					}
				}
				719 = {
					any_province_building_level = {
						province = {
							id = 10146
							id = 13210
							id = 1840
						}
						building = bunker
						level > 2
					}
				}
			}
		}

		remove_effect = {
			country_event = { id = drugs.1 }
		}

		days_remove = 180
	}
	UZB_central_asian_narcotraffic_5 = {

		icon = GFX_decision_drug_prohibition

		cost = 100

		visible = {
			has_country_flag = no_narcotraffic_2
		}

		allowed = {
			tag = UZB
		}

		fire_only_once = yes

		available = {
			has_war = no
			custom_trigger_tooltip = {
				tooltip = UZB_central_asian_border_less_than_three_fort_tt_3
				726 = {
					any_province_building_level = {
						province = {
							id = 12239
							id = 10205
							id = 1216
							id = 10671
							id = 4629
							id = 3007
							id = 11687
						}
						building = bunker
						level > 2
					}
				}
				725 = {
					any_province_building_level = {
						province = {
							id = 1459
							id = 10488
							id = 10530
							id = 2058
						}
						building = bunker
						level > 2
					}
				}
			}
		}

		remove_effect = {
			country_event = { id = drugs.1 }
		}

		days_remove = 180
	}
	KYR_central_asian_narcotraffic_5 = {

		icon = GFX_decision_drug_prohibition

		cost = 100

		visible = {
			has_country_flag = no_narcotraffic_2
		}

		allowed = {
			tag = KYR
		}

		fire_only_once = yes

		available = {
			has_war = no
			custom_trigger_tooltip = {
				tooltip = KYR_central_asian_border_less_than_three_fort_tt_3
				721 = {
					any_province_building_level = {
						province = {
							id = 12581
							id = 14625
							id = 14132
							id = 13214
						}
						building = bunker
						level > 2
					}
				}
			}
		}

		remove_effect = {
			country_event = { id = drugs.1 }
		}

		days_remove = 180
	}
	TAJ_central_asian_narcotraffic_5 = {

		icon = GFX_decision_drug_prohibition

		cost = 100

		visible = {
			has_country_flag = no_narcotraffic_2
		}

		allowed = {
			tag = TAJ
		}

		fire_only_once = yes

		available = {
			has_war = no
			custom_trigger_tooltip = {
				tooltip = TAJ_central_asian_border_less_than_three_fort_tt_3
				722 = {
					any_province_building_level = {
						province = {
							id = 12805
							id = 10417
							id = 1307
							id = 14130
						}
						building = bunker
						level > 2
					}
				}
				724 = {
					any_province_building_level = {
						province = {
							id = 7224
							id = 12218
							id = 14131
						}
						building = bunker
						level > 2
					}
				}
			}
		}

		remove_effect = {
			country_event = { id = drugs.1 }
		}

		days_remove = 180
	}
	#################6666666
	KAZ_central_asian_narcotraffic_6 = {

		icon = GFX_decision_drug_prohibition

		cost = 300

		visible = {
			has_country_flag = drug_dealers_found
		}

		allowed = {
			tag = KAZ
		}

		fire_only_once = yes

		available = {
			has_war = no
			custom_trigger_tooltip = {
				tooltip = KAZ_central_asian_border_less_than_three_fort_tt_3
				718 = {
					any_province_building_level = {
						province = {
							id = 1517
							id = 14373
							id = 14347
						}
						building = bunker
						level > 2
					}
				}
				717 = {
					any_province_building_level = {
						province = {
							id = 12640
							id = 10603
							id = 4773
						}
						building = bunker
						level > 2
					}
				}
				719 = {
					any_province_building_level = {
						province = {
							id = 10146
							id = 13210
							id = 1840
						}
						building = bunker
						level > 2
					}
				}
			}
		}

		remove_effect = {
			country_event = { id = drugs.4 }
		}

		days_remove = 180
	}
	UZB_central_asian_narcotraffic_6 = {

		icon = GFX_decision_drug_prohibition

		cost = 300

		visible = {
			has_country_flag = drug_dealers_found
		}

		allowed = {
			tag = UZB
		}

		fire_only_once = yes

		available = {
			has_war = no
			custom_trigger_tooltip = {
				tooltip = UZB_central_asian_border_less_than_three_fort_tt_3
				726 = {
					any_province_building_level = {
						province = {
							id = 12239
							id = 10205
							id = 1216
							id = 10671
							id = 4629
							id = 3007
							id = 11687
						}
						building = bunker
						level > 2
					}
				}
				725 = {
					any_province_building_level = {
						province = {
							id = 1459
							id = 10488
							id = 10530
							id = 2058
						}
						building = bunker
						level > 2
					}
				}
			}
		}

		remove_effect = {
			country_event = { id = drugs.4 }
		}

		days_remove = 180
	}
	KYR_central_asian_narcotraffic_6 = {

		icon = GFX_decision_drug_prohibition

		cost = 300

		visible = {
			has_country_flag = drug_dealers_found
		}

		allowed = {
			tag = KYR
		}

		fire_only_once = yes

		available = {
			has_war = no
			custom_trigger_tooltip = {
				tooltip = KYR_central_asian_border_less_than_three_fort_tt_3
				721 = {
					any_province_building_level = {
						province = {
							id = 12581
							id = 14625
							id = 14132
							id = 13214
						}
						building = bunker
						level > 2
					}
				}
			}
		}

		remove_effect = {
			country_event = { id = drugs.4 }
		}

		days_remove = 180
	}
	TAJ_central_asian_narcotraffic_6 = {

		icon = GFX_decision_drug_prohibition

		cost = 300

		visible = {
			has_country_flag = drug_dealers_found
		}

		allowed = {
			tag = TAJ
		}

		fire_only_once = yes

		available = {
			has_war = no
			custom_trigger_tooltip = {
				tooltip = TAJ_central_asian_border_less_than_three_fort_tt_3
				722 = {
					any_province_building_level = {
						province = {
							id = 12805
							id = 10417
							id = 1307
							id = 14130
						}
						building = bunker
						level > 2
					}
				}
				724 = {
					any_province_building_level = {
						province = {
							id = 7224
							id = 12218
							id = 14131
						}
						building = bunker
						level > 2
					}
				}
			}
		}

		remove_effect = {
			country_event = { id = drugs.4 }
		}

		days_remove = 180
	}
}