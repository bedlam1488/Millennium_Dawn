central_asian_narcotraffic = {
	visible = {
		OR = {
			has_idea = KAZ_narcotraffic_1
			has_idea = UZB_narcotraffic_1
			has_idea = KYR_narcotraffic_1
			has_idea = TAJ_narcotraffic_1
			has_idea = KAZ_narcotraffic_2
			has_idea = UZB_narcotraffic_2
			has_idea = KYR_narcotraffic_2
			has_idea = TAJ_narcotraffic_2
			has_idea = KAZ_narcotraffic_3
			has_idea = UZB_narcotraffic_3
			has_idea = KYR_narcotraffic_3
			has_idea = TAJ_narcotraffic_3
			has_idea = CES_controlled_narcotraffic_idea
		}
	}
	allowed = {
		OR = {
			original_tag = UZB
			original_tag = KYR
			original_tag = KAZ
		}
	}
	priority = 151
	picture = GFX_decision_drug_smuggling
	icon = GFX_decision_opium
}
TRK_uzbek_category = {
	icon = GFX_decision_antiuzbek
	picture = GFX_decision_uzbek_turkmen_conflict
	priority = 150
	allowed = {
		original_tag = UZB
	}

	visible = {
		has_completed_focus = CES_our_state_kaz
		country_exists = UZB
	}
}
UZB_karapalkstan_category = {
	icon = GFX_decision_karalp_decis
	picture = GFX_decision_decis_karalpak
	priority = 200
	allowed = {
		original_tag = UZB
	}

	visible = {
		UZB = {
			owns_state = 727
		}
		country_exists = UZB
	}
}