CUB_blockade_fight_category = {
icon = GFX_decisions_category_blockade
		priority = 220
		allowed = {
			original_tag = CUB
		}
		visible = {
			country_exists = CUB
			has_idea = CUB_idea_blockade1
			country_exists = USA
	}
}
CUB_ter_defence_category = {
	icon = GFX_CUB_train1_cat
		priority = 110
		allowed = {
			original_tag = CUB
		}
		visible = {
			has_completed_focus = CUB_create_ter_defense
		}
}
CUB_alba_category = {
	icon = GFX_decisioncategoryalbacub
		priority = 150
		allowed = {
			original_tag = CUB
		}
		visible = {
			has_idea = CUB_alba_idea
		}
}
CUB_confederation_category = {
	scripted_gui = cuba_gui
	icon = GFX_decisions_category_antillean
		priority = 150
		allowed = {
			original_tag = CUB
		}
		visible = {
			has_completed_focus = CUB_first_stage_of_federation
		}
}
#CUB_blockade_america_category = {
	 #  priority = 220
	   #allowed = {
		#   original_tag = USA
	   #}
	   #visible = {
		#has_country_flag = blockade_cuba_yes
		#has_completed_focus = CUB_demand_from_us
  # }
#}