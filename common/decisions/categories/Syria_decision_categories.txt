Syria_decisions = {

	priority = 100

	allowed = {
		OR = {
			original_tag = SYR
			original_tag = NUS
			original_tag = FSA
			original_tag = ALA
			original_tag = ISI
			original_tag = DRU
			original_tag = ROJ
		}
	}

}

Syria_foreign_policy = {

	priority = 100

	allowed = {
		OR = {
			original_tag = SYR
			original_tag = FSA
			original_tag = NUS
		}
	}

}

Syria_external_countries = {

	priority = 100

	allowed = {
		always = yes
	}

}

Syria_hafez_succession = {

	priority = 100

	allowed = {
		original_tag = SYR
	}

	visible = {
		NOT = { has_country_flag = Syria_successor_chosen }
		NOT = { has_start_date > 2016.1.1 }
	}

}

Syria_greater_syria_decisions = {

	priority = 100

	allowed = {
		OR = {
			original_tag = SYR
			original_tag = FSA
			original_tag = NUS
		}
	}

	visible = {
		has_completed_focus = SYR_greater_syria
		has_government = nationalist
	}
}