libya_oil_nationalisation = {

	priority = 130
	
	icon = generic_prospect_for_resources
	
	allowed = {
		OR = {
			original_tag = LBA
			original_tag = CYR
			original_tag = FEZ
			original_tag = TRP
		}
	}
	
}

libya_gaddafi_family = {

	scripted_gui = libya_gaddafi_family_gui

	priority = 150
	
	icon = power_struggle

	visible_when_empty = yes
	
	allowed = {
		original_tag = LBA
	}
	
	visible = {
		NOT = { has_country_flag = LBA_fall_of_gaddafi }
		is_ruled_by_a_gaddafi = yes
	}
	
}

libya_libyan_tribalism = {

	scripted_gui = libya_libyan_tribalism_gui

	priority = 140

	icon = anti_colonial

	allowed = {
		original_tag = LBA
	}

	visible = {
		NOT = { has_country_flag = LBA_tribalism_disabled }
	}

}

libya_libyan_foreign_policy = {

	priority = 130
	
	icon = generic_propaganda
	
	allowed = {
		original_tag = LBA
	}
	
	visible = {
	}
	
}