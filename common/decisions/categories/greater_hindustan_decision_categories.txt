RAJ_greater_hindustan_decisions_category = {
	icon = GFX_decision_zsr_unrest
	priority = 150
	picture = GFX_decision_greater_hindustan
	allowed = {
		original_tag = RAJ
	}
	visible = {
		has_completed_focus = RAJ_greater_india
		has_government = nationalist
	}
}