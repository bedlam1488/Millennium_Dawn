################
##### Decisions for quick help ######
################


influence_desicions = {

	icon = GFX_decisions_category_influence

	allowed = {
		always = yes
	}
	visible = {
		ERI_is_transitional_government_DIPLO = yes
	}
	priority = 20
}
