migration_central_asia = {
	visible = {
		OR = {
			has_idea = KAZ_high_emigration_5
			has_idea = KAZ_high_emigration_4
			has_idea = KAZ_high_emigration_3
			has_idea = KAZ_high_emigration_2
			has_idea = KAZ_high_emigration_1
			has_idea = UZB_high_emigration_5
			has_idea = UZB_high_emigration_4
			has_idea = UZB_high_emigration_3
			has_idea = UZB_high_emigration_2
			has_idea = UZB_high_emigration_1
			has_idea = KYR_high_emigration_5
			has_idea = KYR_high_emigration_4
			has_idea = KYR_high_emigration_3
			has_idea = KYR_high_emigration_2
			has_idea = KYR_high_emigration_1
		}
	}
	priority = 152
	picture = GFX_decision_decis_centralas_migr
	icon = GFX_decision_migration_central_asia
}