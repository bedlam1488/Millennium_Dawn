kasmir_war_decisions_category = {
	icon = GFX_decision_category_india
	picture = GFX_indian_conflict
	priority = 100

	allowed = {
		OR = {
			original_tag = RAJ
			original_tag = PAK
		}
	}

	visible = {
		country_exists = RAJ
		country_exists = PAK
		NOT = {
			OR = {
				PAK = { is_subject_of = RAJ }
				RAJ = { is_subject_of = PAK }
				RAJ = { is_in_faction_with = PAK }
			}
		}
	}
}

kasmir_war_china_decisions_category = {
	icon = GFX_decision_category_india
	picture = GFX_indian_conflict
	priority = 100

	allowed = {
		OR = {
			original_tag = RAJ
			original_tag = CHI
		}
	}

	visible = {
		country_exists = RAJ
		country_exists = CHI
		591 = { is_core_of = RAJ
				is_controlled_by = CHI }
		NOT = {
			OR = {
				CHI = { is_subject_of = RAJ }
				RAJ = { is_subject_of = CHI }
				RAJ = { is_in_faction_with = CHI }
			}
		}
	}
}