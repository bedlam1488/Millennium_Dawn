VEN_destroy_farc = {
	icon = GFX_decision_generic_scorched_earth
	priority = 180
	allowed = { original_tag = VEN }
	visible = { has_completed_focus = VEN_dismantle_the_farc }
}
