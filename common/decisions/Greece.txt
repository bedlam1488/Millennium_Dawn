GRE_austerity_category = {

	GRE_austerity_tension = {

		icon = a
		allowed = { tag = GRE }

		visible = {
			always = no
		}

		activation = {
			always = no
		}

		available = {
			NOT = {
				has_country_flag = GRE_austerity
			}
		}

		days_mission_timeout = 120

		is_good = no
		fire_only_once = no

		timeout_effect = {
			log = "[GetDateText]: [Root.GetName]: mission timeout_effect GRE_austerity_tension"
			GRE = { country_event = { id = greece.2 } }
			if = {
				limit = { has_idea = GRE_austerity_1 }
				swap_ideas = {
					remove_idea = GRE_austerity_1
					add_idea = GRE_austerity_2
				}
				activate_mission = GRE_austerity_tension
			}
			else_if = {
				limit = { has_idea = GRE_austerity_2 }
				swap_ideas = {
					remove_idea = GRE_austerity_2
					add_idea = GRE_austerity_3
				}
				activate_mission = GRE_austerity_tension
			}
			else_if = {
				limit = { has_idea = GRE_austerity_3 }
				swap_ideas = {
					remove_idea = GRE_austerity_3
					add_idea = GRE_austerity_4
				}
				activate_mission = GRE_austerity_tension
			}
			else_if = {
				limit = { has_idea = GRE_austerity_4 }
				swap_ideas = {
					remove_idea = GRE_austerity_4
					add_idea = GRE_austerity_5
				}
			}
		}
	}
	GRE_austerity_placate = {
		icon = GFX_decision_generic_civil_support
		cost = 75
		visible = {
			has_country_flag = GRE_austerity
		}
		available = {
			tag = GRE
			NOT = { has_idea = GRE_austerity_1 }
		}

		days_remove = 55
		days_re_enable = 0
		fire_only_once = no

		complete_effect = {
			add_stability = 0.05
			log = "[GetDateText]: [Root.GetName]: Decision GRE_austerity_placate"
			if = {
				limit = { has_idea = GRE_austerity_2 }
				swap_ideas = {
					remove_idea = GRE_austerity_2
					add_idea = GRE_austerity_1
				}
			}
			else_if = {
				limit = { has_idea = GRE_austerity_3 }
				swap_ideas = {
					remove_idea = GRE_austerity_3
					add_idea = GRE_austerity_2
				}
			}
			else_if = {
				limit = { has_idea = GRE_austerity_4 }
				swap_ideas = {
					remove_idea = GRE_austerity_4
					add_idea = GRE_austerity_3
				}
			}
			else_if = {
				limit = { has_idea = GRE_austerity_5 }
				swap_ideas = {
					remove_idea = GRE_austerity_5
					add_idea = GRE_austerity_4
				}
			}
		}
		ai_will_do = {
			factor = 1

			modifier = {
				if = {
					limit = {
						has_idea = GRE_austerity_3
					}
				}
				factor = 3
			}
			modifier = {
				if = {
					limit = {
						has_idea = GRE_austerity_3
					}
				}
				factor = 5
			}
			modifier = {
				if = {
					limit = {
						has_idea = GRE_austerity_5
					}
				}
				factor = 10
			}
		}
	}
}
generic_coalition_politics_desicions = {

	GRE_rename_istanbul = {
		icon = generic
		visible = {
			tag = GRE
			owns_state = 934
		}
		available = {
			NOT = {
				has_country_flag = istanbul_renamed
			}
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision GRE_rename_istanbul"
			set_province_name = { id = 9833 name = "Constantinople" }
			add_stability = 0.01
			set_country_flag = istanbul_renamed
		}
	}
}
GRE_CYP_category = {

	GRE_guarantee_turkish_property = {

		icon = GFX_decision_turkey
		cost = 300
		days_remove = 50

		visible = {
			tag = GRE
			has_country_flag = GRE_Enosis_In_Progress
		}
		available = {
			has_equipment = {
				infantry_weapons > 99
			}
			check_variable = { 145.GRE_SUPPORT > 50 }
			custom_trigger_tooltip = {
				tooltip = GRE_NCY_incite_riot_TT
				check_variable = { 145.GRE_SUPPORT > 49 }
			}
			custom_trigger_tooltip = {
				tooltip = PER_presidential_decisions_only_one_is_available_TT
				NOT = {
					has_decision = GRE_please_north
					has_decision = GRE_attack_turkish_strength
				}
			}
		}

		complete_effect = {
			custom_effect_tooltip = GRE_lose_20_gain_20
			custom_effect_tooltip = GRE_gain_20_strength
			add_equipment_to_stockpile = {
				type = infantry_weapons
				amount = -100
				producer = GRE
			}
			hidden_effect = {
				log = "[GetDateText]: [Root.GetName]: Decision GRE_guarantee_turkish_property"
				add_to_variable = { 145.GRE_SUPPORT = 20 }
				add_to_variable = { 146.GRE_SUPPORT = -20 }
				clamp_variable = {
					var = 146.GRE_SUPPORT
					min = 0
					max = 100
				}
				clamp_variable = {
					var = 145.GRE_SUPPORT
					min = 0
					max = 100
				}
			}
		}
		ai_will_do = {
			factor = 0.5

			modifier = {
				if = {
					limit = {
						check_variable = { 146.GRE_SUPPORT < 50 }
						check_variable = { 145.GRE_SUPPORT > 90 }
					}
				}
				factor = 10
			}
		}
	}
	GRE_please_north = {

		icon = GFX_decision_turkey
		cost = 30

		visible = {
			has_country_flag = GRE_Enosis_In_Progress
			tag = GRE
		}
		available = {
			custom_trigger_tooltip = {
				tooltip = PER_presidential_decisions_only_one_is_available_TT
				NOT = {
					has_decision = GRE_guarantee_turkish_property
					has_decision = GRE_attack_turkish_strength
				}
			}
			hidden_trigger = {
				NOT = {
					has_country_flag = GRE_cant_gain_strength
				}
			}
		}

		days_remove = 30
		days_re_enable = 0
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision GRE_please_north"
			custom_effect_tooltip = GRE_gain_5_strength
			custom_effect_tooltip = GRE_turks_lose_5_strength
			add_to_variable = { 145.GRE_SUPPORT = 5 }
			add_to_variable = { 146.GRE_SUPPORT = -5 }
			clamp_variable = {
				var = 146.GRE_SUPPORT
				min = 0
				max = 100
			}
		}
		ai_will_do = {
			factor = 3

			modifier = {
				if = {
					limit = {
						check_variable = { 146.GRE_SUPPORT > 50 }
					}
				}
				factor = 2
			}
			modifier = {
				if = {
					limit = {
						check_variable = { 146.GRE_SUPPORT > 80 }
					}
				}
				factor = 1
			}
		}
	}
	GRE_attack_turkish_strength = {

		icon = GFX_decision_turkey
		cost = 30
		days_remove = 60

		visible = {
			has_country_flag = GRE_Enosis_In_Progress
			tag = GRE
		}
		available = {
			custom_trigger_tooltip = {
				tooltip = PER_presidential_decisions_only_one_is_available_TT
				NOT = {
					has_decision = GRE_guarantee_turkish_property
					has_decision = GRE_please_north
				}
			}
		}
		days_re_enable = 10
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision GRE_attack_turkish_strength"
			custom_effect_tooltip = TUR_unable_to_gain_strength
			TUR = {
				set_country_flag = TUR_greece_blocked_us
			}
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision GRE_attack_turkish_strength"
			TUR = {
				clr_country_flag = TUR_greece_blocked_us
			}
		}
		ai_will_do = {
			factor = 4

			modifier = {
				if = {
					limit = {
						check_variable = { 145.GRE_SUPPORT > 50 }
					}
				}
				factor = 3
			}
			modifier = {
				if = {
					limit = {
						check_variable = { 145.GRE_SUPPORT > 80 }
					}
				}
				factor = 1
			}
		}
	}
	#############################################################################
	TUR_CYP_incite_riot = {

		icon = GFX_decision_revolt
		cost = 30

		available = {
			custom_trigger_tooltip = {
				tooltip = PER_presidential_decisions_only_one_is_available_TT
				NOT = {
					has_decision = TUR_CYP_propaganda_south
					has_decision = TUR_CYP_propaganda_north
				}
			}
			custom_trigger_tooltip = {
				tooltip = GRE_enemy_blocked_us
				NOT = { has_country_flag = TUR_greece_blocked_us }
			}
			has_equipment = {
				infantry_weapons > 99
			}
			custom_trigger_tooltip = {
				tooltip = TUR_CYP_incite_riot_TT
				check_variable = { 146.GRE_SUPPORT > 49 }
			}
		}

		visible = {
			tag = TUR
		}

		days_remove = 56
		days_re_enable = 0
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision TUR_incite_riot"
			custom_effect_tooltip = GRE_south_-20_TT
			custom_effect_tooltip = TUR_gain_20_strength
			add_equipment_to_stockpile = {
				type = infantry_weapons
				amount = -100
				producer = TUR
			}
			hidden_effect = {
				add_to_variable = { 145.GRE_SUPPORT = -20 }
				clamp_variable = {
					var = 145.GRE_SUPPORT
					min = 0
					max = 100
				}
				add_to_variable = { 146.GRE_SUPPORT = 20 }
				clamp_variable = {
					var = 146.GRE_SUPPORT
					min = 0
					max = 100
				}
				GRE = {
					country_event = {
						id = greece.13
						days = 0
					}
				}
			}
		}
		ai_will_do = {
			factor = 1

			modifier = {
				if = {
					limit = {
						check_variable = { 146.GRE_SUPPORT > 50 }
					}
				}
				factor = 3
			}
			modifier = {
				if = {
					limit = {
						has_equipment = {
							infantry_weapons > 499
						}
					}
				}
				add = 1
			}
			modifier = {
				if = {
					limit = {
						check_variable = { 146.GRE_SUPPORT > 80 }
					}
				}
				add = 2
			}
		}
	}
	TUR_CYP_propaganda_north = {

		icon = eng_propaganda_campaigns
		cost = 30

		available = {
			custom_trigger_tooltip = {
				tooltip = PER_presidential_decisions_only_one_is_available_TT
				NOT = {
					has_decision = TUR_CYP_propaganda_south
					has_decision = TUR_CYP_incite_riot
				}
			}
			custom_trigger_tooltip = {
				tooltip = GRE_enemy_blocked_us
				NOT = { has_country_flag = TUR_greece_blocked_us }
			}
			custom_trigger_tooltip = {
				tooltip = TUR_GRE_support
				check_variable = { 145.GRE_SUPPORT > 5 }
			}
		}

		visible = {
			tag = TUR
		}

		days_remove = 60
		days_re_enable = 10
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision TUR_incite_riot"
			custom_effect_tooltip = GRE_unable_to_gain_strength
			GRE = {
				set_country_flag = GRE_cant_gain_strength
			}
		}
		remove_effect = {
			GRE = {
				clr_country_flag = GRE_cant_gain_strength
			}
		}
		ai_will_do = {
			factor = 2

			modifier = {
				if = {
					limit = {
						check_variable = { 146.GRE_SUPPORT > 50 }
					}
				}
				factor = 4
			}
			modifier = {
				if = {
					limit = {
						check_variable = { 146.GRE_SUPPORT > 80 }
					}
				}
				factor = 5
			}
		}
	}
	TUR_CYP_propaganda_south = {

		icon = eng_propaganda_campaigns
		cost = 30

		available = {
			custom_trigger_tooltip = {
				tooltip = PER_presidential_decisions_only_one_is_available_TT
				NOT = {
					has_decision = TUR_CYP_propaganda_north
					has_decision = TUR_CYP_incite_riot
				}
			}
			custom_trigger_tooltip = {
				tooltip = GRE_enemy_blocked_us
				NOT = { has_country_flag = TUR_greece_blocked_us }
			}
			custom_trigger_tooltip = {
				tooltip = TUR_GRE_support
				check_variable = { 145.GRE_SUPPORT > 1 }
			}
		}

		visible = {
			tag = TUR
		}

		days_remove = 30
		days_re_enable = 0
		fire_only_once = no

		complete_effect = {
			custom_effect_tooltip = GRE_north_5_TT
			custom_effect_tooltip = GRE_south_-5_TT
			hidden_effect = {
				log = "[GetDateText]: [Root.GetName]: Decision TUR_incite_riot"
				add_to_variable = { 146.GRE_SUPPORT = 5 }
				add_to_variable = { 145.GRE_SUPPORT = -5 }
				clamp_variable = {
					var = 146.GRE_SUPPORT
					min = 0
					max = 100
				}
			}
		}
		ai_will_do = {
			factor = 2

			modifier = {
				if = {
					limit = {
						check_variable = { 146.GRE_SUPPORT > 50 }
					}
				}
				factor = 4
			}
			modifier = {
				if = {
					limit = {
						check_variable = { 146.GRE_SUPPORT > 80 }
					}
				}
				factor = 5
			}
		}
	}
	##############
	TUR_unify_cyprus = {

		icon = eng_propaganda_campaigns
		cost = 30

		available = {
			custom_trigger_tooltip = {
				tooltip = TUR_has_100_strength
				check_variable = { 146.GRE_SUPPORT > 99 }
			}
		}

		visible = {
			tag = TUR
		}

		days_remove = 100
		days_re_enable = 0
		fire_only_once = yes

		complete_effect = {
			custom_effect_tooltip = TUR_unify_cyp
			clr_country_flag = GRE_TUR_CYP
			hidden_effect = {
				news_event = {
					id = Turkey.77
				}
				NCY = { set_cosmetic_tag = CYP_TURK_UNIFIED }
				145 = { transfer_state_to = NCY }
				NCY = {
					set_country_flag = is_VANGUARD
				}
			}
		}
		ai_will_do = {
			factor = 5
		}
	}
	#######
	GRE_unify_cyprus = {

		icon = eng_propaganda_campaigns
		cost = 30

		available = {
			custom_trigger_tooltip = {
				tooltip = GRE_has_100_strength
				check_variable = { 145.GRE_SUPPORT > 99 }
			}
		}

		visible = {
			tag = GRE
		}

		days_remove = 100
		days_re_enable = 0
		fire_only_once = yes

		complete_effect = {
			custom_effect_tooltip = GRE_unify_cyp
			clr_country_flag = GRE_TUR_CYP
			hidden_effect = {
				news_event = {
					id = Turkey.88
				}
				146 = { transfer_state_to = GRE }
				145 = { transfer_state_to = GRE }
			}
		}
		ai_will_do = {
			factor = 5
		}
	}
}