leader_traits = {

	populist_president = {
		random = no
		political_power_gain = 0.50
		drift_defence_factor = 0.10
		stability_factor = 0.05

		ai_will_do = {
			factor = 1
		}
	}

	populist_president2 = {
		random = no
		political_power_gain = 1.0
		drift_defence_factor = 0.25
		stability_factor = 0.05

		ai_will_do = {
			factor = 1
		}
	}

	economic_managers = {
		random = no
		political_power_gain = 0.25
		tax_gain_multiplier_modifier = 0.05
		stability_factor = -0.05
	}
}
