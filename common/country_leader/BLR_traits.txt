leader_traits = {
	patriot_of_belarus = {
		random = no
		nationalist_drift = 0.05
		stability_factor = 0.02

		ai_will_do = {
			factor = 1
		}
	}
	last_dictator_of_europe = {
		random = no
		political_power_gain = 0.02
		communism_drift = 0.07

		ai_will_do = {
			factor = 1
		}
	}
	anti_russian = {
		random = no
		ai_strategy = {
			type = antagonize
			id = "SOV"
			value = 200
		}
		ai_strategy = {
			type = conquer
			id = "SOV"
			value = 100
		}
		ai_will_do = {
			factor = 1
		}
	}
	hater_opposition = {
		random = no
		drift_defence_factor = 0.3
		ai_will_do = {
			factor = 1
		}
	}


}