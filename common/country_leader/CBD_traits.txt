leader_traits = {
	samdech_akka_moha = { ##Lord Prime Minister in Khmer
		random = no

		political_power_gain = 0.25
		communism_drift = 0.05

		ai_will_do = {
			factor = 1
		}
	}
	ex_khmer_rouge_solider = {
		random = no

		army_core_defence_factor = 0.10
		stability_factor = -0.05
		war_support_factor = 0.05

		ai_will_do = {
			factor = 1
		}
	}
}
