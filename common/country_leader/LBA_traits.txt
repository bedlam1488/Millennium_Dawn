leader_traits = {

	head_of_gptc = {
		random = no
		production_speed_internet_station_factor = 0.10
		drift_defence_factor = 0.25

		ai_will_do = {
			factor = 1
		}
	}

	uninterested_in_politics = {
		random = no
		stability_factor = -0.10

		ai_will_do = {
			factor = 1
		}
	}

	vain_football_star = {
		random = no
		communism_drift = -0.01

		ai_will_do = {
			factor = 1
		}
	}

	the_77th_tank_battalion = {
		random = no
		army_armor_attack_factor = 0.10
	    army_armor_defence_factor = 0.10

		ai_will_do = {
			factor = 1
		}
	}

	suspected_revolutionary = {
		random = no
		stability_factor = -0.15

		ai_will_do = {
			factor = 1
		}
	}

	bachelor_of_marine_navigation = {
		random = no
		naval_speed_factor = 0.05

		ai_will_do = {
			factor = 1
		}
	}

	master_of_shipping_economics_and_logistics = {
		random = no
		naval_speed_factor = 0.10
		production_speed_dockyard_factor = 0.15

		ai_will_do = {
			factor = 1
		}
	}

	graduate_of_kuznetsov = {
		random = no
		naval_speed_factor = 0.10
		naval_enemy_fleet_size_ratio_penalty_factor = 0.25

		ai_will_do = {
			factor = 1
		}
	}

	king_of_coke = {
		random = no
		production_speed_industrial_complex_factor = 0.15

		ai_will_do = {
			factor = 1
		}
	}

	national_security_advisor = {
		random = no
		police_cost_multiplier_modifier = -0.15

		ai_will_do = {
			factor = 1
		}
	}

	claudia_schiffer_of_north_africa = {
		random = no
		foreign_influence_modifier = 0.10
		social_cost_multiplier_modifier = 0.10

		ai_will_do = {
			factor = 1
		}
	}

	un_goodwill_ambassador = {
		random = no
		foreign_influence_modifier = 0.10
		expected_welfare_modifier = 0.5
		expected_healthcare_modifier = 0.5

		ai_will_do = {
			factor = 1
		}
	}

	amazon_of_libya = {
		random = no
		conscription_factor = 0.15
		army_defence_factor = 0.15
		experience_gain_factor = 0.15

		ai_will_do = {
			factor = 1
		}
	}

	khamis_brigade = {
		random = no
		mechanized_attack_factor = 0.10
		mechanized_defence_factor = 0.10

		ai_will_do = {
			factor = 1
		}
	}

	political_flip_flopper = {
		random = no
		political_power_factor = 0.10
		foreign_influence_modifier = -0.15

		ai_will_do = {
			factor = 1
		}
	}

	grand_mufti = {
		random = no
		fascism_drift = 0.05
		neutrality_drift = 0.05
		foreign_influence_modifier = 0.10
		stability_factor = 0.05

		ai_will_do = {
			factor = 1
		}
	}

	illegitimate_king = {
		random = no
		nationalist_drift = -0.05
		political_power_factor = -0.10

		ai_will_do = {
			factor = 1
		}
	}

	king_of_kings_of_africa = {
		random = no
		foreign_influence_modifier = 0.15
		communism_drift = 0.05
		neutrality_drift = 0.05

		ai_will_do = {
			factor = 1
		}
	}

	fatherly_leader = {
		random = no
		foreign_influence_defense_modifier = 0.15
		communism_drift = 0.20

		ai_will_do = {
			factor = 1
		}
	}

}