# Author(s): AngriestBird
FNCI_foreign_supporters = {
	allowed = {
		OR = {
			original_tag = LIB
			original_tag = BFA
		}
	}
	enable = {
		country_exists = FNC
		FNC = { has_war_with = CDI }
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "FNC" value = 75 }
	ai_strategy = { type = support id = "FNC" value = 75 }
	ai_strategy = { type = send_volunteers_desire id = "FNC" value = 100 }
}

CDI_foreign_supporters = {
	allowed = {
		original_tag = FRA
	}
	enable = {
		country_exists = CDI
		CDI = { has_war_with = FNC }
	}
	abort_when_not_enabled = yes


	ai_strategy = { type = befriend id = "CDI" value = 75 }
	ai_strategy = { type = support id = "CDI" value = 75 }
	ai_strategy = { type = send_volunteers_desire id = "CDI" value = 100 }
}