VEN_recognize_abkhazia = {
	allowed = { original_tag = VEN }
	enable = {
		NOT = { has_war_with = ABK }
	}
	abort = {
		has_war_with = ABK
	}
	ai_strategy = { type = befriend id = "ABK" value = 25 }
}

VEN_gran_colombia_alliance = {
	allowed = { NOT = { original_tag = VEN } }
	enable = {
		VEN = {
			has_idea = faction_gca_alliance
		}
		has_idea = faction_gca_alliance
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = alliance id = "VEN" value = 100 }
}