SUD_support_AGF = {
	allowed = {
		original_tag = SSU
	}
	enable = {
		country_exists = AGF
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "AGF" value = 50 }
	ai_strategy = { type = protect id = "AGF" value = 50 }
	ai_strategy = { type = influence id = "AGF" value = 50 }
	ai_strategy = { type = support id = "AGF" value = 50 }
}

SUD_support_YEM = {
	allowed = {
		original_tag = SSU
	}
	enable = {
		YEM = { has_war_with = HOU }
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "YEM" value = 100 }
	ai_strategy = { type = protect id = "YEM" value = 100 }
	ai_strategy = { type = influence id = "YEM" value = 100 }
	ai_strategy = { type = support id = "YEM" value = 200 }
	ai_strategy = { type = send_volunteers_desire id = "YEM" value = 200 }
}

SUD_support_kablia = {
	allowed = {
		original_tag = SUD
		has_start_date < 2001.1.1
	}
	enable = {
		NOT = { has_war_with = DRC }
		DRC = { has_country_leader = { name = "Laurent-D�sir� Kabila" } }
	}
	abort = {
		OR = {
			has_war_with = DRC
			DRC = { NOT = { has_country_leader = { name = "Laurent-D�sir� Kabila" } } }
		}
	}

	ai_strategy = { type = befriend id = "DRC" value = 50 }
	ai_strategy = { type = support id = "DRC" value = 100 }
}

#Libya befriended Sudan
SUD_befriend_libya = {
	allowed = { original_tag = SUD }

	enable = {
		LBA = { has_completed_focus = LBA_support_sudan }
		NOT = { has_war_with = SUD }
	}

	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "SUD" value = 25 }
}