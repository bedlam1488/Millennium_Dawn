﻿MD_desperately_need_guns = {
	allowed = {
		ai_for_units_is_not_disabled = yes
	}
	enable = {
		has_equipment = { Inf_equipment < 1 }
	}
	abort = {
		has_equipment = { Inf_equipment > 2000 }
	}

	ai_strategy = { type = equipment_variant_production_factor id = Inf_equipment value = 2500 }
	ai_strategy = { type = equipment_production_factor id = anti_tank value = -150 }
	ai_strategy = { type = equipment_production_factor id = artillery value = -150 }
	ai_strategy = { type = equipment_variant_production_factor id = cnc_equipment value = -150 }
	ai_strategy = { type = equipment_production_factor id = mechanized value = -150 }
	ai_strategy = { type = equipment_production_factor id = armor value = -150 }
	ai_strategy = { type = role_ratio id = L_Inf value = -500 }
	ai_strategy = { type = role_ratio id = infantry value = -500 }
	ai_strategy = { type = role_ratio id = garrison value = -500 }
	ai_strategy = { type = role_ratio id = apc_mechanized value = -500 }
	ai_strategy = { type = role_ratio id = ifv_mechanized value = -500 }
	ai_strategy = { type = role_ratio id = armor value = -500 }
	ai_strategy = { type = role_ratio id = marines value = -500 }
	ai_strategy = { type = role_ratio id = Air_helicopters value = -500 }
	ai_strategy = { type = role_ratio id = Air_mech value = -500 }
	ai_strategy = { type = role_ratio id = Special_Forces value = -500 }
}

MD_excess_inf_equipment = {
	allowed = {
		ai_for_units_is_not_disabled = yes
	}
	enable = {
		has_equipment = { Inf_equipment > 49900 }
	}
	abort = {
		has_equipment = { Inf_equipment < 50000 }
		has_equipment = { Inf_equipment > 19999 }
	}

	ai_strategy = { type = equipment_variant_production_factor id = Inf_equipment value = -150 }
}

MD_poor_production_strategy = {
	allowed = {
		ai_for_units_is_not_disabled = yes
	}
	enable = {
		num_of_military_factories < 6
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = equipment_variant_production_factor id = Inf_equipment value = 300 }
	ai_strategy = { type = equipment_variant_production_factor id = cnc_equipment value = 100 }
	ai_strategy = { type = equipment_variant_production_factor id = util_vehicle_equipment value = 100 }
	ai_strategy = { type = equipment_variant_production_factor id = train_equipment value = 20 }
	ai_strategy = { type = equipment_variant_production_factor id = L_AT_Equipment value = 60 }
	ai_strategy = { type = equipment_variant_production_factor id = H_AT_Equipment value = 40 }
	ai_strategy = { type = equipment_variant_production_factor id = AA_Equipment value = 60 }
	ai_strategy = { type = equipment_variant_production_factor id = apc_hull value = 20 }
	ai_strategy = { type = equipment_variant_production_factor id = ifv_hull value = 10 }
	ai_strategy = { type = equipment_variant_production_factor id = spart_hull value = 10 }
	ai_strategy = { type = equipment_variant_production_factor id = spaa_hull value = 10 }
	ai_strategy = { type = equipment_variant_production_factor id = mbt_hull value = 10 }
	ai_strategy = { type = equipment_variant_production_factor id = artillery_equipment value = 40 }
}

MD_default_production_strategy = {
	allowed = {
		ai_for_units_is_not_disabled = yes
	}
	enable = {
		num_of_military_factories > 5
		num_of_military_factories < 11
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = equipment_variant_production_factor id = Inf_equipment value = 200 }
	ai_strategy = { type = equipment_variant_production_factor id = cnc_equipment value = 80 }
	ai_strategy = { type = equipment_variant_production_factor id = util_vehicle_equipment value = 80 }
	ai_strategy = { type = equipment_variant_production_factor id = train_equipment value = 20 }
	ai_strategy = { type = equipment_variant_production_factor id = L_AT_Equipment value = 60 }
	ai_strategy = { type = equipment_variant_production_factor id = H_AT_Equipment value = 40 }
	ai_strategy = { type = equipment_variant_production_factor id = AA_Equipment value = 60 }
	ai_strategy = { type = equipment_variant_production_factor id = apc_hull value = 60 }
	ai_strategy = { type = equipment_variant_production_factor id = ifv_hull value = 60 }
	ai_strategy = { type = equipment_variant_production_factor id = spart_hull value = 30 }
	ai_strategy = { type = equipment_variant_production_factor id = spaa_hull value = 30 }
	ai_strategy = { type = equipment_variant_production_factor id = mbt_hull value = 60 }
	ai_strategy = { type = equipment_variant_production_factor id = artillery_equipment value = 20 }
	ai_strategy = { type = equipment_variant_production_factor id = transport_helicopter_equipment value = -5 }
}

MD_default_production_mandatory_spread_strategy = {
	allowed = {
		ai_for_units_is_not_disabled = yes
	}
	enable = {
		num_of_military_factories > 5
		num_of_military_factories < 11
		# we have good equipment
		has_equipment = { mbt_hull > 0 }
		has_equipment = { apc_hull > 0 }
		has_equipment = { ifv_hull > 0 }
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = equipment_production_min_factories id = infantry value = 1 }
	ai_strategy = { type = equipment_production_min_factories id = motorized value = 1 }
}

MD_major_production_strategy = {
	allowed = {
		ai_for_units_is_not_disabled = yes
	}
	enable = {
		num_of_military_factories > 10
		num_of_military_factories < 25
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = equipment_variant_production_factor id = Inf_equipment value = 160 }
	ai_strategy = { type = equipment_variant_production_factor id = cnc_equipment value = 60 }
	ai_strategy = { type = equipment_variant_production_factor id = util_vehicle_equipment value = 60 }
	ai_strategy = { type = equipment_variant_production_factor id = train_equipment value = 10 }
	ai_strategy = { type = equipment_variant_production_factor id = L_AT_Equipment value = 20 }
	ai_strategy = { type = equipment_variant_production_factor id = H_AT_Equipment value = 30 }
	ai_strategy = { type = equipment_variant_production_factor id = AA_Equipment value = 20 }
	ai_strategy = { type = equipment_variant_production_factor id = apc_hull value = 60 }
	ai_strategy = { type = equipment_variant_production_factor id = ifv_hull value = 80 }
	ai_strategy = { type = equipment_variant_production_factor id = spart_hull value = 30 }
	ai_strategy = { type = equipment_variant_production_factor id = spaa_hull value = 30 }
	ai_strategy = { type = equipment_variant_production_factor id = mbt_hull value = 80 }
	ai_strategy = { type = equipment_variant_production_factor id = light_tank_hull value = 10 }
	ai_strategy = { type = equipment_variant_production_factor id = artillery_equipment value = 10 }
	ai_strategy = { type = equipment_variant_production_factor id = transport_helicopter_equipment value = 1 }
}

MD_major_production_mandatory_spread_strategy = {
	allowed = {
		ai_for_units_is_not_disabled = yes
	}
	enable = {
		num_of_military_factories > 10
		num_of_military_factories < 25
		# we have good equipment
		has_equipment = { mbt_hull > 0 }
		has_equipment = { apc_hull > 0 }
		has_equipment = { ifv_hull > 0 }
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = equipment_production_min_factories id = infantry value = 2 }
	ai_strategy = { type = equipment_production_min_factories id = motorized value = 1 }
	ai_strategy = { type = equipment_production_min_factories id = mechanized value = 1 }
}

MD_superpower_production_strategy = {
	allowed = {
		ai_for_units_is_not_disabled = yes
	}
	enable = {
		num_of_military_factories > 24
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = equipment_variant_production_factor id = Inf_equipment value = 100 }
	ai_strategy = { type = equipment_variant_production_factor id = cnc_equipment value = 50 }
	ai_strategy = { type = equipment_variant_production_factor id = util_vehicle_equipment value = 50 }
	ai_strategy = { type = equipment_variant_production_factor id = train_equipment value = 10 }
	ai_strategy = { type = equipment_variant_production_factor id = L_AT_Equipment value = 20 }
	ai_strategy = { type = equipment_variant_production_factor id = H_AT_Equipment value = 30 }
	ai_strategy = { type = equipment_variant_production_factor id = AA_Equipment value = 20 }
	ai_strategy = { type = equipment_variant_production_factor id = apc_hull value = 60 }
	ai_strategy = { type = equipment_variant_production_factor id = ifv_hull value = 80 }
	ai_strategy = { type = equipment_variant_production_factor id = spart_hull value = 30 }
	ai_strategy = { type = equipment_variant_production_factor id = spaa_hull value = 30 }
	ai_strategy = { type = equipment_variant_production_factor id = mbt_hull value = 80 }
	ai_strategy = { type = equipment_variant_production_factor id = light_tank_hull value = 15 }
	ai_strategy = { type = equipment_variant_production_factor id = artillery_equipment value = 5 }
	ai_strategy = { type = equipment_variant_production_factor id = transport_helicopter_equipment value = 5 }

	ai_strategy = { type = equipment_production_min_factories id = infantry value = 2 }
	ai_strategy = { type = equipment_production_min_factories id = train value = 1 }
	ai_strategy = { type = equipment_production_min_factories id = motorized value = 1 }
	ai_strategy = { type = equipment_production_min_factories id = mechanized value = 2 }
	ai_strategy = { type = equipment_production_min_factories id = armor value = 1 }
}

MD_build_equipment_not_units_while_at_war = {
	allowed = {
		ai_for_units_is_not_disabled = yes
	}
	enable = {
		has_war = yes
		OR = {
			has_equipment = { Inf_equipment < 1000 }
			has_equipment = { L_AT_Equipment < 200 }
			has_equipment = { util_vehicle_equipment < 400 }
			has_equipment = { cnc_equipment < 500 }
			has_equipment = { AA_Equipment < 200 }
		}
	}

	abort_when_not_enabled = yes

	ai_strategy = { type = equipment_variant_production_factor id = Inf_equipment value = 100 }
	ai_strategy = { type = role_ratio id = L_Inf value = -500 }
	ai_strategy = { type = role_ratio id = infantry value = -500 }
	ai_strategy = { type = role_ratio id = garrison value = -500 }
	ai_strategy = { type = role_ratio id = apc_mechanized value = -500 }
	ai_strategy = { type = role_ratio id = ifv_mechanized value = -500 }
	ai_strategy = { type = role_ratio id = armor value = -500 }
	ai_strategy = { type = role_ratio id = marines value = -500 }
	ai_strategy = { type = role_ratio id = Air_helicopters value = -500 }
	ai_strategy = { type = role_ratio id = Air_mech value = -500 }
	ai_strategy = { type = role_ratio id = Special_Forces value = -500 }
}

MD_build_command_equipment = {
	allowed = {
		ai_for_units_is_not_disabled = yes
	}
	enable = { has_equipment = { cnc_equipment < 750 } }
	abort = { has_equipment = { cnc_equipment > 2250 } }
	ai_strategy = { type = equipment_variant_production_factor id = cnc_equipment value = 150 }
}

default_stockpile_management = {
	enable = { has_war = no NOT = { original_tag = USA original_tag = CHI } }
	abort = { has_war = yes }

	ai_strategy = { type = equipment_stockpile_surplus_ratio value = 45 } # double base stockpile #from 25 to 35
}

war_stockpile_management = {
	enable = { has_war = yes NOT = { original_tag = USA original_tag = CHI } }
	abort = { has_war = no }

	ai_strategy = { type = equipment_stockpile_surplus_ratio value = 35 } # double base stockpile #from 25 to 35
}

#Aircraft
AI_air_default_priority_low_military = {
	allowed = {
		ai_for_units_is_not_disabled = yes

		NOT = {
			original_tag = USA #United States
			original_tag = CHI
		}
	}
	enable = {
		num_of_military_factories < 25
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = equipment_variant_production_factor id = small_plane_airframe value = -50 }
	ai_strategy = { type = equipment_variant_production_factor id = small_plane_strike_airframe value = 0 }
	ai_strategy = { type = equipment_variant_production_factor id = small_plane_cas_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = small_plane_naval_bomber_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = small_plane_suicide_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = medium_plane_airframe value = -90 }
	ai_strategy = { type = equipment_variant_production_factor id = medium_plane_fighter_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = medium_plane_cas_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = medium_plane_maritime_patrol_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = medium_plane_suicide_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = medium_plane_air_transport_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = large_plane_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = large_plane_air_transport_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = large_plane_awacs_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = large_plane_cas_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = large_plane_maritime_patrol_airframe value = -100 }

	ai_strategy = { type = equipment_variant_production_factor id = cv_small_plane_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = cv_small_plane_strike_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = cv_small_plane_cas_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = cv_small_plane_naval_bomber_airframe value = -95 }
	ai_strategy = { type = equipment_variant_production_factor id = cv_small_plane_suicide_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = cv_medium_plane_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = cv_medium_plane_fighter_airframe value = -95 }
	ai_strategy = { type = equipment_variant_production_factor id = cv_medium_plane_cas_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = cv_medium_plane_maritime_patrol_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = cv_medium_plane_air_transport_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = cv_medium_plane_scout_airframe value = -100 }

	ai_strategy = { type = equipment_production_min_factories id = tactical_bomber value = 2 }

	ai_strategy = { type = air_factory_balance value = 20 }
}

AI_air_default_priority_mod_military = {
	allowed = {
		ai_for_units_is_not_disabled = yes

		NOT = {
			original_tag = USA #United States
			original_tag = CHI
		}
	}
	enable = {
		num_of_military_factories > 24
		num_of_military_factories < 50
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = equipment_variant_production_factor id = small_plane_airframe value = -90 }
	ai_strategy = { type = equipment_variant_production_factor id = small_plane_strike_airframe value = -90 }
	ai_strategy = { type = equipment_variant_production_factor id = small_plane_cas_airframe value = -90 }
	ai_strategy = { type = equipment_variant_production_factor id = small_plane_naval_bomber_airframe value = -90 }
	ai_strategy = { type = equipment_variant_production_factor id = small_plane_suicide_airframe value = -90 }
	ai_strategy = { type = equipment_variant_production_factor id = medium_plane_airframe value = -50 }
	ai_strategy = { type = equipment_variant_production_factor id = medium_plane_fighter_airframe value = -80 }
	ai_strategy = { type = equipment_variant_production_factor id = medium_plane_cas_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = medium_plane_maritime_patrol_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = medium_plane_suicide_airframe value = -80 }
	ai_strategy = { type = equipment_variant_production_factor id = medium_plane_air_transport_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = large_plane_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = large_plane_air_transport_airframe value = -95 }
	ai_strategy = { type = equipment_variant_production_factor id = large_plane_awacs_airframe value = -95 }
	ai_strategy = { type = equipment_variant_production_factor id = large_plane_cas_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = large_plane_maritime_patrol_airframe value = -95 }

	ai_strategy = { type = equipment_variant_production_factor id = cv_small_plane_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = cv_small_plane_strike_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = cv_small_plane_cas_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = cv_small_plane_naval_bomber_airframe value = -90 }
	ai_strategy = { type = equipment_variant_production_factor id = cv_small_plane_suicide_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = cv_medium_plane_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = cv_medium_plane_fighter_airframe value = -50 }
	ai_strategy = { type = equipment_variant_production_factor id = cv_medium_plane_cas_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = cv_medium_plane_maritime_patrol_airframe value = -90 }
	ai_strategy = { type = equipment_variant_production_factor id = cv_medium_plane_air_transport_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = cv_medium_plane_scout_airframe value = -95 }

	ai_strategy = { type = air_factory_balance value = 40 }
}

AI_air_default_priority_high_military = {
	allowed = {
		ai_for_units_is_not_disabled = yes

		NOT = {
			original_tag = USA #United States
			original_tag = CHI
		}
	}
	enable = {
		num_of_military_factories > 50
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = equipment_variant_production_factor id = small_plane_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = small_plane_strike_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = small_plane_cas_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = small_plane_naval_bomber_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = small_plane_suicide_airframe value = -25 }
	ai_strategy = { type = equipment_variant_production_factor id = medium_plane_airframe value = 0 }
	ai_strategy = { type = equipment_variant_production_factor id = medium_plane_fighter_airframe value = 0 }
	ai_strategy = { type = equipment_variant_production_factor id = medium_plane_cas_airframe value = -25 }
	ai_strategy = { type = equipment_variant_production_factor id = medium_plane_maritime_patrol_airframe value = -90 }
	ai_strategy = { type = equipment_variant_production_factor id = medium_plane_suicide_airframe value = -75 }
	ai_strategy = { type = equipment_variant_production_factor id = medium_plane_air_transport_airframe value = -90 }
	ai_strategy = { type = equipment_variant_production_factor id = large_plane_airframe value = -80 }
	ai_strategy = { type = equipment_variant_production_factor id = large_plane_air_transport_airframe value = -90 }
	ai_strategy = { type = equipment_variant_production_factor id = large_plane_awacs_airframe value = -90 }
	ai_strategy = { type = equipment_variant_production_factor id = large_plane_cas_airframe value = -90 }
	ai_strategy = { type = equipment_variant_production_factor id = large_plane_maritime_patrol_airframe value = -90 }

	ai_strategy = { type = equipment_variant_production_factor id = cv_small_plane_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = cv_small_plane_strike_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = cv_small_plane_cas_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = cv_small_plane_naval_bomber_airframe value = -90 }
	ai_strategy = { type = equipment_variant_production_factor id = cv_small_plane_suicide_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = cv_medium_plane_airframe value = -99 }
	ai_strategy = { type = equipment_variant_production_factor id = cv_medium_plane_fighter_airframe value = 150 }
	ai_strategy = { type = equipment_variant_production_factor id = cv_medium_plane_cas_airframe value = -90 }
	ai_strategy = { type = equipment_variant_production_factor id = cv_medium_plane_maritime_patrol_airframe value = -80 }
	ai_strategy = { type = equipment_variant_production_factor id = cv_medium_plane_air_transport_airframe value = -100 }
	ai_strategy = { type = equipment_variant_production_factor id = cv_medium_plane_scout_airframe value = -100 }

	ai_strategy = { type = air_factory_balance value = 70 }
}

AI_air_default_war_panic_fighter_production_light = {
	allowed = {
		ai_for_units_is_not_disabled = yes
		NOT = {
			original_tag = USA #United States
			original_tag = CHI
		}
	}
	enable = {
		OR = {
			has_equipment = { small_plane_airframe < 200 }
			has_equipment = { small_plane_strike_airframe < 200 }
			has_equipment = { small_plane_cas_airframe < 200 }
			has_equipment = { small_plane_naval_bomber_airframe < 200 }
			has_equipment = { small_plane_suicide_airframe < 200 }
		}
		has_war = yes
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = equipment_variant_production_factor id = small_plane_airframe value = 200 }
	ai_strategy = { type = equipment_variant_production_factor id = small_plane_strike_airframe value = 200 }
	ai_strategy = { type = equipment_variant_production_factor id = small_plane_cas_airframe value = 200 }
	ai_strategy = { type = equipment_variant_production_factor id = small_plane_naval_bomber_airframe value = 200 }
	ai_strategy = { type = equipment_variant_production_factor id = small_plane_suicide_airframe value = 200 }
}

MD_dont_build_transport_helicopter_strong = {
	allowed = {
		ai_for_units_is_not_disabled = yes
		NOT = {
			original_tag = CHI
			original_tag = SOV
			original_tag = USA
		}
	}
	enable = {
		num_of_military_factories > 50
		has_equipment = { transport_helicopter_equipment > 1500 }
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = equipment_variant_production_factor id = transport_helicopter_equipment value = -500 }
}

MD_build_transport_helicopter = {
	allowed = {
		ai_for_units_is_not_disabled = yes
		NOT = {
			original_tag = CHI
			original_tag = SOV
			original_tag = USA
		}
	}
	enable = {
		num_of_military_factories > 50
		has_equipment = { transport_helicopter_equipment < 800 }
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = equipment_variant_production_factor id = transport_helicopter_equipment value = 50 }
}

MD_need_trains = {
	allowed = { ai_for_units_is_not_disabled = yes }

	enable = {
		has_equipment = { train_equipment < 200 }
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = equipment_variant_production_factor id = train_equipment value = 100 }
}