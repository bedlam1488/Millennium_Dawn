GEO_support_afghanistan = {
	allowed = { original_tag = GEO }
	enable = {
		NOT = { has_war_with = AFG }
		AFG = { NOT = { has_government = fascism } }
	}
	abort = {
		has_war_with = AFG
		NOT = { has_government = fascism }
	}

	ai_strategy = { type = befriend id = "AFG" value = 50 }
	ai_strategy = { type = support id = "AFG" value = 150 }
	ai_strategy = { type = send_volunteers_desire id = "AFG" value = 100 }
}

GEO_breakaway_states = {
	allowed = { original_tag = GEO }
	enable = {
		NOT = {
			is_in_faction_with = ABK
			is_in_faction_with = SOO
		}
		OR = {
			country_exists = ABK
			country_exists = SOO
		}
	}
	abort = {
		OR = {
			AND = {
				is_in_faction_with = ABK
				is_in_faction_with = SOO
			}
			NOT = {
				country_exists = ABK
				country_exists = SOO
			}
		}
	}

	ai_strategy = { type = contain id = "SOV" value = 50 }
	ai_strategy = { type = antagonize id = "SOV" value = 25 }
	ai_strategy = { type = antagonize id = "ABK" value = 100 }
	ai_strategy = { type = conquer id = "ABK" value = 100 }
	ai_strategy = { type = antagonize id = "SOO" value = 100 }
	ai_strategy = { type = conquer id = "SOO" value = 100 }
}
GEO_dont_declare_on_turkey = {
	allowed = { original_tag = GEO }
	enable = {
		original_tag = GEO
		has_wargoal_against = TUR
		strength_ratio = {
			tag = TUR
			ratio < 1.0
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = conquer id = "TUR" value = -300 }
	ai_strategy = { type = declare_war id = "TUR" value = -300 }
}
GEO_dont_declare_on_azeri = {
	allowed = { original_tag = GEO }
	enable = {
		original_tag = GEO
		has_wargoal_against = AZE
		strength_ratio = {
			tag = AZE
			ratio < 1.0
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = conquer id = "AZE" value = -300 }
	ai_strategy = { type = declare_war id = "AZE" value = -300 }
}