ROM_support_AFG = {
	allowed = {
		original_tag = ROM
		has_start_date > 2016.1.1
	}
	enable = {
		NOT = { has_war_with = AFG }
		AFG = { NOT = { has_government = fascism } }
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "AFG" value = 50 }
	ai_strategy = { type = support id = "AFG" value = 150 }
	ai_strategy = { type = send_volunteers_desire id = "AFG" value = 20 }
}

ROM_support_bosnian_against_rsk = {
	allowed = { original_tag = ROM }
	enable = {
		BOS = {
			has_country_flag = defeat_all_serbs
			OR = {
				has_idea = BOS_EUFOR
				has_idea = BOS_SFOR
			}
		}
		RSK = { has_war_with = BOS }
		has_idea = bos_sfor_vol_troops
	}
	abort_when_not_enabled = yes

	ai_strategy = {
		type = send_volunteers_desire
		id = BOS
		value = 200
	}
	ai_strategy = {
		type = befriend
		id = BOS
		value = 200
	}
	ai_strategy = {
		type = support
		id = BOS
		value = 200
	}
}
ROM_support_mlv = {
	allowed = { original_tag = SOV }
	enable = {
		OR = {
			MLV = { is_subject_of = ROM }
			MLV = { is_subject = no }
		}
		MLV = {
			has_war = yes
		}
	}
	abort_when_not_enabled = yes
	ai_strategy = {
		type = send_volunteers_desire
		id = MLV
		value = 1500
	}
}