AFRICA_african_union = {
	allowed = { is_african_nation = yes }
	enable = {
		is_african_nation = yes
		has_idea = AU_member
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "AFR" value = 25 }
	ai_strategy = { type = befriend id = "AGF" value = 25 }
	ai_strategy = { type = befriend id = "AGL" value = 25 }
	ai_strategy = { type = befriend id = "ALG" value = 25 }
	ai_strategy = { type = befriend id = "BAL" value = 25 }
	ai_strategy = { type = befriend id = "BEN" value = 25 }
	ai_strategy = { type = befriend id = "BFA" value = 25 }
	ai_strategy = { type = befriend id = "BOT" value = 25 }
	ai_strategy = { type = befriend id = "BUR" value = 25 }
	ai_strategy = { type = befriend id = "CAB" value = 25 }
	ai_strategy = { type = befriend id = "CAM" value = 25 }
	ai_strategy = { type = befriend id = "CAR" value = 25 }
	ai_strategy = { type = befriend id = "CDI" value = 25 }
	ai_strategy = { type = befriend id = "CHA" value = 25 }
	ai_strategy = { type = befriend id = "CNG" value = 25 }
	ai_strategy = { type = befriend id = "CNR" value = 25 }
	ai_strategy = { type = befriend id = "COM" value = 25 }
	ai_strategy = { type = befriend id = "CSM" value = 25 }
	ai_strategy = { type = befriend id = "CYR" value = 25 }
	ai_strategy = { type = befriend id = "DAR" value = 25 }
	ai_strategy = { type = befriend id = "DJI" value = 25 }
	ai_strategy = { type = befriend id = "DRC" value = 25 }
	ai_strategy = { type = befriend id = "EGU" value = 25 }
	ai_strategy = { type = befriend id = "EGY" value = 25 }
	ai_strategy = { type = befriend id = "ERI" value = 25 }
	ai_strategy = { type = befriend id = "ETH" value = 25 }
	ai_strategy = { type = befriend id = "FEZ" value = 25 }
	ai_strategy = { type = befriend id = "FNC" value = 25 }
	ai_strategy = { type = befriend id = "GAB" value = 25 }
	ai_strategy = { type = befriend id = "GAH" value = 25 }
	ai_strategy = { type = befriend id = "GAM" value = 25 }
	ai_strategy = { type = befriend id = "GNA" value = 25 }
	ai_strategy = { type = befriend id = "GNC" value = 25 }
	ai_strategy = { type = befriend id = "GUB" value = 25 }
	ai_strategy = { type = befriend id = "GUI" value = 25 }
	ai_strategy = { type = befriend id = "HOR" value = 25 }
	ai_strategy = { type = befriend id = "JUB" value = 25 }
	ai_strategy = { type = befriend id = "KEN" value = 25 }
	ai_strategy = { type = befriend id = "LBA" value = 25 }
	ai_strategy = { type = befriend id = "LES" value = 25 }
	ai_strategy = { type = befriend id = "LIB" value = 25 }
	ai_strategy = { type = befriend id = "LOG" value = 25 }
	ai_strategy = { type = befriend id = "LUR" value = 25 }
	ai_strategy = { type = befriend id = "MAD" value = 25 }
	ai_strategy = { type = befriend id = "MAL" value = 25 }
	ai_strategy = { type = befriend id = "MAU" value = 25 }
	ai_strategy = { type = befriend id = "MLC" value = 25 }
	ai_strategy = { type = befriend id = "MLW" value = 25 }
	ai_strategy = { type = befriend id = "MOR" value = 25 }
	ai_strategy = { type = befriend id = "MOZ" value = 25 }
	ai_strategy = { type = befriend id = "NAM" value = 25 }
	ai_strategy = { type = befriend id = "NGR" value = 25 }
	ai_strategy = { type = befriend id = "NIG" value = 25 }
	ai_strategy = { type = befriend id = "PUN" value = 25 }
	ai_strategy = { type = befriend id = "RCD" value = 25 }
	ai_strategy = { type = befriend id = "RWA" value = 25 }
	ai_strategy = { type = befriend id = "SAF" value = 25 }
	ai_strategy = { type = befriend id = "SAO" value = 25 }
	ai_strategy = { type = befriend id = "SEL" value = 25 }
	ai_strategy = { type = befriend id = "SEN" value = 25 }
	ai_strategy = { type = befriend id = "SEY" value = 25 }
	ai_strategy = { type = befriend id = "SHA" value = 25 }
	ai_strategy = { type = befriend id = "SHB" value = 25 }
	ai_strategy = { type = befriend id = "SIE" value = 25 }
	ai_strategy = { type = befriend id = "SML" value = 25 }
	ai_strategy = { type = befriend id = "SNA" value = 25 }
	ai_strategy = { type = befriend id = "SOM" value = 25 }
	ai_strategy = { type = befriend id = "SRF" value = 25 }
	ai_strategy = { type = befriend id = "SSU" value = 25 }
	ai_strategy = { type = befriend id = "STH" value = 25 }
	ai_strategy = { type = befriend id = "SUD" value = 25 }
	ai_strategy = { type = befriend id = "SWA" value = 25 }
	ai_strategy = { type = befriend id = "SWS" value = 25 }
	ai_strategy = { type = befriend id = "TIE" value = 25 }
	ai_strategy = { type = befriend id = "TNZ" value = 25 }
	ai_strategy = { type = befriend id = "TOG" value = 25 }
	ai_strategy = { type = befriend id = "TRP" value = 25 }
	ai_strategy = { type = befriend id = "TUA" value = 25 }
	ai_strategy = { type = befriend id = "TUN" value = 25 }
	ai_strategy = { type = befriend id = "UGA" value = 25 }
	ai_strategy = { type = befriend id = "UNI" value = 25 }
	ai_strategy = { type = befriend id = "VER" value = 25 }
	ai_strategy = { type = befriend id = "ZAM" value = 25 }
	ai_strategy = { type = befriend id = "ZIM" value = 25 }
}

AFRICA_likes_nigeria = {
	allowed = { is_african_nation = yes }
	enable = {
		is_african_nation = yes
		NOT = { original_tag = NIG }
		NIG = {
			NOT = { has_government = fascism }
		}
	}
	abort_when_not_enabled = yes
	ai_strategy = { type = befriend id = "NIG" value = 50 }
}

AFRICA_hate_nigerian_caliphate = {
	allowed = { is_african_nation = yes }
	enable = {
		is_african_nation = yes
		NOT = { original_tag = NIG }
		NIG = {
			has_government = fascism
		}
	}
	abort_when_not_enabled = yes
	ai_strategy = { type = befriend id = "NIG" value = -100 }
	ai_strategy = { type = contain id = "NIG" value = 100 }
	ai_strategy = { type = antagonize id = "NIG" value = 50 }
}