USA_support_haiti = {
	allowed = { original_tag = USA }
	enable = {
		country_exists = HAI
		has_country_flag = USA_intervene_in_haiti
		HAI = { has_civil_war = yes }
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "HAI" value = 50 }
	ai_strategy = { type = support id = "HAI" value = 150 }
	ai_strategy = { type = send_volunteers_desire id = "HAI" value = 50 }
}

FRA_support_haiti = {
	allowed = { original_tag = FRA }
	enable = {
		country_exists = HAI
		has_country_flag = FRA_intervening_in_haiti
		HAI = { has_civil_war = yes }
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "HAI" value = 50 }
	ai_strategy = { type = support id = "HAI" value = 150 }
	ai_strategy = { type = send_volunteers_desire id = "HAI" value = 50 }
}

HAI_hate_dominican_republic = {
	allowed = { original_tag = HAI }
	enable = {
		country_exists = DOM
		HAI = {
			is_subject = no
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = antagonize id = "DOM" value = 50 }
	ai_strategy = { type = contain id = "DOM" value = 50 }
}
