#Written by Hiddengearz, Improved by KyCb(rockon)

##Fuck Terroists
hate_terrorists = {
	allowed = {
		NOT = {
			original_tag = AQY
			original_tag = TAL
			original_tag = TTP
			original_tag = NUS
		}
	}
	enable = {
		has_opinion_modifier = AlQaida_Are_Terrorists
		NOT = {
			original_tag = AQY
			original_tag = TAL
			original_tag = TTP
			original_tag = NUS
		}
	}
	abort = {
		NOT = { has_opinion_modifier = AlQaida_Are_Terrorists }
	}
	ai_strategy = { type = antagonize id = "AQY" value = 100 }
	ai_strategy = { type = antagonize id = "TAL" value = 100 }
	ai_strategy = { type = antagonize id = "TTP" value = 100 }
	ai_strategy = { type = antagonize id = "NUS" value = 100 }
	ai_strategy = { type = antagonize id = "SHB" value = 100 }
	ai_strategy = { type = contain id = "AQY" value = 100 }
	ai_strategy = { type = contain id = "TAL" value = 100 }
	ai_strategy = { type = contain id = "TTP" value = 100 }
	ai_strategy = { type = contain id = "NUS" value = 100 }
	ai_strategy = { type = contain id = "SHB" value = 100 }
	ai_strategy = { type = alliance id = "AQY" value = -100 }
	ai_strategy = { type = alliance id = "TAL" value = -100 }
	ai_strategy = { type = alliance id = "TTP" value = -100 }
	ai_strategy = { type = alliance id = "NUS" value = -100 }
	ai_strategy = { type = alliance id = "SHB" value = -100 }
}

everyone_hates_isis = {
	enable = {
		has_opinion_modifier = ISIS_Are_Terrorists
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = antagonize id = "ISI" value = 100 }
	ai_strategy = { type = contain id = "ISI" value = 100 }
	ai_strategy = { type = alliance id = "ISI" value = -100 }
}
