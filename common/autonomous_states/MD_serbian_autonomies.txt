# Author(s): LordBogdanoff

# Autonomous Province Of Serbia
autonomy_state = {
	id = autonomy_autonomous_province

	default = no
	is_puppet = no
	use_overlord_color = yes
	min_freedom_level = 0.0
	manpower_influence = 0.1

	rule = {
		can_not_declare_war = yes
		can_decline_call_to_war = no
		units_deployed_to_overlord = yes
		can_send_volunteers = no
	}

	modifier = {
		cic_to_overlord_factor = 0
		mic_to_overlord_factor = 0.75
		can_master_build_for_us = 1
		autonomy_manpower_share = 0.1
		extra_trade_to_overlord_factor = 0.8
		overlord_trade_cost_factor = -0.8
		research_sharing_per_country_bonus_factor = -0.8
	}

	ai_subject_wants_higher = {
		factor = 0.0
	}

	ai_overlord_wants_lower = {
		factor = 0.0
	}

	ai_overlord_wants_garrison = {
		always = yes
	}

	allowed = {
		OVERLORD = {
			original_tag = SER
		}
		original_tag = VOJ
	}

	can_take_level = {
		always = no
	}

	can_lose_level = {
		always = no
	}
}
# Subject Federation Of Serbia
autonomy_state = {
	id = autonomy_autonomous_province1

	default = no
	is_puppet = no
	use_overlord_color = yes
	min_freedom_level = 0.0
	manpower_influence = 0.1

	rule = {
		can_not_declare_war = yes
		can_decline_call_to_war = no
		units_deployed_to_overlord = yes
		can_send_volunteers = yes
	}

	modifier = {
		cic_to_overlord_factor = 0.0
		mic_to_overlord_factor = 0.0
		can_master_build_for_us = 1
		autonomy_manpower_share = 1.00
		extra_trade_to_overlord_factor = 0.2
		overlord_trade_cost_factor = -0.2
		research_sharing_per_country_bonus_factor = -0.2
	}

	ai_subject_wants_higher = {
		factor = 0.0
	}

	ai_overlord_wants_lower = {
		factor = 0.0
	}

	ai_overlord_wants_garrison = {
		always = yes
	}

	allowed = {
		OVERLORD = {
			original_tag = SER
		}
		original_tag = VOJ
		original_tag = MNT
	}

	can_take_level = {
		always = no
	}

	can_lose_level = {
		always = no
	}
}

# Balkan Federation
autonomy_state = {
	id = autonomy_balkan_federation

	default = no
	is_puppet = no
	use_overlord_color = yes
	min_freedom_level = 0.0
	manpower_influence = 0.1

	rule = {
		can_not_declare_war = yes
		can_decline_call_to_war = no
		units_deployed_to_overlord = yes
		can_send_volunteers = yes
	}

	modifier = {
		cic_to_overlord_factor = 0
		mic_to_overlord_factor = 0.75
		can_master_build_for_us = 1
		autonomy_manpower_share = 0.1
		extra_trade_to_overlord_factor = 0.8
		overlord_trade_cost_factor = -0.8
		research_sharing_per_country_bonus_factor = -0.8
	}

	ai_subject_wants_higher = {
		factor = 0.0
	}

	ai_overlord_wants_lower = {
		factor = 0.0
	}

	ai_overlord_wants_garrison = {
		always = yes
	}

	allowed = {
		OVERLORD = { 
			original_tag = SER
		}
		OR = {
			original_tag = CRO
			original_tag = BUL
			original_tag = ROM
			original_tag = SLV
			original_tag = BOS
			original_tag = RSK
			original_tag = MON
			original_tag = KOS
			original_tag = FYR
			original_tag = ALB
			original_tag = GRE
			original_tag = MNT
		}
		has_country_flag = balkan_subject_agree
	}

	can_take_level = { always = no }
	can_lose_level = { always = no }
}

