# Author(s): DROID
# 51st State (Iraq)
autonomy_state = {
	id = autonomy_fifty_first_state_iraq

	default = no
	is_puppet = no
	min_freedom_level = 0.85
	manpower_influence = 0.3
	use_overlord_color = yes

	rule = {
		can_not_declare_war = yes
		can_decline_call_to_war = yes
		can_access_market = no
		can_join_factions = no
		can_send_volunteers = yes
	}

	modifier = {
		cic_to_overlord_factor = 0.25
		mic_to_overlord_factor = 0.40
		can_master_build_for_us = 0
		autonomy_manpower_share = 0.1
		extra_trade_to_overlord_factor = 0.25
		overlord_trade_cost_factor = -0.25
		research_sharing_per_country_bonus_factor = -0.2
	}


	ai_subject_wants_higher = { factor = 0.0 }
	ai_overlord_wants_lower = { factor = 0.3 }
	ai_overlord_wants_garrison = { always = yes }

	allowed = {
		OVERLORD = {
			original_tag = USA
		}
	}

	can_take_level = { always = no }
	can_lose_level = { always = no }
}
