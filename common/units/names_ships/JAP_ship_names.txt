﻿##### JAPAN NAME LISTS #####
JAP_CV_MODERN = {
	name = NAME_THEME_HISTORICAL_CARRIERS

	for_countries = { JAP }

	type = ship
	ship_types = { carrier helicopter_operator }	

	prefix = "JDS "		

	fallback_name = "Kubo %d"					

	unique = {											
		"Hosho" "Akagi" "Kaga" "Ryujo" "Soryu" "Hiryu" "Shokaku" "Zuikaku" "Zuiho" "Shoho" "Jun'yo" "Hiyo" "Unyo" "Chuyo" "Taiyo" "Kaiyo" "Akitsu Maru" "Nigitsu Maru" "Ryuho" "Taiho" "Chiyoda" "Chihotse" "Amagi" "Unryu"
		"Katsuragi" "Kasagi" "Aso" "Ikoma" "Shin'yo" "Shinano" "Shimane Maru" "Otakisan Maru" "Yamashio Maru" "Chigusa Maru" "Kumano Maru"
	}
}
JAP_BB_MODERN = {
	name = NAME_THEME_HISTORICAL_BB

	for_countries = { JAP }

	type = ship
	ship_types = { battleship }	

	prefix = "JDS "		

	fallback_name = "Senkan %d"					

	unique = {											
		"Kotetsu" "Fuso" "Chin'en" "Fuji" "Yashima" "Shikishima" "Hatsuse" "Asahi" "Mikasa" "Iki" "Tango" "Sagami" "Suwo" "Hizen" "Iwami" "Mishima" "Okinoshima" "Katori" "Kashima" "Satsuma" "Aki" "Kawachi" "Settsu" "Kongo"
		"Haruna" "Hiei" "Kirishima" "Fuso" "Yamashiro" "Ise" "Hyuga" "Nagato" "Mutsu" "Tosa" "Kaga" "Yamato" "Mushashi" "Shinano"
	}
}

JAP_BC_MODERN = {
	name = NAME_THEME_HISTORICAL_BC

	for_countries = { JAP }

	type = ship
	ship_types = { battle_cruiser }	

	prefix = "JDS "		

	fallback_name = "Senkan %d"					

	unique = {											
		"Tsukuma" "Ikoma" "Ibuki" "Kurama" "Kongo" "Haruna" "Hiei" "Kirishima" "Amagi" "Akagi" "Atago" "Takao" "Tosa" "Kaga"
	}
}

JAP_CA_MODERN = {
	name = NAME_THEME_HISTORICAL_CRUISER

	for_countries = { JAP }

	type = ship
	ship_types = { cruiser }	

	prefix = "JDS "		

	fallback_name = "Kuruza %d"					

	unique = {											
		"Saien" "Izumi" "Naniwa" "Takachiho" "Unebi" "Yaeyama" "Chiyoda" "Chishima" "Itsukushima" "Matsushima" "Hashidate" "Akitsushima" "Yoshino" "Suma" "Akashi" "Takasago" "Kasagi" "Chitose" "Niitaka" "Tsushima" "Otowa" "Tone"
		"Chikuma" "Hirado" "Yahagi" "Tsugaru" "Soya" "Suzuya" "Asama" "Tokiwa" "Izumo" "Iwate" "Yakumo" "Azuma" "Kasuga" "Nisshin" "Aso" "Yodo" "Mogami" "Ioshima" "Yashoshima" "Tatsuta" "Tenryu" "Kuma" "Tama" "Kitakami" "Kiso"
		"Oi" "Nagara" "Natori" "Kinu" "Yura" "Isuzu" "Abukuma" "Yubari" "Sendai" "Jintsu" "Naka" "Furutaka" "Kako" "Aoba" "Kinusaga" "Myoko" "Nachi" "Haguro" "Ashigara" "Takoa" "Atago" "Chokai" "Maya" "Mogami" "Mikuma" "Suzuya"
		"Kumano" "Tone" "Chikuma" "Ibuki" "Katori" "Kashima" "Kashii" "Kashiwara" "Agano" "Noshiro" "Yahagi" "Sakawa" "Oyodo" "Niyodo"
	}
}

JAP_DD_MODERN = {
	name = NAME_THEME_MODERN_DESTROYERS

	for_countries = { JAP }

	type = ship
	ship_types = { destroyer stealth_destroyer }	

	prefix = "JDS "		

	fallback_name = "Kuchiku-kan %d"					

	unique = {											
		"Haruna" "Hiei" "Shirane" "Kurama" "Huyga" "Ise" "Izumo" "Kaga" "Amatsukaze" "Tachikaze" "Asakaze" "Sawakaze" "Hatakaze" "Shimakaze" "Kongo" "Kirishima" "Myoko" "Chokai" "Atago" "Ashigara" "Maya" "Haguro" "Ariake"
		"Yugure" "Harukaze" "Yukikaze" "Akizuki" "Teruzuki" "Hatsuyuki" "Shirayuki" "Mineyuki" "Sawayuki" "Hamayuki" "Isoyuki" "Haruyuki" "Yamayuki" "Matsuyuki" "Setoyuki" "Asayuki" "Shimayuki" "Asagiri" "Yamagiri" "Yugiri"
		"Amagiri" "Hamagiri" "Setogiri" "Sawagiri" "Umigiri" "Murasame" "Harusame" "Yudachi" "Kirisame" "Inazuma" "Samidare" "Ikazuchi" "Akebono" "Ariake" "Takanami" "Onami" "Makinami" "Sazanami" "Suzunami" "Suzutzuki" "Fuyuzuki"
		"Asahi" "Shiranui" "Murasame" "Yudachi" "Harusame" "Takatsuki" "Kikuzuki" "Mochizuki" "Nagatsuki" "Ayanami" "Isonami" "Uranami" "Shikinami" "Yamagumo" "Makigumo" "Asagumo" "Aokumo" "Akigumo" "Yugumo" "Minegumo" "Natsugumo"
	}
}

JAP_FRIG_MODERN = {
	name = NAME_THEME_MODERN_FRIGATES

	for_countries = { JAP }

	type = ship
	ship_types = { frigate }	

	prefix = "JDS "		

	fallback_name = "Furigeto %d"					

	unique = {											
		"Mogami" "Kumano" "Noshiro" "Mikuma" "Yahagi" "Akano" "Kashi" "Tochi" "Kiri" "Buna" "Tsuge" "Matsu" "Momi" "Kaede" "Shii" "Kaya" "Sugi" "Kusu" "Sakura" "Ume" "Nara" "Nire" "Maki" "Keyaki" "Kanko Maru" "Kanrin Maru"
		"Choyo" "Kaiyo Maru" "Kaiten" "Banryu" "Chogei" "Shinsoku" "Mikaho" "Yoshun" "Kasuga" "Chiyodataga" "Hiryu" "Teibo" "Ryujo" "Unyo" "Nisshin" "Takao" "Moshun"
	}
}

JAP_CORV_MODERN = {
	name = NAME_THEME_MODERN_CORVETTES

	for_countries = { JAP }

	type = ship
	ship_types = { corvette }	

	prefix = "JDS "		

	fallback_name = "Korubetto %d"					

	unique = {											
		"Hayabusa" "Wakataka" "Otaka" "Kumataka" "Umitaka" "Shirataka" "Kongo" "Hiei" "Amagi" "Tsukushi" "Kaimon" "Tenryu" "Katsuragi" "Yamato" "Musashi" "Heien" "Maya" "Akagi" "Atago" "Chokai" "Oshima" "Banjo" "Uji" "Shirataka"
		"Kasasagi" "Manazuru" "Chidori" "Aotaka" "Kari" "Hato" "Tsubame" "Hibari" "Kiji" "Hashitaka" "Sagi" "Uzura" "Kamome" "Otori" "Chidori" "Hatsukari" "Manazuru" "Tomozuru" "Hato" "Hiyodori" "Sumida" "Fushimi" "Ataka" "Okitsu"
		"Karatsu" "Katada" "Fushimi" "Sumida" "Hozu" "Katata" "Futami" "Atami" "Seta" "Kotaka" "Toba" "Hira" "Tatara" "Suma" "Kozakura" "Itsukushima" "Yaeyama"
	}
}

JAP_SUB_MODERN = {
	name = NAME_THEME_MODERN_SUBMARINES

	for_countries = { JAP }

	type = ship
	ship_types = { missile_submarine attack_submarine }	

	prefix = "JDS "		

	fallback_name = "Sensuikan %d"					

	unique = {											
		"Kuroshio" "Oyashio" "Hayashio" "Wakashio" "Natsushio" "Fuyushio" "Oshio" "Asashio" "Harushio" "Michishio" "Arashio" "Uzushio" "Makishio" "Isoshio" "Narushio" "Kuroshio" "Takashio" "Yaeshio" "Yushio" "Mochishio" "Setoshio"
		"Okishio" "Nadashio" "Hamashio" "Akishio" "Takeshio" "Yukishio" "Sachishio" "Soryu" "Unruy" "Hakuryu" "Kenryu" "Zuiryu" "Kokuryu" "Jinryu" "Sekiryu" "Seiryu" "Shoryu" "Oryu" "Toryu" "Taigei" "Hakugei" "Jingei" "Junsen"
		"Kou" "Otsu" "Hei" "Tei" "Kaidai" "Sentoku" "Sentaka" "Senho" "Kiraisen" "Kaichu" "Ko-hyoteki" "Kairyu" "Kaiten" "Kawasaki" 
	}
}

### THEME: JAPANESE PREFECTURES ###
JAP_PREFECTURES = {
	name = NAME_THEME_PREFECTURES

	for_countries = { JAP }

	prefix = "JDS "		

	type = ship

	unique = {
		"Yamashiro" "Kawachi" "Izumi" "Settsu" "Iga" "Ise" "Shima" "Owari" "Mikawa" "Totomi" "Suruga" "Izu" "Kai" "Sagami" "Musashi" "Awa" "Kazusa" 
		"Shimosa" "Hitachi" "Omi" "Mino" "Hida" "Shinano" "Kozuke" "Shimotsuke" "Dewa" "Mutsu" "Wakasa" "Echizen" "Kaga" "Noto" "Etchu" "Echigo" 
		"Sado" "Tanba" "Tango" "Tajima" "Inaba" "Hoki" "Izumo" "Iwami" "Oki" "Harima" "Mimasaka" "Bizen" "Bitchu" "Bingo" "Aki" "Suo" "Nagato" 
		"Kii" "Awaji" "Awa" "Sanuki" "Iyo" "Tosa" "Buzen" "Chikuzen" "Chikugo" "Hizen" "Higo" "Hyuga" "Osumi" "Satsuma" "Iki" "Tsushima" "Oshima" 
		"Shiribeshi" "Iburi" "Ishikari" "Teshio" "Kitami" "Hidaka" "Tokachi" "Kushiro" "Nemuro" "Chishima" 
		"Hokkaido" "Aichi" "Ehime" "Gunma" "Hyogo" "Ishikawa" "Iwate" "Kagawa" "Kanagawa" "Mie" "Miyagi" "Okinawa" "Shiga" "Shimane" "Tochigi" "Yamanashi"
	}
}
## THEME: JAPANESE CITIES ###
JAP_CITIES = {
	name = NAME_THEME_CITIES

	for_countries = { JAP }

	prefix = "JDS "		

	type = ship

	unique = {
		"Tokyo" "Yokohama" "Osaka" "Nagoya" "Sapporo" "Kobe" "Kyoto" "Fukuoka" "Kawasaki" "Saitama" "Hiroshima" "Sendai" "Kitakyushu" "Chiba" "Setagaya" 
		"Sakai" "Niigata" "Hamamatsu" "Shizuoka" "Sagamihara" "Nerima" "Okayama" "Oita" "Kumamoto" "Edogawa" "Adachi" "Kagoshima" "Funabashi" "Hachioji" 
		"Kawaguchi" "Himeji" "Suginami" "Itabashi" "Matsuyama" "Higashiosaka" "Utsunomiya" "Matsudo" "Nishinomiya" "Kurashiki" "Ichikawa" "Fukuyama" 
		"Amagasaki" "Kanazawa" "Nagasaki" "Koto" "Katsushika" "Yokusaka" "Toyama" "Toyota" "Takamatsu" "Machida" "Gifu" "Hirakata" "Fujisawa" "Kashiwa" 
		"Toyonaka" "Nagano" "Toyohashi" "Ichinomiya" "Wakayama" "Okazaki" "Miyazaki" "Nara" "Suita" "Takatsuki" "Shinagawa" "Asahikawa" "Iwaki" "Kochi" 
		"Takasaki" "Koriyama" "Tokorozawa" "Kawagoe" "Kita" "Akita" "Otsu" "Koshigaya" "Maebashi" "Naha" "Nakano" "Shinjuku" "Yokaichi" "Aomori" "Kurume" 
		"Kasugai" "Morioka" "Akashi" "Fukushima" "Tsu" "Shimonoseki" "Nagaoka" "Ichihara" "Hakodate" "Yao" "Ibaraki" "Fukui" "Meguro" "Kakogawa" "Tokushima" 
		"Mito" "Hiratsuka" "Toshima" "Yamagata" "Sasebo" "Fuchu" "Kure" "Hachinohe" "Saga" "Neyagawa" "Soka" "Sumida" "Fuji" "Kasukabe" "Chigasaki" "Matsumoto"   
		"Atsugi" "Yamato" "Ageo" "Takarazuka" "Chofu" "Ota" "Tsukuba" "Numazu" "Joetsu" "Shibuya" "Minato" "Kumagaya" "Isesaki" "Kishiwada" "Tottori" "Kofu" 
		"Odawara" "Suzuka" "Matsue" "Hitachi" "Bunkyo" "Arakawa" "Itami" "Nishitokyo" "Yamaguchi" "Uji"		
	}
}

JAP_WEATHER = {
	name = NAME_THEME_WEATHER

	for_countries = { JAP }

	prefix = "JDS "		

	type = ship

	unique = {
		"Kawakaze" "Tanikaze" "Minekaze" "Sawakaze" "Okikaze" "Shimakaze" "Nadakaze" "Yakaze" "Hakaze" "Shiokaze" "Akikaze" "Yukaze" "Tachikaze" "Hokaze" "Nokaze" 
		"Namikaze" "Numakaze" "Kamikaze" "Asakaze" "Harukaze" "Matsukaze" "Hatakaze" "Oite" "Hayate" "Asanagi" "Yunagi" 
		"Fubuki" "Shirayuki" "Hatsuyuki" "Miyuki" "Murakumo" "Shinonome" "Usugumo" "Shirakumo" "Isonami" "Uranami" "Ayanami" "Shikinami" 
		"Asagiri" "Yugiri" "Amagiri" "Sagiri" "Oboro" "Akebono" "Sazanami" "Ushio" "Akatsuki" "Hibiki" "Ikazuchi" "Inazuma" 
		"Shiratsuyu" "Shigure" "Murasame" "Yudachi" "Umikaze" "Samidare" "Yamakaze" "Harusame" "Suzukaze"
		"Asashio" "Arashio" "Oshio" "Michishio" "Yamagumo" "Natsugumo" "Minegumo" "Asagumo" "Arare" "Kasumi"
		"Kagero" "Shiranui" "Kuroshio" "Yukikaze" "Oyashio" "Hayashio" "Natsushio" "Hatsukaze" "Amatsukaze" "Isokaze" "Tokitsukaze" "Urakaze" "Arashi" 
		"Hagikaze" "Nowaki" "Tanikaze" "Hamakaze" "Maikaze" "Akigumo" "Yugumo" "Makigumo" "Kazagumo" "Naganami" "Makinami" "Takanami" "Onami" "Kiyonami" "Tamanami" 
		"Suzunami" "Fujinami" "Hayanami" "Hamanami" "Asashimo" "Okinami" "Kishinami" "Hayashimo" "Akishimo" "Kiyoshimo"
	}
}

JAP_NATURE = {
	name = NAME_THEME_NATURE

	for_countries = { JAP }

	prefix = "JDS "		

	type = ship

	unique = {
			"Momo" "Kashi" "Hinoki" "Yanagi" "Enoki" "Maki" "Keyaki" "Kuwa" "Tsubaki" "Nara" 
			"Momi" "Kaya" "Nashi" "Take" "Kaki" "Tsuga" "Nire" "Kuri" "Kiku" "Aoi" "Fuji" "Hagi" "Susuki" "Hishi" "Hasu" "Warabi" "Tade" "Sumire" "Tsuta" "Ashi" 
			"Yomogi" "Wakatake" "Kuretake" "Sanae" "Sarawabi" "Asagao" "Yugao" "Fuyo" "Karukaya"
			"Matsu" "Ume" "Kiri" "Sugi" "Sakura" "Kaba" "Kaede" "Yaezakura" "Yadake" "Kudzu" "Madake" "Hayaume" "Tobiume" "Wakazakura" "Yamazakura" 
			"Tachibana" "Shii" "Kusunoki" "Odake" "Hatsuzakura" "Hatsuume" "Azusa" "Mikura" "Miyake" "Awaji" "Kurahashi" "Nomi" "Chiburi" "Yashiro" 
			"Kusagaki" "Shinodake" "Shiraume" "Kashiwa" "Kigiku" "Hatsugiku" "Akane" "Shiragiku" "Chigusa" "Wakakusa" "Natsugusa" "Akikusa" "Tochi" "Nogiku" 			
		}
}

JAP_RIVER_MOUNTAIN = {
	name = NAME_THEME_RIVER_MOUNTAIN

	for_countries = { JAP }

	prefix = "JDS "		

	type = ship

	unique = {
		"Asama" "Furutaka" "Kako" "Aoba" "Kinugasa" "Myoko" "Nachi" "Haguro" "Ashigara" "Takao" "Atago" "Maya" "Chokai" "Mogami" "Mikuma" "Suzuya" 
		"Kumano" "Tone" "Chikuma" "Ibuki" "Yodo" "Hirado" "Yahagi" "Tenryu" "Tatsuta" "Kuma" "Tama" "Kitakami" "Oi" "Kiso" "Nagara" "Isuzu" "Natori" 
		"Yura" "Kinu" "Abukuma" "Yubari" "Sendai" "Jintsu" "Naka" "Agano" "Noshiro" "Yahagi" "Sakawa" "Oyodo"
		"Ayase" "Minase" "Otonase" 
	}
}