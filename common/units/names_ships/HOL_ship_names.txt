﻿##### Netherlands NAME LISTS #####

### REGULAR DESTROYER NAMES###
HOL_DD_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_DESTROYERS

	for_countries = { HOL }

	type = ship
	ship_types = { stealth_destroyer destroyer }

	prefix = "HNLMS "
	fallback_name = "DDG-%d"

	unique = {
		"Zuiderzee" "IJsselmeer" "Veerse Meer" "Waddenzee" "Markermeer" "Biesbosch" "Haringvliet" "Grevelingen" "Veluwemeer" "Gooimeer" "Amstel" "Maas" "Rijn" "Lek" "Schelde" "Merwede" "Vecht" "Waalkade" "IJ" "Gouwe" "Jan van Gent" "Fuut" "Aalscholver" "Zilvermeeuw" "Stormmeeuw" "Kokmeeuw" "Kluut" "Visdief" "Lepelaar"
	
	}
}

HOL_FRIGATES_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_FRIGATES
	for_countries = { HOL }
	type = ship
	ship_types = {
		frigate
	}
	prefix = "HNLMS "
	fallback_name = "FFG-%d"

	unique = { ## Namelist 4
		"Ooievaar" "Kraanvogel" "Flamingo" "Roerdomp" "Porseleinhoen" "Woudaap" "Blauwborst" "Roodborst" "Nachtegaal" "Lijster" "Zwaluw" "Vlaamse Gaai" "Ekster" "Reiger" "Zwaan" "Ibis" "Koekoek" "Kwikstaart" "Specht" "Uil" "Leeuwerik" "Grutto" "Tureluur" "Kievit" "Scholekster" "Plevier" "Strandloper" "Kanoet" "Eider" "Zee-eend" "Kuifeend" "Aalscholver II" "Stormmeeuw II" "Kokmeeuw II" "Kluut II" "Visdief II" "Lepelaar II" "Ooievaar II" "Kraanvogel II" "Flamingo II" "Roerdomp II" "Porseleinhoen II" "Woudaap II" "Blauwborst II" "Roodborst II" "Nachtegaal II" "Lijster II" "Zwaluw II" "Vlaamse Gaai II" "Ekster II"
	}
}

### CORVETTE NAMES ###
HOL_CORVETTE_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_CORVETTE

	for_countries = { HOL }
	prefix = "HNLMS "
	type = ship
	ship_types = { corvette }
	fallback_name = "KKG-%d"

	unique = {
		"Stern" "Kolibrie" "Huismus" "Meerkoet" "Waterhoen" "Gierzwaluw" "Havik" "Fazant" "Pauw" "Zwaardvis" "Zeehond" "Dolfijn" "Walvis" "Krokodil" "Schildpad" "Hagedis" "Kameleon" "Miereneter" "Tapir" "Leguaan" "Orang-Oetan" "Tijger" "Luipaard" "Jaguar" "Leeuw" "Neushoorn" "Olifant" "Buffel" "Giraffe" "Nijlpaard" "Salamander" "Kikker" "Pad" "Paddenstoel" "Eekhoorn" "Mol" "Bever" "Wezel" "Hermelijn" "Otter"
	}
}

HOL_SUBMARINES_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_SUBMARINES
	for_countries = { HOL }
	prefix = "HNLMS "
	type = ship
	ship_types = {
		attack_submarine missile_submarine
	}
	fallback_name = "SS-%d"
}