##### Bangladesh NAME LISTS #####

### REGULAR DESTROYER NAMES###
BAN_DD_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_DESTROYERS

	for_countries = { BAN }

	type = ship
	ship_types = { stealth_destroyer destroyer }

	prefix = "BNS "
	fallback_name = "DDG-%d"

	unique = {
		"Bijoy" "Prottoy" "Durjoy" "Mohona" "Shantinagar" "Meghna" "Turag" "Bangabandhu" "Aparajeyo" "Shurjomukhi" "Lalon" "Padma" "Jibanananda" "Birshreshtha" "Sagarika" "Nodi" "Agnishikha" "Ekattor" "Bishwajit" "Chayabani"
	}
}

BAN_FRIGATES_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_FRIGATES
	for_countries = { BAN }
	type = ship
	ship_types = {
		frigate
	}
	prefix = "BNS "
	fallback_name = "FFG-%d"

	unique = {
		"Sonar Bangla" "Muktijoddha" "Bhatiari" "Anirban" "Chitrangada" "Dhaleshwari" "Ektihaas" "Foyez" "Gourab" "Himadri" "Ishan" "Jahanara" "Karnafuli" "Lokkhi" "Manik" "Nongor" "Oporajita""Purbasha" "Quamrul" "Rupsha" "Shapla" "Titas" "Udayan" "Vasha" "Warid" "Xingho" "Yamuna" "Zorina" "Aakashlina" "Bahadur" "Chandrima" "Deshattobodhok" "Ekush" "Falguni" "Ganga""Hridoyhina" "Indrani" "Joyjatra" "Kallol" "Lohitakash"
	}
}

### CORVETTE NAMES ###
BAN_CORVETTE_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_CORVETTE

	for_countries = { BAN }
	prefix = "BNS "
	type = ship
	ship_types = { corvette }
	fallback_name = "KKG-%d"

	unique = {
		"Mrittika" "Nirmal" "Ognidut" "Prantik" "Quasar" "Rajhans" "Suryamukhi" "Taranga" "Udoy" "Vromor" "Washim" "Xatriya" "Yatri" "Zahir" "Abhijatri" "Bishal" "Chhayapath" "Drishtipat" "Eshani" "Fagun" "Gobeshona" "Harinavi" "Ityadi" "Jibontara" "Kuheli" "Lalonpalon" "Mounota" "Nirvik" "Obhijaan" "Prokriti" "Quddus" "Rongdhonu" "Shobujpata" "Tritio" "Ullas" "Voboghure" "Warish" "Xena" "Yashobati" "Zircon"
	}
}

BAN_SUBMARINES_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_SUBMARINES
	for_countries = { BAN }
	prefix = "BNS "
	type = ship
	ship_types = {
		attack_submarine missile_submarine
	}
	fallback_name = "SS-%d"
}