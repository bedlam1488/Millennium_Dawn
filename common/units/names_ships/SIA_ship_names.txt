﻿SIA_AIRCRAFT_CARRIERS_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_AIRCRAFT_CARRIERS
	for_countries = { SIA }
	type = ship
	ship_types = {
		carrier
	}
	fallback_name = "CV-%d"

	unique = {
		"Chakri Naruebet" "Sukhothai" "Ayutthaya" "Naresuan" "Thongthin" "Ramathibodi" "Taksin" "Mahidol" "Bhumibol Adulyadej" "Ananda Mahidol"
	}
}

SIA_HELICOPTER_CARRIERS_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_HELICOPTER_CARRIERS
	for_countries = { SIA }
	type = ship
	ship_types = {
		helicopter_operator
	}
	fallback_name = "LHA-%d"

	unique = {
		"Mongkut" "Chulalongkorn" "Vajiravudh" "Prajadhipok" "Ananda Mahidol" "Bhumibol Adulyadej" "Vajiralongkorn" "Maha Vajiralongkorn"
	}
}

SIA_DESTROYERS_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_DESTROYERS
	for_countries = { SIA }
	type = ship
	ship_types = {
		stealth_destroyer
		destroyer
	}
	fallback_name = "DDG-%d"

	unique = { ## Namelist 3
		"Chulabhorn" "Suthida" "Sirindhorn" "Ubol Ratana" "Sirikit" "Galyani Vadhana" "Bajrakitiyabha" "Srirasm" "Dusit" "Krom Luang Voravan"
		"Chumphon" "Krom Luang Chumphon" "Kamphaengphet" "Phitsanulok" "Khorat" "Mahasarakham" "Surin" "Si Sa Ket" "Yasothon" "Amnat Charoen"
	}
}

SIA_FRIGATES_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_FRIGATES
	for_countries = { SIA }
	type = ship
	ship_types = {
		frigate
	}
	fallback_name = "FFG-%d"

	unique = { ## Namelist 4
		"Ubon Ratchathani" "Yasothon" "Buri Ram" "Nakhon Ratchasima" "Kalasin" "Sisaket" "Chaiyaphum" "Lopburi" "Sara Buri" "Nonthaburi" "Samut Prakan" "Nakhon Nayok" "Prachinburi" "Chachoengsao" "Sakaeo" "Chonburi" "Rayong" "Chanthaburi" "Trat" "Chumphon" "Ranong" "Phangnga" "Phuket" "Krabi" "Trang" "Satun" "Narathiwat" "Pattani" "Yala" "Songkhla"
	}
}

### CORVETTE NAMES ###
SIA_CORVETTE_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_CORVETTE

	for_countries = { SIA }

	type = ship
	ship_types = { corvette }
	fallback_name = "KKG-%d"

	unique = {
		"Phra Nakhon" "Chao Phraya" "Sukhothai" "Ayutthaya" "Rattanakosin" "Dvaravati" "Lan Na" "Nan" "Ligor" "Srivijaya" "Naresuan" "Thonburi" "Thammasat" "Kasetsart" "Mahidol" "Chulalongkorn" "Silpakorn" "Ramkhamhaeng" "Mongkut" "Siam" "Angkor" "Khmer" "Pattani" "Nakhon Si Thammarat" "Chiang Mai" "Chiang Rai" "Lampang" "Phitsanulok" "Suthep" "Doi Inthanon" "Khao Yai" "Kaeng Krachan" "Erawan" "Bang Pa-In" "Saphan Mon" "Saphan Taksin" "Saphan Phut" "Saphan Daeng" "Saphan Khwai" "Saphan Lek" "Saphan Pla" "Saphan Pla Nakhon" "Saphan Khao" "Namtok Phlio" "Namtok Sai Yok" "Namtok Erawan" "Namtok Huai Mae Khamin" "Namtok Chet Sao Noi" "Namtok Tad Ton" "Chamlong" "Chatchai" "Chavalit" "Chinawat" "Chonlasit" "Chuan" "Chulanont" "Chumporn"
	}
}

SIA_SUBMARINES_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_SUBMARINES
	for_countries = { SIA }
	type = ship
	ship_types = {
		attack_submarine missile_submarine
	}
	fallback_name = "SS-%d"
}