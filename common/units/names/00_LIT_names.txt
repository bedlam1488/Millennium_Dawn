LIT = {
	air_wing_names_template = AIR_WING_NAME_LIT_FALLBACK

	#Air wings can only be named through archetype
	small_plane_airframe = {
		prefix = ""
		generic = { "Naikintuvas" }
		generic_pattern = AIR_WING_NAME_LIT_GENERIC
	}
	small_plane_strike_airframe = {
		prefix = ""
		generic = { "Naikintuvas" }
		generic_pattern = AIR_WING_NAME_LIT_GENERIC

	}
	small_plane_sucide_airframe = {
		prefix = ""
		generic = { "Naikintuvas" }
		generic_pattern = AIR_WING_NAME_LIT_GENERIC
		unique = {
		}
	}

	small_plane_naval_bomber_airframe = {
		prefix = ""
		generic = { "Naikintuvas" }
		generic_pattern = AIR_WING_NAME_LIT_GENERIC
		unique = {
		}
	}

	small_plane_cas_airframe = {
		prefix = ""
		generic = { "Naikintuvas" }
		generic_pattern = AIR_WING_NAME_LIT_GENERIC
		unique = {
		}
	}

	cv_small_plane_airframe = {
		prefix = ""
		generic = { "Naikintuvas" }
		generic_pattern = AIR_WING_NAME_LIT_GENERIC
		unique = {
		}
	}

	cv_small_plane_strike_airframe = {
		prefix = ""
		generic = { "Naikintuvas" }
		generic_pattern = AIR_WING_NAME_LIT_GENERIC
		unique = {
		}
	}

	cv_small_plane_naval_bomber_airframe = {
		prefix = ""
		generic = { "Naikintuvas" }
		generic_pattern = AIR_WING_NAME_LIT_GENERIC
		unique = {
		}
	}

	cv_small_plane_suicide_airframe = {
		prefix = ""
		generic = { "Naikintuvas" }
		generic_pattern = AIR_WING_NAME_LIT_GENERIC
		unique = {
		}
	}

	medium_plane_airframe = {
		prefix = ""
		generic = { "Naikintuvas" }
		generic_pattern = AIR_WING_NAME_LIT_GENERIC
		unique = {
			"3 Naikintuvas" "7 Naikintuvas" "8 Naikintuvas"
		}
	}
	medium_plane_fighter_airframe = {
		prefix = ""
		generic = { "Naikintuvas" }
		generic_pattern = AIR_WING_NAME_LIT_GENERIC

	}
	medium_plane_cas_airframe = {
		prefix = ""
		generic = { "Naikintuvas" }
		generic_pattern = AIR_WING_NAME_LIT_GENERIC
	}
	medium_plane_maritime_patrol_airframe = {
		prefix = ""
		generic = { "Naikintuvas" }
		generic_pattern = AIR_WING_NAME_LIT_GENERIC
		unique = {
		}
	}
	medium_plane_suicide_airframe = {
		prefix = ""
		generic = { "Naikintuvas" }
		generic_pattern = AIR_WING_NAME_LIT_GENERIC
	}
	cv_medium_plane_airframe = {
		prefix = ""
		generic = { "Naikintuvas" }
		generic_pattern = AIR_WING_NAME_LIT_GENERIC
	}

	cv_medium_plane_fighter_airframe = {
		prefix = ""
		generic = { "Naikintuvas" }
		generic_pattern = AIR_WING_NAME_LIT_GENERIC
		unique = {
		}
	}

	cv_medium_plane_maritime_patrol_airframe = {
		prefix = ""
		generic = { "Naikintuvas" }
		generic_pattern = AIR_WING_NAME_LIT_GENERIC
		unique = {
		}
	}

	cv_medium_plane_cas_airframe = {
		prefix = ""
		generic = { "Naikintuvas" }
		generic_pattern = AIR_WING_NAME_LIT_GENERIC
		unique = {
		}
	}

	cv_medium_plane_air_transport_airframe = {
		prefix = ""
		generic = { "Naikintuvas" }
		generic_pattern = AIR_WING_NAME_LIT_GENERIC
		unique = {
		}
	}

	cv_medium_plane_scout_airframe = {
		prefix = ""
		generic = { "Naikintuvas" }
		generic_pattern = AIR_WING_NAME_LIT_GENERIC
		unique = {
		}
	}

	large_plane_airframe = {
		prefix = ""
		generic = { "Naikintuvas" }
		generic_pattern = AIR_WING_NAME_LIT_GENERIC
		unique = {
		}
	}
	large_plane_maritime_patrol_airframe = {
		prefix = ""
		generic = { "Naikintuvas" }
		generic_pattern = AIR_WING_NAME_LIT_GENERIC

	}
	large_plane_air_transport_airframe = {
		prefix = ""
		generic = { "Transporto lėktuvas" }
		generic_pattern = AIR_WING_NAME_LIT_GENERIC

	}

	large_plane_cas_airframe = {
		prefix = ""
		generic = { "Naikintuvas" }
		generic_pattern = AIR_WING_NAME_LIT_GENERIC
		unique = {
		}
	}

	large_plane_awacs_airframe = {
		prefix = ""
		generic = { "Naikintuvas" }
		generic_pattern = AIR_WING_NAME_LIT_GENERIC
		unique = {
		}
	}

	###################################

	attack_helicopter_equipment = {
		prefix = ""
		generic = { "Atakos malūnsparnis" }
		generic_pattern = AIR_WING_NAME_LIT_GENERIC

	}
	Air_UAV_equipment = {
		prefix = ""
		generic = { "Mehitamata Naikintuvas" }
		generic_pattern = AIR_WING_NAME_LIT_GENERIC

	}
}
