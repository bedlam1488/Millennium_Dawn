﻿AST_ARMY_DIVISIONS = {
	name = "Army Divisions"
	for_countries = { AST }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	fallback_name = "%d Australian Division"
}

AST_ARMY_BRIGADES = {
	name = "Army Brigades"

	for_countries = { AST }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	fallback_name = "%d Australian Brigades"
}

AST_MECHANIZED_DIVISIONS = {
	name = "Mechanized Divisions"

	for_countries = { AST }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	fallback_name = "%d Royal Australian Mechanized"
}

AST_ARMOURED_DIVISIONS = {
	name = "Armoured Divisions"

	for_countries = { AST }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	fallback_name = "%d Royal Australian Armoured"
}

AST_AIR_CAV_BRIGADES = {
	name = "Air Assault Brigades"

	for_countries = { AST }

	division_types = { "L_Air_assault_Bat" "Arm_Air_assault_Bat" "L_Air_Inf_Bat" "Mot_Air_Inf_Bat"  }

	fallback_name = "%d Royal Air Cavalry"
}


AST_MARINE_BRIGADES = {
	name = "Marine Brigades"

	for_countries = { AST }

	division_types = { "L_Marine_Bat" "Mot_Marine_Bat" "Mech_Marine_Bat" "Arm_Marine_Bat" }

	fallback_name = "%d Amphibious Brigades"
}

AST_SPEC_FORCES_BRIGADES = {
	name = "Special Forces Groups"

	for_countries = { AST }

	division_types = { "Special_Forces" }

	fallback_name = "%d Australian Air Service Regiment"
}

AST_REGIONAL_BRIGADES = {
	name = "Regional Defence Forces"

	for_countries = { AST }

	division_types = { "Militia_Bat" "Mot_Militia_Bat" }

	fallback_name = "%d Regional Defence Forces"
}
