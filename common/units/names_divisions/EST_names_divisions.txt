﻿EST_ARMY_DIVISIONS = {
	name = "Army Divisions"

	for_countries = { EST }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	link_numbering_with = { "EST_ARMOURED_DIVISIONS" }

	fallback_name = "%d Jalaväediviisi"
}

EST_ARMY_BRIGADES = {
	name = "Army Brigades"
	for_countries = { EST }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	fallback_name = "%d Jalaväebrigaad"
}

EST_MECHANIZED_DIVISIONS = {
	name = "Mechanized Division"
	for_countries = { EST }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	fallback_name = "%s Mehhaniseeritud diviisi"

}

EST_ARMOURED_DIVISIONS = {
	name = "Armoured Divisions"

	for_countries = { EST }

	division_types = { "armor_Bat" }

	link_numbering_with = { "EST_ARMY_DIVISIONS" }

	fallback_name = "%d Tankidiviisi"
}


EST_AIR_CAV_BRIGADES = {
	name = "Air Assault Brigades"

	for_countries = { EST }

	division_types = { "L_Air_assault_Bat" "Arm_Air_assault_Bat" "L_Air_Inf_Bat" "Mot_Air_Inf_Bat"  }

	fallback_name = "%d Õhudessantbrigaadid"

}

EST_MAR_BRIGADES = {
	name = "Amphibious Rapid Deployment Brigades"

	for_countries = { EST }

	division_types = { "L_Marine_Bat" "Mot_Marine_Bat" "Mech_Marine_Bat" "Arm_Marine_Bat" }

	fallback_name = "%d Amfiibide kiirabibrigaad"
}

EST_SPEC_FORCES_BRIGADES = {
	name = "Special Forces Groups"

	for_countries = { EST }

	division_types = { "Special_Forces" }

	fallback_name = "%d Erivägede rühmad"
}

EST_MIL_BRIGADES = {
	name = "Regional Defence Brigades"

	for_countries = { EST }

	division_types = { "Militia_Bat" "Mot_Militia_Bat" }

	fallback_name = "%d Piirkondlikud kaitsebrigaadid"
}
