﻿KYR_ARMY_DIVISIONS = {
	name = "Army Divisions"

	for_countries = { KYR }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	link_numbering_with = { "KYR_ARMOURED_DIVISIONS" }

	fallback_name = "%d Jöö askerler diviziyasi"
}

KYR_ARMY_BRIGADES = {
	name = "Army Brigades"
	for_countries = { KYR }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	fallback_name = "%d Jöö askerler brigadasi"

	ordered = {
		68 = { "%d-ajrym too atkychtar brigadasy" }
	}
}

KYR_MECHANIZED_DIVISIONS = {
	name = "Mechanized Division"
	for_countries = { KYR }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	fallback_name = "%d Mehanizatsiyalaştirilgan belum"

	ordered = {
		2 = { "%d-Ajrykcha gvardiyalyk motoatkychtar brigadasy" }
		8 = { "%d-Gvardiyalyk motoatkychtar diviziyasy" }
	}
}

KYR_ARMOURED_DIVISIONS = {
	name = "Armoured Divisions"

	for_countries = { KYR }

	division_types = { "armor_Bat" }

	link_numbering_with = { "KYR_ARMY_DIVISIONS" }

	fallback_name = "%d Tank diviziyasi"
}


KYR_AIR_CAV_BRIGADES = {
	name = "Air Assault Brigades"

	for_countries = { KYR }

	division_types = { "L_Air_assault_Bat" "Arm_Air_assault_Bat" "L_Air_Inf_Bat" "Mot_Air_Inf_Bat"  }

	fallback_name = "%d Aba-desanttik brigadalar"

}

KYR_MAR_BRIGADES = {
	name = "Amphibious Rapid Deployment Brigades"

	for_countries = { KYR }

	division_types = { "L_Marine_Bat" "Mot_Marine_Bat" "Mech_Marine_Bat" "Arm_Marine_Bat" }

	fallback_name = "%d Amfibiya tez jardam brigadasi"
}

KYR_SPEC_FORCES_BRIGADES = {
	name = "Special Forces Groups"

	for_countries = { KYR }

	division_types = { "Special_Forces" }

	fallback_name = "%d Atayin küçtördün toptoru"

	ordered = {
		24 = { "'Ilbirs' atajyn bagyttagy %d-otryad" }
		25 = { "%d-Specnaz brigadasy 'Skorpion'" }
	}
}

KYR_MIL_BRIGADES = {
	name = "Regional Defence Brigades"

	for_countries = { KYR }

	division_types = { "Militia_Bat" "Mot_Militia_Bat" }

	fallback_name = "%d Aymaktik korgonuu brigadalari"
}