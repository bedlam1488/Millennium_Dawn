﻿JAP_ARMY_DIVISIONS = {
	name = "Army Divisions"

	for_countries = { JAP }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	link_numbering_with = { "JAP_ARMOURED_DIVISIONS" }

	fallback_name = "%d Shidan"
}

JAP_ARMY_BRIGADES = {
	name = "Army Brigades"

	for_countries = { JAP }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	fallback_name = "%d Ryodan"
}

JAP_ARMOURED_DIVISIONS = {
	name = "Armoured Divisions"

	for_countries = { JAP }

	division_types = { "armor_Bat" }

	link_numbering_with = { "JAP_ARMY_DIVISIONS" }

	fallback_name = "%d Kikoshidan"
}

JAP_PARA_BRIGADES = {
	name = "Airborne Brigades"

	for_countries = { JAP }

	division_types = { "L_Air_Inf_Bat" "Mot_Air_Inf_Bat" }

	fallback_name = "%d Kutei Dan"
}

JAP_AIR_CAV_BRIGADES = {
	name = "Air Assault Brigades"

	for_countries = { JAP }

	division_types = { "L_Air_assault_Bat" "Arm_Air_assault_Bat" }

	fallback_name = "%d Kushu Ryodan"
}

JAP_MAR_BRIGADES = {
	name = "Amphibious Rapid Deployment Brigades"

	for_countries = { JAP }

	division_types = { "L_Marine_Bat" "Mot_Marine_Bat" "Mech_Marine_Bat" "Arm_Marine_Bat" }

	fallback_name = "%d Suirikukidodan"
}

JAP_SPEC_FORCES_BRIGADES = {
	name = "Special Forces Groups"

	for_countries = { JAP }

	division_types = { "Special_Forces" }

	fallback_name = "%d Tokushusakusengun"
}

JAP_MIL_BRIGADES = {
	name = "Regional Defence Brigades"

	for_countries = { JAP }

	division_types = { "Militia_Bat" "Mot_Militia_Bat" }

	fallback_name = "%d Chiho Boei Ryodan"
}