﻿ARM_ARMY_DIVISIONS = {
	name = "Army Corps"

	for_countries = { ARM }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	fallback_name = "%d. Banaki Korpus"
}

ARM_MECHANIZED_DIVISIONS = {
	name = "Motor Rifle Brigade"
	for_countries = { ARM }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	fallback_name = "%s. Sharzhich'ayin Hradzgayin Brigad"

}

ARM_ARMOURED_DIVISIONS = {
	name = "Armoured Divisions"

	for_countries = { ARM }

	division_types = { "armor_Bat" }

	fallback_name = "%d. Zrahapat Divizianer"
}


ARM_AIR_CAV_BRIGADES = {
	name = "Air Assault Brigades"

	for_countries = { ARM }

	division_types = { "L_Air_assault_Bat" "Arm_Air_assault_Bat" "L_Air_Inf_Bat" "Mot_Air_Inf_Bat"  }

	fallback_name = "%d. Odayin Hardzakman Brigadner"

}

ARM_MAR_BRIGADES = {
	name = "Amphibious Rapid Deployment Brigades"

	for_countries = { ARM }

	division_types = { "L_Marine_Bat" "Mot_Marine_Bat" "Mech_Marine_Bat" "Arm_Marine_Bat" }

	fallback_name = "%d. Amfibianeri Arag Teghakayman Brigadner"
}

ARM_SPEC_FORCES_BRIGADES = {
	name = "Special Forces Groups"

	for_countries = { ARM }

	division_types = { "Special_Forces" }

	fallback_name = "%d. Gndakits' Hatuk Npatak"
}

ARM_MIL_BRIGADES = {
	name = "Revolutionary Federation Volunteers"

	for_countries = { ARM }

	division_types = { "Militia_Bat" "Mot_Militia_Bat" }

	fallback_name = "%d. Heghap'vokhakan Dashnakts'ut'yan kamavorner"
}