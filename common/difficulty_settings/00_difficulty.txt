difficulty_settings = {
	difficulty_setting = {
		key = "custom_diff_strong_isi"
		modifier = diff_strong_ai_generic
		countries = { ISI }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_ger"
		modifier = diff_strong_ai_generic
		countries = { GER }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_sov"
		modifier = diff_strong_ai_generic
		countries = { SOV }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_fra"
		modifier = diff_strong_ai_generic
		countries = { FRA }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_eng"
		modifier = diff_strong_ai_generic
		countries = { ENG }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_usa"
		modifier = diff_strong_ai_generic
		countries = { USA }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_jap"
		modifier = diff_strong_ai_generic
		countries = { JAP }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_chi"
		modifier = diff_strong_ai_generic
		countries = { CHI }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_kor"
		modifier = diff_strong_ai_generic
		countries = { KOR }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_nko"
		modifier = diff_strong_ai_generic
		countries = { NKO }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_raj"
		modifier = diff_strong_ai_generic
		countries = { RAJ }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_spr"
		modifier = diff_strong_ai_generic
		countries = { SPR }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_nig"
		modifier = diff_strong_ai_generic
		countries = { NIG }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_bra"
		modifier = diff_strong_ai_generic
		countries = { BRA }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_mex"
		modifier = diff_strong_ai_generic
		countries = { MEX }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_can"
		modifier = diff_strong_ai_generic
		countries = { CAN }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_ita"
		modifier = diff_strong_ai_generic
		countries = { ITA }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_saf"
		modifier = diff_strong_ai_generic
		countries = { SAF }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_ast"
		modifier = diff_strong_ai_generic
		countries = { AST }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_ind"
		modifier = diff_strong_ai_generic
		countries = { IND }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_per"
		modifier = diff_strong_ai_generic
		countries = { PER }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_isr"
		modifier = diff_strong_ai_generic
		countries = { ISR }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_tur"
		modifier = diff_strong_ai_generic
		countries = { TUR }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_gre"
		modifier = diff_strong_ai_generic
		countries = { GRE }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_sau"
		modifier = diff_strong_ai_generic
		countries = { SAU }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_swe"
		modifier = diff_strong_ai_generic
		countries = { SWE }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_pol"
		modifier = diff_strong_ai_generic
		countries = { POL }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_ukr"
		modifier = diff_strong_ai_generic
		countries = { UKR }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_swi"
		modifier = diff_strong_ai_generic
		countries = { SWI }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_brm"
		modifier = diff_strong_ai_generic
		countries = { BRM }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_syr"
		modifier = diff_strong_ai_generic
		countries = { SYR }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_uae"
		modifier = diff_strong_ai_generic
		countries = { UAE }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_eth"
		modifier = diff_strong_ai_generic
		countries = { ETH }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_arg"
		modifier = diff_strong_ai_generic
		countries = { ARG }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_den"
		modifier = diff_strong_ai_generic
		countries = { DEN }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_fin"
		modifier = diff_strong_ai_generic
		countries = { FIN }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_aze"
		modifier = diff_strong_ai_generic
		countries = { AZE }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_arm"
		modifier = diff_strong_ai_generic
		countries = { ARM }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_bot"
		modifier = diff_strong_ai_generic
		countries = { BOT }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_lic"
		modifier = diff_strong_ai_generic
		countries = { LIC }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_bul"
		modifier = diff_strong_ai_generic
		countries = { BUL }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_sin"
		modifier = diff_strong_ai_generic
		countries = { SIN }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_com"
		modifier = diff_strong_ai_generic
		countries = { COM }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_geo"
		modifier = diff_strong_ai_generic
		countries = { GEO }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_abk"
		modifier = diff_strong_ai_generic
		countries = { ABK }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_ser"
		modifier = diff_strong_ai_generic
		countries = { SER }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_pmr"
		modifier = diff_strong_ai_generic
		countries = { PMR }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_soo"
		modifier = diff_strong_ai_generic
		countries = { SOO }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_kos"
		modifier = diff_strong_ai_generic
		countries = { KOS }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_che"
		modifier = diff_strong_ai_generic
		countries = { CHE }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_bsh"
		modifier = diff_strong_ai_generic
		countries = { BSH }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_irq"
		modifier = diff_strong_ai_generic
		countries = { IRQ }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_egy"
		modifier = diff_strong_ai_generic
		countries = { EGY }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_moz"
		modifier = diff_strong_ai_generic
		countries = { MOZ }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_drc"
		modifier = diff_strong_ai_generic
		countries = { DRC }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_oma"
		modifier = diff_strong_ai_generic
		countries = { OMA }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_mor"
		modifier = diff_strong_ai_generic
		countries = { MOR }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_alg"
		modifier = diff_strong_ai_generic
		countries = { ALG }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_mal"
		modifier = diff_strong_ai_generic
		countries = { MAL }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_cub"
		modifier = diff_strong_ai_generic
		countries = { CUB }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_hai"
		modifier = diff_strong_ai_generic
		countries = { HAI }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_dom"
		modifier = diff_strong_ai_generic
		countries = { DOM }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_log"
		modifier = diff_strong_ai_generic
		countries = { LOG }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_bal"
		modifier = diff_strong_ai_generic
		countries = { BAL }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_sel"
		modifier = diff_strong_ai_generic
		countries = { SEL }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_jub"
		modifier = diff_strong_ai_generic
		countries = { JUB }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_sna"
		modifier = diff_strong_ai_generic
		countries = { SNA }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_som"
		modifier = diff_strong_ai_generic
		countries = { SOM }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_shb"
		modifier = diff_strong_ai_generic
		countries = { SHB }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_pak"
		modifier = diff_strong_ai_generic
		countries = { PAK }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_nor"
		modifier = diff_strong_ai_generic
		countries = { NOR }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_hol"
		modifier = diff_strong_ai_generic
		countries = { HOL }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_rom"
		modifier = diff_strong_ai_generic
		countries = { ROM }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_blr"
		modifier = diff_strong_ai_generic
		countries = { BLR }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_est"
		modifier = diff_strong_ai_generic
		countries = { EST }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_lat"
		modifier = diff_strong_ai_generic
		countries = { LAT }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_lit"
		modifier = diff_strong_ai_generic
		countries = { LIT }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_ice"
		modifier = diff_strong_ai_generic
		countries = { ICE }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_hez"
		modifier = diff_strong_ai_generic
		countries = { HEZ }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_leb"
		modifier = diff_strong_ai_generic
		countries = { LEB }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_dpr"
		modifier = diff_strong_ai_generic
		countries = { DPR }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_strong_lpr"
		modifier = diff_strong_ai_generic
		countries = { LPR }
		multiplier = 2.0
	}
	# Weak AI
	difficulty_setting = {
		key = "custom_diff_weak_bsh"
		modifier = diff_weak_ai_generic
		countries = { BSH }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_che"
		modifier = diff_weak_ai_generic
		countries = { CHE }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_kos"
		modifier = diff_weak_ai_generic
		countries = { KOS }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_soo"
		modifier = diff_weak_ai_generic
		countries = { SOO }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_pmr"
		modifier = diff_weak_ai_generic
		countries = { PMR }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_ser"
		modifier = diff_weak_ai_generic
		countries = { SER }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_isi"
		modifier = diff_weak_ai_generic
		countries = { ISI }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_ger"
		modifier = diff_weak_ai_generic
		countries = { GER }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_sov"
		modifier = diff_weak_ai_generic
		countries = { SOV }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_fra"
		modifier = diff_weak_ai_generic
		countries = { FRA }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_eng"
		modifier = diff_weak_ai_generic
		countries = { ENG }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_usa"
		modifier = diff_weak_ai_generic
		countries = { USA }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_jap"
		modifier = diff_weak_ai_generic
		countries = { JAP }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_chi"
		modifier = diff_weak_ai_generic
		countries = { CHI }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_kor"
		modifier = diff_weak_ai_generic
		countries = { KOR }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_nko"
		modifier = diff_weak_ai_generic
		countries = { NKO }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_raj"
		modifier = diff_weak_ai_generic
		countries = { RAJ }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_spr"
		modifier = diff_weak_ai_generic
		countries = { SPR }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_nig"
		modifier = diff_weak_ai_generic
		countries = { NIG }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_bra"
		modifier = diff_weak_ai_generic
		countries = { BRA }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_mex"
		modifier = diff_weak_ai_generic
		countries = { MEX }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_can"
		modifier = diff_weak_ai_generic
		countries = { CAN }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_ita"
		modifier = diff_weak_ai_generic
		countries = { ITA }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_saf"
		modifier = diff_weak_ai_generic
		countries = { SAF }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_ast"
		modifier = diff_weak_ai_generic
		countries = { AST }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_ind"
		modifier = diff_weak_ai_generic
		countries = { IND }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_per"
		modifier = diff_weak_ai_generic
		countries = { PER }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_isr"
		modifier = diff_weak_ai_generic
		countries = { ISR }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_tur"
		modifier = diff_weak_ai_generic
		countries = { TUR }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_gre"
		modifier = diff_weak_ai_generic
		countries = { GRE }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_sau"
		modifier = diff_weak_ai_generic
		countries = { SAU }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_swe"
		modifier = diff_weak_ai_generic
		countries = { SWE }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_pol"
		modifier = diff_weak_ai_generic
		countries = { POL }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_ukr"
		modifier = diff_weak_ai_generic
		countries = { UKR }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_swi"
		modifier = diff_weak_ai_generic
		countries = { SWI }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_brm"
		modifier = diff_weak_ai_generic
		countries = { BRM }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_syr"
		modifier = diff_weak_ai_generic
		countries = { SYR }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_uae"
		modifier = diff_weak_ai_generic
		countries = { UAE }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_eth"
		modifier = diff_weak_ai_generic
		countries = { ETH }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_arg"
		modifier = diff_weak_ai_generic
		countries = { ARG }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_den"
		modifier = diff_weak_ai_generic
		countries = { DEN }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_fin"
		modifier = diff_weak_ai_generic
		countries = { FIN }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_aze"
		modifier = diff_weak_ai_generic
		countries = { AZE }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_arm"
		modifier = diff_weak_ai_generic
		countries = { ARM }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_bot"
		modifier = diff_weak_ai_generic
		countries = { BOT }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_lic"
		modifier = diff_weak_ai_generic
		countries = { LIC }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_bul"
		modifier = diff_weak_ai_generic
		countries = { BUL }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_sin"
		modifier = diff_weak_ai_generic
		countries = { SIN }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_com"
		modifier = diff_weak_ai_generic
		countries = { COM }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_geo"
		modifier = diff_weak_ai_generic
		countries = { GEO }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_abk"
		modifier = diff_weak_ai_generic
		countries = { ABK }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_irq"
		modifier = diff_weak_ai_generic
		countries = { IRQ }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_egy"
		modifier = diff_weak_ai_generic
		countries = { EGY }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_egy"
		modifier = diff_weak_ai_generic
		countries = { EGY }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_moz"
		modifier = diff_weak_ai_generic
		countries = { MOZ }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_drc"
		modifier = diff_weak_ai_generic
		countries = { DRC }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_oma"
		modifier = diff_weak_ai_generic
		countries = { OMA }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_mor"
		modifier = diff_weak_ai_generic
		countries = { MOR }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_alg"
		modifier = diff_weak_ai_generic
		countries = { ALG }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_mal"
		modifier = diff_weak_ai_generic
		countries = { MAL }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_cub"
		modifier = diff_weak_ai_generic
		countries = { CUB }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_hai"
		modifier = diff_weak_ai_generic
		countries = { HAI }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_dom"
		modifier = diff_weak_ai_generic
		countries = { DOM }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_log"
		modifier = diff_weak_ai_generic
		countries = { LOG }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_bal"
		modifier = diff_weak_ai_generic
		countries = { BAL }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_sel"
		modifier = diff_weak_ai_generic
		countries = { SEL }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_jub"
		modifier = diff_weak_ai_generic
		countries = { JUB }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_sna"
		modifier = diff_weak_ai_generic
		countries = { SNA }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_som"
		modifier = diff_weak_ai_generic
		countries = { SOM }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_shb"
		modifier = diff_weak_ai_generic
		countries = { SHB }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_pak"
		modifier = diff_weak_ai_generic
		countries = { PAK }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_nor"
		modifier = diff_weak_ai_generic
		countries = { NOR }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_hol"
		modifier = diff_weak_ai_generic
		countries = { HOL }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_rom"
		modifier = diff_weak_ai_generic
		countries = { ROM }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_blr"
		modifier = diff_weak_ai_generic
		countries = { BLR }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_est"
		modifier = diff_weak_ai_generic
		countries = { EST }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_lat"
		modifier = diff_weak_ai_generic
		countries = { LAT }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_lit"
		modifier = diff_weak_ai_generic
		countries = { LIT }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_ice"
		modifier = diff_weak_ai_generic
		countries = { ICE }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_hez"
		modifier = diff_weak_ai_generic
		countries = { HEZ }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_leb"
		modifier = diff_weak_ai_generic
		countries = { LEB }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_dpr"
		modifier = diff_weak_ai_generic
		countries = { DPR }
		multiplier = 2.0
	}
	difficulty_setting = {
		key = "custom_diff_weak_lpr"
		modifier = diff_weak_ai_generic
		countries = { LPR }
		multiplier = 2.0
	}
}
