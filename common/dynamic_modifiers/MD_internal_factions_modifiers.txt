# Author(s): AngriestBird
# Economic Factions
small_medium_business_owners_dynamic_modifier = {
	enable = { has_idea = small_medium_business_owners }

	production_speed_industrial_complex_factor = SMBO_production_speed_industrial_complex_factor
	stability_factor = SMBO_stability_factor
	consumer_goods_factor = SMBO_consumer_goods_factor_var
	civilian_industry_tax_modifier = SMBO_civilian_industry_tax_modifier_var
	production_speed_internet_station_factor = SMBO_production_speed_internet_station_factor_var
	civilian_factories_productivity = SMBO_civilian_factories_productivity_var
}
international_bankers_dynamic_modifier = {
	enable = { has_idea = international_bankers }

	production_speed_offices_factor = INTB_psof_var
	local_resources_factor = INTB_local_resources_factor_var
	trade_opinion_factor = INTB_trade_opinion_factor_var
	democratic_acceptance = INTB_democratic_acceptance_factor_var
	democratic_drift = INTB_democratic_drift_var
	investment_duration_modifier = INTB_investment_duration_var
	investment_cost_modifier = INTB_investment_cost_var
	office_park_income_tax_modifier = INTB_office_park_income_tax_modifier_var
}
fossil_fuel_industry_dynamic_modifier = {
	enable = { has_idea = fossil_fuel_industry }

	local_resources_factor = FFI_local_resource_factor_var
	base_fuel_gain_factor = FFI_fuel_gain_factor_var
	justify_war_goal_time = FFI_justify_war_goal_time_var
	oil_export_multiplier_modifier = FFI_oil_export_multiplier_modifier_var
	production_speed_fuel_silo_factor = FFI_production_speed_fuel_silo_factor_var
}
industrial_conglomerates_dynamic_modifier = {
	enable = { has_idea = industrial_conglomerates }

	production_speed_infrastructure_factor = ICL_psifra_factor_var
	local_resources_factor = ICL_local_resources_factor_var
	civilian_industry_tax_modifier = ICL_civilian_industry_tax_modifier_var
	communism_acceptance = ICL_communism_acceptance_factor_var
	communism_drift = ICL_communism_drift
}
oligarchs_dynamic_modifier = {
	enable = { has_idea = oligarchs }

	production_speed_industrial_complex_factor = OLI_psic_factor_var
	local_resources_factor = OLI_local_resources_factor_var
	communism_acceptance = OLI_communism_acceptance_factor_var
	communism_drift = OLI_communism_drift_var
}

# Militaristic Groups
defense_industry_dynamic_modifier = {
	enable = { has_idea = defense_industry }

	production_speed_arms_factory_factor = DEF_military_factory_speed_factor_var
	production_factory_efficiency_gain_factor = DEF_factory_efficiency_gain_factor_var
	production_factory_start_efficiency_factor = DEF_factory_base_efficiency_factor_var
	military_factories_productivity = DEF_military_factories_productivity_var
	military_industry_tax_modifier = DEF_military_industry_tax_modifier_var
}
maritime_industry_dynamic_modifier = {
	enable = { has_idea = maritime_industry }

	production_speed_dockyard_factor = MRI_dockyard_factor_var
	production_speed_naval_base_factor = MRI_psvb_factor_var
	industrial_capacity_dockyard = MRI_industrial_capacity_dockyard_var
	navy_max_range_factor = MRI_navy_max_range_factor_var
	dockyard_productivity = MRI_dockyard_productivity_var
	dockyard_income_tax_modifier = MRI_dockyard_income_tax_modifier_var
}
the_military_dynamic_modifier = {
	enable = { has_idea = the_military }

	army_org_Factor = MILI_army_org_factor_var
	army_morale_factor = MILI_army_morale_factor_var
	training_time_army_factor = MILI_training_time_factor_var
	production_speed_air_base_factor = MILI_air_base_production_factor_var
	personnel_cost_multiplier_modifier = MILI_personnel_cost_multiplier_modifier_var
}

intelligence_community_dynamic_modifier = {
	enable = { has_idea = intelligence_community }

	decryption_factor = INTC_decryption_factor_var
	encryption_factor = INTC_encryption_factor_var
	civilian_intel_factor = INTC_civilian_intel_factor_var
	army_intel_factor = INTC_army_intel_factor_var
	navy_intel_factor = INTC_navy_intel_factor_var
	airforce_intel_factor = INTC_airforce_intel_factor_var
	production_speed_radar_station_factor = INTC_radar_station_factor_var
	subversive_activites_upkeep = INTC_foreign_subversive_activies_upkeep_var
	foreign_subversive_activites = INTC_foreign_subversive_activies_var
	foreign_influence_modifier = INTC_foreign_influence_modifier
}

# Special Interest Groups
labour_unions_dynamic_modifier = {
	enable = { has_idea = labour_unions }

	production_factory_start_efficiency_factor = LBU_production_start_effiency_factor_var #-0.10 to 0.15
	production_factory_efficiency_gain_factor = LBU_production_factory_efficiency_gain_factor_var #-0.10 to 0.15
	political_power_factor = LBU_political_power_factor_var #-0.15 to 0.15
	health_cost_multiplier_modifier = LBU_health_cost_multiplier_modifier_var
	social_cost_multiplier_modifier = LBU_social_cost_multiplier_modifier_var
}
landowners_dynamic_modifier = {
	enable = { has_idea = landowners }

	local_resources_factor = LAND_local_resources_factor_var #0.15
	conscription_factor = LAND_conscription_factor_var #0.15
	production_speed_industrial_complex_factor = LAND_psic_factor_var #0.15
	office_park_income_tax_modifier = LAND_office_park_income_tax_modifier_var
	political_power_factor = LAND_political_power_factor_var
}

farmers_dynamic_modifier = {
	enable = { has_idea = farmers }

	conscription_factor = FARM_conscription_factor_var #0.10
	consumer_goods_factor = FARM_consumer_goods_factor_var #-0.02
	MONTHLY_POPULATION = FARM_monthly_pop_var #0.05
	agricolture_productivity_modifier = FARM_productivity_modifier_var
	agriculture_tax_modifier = FARM_agriculture_income_modifier_var
	agriculture_district_income_tax_modifier = FARM_agriculture_income_modifier_var
	production_speed_agriculture_district_factor = FARM_production_speed_agricultural_district_factor_var
}
communist_cadres_dynamic_modifier = {
	enable = { has_idea = communist_cadres }

	army_org_regain = COM_army_org_regain_var
	consumer_goods_factor = COM_consumer_good_factor_var
	army_core_defence_factor = COM_army_core_defence_factor_var
	mobilization_speed = COM_mobilization_speed_var
	bureaucracy_cost_multiplier_modifier = COM_bureaucracy_cost_multiplier_modifier_var
}

#Religious Factions#
the_priesthood_dynamic_modifier = {
	enable = { has_idea = the_priesthood }

	stability_factor = PRIEST_stability_factor_var
	MONTHLY_POPULATION = PRIEST_monthly_population_var
	political_power_factor = PRIEST_political_power_var
	education_cost_multiplier_modifier = PRIEST_education_cost_multiplier_modifier_var
}

the_ulema_dynamic_modifier = {
	enable = { has_idea = the_ulema }

	stability_factor = ULE_stability_factor_var
	MONTHLY_POPULATION = ULE_monthly_population_var
	political_power_factor = ULE_political_power_var
	education_cost_multiplier_modifier = ULE_education_cost_multiplier_modifier_var
}

the_clergy_dynamic_modifier = {
	enable = { has_idea = the_clergy }

	stability_factor = CLER_stability_factor_var
	MONTHLY_POPULATION = CLER_monthly_population_var
	political_power_factor = CLER_political_power_var
	education_cost_multiplier_modifier = CLER_education_cost_multiplier_modifier_var
}

wahabi_ulema_dynamic_modifier = {
	enable = { has_idea = wahabi_ulema }

	stability_factor = WAHULE_stability_factor_var
	MONTHLY_POPULATION = WAHULE_monthly_population_var
	political_power_factor = WAHULE_political_power_var
	fascism_drift = WAHULE_fascism_drift_var
	education_cost_multiplier_modifier = CLER_education_cost_multiplier_modifier_var
}

# Nation Specific
the_donju_dynamic_modifier = {
	enable = { has_idea = the_donju }

	production_speed_industrial_complex_factor = DONJU_production_speed_industrial_complex_factor
	stability_factor = DONJU_stability_factor
	consumer_goods_factor = DONJU_consunmer_goods_factor_var
	production_speed_infrastructure_factor = DONJU_infrastructure_production_speed_var
}

the_bazaar_dynamic_modifier = {
	enable = { has_idea = the_bazaar }

	production_speed_industrial_complex_factor = TBAZ_production_speed_industrial_complex_factor
	stability_factor = TBAZ_stability_factor
	consumer_goods_factor = TBAZ_consunmer_goods_factor_var
}

saudi_royal_family_dynamic_modifier = {
	enable = { has_idea = saudi_royal_family }

	political_power_factor = SARF_political_power_factor_var #20% Max
	drift_defence_factor = SARF_ideology_drift_defense_var #25% Max
	stability_factor = SARF_stability_factor_var #5% max
}

irgc_dynamic_modifier = {
	enable = { has_idea = irgc }

	communism_drift = IRGC_commmunist_drift_var
	foreign_influence_modifier = IRGC_offence_influence_var
}

iranian_quds_force_dynamic_modifier = {
	enable = { has_idea = iranian_quds_force }

	communism_drift = IRGC_commmunist_drift_var
}

foreign_jihadis_force_dynamic_modifier = {
	enable = { has_idea = foreign_jihadis }

	fascism_drift = JIHAD_fascism_drift_var
	non_core_manpower = JIHAD_non_core_manpower_var
	special_forces_cap = JIHAD_special_forces_cap_var
}

vevak_force_dynamic_modifier = {
	enable = { has_idea = vevak }

	decryption_factor = VEVAK_decryption_factor_var
	encryption_factor = VEVAK_encryption_factor_var
	civilian_intel_factor = VEVAK_civilian_intel_factor_var
	army_intel_factor = VEVAK_army_intel_factor_var
	navy_intel_factor = VEVAK_navy_intel_factor_var
	airforce_intel_factor = VEVAK_airforce_intel_factor_var
	production_speed_radar_station_factor = VEVAK_radar_station_factor_var
	subversive_activites_upkeep = VEVAK_subversive_activies_upkeep_var
	foreign_subversive_activites = VEVAK_foreign_subversive_activites_var
}

isi_pakistan_dynamic_modifier = {
	enable = { has_idea = isi_pakistan }

	decryption_factor = ISI_decryption_factor_var
	encryption_factor = ISI_encryption_factor_var
	civilian_intel_factor = ISI_civilian_intel_factor_var
	army_intel_factor = ISI_army_intel_factor_var
	navy_intel_factor = ISI_navy_intel_factor_var
	airforce_intel_factor = ISI_airforce_intel_factor_var
	production_speed_radar_station_factor = ISI_radar_station_factor_var
	subversive_activites_upkeep = ISI_subversive_activies_upkeep_var
	foreign_subversive_activites = ISI_foreign_subversive_activites_var
}

wall_street_force_dynamic_modifier = {
	enable = { has_idea = wall_street }

	local_resources_factor = WALL_local_resources_factor_var
	trade_opinion_factor = WALL_trade_opinion_factor_var
	production_speed_industrial_complex_factor = WALL_psic_factor_var
	office_park_income_tax_modifier = WALL_office_park_income_tax_modifier_var
	investment_cost_modifier = WALL_investment_cost_var
	investment_duration_modifier = WALL_investment_duration_var
}

chaebols_force_dynamic_modifier = {
	enable = { has_idea = chaebols }

	production_speed_industrial_complex_factor = CHAE_psic_factor_var
	production_speed_infrastructure_factor = CHAE_psinfra_factor_avr
	local_resources_factor = CHAE_local_resouces_factor
}
