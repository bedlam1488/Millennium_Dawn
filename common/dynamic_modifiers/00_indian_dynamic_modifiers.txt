#example_dynamic_modifier = {
#		icon = "GFX_idea_unknown" # optional, will show up in guis if icon is specified
#		enable = { always = yes } #optional, the modifier won't apply if not enabled
#		remove_trigger = { always = no } #optional, will remove the modifier if true
#
#		# list of modifiers
#		fuel_cost = 321
#		max_fuel = var_max_fuel # will be taken from a variable
#	}

RAJ_hoxaists_rebel = {
	enable = { always = yes }
	remove_trigger = {
		has_communist_government = yes
	}
	icon = GFX_idea_SOV_soviet_hegemony
	custom_modifier_tooltip = RAJ_land_mines_desc_info
	recruitable_population_factor = -0.5
	state_resources_factor = -0.60
	army_speed_factor_for_controller = -0.5
	army_defence_factor = -0.4
	army_attack_factor = -0.1
	state_productivity_growth_modifier = -0.25
}
RAJ_hoxaists_rebel1 = {
	enable = { always = yes }
	remove_trigger = {
		has_communist_government = yes
	}
	icon = GFX_idea_SOV_soviet_hegemony
	custom_modifier_tooltip = RAJ_land_mines_desc_info
	recruitable_population_factor = -0.45
	state_resources_factor = -0.50
	army_speed_factor_for_controller = -0.41
	army_defence_factor = -0.32
	army_attack_factor = -0.09
	state_productivity_growth_modifier = -0.20
}
RAJ_hoxaists_rebel2 = {
	enable = { always = yes }
	remove_trigger = {
		has_communist_government = yes
	}
	icon = GFX_idea_SOV_soviet_hegemony
	custom_modifier_tooltip = RAJ_land_mines_desc_info
	recruitable_population_factor = -0.36
	state_resources_factor = -0.39
	army_speed_factor_for_controller = -0.31
	army_defence_factor = -0.25
	army_attack_factor = -0.06
	state_productivity_growth_modifier = -0.15
}
RAJ_hoxaists_rebel3 = {
	enable = { always = yes }
	remove_trigger = {
		has_communist_government = yes
	}
	icon = GFX_idea_SOV_soviet_hegemony
	custom_modifier_tooltip = RAJ_land_mines_desc_info
	recruitable_population_factor = -0.27
	state_resources_factor = -0.3
	army_speed_factor_for_controller = -0.21
	army_defence_factor = -0.17
	army_attack_factor = -0.04
	state_productivity_growth_modifier = -0.10
}
RAJ_hoxaists_rebel4 = {
	enable = { always = yes }
	remove_trigger = {
		has_communist_government = yes
	}
	icon = GFX_idea_SOV_soviet_hegemony
	custom_modifier_tooltip = RAJ_land_mines_desc_info
	recruitable_population_factor = -0.17
	state_resources_factor = -0.15
	army_speed_factor_for_controller = -0.11
	army_defence_factor = -0.08
	army_attack_factor = -0.02
	state_productivity_growth_modifier = -0.05
}

RAJ_social_democracy_policy = {
	icon = "GFX_idea_democratic_socialism"
	enable = {
		original_tag = RAJ
	}
	monthly_population = RAJ_socdem_monthly_population
	war_support_factor = RAJ_socdem_war_support_factor
	stability_factor = RAJ_socdem_stability_factor
	personnel_cost_multiplier_modifier = RAJ_socdem_personnel_cost_multiplier_modifier
	production_speed_arms_factory_factor = RAJ_socdem_production_speed_arms_factory_factor
	production_speed_infrastructure_factor = RAJ_socdem_production_speed_infrastructure_factor
	stability_factor = RAJ_socdem_stabiliti_factor
	conversion_cost_civ_to_mil_factor = RAJ_socdem_conversion_cost_civ_to_mil_factor
	industrial_capacity_factory = RAJ_socdem_industrial_capacity_factory
	production_factory_efficiency_gain_factor = RAJ_socdem_production_factory_efficiency_gain_factor
	send_volunteer_size = RAJ_socdem_send_volunteer_size
	send_volunteer_divisions_required = RAJ_socdem_send_volunteer_divisions_required
	send_volunteers_tension = RAJ_socdem_send_volunteers_tension
	production_speed_buildings_factor = RAJ_socdem_production_speed_buildings_factor
	industrial_capacity_factory = RAJ_socdem_industrial_capacity_factory
	global_building_slots_factor = RAJ_socdem_global_building_slots_factor
	political_power_factor = RAJ_socdem_political_power_factor
	local_resources_factor = RAJ_socdem_local_resources_factor
	production_factory_max_efficiency_factor = RAJ_socdem_production_factory_max_efficiency_factor
}

RAJ_new_democratic_india = {
	icon = "GFX_idea_political_freedom"
	enable = {
		original_tag = RAJ
	}
	stability_factor = RAJ_demp_stability_factor
	democratic_drift = RAJ_demp_democratic_drift
	tax_gain_multiplier_modifier = RAJ_demp_tax_gain_multiplier_modifier
	monthly_population = RAJ_demp_monthly_population
	production_speed_buildings_factor = RAJ_demp_production_speed_buildings_factor
	production_speed_infrastructure_factor = RAJ_demp_production_speed_infrastructure_factor
	research_speed_factor = RAJ_demp_research_speed_factor
	political_power_factor = RAJ_demp_political_power_factor
	education_cost_multiplier_modifier = RAJ_demp_education_cost_multiplier_modifier
	war_support_factor = RAJ_demp_war_support_factor
	production_speed_arms_factory_factor = RAJ_demp_production_speed_arms_factory_factor
	industrial_capacity_factory = RAJ_demp_industrial_capacity_factory
	conscription_factor = RAJ_demp_conscription_factor
}