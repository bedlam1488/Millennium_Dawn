CHI_idea_high_speed_railway_modifier = {
	icon = "GFX_modifiers_high_speed_railway"
	enable = { always = yes }
	army_speed_factor = 0.25
	production_speed_buildings_factor = 0.05
	local_resources_factor = 0.05
	root_out_resistance_effectiveness_factor = 0.05
}