#example_dynamic_modifier = {
#		icon = "GFX_idea_unknown" # optional, will show up in guis if icon is specified
#		enable = { always = yes } #optional, the modifier won't apply if not enabled
#		remove_trigger = { always = no } #optional, will remove the modifier if true
#
#		# list of modifiers
#		fuel_cost = 321
#		max_fuel = var_max_fuel # will be taken from a variable
#	}

#BELARUS DYNAMIC MODIFIERS
# Author(s): LordBogdanoff
BLR_belarus_nati_modifier = {
	icon = "GFX_idea_blr_patriotism_ideology"
	enable = {
		original_tag = BLR
		has_completed_focus = BLR_constitution_from_1994
	}
	remove_trigger = {
		NOT = {
			has_government = nationalist
		}
	}
	stability_factor = BLR_belarus_nati_stability_factor
	nationalist_drift = BLR_belarus_nati_nationalist_drift
	political_power_gain = BLR_belarus_nati_political_power_gain
	communism_drift = BLR_belarus_nati_communism_drift
	bureaucracy_cost_multiplier_modifier = BLR_belarus_nati_bureaucracy_cost_multiplier_modifier
	drift_defence_factor = BLR_belarus_nati_drift_defence_factor
	army_attack_factor = BLR_belarus_nati_army_attack_factor
	army_defence_factor = BLR_belarus_nati_army_defence_factor
	monthly_population = BLR_belarus_nati_monthly_population
	democratic_drift = BLR_belarus_nati_democratic_drift
	foreign_influence_defense_modifier = BLR_belarus_nati_foreign_influence_defense_modifier
	consumer_goods_factor = BLR_belarus_nati_consumer_goods_factor
	war_support_factor = BLR_belarus_nati_war_support_factor
	conscription_factor = BLR_belarus_nati_conscription_factor
	personnel_cost_multiplier_modifier = BLR_belarus_nati_personnel_cost_multiplier_modifier
	production_speed_arms_factory_factor = BLR_belarus_nati_production_speed_arms_factory_factor
	army_leader_cost_factor = BLR_belarus_nati_army_leader_cost_factor
	research_speed_factor = BLR_belarus_nati_research_speed_factor
}
BLR_belarus_police_modifier = {
	icon = "GFX_idea_blr_mvd_mini"
	enable = {
		original_tag = BLR
		has_completed_focus = BLR_Increase_funding_Police
	}
	resistance_growth = BLR_belarus_police_resistance_growth
	police_cost_multiplier_modifier = BLR_belarus_police_police_cost_multiplier_modifier
	root_out_resistance_effectiveness_factor = BLR_belarus_police_root_out_resistance_effectiveness_factor
	foreign_subversive_activites = BLR_belarus_police_foreign_subversive_activites
	special_forces_out_of_supply_factor = BLR_belarus_police_special_forces_out_of_supply_factor
	special_forces_training_time_factor = BLR_belarus_police_special_forces_training_time_factor
	enemy_operative_capture_chance_factor = BLR_belarus_police_enemy_operative_capture_chance_factor
	resistance_damage_to_garrison = BLR_belarus_police_resistance_damage_to_garrison
	encryption_factor = BLR_belarus_police_encryption_factor
	foreign_influence_defense_modifier = BLR_belarus_police_foreign_influence_defense_modifier
	stability_factor = BLR_belarus_police_stability_factor
}
BLR_belarus_kommi_modifier = {
	icon = "GFX_idea_blr_red_army_org"
	enable = {
		original_tag = BLR
		has_completed_focus = BLR_Communism_start
	}
	remove_trigger = {
		NOT = {
			emerging_communist_state_are_in_power = yes
		}
	}
	local_resources_factor = BLR_belarus_kommi_local_resources_factor
	drift_defence_factor = BLR_belarus_kommi_drift_defence_factor
	enemy_operative_detection_chance = BLR_belarus_kommi_enemy_operative_detection_chance
	research_speed_factor = BLR_belarus_kommi_research_speed_factor
	stability_factor = BLR_belarus_stability_factor
	political_power_gain = BLR_belarus_kommi_political_power_gain
	production_factory_efficiency_gain_factor = BLR_belarus_kommi_production_factory_efficiency_gain_factor
	production_speed_buildings_factor = BLR_belarus_kommi_production_speed_buildings_factor
	communism_drift = BLR_belarus_kommi_communism_drift
	education_cost_multiplier_modifier = BLR_belarus_kommi_education_cost_multiplier_modifier
	consumer_goods_factor = BLR_belarus_kommi_consumer_goods_factor
	social_cost_multiplier_modifier = BLR_belarus_kommi_social_cost_multiplier_modifier
	health_cost_multiplier_modifier = BLR_belarus_kommi_health_cost_multiplier_modifier
	recruitable_population_factor = BLR_belarus_kommi_recruitable_population_factor
	nationalist_drift = BLR_belarus_kommi_nationalist_drift
}


