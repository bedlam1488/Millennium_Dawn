#1

IND_Aceh_insurgency = {
	enable = { always = yes }
	icon = GFX_idea_altAceh_insurgency
	custom_modifier_tooltip = IND_Aceh_insurgency_info
	local_org_regain = -0.15
	army_defence_factor = 0.2
	local_building_slots_factor = -0.75
	compliance_growth = -0.15
	resistance_growth = 0.15
	army_speed_factor_for_controller = -0.25
	temporary_state_resource_oil = sabotaged_oil
	temporary_state_resource_aluminium = sabotaged_aluminium
	temporary_state_resource_rubber = sabotaged_rubber
	temporary_state_resource_tungsten = sabotaged_tungsten
	temporary_state_resource_steel = sabotaged_steel
	temporary_state_resource_chromium = sabotaged_chromium
}
IND_Aceh_insurgency2 = {
	enable = { always = yes }
	icon = GFX_idea_altAceh_insurgency
	custom_modifier_tooltip = IND_Aceh_insurgency_info
	local_org_regain = -0.05
	local_building_slots_factor = -0.25
	compliance_growth = -0.10
	resistance_growth = 0.10
	army_speed_factor_for_controller = -0.20
	temporary_state_resource_oil = sabotaged_oil
	temporary_state_resource_aluminium = sabotaged_aluminium
	temporary_state_resource_rubber = sabotaged_rubber
	temporary_state_resource_tungsten = sabotaged_tungsten
	temporary_state_resource_steel = sabotaged_steel
	temporary_state_resource_chromium = sabotaged_chromium
}
IND_Aceh_insurgency3 = {
	enable = { always = yes }
	icon = GFX_idea_altAceh_insurgency
	custom_modifier_tooltip = IND_Aceh_insurgency_info
	local_org_regain = -0.03
	local_building_slots_factor = -0.10
	compliance_growth = -0.05
	resistance_growth = 0.05
	army_speed_factor_for_controller = -0.25
	temporary_state_resource_oil = sabotaged_oil
	temporary_state_resource_aluminium = sabotaged_aluminium
	temporary_state_resource_rubber = sabotaged_rubber
	temporary_state_resource_tungsten = sabotaged_tungsten
	temporary_state_resource_steel = sabotaged_steel
	temporary_state_resource_chromium = sabotaged_chromium
}
IND_Aceh_insurgency4 = {
	enable = { always = yes }
	icon = GFX_idea_altAceh_insurgency
	custom_modifier_tooltip = IND_Aceh_insurgency_info
	local_org_regain = -0.02
	local_building_slots_factor = -0.05
	compliance_growth = -0.02
	resistance_growth = 0.02
	army_speed_factor_for_controller = -0.25
	temporary_state_resource_oil = sabotaged_oil
	temporary_state_resource_aluminium = sabotaged_aluminium
	temporary_state_resource_rubber = sabotaged_rubber
	temporary_state_resource_tungsten = sabotaged_tungsten
	temporary_state_resource_steel = sabotaged_steel
	temporary_state_resource_chromium = sabotaged_chromium
}
##2 Christian Resistance
#Papua modifiers
IND_papua_resist = {
	enable = { always = yes }
	icon = GFX_idea_fallen_ind
	custom_modifier_tooltip = IND_papua_resist_info
	local_org_regain = -0.20
	army_defence_factor = -0.1
	local_building_slots_factor = -0.35
	army_speed_factor_for_controller = -0.35
	state_resources_factor = -0.50
}
IND_papua_resist2 = {
	enable = { always = yes }
	icon = GFX_idea_fallen_ind
	custom_modifier_tooltip = IND_papua_resist_info
	local_org_regain = -0.15
	army_defence_factor = -0.05
	local_building_slots_factor = -0.20
	army_speed_factor_for_controller = -0.25
	state_resources_factor = -0.25
}
IND_papua_resist3 = {
	enable = { always = yes }
	icon = GFX_idea_fallen_ind
	custom_modifier_tooltip = IND_papua_resist_info
	local_org_regain = -0.10
	local_building_slots_factor = -0.10
	army_speed_factor = -0.10
	state_resources_factor = -0.15
}
IND_papua_resist4 = {
	enable = { always = yes }
	icon = GFX_idea_fallen_ind
	custom_modifier_tooltip = IND_papua_resist_info
	local_org_regain = -0.05
	local_building_slots_factor = -0.05
	army_speed_factor = -0.05
	state_resources_factor = -0.05
}
IND_papua_resist_m1 = {
	enable = { always = yes }
	icon = GFX_idea_fallen_ind
	custom_modifier_tooltip = IND_papua_resist_info
	local_org_regain = -0.05
	local_building_slots_factor = -0.15
	army_speed_factor_for_controller = -0.05
	state_resources_factor = -0.25
}
#Sulewesi Islamic Neglect Resistance
IND_islamic_resist = {
	enable = { always = yes }
	icon = GFX_idea_fallen_ind
	custom_modifier_tooltip = IND_islamic_resist_info
	local_org_regain = -0.20
	army_defence_factor = -0.1
	local_building_slots_factor = -0.35
	army_speed_factor_for_controller = -0.35
	state_resources_factor = -0.50
}
IND_islamic_resist2 = {
	enable = { always = yes }
	icon = GFX_idea_fallen_ind
	custom_modifier_tooltip = IND_islamic_resist_info
	local_org_regain = -0.15
	army_defence_factor = -0.05
	local_building_slots_factor = -0.20
	army_speed_factor = -0.25
	state_resources_factor = -0.25
}
IND_islamic_resist3 = {
	enable = { always = yes }
	icon = GFX_idea_fallen_ind
	custom_modifier_tooltip = IND_islamic_resist_info
	local_org_regain = -0.10
	local_building_slots_factor = -0.10
	army_speed_factor_for_controller = -0.10
	state_resources_factor = -0.05
}
IND_islamic_resist4 = {
	enable = { always = yes }
	icon = GFX_idea_fallen_ind
	custom_modifier_tooltip = IND_islamic_resist_info
	local_org_regain = -0.05
	local_building_slots_factor = -0.05
	army_speed_factor_for_controller = -0.05
}
IND_islamic_resist_c1 = {
	enable = { always = yes }
	icon = GFX_idea_fallen_ind
	custom_modifier_tooltip = IND_papua_resist_info
	local_org_regain = -0.05
	local_building_slots_factor = -0.15
	army_speed_factor_for_controller = -0.05
	state_resources_factor = -0.25
}

