kurdish_resistance_dynamic_modifier = {
	enable = { always = yes }

	icon = GFX_idea_pkks

	custom_modifier_tooltip = TT_kurdish_resistance_dynamic_modifier

	resistance_target = TUR.kurdish_resistance_cap
	required_garrison_factor = TUR.kurdish_resistance_garrison_requirement
	resistance_damage_to_garrison = TUR.kurdish_resistance_garrison_damage
	resistance_garrison_penetration_chance = TUR.kurdish_resistance_garrison_penetration_chance
}
#
pkk_static_modifier = {
	enable = { always = yes }

	icon = GFX_idea_pkks

	custom_modifier_tooltip = TT_kurdish_resistance_dynamic_modifier

	local_org_regain = -0.20
	army_defence_factor = -0.1
	local_building_slots_factor = -0.35
	army_speed_factor_for_controller = -0.35
	state_resources_factor = -0.50
}