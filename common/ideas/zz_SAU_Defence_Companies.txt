ideas = {

	materiel_manufacturer = {

		designer = yes

		SAU_military_industries_corporation_materiel_manufacturer = {
			allowed = { original_tag = SAU }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SAU_military_industries_corporation_materiel_manufacturer" }

			picture = KMDB_logo # TODO: Replace with the Company Logo
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_inf_wep = 0.217
			}

			traits = {
				CAT_inf_wep_7

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	tank_manufacturer = {

		designer = yes

		SAU_abdallah_al_faris_tank_manufacturer = {
			allowed = { original_tag = SAU }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SAU_abdallah_al_faris_tank_manufacturer" }

			picture = KMDB_logo # TODO: Replace with the Company Logo
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_armor = 0.217
			}

			traits = {
				CAT_armor_7

			}
			ai_will_do = {
				factor = 1
			}

		}
	}
}