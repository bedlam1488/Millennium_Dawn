ideas = {
	tank_manufacturer = {
		designer = yes
		SIN_st_engineering_tank_manufacturer = {
			allowed = { original_tag = SIN }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SIN_st_engineering_tank_manufacturer" }
			picture = ST_Engineering
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_armor = 0.186
			}

			traits = { CAT_armor_6 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	materiel_manufacturer = {
		designer = yes
		SIN_st_engineering_materiel_manufacturer = {
			allowed = { original_tag = SIN }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SIN_st_engineering_materiel_manufacturer" }
			picture = ST_Engineering
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_inf_wep = 0.186
			}

			traits = { CAT_inf_wep_6 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	naval_manufacturer = {
		designer = yes
		SIN_st_engineering_naval_manufacturer = {
			allowed = { original_tag = SIN }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SIN_st_engineering_naval_manufacturer" }
			picture = ST_Engineering
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_carrier = 0.186
			}

			traits = { CAT_carrier_6 }
			ai_will_do = {
				factor = 1
			}
		}
	}

}
