ideas = {
	country = {
		pmc_asgard_idea = {
			picture = pmc_asgaard
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pmc_asgard" }
			allowed_civil_war = { always = yes }
			modifier = {
				root_out_resistance_effectiveness_factor = 0.25
				foreign_subversive_activites = -0.20
				resistance_damage_to_garrison = -0.15
			}
		}
		pmc_asgard_idea1 = {
			picture = pmc_asgaard
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pmc_asgard1" }
			allowed_civil_war = { always = yes }
			name = pmc_asgard_idea
			modifier = {
				root_out_resistance_effectiveness_factor = 0.15
				foreign_subversive_activites = -0.10
				resistance_damage_to_garrison = -0.07
			}
		}
		pmc_mrpi_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pmc_mrpi" }
			allowed_civil_war = { always = yes }
			picture = pmc_mpri
			modifier = {
				experience_gain_army_factor = 0.25
			}
		}
		pmc_mrpi_idea1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pmc_mrpi1" }
			allowed_civil_war = { always = yes }
			picture = pmc_mpri
			name = pmc_mrpi_idea
			modifier = {
				experience_gain_army_factor = 0.12
			}
		}
		pmc_sadat_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pmc_sadat_idea" }
			allowed_civil_war = { always = yes }
			picture = pmc_sadat
			modifier = {
				special_forces_attack_factor = 0.05
				special_forces_defence_factor = 0.05
				special_forces_training_time_factor = -0.2
			}
		}
		pmc_sadat_idea1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pmc_sadat_idea1" }
			allowed_civil_war = { always = yes }
			picture = pmc_sadat
			name = pmc_sadat_idea
			modifier = {
				special_forces_attack_factor = 0.02
				special_forces_defence_factor = 0.02
				special_forces_training_time_factor = -0.08
			}
		}
		pmc_rsb_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pmc_rsb_idea" }
			allowed_civil_war = { always = yes }
			picture = pmc_rbs
			modifier = {
				recon_factor = 0.15
				resistance_damage_to_garrison = -0.10
				root_out_resistance_effectiveness_factor = 0.12
			}
		}
		pmc_rsb_idea1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pmc_rsb_idea1" }
			allowed_civil_war = { always = yes }
			picture = pmc_rbs
			name = pmc_rsb_idea
			modifier = {
				recon_factor = 0.08
				resistance_damage_to_garrison = -0.04
				root_out_resistance_effectiveness_factor = 0.07
			}
		}
		pmc_kbr_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pmc_kbr_idea" }
			allowed_civil_war = { always = yes }
			picture = pmc_kbr
			modifier = {
				supply_consumption_factor = -0.15
				supply_factor = 0.10
				special_forces_out_of_supply_factor = -0.10
			}
		}
		pmc_kbr_idea1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pmc_kbr_idea1" }
			allowed_civil_war = { always = yes }
			picture = pmc_kbr
			name = pmc_kbr_idea
			modifier = {
				supply_consumption_factor = -0.08
				supply_factor = 0.03
				special_forces_out_of_supply_factor = -0.06
			}
		}
		pmc_nirtal_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pmc_nirtal_idea" }
			allowed_civil_war = { always = yes }
			picture = pmc_nirtal
			modifier = {
				experience_gain_army_factor = 0.15
			}
		}
		pmc_nirtal_idea1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pmc_nirtal_idea1" }
			allowed_civil_war = { always = yes }
			picture = pmc_nirtal
			name = pmc_nirtal_idea
			modifier = {
				experience_gain_army_factor = 0.05
			}
		}
		pmc_halo_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pmc_halo_idea" }
			allowed_civil_war = { always = yes }
			picture = pmc_halo
			modifier = {
				industry_free_repair_factor = 0.05
			}
		}
		pmc_halo_idea1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pmc_halo_idea1" }
			allowed_civil_war = { always = yes }
			picture = pmc_halo
			name = pmc_halo_idea
			modifier = {
				industry_free_repair_factor = 0.1
			}
		}
		pmc_dci_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pmc_dci_idea" }
			allowed_civil_war = { always = yes }
			picture = dci_button
			modifier = {
				army_intel_factor = 0.10
				navy_intel_factor = 0.10
				airforce_intel_factor = 0.10
				drift_defence_factor = 0.05
				experience_gain_air = 0.05
				max_planning = 0.05
			}
		}
		pmc_dci_idea1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pmc_dci_idea1" }
			allowed_civil_war = { always = yes }
			picture = dci_button
			name = pmc_dci_idea
			modifier = {
				army_intel_factor = 0.03
				navy_intel_factor = 0.03
				airforce_intel_factor = 0.03
				drift_defence_factor = 0.01
				experience_gain_air = 0.01
				max_planning = 0.01
			}
		}
		pmc_over_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pmc_over_idea" }
			allowed_civil_war = { always = yes }
			picture = dci_button
			modifier = {
				army_intel_factor = 0.10
				navy_intel_factor = 0.10
				experience_gain_army_factor = 0.15
			}
		}
		pmc_over_idea1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pmc_over_idea1" }
			allowed_civil_war = { always = yes }
			picture = dci_button
			name = pmc_over_idea
			modifier = {
				army_intel_factor = 0.03
				navy_intel_factor = 0.03
				experience_gain_army_factor = 0.03
			}
		}
		pmc_front_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pmc_rsb_idea" }
			allowed_civil_war = { always = yes }
			picture = pmc_frontier
			modifier = {
				recon_factor = 0.10
				resistance_damage_to_garrison = -0.05
				root_out_resistance_effectiveness_factor = 0.06
			}
		}
		pmc_front_idea1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pmc_front_idea1" }
			allowed_civil_war = { always = yes }
			picture = pmc_frontier
			name = pmc_front_idea
			modifier = {
				recon_factor = 0.03
				resistance_damage_to_garrison = -0.02
				root_out_resistance_effectiveness_factor = 0.01
			}
		}
		pmc_gazprom_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pmc_gazprom_idea" }
			allowed_civil_war = { always = yes }
			picture = pmc_gaz
			modifier = {
				country_resource_oil = 2
			}
		}
		pmc_gazprom_idea1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pmc_gazprom_idea1" }
			allowed_civil_war = { always = yes }
			picture = pmc_gaz
			name = pmc_gazprom_idea
			modifier = {
				country_resource_oil = 1
			}
		}
		pmc_eu_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pmc_eu_idea" }
			allowed_civil_war = { always = yes }
			picture = pmc_eu
			modifier = {
				special_forces_attack_factor = 0.05
				special_forces_defence_factor = 0.05
				special_forces_training_time_factor = -0.2
			}
		}
		pmc_eu_idea1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pmc_eu_idea1" }
			allowed_civil_war = { always = yes }
			picture = pmc_eu
			name = pmc_eu_idea
			modifier = {
				special_forces_attack_factor = 0.02
				special_forces_defence_factor = 0.02
				special_forces_training_time_factor = -0.08
			}
		}
		pmc_lukoil_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pmc_lukoil_idea" }
			allowed_civil_war = { always = yes }
			picture = pmc_lukoil
			modifier = {
				country_resource_oil = 2
			}
		}
		pmc_lukoil_idea1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pmc_lukoil_idea1" }
			allowed_civil_war = { always = yes }
			picture = pmc_lukoil
			name = pmc_lukoil_idea
			modifier = {
				country_resource_oil = 1
			}
		}
		pmc_rosneft_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pmc_rosneft_idea" }
			allowed_civil_war = { always = yes }
			picture = pmc_rosneft
			modifier = {
				country_resource_oil = 2
			}
		}
		pmc_rosneft_idea1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pmc_rosneft_idea1" }
			allowed_civil_war = { always = yes }
			picture = pmc_rosneft
			name = pmc_rosneft_idea
			modifier = {
				country_resource_oil = 1
			}
		}
		pmc_sberbank_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pmc_sberbank_idea" }
			allowed_civil_war = { always = yes }
			picture = pmc_sberbank
			modifier = {
				tax_gain_multiplier_modifier = 0.10
			}
		}
		pmc_sberbank_idea1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pmc_sberbank_idea1" }
			allowed_civil_war = { always = yes }
			picture = pmc_sberbank
			name = pmc_sberbank_idea
			modifier = {
				tax_gain_multiplier_modifier = 0.05
			}
		}
		pmc_vtb_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pmc_vtb_idea" }
			allowed_civil_war = { always = yes }
			picture = pmc_vtb
			modifier = {
				tax_gain_multiplier_modifier = 0.10
			}
		}
		pmc_vtb_idea1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea pmc_vtb_idea1" }
			allowed_civil_war = { always = yes }
			picture = pmc_vtb
			name = pmc_vtb_idea
			modifier = {
				tax_gain_multiplier_modifier = 0.05
			}
		}
	}
}
