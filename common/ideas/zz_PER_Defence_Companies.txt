ideas = {
	materiel_manufacturer = {
		designer = yes
		PER_defense_industries_organization_materiel_manufacturer = {
			allowed = { original_tag = PER }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PER_defense_industries_organization_materiel_manufacturer" }
			picture = Defense_Industries_Organization_PER
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_inf_wep = 0.186
			}

			traits = { CAT_inf_wep_6 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	tank_manufacturer = {
		designer = yes
		PER_defense_industries_organization_tank_manufacturer = {
			allowed = { original_tag = PER }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PER_defense_industries_organization_tank_manufacturer" }
			picture = Defense_Industries_Organization_PER
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_armor = 0.186
			}

			traits = { CAT_armor_6 }
			ai_will_do = {
				factor = 1
			}
		}

		PER_panha_tank_manufacturer = {
			allowed = { original_tag = PER }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PER_panha_tank_manufacturer" }
			picture = PANHA_PER
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_heli = 0.186
			}

			traits = { CAT_heli_6 }
			ai_will_do = {
				factor = 1
			}
		}
		PER_hesa_tank_manufacturer = {
			allowed = { original_tag = PER }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PER_hesa_tank_manufacturer" }
			picture = SHAHA_PER
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_heli = 0.155
			}

			traits = { CAT_heli_5 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	aircraft_manufacturer = {
		designer = yes
		PER_hesa_aircraft_manufacturer = {
			allowed = { original_tag = PER }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PER_hesa_aircraft_manufacturer" }
			picture = SHAHA_PER
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_fixed_wing = 0.155
			}

			traits = { CAT_fixed_wing_5 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	naval_manufacturer = {
		designer = yes
		PER_marine_industries_organization_naval_manufacturer = {
			allowed = { original_tag = PER }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PER_marine_industries_organization_naval_manufacturer" }
			picture = Marine_Industries_Organization_PER
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_naval_eqp = 0.155
			}

			traits = { CAT_naval_eqp_5 }
			ai_will_do = {
				factor = 1
			}
		}
		PER_marine_industries_organization_naval_manufacturer2 = {
			allowed = { original_tag = PER }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PER_marine_industries_organization_naval_manufacturer" }
			picture = Marine_Industries_Organization_PER
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_d_sub = 0.155
			}

			traits = { CAT_d_sub_5 }
			ai_will_do = {
				factor = 1
			}
		}
	}
}
