ideas = {
	country = {
		ARM_victory_karabakh = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = arm_win_azer

			modifier = {
				stability_factor = 0.3
				nationalist_drift = 0.02
				war_support_factor = 0.05
			}
		}
		ARM_loyal_in_power = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = colonial_focus

			modifier = {
				corruption_cost_factor = 0.30
				political_power_factor = 0.3
			}
		}
		ARM_huge_unemployment_5 = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = arm_une_idea
			modifier = {
				industrial_capacity_factory = -0.2
				stability_factor = -0.3
				production_speed_buildings_factor = -0.2
				industry_repair_factor = -0.3
				local_resources_factor = -0.10
				research_speed_factor = -0.1
				economic_cycles_cost_factor = 0.5
			}
		}
		ARM_huge_unemployment_4 = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = arm_une_idea
			modifier = {
				industrial_capacity_factory = -0.17
				stability_factor = -0.17
				production_speed_buildings_factor = -0.17
				industry_repair_factor = -0.25
				local_resources_factor = -0.08
				research_speed_factor = -0.08
				economic_cycles_cost_factor = 0.4
			}
		}
		ARM_huge_unemployment_3 = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = arm_une_idea
			modifier = {
				industrial_capacity_factory = -0.13
				stability_factor = -0.13
				production_speed_buildings_factor = -0.13
				industry_repair_factor = -0.2
				local_resources_factor = -0.06
				research_speed_factor = -0.06
				economic_cycles_cost_factor = 0.3
			}
		}
		ARM_huge_unemployment_2 = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = arm_une_idea
			modifier = {
				industrial_capacity_factory = -0.08
				stability_factor = -0.08
				production_speed_buildings_factor = -0.08
				industry_repair_factor = -0.15
				local_resources_factor = -0.04
				research_speed_factor = -0.04
				economic_cycles_cost_factor = 0.2
			}
		}
		ARM_huge_unemployment_1 = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = arm_une_idea
			modifier = {
				industrial_capacity_factory = -0.1
				stability_factor = -0.05
				production_speed_buildings_factor = -0.1
				industry_repair_factor = -0.1
				local_resources_factor = -0.02
				research_speed_factor = -0.02
				economic_cycles_cost_factor = 0.1
			}
		}
		#эмиграция
		ARM_high_emigration_5 = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = arm_emigration_idea
			modifier = {
				migration_rate_value_factor = -1.0
				research_speed_factor = -0.2

			}
		}
		ARM_high_emigration_4 = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = arm_emigration_idea
			modifier = {
				migration_rate_value_factor = -0.90
				research_speed_factor = -0.18
			}
		}
		ARM_high_emigration_3 = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = arm_emigration_idea
			modifier = {
				migration_rate_value_factor = -0.75
				research_speed_factor = -0.15
			}
		}
		ARM_high_emigration_2 = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = arm_emigration_idea
			modifier = {
				migration_rate_value_factor = -0.50
				research_speed_factor = -0.12
			}
		}
		ARM_high_emigration_1 = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = arm_emigration_idea
			modifier = {
				migration_rate_value_factor = -0.30
				research_speed_factor = -0.07
			}
		}
		ARM_immigration = {
			allowed = { always = no }
			allowed_civil_war = { always = no }
			picture = prc_the_long_march

			modifier = {
				migration_rate_value_factor = 0.20
				research_speed_factor = 0.05
			}
		}

		ARM_armenian_mafia_6 = {
			allowed = { original_tag = ARM }

			on_add = {
				ingame_update_setup = yes
			}

			allowed_civil_war = {
				always = no
			}

			picture = arm_mafia_idea

			modifier = {
				political_power_factor = -0.3
				stability_factor = -0.18
				industrial_capacity_factory = -0.12
				corruption_cost_factor = 0.60
			}
		}
		ARM_armenian_mafia_5 = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = arm_mafia_idea

			modifier = {
				political_power_factor = -0.26
				stability_factor = -0.14
				industrial_capacity_factory = -0.1
				corruption_cost_factor = 0.50
			}
		}
		ARM_armenian_mafia_4 = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = arm_mafia_idea

			modifier = {
				political_power_factor = -0.19
				stability_factor = -0.11
				industrial_capacity_factory = -0.08
				corruption_cost_factor = 0.40
			}
		}
		ARM_armenian_mafia_3 = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = arm_mafia_idea

			modifier = {
				political_power_factor = -0.13
				stability_factor = -0.9
				industrial_capacity_factory = -0.06
				corruption_cost_factor = 0.30
			}
		}
		ARM_armenian_mafia_2 = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = arm_mafia_idea

			modifier = {
				political_power_factor = -0.08
				stability_factor = -0.06
				industrial_capacity_factory = -0.03
				corruption_cost_factor = 0.20
			}
		}
		ARM_armenian_mafia_1 = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = arm_mafia_idea

			modifier = {
				stability_factor = -0.02
				corruption_cost_factor = 0.05
			}
		}
		ARM_president_vs_parlament_quo = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = prc_the_long_march

			modifier = {
				democratic_drift = 0.05
				communism_drift = 0.05
				democratic_acceptance = 10
				communism_acceptance = 10
				drift_defence_factor = 0.1
			}
		}
		ARM_president_vs_parlament_democration = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = slightly_reformist

			modifier = {
				democratic_drift = 0.05
				communism_drift = -0.01
				democratic_acceptance = 20
				communism_acceptance = -10
			}
		}
		ARM_president_vs_parlament_autocration = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = slightly_radical

			modifier = {
				democratic_drift = -0.01
				communism_drift = 0.05
				democratic_acceptance = -10
				communism_acceptance = 20
				drift_defence_factor = 0.2
			}
		}
		ARM_mass_doctrine = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = manpower_bonus

			modifier = {
				max_planning = 0.10
				conscription = 0.015
				planning_speed = -0.30
				army_org_factor = -0.15
				experience_gain_factor = -0.10
				mobilization_speed = 0.3
			}
		}
		ARM_deals_with_bandits_3 = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = youth_radicalization

			modifier = {
				political_power_factor = -0.1
				stability_factor = -0.1
				consumer_goods_factor = 0.01
				industrial_capacity_factory = -0.06
				corruption_cost_factor = 0.80
			}
		}
		ARM_deals_with_bandits_2 = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = youth_radicalization

			modifier = {
				political_power_factor = 0.1
				stability_factor = -0.01
				corruption_cost_factor = 0.40
			}
		}
		ARM_deals_with_bandits_1 = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = youth_radicalization

			modifier = {
				political_power_factor = -0.1
				consumer_goods_factor = -0.01
				corruption_cost_factor = 0.60
			}
		}
		#оппозиция
		ARM_democration_opposition_1 = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = democracy

			modifier = {
				stability_factor = -0.05
				democratic_drift = 0.02
				drift_defence_factor = -0.05
			}
		}
		ARM_democration_opposition_2 = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = party_of_progress

			modifier = {
				stability_factor = -0.07
				democratic_drift = 0.05
				drift_defence_factor = -0.1
			}
		}
		ARM_neutrals_dem_coallition = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = communism5

			modifier = {
				stability_factor = -0.05
				neutrality_drift = 0.03
				democratic_drift = 0.03
				drift_defence_factor = -0.05
			}
		}
		ARM_neutrals_dem_coallition_2 = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = party_of_progress

			modifier = {
				stability_factor = -0.07
				democratic_drift = 0.05
				neutrality_drift = 0.05
				drift_defence_factor = -0.1
			}
		}
		ARM_all_coallition = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = party_of_progress

			modifier = {
				stability_factor = -0.1
				political_power_factor = -0.1
				democratic_drift = 0.05
				neutrality_drift = 0.05
				drift_defence_factor = -0.15
			}
		}
		idea_ARM_westernes = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = democracy

			modifier = {
				democratic_drift = 0.1
			}
		}
		idea_ARM_west_help = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = democracy

			modifier = {
				democratic_drift = 0.2
				drift_defence_factor = -0.1
				stability_factor = -0.05
			}
		}
		idea_ARM_rear_front = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = manpower_bonus

			modifier = {
				democratic_drift = 0.05
			}
		}
		ARM_gaz_problems = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = arm_gaz_idea
			modifier = {
				industrial_capacity_factory = -0.05
				stability_factor = -0.07
				production_speed_buildings_factor = -0.05
			}
		}
		ARM_gaz_energy_problems = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = arm_gasenetgry_idea
			modifier = {
				industrial_capacity_factory = -0.1
				stability_factor = -0.1
				production_speed_buildings_factor = -0.1
			}
		}
		ARM_energy_problems = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = arm_energy_idea
			modifier = {
				industrial_capacity_factory = -0.05
				stability_factor = -0.07
				production_speed_buildings_factor = -0.05
			}
		}
		ARM_rear_front_idea = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = revolutionary_army
			modifier = {
				communism_drift = 0.05
			}
		}
		ARM_hard_antikapital_idea = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = communist_army
			modifier = {
				communism_drift = 0.1
			}
		}
		ARM_peace_revolution_idea = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = communism11
			modifier = {
				communism_drift = 0.05
			}
		}

		ARM_GEO_religion_problem = {
			allowed = {
				original_tag = ARM
			}

			modifier = {
				stability_factor = -0.05
				stability_weekly = -0.02
				resistance_activity = 0.5
				resistance_growth_on_our_occupied_states = 0.5
			}
		}

		ARM_GEO_religion_problem_auton = {
			allowed = {
				original_tag = ARM
			}

			modifier = {
				stability_factor = 0.05
				resistance_activity = -0.5
			}
		}

		ARM_GEO_religion_problem_reform = {
			allowed = {
				original_tag = ARM
			}

			modifier = {
				stability_factor = -0.05
				resistance_activity = 0.5
			}
		}

		ARM_GEO_language = {
			allowed = {
				original_tag = ARM
			}

			modifier = {
				political_power_factor = -0.25
				resistance_target = 0.1
				compliance_growth = -0.1
				resistance_activity = 0.5
				resistance_growth_on_our_occupied_states = 0.5
			}
		}

		ARM_GEO_language_toler = {
			allowed = {
				original_tag = ARM
			}

			modifier = {
				political_power_factor = -0.25
				resistance_target = -0.1
				compliance_growth = 0.1
				resistance_activity = -0.5
			}
		}

		ARM_GEO_language_teach = {
			allowed = {
				original_tag = ARM
			}

			modifier = {
				political_power_factor = 0.25
				resistance_target = -0.1
				compliance_growth = 0.1
			}
		}

		ARM_AZE_small_people = {
			allowed = {
				original_tag = ARM
			}

			modifier = {
				stability_factor = -0.15
				war_support_factor = -0.15
				nationalist_drift = -0.05
			}
		}

		ARM_AZE_small_people_podav = {
			allowed = {
				original_tag = ARM
			}

			modifier = {
				stability_factor = 0.05
				compliance_growth_on_our_occupied_states = 0.5
				nationalist_drift = 0.05
				required_garrison_factor = 0.5
			}
		}

		ARM_turkish_peasants = {
			picture = migrant_crisis_europe
			allowed = {
				original_tag = ARM
			}

			modifier = {
				stability_factor = -0.15
				war_support_factor = -0.15
				political_power_factor = -0.10
			}
		}

		ARM_turkish_peasants_assim = {
			allowed = {
				original_tag = ARM
			}
			picture = migrant_crisis_europe

			modifier = {
				stability_factor = -0.05
				war_support_factor = -0.15
				political_power_factor = -0.10
				monthly_population = -0.1
			}
		}

		ARM_turkish_peasants_rekrest = {
			allowed = {
				original_tag = ARM
			}
			picture = migrant_crisis_europe

			modifier = {
				stability_factor = 0.20
				war_support_factor = -0.20
				monthly_population = 0.03
			}
		}

		ARM_turkish_peasants_newident = {
			allowed = {
				original_tag = ARM
			}
			picture = migrant_crisis_europe
			modifier = {
				war_support_factor = 0.05
				political_power_factor = 0.10
				monthly_population = 0.06
				required_garrison_factor = -0.5
			}
		}

		ARM_turkish_peasants_razum = {
			allowed = {
				original_tag = ARM
			}
			picture = migrant_crisis_europe
			modifier = {
				stability_factor = -0.15
				political_power_factor = -0.10
				monthly_population = -1.6
			}
		}

		ARM_turkish_peasants_deport = {
			allowed = {
				original_tag = ARM
			}
			picture = migrant_crisis_europe
			modifier = {
				stability_factor = -0.15
				war_support_factor = 0.20
				political_power_factor = 0.10
				monthly_population = -1.6
			}
		}

		ARM_turkish_peasants_kripto = {
			allowed = {
				original_tag = ARM
			}
			picture = migrant_crisis_europe
			modifier = {
				war_support_factor = -0.10
				political_power_factor = 0.10
				monthly_population = 0.005
				required_garrison_factor = -0.5
			}
		}

		ARM_def_primacy_idea = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = communism11
			modifier = {
				nationalist_drift = 0.05
				stability_factor = 0.005
			}
		}
		ARM_betting_army_idea = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = communism11
			modifier = {
				nationalist_drift = 0.05
				war_support_factor = +0.1
				conscription = +0.01
				drift_defence_factor = 0.15
			}
		}
		ARM_betting_tradition_idea = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = communism11
			modifier = {
				nationalist_drift = 0.05
				stability_factor = 0.001
				army_core_defence_factor = 0.15
				drift_defence_factor = 0.4
			}
		}
		ARM_betting_diplo_idea = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = communism11
			modifier = {
				nationalist_drift = 0.05
				army_org_factor = 0.1
				opinion_gain_monthly_factor = 0.15
				trade_opinion_factor = 0.1
				conscription = +0.003
			}
		}
		ARM_buy_small_bysness_idea = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = communism11
			modifier = {
				political_power_factor = 0.1
			}
		}
		ARM_transcaucasian_sweizland_idea_1 = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = swiss_political_system
			modifier = {
				stability_factor = 0.05
				army_core_defence_factor = 0.1
				drift_defence_factor = 0.4
			}
		}
		ARM_transcaucasian_sweizland_idea_2 = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = swiss_political_system
			modifier = {
				stability_factor = 0.10
				army_core_defence_factor = 0.25
				drift_defence_factor = 0.5
				lend_lease_tension = 0.35
				send_volunteers_tension = 1
				join_faction_tension = 1
				war_support_factor = -0.25
				Foreign_Intervention_Law_cost_factor = 1
			}
		}
		ARM_trade_cooperation_idea = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = positive_gold
			modifier = {
				trade_opinion_factor = 0.25
				license_purchase_cost = -0.30
				improve_relations_maintain_cost_factor = -0.3
				opinion_gain_monthly_factor = 0.1
				monthly_population = 0.05
			}
		}
		ARM_trade_isolyationism_idea = {
			allowed = {
				original_tag = ARM
				always = no
			}
			allowed_civil_war = {
				always = no
			}

			picture = local_self_management
			modifier = {
				opinion_gain_monthly_factor = -0.5
				improve_relations_maintain_cost_factor = 0.3
				trade_opinion_factor = -0.5
				production_factory_max_efficiency_factor = 0.05
				global_building_slots_factor = 0.05
				production_speed_buildings_factor = 0.1

			}
		}
		ARM_armenian_defence_1 = {
			allowed = {
				original_tag = ARM
				always = no
			}
			allowed_civil_war = {
				always = no
			}

			picture = strong_outposts_1
			modifier = {
				conscription = 0.02
				army_org_factor = -0.15
			}
		}
		ARM_armenian_defence_2 = {
			name = ARM_armenian_defence_1
			allowed = {
				original_tag = ARM
				always = no
			}
			allowed_civil_war = {
				always = no
			}

			picture = strong_outposts_2
			modifier = {
				conscription = 0.02
				army_org_factor = -0.15
				mobilization_speed = 0.5
				training_time_factor = -0.2
			}
		}
		ARM_armenian_defence_3 = {
			allowed = {
				original_tag = ARM
				always = no
			}
			allowed_civil_war = {
				always = no
			}

			picture = strong_outposts_3
			modifier = {
				conscription = 0.02
				army_org_factor = -0.15
				mobilization_speed = 0.5
				training_time_factor = -0.2
				max_dig_in = 1
				production_speed_bunker_factor = 0.3
				production_speed_radar_station_factor = 0.15
			}
		}
		ARM_scince_poten_idea = {
			allowed = {
				original_tag = ARM
				always = no
			}
			allowed_civil_war = {
				always = no
			}

			picture = research_bonus
			modifier = {
				research_speed_factor = 0.1
				stability_factor = 0.05
			}
		}
		ARM_scince_poten_idea_2 = {
			allowed = {
				original_tag = ARM
				always = no
			}
			allowed_civil_war = {
				always = no
			}

			picture = duplicate_research
			on_add = {
				add_to_variable = { var = ARM_tax_multiplier_var value = 0.01 }
			}
			on_remove = {
				add_to_variable = { var = ARM_tax_multiplier_var value = -0.01 }
			}
			modifier = {
				research_speed_factor = 0.12
				stability_factor = 0.07
			}
		}
		ARM_develop_turism = {
			allowed = {
				original_tag = ARM
				always = no
			}
			allowed_civil_war = {
				always = no
			}

			picture = communism11
			on_add = {
				add_to_variable = { var = ARM_tax_multiplier_var value = 0.05 }
			}
			on_remove = {
				add_to_variable = { var = ARM_tax_multiplier_var value = -0.05 }
			}
			modifier = {
				production_speed_buildings_factor = 0.15
			}
		}
		ARM_trenings_avia = {
			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = fighter_production_increase
			modifier = {
				experience_gain_air_factor = 0.1
			}
		}
		ARM_old_doctrine = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = army_problems

			modifier = {
				max_planning = 0.10
				conscription = 0.015
				planning_speed = -0.30
				army_org_factor = -0.15
				experience_gain_factor = -0.10
				mobilization_speed = 0.3
				experience_gain_army_factor = 0.3
			}
		}
		ARM_mass_doctrine_2 = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = incompetent_officiers

			modifier = {
				max_planning = 0.10
				conscription = 0.015
				planning_speed = -0.25
				army_org_factor = -0.1
				experience_gain_factor = -0.10
				mobilization_speed = 0.3
				experience_gain_army_factor = 0.3
			}
		}
		ARM_eshalon_doctrine = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = manpower_bonus

			modifier = {
				max_planning = 0.10
				conscription = 0.015
				planning_speed = -0.25
				army_org_factor = -0.05
				experience_gain_factor = -0.10
				mobilization_speed = 0.3
				army_defence_factor = 0.1
				max_dig_in_factor = 0.3
				army_attack_factor = -0.05
				army_speed_factor = -0.05
			}
		}
		ARM_new_doctrine = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = manpower_bonus

			modifier = {
				experience_gain_army_factor = 0.3
				experience_gain_factor = 0.10
				army_attack_factor = 0.05
				army_speed_factor = 0.05
			}
		}
		ARM_squad_doctrine = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = manpower_bonus

			modifier = {
				experience_gain_army_factor = 0.3
				experience_gain_factor = 0.10
				army_attack_factor = 0.15
				army_speed_factor = 0.05
				army_org_factor = 0.1
				special_forces_cap = 0.15
				training_time_factor = 0.3
				mobilization_speed = -0.5
			}
		}
		ARM_greece_armenian_exp = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = manpower_bonus

			modifier = {
				experience_gain_army_factor = 0.05
			}
		}
		#Юнистрим
		ARM_unistrim_idea_debuf = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = arm_no_beznal_idea

			modifier = {
				production_speed_buildings_factor = -0.05
			}
		}
		#Понижение налогов
		ARM_raising_taxes_idea = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = market_dynamics

			modifier = {
				production_speed_buildings_factor = 0.1
				industrial_capacity_factory = 0.1
				economic_cycles_cost_factor = -0.3
				consumer_goods_factor = -0.03
			}
		}
		#Понижение налогов
		ARM_balancing_approach_idea = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = market_dynamics

			modifier = {
				production_speed_buildings_factor = 0.07
				industrial_capacity_factory = 0.03
			}
		}

		#Бойкот парламента
		ARM_bojkot_parlamenta_idea = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = political_censorship

			modifier = {
				political_power_factor = -0.3
				stability_factor = -0.1
			}
		}
		#дополнительные програмы пиндос
		ARM_dop_programms_idea = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = political_censorship

			modifier = {
				research_speed_factor = 0.04
			}
		}
		#бизнес отношение Царь
		ARM_deals_opp_idea1 = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = political_censorship

			modifier = {
				production_speed_buildings_factor = 0.1
				economic_cycles_cost_factor = -0.15
			}
		}
		#скупить местный бизнес
		ARM_monopolization_idea1 = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = political_censorship

			modifier = {
				production_speed_buildings_factor = 0.05
				economic_cycles_cost_factor = -0.1
				political_power_factor = 0.1
				tax_gain_multiplier_modifier = 0.01
			}
		}
		ARM_monopolization_idea2 = {
			name = ARM_monopolization_idea1
			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = political_censorship

			modifier = {
				production_speed_buildings_factor = 0.1
				economic_cycles_cost_factor = -0.2
				political_power_factor = 0.15
				tax_gain_multiplier_modifier = 0.02
			}
		}
		ARM_monopolization_idea3 = {
			name = ARM_monopolization_idea1
			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}
			picture = political_censorship

			modifier = {
				production_speed_buildings_factor = 0.15
				economic_cycles_cost_factor = -0.3
				political_power_factor = 0.2
			}
		}
		#Развитие спорта
		ARM_sports_develop1 = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = international_treaty2
			modifier = {
				stability_factor = 0.01
				monthly_population = 0.05
			}
		}
		#Продвижение спорта
		ARM_sports_develop2 = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = international_treaty2
			modifier = {
				stability_factor = 0.02
				monthly_population = 0.1
			}
		}
		#Мобилизация экономики
		ARM_kom_mobil_econom = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = social_democracy
			modifier = {
				production_speed_buildings_factor = 0.15
				industrial_capacity_factory = 0.2
				consumer_goods_factor = -0.03
			}
		}
		#Кодекс коммунизма
		ARM_kom_code = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = communist_cadres
			modifier = {
				research_speed_factor = 0.2
				political_power_gain = 0.5
				stability_weekly = 0.001
			}
		}
		#Левый милитаризм
		ARM_left_millitarism = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = communism7
			modifier = {
				conscription = 0.03
			}
		}
		#Защитить Ереван
		ARM_defence_erevan = {

			allowed = {
				original_tag = ARM
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = zombies
			modifier = {

				army_defence_factor = 0.15
				max_dig_in_factor = 0.3

			}
		}
		#Красное перевооружение
		ARM_red_rearmament = {
			allowed = {
				original_tag = ARM
				always = no
			}
			allowed_civil_war = {
				always = no
			}

			picture = communism9
			modifier = {
				conversion_cost_civ_to_mil_factor = -0.8
				industrial_capacity_factory = 0.2
				war_support_weekly = 0.01
			}
		}
		#Чистка номенклатуры reactioners
		ARM_cleaning_nomenclature_com = {
			allowed = {
				original_tag = ARM
				always = no
			}
			allowed_civil_war = {
				always = no
			}

			picture = international_brigades
			modifier = {
				communism_drift = 0.1
			}
		}
		#Чистка номенклатуры neutrals
		ARM_cleaning_nomenclature_neutral = {
			allowed = {
				original_tag = ARM
				always = no
			}
			allowed_civil_war = {
				always = no
			}

			picture = international_brigades
			modifier = {
				neutrality_drift = 0.1
			}
		}
		#Правильный рынок
		ARM_true_market = {
			allowed = {
				original_tag = ARM
				always = no
			}
			allowed_civil_war = {
				always = no
			}

			picture = china_factory
			modifier = {
				stability_factor = 0.001
				improve_relations_maintain_cost_factor = -0.3
				trade_opinion_factor = 0.3
				production_factory_max_efficiency_factor = 0.05
				global_building_slots_factor = 0.10
				production_speed_buildings_factor = 0.1
			}
		}
		#Торжество марксизма
		ARM_marxism_victory = {
			allowed_civil_war = {
				always = no
			}

			picture = armenian_sov_coa
			modifier = {
				stability_factor = 0.002
				crypto_strength = 1
				political_power_factor = 0.1
				drift_defence_factor = 0.5
			}
		}

		ARM_western_armenia_economy = {
			allowed = {
				original_tag = ARM
			}

			picture = fascism6
			modifier = {
				consumer_goods_factor = -0.05
				stability_factor = 0.01
				political_power_gain = 0.05
				production_speed_buildings_factor = 0.1
			}
			targeted_modifier = {
				tag	= TUR
				attack_bonus_against = 0.05
			}
		}

		ARM_diplo_blade_idea = {
			allowed = {
				original_tag = ARM
			}

			picture = spirit_of_nzhdeh
			modifier = {
				army_core_defence_factor = 0.1
				stability_factor = 0.05
				political_power_gain = 0.1
				ai_focus_aggressive_factor = 0.15
			}
		}

		ARM_research_iran_russia = {
			allowed = {

			}

			modifier = {
				research_speed_factor = 0.1
			}
			picture = deal_with_the_devil
			research_bonus = {
				CAT_inf_wep = 0.1
			}
		}

		ARM_licenses_from_NATO = {
			allowed = { always = yes }

			picture = Inherent_Resolve
			modifier = {
				license_purchase_cost = -0.25
				political_power_gain = -0.05
			}
		}

		ARM_high_rel = {
			allowed = { always = no }

			picture = christian_idea
			modifier = {
				stability_factor = 0.10
				political_power_gain = 0.05
				army_core_defence_factor = 0.2
			}
		}

		ARM_gos_rel = {
			allowed = { always = no }

			picture = orthodox
			modifier = {
				stability_factor = 0.05
				political_power_gain = 0.10
				army_core_defence_factor = 0.2
			}
		}

		ARM_ngo_ban = {
			allowed = { always = no }

			picture = destroy_capitalism
			modifier = {
				stability_factor = 0.01
				political_power_gain = 0.01
				democratic_drift = -0.05
			}
		}

		ARM_tax_evasion_armenia = {
			allowed = { always = yes }

			picture = political_drain
			modifier = {
				production_speed_buildings_factor = 0.05
				war_support_factor = -0.01
				stability_factor = 0.01
				neutrality_drift = 0.05
			}
		}

		ARM_arevordiyazid = {
			allowed = { always = no }

			picture = sectarian_troubles_kurdistan
			modifier = {
				production_speed_buildings_factor = -0.1
				stability_factor = -0.010
			}
		}

		ARM_arevordiyazid_ban = {
			allowed = { always = no }

			picture = sectarian_troubles_kurdistan
			modifier = {
				production_speed_buildings_factor = 0.1
				war_support_factor = -0.05
				stability_factor = 0.0005
			}
		}

		ARM_arevordiyazid_tol = {
			allowed = { always = no }

			picture = sectarian_troubles_kurdistan
			modifier = {
				production_speed_buildings_factor = -0.1
				war_support_factor = 0.10
				stability_factor = 0.020
				political_power_factor = 0.10
				neutrality_acceptance = 50
			}
		}

		ARM_grzo = {
			allowed = { always = no }

			picture = grzo
			modifier = {
				production_speed_buildings_factor = 0.10
				war_support_factor = -0.05
				stability_factor = 0.01
			}
		}

		ARM_diplomatic_president = {
			allowed = { always = no }

			picture = diplomatic_president
			modifier = {
				democratic_acceptance = 10
				communism_acceptance = 10
				fascism_acceptance = 5
				nationalist_acceptance = 10
				neutrality_acceptance = 10
				stability_factor = 0.01
			}
		}
		ARM_mafia_strikes_back = {
			allowed = { original_tag = ARM }

			picture = BOT_army_of_peacekeepers
			modifier = {
				production_speed_buildings_factor = -0.33
				stability_factor = -0.15
				war_support_factor = -0.15
				production_factory_max_efficiency_factor = -0.15
				production_lack_of_resource_penalty_factor = 0.1
			}
		}
		USA_christian_influence = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea USA_christian_influence" }
			picture = seize_church_assets
			allowed_civil_war = {
				NOT = {
					has_government = fascism
					has_government = communism
				}
			}
			cancel = {
				OR = {
					has_government = fascism
					has_government = communism
				}
			}
			modifier = {
				stability_factor = 0.05
				political_power_factor = 0.1
				fascism_drift = -0.02
				nationalist_drift = 0.02
			}
		}
		bastion_of_freedom = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea bastion_of_freedom" }
			allowed = { always = no }
			allowed_civil_war = {
				always = yes
			}
			picture = land_of_the_free
			modifier = {
				stability_factor = 0.1
				crypto_strength = 1
				political_power_factor = 0.1
				drift_defence_factor = 0.5
			}
		}
		###EACU - Trade Alliance
		ARM_idea_eacu_cooperation = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ARM_idea_eacu_cooperation" }
			picture = sov_eaes_idea
			cancel = {
				has_war_with = SOV
			}
			modifier = {
				political_power_factor = 0.15
				war_support_factor = -0.02
				stability_factor = 0.02
				local_resources_factor = 0.10
			}
		}
		###EACO Integration
		ARM_idea_eacu_cooperation_2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ARM_idea_eacu_cooperation_2" }
			picture = sov_eaes_idea
			cancel = {
				has_war_with = SOV
			}
			modifier = {
				political_power_factor = 0.25
				war_support_factor = -0.05
				stability_factor = 0.05
				local_resources_factor = 0.20
			}
		}

		ARM_inner_network_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ARM_inner_network_idea" }
			allowed = { always = no }
			picture = spy_intel

			modifier = {
				encryption = 1
				decryption = 1
			}
		}

		ARM_idea_pro_farmers = {
			name = ARM_idea_pro_farmers
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ARM_idea_pro_farmers" }

			picture = agriculture

			modifier = {
				global_building_slots_factor = 0.05
				MONTHLY_POPULATION = 0.15
				local_resources_factor = 0.10
				agricolture_productivity_modifier = 0.10
				agriculture_tax_modifier = 0.10
				agriculture_workers_modifier = 0.05
			}
		}

		ARM_idea_agrotech = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ARM_idea_pro_farmers" }

			picture = agriculture

			modifier = {
				global_building_slots_factor = 0.05
				local_resources_factor = 0.10
			}
		}

		ARM_tourism_invigorated = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ARM_tourism_invigorated" }

			allowed = {
				has_war = no
			}
			allowed_civil_war = {
				always = no
			}
			picture = foreign_capital

			modifier = {
				production_speed_buildings_factor = 0.15
			}
		}

		ARM_renewable_energy_focus1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ARM_renewable_energy_focus1" }
			picture = eco_factories
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			modifier = {
				consumer_goods_factor = -0.01
				production_factory_max_efficiency_factor = 0.025
				research_speed_factor = 0.025
				renewable_energy_gain_multiplier = 0.05
			}
		}

		ARM_renewable_energy_focus2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ARM_renewable_energy_focus2" }
			picture = eco_factories
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			modifier = {
				consumer_goods_factor = -0.03
				production_factory_max_efficiency_factor = 0.05
				research_speed_factor = 0.05
				renewable_energy_gain_multiplier = 0.10
			}
		}

		ARM_renewable_energy_focus3 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ARM_renewable_energy_focus3" }
			picture = eco_factories
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			modifier = {
				consumer_goods_factor = -0.05
				production_factory_max_efficiency_factor = 0.10
				research_speed_factor = 0.05
				renewable_energy_gain_multiplier = 0.15
			}
		}
		armenian_perseverance = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea armenian_perseverance" }

			allowed = {
			}

			allowed_civil_war = {
				always = yes
			}

			picture = arm_history

			modifier = {
				army_core_defence_factor = 0.1
				army_core_attack_factor = 0.05
				conscription = 0.005
			}
		}
	}

	materiel_manufacturer = {
		ARM_colt_representative = {
			allowed = {
				original_tag = ARM
			}
			available = {
				has_completed_focus = ARM_filials
			}
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ARM_colt_representative" }
			picture = idea_ITA_aoi
			cost = 50
			removal_cost = 10
			research_bonus = {
				CAT_inf_wep = 0.250
			}

			traits = { CAT_inf_wep_7 }
		}
	}

	materiel_manufacturer = {
		ARM_aspar_arms = {
			allowed = {
				original_tag = ARM
			}
			available = {
				has_completed_focus = ARM_Aspar
			}
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ARM_aspar_arms" }
			picture = aspar_arms_logo
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_inf_wep = 0.217
			}

			traits = { CAT_inf_wep_7 }
		}
	}
}