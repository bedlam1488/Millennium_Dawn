ideas = {

	tank_manufacturer = {

		designer = yes

		TUR_fnss_tank_manufacturer = {
			allowed = { original_tag = TUR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TUR_fnss_tank_manufacturer" }

			picture = FNSS
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_afv = 0.124
			}

			traits = {
				CAT_afv_4

			}
			ai_will_do = {
				factor = 1
			}
		}
		TUR_otokar_tank_manufacturer = {
			allowed = { original_tag = TUR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TUR_otokar_tank_manufacturer" }

			picture = Otokar
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_armor = 0.155
			}

			traits = {
				CAT_armor_5

			}
			ai_will_do = {
				factor = 1
			}
		}
		TUR_mkek_tank_manufacturer = {
			allowed = { original_tag = TUR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TUR_mkek_tank_manufacturer" }

			picture = MKEK

			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_artillery = 0.124
			}

			traits = {
				CAT_artillery_4

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	materiel_manufacturer = {

		designer = yes

		TUR_roketsan_materiel_manufacturer = {
			allowed = { original_tag = TUR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TUR_roketsan_materiel_manufacturer" }

			picture = Roketsan
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_at = 0.186
			}

			traits = {
				CAT_at_6

			}
			ai_will_do = {
				factor = 0.6 #All countries need a land army, vehicles are part of modern warfare

				modifier = {
					num_of_military_factories > 5 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_AT_6 = yes
					factor = 0
				}
				modifier = {
					is_researching_at = yes
					factor = 4000
				}
			}

		}
		TUR_mkek_materiel_manufacturer = {
			allowed = { original_tag = TUR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TUR_mkek_materiel_manufacturer" }

			picture = MKEK
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_inf_wep = 0.124
			}

			traits = {
				CAT_inf_wep_4

			}
			ai_will_do = {
				factor = 1
			}

		}
	}

	aircraft_manufacturer = {

		designer = yes

		TUR_turkish_aerospace_industries_aircraft_manufacturer = {
			allowed = { original_tag = TUR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TURkish_aerospace_industries_aircraft_manufacturer" }

			picture = TAI

			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_fighter = 0.124
			}

			traits = {
				CAT_fighter_4

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	tank_manufacturer = {

		designer = yes

		TUR_turkish_aerospace_industries_tank_manufacturer = {
			allowed = { original_tag = TUR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TURkish_aerospace_industries_tank_manufacturer" }

			picture = TAI

			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_heli = 0.124
			}

			traits = {
				CAT_heli_4

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	naval_manufacturer = {

		designer = yes

		TUR_golcuk_naval_shipyard_naval_manufacturer = {
			allowed = { original_tag = TUR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TUR_golcuk_naval_shipyard_naval_manufacturer" }

			picture = Golcuk_Naval_Shipyard
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_naval_eqp = 0.093
			}

			traits = {
				CAT_naval_eqp_3

			}
			ai_will_do = {
				factor = 1
			}
		}
		TUR_golcuk_naval_shipyard_naval_manufacturer2 = {
			allowed = { original_tag = TUR  }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TUR_golcuk_naval_shipyard_naval_manufacturer" }

			picture = Golcuk_Naval_Shipyard

			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_d_sub = 0.093
			}

			traits = {
				CAT_d_sub_3

			}
			ai_will_do = {
				factor = 1
			}
		}
	}
}
