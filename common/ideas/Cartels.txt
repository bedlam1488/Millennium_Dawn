ideas = {
	country = {
		defeated_the_cartels = {
			picture = politicised_army
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea defeated_the_cartels" }
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				corruption_cost_factor = -0.25
				political_power_factor = 0.20
				production_speed_buildings_factor = 0.25
				tax_gain_multiplier_modifier = 0.15
			}
		}

		hard_drug_trade_restrictions = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea hard_drug_trade_restrictions" }
			allowed_civil_war = {
				always = yes
			}
			picture = opium

			modifier = {
				political_power_factor = -0.05
				police_cost_multiplier_modifier = -0.05
				tax_gain_multiplier_modifier = 0.05
			}
		}

		light_drug_trade_restrictions = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea light_drug_trade_restrictions" }
			allowed_civil_war = {
				always = yes
			}
			picture = opium

			modifier = {
				political_power_factor = -0.15
				police_cost_multiplier_modifier = -0.15
				tax_gain_multiplier_modifier = 0.10
			}
		}

		narcos_state = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea narcos_state" }
			modifier = {
				political_power_factor = 0.30
				corruption_cost_factor = -0.75
				industry_free_repair_factor = 0.5
				stability_factor = 0.20
				#conscription_factor = -0.20
				MONTHLY_POPULATION = 0.10
				army_morale_factor = 0.10
				army_org_factor = 0.10
			}
			allowed_civil_war = {
				always = yes
			}
			picture = cartels
		}
		crime_syndicate_4 = {
			name = crime_syndicate_1
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea crime_syndicate_4" }
			modifier = {
				political_power_factor = -0.30
				corruption_cost_factor = 0.75
				industry_free_repair_factor = -0.5
				stability_factor = -0.20
				#conscription_factor = -0.20
				MONTHLY_POPULATION = -0.10
				army_morale_factor = -0.10
				army_org_factor = -0.10
			}

			allowed_civil_war = {
				always = yes
			}

		}

		crime_syndicate_3 = {
			name = crime_syndicate_1
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea crime_syndicate_3" }
			modifier = {
				political_power_factor = -0.20
				corruption_cost_factor = 0.5
				industry_free_repair_factor = -0.5
				stability_factor = -0.15
				#conscription_factor = -0.15
				MONTHLY_POPULATION = -0.05
				army_morale_factor = -0.05
				army_org_factor = -0.05
			}

			allowed_civil_war = {
				always = yes
			}

		}

		crime_syndicate_2 = {
			name = crime_syndicate_1
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea crime_syndicate_2" }
			modifier = {
				political_power_factor = -0.15
				corruption_cost_factor = 0.25
				industry_free_repair_factor = -0.25
				stability_factor = -0.10
				#conscription_factor = -0.5
				MONTHLY_POPULATION = -0.05
			}

			allowed_civil_war = {
				always = yes
			}

		}

		crime_syndicate_1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea crime_syndicate_1" }
			modifier = {
				political_power_factor = -0.10
				corruption_cost_factor = 0.10
				stability_factor = -0.05
			}

			allowed_civil_war = {
				always = yes
			}
		}

		mafia_state = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea mafia_state" }
			modifier = {
				political_power_factor = 0.30
				corruption_cost_factor = -0.75
				industry_free_repair_factor = 0.5
				stability_factor = 0.20
				#conscription_factor = -0.20
				MONTHLY_POPULATION = 0.10
				army_morale_factor = 0.10
				army_org_factor = 0.10
			}
			allowed_civil_war = {
				always = yes
			}
		}
	}

}
