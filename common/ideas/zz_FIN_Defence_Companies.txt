ideas = {

	tank_manufacturer = {

		designer = yes

		FIN_patria_tank_manufacturer = {
			allowed = { original_tag = FIN }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FIN_patria_tank_manufacturer" }

			picture = Patria

			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_afv = 0.248
			}

			traits = {
				CAT_afv_8

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	materiel_manufacturer = {

		designer = yes

		FIN_patria_materiel_manufacturer = {
			allowed = { original_tag = FIN }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FIN_patria_materiel_manufacturer" }

			picture = Patria

			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_inf_wep = 0.155
			}

			traits = {
				CAT_inf_wep_5

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	materiel_manufacturer = {

		designer = yes

		FIN_sako_materiel_manufacturer = {
			allowed = { original_tag = FIN }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FIN_sako_materiel_manufacturer" }

			picture = SAKO

			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_inf_wep = 0.155
			}

			traits = {
				CAT_inf_wep_5

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	naval_manufacturer = {

		designer = yes

		FIN_meyer_turku_naval_manufacturer = {
			allowed = { original_tag = FIN }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FIN_meyer_turku_naval_manufacturer" }

			picture = Meyer_Turku
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_TRANS_SHIP = 0.186
			}

			traits = {
				Cat_TRANS_SHIP_6
			}
			ai_will_do = {
				factor = 1
			}
		}
	}
}
