ideas = {
	materiel_manufacturer = {
		designer = yes
		JAP_howa_machinery_materiel_manufacturer = {
			allowed = { original_tag = JAP }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea JAP_howa_machinery_materiel_manufacturer" }
			picture = Howa
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_inf_wep = 0.248
			}

			traits = { CAT_inf_wep_8 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	tank_manufacturer = {
		designer = yes
		JAP_komatsu_tank_manufacturer = {
			allowed = { original_tag = JAP }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea JAP_komatsu_tank_manufacturer" }
			picture = Komatsu
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_afv = 0.248
			}

			traits = { CAT_afv_8 }
			ai_will_do = {
				factor = 1
			}
		}
		JAP_mitsubishi_heavy_industries_tank_manufacturer = {
			allowed = { original_tag = JAP }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea JAP_mitsubishi_heavy_industries_tank_manufacturer" }
			picture = Mitsubishi_Heavy_Industries
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_armor = 0.248
			}

			traits = { CAT_armor_8 }
			ai_will_do = {
				factor = 1
			}
		}
		JAP_mitsubishi_aerospace_tank_manufacturer = {
			allowed = { original_tag = JAP }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea JAP_mitsubishi_aerospace_tank_manufacturer" }
			picture = Mitsubishi_aerospace
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_heli = 0.248
			}

			traits = { CAT_heli_8 }
			ai_will_do = {
				factor = 1
			}
		}
		JAP_kawasaki_aerospace_company_tank_manufacturer = {
			allowed = { original_tag = JAP }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea JAP_kawasaki_aerospace_company_tank_manufacturer" }
			picture = kawasaki_aerospace
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_heli = 0.217
			}

			traits = { CAT_heli_7 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	naval_manufacturer = {
		designer = yes
		JAP_mitsubishi_shipbuilding_naval_manufacturer2 = {
			allowed = { original_tag = JAP }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea JAP_mitsubishi_shipbuilding_naval_manufacturer" }
			picture = Mitsubishi_Heavy_Industries
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_surface_ship = 0.217
			}

			traits = { CAT_surface_ship_7 }
			ai_will_do = {
				factor = 1
			}
		}

		JAP_kawasaki_shipbuilding_naval_manufacturer2 = {
			allowed = { original_tag = JAP }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea JAP_kawasaki_shipbuilding_naval_manufacturer" }
			picture = Kawasaki_Shipbuilding
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_naval_eqp = 0.279
			}

			traits = { CAT_naval_eqp_9 }
			ai_will_do = {
				factor = 1
			}
		}

		JAP_ihi_corporation_naval_manufacturer = {
			allowed = { original_tag = JAP }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea JAP_ihi_corporation_naval_manufacturer" }
			picture = IHI
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_naval_eqp = 0.248
			}

			traits = { Cat_TRANS_SHIP_8 }
			ai_will_do = {
				factor = 1
			}
		}

		JAP_mitsubishi_shipbuilding_naval_manufacturer = {
			allowed = { original_tag = JAP }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea JAP_mitsubishi_shipbuilding_naval_manufacturer" }
			picture = Mitsubishi_Heavy_Industries
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_d_sub = 0.217
			}

			traits = { CAT_d_sub_7 }
			ai_will_do = {
				factor = 1
			}
		}

		JAP_kawasaki_shipbuilding_naval_manufacturer = {
			allowed = { original_tag = JAP }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea JAP_kawasaki_shipbuilding_naval_manufacturer" }
			picture = Kawasaki_Shipbuilding
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_d_sub = 0.279
			}

			traits = { CAT_d_sub_9 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	aircraft_manufacturer = {
		designer = yes
		JAP_mitsubishi_aerospace_aircraft_manufacturer = {
			allowed = { original_tag = JAP }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea JAP_mitsubishi_aerospace_aircraft_manufacturer" }
			picture = Mitsubishi_aerospace
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_fixed_wing = 0.248
			}

			traits = { CAT_fixed_wing_8 }
			ai_will_do = {
				factor = 1
			}
		}
		JAP_kawasaki_aerospace_company_aircraft_manufacturer = {
			allowed = { original_tag = JAP }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea JAP_kawasaki_aerospace_company_aircraft_manufacturer" }
			picture = kawasaki_aerospace
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_h_air = 0.217
			}

			traits = { CAT_h_air_7 }
			ai_will_do = {
				factor = 1
			}
		}
	}
}
