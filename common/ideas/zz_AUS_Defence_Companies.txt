ideas = {
	materiel_manufacturer = {
		designer = yes
		AUS_steyr_mannlicher_materiel_manufacturer = {
			allowed = { original_tag = AUS }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea AUS_steyr_mannlicher_materiel_manufacturer" }
			picture = Steyr_Mannlicher_AUS

			cost = 150
			removal_cost = 10

			research_bonus = {
				CAT_inf_wep = 0.217
			}

			traits = { CAT_inf_wep_7 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	tank_manufacturer = {
		designer = yes
		AUS_general_dynamics_steyr_tank_manufacturer = {
			allowed = { original_tag = AUS }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea AUS_general_dynamics_steyr_tank_manufacturer" }

			picture = General_Dynamics_Steyr_AUS
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_afv = 0.217
			}

			traits = { CAT_afv_7 }
			ai_will_do = {
				factor = 1
			}
		}
	}
}
