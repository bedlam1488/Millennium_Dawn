ideas = {
	aircraft_manufacturer = {
		designer = yes
		CZE_aero_aircraft_manufacturer = {
			allowed = { original_tag = CZE }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CZE_aero_aircraft_manufacturer" }
			picture = Aero
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_l_fighter = 0.155
			}

			traits = { CAT_l_fighter_5 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	materiel_manufacturer = {
		designer = yes
		CZE_czub_materiel_manufacturer = {
			allowed = { original_tag = CZE }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CZE_czub_materiel_manufacturer" }
			picture = CZUB
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_inf_wep = 0.124
			}

			traits = { CAT_inf_wep_4 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	tank_manufacturer = {
		designer = yes
		CZE_vop_tank_manufacturer = {
			allowed = { original_tag = CZE }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CZE_vop_tank_manufacturer" }
			picture = VOP
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_afv = 0.155
			}

			traits = { CAT_afv_5 }
			ai_will_do = {
				factor = 1
			}
		}
	}
}