ideas = {
	tank_manufacturer = {
		designer = yes
		HOL_dutch_defense_vehicle_systems_tank_manufacturer = {
			allowed = { original_tag = HOL }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HOL_dutch_defense_vehicle_systems_tank_manufacturer" }
			picture = Dutch_Defense_Vehicle_Systems_HOL
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_afv = 0.186
			}

			traits = {
				CAT_afv_6

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	naval_manufacturer = {

		designer = yes

		HOL_damen_shipyards_naval_manufacturer = {
			allowed = { original_tag = HOL }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HOL_damen_shipyards_naval_manufacturer" }
			picture = Damen_Shipyards_HOL
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_naval_eqp = 0.186
			}

			traits = { CAT_naval_eqp_6 }
			ai_will_do = {
				factor = 1
			}
		}
	}

}
