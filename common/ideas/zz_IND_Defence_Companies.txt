ideas = {
	materiel_manufacturer = {

		designer = yes

		IND_pt_pindad_materiel_manufacturer = {
			allowed = { original_tag = IND }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IND_pt_pindad_materiel_manufacturer" }
			picture = PT_Pindad_IND
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_inf_wep = 0.155
			}

			traits = {
				CAT_inf_wep_5

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	aircraft_manufacturer = {

		designer = yes

		IND_indonesian_aerospace_aircraft_manufacturer = {
			allowed = { original_tag = IND }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IND_indonesian_aerospace_aircraft_manufacturer" }

			picture = Indonesian_Aerospace_IND
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_fixed_wing = 0.062
			}
			traits = {
				CAT_fixed_wing_2

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	tank_manufacturer = {

		designer = yes

		IND_pt_pindad_tank_manufacturer = {
			allowed = { original_tag = IND }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IND_pt_pindad_tank_manufacturer" }
			picture = PT_Pindad_IND
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_afv = 0.155
			}

			traits = { CAT_afv_5 }
			ai_will_do = {
				factor = 1
			}
		}

		IND_indonesian_aerospace_tank_manufacturer = {
			allowed = { original_tag = IND }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IND_indonesian_aerospace_tank_manufacturer" }
			picture = Indonesian_Aerospace_IND
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_heli = 0.062
			}

			traits = {
				CAT_heli_2

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

}
