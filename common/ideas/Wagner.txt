#Made by Lord Bogdanoff
ideas = {
	country = {
		#Wagner Centers
		WAG_centers = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea WAG_centers" }
			allowed_civil_war = { always = yes }
			picture = wag_baze_icon
			modifier = {
				research_speed_factor = 0.09
			}
		}
		#Wagner Centers
		WAG_centers1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea WAG_centers1" }
			allowed_civil_war = { always = yes }
			picture = wag_baze_icon
			modifier = {
				research_speed_factor = 0.09
				health_cost_multiplier_modifier = 0.1
				experience_loss_factor = -0.2
			}
		}
		#Wagner Centers
		WAG_centers2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea WAG_centers2" }
			allowed_civil_war = { always = yes }
			picture = wag_baze_icon
			modifier = {
				research_speed_factor = 0.09
				health_cost_multiplier_modifier = 0.1
				experience_loss_factor = -0.2

				country_resource_aluminium = 1
				country_resource_oil = 2
				country_resource_rubber = 1
				country_resource_tungsten = 1
				country_resource_steel = 2
				country_resource_chromium = 1
			}
		}
		#Recruit DynCopr(USA)
		WAG_pmcs_army = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea WAG_pmcs_army" }
			allowed_civil_war = { always = yes }
			picture = wag_pmcs_icon
			modifier = {
				army_defence_factor = 0.07
			}
		}
		#Recruit Northbridge Services(DOM)
		WAG_pmcs_army1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea WAG_pmcs_army1" }
			allowed_civil_war = { always = yes }
			picture = wag_pmcs_icon
			modifier = {
				army_defence_factor = 0.07
				recon_factor = 0.05
			}
		}
		#Erinys International(ENG)
		WAG_pmcs_army2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea WAG_pmcs_army2" }
			allowed_civil_war = { always = yes }
			picture = wag_pmcs_icon
			modifier = {
				army_defence_factor = 0.07
				recon_factor = 0.05
				acclimatization_cold_climate_gain_factor = 0.2
				acclimatization_hot_climate_gain_factor = 0.2
			}
		}
		#MPRI International(USA)
		WAG_pmcs_army3 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea WAG_pmcs_army3" }
			allowed_civil_war = { always = yes }
			picture = wag_pmcs_icon
			modifier = {
				army_defence_factor = 0.07
				recon_factor = 0.05
				acclimatization_cold_climate_gain_factor = 0.2
				acclimatization_hot_climate_gain_factor = 0.2

				special_forces_attack_factor = 0.03
				special_forces_defence_factor = 0.03
				special_forces_training_time_factor = -0.1
			}
		}
		#Academi(USA)
		WAG_pmcs_army4 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea WAG_pmcs_army4" }
			allowed_civil_war = { always = yes }
			picture = wag_pmcs_icon
			modifier = {
				army_defence_factor = 0.07
				recon_factor = 0.05
				acclimatization_cold_climate_gain_factor = 0.2
				acclimatization_hot_climate_gain_factor = 0.2
				special_forces_attack_factor = 0.03
				special_forces_defence_factor = 0.03
				special_forces_training_time_factor = -0.1

				drift_defence_factor = -0.15
				root_out_resistance_effectiveness_factor = 0.15
				foreign_subversive_activites = -0.10
			}
		}
	}
}