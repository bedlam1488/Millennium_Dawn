ideas = {
	country = {
		swiss_political_system = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea swiss_political_system" }
			picture = swiss_political_system
			allowed = {
				original_tag = SWI
			}
			cancel = {
				OR = {
					is_subject = yes
					AND = {
						NOT = { has_government = democratic }
						NOT = { has_government = neutrality }
					}
				}
			}
			modifier = {
				stability_factor = 0.05
				war_support_factor = 0.1
				political_power_factor = -0.25
			}
		}
		swiss_political_system2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea swiss_political_system2" }
			picture = swiss_political_system
			allowed = {
				original_tag = SWI
			}
			cancel = {
				OR = {
					is_subject = yes
					AND = {
						NOT = { has_government = democratic }
						NOT = { has_government = neutrality }
					}
				}
			}
			modifier = {
				stability_factor = 0.07
				war_support_factor = 0.12
				political_power_factor = -0.20
			}
		}
		swiss_political_system3 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea swiss_political_system3" }
			picture = swiss_political_system
			allowed = {
				original_tag = SWI
			}
			cancel = {
				OR = {
					is_subject = yes
					AND = {
						NOT = { has_government = democratic }
						NOT = { has_government = neutrality }
					}
				}
			}
			modifier = {
				stability_factor = 0.07
				war_support_factor = 0.12
				political_power_factor = -0.17
				bureaucracy_cost_factor = -0.10
			}
		}
		SWI_european_tanks = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SWI_european_tanks" }
			picture = european_tank_license

			allowed = {
				original_tag = SWI
			}

			modifier = {
				license_armor_purchase_cost = -0.75
				license_production_speed = 0.25
			}
		}
		SWI_idea_arms_exports = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SWI_idea_arms_exports" }
			picture = SWI_weaponize_Swissness
			modifier = {
				industrial_capacity_factory = 0.15
				consumer_goods_factor = -0.04
			}
		}
		SWI_idea_armed_population = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SWI_idea_armed_population" }
			picture = politicised_army2
			modifier = {
				industrial_capacity_factory = 0.075
			}
			equipment_bonus = {
				Inf_equipment = {
					build_cost_ic = -0.10
					instant = yes
				}
			}
		}
		SWI_idea_armed_forces = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SWI_idea_armed_forces" }
			picture = SWI_spirit_of_helvetia_passive_peace
			modifier = {
				army_org_factor = 0.02
				planning_speed = 0.10
			}
		}
		SWI_idea_armed_forces2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SWI_idea_armed_forces2" }
			picture = SWI_spirit_of_helvetia_passive_peace
			modifier = {
				army_org_factor = 0.02
				planning_speed = 0.10
				army_leader_start_level = 2
				army_leader_cost_factor = -0.15
			}
		}
		SWI_idea_armed_forces3 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SWI_idea_armed_forces3" }
			picture = SWI_spirit_of_helvetia_passive_peace
			modifier = {
				planning_speed = 0.10
				army_leader_start_level = 2
				army_leader_cost_factor = -0.15
				army_org_factor = 0.05
				army_org_regain = 0.03
			}
		}
		SWI_idea_armed_forces4 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SWI_idea_armed_forces4" }
			picture = SWI_spirit_of_helvetia_passive_peace
			modifier = {
				planning_speed = 0.10
				army_leader_start_level = 2
				army_leader_cost_factor = -0.15
				army_org_factor = 0.05
				army_org_regain = 0.03
				industrial_capacity_dockyard = 0.30
				production_speed_dockyard_factor = 0.15
				naval_speed_factor = 0.1
			}
		}
		SWI_swiss_neutrality = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SWI_swiss_neutrality" }
			picture = swiss_neutrality

			allowed = {
				original_tag = SWI
			}

			modifier = {
				ai_get_ally_desire_factor = -1000
				army_core_defence_factor = 0.25
				Foreign_Intervention_Law_cost_factor = 1
				hidden_modifier = {
					justify_war_goal_time = 1000000
				}
			}
		}
		SWI_swiss_neutrality2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SWI_swiss_neutrality2" }
			picture = swiss_neutrality

			allowed = {
				original_tag = SWI
			}

			modifier = {
				ai_get_ally_desire_factor = -1000
				army_core_defence_factor = 0.325
				Foreign_Intervention_Law_cost_factor = 1
				hidden_modifier = {
					justify_war_goal_time = 1000000
				}
			}
		}
		SWI_defensive_preparations = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SWI_defensive_preparations" }
			picture = swiss_defensive_preparations

			allowed = {
				original_tag = SWI
			}

			modifier = {
				army_core_defence_factor = 0.25
				production_speed_bunker_factor = 0.25
			}
		}
		SWI_defensive_preparations2 = {
			name = SWI_defensive_preparations
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SWI_defensive_preparations2" }
			picture = swiss_defensive_preparations

			allowed = {
				original_tag = SWI
			}

			modifier = {
				army_core_defence_factor = 0.50
				production_speed_bunker_factor = 0.25
			}
		}
		SWI_Expanding_nuclear_research = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SWI_Expanding_nuclear_research" }
			picture = shared_research
			allowed = { original_tag = SWI }

			modifier = {
				research_speed_factor = 0.08
				production_speed_buildings_factor = 0.1
				nuclear_energy_generation_modifier = 0.075
			}
		}
		SWI_Expanding_nuclear_research1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SWI_Expanding_nuclear_research1" }

			picture = shared_research

			allowed = {
				original_tag = SWI
			}

			modifier = {
				research_speed_factor = 0.12
				production_speed_buildings_factor = 0.1
				political_power_factor = 0.05
				nuclear_energy_generation_modifier = 0.15
			}
		}
	}

}