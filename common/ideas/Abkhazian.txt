ideas = {
	country = {
		ABK_victory_war_idea = {
			allowed = {
				original_tag = ABK

			}

			removal_cost = -1
			picture = intel_bonus

			modifier = {
				stability_factor = 0.2
				war_support_factor = 0.04
			}
		}
		ABK_outdated_army_idea = {
			allowed = {
				original_tag = ABK
				always = yes
			}

			removal_cost = -1
			picture = abkhaz_army_low

			modifier = {
				army_attack_factor = -0.10
				army_defence_factor = -0.10
				industrial_capacity_factory = -0.25
			}
		}
		ABK_tourism1_idea = {
			allowed = {
				original_tag = ABK
				always = yes
				NOT = { is_subject_of = GEO }
			}

			removal_cost = -1
			picture = tourism

			modifier = {
				custom_modifier_tooltip = ABK_tourism_TT
			}
		}
		ABK_tourism2_idea = {
			allowed = {
				original_tag = ABK
				always = yes
				NOT = { is_subject_of = GEO }
			}

			removal_cost = -1
			picture = tourism

			modifier = {
				custom_modifier_tooltip = ABK_tourism_TT
			}
		}
		ABK_mafia_idea = {
			allowed = {
				original_tag = ABK
				always = no
			}


			picture = cartels
			removal_cost = -1
			modifier = {
				political_power_factor = -0.25
				stability_factor = -0.15
				industrial_capacity_factory = -0.1
				corruption_cost_factor = 0.40
			}
		}
		ABK_mafia2_idea = {
			allowed = {
				original_tag = ABK
				always = no
			}

			picture = cartels
			removal_cost = -1
			modifier = {
				political_power_factor = -0.25
				stability_factor = -0.15
				corruption_cost_factor = 0.40
			}
		}
		ABK_mafia3_idea = {
			allowed = {
				original_tag = ABK
				always = no
			}

			picture = cartels
			removal_cost = -1
			modifier = {
				political_power_factor = -0.10
				stability_factor = -0.15
				corruption_cost_factor = 0.20
			}
		}
		ABK_mafia4_idea = {
			allowed = {
				original_tag = ABK
				always = no
			}


			picture = cartels
			removal_cost = -1
			modifier = {
				political_power_factor = -0.05
				stability_factor = -0.05
				corruption_cost_factor = 0.10
			}
		}
		ABK_energy_problems_idea = {
			allowed = {
				original_tag = ABK
				always = no
			}


			removal_cost = -1
			picture = segregation2
			modifier = {
				industrial_capacity_factory = -0.1
				stability_factor = -0.1
				production_speed_buildings_factor = -0.1
				energy_gain_multiplier = -0.20
			}
		}
		ABK_energy_problems2_idea = {
			allowed = {
				original_tag = ABK
				always = no
			}


			removal_cost = -1
			picture = segregation2
			modifier = {
				industrial_capacity_factory = -0.06
				stability_factor = -0.07
				production_speed_buildings_factor = -0.06
				energy_gain_multiplier = -0.10
			}
		}
		ABK_visa_free_russia_idea = {
			allowed = {
				original_tag = ABK
				always = no
				NOT = { is_subject_of = GEO }
			}


			removal_cost = -1
			picture = visa_free
			modifier = {
				monthly_population = 0.03
			}
		}
		ABK_narcos_idea = {
			allowed = {
				original_tag = ABK
				always = no
			}


			removal_cost = -1
			picture = narcos
			modifier = {
				monthly_population = -0.05
			}
		}
		ABK_narcos2_idea = {
			allowed = {
				original_tag = ABK
				always = no
			}


			removal_cost = -1
			picture = narcos
			modifier = {
				monthly_population = -0.03
			}
		}
		ABK_repatriation_idea = {
			allowed = {
				original_tag = ABK
				always = no
			}


			removal_cost = -1
			picture = low_popular_support3
			modifier = {
				monthly_population = 0.04
			}
		}
		ABK_defense_of_abkhazia_idea = {
			allowed = {
				original_tag = ABK
				always = no
			}


			removal_cost = -1
			picture = oil_profits
			modifier = {
				army_core_defence_factor = 0.1
			}
		}
		ABK_crypto_idea = {
			allowed = {
				original_tag = ABK
				always = no
			}


			removal_cost = -1
			picture = bitcoin
			modifier = {
				population_tax_income_multiplier_modifier = 0.09
			}
		}
		ABK_russian_railway_help_idea = {
			allowed = {
				original_tag = ABK
				always = no
				NOT = { is_subject_of = GEO }
			}


			removal_cost = -1
			picture = rails_troops
			modifier = {
				production_speed_rail_way_factor = 0.30
			}
		}

		CIS_two_member_state_idea = {
			allowed = {
			original_tag = ABK
			always = no
			NOT = { is_subject_of = GEO }
			}

			removal_cost = -1

			picture = cis_2

			modifier = {
				political_power_factor = 0.10
				trade_opinion_factor = 0.08
				stability_factor = 0.02
			}
		}
	}
}