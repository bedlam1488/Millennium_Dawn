# Author(s): AngriestBird
ideas = {
	country = {
		FRA_idea_military_improvement_army = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_military_improvement_army" }
			allowed = { always = no }
			picture = morale_bonus
			modifier = {
				army_morale_factor = 0.1
				army_attack_factor = 0.05
				army_org_factor = 0.075
				experience_gain_army = 0.05
				justify_war_goal_time = -0.05
			}
		}

		FRA_idea_military_improvement_navy = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_military_improvement_navy" }
			allowed = { always = no }
			picture = morale_bonus
			modifier = {
				experience_gain_navy = 0.05
				navy_screen_defence_factor = 0.10
				navy_submarine_attack_factor = 0.10
				justify_war_goal_time = -0.05
			}
		}

		FRA_idea_military_improvement_air_force = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_military_improvement_air_force" }
			allowed = { always = no }
			picture = morale_bonus
			modifier = {
				experience_gain_air = 0.05
				ground_attack_factor = 0.10
				enemy_army_bonus_air_superiority_factor = -0.10
				justify_war_goal_time = -0.05
			}
		}

		FRA_idea_military_improvement_army_and_navy = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_military_improvement_army_and_navy" }
			allowed = { always = no }
			picture = morale_bonus
			modifier = {
				army_morale_factor = 0.1
				army_attack_factor = 0.05
				army_org_factor = 0.075
				experience_gain_army = 0.05
				experience_gain_navy = 0.05
				navy_screen_defence_factor = 0.10
				navy_submarine_attack_factor = 0.10
				justify_war_goal_time = -0.10
			}
		}

		FRA_idea_military_improvement_army_and_air_force = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_military_improvement_army_and_air_force" }
			allowed = { always = no }
			picture = morale_bonus
			modifier = {
				army_morale_factor = 0.1
				army_attack_factor = 0.05
				army_org_factor = 0.075
				experience_gain_air = 0.05
				experience_gain_army = 0.05
				ground_attack_factor = 0.10
				enemy_army_bonus_air_superiority_factor = -0.10
				justify_war_goal_time = -0.10
			}
		}

		FRA_idea_military_improvement_air_force_and_navy = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_military_improvement_air_force_and_navy" }
			allowed = { always = no }
			picture = morale_bonus
			modifier = {
				experience_gain_air = 0.05
				navy_screen_defence_factor = 0.10
				navy_submarine_attack_factor = 0.10
				experience_gain_navy = 0.05
				ground_attack_factor = 0.10
				enemy_army_bonus_air_superiority_factor = -0.10
				justify_war_goal_time = -0.10
			}
		}

		FRA_idea_the_french_military = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_the_french_military" }
			allowed = { always = no }
			picture = volunteer_defenders3
			modifier = {
				army_morale_factor = 0.1
				army_attack_factor = 0.05
				army_org_factor = 0.10
				experience_gain_air = 0.05
				experience_gain_army = 0.05
				navy_screen_defence_factor = 0.10
				navy_submarine_attack_factor = 0.10
				experience_gain_navy = 0.05
				ground_attack_factor = 0.10
				enemy_army_bonus_air_superiority_factor = -0.10
				justify_war_goal_time = -0.25
			}
		}

		FRA_idea_means_of_production = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_means_of_production" }
			allowed = { always = no }
			picture = communism2

			modifier = {
				industrial_capacity_factory = 0.15
				consumer_goods_factor = -0.02
				production_speed_buildings_factor = 0.10
			}

		}

		FRA_idea_fra_usa_collab = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_fra_usa_collab" }

			allowed = {
				always = no
			}

			picture = research_bonus

			modifier = {
				research_speed_factor = 0.05
			}

		}

		FRA_taxation_reform_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_taxation_reform_idea" }

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			picture = consumer_goods

			modifier = {
				consumer_goods_factor = -0.05
				tax_rate_change_multiplier_modifier = -0.15
				political_power_factor = 0.15
			}

		}

		FRA_fraternity_movement_reformed = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_fraternity_movement_reformed" }

			allowed = { always = no }
			allowed_civil_war = { always = yes }

			picture = fascism3

			modifier = {
				nationalist_drift = 0.10
			}
		}

		FRA_idea_private_contracting_companies = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_private_contracting_companies" }
			allowed = { always = no }

			picture = construction

			modifier = {
				production_speed_industrial_complex_factor = 0.1
				production_speed_naval_base_factor = 0.1
				production_speed_offices_factor = 0.1
				production_speed_internet_station_factor = 0.1
				civilian_industry_tax_modifier = 0.05
				civilian_factories_productivity = 0.15
			}
		}

		FRA_administrative_reforms_1 = {
			name = FRA_idea_administrative_reforms
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_administrative_reforms_1" }
			allowed = { always = no }

			picture = political_power_bonus

			modifier = {
				political_power_factor = 0.15
				bureaucracy_cost_multiplier_modifier = 0.15
				drift_defence_factor = 0.1
				custom_modifier_tooltip = cannot_go_lower_than_bureau_01_tt
			}
		}

		FRA_administrative_reforms_2 = {
			name = FRA_idea_administrative_reforms
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_administrative_reforms_2" }
			allowed = { always = no }

			picture = political_power_bonus

			modifier = {
				political_power_factor = 0.30
				bureaucracy_cost_multiplier_modifier = 0.25
				drift_defence_factor = 0.2
				custom_modifier_tooltip = cannot_go_lower_than_bureau_02_tt
			}
		}

		FRA_idea_class_equality_reform = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_class_equality_reform" }

			allowed = { always = no }
			allowed_civil_war = {
				OR = {
					has_government = communism
					has_government = neutrality
				}
			}
			picture = communism2

			modifier = {
				communism_drift = 0.05
				neutrality_drift = 0.05
				consumer_goods_factor = -0.05
				stability_weekly = 0.001
			}
		}

		FRA_popular_ministry_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_popular_ministry_idea" }

			allowed = { always = no }
			allowed_civil_war = {
				OR = {
					has_government = communism
					has_government = neutrality
				}
			}
			picture = communism5

			modifier = {
				communism_drift = 0.05
				neutrality_drift = 0.05
				conscription_factor = 0.10
				consumer_goods_factor = -0.05
			}
		}

		FRA_restoration_society_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_restoration_society_idea" }
			allowed = { always = no }

			picture = monarchy

			modifier = {
				nationalist_drift = 0.05
				stability_factor = 0.05
			}

		}

		FRA_idea_napoleon_dream = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_napoleon_dream" }
			allowed = { always = no }
			picture = monarchy2
			modifier = {
				training_time_factor = -0.25
				army_org_factor = 0.10
				generate_wargoal_tension = -0.10
				stability_weekly = 0.002
				war_support_weekly = 0.002
			}
		}

		FRA_increased_border_controls_1 = {
			name = FRA_increased_border_controls
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_increased_border_controls_1" }

			allowed = {
				always = no
			}

			picture = fortification2

			modifier = {
				stability_factor = 0.05
				MONTHLY_POPULATION = -0.05
			}
		}

		FRA_increased_border_controls_2 = {
			name = FRA_increased_border_controls
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_increased_border_controls_2" }

			allowed = {
				always = no
			}

			picture = fortification2

			modifier = {
				stability_factor = 0.05
				nationalist_drift = 0.05
				MONTHLY_POPULATION = -0.1
			}
		}

		FRA_idea_economic_interventionism = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_economic_interventionism" }
			allowed = { always = no }
			picture = construction

			modifier = {
				production_speed_buildings_factor = -0.10
				industrial_capacity_factory = 0.15
				industrial_capacity_dockyard = 0.15
				economic_cycles_cost_factor = -0.25
			}

		}

		FRA_idea_fifth_power = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_fifth_power" }
			allowed = {
				always = no
			}
			picture = victors_of_ww1
			modifier = {
				consumer_goods_factor = -0.05
				drift_defence_factor = 0.05
				production_speed_buildings_factor = 0.075
				foreign_influence_defense_modifier = 0.15
				foreign_influence_modifier = 0.10
			}
		}

		FRA_state_investments_in_sa = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_state_investments_in_sa" }
			allowed = { always = no }
			picture = trade
			modifier = {
				local_resources_factor = 0.15
				resource_export_multiplier_modifier = 0.10
				gdp_from_resource_sector_modifier = 0.05
			}
		}

		FRA_investments_in_french_military_industries = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_investments_in_french_military_industries" }
			allowed = { always = no }
			picture = construction
			modifier = {
				production_speed_supply_node_factor = 0.05
				production_speed_naval_base_factor = 0.05
				production_speed_arms_factory_factor = 0.15
				personnel_cost_multiplier_modifier = 0.25
			}
		}

		FRA_idea_infrastructure_projects = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_infrastructure_projects" }
			allowed = { always = no }
			picture = construction
			modifier = {
				production_speed_infrastructure_factor = 0.15
				production_speed_rail_way_factor = 0.15
				tax_gain_multiplier_modifier = -0.05
			}
		}
		FRA_idea_central = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_central" }
			name = FRA_idea_central_decentral
			allowed = { always = no }
			picture = foreign_capital
			modifier = {
				bureaucracy_cost_factor = 2.0
				bureaucracy_cost_multiplier_modifier = 0.25
				political_power_gain = 0.50
				custom_modifier_tooltip = cannot_go_lower_than_bureau_03_tt
			}
		}
		FRA_idea_decentral = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_decentral" }
			name = FRA_idea_central_decentral
			allowed = { always = no }
			picture = foreign_capital
			modifier = {
				bureaucracy_cost_factor = 2.0
				bureaucracy_cost_multiplier_modifier = -0.25
				production_speed_offices_factor = 0.05
				custom_modifier_tooltip = cannot_go_higher_than_bureau_03_tt
			}
		}
		FRA_idea_french_nato_restructures = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_french_nato_restructures" }
			allowed = { always = no }
			picture = morale_bonus
			modifier = {
				army_org_factor = 0.10
				army_attack_factor = 0.05
			}
		}
		FRA_idea_vive_la_france = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_vive_la_france" }
			allowed = { always = no }
			picture = volunteer_defenders2
			modifier = {
				conscription_factor = 0.05
				consumer_goods_factor = -0.03
				army_defence_factor = 0.05
				drift_defence_factor = 0.25
			}
		}

		FRA_idea_communist_revolution1 = {
			name = FRA_idea_communist_revolution
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_communist_revolution1" }
			allowed = { always = no }
			picture = communism4
			modifier = {
				communism_drift = 0.05
				drift_defence_factor = 0.10
				political_power_factor = 0.15
			}
		}

		FRA_idea_communist_revolution2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_communist_revolution2" }
			allowed = { always = no }
			picture = communism4
			modifier = {
				communism_drift = 0.10
				drift_defence_factor = 0.20
				political_power_factor = 0.25
			}
		}

		FRA_idea_french_red_army = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_french_red_army" }
			allowed = { always = no }
			picture = communism2
			modifier = {
				justify_war_goal_time = -0.25
				army_org_factor = 0.10
				army_defence_factor = 0.05
				army_personnel_cost_multiplier_modifier = -0.15
				conscription_factor = 0.05
			}
		}

		FRA_idea_napoleon_guard = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_napoleon_guard" }
			allowed = { always = no }
			picture = monarchy
			modifier = {
				justify_war_goal_time = -0.25
				army_org_factor = 0.10
				army_attack_factor = 0.10
			}
		}

		FRA_idea_new_empire = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_new_empire" }
			allowed = { always = no }
			picture = fascism
			modifier = {
				political_power_gain = 0.25
				consumer_goods_factor = -0.05
				army_org_factor = 0.10
				army_leader_start_level = 1
			}
		}

		FRA_idea_strong_guy = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_strong_guy" }
			allowed = { always = no }
			picture = fascism2
			modifier = {
				drift_defence_factor = 0.25
				political_power_factor = 0.25
				nationalist_drift = 0.05
			}
		}

		FRA_idea_french_armored_production = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_french_armored_production" }
			allowed = { always = no }
			picture = fortification
			modifier = {
				industrial_capacity_factory = 0.10
				mil_facs_worker_requirement_modifier = -0.05
				military_factories_productivity = 0.05
			}
			equipment_bonus = {
				util_vehicle_equipment = {
					build_cost_ic = -0.05
					instant = yes
				}
				apc_hull = {
					build_cost_ic = -0.15
					instant = yes
				}
				ifv_hull = {
					build_cost_ic = -0.15
					instant = yes
				}
			}
		}

		# CNES Section of the Tree
		FRA_idea_space_agency_1 = {
			name = FRA_idea_space_agency_name
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_space_agency_1" }
			allowed = { always = no }
			picture = cnes_idea
		}

		FRA_idea_space_agency_2 = {
			name = FRA_idea_space_agency_name
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_space_agency_2" }
			allowed = { always = no }
			picture = cnes_idea
			modifier = {
				consumer_goods_factor = 0.01
				political_power_gain = -0.05
				research_speed_factor = 0.015
				olv_production_speed_modifier = -0.02
			}
		}
		FRA_idea_cnes_failing = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_cnes_failing" }
			allowed = { always = no }
			picture = consumer_goods
			modifier = {
				consumer_goods_factor = 0.05
				political_power_gain = -0.10
				research_speed_factor = -0.05
				olv_production_speed_modifier = 0.25
			}
		}
		FRA_idea_space_agency_3 = {
			name = FRA_idea_space_agency_name
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_space_agency_3" }
			allowed = { always = no }
			picture = cnes_idea
			modifier = {
				consumer_goods_factor = 0.02
				research_speed_factor = 0.03
				olv_production_speed_modifier = -0.05
			}
		}
		FRA_idea_space_agency_4 = {
			name = FRA_idea_space_agency_name
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_space_agency_4" }
			allowed = { always = no }
			picture = cnes_idea
			modifier = {
				consumer_goods_factor = 0.03
				political_power_gain = 0.10
				research_speed_factor = 0.05
				olv_production_speed_modifier = -0.075
			}
		}

		FRA_idea_reformed_cnes_non_lar = {
			name = FRA_idea_reformed_cnes
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_reformed_cnes_non_lar" }
			allowed = { always = no }
			picture = cnes_idea
			modifier = {
				consumer_goods_factor = 0.04
				political_power_gain = 0.15
				research_speed_factor = 0.10
				encryption = 1
				decryption = 1
				olv_production_speed_modifier = -0.10
			}
		}

		FRA_idea_reformed_cnes_lar = {
			name = FRA_idea_reformed_cnes
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_reformed_cnes_lar" }
			allowed = { always = no }
			picture = cnes_idea
			modifier = {
				consumer_goods_factor = 0.04
				political_power_gain = 0.15
				research_speed_factor = 0.10
				civilian_intel_factor = 0.1
				army_intel_factor = 0.1
				navy_intel_factor = 0.1
				olv_production_speed_modifier = -0.10
			}
		}

		FRA_idea_prep_the_launch = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_prep_the_launch" }
			allowed = { always = no }
			picture = central_management
			modifier = {
				political_power_factor = -0.05
				consumer_goods_factor = 0.01
			}
		}

		FRA_idea_space_research = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_space_research" }
			allowed = { always = no }
			picture = central_management
			modifier = {
				research_speed_factor = 0.10
				political_power_factor = 0.10
			}
		}

		FRA_idea_expanding_r_and_d = {
			name = FRA_idea_expanding_r_and_d
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_expanding_r_and_d"
				ingame_update_setup = yes
			}
			picture = research_bonus
			modifier = {
				research_speed_factor = 0.08
				political_power_factor = -0.05
				education_cost_multiplier_modifier = 0.10
			}
		}

		FRA_idea_expanding_r_and_d1 = {
			name = FRA_idea_expanding_r_and_d
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_expanding_r_and_d1"
				ingame_update_setup = yes
			}
			picture = research_bonus
			allowed = { always = no }

			modifier = {
				research_speed_factor = 0.16
				political_power_factor = -0.1
				education_cost_multiplier_modifier = 0.20
			}
		}

		FRA_idea_nasa_partnership = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_nasa_partnership" }
			picture = shared_research
			allowed = { always = no }

			modifier = {
				research_speed_factor = 0.1
			}
		}

		FRA_idea_the_francosphere = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_the_francosphere" }
			picture = generic_french_flag
			allowed = { always = no }

			modifier = {
				foreign_influence_defense_modifier = 0.10
				foreign_influence_continent_modifier = 0.10
				foreign_influence_auto_influence_cap_modifier = 1
			}
		}

		FRA_idea_extremist_intervention = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_extremist_intervention" }
			picture = hutu_tutsi_tensions
			allowed = { always = no }

			modifier = {
				fascism_drift = 0.20
				fascism_acceptance = 50
			}
		}

		FRA_idea_Grand_ambitions = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_Grand_ambitions" }
			picture = generic_french_flag # TODO: Replace with a better icon
			allowed = { always = no }

			cancel = {
				NOT = { has_government = nationalist }
			}

			modifier = {
				nationalist_outlook_campaign_cost_modifier = -0.15
				influence_coup_modifier = 0.15
				stability_factor = -0.12
				resource_export_multiplier_modifier = 0.08
			}
		}

		FRA_idea_Energy_in_France = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add_ideas FRA_idea_Energy_in_France" }
			picture = tower_investment
			allowed = { always = no }

			modifier = {
				production_speed_fossil_powerplant_factor = 0.05
				production_speed_nuclear_reactor_factor = 0.15
				nuclear_energy_generation_modifier = 0.3
			}
		}

		FRA_idea_Light_for_Europe = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add_ideas FRA_idea_Light_for_Europe" }
			allowed = { always = no }
			picture = nuclear_energy

			modifier = {
				custom_modifier_tooltip = FRA_idea_Light_for_Europe_income_TT
				renewable_energy_gain_multiplier = 0.25
			}
		}

		FRA_Impact_of_tourism = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add_ideas FRA_Impact_of_tourism" }
			allowed = { always = no }
			picture = hyper_inflation2

			modifier = {
				political_power_gain = 0.1
				stability_factor = -0.05
				population_tax_income_multiplier_modifier = 0.1
			}
		}

		FRA_Balanced_and_profitable_tourism = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add_ideas FRA_Balanced_and_profitable_tourism" }
			allowed = { always = no }
			picture = idea_NOR_printing_money

			modifier = {
				political_power_gain = 0.15
				population_tax_income_multiplier_modifier = 0.15
				offices_worker_requirement_modifier = 0.1
			}
		}

		FRA_A_new_transport_revolution = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add_ideas FRA_A_new_transport_revolution" }
			allowed = { always = no }
			picture = armenia_turizm

			modifier = {
				supply_node_range = 0.05
				production_speed_infrastructure_factor = 0.15
				production_speed_rail_way_factor = 0.15
				tax_gain_multiplier_modifier = -0.05
				army_fuel_consumption_factor = -0.1
			}
		}

		FRA_idea_French_Agriculture = {
			name = FRA_idea_French_Agriculture
			on_add = { log = "[GetDateText]: [Root.GetName]: add_ideas FRA_idea_French_Agriculture_1" }
			allowed = { always = no }
			picture = agricultural_reforms

			modifier = {
				stability_factor = 0.05
				agriculture_workers_modifier = 0.05
				political_power_gain = 0.05
			}
		}

		FRA_idea_French_Agriculture_2 = {
			name = FRA_idea_French_Agriculture
			on_add = { log = "[GetDateText]: [Root.GetName]: add_ideas FRA_idea_French_Agriculture_2" }
			allowed = { always = no }
			picture = agricultural_reforms

			modifier = {
				stability_factor = 0.05
				political_power_gain = 0.15
				opinion_gain_monthly_same_ideology_factor = 0.1
			}
		}

		FRA_idea_French_Agriculture_3 = {
			name = FRA_idea_French_Agriculture
			on_add = { log = "[GetDateText]: [Root.GetName]: add_ideas FRA_idea_French_Agriculture_3" }
			allowed = { always = no }
			picture = agricultural_reforms

			modifier = {
				stability_factor = 0.1
				political_power_gain = 0.15
				agriculture_tax_modifier = 0.05
				monthly_population = 0.05
				opinion_gain_monthly_same_ideology_factor = 0.1
			}
		}

		FRA_idea_French_Agriculture_4 = {
			name = FRA_idea_French_Agriculture
			on_add = { log = "[GetDateText]: [Root.GetName]: add_ideas FRA_idea_French_Agriculture_4" }
			allowed = { always = no }
			picture = agricultural_reforms

			modifier = {
				stability_factor = 0.15
				political_power_gain = 0.15
				agriculture_tax_modifier = 0.1
				monthly_population = 0.1
				opinion_gain_monthly_same_ideology_factor = 0.1
			}
		}

		FRA_food_autonomy = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add_ideas = FRA_food_autonomy" }
			allowed = { always = no }
			picture = agriculture

			modifier = {
				stability_factor = 0.15
				political_power_gain = 0.05
				defensive_war_stability_factor = 0.1
				offensive_war_stability_factor = 0.1
				supply_combat_penalties_on_core_factor = -0.2
			}
		}

		# Francosphere
		cfa_franc = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea cfa_franc" }
			picture = cfa_franc_icon
			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				custom_modifier_tooltip = FRA_cfa_franc_influence_gain_TT
				political_power_gain = 0.15
				tax_gain_multiplier_modifier = -0.05
				production_speed_buildings_factor = 0.15
			}
		}

		cfa_franc_2 = {
			name = cfa_franc
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea cfa_franc_2" }
			picture = cfa_franc_icon
			allowed = { always = no }
			allowed_civil_war = { always = yes }

			modifier = {
				custom_modifier_tooltip = FRA_cfa_franc_influence_gain_TT
				political_power_gain = 0.25
				tax_gain_multiplier_modifier = -0.10
				production_speed_buildings_factor = 0.25
			}
		}

		FRA_cfa_franc_custodian = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea cfa_franc_custodian" }
			picture = cfa_franc_custodian
			allowed = { always = no }

			modifier = {
				custom_modifier_tooltip = FRA_cfa_franc_custodian_idea_TT
				political_power_gain = 0.05
				tax_gain_multiplier_modifier = 0.05
				interest_rate_multiplier_modifier = -1
			}
		}

		the_eco = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea the_eco" }
			picture = eco_currency_icon
			allowed = { always = no }

			modifier = {
				drift_defence_factor = 0.10
				political_power_gain = 0.10
				production_speed_buildings_factor = 0.05
			}
		}

		FRA_idea_the_french_pillar_of_counterterroism = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_the_french_pillar_of_counterterroism" }
			picture = pillar_of_terrorism
			allowed = { always = no }

			modifier = {
				special_forces_training_time_factor = -0.05
				command_abilities_cost_factor = -0.05
				special_forces_cap = 0.05
			}
		}

		FRA_idea_the_french_foreign_legion = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_the_french_foreign_legion" }
			picture = french_foreign_legion
			allowed = { always = no }

			modifier = {
				special_forces_training_time_factor = -0.15
				command_abilities_cost_factor = -0.15
				special_forces_cap = 0.10
				modifier_army_sub_unit_Special_Forces_attack_factor = 0.05
				modifier_army_sub_unit_Special_Forces_defence_factor = 0.05
				send_volunteer_size = 3
			}
		}

		FRA_unrestricted_subjugation_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_unrestricted_subjugation_idea" }
			picture = scw_intervention_rep
			allowed = { always = no }

			modifier = {
				foreign_influence_modifier = 0.05
				subjects_autonomy_gain = -0.50
				drift_defence_factor = 0.05
				autonomy_gain_global_factor = -0.20
				nationalist_drift = 0.05
			}
		}

		# Francosphere Ideas
		FRA_SYR_anti_assad_regime_message_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_SYR_anti_assad_regime_message_idea" }
			picture = low_popular_support3
			allowed = { always = no }

			modifier = {
				democratic_drift = 0.05
				foreign_influence_defense_modifier = -0.1
				drift_defence_factor = -0.05
			}
		}

		FRA_SYR_escalation_from_the_damascus_spring_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_SYR_escalation_from_the_damascus_spring_idea" }
			picture = divided_syria
			allowed = { always = no }

			modifier = {
				democratic_drift = 0.05
				expected_police_modifier = -1
				expected_mil_modifier = -1
			}
		}

		FRA_educational_development_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_educational_development_idea" }
			picture = research_bonus
			allowed = { always = no }

			modifier = {
				education_cost_multiplier_modifier = -0.05
				research_speed_factor = 0.05
			}
		}

		FRA_idea_development_investment = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_idea_development_investment" }
			picture = GENERIC_public_works
			allowed = { always = no }

			modifier = {
				custom_modifier_tooltip = FRA_idea_development_investment_tt
				production_speed_buildings_factor = 0.075
				civilian_factories_productivity = 0.05
			}
		}

		FRA_resource_prospects_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_resource_prospects_idea" }
			picture = resource_production
			allowed = { always = no }

			modifier = {
				resource_export_multiplier_modifier = 0.075
				resource_sector_workers_modifier = -0.05
				gdp_from_resource_sector_modifier = 0.08
			}
		}
	}
}
