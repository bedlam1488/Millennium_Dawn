opinion_modifiers = {
	DEN_invested_in_our_resources = {
		value = 25
		decay = 1
	}

	DEN_invested_in_our_infrastructure = {
		value = 25
		decay = 1
	}
	DEN_Danish_Communist_Friends = {
		value = 25
	}
}