# AUTHOR(S): DREAD

on_actions = {
	on_force_government = {
		effect = {
			set_temp_variable = { rul_party_temp = FROM.ruling_party^0 }
			change_ruling_party_effect = yes
			if = { # WESTERN
				limit = {
					FROM = { has_government = democratic }
				}
				ROOT = {
					set_politics = {
						ruling_party = democratic
						elections_allowed = yes
					}
				}
			}
			else_if = { # EMERGING
				limit = {
					FROM = { has_government = communism }
				}
				ROOT = {
					set_politics = {
						ruling_party = communism
						elections_allowed = yes
					}
				}
			}
			else_if = { # SALAFISM
				limit = {
					FROM = { has_government = fascism }
				}
				ROOT = {
					set_politics = {
						ruling_party = fascism
						elections_allowed = yes
					}
				}
			}
			else_if = { # NON-ALIGNED
				limit = {
					FROM = { has_government = neutrality }
				}
				ROOT = {
					set_politics = {
						ruling_party = neutrality
						elections_allowed = yes
					}
				}
			}
			else_if = { # NATIONALIST
				limit = {
					FROM = { has_government = nationalist }
				}
				ROOT = {
					set_politics = {
						ruling_party = nationalist
						elections_allowed = yes
					}
				}
			}
			ROOT = {
				set_temp_variable = { party_index = FROM.ruling_party^0 }
				set_temp_variable = { party_popularity_increase = 0.01 }
				add_relative_party_popularity = yes
				clr_country_flag = partyall_banned
			}
		}
	}

	on_army_leader_lost_combat = {
		effect = {
			if = {
				limit = {
					has_game_rule = {
						rule = rule_disable_generals_getting_killed
						option = no
					}
				}

				set_temp_variable = { base_chance = 0.003 }
				if = {
					limit = {
						FROM = { has_idea = officer_baptism_by_fire }
					}

					multiply_temp_variable = { base_chance = 1.4 }
				}
				else_if = {
					limit = {
						FROM = { has_idea = officer_basic_training }
					}

					multiply_temp_variable = { base_chance = 1.2 }
				}
				else_if = {
					limit = {
						FROM = { has_idea = officer_military_school }
					}

					multiply_temp_variable = { base_chance = 0.85 }
				}
				else_if = {
					limit = {
						FROM = { has_idea = officer_military_academy }
					}

					multiply_temp_variable = { base_chance = 0.7 }
				}
				else_if = {
					limit = {
						FROM = { has_idea = officer_international_education }
					}

					multiply_temp_variable = { base_chance = 0.55 }
				}

				if = {
					limit = {
						ROOT = { has_trait = trait_reckless }
					}

					multiply_temp_variable = { base_chance = 1.2 }
				}
				else_if = {
					limit = {
						ROOT = { has_trait = trait_cautious }
					}

					multiply_temp_variable = { base_chance = 0.8 }
				}

				if = {
					limit = { check_variable = { random < base_chance } }
					unit_leader_event = { id = MD_character_army.1 }
				}
			}
		}
	}

	on_army_leader_won_combat = {
		effect = {
			if = {
				limit = {
					has_game_rule = {
						rule = rule_disable_generals_getting_killed
						option = no
					}
				}

				set_temp_variable = { base_chance = 0.001 }
				if = {
					limit = {
						FROM = { has_idea = officer_baptism_by_fire }
					}

					multiply_temp_variable = { base_chance = 1.4 }
				}
				else_if = {
					limit = {
						FROM = { has_idea = officer_basic_training }
					}

					multiply_temp_variable = { base_chance = 1.2 }
				}
				else_if = {
					limit = {
						FROM = { has_idea = officer_military_school }
					}

					multiply_temp_variable = { base_chance = 0.85 }
				}
				else_if = {
					limit = {
						FROM = { has_idea = officer_military_academy }
					}

					multiply_temp_variable = { base_chance = 0.7 }
				}
				else_if = {
					limit = {
						FROM = { has_idea = officer_international_education }
					}

					multiply_temp_variable = { base_chance = 0.55 }
				}

				if = {
					limit = {
						ROOT = { has_trait = trait_reckless }
					}

					multiply_temp_variable = { base_chance = 1.2 }
				}
				else_if = {
					limit = {
						ROOT = { has_trait = trait_cautious }
					}

					multiply_temp_variable = { base_chance = 0.8 }
				}

				if = {
					limit = { check_variable = { random < base_chance } }
					unit_leader_event = { id = MD_character_army.2 }
				}
			}
		}
	}
}