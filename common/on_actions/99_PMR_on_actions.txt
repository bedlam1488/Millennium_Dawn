on_actions = {
	#CIVIL WAR
	on_daily_PMR = {
		effect = {
			if = {
				limit = {
					original_tag = MLV
					NOT = { has_idea = PMR_civil_war }
					NOT = { has_country_flag = Dnestr_civil_war }
					MLV = { has_war_with = PMR }
				}
				MLV = { add_ideas = PMR_civil_war }
				set_country_flag = Dnestr_civil_war
			}
			if = {
				limit = {
					original_tag = PMR
					NOT = { has_idea = PMR_civil_war }
					NOT = { has_country_flag = Dnestr_civil_war }
					PMR = { has_war_with = MLV }
				}
				PMR = { add_ideas = PMR_civil_war }
				set_country_flag = Dnestr_civil_war
			}
			###Subject Federation Puppet###
			if = {
				limit = {
					OR = {
						original_tag = GGZ
						original_tag = OPR
					}
					NOT = { has_autonomy_state = autonomy_moldovian_federation }
					is_subject_of = PMR
					has_country_flag = PMR_subject_agree
				}
				PMR = {
					set_autonomy = {
						target = ROOT
						autonomy_state = autonomy_moldovian_federation
					}
				}
			}
			###Subject Federation Puppet###
			if = {
				limit = {
					OR = {
						original_tag = GGZ
						original_tag = PMR
					}
					NOT = { has_autonomy_state = autonomy_moldovian_federation1 }
					is_subject_of = MLV
					has_country_flag = Subject_moldova_agree
				}
				MLV = {
					set_autonomy = {
						target = ROOT
						autonomy_state = autonomy_moldovian_federation1
					}
				}
			}
			#Moldova Confederation Added
			if = {
				limit = {
					original_tag = PMR
					OR = {
						GGZ = {
							has_autonomy_state = autonomy_moldovian_federation
						}
						OPR = {
							has_autonomy_state = autonomy_moldovian_federation
						}
					}
					NOT = { has_idea = PMR_confederation }
				}
				add_ideas = PMR_confederation
			}
			#Moldova Confederation Deleted
			if = {
				limit = {
					original_tag = PMR
					NOT = {
						OR = {
						GGZ = {
							has_autonomy_state = autonomy_moldovian_federation
						}
						OPR = {
							has_autonomy_state = autonomy_moldovian_federation
						}
						}
					}
					has_idea = PMR_confederation
				}
				remove_ideas = PMR_confederation
			}
			###Added Federal Idea###
			if = {
				limit = {
					OR = {
						original_tag = GGZ
						original_tag = OPR
					}
					has_autonomy_state = autonomy_moldovian_federation
					NOT = { has_idea = PMR_republic_federation }
				}
				add_ideas = PMR_republic_federation
			}
			#Moldova Confederation Added
			if = {
				limit = {
					original_tag = MLV
					OR = {
						GGZ = {
							has_autonomy_state = autonomy_moldovian_federation1
						}
						PMR = {
							has_autonomy_state = autonomy_moldovian_federation1
						}
					}
					NOT = { has_idea = PMR_confederation }
				}
				add_ideas = PMR_confederation
			}
			#Moldova Confederation Deleted
			if = {
				limit = {
					original_tag = MLV
					NOT = {
						OR = {
							GGZ = {
								has_autonomy_state = autonomy_moldovian_federation
							}
							PMR = {
								has_autonomy_state = autonomy_moldovian_federation
							}
						}
					}
					has_idea = PMR_confederation
				}
				remove_ideas = PMR_confederation
			}
			###Added Federal Idea###
			if = {
				limit = {
					OR = {
					original_tag = GGZ
					original_tag = PMR
					}
					has_autonomy_state = autonomy_moldovian_federation1
					NOT = { has_idea = PMR_republic_federation }
				}
				add_ideas = PMR_republic_federation
			}
		}
	}
}
