#Might need later
on_actions = {
	on_daily = {
		effect = {
			# ADDED AUTONOMY FOR SUBJECTS
			if = {
				limit = {
					OR = {
						original_tag = CRO
						original_tag = BUL
						original_tag = ROM
						original_tag = SLV
						original_tag = BOS
						original_tag = RSK
						original_tag = MON
						original_tag = KOS
						original_tag = FYR
						original_tag = ALB
						original_tag = GRE
						original_tag = MNT
					}
					NOT = { has_autonomy_state = autonomy_balkan_federation }
					is_subject_of = SER
					has_country_flag = balkan_subject_agree
				}
				SER = {
					set_autonomy = {
						target = ROOT
						autonomy_state = autonomy_balkan_federation
					}
				}
			}
			# Added Subject Confederation###
			if = {
				limit = {
					SER = { has_completed_focus = SER_confederation_created }
					OR = {
						original_tag = BOS
						original_tag = KOS
						original_tag = FYR
						original_tag = CRO
						original_tag = SLV
						original_tag = MNT
						original_tag = GRE
						original_tag = BUL
						original_tag = ALB
						original_tag = ROM
					}
					has_autonomy_state = autonomy_balkan_federation
					NOT = { has_idea = SER_subject_federation }
				}
				add_ideas = SER_subject_federation
			}
			# Added Identity
			if = {
				limit = {
					SER = { has_completed_focus = SER_south_european_balkan_confederation }
					OR = {
						original_tag = BOS
						original_tag = KOS
						original_tag = FYR
						original_tag = CRO
						original_tag = SLV
						original_tag = MNT
						original_tag = GRE
						original_tag = BUL
						original_tag = ALB
						original_tag = ROM
					}
					has_autonomy_state = autonomy_balkan_federation
					NOT = { has_idea = SER_balkan_nation1 }
				}
				add_ideas = SER_balkan_nation1
			}
			###Remove Identity###
			if = {
				limit = {
					SER = { has_completed_focus = SER_south_european_balkan_confederation }
					OR = {
						original_tag = BOS
						original_tag = KOS
						original_tag = FYR
						original_tag = CRO
						original_tag = SLV
						original_tag = MNT
						original_tag = GRE
						original_tag = BUL
						original_tag = ALB
						original_tag = ROM
					}
					NOT = { has_autonomy_state = autonomy_balkan_federation }
					has_idea = SER_balkan_nation
				}
				remove_ideas = SER_balkan_nation
			}

		}
	}

	on_daily_SER = {
		effect = {
			# Federation to SER Added
			if = {
				limit = {
					VOJ = {
						has_country_flag = our_federation
						has_autonomy_state = autonomy_autonomous_province1
					}
					NOT = { has_idea = SER_confederation_idea }
				}
				add_ideas = SER_confederation_idea
			}
			#Federation to SER Deleted
			if = {
				limit = {
					VOJ = {	has_country_flag = our_federation }
					NOT = {
						VOJ = {	has_autonomy_state = autonomy_autonomous_province1 }
					}
					has_idea = SER_confederation_idea
				}
				remove_ideas = SER_confederation_idea
			}
			if = {
				limit = {
					134 = { is_owned_and_controlled_by = SER }
					has_country_flag = SER_montenegro_independence
					check_variable = { SER_Montenegro_separatism < 1 }
					check_variable = { SER_Montenegro_combatants < 500 }
				}
				country_event = {
					id = Serbia.17
				}
			}
			#
			if = {
				limit = {
					134 = { is_owned_and_controlled_by = SER }
					check_variable = { SER_Montenegro_separatism > 99 }
				}
				country_event = {
					id = Serbia.15
				}
			}
			#Balkan Confederation Added
			if = {
				limit = {
					SER = { has_completed_focus = SER_confederation_created }
					OR = {
						BOS = {	has_autonomy_state = autonomy_balkan_federation }
						KOS = {	has_autonomy_state = autonomy_balkan_federation }
						FYR = {	has_autonomy_state = autonomy_balkan_federation }
						CRO = {	has_autonomy_state = autonomy_balkan_federation }
						SLV = {	has_autonomy_state = autonomy_balkan_federation }
						MNT = {	has_autonomy_state = autonomy_balkan_federation }
						GRE = {	has_autonomy_state = autonomy_balkan_federation }
						BUL = {	has_autonomy_state = autonomy_balkan_federation }
						ALB = {	has_autonomy_state = autonomy_balkan_federation }
						ROM = {	has_autonomy_state = autonomy_balkan_federation }
					}
					NOT = { has_idea = SER_federation }
				}
				add_ideas = SER_federation
			}
			#Balkan Confederation Delete
			if = {
				limit = {
					SER = { has_completed_focus = SER_confederation_created }
					NOT = {
					OR = {
						BOS = {	has_autonomy_state = autonomy_balkan_federation }
						KOS = {	has_autonomy_state = autonomy_balkan_federation }
						FYR = {	has_autonomy_state = autonomy_balkan_federation }
						CRO = {	has_autonomy_state = autonomy_balkan_federation }
						SLV = {	has_autonomy_state = autonomy_balkan_federation }
						MNT = {	has_autonomy_state = autonomy_balkan_federation }
						GRE = {	has_autonomy_state = autonomy_balkan_federation }
						BUL = {	has_autonomy_state = autonomy_balkan_federation }
						ALB = {	has_autonomy_state = autonomy_balkan_federation }
						ROM = {	has_autonomy_state = autonomy_balkan_federation }
					}
					}
					has_idea = SER_federation
				}
				remove_ideas = SER_federation
			}
		}
	}

	on_daily_VOJ = {
		effect = {
			#Province to VOJ Added
			if = {
				limit = {
					original_tag = VOJ
					VOJ = {	has_autonomy_state = autonomy_autonomous_province }
					NOT = {
						has_country_flag = our_military
						has_idea = SER_subject_idea
					}
				}
				add_ideas = SER_subject_idea
			}
			#Province to VOJ Delete
			if = {
				limit = {
					original_tag = VOJ
					NOT = {
						VOJ = {	has_autonomy_state = autonomy_autonomous_province }
					}
					has_idea = SER_subject_idea
				}
				remove_ideas = SER_subject_idea
			}
			#Subject to VOJ Added
			if = {
				limit = {
					original_tag = VOJ
					VOJ = {	has_autonomy_state = autonomy_autonomous_province1 }
					has_country_flag = our_federation
					NOT = { has_idea = SER_republic_ser }
				}
				add_ideas = SER_republic_ser
			}
			#Subject to VOJ Deleted
			if = {
				limit = {
					original_tag = VOJ
					NOT = {
						VOJ = {	has_autonomy_state = autonomy_autonomous_province1 }
					}
					has_country_flag = our_federation
					has_idea = SER_republic_ser
				}
				remove_ideas = SER_republic_ser
			}
			###Subject VOJ Puppet###
			if = {
				limit = {
					original_tag = VOJ
					NOT = { has_autonomy_state = autonomy_autonomous_province }
					NOT = { has_country_flag = VOJ_new_autonomy }
					is_subject_of = SER
				}
				SER = {
					set_autonomy = {
						target = VOJ
						autonomy_state = autonomy_autonomous_province
					}
				}
			}
			#Vojvodina start Rebelleion
			if = {
				limit = {
					original_tag = VOJ
					check_variable = { VOJ.voj_separ > 55 }
					VOJ = {	has_country_flag = start_reb_voj }
					OR = {
						VOJ = {	has_autonomy_state = autonomy_autonomous_province1 }
						VOJ = {	has_autonomy_state = autonomy_autonomous_province }
					}
				}
				set_country_flag = start_reb_voj
				VOJ = {
					country_event = {
						id = SerbiaFocus.99
						days = 1
					}
				}
			}
		}
	}
}
# If in 210 days Montenegrin Sepereatism is not 70%, then you get the event for violence.
# Then a fail-safe check in case the player changed their mind mid way, to see if they aren't 70% in 420 days.
# If movement goes violent, player will need 0% Montenegrin seperatism and less than 500 Montenegrin combatants