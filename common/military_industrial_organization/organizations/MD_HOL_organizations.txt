HOL_dutch_defense_vehicle_systems_tank_manufacturer = {
	allowed = { original_tag = HOL }
	icon = GFX_idea_Dutch_Defense_Vehicle_Systems_HOL
	name = HOL_dutch_defense_vehicle_systems_tank_manufacturer
	include = generic_specialized_tank_organization
}

HOL_damen_shipyards_naval_manufacturer = {
	allowed = { original_tag = HOL }
	icon = GFX_idea_Damen_Shipyards_HOL
	name = HOL_damen_shipyards_naval_manufacturer
	include = generic_naval_equipment_organization
}
