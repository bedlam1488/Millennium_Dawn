SOV_military_industry_company_tank_manufacturer = {
	allowed = { original_tag = SOV }
	icon = GFX_idea_Military_Industry_Company_SOV
	name = SOV_military_industry_company_tank_manufacturer
	# TODO: Replace with artillery MIO
	include = generic_tank_equipment_organization
}

SOV_rostec_tank_manufacturer = {
	allowed = { original_tag = SOV }
	icon = GFX_idea_Rostec_SOV
	name = SOV_rostec_tank_manufacturer
	include = generic_tank_equipment_organization
}

SOV_kurganmashzavod_tank_manufacturer = {
	allowed = { original_tag = SOV }
	icon = GFX_idea_Kurganmashzavod_SOV
	name = SOV_kurganmashzavod_tank_manufacturer
	include = generic_specialized_tank_organization
}

SOV_rostec_materiel_manufacturer = {
	allowed = { original_tag = SOV }
	icon = GFX_idea_Rostec_SOV
	name = SOV_rostec_materiel_manufacturer
	include = generic_infantry_equipment_organization
}

SOV_almaz_antey_materiel_manufacturer = {
	allowed = { original_tag = SOV }
	icon = GFX_idea_Almaz_Antey_SOV
	name = SOV_almaz_antey_materiel_manufacturer
	include = generic_infantry_equipment_organization
}

SOV_jsc_defense_systems_materiel_manufacturer = {
	allowed = { original_tag = SOV }
	icon = GFX_idea_JSC_Defense_systems_SOV
	name = SOV_jsc_defense_systems_materiel_manufacturer
	include = generic_aa_at_equipment_organization
}

SOV_russian_helicopters_tank_manufacturer = {
	allowed = { original_tag = SOV }
	icon = GFX_idea_Russian_Helicopters_SOV
	name = SOV_russian_helicopters_tank_manufacturer
	include = generic_specialized_helicopter_organization
}

SOV_rostec_tank_manufacturer2 = {
	allowed = { original_tag = SOV }
	icon = GFX_idea_Rostec_SOV
	name = SOV_rostec_tank_manufacturer2
	include = generic_specialized_helicopter_organization
}

SOV_mil_helicopters_tank_manufacturer = {
	allowed = { original_tag = SOV }
	icon = GFX_idea_Mil_Helicopters_SOV
	name = SOV_mil_helicopters_tank_manufacturer
	include = generic_specialized_helicopter_organization
}

SOV_united_aircraft_corporation_aircraft_manufacturer = {
	allowed = { original_tag = SOV }
	icon = GFX_idea_United_Aircraft_Corporation_SOV
	name = SOV_united_aircraft_corporation_aircraft_manufacturer
	include = generic_fixed_wing_equipment_organization
}

SOV_irkut_aircraft_manufacturer = {
	allowed = { original_tag = SOV }
	icon = GFX_idea_Irkut_SOV
	name = SOV_irkut_aircraft_manufacturer
	# TODO: Replace with fighter MIO 
	include = generic_air_equipment_organization
}

SOV_united_shipbuilding_corporation_naval_manufacturer = {
	allowed = { original_tag = SOV }
	icon = GFX_idea_United_Shipbuilding_Corporation_SOV
	name = SOV_united_shipbuilding_corporation_naval_manufacturer
	include = generic_naval_equipment_organization
}

SOV_united_shipbuilding_corporation_naval_manufacturer2 = {
	allowed = { original_tag = SOV }
	icon = GFX_idea_United_Shipbuilding_Corporation_SOV
	name = SOV_united_shipbuilding_corporation_naval_manufacturer2
	include = generic_sub_equipment_organization
}