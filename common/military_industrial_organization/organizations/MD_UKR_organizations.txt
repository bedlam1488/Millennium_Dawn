UKR_kmdb_materiel_manufacturer = {
	allowed = { original_tag = UKR }
	icon = GFX_idea_KMDB_logo
	name = UKR_kmdb_materiel_manufacturer
	include = generic_infantry_equipment_organization
}

UKR_kmdb_tank_manufacturer = {
	allowed = { original_tag = UKR }
	icon = GFX_idea_KMDB_logo
	name = UKR_kmdb_tank_manufacturer
	include = generic_tank_equipment_organization
}

UKR_malyshev_factory_tank_manufacturer = {
	allowed = { original_tag = UKR }
	icon = GFX_idea_Malyshev_Factory_logo
	name = UKR_malyshev_factory_tank_manufacturer
	include = generic_tank_equipment_organization
}

UKR_antonov_aircraft_manufacturer = {
	allowed = { original_tag = UKR }
	icon = GFX_idea_Antonov_logo
	name = UKR_antonov_aircraft_manufacturer
	include = generic_fixed_wing_equipment_organization
}

UKR_nikolayev_shipyard_naval_manufacturer = {
	allowed = { original_tag = UKR }
	icon = GFX_idea_Nikolayev
	name = UKR_nikolayev_shipyard_naval_manufacturer
	include = generic_naval_light_equipment_organization
}