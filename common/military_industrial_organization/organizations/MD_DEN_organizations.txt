DEN_falck_schmidt_tank_manufacturer = {
	allowed = { original_tag = DEN }
	icon = GFX_idea_Falk_Schmidt
	name = DEN_falck_schmidt_tank_manufacturer
	include = generic_tank_equipment_organization
}

################
### Infantry ###
################

DEN_falck_schmidt_materiel_manufacturer = {
	allowed = { original_tag = DEN }
	icon = GFX_idea_Falk_Schmidt
	name = DEN_falck_schmidt_materiel_manufacturer
	include = generic_infantry_equipment_organization
}

DEN_DISA_materiel_manufacturer = {
	allowed = { original_tag = DEN }
	icon = GFX_idea_DISA
	name = DEN_DISA_materiel_manufacturer
	include = generic_infantry_equipment_organization
}

##############
### Marine ###
##############

DEN_Svendborg_skibsvaerft_naval_manufacturer = {
	allowed = { original_tag = DEN }
	icon = GFX_idea_Svendborg_skibsvaerft
	name = DEN_Svendborg_skibsvaerft
	include = generic_naval_light_equipment_organization
}

DEN_Odense_Staalskibsvaerft_naval_manufacturer = {
	allowed = { original_tag = DEN }
	icon = GFX_idea_Odense_Staalskibsvaerft
	name = DEN_Odense_Staalskibsvaerft
	include = generic_naval_equipment_organization
}

###############
### Vehicle ###
###############

DEN_Composhield = {
	allowed = { original_tag = DEN }
	icon = GFX_idea_Composhield
	name = DEN_Composhield_utility_vehicle_manufacturer
	include = GER_generic_utility_vehicle_manufacturer

	initial_trait = {
		name = GER_generic_utility_vehicle_manufacturer

		organization_modifier = {
			military_industrial_organization_research_bonus = 0.05
		}

		equipment_bonus = {
			reliability = 0.07
		}

		production_bonus = {
			production_efficiency_cap_factor = 0.03
		}
	}
}

#################
### Air Force ###
#################

DEN_Terma_AS_aircraft_manufacturer = {
	allowed = { original_tag = DEN }
	icon = GFX_idea_Terma_AS
	name = DEN_Terma_AS_aircraft_manufacturer
	include = generic_air_equipment_organization
}

