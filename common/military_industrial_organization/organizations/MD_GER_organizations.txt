## MATERIAL DESIGNERS ##
GER_rheinmetall_materiel_manufacturer = {
	allowed = { original_tag = GER }
	icon = GFX_idea_Rheinmetall
	name = GER_rheinmetall_materiel_manufacturer
	include = generic_infantry_equipment_organization
}

GER_heckler_koch_materiel_manufacturer = {
	allowed = { original_tag = GER }
	icon = GFX_idea_HK
	name = GER_heckler_koch_materiel_manufacturer
	include = GER_generic_small_arms_manufacturer
}

GER_SIG_Sauer_materiel_manufacturer = {
	allowed = { original_tag = GER }
	icon = GFX_idea_SIG_Sauer
	name = GER_SIG_Sauer_materiel_manufacturer_name
	include = GER_generic_small_arms_manufacturer
}

## TANK DESIGNER SECTION ##
GER_rheinmetall_tank_manufacturer = {
	allowed = { original_tag = GER }
	icon = GFX_idea_Rheinmetall
	name = GER_rheinmetall_tank_manufacturer
	include = generic_tank_equipment_organization

	initial_trait = {
		name = generic_mio_initial_trait_md_tank_manufacturer

		organization_modifier = {
			military_industrial_organization_research_bonus = 0.05
		}

		equipment_bonus = {
			reliability = 0.07
			armor_value = 0.05
		}

		production_bonus = {
			production_efficiency_cap_factor = 0.06
		}
	}

	trait = {
		token = GER_mio_trait_enhanced_fire_package
		name = GER_mio_trait_enhanced_fire_package
		icon = GFX_generic_mio_trait_icon_ap_attack

		position = { x = 1 y = 0 }

		all_parents = { generic_mio_tank_trait_heavy_tank_stats_2 }
		relative_position_id = generic_mio_tank_trait_heavy_tank_stats_2

		equipment_bonus = {
			hard_attack = 0.1
			ap_attack = 0.1
		}

		limit_to_equipment_type = { mbt_hull }

		on_complete = {
			FROM = {
				medium_expenditure = yes
			}
		}

		ai_will_do = {
			factor = 50
			modifier = {
				factor = 0.1
				FROM = {
					num_of_military_factories < 10
				}
			}
		}
	}
}

GER_krauss_maffei_tank_manufacturer = {
	allowed = { original_tag = GER }
	icon = GFX_idea_KMW
	name = GER_krauss_maffei_tank_manufacturer
	include = generic_tank_equipment_organization

	initial_trait = {
		name = generic_mio_initial_trait_md_tank_manufacturer

		organization_modifier = {
			military_industrial_organization_research_bonus = 0.05
		}

		equipment_bonus = {
			armor_value = 0.05
			maximum_speed = 0.06
			defense = 0.05
		}

		production_bonus = {
			production_efficiency_cap_factor = 0.03
			production_cost_factor = 0.05
		}
	}
}

GER_BMW = {
	allowed = { original_tag = GER }
	icon = GFX_idea_BMW
	name = GER_BMW_name
	include = GER_generic_utility_vehicle_manufacturer

	initial_trait = {
		name = GER_generic_utility_vehicle_manufacturer

		organization_modifier = {
			military_industrial_organization_research_bonus = 0.05
		}

		equipment_bonus = {
			maximum_speed = 0.04
		}

		production_bonus = {
			production_efficiency_cap_factor = 0.06
		}
	}
}

GER_Mercedes = {
	allowed = { original_tag = GER }
	icon = GFX_idea_Mercedes_Benz
	name = GER_Mercedes_Benz_name
	include = GER_generic_utility_vehicle_manufacturer

	initial_trait = {
		name = GER_generic_utility_vehicle_manufacturer

		organization_modifier = {
			military_industrial_organization_research_bonus = 0.05
		}

		equipment_bonus = {
			reliability = 0.07
		}

		production_bonus = {
			production_efficiency_cap_factor = 0.03
		}
	}
}

GER_artec = {
	allowed = { original_tag = GER }
	icon = GFX_idea_ARTEC
	name = GER_artec_name
	include = generic_tank_equipment_organization

	initial_trait = {
		name = generic_mio_initial_trait_md_tank_manufacturer

		organization_modifier = {
			military_industrial_organization_research_bonus = 0.05
		}

		equipment_bonus = {
			maximum_speed = 0.06
			breakthrough = 0.04
		}

		production_bonus = {
			production_efficiency_cap_factor = 0.03
		}
	}
}

GER_dynamit_nobel = {
	allowed = { original_tag = GER }
	icon = GFX_idea_Dynamit_Nobel
	name = GER_dynamit_nobel_name
	include = generic_aa_at_equipment_organization
}

## AIRCRAFT DESIGNERS ##
#GER_airbus_helicopters_tank_manufacturer = {
#	allowed = { original_tag = GER }
#	icon = GFX_idea_Airbus_helicopters
#	name = GER_airbus_helicopters_tank_manufacturer
#	include = generic_specialized_helicopter_organization
#}

GER_Eurocopter_helicopters_tank_manufacturer = {
	allowed = { original_tag = GER }
	icon = GFX_idea_eurocopter
	name = GER_Eurocopter_helicopters_tank_manufacturer_name
	include = generic_specialized_helicopter_organization

	initial_trait = {
		name = generic_specialized_helicopter_organization

		organization_modifier = {
			military_industrial_organization_research_bonus = 0.05
		}

		equipment_bonus = {
			reliability = -0.05
			defense = 0.05
			breakthrough = 0.05
		}

		production_bonus = {
			production_cost_factor = 0.05
		}
	}
}

GER_MBB_helicopters_tank_manufacturer = {
	allowed = { original_tag = GER }
	icon = GFX_idea_MBB
	name = GER_MBB_helicopters_tank_manufacturer_name
	include = generic_specialized_helicopter_organization

	initial_trait = {
		name = generic_specialized_helicopter_organization

		organization_modifier = {
			military_industrial_organization_research_bonus = 0.05
		}

		equipment_bonus = {
			reliability = 0.07
		}

		production_bonus = {
			production_efficiency_cap_factor = 0.03
		}
	}
}

GER_airbus_defence_aircraft_manufacturer = {
	allowed = { original_tag = GER }
	icon = GFX_idea_Airbus_Defence
	name = GER_airbus_defence_aircraft_manufacturer
	include = generic_fixed_wing_equipment_organization
}

GER_Dornier_aircraft_manufacturer = {
	allowed = { original_tag = GER }
	icon = GFX_idea_dornier
	name = GER_Dornier_aircraft_manufacturer_name
	include = generic_fixed_wing_equipment_organization
}

## NAVAL DESIGNERS ##
GER_thyssenkrupp_marine_naval_manufacturer = {
	allowed = { original_tag = GER }
	icon = GFX_idea_Thyssen_Krupp
	name = GER_thyssenkrupp_marine_naval_manufacturer
	include = generic_naval_light_equipment_organization
}

GER_lurssen_werft_naval_manufacturer = {
	allowed = { original_tag = GER }
	icon = GFX_idea_Lurssen
	name = GER_lurssen_werft_naval_manufacturer
	include = generic_naval_light_equipment_organization
}

#GER_thyssenkrupp_marine_naval_manufacturer2 = {
#	allowed = { original_tag = GER }
#	icon = GFX_idea_Thyssen_Krupp
#	name = GER_thyssenkrupp_marine_naval_manufacturer2
#	include = generic_sub_equipment_organization
#}

GER_HDW_naval_manufacturer = {
	allowed = { original_tag = GER }
	icon = GFX_idea_HDW
	name = GER_HDW_naval_manufacturer_name
	include = generic_sub_equipment_organization
}

GER_blohm_and_voss = {
	allowed = { original_tag = GER }
	icon = GFX_idea_Blohm_and_Voss
	name = GER_blohm_and_voss_name
	include = generic_naval_equipment_organization
}
