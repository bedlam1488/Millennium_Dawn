#### COMBAT TACTICS ###########################################################
#
# NOTE: tactics must start with "tactic_" in their name to work with techs.
###############################################################################
# recon phase is phase = no
# main phase is phase = main
# recon phase simulates forces probing and trying to get into a specific phase
# main phase is the actual joined battle phase, and is the full engagement
phases = {
	main
	thunder_run #rapid breakthrough tank forces / mobile forces
	deep_battle #large formations punching deep through enemy lines
	close_combat
	tactical_withdrawal
	seize_bridge
	hold_bridge
	city_combat
	insurgency #insurgency ops, reduce cbt width, reduce damage, ect
}

### Recon tactics ###
tactic_basic_recon_attack = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = no
	}

	active = yes

	base = { factor = 4 }
	picture = attack

	attacker = -0.1
	defender = -0.1
}

tactic_aggresive_recon = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = no
	}

	active = yes

	base = {
		factor = 4
		modifier = {
			add = 2
			has_trait = aggressive_assaulter
		}
		modifier = {
			add = 1
			has_trait = harsh_leader
		}
	}
	picture = assault

	attacker = -0.03
	defender = -0.05

	countered_by = tactic_recon_ambush
}

tactic_limited_recon = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = no
	}

	active = yes

	base = {
		factor = 4
		modifier = {
			add = 2
			has_trait = thorough_planner
		}
		modifier = {
			add = 2
			has_trait = kind_leader
		}
	}
	picture = delay

	attacker = -0.15
	defender = -0.2

	countered_by = tactic_recon_hunter_packs
}

tactic_supression_advance_recon = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = no
	}

	active = no

	base = {
		factor = 4
		modifier = {
			add = 2
			has_trait = artillery_expert
		}
	}

	picture = tactic_barrage

	attacker = -0.05
	defender = -0.1

	countered_by = tactic_recon_counter_battery
}

tactic_mobile_recon = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = no
	}

	active = no

	base = {
		factor = 4
		modifier = {
			add = 2
			has_trait = infantry_expert
		}
	}

	picture = tactic_unexpected_thrust

	attacker = -0.05
	defender = -0.05
	attacker_movement_speed = 0.1

	countered_by = tactic_recon_node_defense
}

tactic_breakthrough_recon = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = no
	}

	# changes stage
	active = yes

	base = {
		factor = 2
		modifier = {
			add = 2
			has_trait = aggressive_assaulter
		}
	}

	picture = breakthrough

	phase = main

	defender = -0.05
	attacker_movement_speed = 0.15

	countered_by = tactic_recon_node_defense
}

tactic_pincer_recon = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = no
		OR = {
			skill_advantage > 1
			skill > 2
			has_trait = trickster
		}
	}

	# changes stage
	active = no

	base = {
		factor = 2
		modifier = {
			add = 2
			has_trait = panzer_expert
		}
		modifier = {
			add = 1
			has_trait = combined_arms_expert
		}
		modifier = {
			add = 1
			has_trait = armoured_cavalry_expert
		}
	}

	picture = encircle

	phase = main

	defender = -0.05
	attacker_movement_speed = 0.15

	countered_by = tactic_recon_ambush
}

tactic_basic_recon_defense = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = no
	}

	active = yes

	base = { factor = 4 }
	picture = defend

	attacker = -0.1
	defender = -0.1
}

tactic_recon_ambush = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = no
	}

	active = yes

	base = {
		factor = 4
		modifier = {
			add = 2
			has_trait = trickster
		}
	}
	picture = ambush

	countered_by = tactic_limited_recon

	attacker = -0.2
	defender = -0.15
}

tactic_recon_hunter_packs = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = no
	}

	active = yes

	base = { factor = 4 }

	picture = counterattack

	countered_by = tactic_aggresive_recon

	attacker = -0.05
	defender = -0.05
}

tactic_recon_counter_battery = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = no
	}

	active = yes

	base = {
		factor = 4
		modifier = {
			add = 2
			has_trait = artillery_expert
		}
	}

	picture = tactic_overwhelming_fire

	countered_by = tactic_mobile_recon

	attacker = -0.1
	defender = -0.05
}

tactic_recon_node_defense = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = no
	}

	active = yes

	base = {
		factor = 4
		modifier = {
			add = 2
			has_trait = defensive_doctrine
		}
	}

	picture = elastic_defence

	countered_by = tactic_supression_advance_recon

	attacker = -0.1
	defender = -0.05
}

tactic_recon_insurgency_attack = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = no
	}

	active = no

	base = { factor = 4 }

	picture = attack

	attacker = -0.1
	defender = -0.05
	combat_width = -0.50
	phase = insurgency
}

tactic_recon_insurgency_defender = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = no
	}

	active = no

	base = { factor = 4 }

	picture = attack

	attacker = -0.1
	defender = -0.05
	combat_width = -0.50
	phase = insurgency
}

#####################
### Basic Tactitcs###
#####################

tactic_basic_attack = {
	is_attacker = yes
	trigger = { # combined scope for both country and combatant
		is_attacker = yes
		phase = main
	}

	active = yes

	base = { factor = 4 }
	picture = attack

	countered_by = tactic_counterattack #You can only be countered by 1 tactic

	attacker = 0.05
}

tactic_basic_defend = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = main
	}

	active = yes

	base = { factor = 4 }
	picture = defend

	defender = 0.05
	#countered_by = tactic_insurgent_assault
}

tactic_counterattack = { # counters basic attack, assault
	is_attacker = no
	trigger = {
		is_attacker = no
		skill_advantage > 0
		phase = main
	}

	active = yes

	base = { factor = 4 }
	picture = counterattack

	defender = 0.25
}

tactic_assault = { #starts close_combat phase
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = main
	}

	active = yes

	base = {
		factor = 1
		modifier = {
			add = 1
			has_trait = aggressive_assaulter
		}
		modifier = {
			add = 2
			is_fighting_in_terrain = urban
		}
		modifier = {
			add = 2
			is_fighting_in_terrain = supercity
		}
		modifier = {
			add = 1
			is_fighting_in_terrain = forest
		}
		modifier = {
			add = 1
			has_trait = harsh_leader
		}
	}

	picture = assault
	countered_by = tactic_counterattack

	phase = close_combat #changes phase to close combat

	attacker = 0.25			#attacker gets a bonus to doing damage
}

tactic_breakthrough = { #counters ambush
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = main
		OR = {
			hardness > 0.35
			skill_advantage > 1
		}
	}

	active = no

	base = {
		factor = 4
		modifier = {
			add = 1
			is_fighting_in_terrain = plains
		}
	}
	picture = breakthrough

	countered_by = tactic_backhand_blow

	attacker_movement_speed = 0.5
	attacker = 0.25
	defender = -0.15
}

tactic_ambush = { #counters shock
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = main
		OR = {
			skill_advantage > 1
			skill > 2
			has_trait = trickster
		}
	}

	active = yes

	base = {
		factor = 4
		modifier = {
			add = 1
			is_fighting_in_terrain = forest
		}
	}
	picture = ambush
	countered_by = tactic_breakthrough

	attacker = -0.25
}

tactic_backhand_blow = { #counters breakthrough
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = main
		OR = {
			skill > 4
			AND = {
				has_trait = defensive_doctrine
				skill > 3
			}
		}
	}

	base = { factor = 4 }
	picture = backhand_blow

	attacker_movement_speed = -0.3
	attacker = -0.2
	defender = 0.25
}

tactic_encirclement = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = main
		frontage_full = yes
		has_reserves = yes
		OR = {
			skill_advantage > 0
			has_trait = panzer_leader
			has_trait = trickster
		}
	}

	active = yes

	base = { factor = 4 }
	picture = encircle
	countered_by = tactic_tactical_withdrawal

	combat_width = 0.5		#increased combat_width
	attacker = 0.25
	defender = 0.05
}

tactic_delay = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = main
	}

	active = no

	base = { factor = 4 }
	picture = delay
	countered_by = tactic_shock

	attacker_movement_speed = -0.25		#
	attacker = -0.25
	defender = -0.15
}

tactic_shock = { #counters delay
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = main
	}

	active = yes

	base = { factor = 4 }
	picture = shock
	countered_by = tactic_ambush

	defender = -0.25
}

tactic_tactical_withdrawal = { #counters encirclement - starts tactical_withdrawal phase
	is_attacker = no
	trigger = {
		is_attacker = no
		OR = {
			phase = main
			phase = thunder_run
			phase = deep_battle
		}
		OR = {
			skill_advantage > 0
			has_trait = trickster
		}
	}

	active = yes

	base = { factor = 4 }
	picture = withdraw

	phase = tactical_withdrawal

	combat_width = -0.25
	attacker = -0.25
	defender = -0.05
}

city_attack_movement = { #counters encirclement - starts city_combat phase
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = main
	}

	active = yes

	base = {
		factor = 0
		modifier = {
			add = 5
			is_fighting_in_terrain = urban
		}
		modifier = {
			add = 10
			is_fighting_in_terrain = supercity
		}
	}
	picture = shock

	phase = city_combat

	attacker = -0.05
	defender = -0.05
}

tactic_thunder_run_breakthrough = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = main
	}

	#modify this to change chances of this occurning
	base = { factor = 4 }

	active = no
	picture = attack

	attacker = 0.15

	phase = thunder_run
}

tactic_deep_battle_breakthrough = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = main
	}

	base = { factor = 4 }

	active = no
	picture = blitz

	attacker = 0.10
	attacker_movement_speed = 0.05

	phase = deep_battle
}

tactic_insurgency_attacker = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = main
	}

	active = no

	base = { factor = 4 }

	picture = attack

	attacker = -0.1
	defender = -0.05
	combat_width = -0.50
	phase = insurgency
}

tactic_insurgency_defender = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = main
	}

	active = no

	base = { factor = 4 }

	picture = attack

	attacker = -0.1
	defender = -0.05
	combat_width = -0.50
	phase = insurgency
}

tactic_flexible_lines = {
	is_attacker = no
	trigger = {
		is_attacker = no
		OR = {
			phase = main
			phase = deep_battle
			phase = thunder_run
		}
	}

	active = no
	base = { factor = 4 }

	attacker = -0.05
	defender = 0.15
	combat_width = -0.05
	attacker_movement_speed = -0.10
}

tactic_dedicated_emplacements = {
	is_attacker = no
	trigger = {
		is_attacker = no
		OR = {
			phase = main
			phase = deep_battle
			phase = thunder_run
		}
	}

	active = no
	base = { factor = 4 }

	attacker = -0.25
	defender = 0.20
}

#close combat tactics here
tactic_cc_attack = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = close_combat
	}

	active = yes

	base = { factor = 4 }
	picture = cc_attack
	display_phase = close_combat

	attacker = 0.1			#attacker gets a bonus to doing damage
	defender = 0.05
}

tactic_cc_defend = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = close_combat
	}

	active = yes

	base = { factor = 4 }
	picture = cc_defend
	display_phase = close_combat

	attacker = 0.05
	defender = 0.1			#defender gets a bonus to doing damage
}

tactic_cc_storm = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = close_combat
	}

	active = yes

	base = { factor = 2 }
	picture = cc_storm
	display_phase = close_combat

	attacker = 0.2			#attacker gets a bonus to doing damage
	defender = 0.2
}

tactic_cc_local_strong_point = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = close_combat
	}

	active = yes
	display_phase = close_combat

	base = { factor = 2 }
	picture = cc_local_strong_point

	attacker = -0.2			#attacker gets a penalty to doing damage
}

tactic_cc_withdraw = { #changes phase back to no
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = close_combat
	}

	active = yes

	base = { factor = 1 }
	picture = cc_withdraw
	display_phase = close_combat

	phase = main #changes phase to no

	attacker = -0.05
	defender = -0.05
}

#tactical withdrawal tactics
tactic_tw_attack = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = tactical_withdrawal
	}

	base = { factor = 4 }
	picture = tw_attack
	display_phase = tactical_withdrawal

	combat_width = -0.25
	attacker = -0.25
	defender = -0.1
}

tactic_tw_defend = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = tactical_withdrawal
	}

	base = { factor = 4 }
	picture = tw_defend
	display_phase = tactical_withdrawal

	combat_width = -0.25
	attacker = -0.3
	defender = -0.05
}

tactic_tw_chase = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = tactical_withdrawal
	}

	base = { factor = 4 }
	picture = tw_chase
	display_phase = tactical_withdrawal


	combat_width = -0.25
	attacker = -0.15
	defender = -0.05
}

tactic_tw_evade = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = tactical_withdrawal
	}

	base = { factor = 4 }
	picture = tw_evade
	display_phase = tactical_withdrawal

	countered_by = tactic_tw_intercept

	phase = no

	combat_width = -0.25
	attacker = -0.4
	defender = -0.1
}

tactic_tw_intercept = { #changes phase to no
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = tactical_withdrawal
	}

	base = { factor = 4 }
	picture = tw_intercept
	display_phase = tactical_withdrawal

	phase = main

	attacker = -0.05
	defender = -0.1
}

#Siege bridge phase
tactic_seize_bridge = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		has_combat_modifier = river_crossing
		phase = main
		OR = {
			skill > 3
			AND = {
				has_trait = offensive_doctrine
				skill > 2
			}
		}
	}

	base = { factor = 2 }
	picture = seize_bridge

	phase = seize_bridge

	attacker_movement_speed = 0.1
	combat_width = -0.25
	attacker = 0.2
	defender = -0.05
}

tactic_attacker_sb_hold = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = seize_bridge
	}

	base = { factor = 4 }
	picture = attacker_sb_hold
	display_phase = seize_bridge

	combat_width = -0.25
	attacker = 0.2
}

tactic_attacker_sb_skillful_defence = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = seize_bridge
		skill > 4
	}

	base = { factor = 4 }
	picture = attacker_sb_skillful_defence
	display_phase = seize_bridge

	combat_width = -0.25
	attacker = 0.2
	defender = -0.1
}

tactic_defender_sb_assault = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = seize_bridge
	}

	base = { factor = 4 }
	picture = defender_sb_assault
	display_phase = seize_bridge

	combat_width = -0.25
	defender = -0.05
}

tactic_defender_sb_reckless_assault = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = seize_bridge
		skill < 3
	}

	base = { factor = 4 }
	picture = defender_sb_reckless_assault
	display_phase = seize_bridge

	combat_width = -0.25
	attacker = 0.25
	defender = -0.1
}

tactic_defender_sb_retake_bridge = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = seize_bridge
		OR = {
			skill > 2
			has_trait = trickster
		}
	}

	countered_by = tactic_attacker_sb_skillful_defence
	phase = hold_bridge

	base = { factor = 4 }
	picture = defender_sb_retake_bridge
	display_phase = seize_bridge

	combat_width = -0.25
	attacker = 0.1
	defender = -0.05
}

tactic_hold_bridge = {
	is_attacker = no
	trigger = {
		has_combat_modifier = river_crossing
		is_attacker = no
		phase = main
		OR = {
			skill > 2
			has_trait = defensive_doctrine
		}
	}

	base = { factor = 2 }
	picture = tactic_hold_bridge

	phase = hold_bridge

	attacker_movement_speed = 0.1
	combat_width = -0.25
	attacker = 0.2
	defender = -0.05
}

#hold_bridge phase
tactic_attacker_hb_attack = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = hold_bridge
	}

	base = { factor = 4 }
	picture = attacker_hb_attack
	display_phase = hold_bridge

	combat_width = -0.25
	attacker = 0.1
}

tactic_attacker_hb_rush = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = hold_bridge
		skill > 4
	}

	base = { factor = 4 }
	picture = attacker_hb_rush
	display_phase = hold_bridge

	combat_width = -0.25
	attacker = 0.2
}

tactic_attacker_hb_storm = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = hold_bridge
	}

	base = { factor = 2 }
	picture = attacker_hb_storm
	display_phase = hold_bridge

	countered_by = tactic_defender_hb_skillful_defence

	phase = seize_bridge

	combat_width = -0.25
	attacker = 0.2
	defender = 0.05
}

tactic_defender_hb_hold = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = hold_bridge
		skill < 3
	}

	base = { factor = 2 }
	picture = defender_hb_hold
	display_phase = hold_bridge

	combat_width = -0.25
	attacker = 0.2
	defender = -0.1
}

tactic_defender_hb_skillful_defence = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = hold_bridge
		OR = {
			skill > 2
			has_trait = trickster
		}
	}

	base = { factor = 2 }
	picture = defender_hb_skillful_defence
	display_phase = hold_bridge

	combat_width = -0.25
	attacker = 0.1
	defender = 0.05
}

#################
### Doctrines ###
#################

# city combat tactics
tactic_city_attack = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = city_combat
	}

	active = yes

	base = { factor = 4 }
	picture = attack
	display_phase = city_combat

	attacker = 0.05
}

tactic_city_fixing_attack = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = city_combat
	}

	active = yes

	base = {
		factor = 4
		modifier = {
			add = 2
			has_trait = trickster
		}
	}
	picture = tactic_unexpected_thrust
	display_phase = city_combat

	countered_by = tactic_city_all_round_defense

	attacker = 0.05
	defender = -0.05
}

tactic_city_frontal_attack = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = city_combat
	}

	active = yes

	base = {
		factor = 4
		modifier = {
			add = 2
			has_trait = aggressive_assaulter
		}
		modifier = {
			add = 1
			has_trait = harsh_leader
		}
	}
	picture = shock
	display_phase = city_combat

	attacker = 0.1
	defender = 0.1
}

tactic_city_infiltration_attack = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = city_combat
	}

	active = no

	base = { factor = 4 }
	picture = tactic_infantry_charge
	display_phase = city_combat

	countered_by = tactic_city_layered_defense

	attacker = 0.15
	defender = 0.05
}

tactic_city_node_attack = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = city_combat
	}

	active = no

	base = { factor = 4 }
	picture = breakthrough
	display_phase = city_combat

	attacker = 0.1
}

tactic_city_defense = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = city_combat
	}

	active = yes

	base = { factor = 4 }
	picture = defend
	display_phase = city_combat

	defender = 0.05
}

tactic_city_layered_defense = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = city_combat
	}

	active = yes

	base = {
		factor = 4
		modifier = {
			add = 2
			has_trait = defensive_doctrine
		}
	}
	picture = elastic_defence
	display_phase = city_combat

	defender = -0.05
	attacker = -0.1
}

tactic_city_static_defense = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = city_combat
	}

	active = yes

	base = { factor = 4 }
	picture = ambush
	display_phase = city_combat

	countered_by = tactic_city_node_attack

	defender = 0.1
	attacker = 0.05
}

tactic_city_all_round_defense = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = city_combat
	}

	active = no

	base = { factor = 4 }
	picture = encircle
	display_phase = city_combat

	defender = 0.1
	attacker = 0.05
}

tactic_city_crossfire_defense = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = city_combat
	}

	active = no

	base = { factor = 4 }
	picture = counterattack
	display_phase = city_combat

	countered_by = tactic_city_fixing_attack

	defender = 0.1
}

# Thunder Run Tactics
tactic_thunder_run = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = thunder_run
	}

	active = yes

	base = { factor = 4 }
	picture = attack
	display_phase = thunder_run

	countered_by = tactic_tactical_withdrawal

	attacker = 0.25
	defender = -0.15
	combat_width = 0.50
	attacker_movement_speed = 0.50
}

tactic_thunder_run_push = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = thunder_run
	}

	active = yes
	picture = shock
	display_phase = thunder_run

	attacker = 0.15
	combat_width = 0.10
	attacker_movement_speed = 0.10
}

tactic_thunder_run_maneuver = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = thunder_run
	}

	active = yes
	picture = flank
	display_phase = thunder_run

	attacker = 0.05
	combat_width = 0.25
	attacker_movement_speed = 0.20
}

tactic_thunder_run_retreat = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = thunder_run
	}
    
	picture = cc_defend
	attacker = -0.05
	defender = 0.15
	attacker_movement_speed = 0.05

	active = yes
	display_phase = thunder_run
}

tactic_thunder_run_delay = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = thunder_run
	}

	active = yes
	picture = defend
	display_phase = thunder_run

	defender = 0.10
	attacker_movement_speed = -0.15
}

# Deep Battle
tactic_deep_battle_push = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = deep_battle
	}

	active = yes
	display_phase = deep_battle
	picture = cc_attack

	defender = -0.10
	attacker = 0.10
	attacker_movement_speed = 0.15
	combat_width = 0.10

	countered_by = tactic_deep_battle_hold
}

tactic_deep_battle_assault = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = deep_battle
	}

	active = no
	display_phase = deep_battle
	picture = breakthrough

	attacker = 0.25
	defender = -0.15

	countered_by = tactic_deep_battle_counter_attack
}

tactic_deep_battle_encirclement = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = deep_battle
	}

	active = yes
	picture = encircle

	attacker = 0.15
	defender = 0.10
	combat_width = 0.50
	attacker_movement_speed = 0.25

	countered_by = tactic_tactical_withdrawal
}

tactic_deep_battle_rush = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = deep_battle
	}

	active = no
	display_phase = deep_battle
	picture = assault

	attacker = 0.15
	attacker_movement_speed = 0.40
}

tactic_deep_battle_retreat = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = deep_battle
	}

	active = yes
	picture = ambush

	attacker = -0.05
	defender = 0.05
	attacker_movement_speed = -0.25

	countered_by = tactic_deep_battle_rush
}

tactic_deep_battle_hold = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = deep_battle
	}

	active = yes
	display_phase = deep_battle

	attacker = -0.1
	defender = 0.25

	countered_by = tactic_deep_battle_assault
}

tactic_deep_battle_counter_attack = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = deep_battle
	}

	active = yes
	display_phase = deep_battle

	defender = 0.25
	combat_width = -0.25

	countered_by = tactic_deep_battle_encirclement
}

# Insurgency

tactic_insurgent_ied_strike = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = insurgency
	}

	active = no
	display_phase = insurgency

	combat_width = -0.75
	attacker = 0.50
	defender = -0.45
	attacker_movement_speed = -0.65
}

tactic_insurgent_raid = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = insurgency
	}

	active = no
	display_phase = insurgency

	combat_width = -0.75

	attacker = 0.35
	defender = -0.15

	countered_by = tactic_counter_foot_patrols
}

tactic_insurgent_vbied = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = insurgency
	}

	active = no
	display_phase = insurgency

	combat_width = -0.75
	attacker = 0.75
	defender = -0.50
}

tactic_insurgent_terror_strike = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = insurgency
	}

	active = no
	display_phase = insurgency

	combat_width = -0.75
	attacker = 0.25
	defender = -0.25
}

tactic_insurgent_base_attack = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = insurgency
	}

	active = yes
	display_phase = insurgency

	combat_width = -0.75

	attacker = 0.15
	defender = -0.15

	countered_by = tactic_counter_perimeter_defence
}

tactic_insurgent_tunnel_strike = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = insurgency
	}

	active = no
	display_phase = insurgency

	combat_width = -0.75
	attacker = 0.30
	defender = -0.25
}

tactic_insurgent_tunnel_hiding = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = insurgency
	}

	active = no
	display_phase = insurgency

	combat_width = -0.75

	countered_by = tactic_counter_tunnel_rats
}

tactic_insurgent_urban_hideouts = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = insurgency
		OR = {
			is_fighting_in_terrain = urban
			is_fighting_in_terrain = supercity
		}
	}

	active = no
	display_phase = insurgency

	combat_width = -0.75
	attacker = 0.50
	defender = -0.75
}

tactic_insurgent_sniper_ops = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = insurgency
	}

	active = no
	display_phase = insurgency

	combat_width = -0.75
	attacker = 0.25
	defender = -0.35

	countered_by = tactic_counter_cordon_search
}

tactic_insurgent_convoy_ambush = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = insurgency
	}

	active = no
	display_phase = insurgency

	combat_width = -0.75
	attacker = -0.25
	defender = 0.25
	attacker_movement_speed = -0.75

	countered_by = tactic_counter_react_to_contact
}

tactic_counter_ied = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = insurgency
	}

	active = yes
	display_phase = insurgency

	attacker = -0.50
	defender = -0.10
}

tactic_counter_foot_patrols = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = insurgency
	}

	defender = 0.25

	active = yes
	display_phase = insurgency
}

tactic_counter_stronghold_raid = {
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = insurgency
	}

	attacker = 0.35
	active = no

	display_phase = insurgency
}

tactic_counter_cordon_search = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = insurgency
	}

	defender = 0.25

	active = no
	display_phase = insurgency
}

tactic_counter_react_to_contact = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = insurgency
	}

	defender = 0.35
	attacker = -0.15

	active = yes
	display_phase = insurgency
}

tactic_counter_fireforce = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = insurgency
	}

	active = no
	display_phase = insurgency

	combat_width = 0.50
	attacker = -0.50
	defender = 0.25

	phase = main
}

tactic_counter_perimeter_defence = {
	is_attacker = no
	trigger = {
		is_attacker = no
		phase = insurgency
	}

	defender = 0.15

	active = yes
	display_phase = insurgency
}

tactic_counter_tunnel_rats = {
	#limit this to counter insurgency op units only
	is_attacker = yes
	trigger = {
		is_attacker = yes
		phase = insurgency
	}

	active = yes
	display_phase = insurgency

	attacker = 0.15
	defender = -0.30
}