defined_text = {
	name = african_union_mandate_status
	text = {
		trigger = {
			has_global_flag = african_union_mandate_granted
		}
		localization_key = african_union_mandate_status_green
	}
	text = {
		trigger = {
			NOT = { has_global_flag = african_union_mandate_granted }
		}
		localization_key = african_union_mandate_status_red
	}
}