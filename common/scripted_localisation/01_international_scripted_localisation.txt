# Author(s): AngriestBird, Doolittle
defined_text = {
	name = gfx_of_button

	text = { trigger = { check_variable = { v = 001 } }
		localization_key = PMC_blackwater_loc
	}
	text = { trigger = { check_variable = { v = 002 } }
		localization_key = PMC_aegis_loc
	}
	text = { trigger = { check_variable = { v = 003 } }
		localization_key = PMC_constellis_loc
	}
	text = { trigger = { check_variable = { v = 004 } }
		localization_key = PMC_wagner_loc
	}
	text = { trigger = { check_variable = { v = 005 } }
		localization_key = PMC_mpri_loc
	}
	text = { trigger = { check_variable = { v = 006 } }
		localization_key = PMC_sadat_loc
	}
	text = { trigger = { check_variable = { v = 007 } }
		localization_key = PMC_rsb_loc
	}
	text = { trigger = { check_variable = { v = 008 } }
		localization_key = PMC_sandline_loc
	}
	text = { trigger = { check_variable = { v = 009 } }
		localization_key = PMC_kbr_loc
	}
	text = { trigger = { check_variable = { v = 010 } }
		localization_key = PMC_nirtal_loc
	}
	text = { trigger = { check_variable = { v = 011 } }
		localization_key = PMC_halo_loc
	}
	text = { trigger = { check_variable = { v = 012 } }
		localization_key = PMC_defense_loc
	}
	text = { trigger = { check_variable = { v = 013 } }
		localization_key = PMC_gardaworld_loc
	}
	text = { trigger = { check_variable = { v = 014 } }
		localization_key = PMC_esa_loc
	}
	text = { trigger = { check_variable = { v = 015 } }
		localization_key = PMC_g4s_loc
	}
	text = { trigger = { check_variable = { v = 016 } }
		localization_key = PMC_asgard_loc
	}
	text = { trigger = { check_variable = { v = 017 } }
		localization_key = PMC_sinai_loc
	}
	text = { trigger = { check_variable = { v = 018 } }
		localization_key = PMC_gard_loc
	}
	text = { trigger = { check_variable = { v = 019 } }
		localization_key = PMC_patriot_loc
	}
	text = { trigger = { check_variable = { v = 020 } }
		localization_key = PMC_dewe_loc
	}
	text = { trigger = { check_variable = { v = 021 } }
		localization_key = PMC_over_loc
	}
	text = { trigger = { check_variable = { v = 022 } }
		localization_key = PMC_front_loc
	}
	text = { trigger = { check_variable = { v = 023 } }
		localization_key = PMC_redut_loc
	}
	text = { trigger = { check_variable = { v = 024 } }
		localization_key = PMC_gazprom_loc
	}
	text = { trigger = { check_variable = { v = 025 } }
		localization_key = PMC_iron_loc
	}
	text = { trigger = { check_variable = { v = 026 } }
		localization_key = PMC_sheriff_loc
	}
	text = { trigger = { check_variable = { v = 027 } }
		localization_key = PMC_lukoil_loc
	}
	text = { trigger = { check_variable = { v = 028 } }
		localization_key = PMC_rosneft_loc
	}
	text = { trigger = { check_variable = { v = 029 } }
		localization_key = PMC_sberbank_loc
	}
	text = { trigger = { check_variable = { v = 030 } }
		localization_key = PMC_vtb_loc
	}
	text = { trigger = { check_variable = { v = 031 } }
		localization_key = PMC_md_loc
	}
}

defined_text = {
	name = name_of_corp

	text = { trigger = { check_variable = { v = 001 } }
		localization_key = PMC_blackwater_name
	}
	text = { trigger = { check_variable = { v = 002 } }
		localization_key = PMC_aegis_name
	}
	text = { trigger = { check_variable = { v = 003 } }
		localization_key = PMC_constellis_name
	}
	text = { trigger = { check_variable = { v = 004 } }
		localization_key = PMC_wagner_name
	}
	text = { trigger = { check_variable = { v = 005 } }
		localization_key = PMC_mpri_name
	}
	text = { trigger = { check_variable = { v = 006 } }
		localization_key = PMC_sadat_name
	}
	text = { trigger = { check_variable = { v = 007 } }
		localization_key = PMC_rsb_name
	}
	text = { trigger = { check_variable = { v = 008 } }
		localization_key = PMC_sandline_name
	}
	text = { trigger = { check_variable = { v = 009 } }
		localization_key = PMC_kbr_name
	}
	text = { trigger = { check_variable = { v = 010 } }
		localization_key = PMC_nirtal_name
	}
	text = { trigger = { check_variable = { v = 011 } }
		localization_key = PMC_halo_name
	}
	text = { trigger = { check_variable = { v = 012 } }
		localization_key = PMC_defense_name
	}
	text = { trigger = { check_variable = { v = 013 } }
		localization_key = PMC_gardaworld_name
	}
	text = { trigger = { check_variable = { v = 014 } }
		localization_key = PMC_esa_name
	}
	text = { trigger = { check_variable = { v = 015 } }
		localization_key = PMC_g4s_name
	}
	text = { trigger = { check_variable = { v = 016 } }
		localization_key = PMC_asgard_name
	}
	text = { trigger = { check_variable = { v = 017 } }
		localization_key = PMC_sinai_name
	}
	text = { trigger = { check_variable = { v = 018 } }
		localization_key = PMC_gard_name
	}
	text = { trigger = { check_variable = { v = 019 } }
		localization_key = PMC_patriot_name
	}
	text = { trigger = { check_variable = { v = 020 } }
		localization_key = PMC_dewe_name
	}
	text = { trigger = { check_variable = { v = 021 } }
		localization_key = PMC_over_name
	}
	text = { trigger = { check_variable = { v = 022 } }
		localization_key = PMC_front_name
	}
	text = { trigger = { check_variable = { v = 023 } }
		localization_key = PMC_redut_name
	}
	text = { trigger = { check_variable = { v = 024 } }
		localization_key = PMC_gazprom_name
	}
	text = { trigger = { check_variable = { v = 025 } }
		localization_key = PMC_iron_name
	}
	text = { trigger = { check_variable = { v = 026 } }
		localization_key = PMC_sheriff_name
	}
	text = { trigger = { check_variable = { v = 027 } }
		localization_key = PMC_lukoil_name
	}
	text = { trigger = { check_variable = { v = 028 } }
		localization_key = PMC_rosneft_name
	}
	text = { trigger = { check_variable = { v = 029 } }
		localization_key = PMC_sberbank_name
	}
	text = { trigger = { check_variable = { v = 030 } }
		localization_key = PMC_vtb_name
	}
	text = { trigger = { check_variable = { v = 031 } }
		localization_key = PMC_md_name
	}
}

defined_text = {
	name = name_of_corp_desc
	text = { trigger = { check_variable = { v = 001 } }
		localization_key = PMC_blackwater_name_desc
	}
	text = { trigger = { check_variable = { v = 002 } }
		localization_key = PMC_aegis_name_desc
	}
	text = { trigger = { check_variable = { v = 003 } }
		localization_key = PMC_constellis_name_desc
	}
	text = { trigger = { check_variable = { v = 004 } }
		localization_key = PMC_wagner_name_desc
	}
	text = { trigger = { check_variable = { v = 005 } }
		localization_key = PMC_mpri_name_desc
	}
	text = { trigger = { check_variable = { v = 006 } }
		localization_key = PMC_sadat_name_desc
	}
	text = { trigger = { check_variable = { v = 007 } }
		localization_key = PMC_rsb_name_desc
	}
	text = { trigger = { check_variable = { v = 008 } }
		localization_key = PMC_sandline_name_desc
	}
	text = { trigger = { check_variable = { v = 009 } }
		localization_key = PMC_kbr_name_desc
	}
	text = { trigger = { check_variable = { v = 010 } }
		localization_key = PMC_nirtal_name_desc
	}
	text = { trigger = { check_variable = { v = 011 } }
		localization_key = PMC_halo_name_desc
	}
	text = { trigger = { check_variable = { v = 012 } }
		localization_key = PMC_defense_name_desc
	}
	text = { trigger = { check_variable = { v = 013 } }
		localization_key = PMC_gardaworld_name_desc
	}
	text = { trigger = { check_variable = { v = 014 } }
		localization_key = PMC_esa_name_desc
	}
	text = { trigger = { check_variable = { v = 015 } }
		localization_key = PMC_g4s_name_desc
	}
	text = { trigger = { check_variable = { v = 016 } }
		localization_key = PMC_asgard_name_desc
	}
	text = { trigger = { check_variable = { v = 017 } }
		localization_key = PMC_sinai_name_desc
	}
	text = { trigger = { check_variable = { v = 018 } }
		localization_key = PMC_gard_name_desc
	}
	text = { trigger = { check_variable = { v = 019 } }
		localization_key = PMC_patriot_name_desc
	}
	text = { trigger = { check_variable = { v = 020 } }
		localization_key = PMC_dewe_name_desc
	}
	text = { trigger = { check_variable = { v = 021 } }
		localization_key = PMC_over_name_desc
	}
	text = { trigger = { check_variable = { v = 022 } }
		localization_key = PMC_front_name_desc
	}
	text = { trigger = { check_variable = { v = 023 } }
		localization_key = PMC_redut_name_desc
	}
	text = { trigger = { check_variable = { v = 024 } }
		localization_key = PMC_gazprom_name_desc
	}
	text = { trigger = { check_variable = { v = 025 } }
		localization_key = PMC_iron_name_desc
	}
	text = { trigger = { check_variable = { v = 026 } }
		localization_key = PMC_sheriff_name_desc
	}
	text = { trigger = { check_variable = { v = 027 } }
		localization_key = PMC_lukoil_name_desc
	}
	text = { trigger = { check_variable = { v = 028 } }
		localization_key = PMC_rosneft_name_desc
	}
	text = { trigger = { check_variable = { v = 029 } }
		localization_key = PMC_sberbank_name_desc
	}
	text = { trigger = { check_variable = { v = 030 } }
		localization_key = PMC_vtb_name_desc
	}
	text = { trigger = { check_variable = { v = 031 } }
		localization_key = PMC_md_name_desc
	}
}

defined_text = {
	name = name_of_unit

	text = {
		trigger = {
			check_variable = { v = 001 }
			is_major_company = yes
		}
		localization_key = light_inf_loc
	}
	text = {
		trigger = {
			check_variable = { v = 002 }
			is_major_company = yes
		}
		localization_key = mot_inf_loc
	}
	text = {
		trigger = {
			check_variable = { v = 003 }
			is_major_company = yes
		}
		localization_key = mech_inf_loc
	}
	text = {
		trigger = {
			check_variable = { v = 004 }
			is_major_company = yes
		}
		localization_key = hvy_mech_inf_loc
	}
	text = {
		trigger = {
			check_variable = { v = 005 }
			is_major_company = yes
		}
		localization_key = spc_inf_loc
	}
	text = {
		trigger = {
			check_variable = { v = 006 }
			is_major_company = yes
		}
		localization_key = tank_loc
	}
	text = {
		trigger = {
			check_variable = { v = 007 }
			is_major_company = yes
		}
		localization_key = spec_op_loc
	}
	text = {
		trigger = {
			check_variable = { v = 008 }
		}
		localization_key = mini_pmc
	}
}