defined_text = { # Country
	name = GetKimJongNam
	text = {
		trigger = {
			original_tag = NKO
			has_country_flag = KimJongNam_Run_flag
		}
		localization_key = KimJongNam_BACK_NAME
	}
	text = {
		trigger = {
			original_tag = NKO
		}
		localization_key = KimJongNam_NAME
	}
}
defined_text = { # Country
	name = GetSuccessor
	text = {
		trigger = {
			check_variable = {
				var = Kim_Jong_Nam
				value = 75
				compare = greater_than_or_equals
			}
		}
		localization_key = Kim_Jong_Nam_Successor_tt
	}
	text = {
		trigger = {
			check_variable = {
				var = Kim_Jong_un
				value = 80
				compare = greater_than_or_equals
			}
		}
		localization_key = Kim_Jong_un_Successor_tt
	}
	text = {
		trigger = {
			NOT = {
				check_variable = {
					var = Kim_Jong_Nam
					value = 75
					compare = greater_than_or_equals
				}
				check_variable = {
					var = Kim_Jong_un
					value = 80
					compare = greater_than_or_equals
				}
			}
		}
		localization_key = None_Successor_tt
	}
}
defined_text = { # Country
	name = GetChosenSuccessor
	text = {
		trigger = {
			has_country_flag = Kim_Jong_Nam_successor
		}
		localization_key = Kim_Jong_Nam_Successor_tt
	}
	text = {
		trigger = {
			has_country_flag = Kim_Jong_un_successor
		}
		localization_key = Kim_Jong_un_Successor_tt
	}
	text = {
		trigger = {
		}
		localization_key = None_Successor_tt
	}
}