ai_weapon_dump = {
	if = {
		limit = {
			is_ai = yes
			has_war = no
			threat < 0.51
		}

		if = {
			limit = { is_debug = yes }
			log = "[GetDateText]: [THIS.GetName]: AI Weapon Dump - Trying to dump"
		}

		if = {
			limit = { has_equipment = { Inf_equipment > 150000 } }
			set_temp_variable = { treasury_change = 30 }
			modify_treasury_effect = yes
			add_equipment_to_stockpile = {
				type = Inf_equipment
				amount = -25000
			}
		}
		if = {
			limit = { has_equipment = { cnc_equipment > 20000 } }
			set_temp_variable = { treasury_change = 30 }
			modify_treasury_effect = yes
			add_equipment_to_stockpile = {
				type = cnc_equipment
				amount = -5000
			}
		}
		if = {
			limit = { has_equipment = { L_AT_Equipment > 12000 } }
			set_temp_variable = { treasury_change = 30 }
			modify_treasury_effect = yes
			add_equipment_to_stockpile = {
				type = L_AT_Equipment
				amount = -2000
			}
		}
		if = {
			limit = { has_equipment = { AA_Equipment > 12000 } }
			set_temp_variable = { treasury_change = 30 }
			modify_treasury_effect = yes
			add_equipment_to_stockpile = {
				type = AA_Equipment
				amount = -2000
			}
		}
		if = {
			limit = { has_equipment = { Inf_equipment > 150000 } }
			set_temp_variable = { treasury_change = 30 }
			modify_treasury_effect = yes
			add_equipment_to_stockpile = {
				type = Inf_equipment
				amount = -25000
			}
		}
		if = {
			limit = { has_equipment = { util_vehicle_equipment > 10000 } }
			set_temp_variable = { treasury_change = 30 }
			modify_treasury_effect = yes
			add_equipment_to_stockpile = {
				type = util_vehicle_equipment
				amount = -1000
			}
		}
		if = {
			limit = { has_equipment = { H_AT_Equipment > 10000 } }
			set_temp_variable = { treasury_change = 30 }
			modify_treasury_effect = yes
			add_equipment_to_stockpile = {
				type = H_AT_Equipment
				amount = -2000
			}
		}
		if = {
			limit = { has_equipment = { spaa_hull > 2500 } }
			set_temp_variable = { treasury_change = 30 }
			modify_treasury_effect = yes
			add_equipment_to_stockpile = {
				type = spaa_hull
				amount = -500
			}
		}
		if = {
			limit = { has_equipment = { apc_hull > 5000 } }
			set_temp_variable = { treasury_change = 30 }
			modify_treasury_effect = yes
			add_equipment_to_stockpile = {
				type = apc_hull
				amount = -500
			}
		}
		if = {
			limit = { has_equipment = { ifv_hull > 5000 } }
			set_temp_variable = { treasury_change = 30 }
			modify_treasury_effect = yes
			add_equipment_to_stockpile = {
				type = ifv_hull
				amount = -500
			}
		}
		if = {
			limit = { has_equipment = { mbt_hull > 5000 } }
			set_temp_variable = { treasury_change = 30 }
			modify_treasury_effect = yes
			add_equipment_to_stockpile = {
				type = mbt_hull
				amount = -500
			}
		}
		if = {
			limit = { has_equipment = { spart_hull > 3000 } }
			set_temp_variable = { treasury_change = 30 }
			modify_treasury_effect = yes
			add_equipment_to_stockpile = {
				type = spart_hull
				amount = -500
			}
		}
		if = {
			limit = { has_equipment = { artillery_equipment > 5000 } }
			set_temp_variable = { treasury_change = 30 }
			modify_treasury_effect = yes
			add_equipment_to_stockpile = {
				type = artillery_equipment
				amount = -750
			}
		}
		if = {
			limit = { has_equipment = { transport_helicopter_equipment > 5000 } }
			set_temp_variable = { treasury_change = 30 }
			modify_treasury_effect = yes
			add_equipment_to_stockpile = {
				type = transport_helicopter_equipment
				amount = -750
			}
		}
	}
}