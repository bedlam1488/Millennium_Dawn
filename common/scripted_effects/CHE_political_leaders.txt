set_leader_CHE = {

	if = { limit = { has_country_flag = set_Conservative }

		if = { limit = { check_variable = { Conservative_leader = 0 } }
			add_to_variable = { Conservative_leader = 0 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Ahmat Kadyrov"
				picture = "Ahmat_Kadyrov.dds"
				ideology = Conservative
				traits = {
					emerging_Conservative
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Conservative_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Conservative_leader = 1 } }
			add_to_variable = { Conservative_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Ramzan Kadyrov"
				picture = "CHE_Ramzan_Kadyrov.dds"
				ideology = Conservative
				traits = {
					emerging_Conservative
					sly
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Conservative_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	
	else_if = { limit = { has_country_flag = set_Caliphate }
	if = { limit = { check_variable = { Caliphate_leader = 0 } }
			add_to_variable = { Caliphate_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Dokka Umarov"
				picture = "Dokka_Umarov.dds"
				ideology = Caliphate
				traits = {
					salafist_Caliphate
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Caliphate_leader = 1 } }
		}
	}
}