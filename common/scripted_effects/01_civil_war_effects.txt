put_the_largest_opposition_party_in_power = {
	#Find the strongest non-government party
	set_temp_variable = { opposition_party_index = 0 }
	set_temp_variable = { opposition_party_popularity = 0 }
	for_each_loop = {
		array = party_pop_array
		value = v
		index = i
		break = break
		if = {
			limit = {
				check_variable = { v > opposition_party_popularity }
				NOT = { is_in_array = { ruling_party = i } }
				NOT = { is_in_array = { gov_coalition_array = i } }
			}
			set_temp_variable = { opposition_party_index = i }
			set_temp_variable = { opposition_party_popularity = v }
		}
	}

	#Save current ruling data
	set_temp_variable = { party_index = 0 }
	set_temp_variable_to_ruling_party = yes
	set_temp_variable = { current_ruling_popularity = party_pop_array^party_index }

	#Set new politics for us
	clear_array = ruling_party
	clear_array = gov_coalition_array
	add_to_array = { ruling_party = opposition_party_index }
	update_party_name = yes
	set_coalition_drift = yes
	update_government_coalition_strength = yes
	hidden_effect = {
		set_ruling_leader = yes
		set_leader = yes
		set_variable = { current_term = 0 } #not a full term
	}
	if = {
		limit = {
			check_variable = { opposition_party_index < 4 }
		}
		if = {
			limit = { has_elections = yes }
			set_politics = {
				ruling_party = democratic
				elections_allowed = yes
			}
		}
		else = {
			set_politics = {
				ruling_party = democratic
				elections_allowed = no
			}
		}
	}
	if = {
		limit = {
			check_variable = { opposition_party_index > 3 }
			check_variable = { opposition_party_index < 10 }
		}
		if = {
			limit = { has_elections = yes }
			set_politics = {
				ruling_party = communism
				elections_allowed = yes
			}
		}
		else = {
			set_politics = {
				ruling_party = communism
				elections_allowed = no
			}
		}
	}
	if = {
		limit = {
			check_variable = { opposition_party_index > 9 }
			check_variable = { opposition_party_index < 12 }
		}
		if = {
			limit = { has_elections = yes }
			set_politics = {
				ruling_party = fascism
				elections_allowed = no
			}
		}
		else = {
			set_politics = {
				ruling_party = fascism
				elections_allowed = no
			}
		}
	}
	if = {
		limit = {
			check_variable = { opposition_party_index > 11 }
			check_variable = { opposition_party_index < 20 }
		}
		if = {
			limit = { has_elections = yes }
			set_politics = {
				ruling_party = neutrality
				elections_allowed = yes
			}
		}
		else = {
			set_politics = {
				ruling_party = neutrality
				elections_allowed = no
			}
		}
	}
	if = {
		limit = {
			check_variable = { opposition_party_index > 19 }
		}
		if = {
			limit = { has_elections = yes }
			set_politics = {
				ruling_party = nationalist
				elections_allowed = yes
			}
		}
		else = {
			set_politics = {
				ruling_party = nationalist
				elections_allowed = yes
			}
		}
	}
}

start_civil_war_against_the_strongest_opposition = {
	#Find the strongest non-government party
	set_temp_variable = { opposition_party_index = 0 }
	set_temp_variable = { opposition_party_popularity = 0 }
	for_each_loop = {
		array = party_pop_array
		value = v
		index = i
		break = break
		if = {
			limit = {
				check_variable = { v > opposition_party_popularity }
				NOT = { is_in_array = { ruling_party = i } }
				NOT = { is_in_array = { gov_coalition_array = i } }
			}
			set_temp_variable = { opposition_party_index = i }
			set_temp_variable = { opposition_party_popularity = v }
		}
	}

	#Set size for the civil war (2xpopularity)
	multiply_temp_variable = { opposition_party_popularity = 2 }
	clamp_temp_variable = {
		var = opposition_party_popularity
		min = 0
		max = 1
	}
	custom_effect_tooltip = start_civil_war_against_the_strongest_opposition_tt

	hidden_effect = {
		#Start the civil war
		if = {
			limit = {
				check_variable = { opposition_party_index < 4 }
			}
			start_civil_war = {
				ideology = democratic
				size = opposition_party_popularity
			}
		}
		if = {
			limit = {
				check_variable = { opposition_party_index > 3 }
				check_variable = { opposition_party_index < 10 }
			}
			start_civil_war = {
				ideology = communism
				size = opposition_party_popularity
			}
		}
		if = {
			limit = {
				check_variable = { opposition_party_index > 9 }
				check_variable = { opposition_party_index < 12 }
			}
			start_civil_war = {
				ideology = fascism
				size = opposition_party_popularity
			}
		}
		if = {
			limit = {
				check_variable = { opposition_party_index > 11 }
				check_variable = { opposition_party_index < 20 }
			}
			start_civil_war = {
				ideology = neutrality
				size = opposition_party_popularity
			}
		}
		if = {
			limit = {
				check_variable = { opposition_party_index > 19 }
			}
			start_civil_war = {
				ideology = nationalist
				size = opposition_party_popularity
			}
		}

		#Set the revolting side correctly
		random_enemy_country = {
			limit = { original_tag = ROOT }
			clear_array = ruling_party
			clear_array = gov_coalition_array
			add_to_array = { ruling_party = opposition_party_index }
			update_party_name = yes
			set_coalition_drift = yes
			update_government_coalition_strength = yes
			hidden_effect = {
				set_ruling_leader = yes
				set_leader = yes
				set_variable = { current_term = 0 } #not a full term
			}
		}
	}

}

start_civil_war_as_the_strongest_opposition = {

	hidden_effect = { put_the_largest_opposition_party_in_power = yes }

	#Set size for the civil war (2xpopularity)
	multiply_temp_variable = { current_ruling_popularity = 2 }
	clamp_temp_variable = {
		var = current_ruling_popularity
		min = 0
		max = 1
	}

	custom_effect_tooltip = start_civil_war_as_the_strongest_opposition_tt

	hidden_effect = {
		#Start the civil war
		set_country_flag = no_leader_change
		if = {
			limit = {
				check_variable = { party_index < 4 }
			}
			start_civil_war = {
				ideology = democratic
				size = opposition_party_popularity
			}
		}
		if = {
			limit = {
				check_variable = { party_index > 3 }
				check_variable = { party_index < 10 }
			}
			start_civil_war = {
				ideology = communism
				size = opposition_party_popularity
			}
		}
		if = {
			limit = {
				check_variable = { party_index > 9 }
				check_variable = { party_index < 12 }
			}
			start_civil_war = {
				ideology = fascism
				size = opposition_party_popularity
			}
		}
		if = {
			limit = {
				check_variable = { party_index > 11 }
				check_variable = { party_index < 20 }
			}
			start_civil_war = {
				ideology = neutrality
				size = opposition_party_popularity
			}
		}
		if = {
			limit = {
				check_variable = { party_index > 19 }
			}
			start_civil_war = {
				ideology = nationalist
				size = opposition_party_popularity
			}
		}
		random_enemy_country = {
			limit = { original_tag = ROOT }
			clear_array = ruling_party
			clear_array = gov_coalition_array
			add_to_array = { ruling_party = party_index }
			update_party_name = yes
			set_coalition_drift = yes
			update_government_coalition_strength = yes
		}
		clr_country_flag = no_leader_change
	}

}

add_civil_war_debuff = {
	#Remove all ideas in-case not fully recovered yet
	set_country_flag = manual_civil_war_debuff_removal
	remove_ideas = civil_war_disorganised_military_1
	remove_ideas = civil_war_disorganised_military_2_military
	remove_ideas = civil_war_disorganised_military_2_production
	remove_ideas = civil_war_disorganised_military_2_experience
	remove_ideas = civil_war_disorganised_military_3_military
	remove_ideas = civil_war_disorganised_military_3_production
	remove_ideas = civil_war_disorganised_military_3_experience
	remove_ideas = civil_war_disorganised_military_4_military
	remove_ideas = civil_war_disorganised_military_4_production
	remove_ideas = civil_war_disorganised_military_4_experience
	remove_ideas = civil_war_disorganised_military_5_military
	remove_ideas = civil_war_disorganised_military_5_production
	remove_ideas = civil_war_disorganised_military_5_experience
	clr_country_flag = manual_civil_war_debuff_removal

	#Add timed idea for 5 months, other levels will be handled with on_remove events
	add_timed_idea = {
		idea = civil_war_disorganised_military_1
		days = 150
	}
}

# Common Setup for the CAR/MAL Civil Wars
# Executes in the scope of the rebel tag
CAR_common_rebel_setup = {
	set_variable = { population_tax_rate = CAR.population_tax_rate }
	set_variable = { corporate_tax_rate = CAR.corporate_tax_rate }
	ingame_update_setup = yes
	EDU_learn_your_ABCs_with_dread = yes
	set_variable = { literacy_rate = CAR.literacy_rate }
	if = {
		limit = {
			CAR = { is_agrarian_economy = yes }
		}
		AGRI_plant_the_seeds = yes
		set_variable = { agri_crop_allocation^0 = CAR.agri_crop_allocation^0 }
		set_variable = { agri_crop_allocation^1 = CAR.agri_crop_allocation^1 }
		set_variable = { agri_crop_price^0 = CAR.agri_crop_price^0 }
		set_variable = { agri_crop_price^1 = CAR.agri_crop_price^1 }
		AGRI_update_math_variables = yes
	}
	# Initialization for the country
	calculate_influence_percentage = yes #set alignment drifts from influence
	startup_politics = yes
	ingame_update_setup = yes 	#money system
	setup_init_factions = yes 	#flags for int. factions
	set_law_vars = yes 	#vars for change laws secondary effects
	update_neighbors_effects = yes # Updates Neighbor Effects
	economic_cycle_drift_popularity = yes # Actually impacts the economic cycle drifts
	if = { limit = { is_ai = yes }
		ai_update_build_units = yes
	}
	# Ensures the Rebel army is supplied for the conflict
	add_equipment_to_stockpile = { type = infantry_weapons amount = 1500 producer = SOV }
	division_template = {
		name = "Rebel Groups"
		is_locked = yes
		regiments = {
			Militia_Bat = { x = 0 y = 0 }
			Militia_Bat = { x = 0 y = 1 }
			Militia_Bat = { x = 0 y = 2 }
			Militia_Bat = { x = 0 y = 3 }
		}
	}
	inherit_technology = CAR
}

MAL_common_rebel_setup = {
	set_variable = { population_tax_rate = MAL.population_tax_rate }
	set_variable = { corporate_tax_rate = MAL.corporate_tax_rate }
	ingame_update_setup = yes
	EDU_learn_your_ABCs_with_dread = yes
	set_variable = { literacy_rate = MAL.literacy_rate }
	if = {
		limit = {
			MAL = { is_agrarian_economy = yes }
		}
		AGRI_plant_the_seeds = yes
		set_variable = { agri_crop_allocation^0 = MAL.agri_crop_allocation^0 }
		set_variable = { agri_crop_allocation^1 = MAL.agri_crop_allocation^1 }
		set_variable = { agri_crop_price^0 = MAL.agri_crop_price^0 }
		set_variable = { agri_crop_price^1 = MAL.agri_crop_price^1 }
		AGRI_update_math_variables = yes
	}
	# Initialization for the country
	calculate_influence_percentage = yes #set alignment drifts from influence
	startup_politics = yes
	ingame_update_setup = yes 	#money system
	setup_init_factions = yes 	#flags for int. factions
	set_law_vars = yes 	#vars for change laws secondary effects
	update_neighbors_effects = yes # Updates Neighbor Effects
	economic_cycle_drift_popularity = yes # Actually impacts the economic cycle drifts
	if = { limit = { is_ai = yes }
		ai_update_build_units = yes
	}
	# Ensures the Rebel army is supplied for the conflict
	add_equipment_to_stockpile = { type = infantry_weapons amount = 1500 producer = SOV }
	division_template = {
		name = "Rebel Groups"
		is_locked = yes
		regiments = {
			Militia_Bat = { x = 0 y = 0 }
			Militia_Bat = { x = 0 y = 1 }
			Militia_Bat = { x = 0 y = 2 }
			Militia_Bat = { x = 0 y = 3 }
		}
	}
	inherit_technology = MAL
}
