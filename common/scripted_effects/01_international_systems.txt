on_startup_international = {
	add_to_array = { global.PMC_companies = 001 } #Blackwater
	add_to_array = { global.PMC_companies = 002 } #Aegis
	add_to_array = { global.PMC_companies = 003 } #Constellis
	add_to_array = { global.PMC_companies = 004 } #Wagner
	add_to_array = { global.PMC_Companies = 031 } #MD
	add_to_array = { global.PMC_companies = 005 } #MPRI
	add_to_array = { global.PMC_companies = 006 } #SADAT
	add_to_array = { global.PMC_companies = 007 } #RSB_Group
	add_to_array = { global.PMC_companies = 008 } #SandLine
	add_to_array = { global.PMC_companies = 009 } #KBR Supply
	add_to_array = { global.PMC_companies = 010 } #Nirtal
	add_to_array = { global.PMC_companies = 011 } #HALO
	add_to_array = { global.PMC_companies = 012 } #Defense Conseil
	add_to_array = { global.PMC_companies = 013 } #GardaWorld
	add_to_array = { global.PMC_companies = 014 } #European Security Academy
	add_to_array = { global.PMC_companies = 015 } #g4s
	add_to_array = { global.PMC_Companies = 016 } #Asgard
	add_to_array = { global.PMC_Companies = 017 } #Union of Tribes of Sinai
	add_to_array = { global.PMC_Companies = 018 } #Gard
	add_to_array = { global.PMC_Companies = 019 } #Patriot
	add_to_array = { global.PMC_Companies = 020 } #Dewe Security Services
	add_to_array = { global.PMC_Companies = 021 } #China OGS
	add_to_array = { global.PMC_Companies = 022 } #Front
	add_to_array = { global.PMC_Companies = 023 } #Redut
	add_to_array = { global.PMC_Companies = 024 } #Gazprom
	add_to_array = { global.PMC_Companies = 025 } #Iron
	add_to_array = { global.PMC_Companies = 026 } #Sheriff
	add_to_array = { global.PMC_Companies = 027 } #Lukoil
	add_to_array = { global.PMC_Companies = 028 } #Rosneft
	add_to_array = { global.PMC_Companies = 029 } #Sberbank
	add_to_array = { global.PMC_Companies = 030 } #VTB

	add_to_array = { global.PMC_units_major = 001 }
	add_to_array = { global.PMC_units_major = 002 }
	add_to_array = { global.PMC_units_major = 003 }
	add_to_array = { global.PMC_units_major = 004 }
	add_to_array = { global.PMC_units_major = 031 }

	add_to_array = { global.PMC_units_minor = 008 }
	add_to_array = { global.PMC_units_minor = 013 }
	add_to_array = { global.PMC_units_minor = 015 }
	add_to_array = { global.PMC_units_minor = 017 }
	add_to_array = { global.PMC_units_minor = 018 }
	add_to_array = { global.PMC_units_minor = 019 }
	add_to_array = { global.PMC_units_minor = 020 }
	add_to_array = { global.PMC_units_minor = 023 }
	add_to_array = { global.PMC_units_minor = 024 }
	add_to_array = { global.PMC_units_minor = 025 }
	add_to_array = { global.PMC_units_minor = 026 }
	add_to_array = { global.PMC_units_minor = 027 }
	add_to_array = { global.PMC_units_minor = 028 }
	add_to_array = { global.PMC_units_minor = 029 }
	add_to_array = { global.PMC_units_minor = 030 }

	add_to_array = { global.PMC_buffs = 005 }
	add_to_array = { global.PMC_buffs = 006 }
	add_to_array = { global.PMC_buffs = 007 }
	add_to_array = { global.PMC_buffs = 009 }
	add_to_array = { global.PMC_buffs = 010 }
	add_to_array = { global.PMC_buffs = 011 }
	add_to_array = { global.PMC_buffs = 012 }
	add_to_array = { global.PMC_buffs = 014 }
	add_to_array = { global.PMC_buffs = 016 }
	add_to_array = { global.PMC_buffs = 021 }
	add_to_array = { global.PMC_buffs = 022 }
	add_to_array = { global.PMC_buffs = 024 }
	add_to_array = { global.PMC_buffs = 026 }
	add_to_array = { global.PMC_buffs = 027 }
	add_to_array = { global.PMC_buffs = 028 }
	add_to_array = { global.PMC_buffs = 029 }
	add_to_array = { global.PMC_buffs = 030 }

	set_variable = { global.international_system_refresh_gui = 1 }
}

clr_country_pmc_flag = {
	clr_country_flag = blackwater_sel
	clr_country_flag = aegis_sel
	clr_country_flag = constellis_sel
	clr_country_flag = wagner_sel
	clr_country_flag = md_sel
	clr_country_flag = mpri_sel
	clr_country_flag = sadat_sel
	clr_country_flag = rsb_sel
	clr_country_flag = sandline_sel
	clr_country_flag = kbr_sel
	clr_country_flag = nirtal_sel
	clr_country_flag = halo_sel
	clr_country_flag = defense_sel
	clr_country_flag = gardaworld_sel
	clr_country_flag = esa_sel
	clr_country_flag = g4s_sel
	clr_country_flag = asgard_sel
	clr_country_flag = union_sel
	clr_country_flag = gard_sel
	clr_country_flag = patriot_sel
	clr_country_flag = dewe_sel
	clr_country_flag = over_sel
	clr_country_flag = front_sel
	clr_country_flag = redut_sel
	clr_country_flag = gaz_sel
	clr_country_flag = iron_sel
	clr_country_flag = sheriff_sel
	clr_country_flag = lukoil_sel
	clr_country_flag = rosneft_sel
	clr_country_flag = sberbank_sel
	clr_country_flag = vtb_sel
	clr_country_flag = md_sel
}

clear_ct_flags = {
	clr_country_flag = ct_selected_asia
	clr_country_flag = ct_selected_africa
	clr_country_flag = ct_selected_me
}

set_sel_pmc_units = {
	clear_array = selected_units
	#group these by number of units that can be hired
	#sorted by country flags
	#then use the specific country flag in dynamic text / effects to apply
	if = {
		limit = {
			is_major_company = yes
		}
		add_to_array = { selected_units = 001 }
		add_to_array = { selected_units = 002 }
		add_to_array = { selected_units = 003 }
		add_to_array = { selected_units = 004 }
		add_to_array = { selected_units = 005 }
		add_to_array = { selected_units = 006 }
		add_to_array = { selected_units = 007 }
	}
	if = { limit = { has_country_flag = blackwater_sel }
		set_variable = { comp_check = 0 }
	}
	else_if = { limit = { has_country_flag = aegis_sel }
		set_variable = { comp_check = 2 }
	}
	else_if = { limit = { has_country_flag = constellis_sel }
		set_variable = { comp_check = 3 }
	}
	else_if = { limit = { has_country_flag = wagner_sel }
		set_variable = { comp_check = 1 }
	}
	else_if = { limit = { has_country_flag = md_sel }
		set_variable = { comp_check = 4 }
	}
}

update_international_system_gui = {
	if = {
		limit = { check_variable = { global.international_system_refresh_gui < 1000000 } }
		add_to_variable = { global.international_system_refresh_gui = 1 }
	}
	else = {
		set_variable = { global.international_system_refresh_gui = 1 }
	}
}
