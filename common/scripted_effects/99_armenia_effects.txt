ARM_decrease_huge_unemployment = {
	if = {
		limit = { has_idea = ARM_huge_unemployment_5 }
		swap_ideas = {
			remove_idea = ARM_huge_unemployment_5
			add_idea = ARM_huge_unemployment_4
		}
	}
	else_if = {
		limit = { has_idea = ARM_huge_unemployment_4 }
		swap_ideas = {
			remove_idea = ARM_huge_unemployment_4
			add_idea = ARM_huge_unemployment_3
		}
	}
	else_if = {
		limit = { has_idea = ARM_huge_unemployment_3 }
		swap_ideas = {
			remove_idea = ARM_huge_unemployment_3
			add_idea = ARM_huge_unemployment_2
		}
	}
	else_if = {
		limit = { has_idea = ARM_huge_unemployment_2 }
		swap_ideas = {
			remove_idea = ARM_huge_unemployment_2
			add_idea = ARM_huge_unemployment_1
		}
	}
	else_if = {
		limit = { has_idea = ARM_huge_unemployment_1 }
		remove_ideas = ARM_huge_unemployment_1
	}
	else = { add_political_power = 50 }
}

ARM_decrease_high_emigration = {
	if = {
		limit = { has_idea = ARM_high_emigration_5 }
		swap_ideas = {
			remove_idea = ARM_high_emigration_5
			add_idea = ARM_high_emigration_4
		}
	}
	else_if = {
		limit = { has_idea = ARM_high_emigration_4 }
		swap_ideas = {
			remove_idea = ARM_high_emigration_4
			add_idea = ARM_high_emigration_3
		}
	}
	else_if = {
		limit = { has_idea = ARM_high_emigration_3 }
		swap_ideas = {
			remove_idea = ARM_high_emigration_3
			add_idea = ARM_high_emigration_2
		}
	}
	else_if = {
		limit = { has_idea = ARM_high_emigration_2 }
		swap_ideas = {
			remove_idea = ARM_high_emigration_2
			add_idea = ARM_high_emigration_1
		}
	}
	else_if = {
		limit = { has_idea = ARM_high_emigration_1 }
		remove_ideas = ARM_high_emigration_1
		add_ideas = ARM_immigration
	}
	else = { add_stability = 0.02 }
}
ARM_increase_monopolization = {
	if = {
		limit = {
			has_idea = ARM_monopolization_idea1
		}
		swap_ideas = {
			remove_idea = ARM_monopolization_idea1
			add_idea = ARM_monopolization_idea2
		}
	}
	else_if = {
		limit = { has_idea = ARM_monopolization_idea2 }
		swap_ideas = {
			remove_idea = ARM_monopolization_idea2
			add_idea = ARM_monopolization_idea3
		}
	}
	else_if = {
		limit = { has_idea = ARM_monopolization_idea3 }
		add_political_power = 50
	}
	else = { add_ideas = ARM_monopolization_idea1 }
}