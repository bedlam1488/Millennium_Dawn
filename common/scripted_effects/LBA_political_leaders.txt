set_leader_LBA = {

	if = { limit = { has_country_flag = set_conservatism }

		if = { limit = { check_variable = { conservatism_leader = 0 } }
			add_to_variable = { conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Abdulrahman Sewehli"
				desc = LBA_abdulrahman_sewehli_desc
				picture = "abdulrahman_sewehli.dds"
				ideology = conservatism
				traits = {
					western_conservatism
					defender_of_democracy
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_liberalism }

		if = { limit = { check_variable = { liberalism_leader = 0 } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Ali Zeidan"
				desc = LBA_ali_zeidan_desc
				picture = "ali_zeidan.dds"
				ideology = liberalism
				traits = {
					western_liberalism
					lawyer
					stubborn
					ideological_crusader
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_socialism }

		if = { limit = { check_variable = { socialism_leader = 0 } }
			add_to_variable = { socialism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Ali Tarhouni"
				desc = LBA_ali_tarhouni_desc
				picture = "ali_tarhouni.dds"
				ideology = socialism
				traits = {
					western_socialism
					economist
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { socialism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { socialism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { socialism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mahmoud Shammam"
				picture = "mahmoud_shamam.dds"
				ideology = socialism
				traits = {
					western_socialism
					geopolitical_thinker
					pro_brotherhood
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { socialism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { socialism_leader = 2 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { socialism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Fathi Al-Baaja"
				desc = LBA_fathi_al_baaja_desc
				picture = "fathi_al_baaja.dds"
				ideology = socialism
				traits = {
					western_socialism
					scientist
					cautious
					staunch_constitutionalist
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { socialism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Conservative }

		if = { limit = { check_variable = { Conservative_leader = 0 } }
			add_to_variable = { Conservative_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Khweldi Hameidi"
				desc = LBA_khweldi_hameidi_desc
				picture = "Khweldi_Hameidi.dds"
				ideology = Conservative
				traits = {
					emerging_Conservative
					military_career
					capable
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Conservative_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Conservative_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Conservative_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mustafa al-Zaidi"
				picture = "mustafa_al_zaidi.dds"
				ideology = Conservative
				traits = {
					emerging_Conservative
					rash
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Conservative_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Autocracy }
		if = { limit = { check_variable = { Autocracy_leader = 0 } }
			add_to_variable = { Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Muhammad Muammar Gaddafi"
				desc = LBA_muhammad_muammar_gaddafi_desc
				picture = "muhammad_mummar_gaddafi.dds"
				ideology = Autocracy
				traits = {
					emerging_Autocracy
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Autocracy_leader = 1 } }
			if = { limit = { NOT = { has_country_flag = muhammad_muammar_gaddafi_exile has_country_flag = muhammad_muammar_gaddafi_dead } } set_temp_variable = { b = 1 } } #skip if dead or exile
			if = {
				limit = { check_variable = { b = 1 } }
				if = {
					limit = { has_country_flag = muhammad_muammar_gaddafi_head_of_GPTC }
					add_country_leader_trait = head_of_gptc
				}
				if = {
					limit = { has_country_flag = muhammad_muammar_gaddafi_uninterested_in_politics }
					add_country_leader_trait = uninterested_in_politics
				}
				if = {
					limit = { has_country_flag = muhammad_muammar_gaddafi_king_of_coke }
					add_country_leader_trait = king_of_coke
				}
			}
		}
		if = { limit = { check_variable = { Autocracy_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Saif al-Islam Gaddafi"
				desc = LBA_saif_al_islam_gaddafi_desc
				picture = "Saif_al_Islam_Gaddafi.dds"
				ideology = Autocracy
				traits = {
					emerging_Autocracy
					ex_engineer
					humble
					Reformer
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Autocracy_leader = 1 } }
			if = { limit = { NOT = { has_country_flag = saif_al_islam_gaddafi_exile has_country_flag = saif_al_islam_gaddafi_dead } } set_temp_variable = { b = 1 } } #skip if dead or exile
			if = {
				limit = { check_variable = { b = 1 } }
				if = {
					limit = { has_country_flag = saif_al_islam_gaddafi_master_maneuverer }
					add_country_leader_trait = political_dancer
				}
				if = {
					limit = { has_country_flag = saif_al_islam_gaddafi_geopolitical_thinker }
					add_country_leader_trait = geopolitical_thinker
				}
				if = {
					limit = { has_country_flag = saif_al_islam_gaddafi_democratic_reformer }
					add_country_leader_trait = democratic_reformer
				}
			}
		}
		if = { limit = { check_variable = { Autocracy_leader = 2 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Al-Saadi Gaddafi"
				picture = "al_saadi_gaddafi.dds"
				desc = LBA_al_saadi_gaddafi_desc
				ideology = Autocracy
				traits = {
					emerging_Autocracy
					narcissist
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Autocracy_leader = 1 } }
			if = { limit = { NOT = { has_country_flag = al_saadi_gaddafi_exile has_country_flag = al_saadi_gaddafi_dead } } set_temp_variable = { b = 1 } } #skip if dead or exile
			if = {
				limit = { check_variable = { b = 1 } }
				if = {
					limit = { has_country_flag = al_saadi_gaddafi_vain_football_star }
					add_country_leader_trait = vain_football_star
				}
				if = {
					limit = { has_country_flag = al_saadi_gaddafi_substance_abuser }
					add_country_leader_trait = substance_abuser
				}
				if = {
					limit = { has_country_flag = al_saadi_gaddafi_playboy_lifestyle }
					add_country_leader_trait = playboy_lifestyle
				}
			}
		}
		if = { limit = { check_variable = { Autocracy_leader = 3 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mutassim Gaddafi"
				picture = "mutassim_gaddafi.dds"
				desc = LBA_mutassim_gaddafi_desc
				ideology = Autocracy
				traits = {
					emerging_Autocracy
					military_career
					ruthless
					stubborn
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Autocracy_leader = 1 } }
			if = { limit = { NOT = { has_country_flag = mutassim_gaddafi_exile has_country_flag = mutassim_gaddafi_dead } } set_temp_variable = { b = 1 } } #skip if dead or exile
			if = {
				limit = { check_variable = { b = 1 } }
				if = {
					limit = { has_country_flag = mutassim_gaddafi_77th_tank_battalion }
					add_country_leader_trait = the_77th_tank_battalion
				}
				if = {
					limit = { has_country_flag = mutassim_gaddafi_suspected_revolutionary }
					add_country_leader_trait = suspected_revolutionary
				}
				if = {
					limit = { has_country_flag = mutassim_gaddafi_king_of_coke }
					add_country_leader_trait = king_of_coke
				}
				if = {
					limit = { has_country_flag = mutassim_gaddafi_national_security_advisor }
					add_country_leader_trait = national_security_advisor
				}
			}
		}
		if = { limit = { check_variable = { Autocracy_leader =4 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Hannibal Gaddafi"
				desc = LBA_hannibal_gaddafi_desc
				picture = "hannibal_gaddafi.dds"
				ideology = Autocracy
				traits = {
					emerging_Autocracy
					rash
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Autocracy_leader = 1 } }
			if = { limit = { NOT = { has_country_flag = hannibal_gaddafi_exile has_country_flag = hannibal_gaddafi_dead } } set_temp_variable = { b = 1 } } #skip if dead or exile
			if = {
				limit = { check_variable = { b = 1 } }
				if = {
					limit = { has_country_flag = hannibal_gaddafi_bachelor_of_marine_navigation }
					add_country_leader_trait = bachelor_of_marine_navigation
				}
				if = {
					limit = { has_country_flag = hannibal_gaddafi_master_of_shipping_economics_and_logistics }
					add_country_leader_trait = master_of_shipping_economics_and_logistics
				}
				if = {
					limit = { has_country_flag = hannibal_gaddafi_scholarly_challenged }
					add_country_leader_trait = scholarly_challenged
				}
				if = {
					limit = { has_country_flag = hannibal_gaddafi_graduate_of_kuznetsov }
					add_country_leader_trait = graduate_of_kuznetsov
				}
			}
		}
		if = { limit = { check_variable = { Autocracy_leader = 5 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Ayesha Gaddafi"
				desc = LBA_ayesha_gaddafi_desc
				picture = "ayesha_gaddafi.dds"
				ideology = Autocracy
				traits = {
					emerging_Autocracy
					military_career
					lawyer
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Autocracy_leader = 1 } }
			if = { limit = { NOT = { has_country_flag = ayesha_gaddafi_exile has_country_flag = ayesha_gaddafi_exile } } set_temp_variable = { b = 1 } } #skip if dead or exile
			if = {
				limit = { check_variable = { b = 1 } }
				if = {
					limit = { has_country_flag = ayesha_gaddafi_claudia_schiffer }
					add_country_leader_trait = claudia_schiffer_of_north_africa
				}
				if = {
					limit = { has_country_flag = ayesha_gaddafi_un_goodwill_ambassador }
					add_country_leader_trait = un_goodwill_ambassador
				}
				if = {
					limit = { has_country_flag = ayesha_gaddafi_amazon_of_libya }
					add_country_leader_trait = amazon_of_libya
				}
				if = {
					limit = { has_country_flag = ayesha_gaddafi_inexperienced_general }
					add_country_leader_trait = inexperienced_general
				}
			}
		}
		if = { limit = { check_variable = { Autocracy_leader = 6 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Saif al-Arab Gaddafi"
				desc = LBA_saif_al_arab_gaddafi_desc
				picture = "saif_al_arab_gaddafi.dds"
				ideology = Autocracy
				traits = {
					emerging_Autocracy
					sly
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Autocracy_leader = 1 } }
			if = { limit = { NOT = { has_country_flag = saif_al_arab_gaddafi_exile has_country_flag = saif_al_arab_gaddafi_exile } } set_temp_variable = { b = 1 } } #skip if dead or exile
			if = {
				limit = { check_variable = { b = 1 } }
				if = {
					limit = { has_country_flag = saif_al_arab_gaddafi_convicted_criminal }
					add_country_leader_trait = convicted_criminal
				}
			}
		}
		if = { limit = { check_variable = { Autocracy_leader = 7 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Khamis Gaddafi"
				desc = LBA_khamis_gaddafi_desc
				picture = "Khamis_Gaddafi.dds"
				ideology = Autocracy
				traits = {
					emerging_Autocracy
					military_career
					rational
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Autocracy_leader = 1 } }
			if = { limit = { NOT = { has_country_flag = khamis_gaddafi_exile has_country_flag = khamis_gaddafi_dead } } set_temp_variable = { b = 1 } } #skip if dead or exile
			if = {
				limit = { check_variable = { b = 1 } }
				if = {
					limit = { has_country_flag = khamis_gaddafi_khamis_brigade }
					add_country_leader_trait = khamis_brigade
				}
				if = {
					limit = { has_country_flag = khamis_gaddafi_war_industrialist }
					add_country_leader_trait = war_industrialist
				}
				if = {
					limit = { has_country_flag = khamis_gaddafi_captain_of_industry }
					add_country_leader_trait = captain_of_industry
				}
			}
		}
		if = { limit = { check_variable = { b = 1 } } libya_find_next_gaddafi_heir = yes }
	}
	else_if = { limit = { has_country_flag = set_Kingdom }
		if = { limit = { check_variable = { Kingdom_leader = 0 } }
			add_to_variable = { Kingdom_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Ali al-Sallabi"
				picture = "ali_al_sallabi.dds"
				desc = LBA_ali_al_sallabi_desc
				ideology = Kingdom
				traits = {
					salafist_Kingdom
					writer
					cautious
					pro_al_qaeda
					pro_brotherhood
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Kingdom_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Caliphate }
		if = { limit = { check_variable = { Caliphate_leader = 0 } }
			add_to_variable = { Caliphate_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Abdelhakim Belhaj"
				picture = "Abdel_Hakim_Belhadj.dds"
				desc = LBA_abdelhakim_belhaj_desc
				ideology = Caliphate
				traits = {
					salafist_Caliphate
					guerrilla_leader
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Caliphate_leader = 1 } }
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_Muslim_Brotherhood }
		if = { limit = { check_variable = { Neutral_Muslim_Brotherhood_leader = 0 } }
			add_to_variable = { Neutral_Muslim_Brotherhood_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Emad al-Banani"
				picture = "emad_al_banani.dds"
				desc = LBA_emad_al_banani_desc
				ideology = Neutral_Muslim_Brotherhood
				traits = {
					neutrality_Neutral_Muslim_Brotherhood
					aviation_engineer
					rash
					silent_workhorse
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Muslim_Brotherhood_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Neutral_Muslim_Brotherhood_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_Muslim_Brotherhood_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mohamed Gaair"
				picture = "mohamed_gaair.dds"
				ideology = Neutral_Muslim_Brotherhood
				traits = {
					neutrality_Neutral_Muslim_Brotherhood
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Muslim_Brotherhood_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_Autocracy }
		if = { limit = { check_variable = { Neutral_Autocracy_leader = 0 } }
			add_to_variable = { Neutral_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mahmoud Jibril"
				desc = LBA_mahmoud_jibril_desc
				picture = "mahmoud_jibirl.dds"
				ideology = Neutral_Autocracy
				traits = {
					neutrality_Neutral_Autocracy
					economist
					corruptible
					smooth_talking_charmer
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Autocracy_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Neutral_Autocracy_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Abdul Rahman Al Shater"
				picture = "abdul_rahman_al_shater.dds"
				ideology = Neutral_Autocracy
				traits = {
					neutrality_Neutral_Autocracy
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Autocracy_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_conservatism }
		if = { limit = { check_variable = { Neutral_conservatism_leader = 0 } }
			add_to_variable = { Neutral_conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mohammed Magariaf"
				desc = LBA_mohammed_magariaf_desc
				picture = "mohammed_magariaf.dds"
				ideology = Neutral_conservatism
				traits = {
					neutrality_Neutral_conservatism
					economist
					honest
					assassination_survivor
					ideological_crusader
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Neutral_conservatism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mustafa A. G. Abushagur"
				desc = LBA_mustafa_a_g_abushagur_desc
				picture = "mustafa_a_g_abushagur.dds"
				ideology = Neutral_conservatism
				traits = {
					neutrality_Neutral_conservatism
					scientist
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_Libertarian }
		if = { limit = { check_variable = { Neutral_Libertarian_leader = 0 } }
			add_to_variable = { Neutral_Libertarian_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Ahmed Shebani"
				desc = LBA_ahmed_shebani_desc
				picture = "ahmed_shebani.dds"
				ideology = Neutral_Libertarian
				traits = {
					neutrality_Neutral_Libertarian
					ex_engineer
					pro_israel
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Libertarian_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Neutral_Libertarian_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_Libertarian_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mohamed Sowan"
				picture = "mohammed_sowan.dds"
				ideology = Neutral_Libertarian
				traits = {
					neutrality_Neutral_Libertarian
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Libertarian_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_neutral_Social }
		if = { limit = { check_variable = { neutral_Social_leader = 0 } }
			add_to_variable = { neutral_Social_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mohammad Alareshiya"
				picture = "mohammad_alareshiya.dds"
				ideology = neutral_Social
				traits = {
					neutrality_neutral_Social
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { neutral_Social_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Nat_Fascism }
		if = { limit = { check_variable = { Nat_Fascism_leader = 0 } }
			add_to_variable = { Nat_Fascism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Muftah Lamlum"
				picture = "muftah_lamlum.dds"
				ideology = Nat_Fascism
				traits = {
					nationalist_Nat_Fascism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Fascism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Nat_Autocracy }
		if = { limit = { check_variable = { Nat_Autocracy_leader = 0 } }
			add_to_variable = { Nat_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Abu-Bakr Yunis Jabr"
				picture = "abu_bakr_yunis.dds"
				ideology = Nat_Autocracy
				traits = {
					nationalist_Nat_Autocracy
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Autocracy_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Monarchist }
		if = { limit = { check_variable = { Monarchist_leader = 0 } }
			add_to_variable = { Monarchist_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mohammed I"
				picture = "mohammed_el_senussi.dds"
				ideology = Monarchist
				traits = {
					nationalist_Monarchist
					agrarian_expert
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Monarchist_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Monarchist_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Monarchist_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Idris II"
				picture = "idris_bin_abdullah_al_senussi.dds"
				ideology = Monarchist
				traits = {
					nationalist_Monarchist
					illegitimate_king
					skilled_lobbyist
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Monarchist_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Monarchist_leader = 2 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Monarchist_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Khaled I"
				picture = "khaled_al_senussi.dds"
				ideology = Monarchist
				traits = {
					nationalist_Monarchist
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Monarchist_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { Monarchist_leader = 3 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Monarchist_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Ahmed I"
				picture = "ahmed_al_senussi.dds"
				ideology = Monarchist
				traits = {
					nationalist_Monarchist
					staunch_constitutionalist
					political_prisoner
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Monarchist_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}


}