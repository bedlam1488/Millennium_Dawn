modify_russians_support = {
	add_to_variable = { russians = modify_russians }
	custom_effect_tooltip = PMR_russians_tt
	clamp_variable = {
		var = russians
		min = 0
		max = 100
	}
}
modify_moldovans_support = {
	add_to_variable = { moldovans = modify_moldovans }
	custom_effect_tooltip = PMR_moldovans_tt
	clamp_variable = {
		var = moldovans
		min = 0
		max = 100
	}
}
modify_kolbasna_support = {
	add_to_variable = { kolbasna = modify_kolbasna }
	custom_effect_tooltip = PMR_kolbasna_tt
	clamp_variable = {
		var = kolbasna
		min = 0
		max = 90000
	}
}
modify_sheriffs_support = {
	add_to_variable = { sheriffs = modify_sheriffs }
	custom_effect_tooltip = PMR_sheriffs_tt
	clamp_variable = {
		var = sheriffs
		min = 0
		max = 100
	}
}