focus_tree = {
	id = tibesti_focus

	country = {
		factor = 0
		modifier = {
			add = 10
			original_tag = TIE
		}
	}

	default = no

	shared_focus = AFRICAN_UNION_shared_focus

	continuous_focus_position = { x = 1000 y = 1000 }

	focus = {
		id = TIE_birth_of_a_nation
		icon = africa_guy_shield
		cost = 5
		x = 0
		y = 0

		search_filters = {	FOCUS_FILTER_POLITICAL }

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_birth_of_a_nation"
			add_political_power = 100
		}

		ai_will_do = {
			factor = 10
		}

	}

	focus = {
		id = TIE_arm_tribesmen
		icon = Generic_African_Soldier
		cost = 5
		x = -1
		y = 1
		relative_position_id = TIE_birth_of_a_nation

		search_filters = {	FOCUS_FILTER_MILITARY }

		prerequisite = {
			focus = TIE_birth_of_a_nation
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_arm_tribesmen"
			division_template = {
				name = "Milishia Qabalia"
				regiments = {
					Militia_Bat = { x = 0 y = 0 }
					Militia_Bat = { x = 0 y = 1 }
				}
				support = {
				}
			}
			capital_scope = {
				create_unit = {
					division = "name = \"Milishia Qabalia\" division_template = \"Milishia Qabalia\" start_experience_factor = 0.2"
					owner = TIE
				}
				create_unit = {
					division = "name = \"Milishia Qabalia\" division_template = \"Milishia Qabalia\" start_experience_factor = 0.2"
					owner = TIE
				}
			}
		}

		ai_will_do = {
			factor = 10
		}

	}

	focus = {
		id = TIE_toubou_warrior_spirit
		icon = focus_generic_full_social_mobilization
		cost = 7
		x = 0
		y = 1
		relative_position_id = TIE_arm_tribesmen

		search_filters = {	FOCUS_FILTER_MILITARY }

		prerequisite = {
			focus = TIE_arm_tribesmen
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_toubou_warrior_spirit"
			add_ideas = TIE_warrior_spirit
		}

		ai_will_do = {
			factor = 10
			modifier = {
				factor = 1.5
				has_war = yes
			}
		}

	}

	focus = {
		id = TIE_tibesti_fortress
		icon = focus_SMB_a_land_of_mountains
		cost = 5
		x = 2
		y = 1
		relative_position_id = TIE_arm_tribesmen

		search_filters = {	FOCUS_FILTER_MILITARY }

		prerequisite = {
			focus = TIE_arm_tribesmen
		}

		available = {
			owns_state = 327
			controls_state = 327
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_tibesti_fortress"
			set_temp_variable = { treasury_change = -1.5 }
			modify_treasury_effect = yes
			327 = {
				add_building_construction = {
					type = bunker
					level = 1
					instant_build = yes
					province = 4902
				}
				add_building_construction = {
					type = bunker
					level = 1
					instant_build = yes
					province = 14592
				}
				add_building_construction = {
					type = bunker
					level = 1
					instant_build = yes
					province = 4961
				}
			}
		}

		ai_will_do = {
			factor = 10
			modifier = {
				factor = 1.5
				has_war = yes
			}
			modifier = {
				factor = 0
				check_variable = { interest_rate > 8 }
			}
		}

	}

	focus = {
		id = TIE_camel_corps
		icon = focus_generic_camel_corps
		cost = 7
		x = -1
		y = 2
		relative_position_id = TIE_arm_tribesmen

		search_filters = {	FOCUS_FILTER_MILITARY }

		prerequisite = {
			focus = TIE_toubou_warrior_spirit
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_camel_corps"
			swap_ideas = {
				remove_idea = TIE_warrior_spirit
				add_idea = TIE_warrior_spirit_2
			}
		}

		ai_will_do = {
			factor = 10
			modifier = {
				factor = 1.5
				has_war = yes
			}
		}

	}

	focus = {
		id = TIE_warrior_experience
		icon = desert_warfare
		cost = 10
		x = 1
		y = 2
		relative_position_id = TIE_arm_tribesmen

		search_filters = {	FOCUS_FILTER_MILITARY }

		prerequisite = {
			focus = TIE_toubou_warrior_spirit
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_warrior_experience"
			army_experience = 15
			add_tech_bonus = {
				category = CAT_land_doctrine
				bonus = 0.50
				uses = 2
			}
		}

		ai_will_do = {
			factor = 10
			modifier = {
				factor = 1.5
				has_war = yes
			}
		}

	}

	focus = {
		id = TIE_local_military_production
		icon = Focus_Rifle_Production
		cost = 10
		x = 0
		y = 3
		relative_position_id = TIE_arm_tribesmen

		search_filters = {	FOCUS_FILTER_MILITARY FOCUS_FILTER_ECONOMY }

		prerequisite = {
			focus = TIE_warrior_experience
		}
		prerequisite = {
			focus = TIE_camel_corps
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_local_military_production"
			set_temp_variable = { treasury_change = -7.5 }
			modify_treasury_effect = yes
			random_owned_state = {
				limit = {
					NOT = { has_state_category = state_inhospitable }
					NOT = { has_state_category = state_military_base }
				}
				add_building_construction = {
					type = arms_factory
					level = 1
					instant_build = yes
				}
				add_extra_state_shared_building_slots = 1
			}
		}

		ai_will_do = {
			factor = 10
			modifier = {
				factor = 1.5
				has_war = yes
			}
			modifier = {
				factor = 0
				check_variable = { interest_rate > 8 }
			}
		}

	}

	focus = {
		id = TIE_hold_clan_meeting
		icon = goal_six_party
		cost = 10
		x = 5
		y = 1
		relative_position_id = TIE_birth_of_a_nation

		search_filters = {	FOCUS_FILTER_INTERNAL_FACTION }

		prerequisite = {
			focus = TIE_birth_of_a_nation
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_hold_clan_meeting"
			set_temp_variable = { temp_opinion = 10 }
			change_landowners_opinion = yes
			set_temp_variable = { temp_opinion = 10 }
			change_the_ulema_opinion = yes
			set_temp_variable = { temp_opinion = 10 }
			change_the_military_opinion = yes
		}

		ai_will_do = {
			factor = 10
		}

	}

	focus = {
		id = TIE_establish_sharia_courts
		icon = sharia_law
		cost = 7
		x = -2
		y = 1
		relative_position_id = TIE_hold_clan_meeting

		search_filters = {	FOCUS_FILTER_POLITICAL }

		prerequisite = {
			focus = TIE_hold_clan_meeting
		}

		available = {
			has_government = fascism
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_establish_sharia_courts"
			add_ideas = TIE_sharia_law
		}

		ai_will_do = {
			factor = 10
		}

	}

	focus = {
		id = TIE_reappoint_derde
		icon = focus_generic_court
		cost = 7
		x = 0
		y = 1
		relative_position_id = TIE_hold_clan_meeting

		search_filters = {	FOCUS_FILTER_POLITICAL }

		prerequisite = {
			focus = TIE_hold_clan_meeting
		}

		available = {
			OR = {
				has_government = communism
				has_government = nationalist
			}
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_reappoint_derde"
			add_ideas = TIE_derde
		}

		ai_will_do = {
			factor = 10
		}

	}

	focus = {
		id = TIE_rely_on_clan_elders
		icon = old_man
		cost = 7
		x = 2
		y = 1
		relative_position_id = TIE_hold_clan_meeting

		search_filters = {	FOCUS_FILTER_POLITICAL }

		prerequisite = {
			focus = TIE_hold_clan_meeting
		}

		available = {
			OR = {
				has_government = neutrality
				has_government = democratic
			}
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_rely_on_clan_elders"
			add_ideas = TIE_clan_elders
		}

		ai_will_do = {
			factor = 10
		}

	}

	focus = {
		id = TIE_seek_help_from_boko_haram
		icon = isis_aks
		cost = 10
		x = -3
		y = 2
		relative_position_id = TIE_hold_clan_meeting

		search_filters = {	FOCUS_FILTER_EQUIPMENT FOCUS_FILTER_MANPOWER }

		prerequisite = {
			focus = TIE_establish_sharia_courts
		}

		available = {
			has_government = fascism
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_seek_help_from_boko_haram"
			add_manpower = 3000
			add_equipment_to_stockpile = {
				type = infantry_weapons
				amount = 1600
				producer = SOV
			}
		}

		ai_will_do = {
			factor = 10
		}

	}

	focus = {
		id = TIE_make_money_of_refugees
		icon = refugees
		cost = 7
		x = -3
		y = 3
		relative_position_id = TIE_hold_clan_meeting

		search_filters = {	FOCUS_FILTER_ECONOMY }

		prerequisite = {
			focus = TIE_seek_help_from_boko_haram
		}

		available = {
			has_government = fascism
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_seek_help_from_boko_haram"
			add_ideas = TIE_money_from_refugees
		}

		ai_will_do = {
			factor = 10
		}

	}

	focus = {
		id = TIE_break_clan_structure
		icon = break_free2
		cost = 7
		x = -1
		y = 2
		relative_position_id = TIE_hold_clan_meeting

		search_filters = {	FOCUS_FILTER_POLITICAL }

		prerequisite = {
			focus = TIE_establish_sharia_courts
			focus = TIE_reappoint_derde
		}

		available = {
			OR = {
				has_government = fascism
				has_government = communism
				has_government = nationalist
			}
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_break_clan_structure"
			swap_ideas = {
				add_idea = TIE_clan_structure_reduced
				remove_idea = TIE_clan_structure
			}
		}

		ai_will_do = {
			factor = 10
		}

	}

	focus = {
		id = TIE_large_scale_farming
		icon = focus_CHL_focus_on_cattle_and_wool
		cost = 7
		x = -1
		y = 3
		relative_position_id = TIE_hold_clan_meeting

		search_filters = {	FOCUS_FILTER_ECONOMY }

		prerequisite = {
			focus = TIE_break_clan_structure
		}

		available = {
			OR = {
				has_government = fascism
				has_government = communism
				has_government = nationalist
			}
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_large_scale_farming"
			set_temp_variable = { treasury_change = -3.75 }
			modify_treasury_effect = yes
			random_owned_state = {
				limit = {
					NOT = { has_state_category = state_inhospitable }
					NOT = { has_state_category = state_military_base }
				}
				add_building_construction = {
					type = agriculture_district
					level = 1
					instant_build = yes
				}
				add_extra_state_shared_building_slots = 1
			}
		}

		ai_will_do = {
			factor = 10
			modifier = {
				factor = 0
				check_variable = { interest_rate > 8 }
			}
		}

	}

	focus = {
		id = TIE_prepare_for_war
		icon = Generic_Liberation
		cost = 7
		x = -2
		y = 4
		relative_position_id = TIE_hold_clan_meeting

		search_filters = {	FOCUS_FILTER_STABILITY FOCUS_FILTER_WAR_SUPPORT }

		prerequisite = {
			focus = TIE_large_scale_farming
		}
		prerequisite = {
			focus = TIE_make_money_of_refugees
		}

		available = {
			has_government = fascism
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_prepare_for_war"
			add_stability = 0.10
			add_war_support = 0.10
		}

		ai_will_do = {
			factor = 10
		}

	}

	focus = {
		id = TIE_declare_the_caliphate
		icon = isis_allegiance
		cost = 7
		x = -3
		y = 5
		relative_position_id = TIE_hold_clan_meeting

		search_filters = {	FOCUS_FILTER_DIPLOMACY FOCUS_FILTER_WAR_SUPPORT }

		prerequisite = {
			focus = TIE_prepare_for_war
		}

		mutually_exclusive = {
			focus = TIE_join_libya
		}

		available = {
			has_government = fascism
			NOT = { is_subject = yes }
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_declare_the_caliphate"
			if = {
				limit = {
					NIG = { salafist_caliphate_are_in_power = yes }
					NIG = { has_completed_focus = NIG_declare_a_new_caliph }
				}
				NIG = { country_event = { id = TibestiFocusEvents.1 hours = 6 } }
			}
			else = {
				add_ideas = NIG_the_new_nigerian_caliph_idea
			}
		}

		ai_will_do = {
			factor = 10
		}

	}

	focus = {
		id = TIE_punch_above_our_weight
		icon = infilitration
		cost = 7
		x = 1
		y = 2
		relative_position_id = TIE_hold_clan_meeting

		search_filters = {	FOCUS_FILTER_INFLUENCE }

		prerequisite = {
			focus = TIE_reappoint_derde
			focus = TIE_rely_on_clan_elders
		}

		available = {
			OR = {
				has_government = communism
				has_government = nationalist
				has_government = democratic
				has_government = neutrality
			}
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_punch_above_our_weight"
			add_ideas = TIE_punching_above
		}

		ai_will_do = {
			factor = 10
		}

	}

	focus = {
		id = TIE_middle_of_nowhere
		icon = focus_BRA_reach_out_to_military_dem
		cost = 10
		x = 1
		y = 3
		relative_position_id = TIE_hold_clan_meeting

		search_filters = {	FOCUS_FILTER_INFLUENCE }

		prerequisite = {
			focus = TIE_punch_above_our_weight
		}

		available = {
			OR = {
				has_government = communism
				has_government = nationalist
				has_government = democratic
				has_government = neutrality
			}
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_middle_of_nowhere"
			every_neighbor_country = {
				set_temp_variable = { influence_target = THIS }
				set_temp_variable = { percent_change = 3 }
				set_temp_variable = { tag_index = ROOT }
				change_influence_percentage = yes
			}
		}

		ai_will_do = {
			factor = 10
		}

	}

	focus = {
		id = TIE_derde_guard
		icon = Generic_Soldier_Aiming
		cost = 7
		x = 0
		y = 4
		relative_position_id = TIE_hold_clan_meeting

		search_filters = {	FOCUS_FILTER_MILITARY }

		prerequisite = {
			focus = TIE_middle_of_nowhere
		}
		prerequisite = {
			focus = TIE_large_scale_farming
		}

		available = {
			OR = {
				has_government = communism
				has_government = nationalist
			}
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_derde_guard"
			swap_ideas = {
				add_idea = TIE_derde_guards
				remove_idea = TIE_derde
			}
		}

		ai_will_do = {
			factor = 10
		}

	}

	focus = {
		id = TIE_join_libya
		icon = Map_with_coat_of_arms
		cost = 5
		x = -1
		y = 5
		relative_position_id = TIE_hold_clan_meeting

		search_filters = {	FOCUS_FILTER_DIPLOMACY }

		mutually_exclusive = {
			focus = TIE_declare_the_caliphate
			focus = TIE_birthright_over_chad
		}

		prerequisite = {
			focus = TIE_prepare_for_war
			focus = TIE_derde_guard
		}

		available = {
			OR = {
				AND = {
					has_government = fascism
					LBA = { has_government = fascism }
				}
				has_government = communism
			}
			OR = {
				NOT = { is_subject = yes }
				AND = {
					is_subject = yes
					OVERLORD = { original_tag = LBA }
				}
			}
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_join_libya"
			LBA = {
				country_event = { id = TibestiFocusEvents.4 hours = 6 }
			}
		}

		ai_will_do = {
			factor = 10
		}

	}

	focus = {
		id = TIE_birthright_over_chad
		icon = attack_chad
		cost = 5
		x = 1
		y = 5
		relative_position_id = TIE_hold_clan_meeting

		will_lead_to_war_with = CHA

		search_filters = {	FOCUS_FILTER_ANNEXATION }

		mutually_exclusive = {
			focus = TIE_join_libya
			focus = TIE_adopt_cfa_franc
		}

		prerequisite = {
			focus = TIE_use_azza_to_industrialise
			focus = TIE_derde_guard
		}

		available = {
			OR = {
				has_government = nationalist
				has_government = neutrality
				AND = {
					has_government = democratic
					CHA = { NOT = { has_government = democratic } }
				}
			}
			country_exists = CHA
			CHA = { NOT = { is_subject_of = ROOT } }
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_birthright_over_chad"
			create_wargoal = {
				target = CHA
				type = puppet_wargoal_focus
			}
		}

		ai_will_do = {
			factor = 10
		}

	}

	focus = {
		id = TIE_continue_as_herders
		icon = focus_ARG_capitalize_the_beef_industry
		cost = 10
		x = 3
		y = 2
		relative_position_id = TIE_hold_clan_meeting

		search_filters = {	FOCUS_FILTER_ECONOMY }

		prerequisite = {
			focus = TIE_rely_on_clan_elders
		}

		available = {
			OR = {
				has_government = democratic
				has_government = neutrality
			}
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_continue_as_herders"
			set_temp_variable = { treasury_change = -2 }
			modify_treasury_effect = yes
			random_owned_state = {
				limit = {
					NOT = { has_state_category = state_inhospitable }
					NOT = { has_state_category = state_military_base }
				}
				add_extra_state_shared_building_slots = 2
			}
		}

		ai_will_do = {
			factor = 10
			modifier = {
				factor = 0
				check_variable = { interest_rate > 8 }
			}
		}

	}

	focus = {
		id = TIE_share_oases
		icon = tree_focus
		cost = 7
		x = 3
		y = 3
		relative_position_id = TIE_hold_clan_meeting

		search_filters = {	FOCUS_FILTER_POLITICAL }

		prerequisite = {
			focus = TIE_continue_as_herders
		}

		available = {
			OR = {
				has_government = democratic
				has_government = neutrality
			}
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_share_oases"
			swap_ideas = {
				add_idea = TIE_clan_structure_strengthened
				remove_idea = TIE_clan_structure
			}
		}

		ai_will_do = {
			factor = 10
		}

	}

	focus = {
		id = TIE_use_azza_to_industrialise
		icon = sustainable_development
		cost = 10
		x = 2
		y = 4
		relative_position_id = TIE_hold_clan_meeting

		search_filters = {	FOCUS_FILTER_ECONOMY }

		prerequisite = {
			focus = TIE_share_oases
		}
		prerequisite = {
			focus = TIE_middle_of_nowhere
		}

		available = {
			OR = {
				has_government = democratic
				has_government = neutrality
			}
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_use_azza_to_industrialise"
			set_temp_variable = { treasury_change = -7.5 }
			modify_treasury_effect = yes
			random_owned_state = {
				limit = {
					NOT = { has_state_category = state_inhospitable }
					NOT = { has_state_category = state_military_base }
				}
				add_building_construction = {
					type = industrial_complex
					level = 1
					instant_build = yes
				}
				add_extra_state_shared_building_slots = 1
			}
		}

		ai_will_do = {
			factor = 10
			modifier = {
				factor = 0
				check_variable = { interest_rate > 8 }
			}
		}

	}

	focus = {
		id = TIE_adopt_cfa_franc
		icon = AFR_western_frank
		cost = 7
		x = 3
		y = 5
		relative_position_id = TIE_hold_clan_meeting

		search_filters = {	FOCUS_FILTER_ECONOMY }

		mutually_exclusive = {
			focus = TIE_birthright_over_chad
		}

		prerequisite = {
			focus = TIE_use_azza_to_industrialise
		}

		available = {
			has_government = democratic
			NOT = { has_war_with = FRA }
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_adopt_cfa_franc"
			add_ideas = cfa_Franc
		}

		ai_will_do = {
			factor = 10
		}

	}

	focus = {
		id = TIE_diversify_our_economy
		icon = focus_generic_industrialists
		cost = 7
		x = -6
		y = 1
		relative_position_id = TIE_birth_of_a_nation

		search_filters = {	FOCUS_FILTER_ECONOMY }

		prerequisite = {
			focus = TIE_birth_of_a_nation
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_diversify_our_economy"
			increase_economic_growth = yes
		}

		ai_will_do = {
			factor = 10
		}

	}

	focus = {
		id = TIE_mine_natron
		icon = PB_Generic_Mining_Industry
		cost = 7
		x = -1
		y = 1
		relative_position_id = TIE_diversify_our_economy

		search_filters = {	FOCUS_FILTER_ECONOMY }

		prerequisite = {
			focus = TIE_diversify_our_economy
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_mine_natron"
			add_ideas = TIE_natron
		}

		ai_will_do = {
			factor = 10
		}

	}

	focus = {
		id = TIE_survey_for_gold
		icon = gold_standard
		cost = 10
		x = 1
		y = 1
		relative_position_id = TIE_diversify_our_economy

		search_filters = {	FOCUS_FILTER_RESOURCE }

		prerequisite = {
			focus = TIE_diversify_our_economy
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_mine_natron"
			set_temp_variable = { treasury_change = -4 }
			modify_treasury_effect = yes
			random_owned_state = {
				limit = {
					NOT = { has_state_category = state_inhospitable }
					NOT = { has_state_category = state_military_base }
				}
				add_resource = {
					type = chromium
					amount = 4
				}
			}
		}

		ai_will_do = {
			factor = 10
			modifier = {
				factor = 0
				check_variable = { interest_rate > 8 }
			}
		}

	}

	focus = {
		id = TIE_mining_in_aozou
		icon = uranium_processing
		cost = 10
		x = -2
		y = 2
		relative_position_id = TIE_diversify_our_economy

		search_filters = {	FOCUS_FILTER_RESOURCE }

		prerequisite = {
			focus = TIE_mine_natron
		}

		available = {
			owns_state = 580
			LBA = {	NOT = { has_completed_focus = LBA_mining_in_aozou }	}
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_mining_in_aozou"
			set_temp_variable = { treasury_change = -15 }
			modify_treasury_effect = yes
			add_resource = {
				type = tungsten
				amount = 15
				state = 580
			}
		}

		ai_will_do = {
			factor = 10
			modifier = {
				factor = 0
				check_variable = { interest_rate > 8 }
			}
		}

	}

	focus = {
		id = TIE_new_trans_saharan_trade
		icon = GFX_goal_generic_trade
		cost = 7
		x = 0
		y = 2
		relative_position_id = TIE_diversify_our_economy

		search_filters = {	FOCUS_FILTER_ECONOMY }

		prerequisite = {
			focus = TIE_mine_natron
		}
		prerequisite = {
			focus = TIE_survey_for_gold
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_new_trans_saharan_trade"
			add_ideas = TIE_trans_saharan_trade_1
		}

		ai_will_do = {
			factor = 10
		}

	}

	focus = {
		id = TIE_trans_saharan_trade_niger
		icon = ExpandTrade
		cost = 7
		x = -1
		y = 3
		relative_position_id = TIE_diversify_our_economy

		search_filters = {	FOCUS_FILTER_RESOURCE }

		prerequisite = {
			focus = TIE_new_trans_saharan_trade
		}

		available = {
			country_exists = NGR
			NOT = { has_war_with = NGR }
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_trans_saharan_trade_niger"
			if = {
				limit = {
					has_idea = TIE_trans_saharan_trade_1
				}
				swap_ideas = {
					add_idea = TIE_trans_saharan_trade_2
					remove_idea = TIE_trans_saharan_trade_1
				}
			}
			else_if = {
				limit = {
					has_idea = TIE_trans_saharan_trade_2
				}
				swap_ideas = {
					add_idea = TIE_trans_saharan_trade_3
					remove_idea = TIE_trans_saharan_trade_2
				}
			}
			else_if = {
				limit = {
					has_idea = TIE_trans_saharan_trade_3
				}
				swap_ideas = {
					add_idea = TIE_trans_saharan_trade_4
					remove_idea = TIE_trans_saharan_trade_3
				}
			}
			set_temp_variable = { treasury_change = -4 }
			modify_treasury_effect = yes
			random_owned_state = {
				limit = {
					NOT = { has_state_category = state_inhospitable }
					NOT = { has_state_category = state_military_base }
				}
				add_resource = {
					type = chromium
					amount = 4
				}
			}
		}

		ai_will_do = {
			factor = 10
			modifier = {
				factor = 0
				check_variable = { interest_rate > 8 }
			}
		}

	}

	focus = {
		id = TIE_trans_saharan_trade_libya
		icon = export_economy
		cost = 7
		x = 1
		y = 3
		relative_position_id = TIE_diversify_our_economy

		search_filters = {	FOCUS_FILTER_RESOURCE }

		prerequisite = {
			focus = TIE_new_trans_saharan_trade
		}

		available = {
			country_exists = LBA
			NOT = { has_war_with = LBA }
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_trans_saharan_trade_niger"
			if = {
				limit = {
					has_idea = TIE_trans_saharan_trade_1
				}
				swap_ideas = {
					add_idea = TIE_trans_saharan_trade_2
					remove_idea = TIE_trans_saharan_trade_1
				}
			}
			else_if = {
				limit = {
					has_idea = TIE_trans_saharan_trade_2
				}
				swap_ideas = {
					add_idea = TIE_trans_saharan_trade_3
					remove_idea = TIE_trans_saharan_trade_2
				}
			}
			else_if = {
				limit = {
					has_idea = TIE_trans_saharan_trade_3
				}
				swap_ideas = {
					add_idea = TIE_trans_saharan_trade_4
					remove_idea = TIE_trans_saharan_trade_3
				}
			}
			set_temp_variable = { treasury_change = -4 }
			modify_treasury_effect = yes
			random_owned_state = {
				limit = {
					NOT = { has_state_category = state_inhospitable }
					NOT = { has_state_category = state_military_base }
				}
				add_resource = {
					type = oil
					amount = 4
				}
			}
		}

		ai_will_do = {
			factor = 10
			modifier = {
				factor = 0
				check_variable = { interest_rate > 8 }
			}
		}

	}
	focus = {
		id = TIE_trans_saharan_trade_chad
		icon = trade
		cost = 7
		x = 0
		y = 4
		relative_position_id = TIE_diversify_our_economy

		search_filters = {	FOCUS_FILTER_RESOURCE }

		prerequisite = {
			focus = TIE_trans_saharan_trade_niger
		}
		prerequisite = {
			focus = TIE_trans_saharan_trade_libya
		}

		available = {
			country_exists = CHA
			NOT = { has_war_with = CHA }
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_trans_saharan_trade_niger"
			if = {
				limit = {
					has_idea = TIE_trans_saharan_trade_1
				}
				swap_ideas = {
					add_idea = TIE_trans_saharan_trade_2
					remove_idea = TIE_trans_saharan_trade_1
				}
			}
			else_if = {
				limit = {
					has_idea = TIE_trans_saharan_trade_2
				}
				swap_ideas = {
					add_idea = TIE_trans_saharan_trade_3
					remove_idea = TIE_trans_saharan_trade_2
				}
			}
			else_if = {
				limit = {
					has_idea = TIE_trans_saharan_trade_3
				}
				swap_ideas = {
					add_idea = TIE_trans_saharan_trade_4
					remove_idea = TIE_trans_saharan_trade_3
				}
			}
			set_temp_variable = { treasury_change = -4 }
			modify_treasury_effect = yes
			random_owned_state = {
				limit = {
					NOT = { has_state_category = state_inhospitable }
					NOT = { has_state_category = state_military_base }
				}
				add_resource = {
					type = chromium
					amount = 2
				}
				add_resource = {
					type = oil
					amount = 2
				}
			}
		}

		ai_will_do = {
			factor = 10
			modifier = {
				factor = 0
				check_variable = { interest_rate > 8 }
			}
		}

	}

	focus = {
		id = TIE_build_an_airport
		icon = build_airport
		cost = 10
		x = 2
		y = 2
		relative_position_id = TIE_diversify_our_economy

		search_filters = {	FOCUS_FILTER_AIRCRAFT }

		prerequisite = {
			focus = TIE_survey_for_gold
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_build_an_airport"
			set_temp_variable = { treasury_change = -2.5 }
			modify_treasury_effect = yes
			capital_scope = {
				add_building_construction = {
					type = air_base
					level = 1
					instant_build = yes
				}
			}
		}

		ai_will_do = {
			factor = 10
			modifier = {
				factor = 0
				check_variable = { interest_rate > 8 }
			}
		}

	}

	focus = {
		id = TIE_establish_air_force
		icon = blr_mirage_2000c
		cost = 10
		x = 3
		y = 3
		relative_position_id = TIE_diversify_our_economy

		search_filters = {	FOCUS_FILTER_AIRCRAFT }

		prerequisite = {
			focus = TIE_build_an_airport
		}
		prerequisite = {
			focus = TIE_camel_corps
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_establish_air_force"
			set_temp_variable = { treasury_change = -5 }
			modify_treasury_effect = yes
			add_equipment_to_stockpile = {
				variant_name = "Mirage F1"
				type = small_plane_airframe_1
				amount = 5
				producer = FRA
			}
		}

		ai_will_do = {
			factor = 10
			modifier = {
				factor = 0
				check_variable = { interest_rate > 8 }
			}
		}

	}

	focus = {
		id = TIE_import_tourists
		icon = generic_office_construct
		cost = 10
		x = 2
		y = 4
		relative_position_id = TIE_diversify_our_economy

		search_filters = {	FOCUS_FILTER_ECONOMY }

		prerequisite = {
			focus = TIE_build_an_airport
		}

		available = {
			any_other_country = {
				capital_scope = { is_on_continent = europe }
				OR = {
					has_idea = regional_power
					has_idea = large_power
					has_idea = great_power
					has_idea = superpower
				}
				influence_higher_10 = yes
			}
		}

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus TIE_import_tourists"
			set_temp_variable = { treasury_change = -12 }
			modify_treasury_effect = yes
			capital_scope = {
				add_building_construction = {
					type = offices
					level = 1
					instant_build = yes
				}
				add_extra_state_shared_building_slots = 1
			}
		}

		ai_will_do = {
			factor = 10
			modifier = {
				factor = 0
				check_variable = { interest_rate > 8 }
			}
		}

	}
}
