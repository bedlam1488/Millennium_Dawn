﻿division_template = {
	name = "Special Forces Battalion"

	regiments = {
		Special_Forces = { x = 0 y = 0 }
	}
	priority = 2
}

division_template = {
	name = "Light Tank Regiment"

	regiments = {
		L_arm_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
	}
	support = {
		SP_Arty_Battery = { x = 0 y = 0 }
		L_Engi_Comp = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Mechanized Regiment"

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
	}
	support = {
		armor_Recce_Comp = { x = 0 y = 0 }
		SP_Arty_Battery = { x = 0 y = 1 }
		L_Engi_Comp = { x = 0 y = 2 }
	}
}

division_template = {
	name = "Infantry Brigade"

	regiments = {
		Mot_Inf_Bat = { x = 0 y = 0 }
		Mot_Inf_Bat = { x = 0 y = 1 }
		Mot_Inf_Bat = { x = 0 y = 2 }
	}
	support = {
		armor_Recce_Comp = { x = 0 y = 0 }
		Arty_Battery = { x = 0 y = 1 }
		L_Engi_Comp = { x = 0 y = 2 }
	}
}

division_template = {
	name = "Parachute Regiment"

	regiments = {
		L_Air_Inf_Bat = { x = 0 y = 0 }
		L_Air_Inf_Bat = { x = 0 y = 1 }
	}
	support = {
		L_Recce_Comp = { x = 0 y = 0 }
	}
}

units = {
	division = {
		name = "4th Special Forces Regiment"
		location = 1247
		division_template = "Special Forces Battalion"
		start_experience_factor = 0.7
		start_equipment_factor = 0.01
	}
	division = {
		name = "5th Special Forces Regiment"
		location = 10566
		division_template = "Special Forces Battalion"
		start_experience_factor = 0.7
		start_equipment_factor = 0.01
	}
	division = {
		name = "1st Special Service Battalion"
		location = 12648
		division_template = "Light Tank Regiment"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "1st Infantry Regiment"
		location = 12648
		division_template = "Mechanized Regiment"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "12th Infantry Brigade"
		location = 1363
		division_template = "Infantry Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "21st Infantry Brigade"
		location = 10400
		division_template = "Infantry Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "58th Infantry Brigade"
		location = 10400
		division_template = "Infantry Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "60th Infantry Brigade"
		location = 10400
		division_template = "Infantry Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "37th Infantry Brigade"
		location = 10400
		division_template = "Infantry Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "1st Parachute Battalion"
		location = 12648
		division_template = "Parachute Regiment"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}

}
instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons2 #1985
		amount = 6000
		producer = SAF
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_2 #ZT-3
		amount = 100
		producer = SAF
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_1 #Milan
		amount = 200
		producer = FRA
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1 #ZT3 Ingwe
		amount = 100
		producer = SAF
	}

	add_equipment_to_stockpile = {
		type = command_control_equipment2 #1995
		amount = 800
		producer = SAF
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_1 #Starstreak
		amount = 200
		producer = ENG
	}

	### Vehicles ###
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 620
		producer = SAF
		variant_name = "Ratel 90"
	}

	add_equipment_to_stockpile = {
		type = light_tank_hull_2
		amount = 242
		producer = SAF
		variant_name = "Rooikat"
	}

	add_equipment_to_stockpile = {
		type = util_vehicle_2 #Casspir
		amount = 429
		producer = SAF
	}

	add_equipment_to_stockpile = {
		type = util_vehicle_3 #Mamba
		amount = 538
		producer = SAF
	}

	add_equipment_to_stockpile = {
		type = artillery_1 #G5
		amount = 72
		producer = SAF
	}

	#Helicopters
	add_equipment_to_stockpile = {
		type = transport_helicopter2 #Atlas Oryx
		amount = 51
		producer = SAF
	}
	add_equipment_to_stockpile = {
		type = attack_helicopter_hull_1
		amount = 11
		producer = SAF
		variant_name = "Rooivalk"
	}
}