﻿instant_effect = {

	####################
	## Transports ######
	####################
	
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		amount = 88
		variant_name = "C-141 Starlifter"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		amount = 400
		variant_name = "C-130 Hercules"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		amount = 20
		variant_name = "C-130J Super Hercules"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		amount = 104
		variant_name = "C-5 Galaxy"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_2
		amount = 58
		variant_name = "C-17 Globemaster III"
		producer = USA
	}


	################################
	### Land Based Fighters ########
	################################

	add_equipment_to_stockpile = {
		type = medium_plane_fighter_airframe_2
		amount = 81 #400 deployed
		variant_name = "F-15C Eagle"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = medium_plane_airframe_2
		amount = 60 #150 deployed
		variant_name = "F-15E Strike Eagle"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_2
		amount = 200
		variant_name = "F-16A Blk 20"
		producer = USA
	}
	#add_equipment_to_stockpile = {
	#	type = small_plane_strike_airframe_2
	#	amount = 548
	#	variant_name = "F-16C Blk 40/42"
	#	producer = USA
	#}
	#add_equipment_to_stockpile = {
	#	type = small_plane_strike_airframe_2
	#	amount = 200
	#	variant_name = "F-16C Blk 50/52"
	#	producer = USA
	#}
	add_equipment_to_stockpile = {
		type = small_plane_cas_airframe_2
		amount = 2 #50 deployed
		variant_name = "F-117A Nighthawk"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = medium_plane_cas_airframe_2
		amount = 112 #250 deployed
		variant_name = "A-10A Thunderbolt II"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = medium_plane_airframe_3
		amount = 6
		variant_name = "F-22 Raptor"
		producer = USA
	}
	################################
	### Naval Based Fighters #######
	################################

	add_equipment_to_stockpile = {
		type = cv_medium_plane_fighter_airframe_1
		variant_name = "F-14A Tomcat"
		amount = 74
		producer = USA
	}
	#Numbers kept for reference. Planes now assigned to their historical squadrons
	#add_equipment_to_stockpile = {
	#	type = cv_medium_plane_fighter_airframe_1
	#	variant_name = "F-14B Tomcat"
	#	amount = 73
	#	producer = USA
	#}
	#add_equipment_to_stockpile = {
	#	type = cv_medium_plane_fighter_airframe_1
	#	variant_name = "F-14D Tomcat"
	#	amount = 46
	#	producer = USA
	#}
	add_equipment_to_stockpile = {
		type = cv_medium_plane_airframe_2
		amount = 222
		variant_name = "F/A-18A/B Hornet"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = cv_medium_plane_airframe_2
		amount = 125 #280 deployed
		variant_name = "F/A-18C Hornet"
		producer = USA
	}
	#Numbers kept for reference. Planes now assigned to their historical VMFA(AW) squadrons
	#add_equipment_to_stockpile = {
	#	type = cv_medium_plane_cas_airframe_2
	#	amount = 130
	#	variant_name = "F/A-18D Hornet"
	#	producer = USA
	#}
	#Numbers kept for reference. Planes now assigned to their historical test squadrons
	#add_equipment_to_stockpile = {
	#	type = cv_medium_plane_fighter_airframe_2
	#	amount = 9
	#	variant_name = "F/A-18E Super Hornet"
	#	producer = USA
	#}
	#add_equipment_to_stockpile = {
	#	type = cv_medium_plane_airframe_2
	#	amount = 5
	#	variant_name = "F/A-18F Super Hornet"
	#	producer = USA
	#}
	add_equipment_to_stockpile = {
		type = cv_small_plane_strike_airframe_1 #AV8B
		amount = 49 #70 deployed
		variant_name = "AV-8B Harrier II"
		producer = USA
	}

	################################
	### Strat Bombers ##############
	################################

	add_equipment_to_stockpile = {
		type = large_plane_airframe_2 #B1-B
		amount = 16 #75 deployed
		variant_name = "B-1B Lancer"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = large_plane_airframe_1 #b52
		amount = 17 #75 deployed
		variant_name = "B-52H Stratofortress"
		producer = USA
	}
	#add_equipment_to_stockpile = {
	#	type = large_plane_airframe_3 #B2
	#	amount = 21
	#	variant_name = "B-2 Spirit"
	#	producer = USA
	#}


	####################
	## Gunships ########
	####################

	add_equipment_to_stockpile = {
		type = large_plane_cas_airframe_1
		amount = 21
		variant_name = "AC-130H Spectre"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = large_plane_cas_airframe_1
		amount = 31
		variant_name = "AC-130U Spooky"
		producer = USA
	}


	#######################
	## AWACS  #############
	#######################

	add_equipment_to_stockpile = {
		type = large_plane_awacs_airframe_2
		amount = 33
		variant_name = "E-3 Sentry"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = large_plane_awacs_airframe_1
		amount = 31
		variant_name = "U-2S Dragon Lady"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = cv_medium_plane_scout_airframe_2
		amount = 60
		variant_name = "E-2 Hawkeye"
		producer = USA
	}


	####################
	## Drones ##########
	####################

	add_equipment_to_stockpile = {
		type = small_plane_suicide_airframe_2
		amount = 50
		variant_name = "RQ-1 Predator"
		producer = USA
	}


	####################
	## Patrol ##########
	####################

	add_equipment_to_stockpile = {
		type = large_plane_maritime_patrol_airframe_1 #P-3C Orion
		amount = 208
		variant_name = "P-3 Orion"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = cv_medium_plane_maritime_patrol_airframe_1
		amount = 112
		variant_name = "S-3B Viking"
		producer = USA
	}
}
air_wings = {
	827 = { #Japan/Korea wings, based in Okinawa for HOI4 mechanics
		small_plane_strike_airframe_2 = { owner = "USA" amount = 50 version_name = "F-16C Blk 50/52" }
		name = "8th Fighter Wing 'Wolf Pack'" #Kunsan AB Korea
		start_experience_factor = 0.5
		#
		small_plane_strike_airframe_2 = { owner = "USA" amount = 50 version_name = "F-16C Blk 40/42" }
		name = "35th Fighter Wing 'Attack to Defend'" #Misawa AB Japan
		start_experience_factor = 0.5
		#
		small_plane_strike_airframe_2 = { owner = "USA" amount = 50 version_name = "F-16C Blk 40/42" }
		name = "51st Fighter Wing 'Leading the Charge'" #Osan AB Korea
		start_experience_factor = 0.5
		#
		medium_plane_fighter_airframe_2 = { owner = "USA" amount = 50 version_name = "F-15C Eagle" }
		name = "18th Fighter Wing 'With Talons and Beak'"
		start_experience_factor = 0.5
	}
	790 = { #North Carolina
		medium_plane_airframe_2 = { owner = "USA" amount = 50 version_name = "F-15E Strike Eagle" }
		name = "4th Fighter Wing 'Fourth But First'"
		start_experience_factor = 0.5
	}
	791 = { #South Carolina
		small_plane_strike_airframe_2 = { owner = "USA" amount = 50 version_name = "F-16C Blk 50/52" }
		name = "20th Fighter Wing 'Victory by Valor'" #Shaw AFB
		start_experience_factor = 0.5
		#
		small_plane_strike_airframe_2 = { owner = "USA" amount = 50 version_name = "F-16C Blk 40/42" }
		name = "169th Fighter Wing 'Swamp Foxes'" #ANG
		start_experience_factor = 0.5
	}
	1061 = { #Italy
		small_plane_strike_airframe_2 = { owner = "USA" amount = 50 version_name = "F-16C Blk 40/42" }
		name = "31st Fighter Wing 'Return With Honor'" #Aviano AB
		start_experience_factor = 0.5
	}
	966 = { #Germany
		small_plane_strike_airframe_2 = { owner = "USA" amount = 50 version_name = "F-16C Blk 50/52" }
		name = "52nd Fighter Wing 'Seek, Attack, Destroy'" #Spangdahlem AB
		start_experience_factor = 0.5
	}
	14 = { #RAF Lakenheath
		medium_plane_airframe_2 = { owner = "USA" amount = 50 version_name = "F-15E Strike Eagle" }
		name = "48th Fighter Wing 'Statue of Liberty'"
		start_experience_factor = 0.5
	}
	806 = { #Arizona
		small_plane_strike_airframe_2 = { owner = "USA" amount = 50 version_name = "F-16C Blk 50/52" }
		name = "56th Fighter Wing 'Thunderbolts'" #Luke AFB
		start_experience_factor = 0.5
		#
		medium_plane_cas_airframe_2 = { owner = "USA" amount = 50 version_name = "A-10A Thunderbolt II" }
		name = "355th Wing 'Our Might Always'"
		start_experience_factor = 0.5
		#
		small_plane_strike_airframe_2 = { owner = "USA" amount = 50 version_name = "F-16C Blk 40/42" }
		name = "162nd Wing" #ANG
		start_experience_factor = 0.4
	}
	777 = { #Michigan
		medium_plane_cas_airframe_2 = { owner = "USA" amount = 50 version_name = "A-10A Thunderbolt II" }
		name = "127th Wing" #ANG
		start_experience_factor = 0.4
	}
	808 = { #Idaho
		medium_plane_airframe_2 = { owner = "USA" amount = 50 version_name = "F-15E Strike Eagle" }
		name = "366th Fighter Wing 'Fortune Favors the Bold'"
		start_experience_factor = 0.5
		#
		medium_plane_cas_airframe_2 = { owner = "USA" amount = 50 version_name = "A-10A Thunderbolt II" }
		name = "124th Fighter Wing" #ANG
		start_experience_factor = 0.4
	}
	814 = { #Alaska
		small_plane_strike_airframe_2 = { owner = "USA" amount = 50 version_name = "F-16C Blk 40/42" }
		name = "354th Fighter Wing 'Valor in Combat'" #Eielson AFB
		start_experience_factor = 0.5
		#
		medium_plane_fighter_airframe_2 = { owner = "USA" amount = 50 version_name = "F-15C Eagle" }
		name = "3rd Wing 'Non Solum Armis'"
		start_experience_factor = 0.5
	}
	805 = { #Utah
		small_plane_strike_airframe_2 = { owner = "USA" amount = 50 version_name = "F-16C Blk 40/42" }
		name = "388th Fighter Wing 'Libertas Vel Mors'" #Hills AFB
		start_experience_factor = 0.5
	}
	817 = { #Hawaii
		medium_plane_fighter_airframe_2 = { owner = "USA" amount = 50 version_name = "F-15C Eagle" }
		name = "15th Wing"
		start_experience_factor = 0.5
	}
	772 = { #Maryland/Washington DC
		small_plane_strike_airframe_2 = { owner = "USA" amount = 50 version_name = "F-16C Blk 40/42" }
		name = "113th Fighter Wing 'Capital Guardians'" #ANG
		start_experience_factor = 0.4
		#
		medium_plane_fighter_airframe_2 = { owner = "USA" amount = 50 version_name = "F-15C Eagle" }
		name = "1st Fighter Wing 'Aut Vincere Aut Mori'"
		start_experience_factor = 0.5
		#
		medium_plane_fighter_airframe_2 = { owner = "USA" amount = 50 version_name = "F-15C Eagle" }
		name = "192nd Wing"
		start_experience_factor = 0.4
		#
		medium_plane_cas_airframe_2 = { owner = "USA" amount = 50 version_name = "A-10A Thunderbolt II" }
		name = "175th Wing 'Librati In Promptu'" #ANG
		start_experience_factor = 0.4
	}
	783 = { #Missouri
		large_plane_airframe_3 = { owner = "USA" amount = 21 version_name = "B-2 Spirit" }
		name = "509th Bomb Wing 'Defensor Vindex'"
		start_experience_factor = 0.5
	}
	786 = { #South Dakota
		small_plane_strike_airframe_2 = { owner = "USA" amount = 50 version_name = "F-16A Blk 20" }
		name = "114th Fighter Wing 'Lobos'"
		start_experience_factor = 0.4
		#
		large_plane_airframe_2 = { owner = "USA" amount = 25 version_name = "B-1B Lancer" }
		name = "28th Bomb Wing 'Guardian of the North'"
		start_experience_factor = 0.5
	}
	787 = { #North Dakota
		large_plane_airframe_1 = { owner = "USA" amount = 25 version_name = "B-52H Stratofortress" }
		name = "5th Bomb Wing 'Warbirds'"
		start_experience_factor = 0.5
	}
	779 = { #Wisconsin
		small_plane_strike_airframe_2 = { owner = "USA" amount = 50 version_name = "F-16A Blk 20" }
		name = "115th Fighter Wing"
		start_experience_factor = 0.4
	}
	776 = { #Indiana
		small_plane_strike_airframe_2 = { owner = "USA" amount = 50 version_name = "F-16A Blk 20" }
		name = "122nd Fighter Wing"
		start_experience_factor = 0.4
	}
	792 = { #Georgia
		large_plane_airframe_2 = { owner = "USA" amount = 25 version_name = "B-1B Lancer" }
		name = "116th Bomb Wing 'Vincet Amor Patriae'"
		start_experience_factor = 0.4
		#
		medium_plane_cas_airframe_2 = { owner = "USA" amount = 50 version_name = "A-10A Thunderbolt II" }
		name = "23rd Fighter Group 'Tiger with Wings'"
		start_experience_factor = 0.5
	}
	797 = { #Louisiana
		large_plane_airframe_1 = { owner = "USA" amount = 25 version_name = "B-52H Stratofortress" }
		name = "2nd Bomb Wing 'We Defend Liberty'"
		start_experience_factor = 0.5
		#
		large_plane_airframe_1 = { owner = "USA" amount = 25 version_name = "B-52H Stratofortress" }
		name = "917th Wing"
		start_experience_factor = 0.4
	}
	799 = { #Oklahoma
		small_plane_strike_airframe_2 = { owner = "USA" amount = 50 version_name = "F-16C Blk 40/42" }
		name = "138th Fighter Wing 'Strength through Preparedness'"
		start_experience_factor = 0.4
	}
	804 = { #Colorado
		small_plane_strike_airframe_2 = { owner = "USA" amount = 50 version_name = "F-16C Blk 40/42" }
		name = "140th Wing"
		start_experience_factor = 0.4
	}
	780 = { #Minnesota
		small_plane_strike_airframe_2 = { owner = "USA" amount = 50 version_name = "F-16C Blk 40/42" }
		name = "148th Fighter Wing 'Bulldogs'"
		start_experience_factor = 0.4
	}
	769 = { #New York
		small_plane_strike_airframe_2 = { owner = "USA" amount = 50 version_name = "F-16A Blk 20" }
		name = "174th Fighter Wing 'The Boys From Syracuse'"
		start_experience_factor = 0.4
	}
	770 = { #New Jersey
		small_plane_strike_airframe_2 = { owner = "USA" amount = 50 version_name = "F-16C Blk 40/42" }
		name = "177th Fighter Wing 'Jersey Devils'"
		start_experience_factor = 0.4
	}
	775 = { #Ohio
		small_plane_strike_airframe_2 = { owner = "USA" amount = 50 version_name = "F-16C Blk 40/42" }
		name = "180th Fighter Wing 'Stingers'"
		start_experience_factor = 0.4
	}
	800 = { #Texas
		small_plane_strike_airframe_2 = { owner = "USA" amount = 50 version_name = "F-16C Blk 40/42" }
		name = "301st Fighter Wing" #Forth Worth
		start_experience_factor = 0.4 #USAF reserve
		#
		small_plane_strike_airframe_2 = { owner = "USA" amount = 50 version_name = "F-16C Blk 40/42" }
		name = "149th Fighter Wing" #Forth Worth
		start_experience_factor = 0.4 #ANG
		#
		large_plane_airframe_2 = { owner = "USA" amount = 25 version_name = "B-1B Lancer" }
		name = "28thBomb Wing 'Mors Ab Alto'"
		start_experience_factor = 0.5
	}
	795 = { #Florida
		small_plane_strike_airframe_2 = { owner = "USA" amount = 50 version_name = "F-16C Blk 40/42" }
		name = "482nd Fighter Wing" #Eielson AFB
		start_experience_factor = 0.4 #USAF reserve
		#
		medium_plane_fighter_airframe_2 = { owner = "USA" amount = 50 version_name = "F-15C Eagle" }
		name = "325th Fighter Wing 'Locare et Liquidare'"
		start_experience_factor = 0.5
	}
	803 = { #New Mexico
		small_plane_cas_airframe_2 = { owner = "USA" amount = 50 version_name = "F-117A Nighthawk" }
		name = "49th Fighter Wing 'Tutor Et Ultor'"
		start_experience_factor = 0.5
	}
	810 = { #Oregon
		medium_plane_fighter_airframe_2 = { owner = "USA" amount = 50 version_name = "F-15C Eagle" }
		name = "142nd Wing 'Redhawks'" #ANG
		start_experience_factor = 0.4
	}
	811 = { #California
		medium_plane_fighter_airframe_2 = { owner = "USA" amount = 50 version_name = "F-15C Eagle" }
		name = "144th Fighter Wing" #ANG
		start_experience_factor = 0.4
	}
}
air_wings = {
	811 = {
		##### test squadrons #####
		cv_medium_plane_fighter_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18E Super Hornet" } #VX-1
		start_experience_factor = 0.5
		cv_medium_plane_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18F Super Hornet" } #VX-9
		start_experience_factor = 0.5
	}
	"USS Kitty Hawk (CV-63)" = {
		cv_medium_plane_fighter_airframe_1 = { owner = "USA" amount = 10 version_name = "F-14D Tomcat" } #VF-2
		start_experience_factor = 0.5
		cv_medium_plane_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18C Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18C Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_scout_airframe_2 = { owner = "USA" amount = 10 version_name = "E-2 Hawkeye" }
		start_experience_factor = 0.5
		cv_medium_plane_maritime_patrol_airframe_1 = { owner = "USA" amount = 10 version_name = "S-3B Viking" }
		start_experience_factor = 0.5
	}
	"USS Constellation (CV-64)" = {
		cv_medium_plane_fighter_airframe_1 = { owner = "USA" amount = 10 version_name = "F-14B Tomcat" } #VF-11
		start_experience_factor = 0.5
		cv_medium_plane_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18C Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18C Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_scout_airframe_2 = { owner = "USA" amount = 10 version_name = "E-2 Hawkeye" }
		start_experience_factor = 0.5
		cv_medium_plane_maritime_patrol_airframe_1 = { owner = "USA" amount = 10 version_name = "S-3B Viking" }
		start_experience_factor = 0.5
	}
	"USS Enterprise (CVN-65)" = {
		cv_medium_plane_fighter_airframe_1 = { owner = "USA" amount = 10 version_name = "F-14A Tomcat" } #VF-14
		start_experience_factor = 0.5
		cv_medium_plane_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18C Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18C Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_scout_airframe_2 = { owner = "USA" amount = 10 version_name = "E-2 Hawkeye" }
		start_experience_factor = 0.5
		cv_medium_plane_maritime_patrol_airframe_1 = { owner = "USA" amount = 10 version_name = "S-3B Viking" }
		start_experience_factor = 0.5
	}
	"USS John F. Kennedy (CV-67)" = {
		cv_medium_plane_fighter_airframe_1 = { owner = "USA" amount = 10 version_name = "F-14D Tomcat" } #VF-31
		start_experience_factor = 0.5
		cv_medium_plane_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18C Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18C Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_scout_airframe_2 = { owner = "USA" amount = 10 version_name = "E-2 Hawkeye" }
		start_experience_factor = 0.5
		cv_medium_plane_maritime_patrol_airframe_1 = { owner = "USA" amount = 10 version_name = "S-3B Viking" }
		start_experience_factor = 0.5
	}
	"USS Dwight D. Eisenhower (CVN-69)" = {
		cv_medium_plane_fighter_airframe_1 = { owner = "USA" amount = 10 version_name = "F-14B Tomcat" } #VF-32
		start_experience_factor = 0.5
		cv_medium_plane_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18C Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18C Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_cas_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18D Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_scout_airframe_2 = { owner = "USA" amount = 10 version_name = "E-2 Hawkeye" }
		start_experience_factor = 0.5
		cv_medium_plane_maritime_patrol_airframe_1 = { owner = "USA" amount = 10 version_name = "S-3B Viking" }
		start_experience_factor = 0.5
	}
	"USS Theodore Roosevelt (CVN-71)" = {
		cv_medium_plane_fighter_airframe_1 = { owner = "USA" amount = 10 version_name = "F-14A Tomcat" } #VF-41
		start_experience_factor = 0.5
		cv_medium_plane_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18C Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18C Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_cas_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18D Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_scout_airframe_2 = { owner = "USA" amount = 10 version_name = "E-2 Hawkeye" }
		start_experience_factor = 0.5
		cv_medium_plane_maritime_patrol_airframe_1 = { owner = "USA" amount = 10 version_name = "S-3B Viking" }
		start_experience_factor = 0.5
	}
	"USS Abraham Lincoln (CVN-72)" = {
		cv_medium_plane_fighter_airframe_1 = { owner = "USA" amount = 10 version_name = "F-14B Tomcat" } #VF-101
		start_experience_factor = 0.5
		cv_medium_plane_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18C Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18C Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_cas_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18D Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_scout_airframe_2 = { owner = "USA" amount = 10 version_name = "E-2 Hawkeye" }
		start_experience_factor = 0.5
		cv_medium_plane_maritime_patrol_airframe_1 = { owner = "USA" amount = 10 version_name = "S-3B Viking" }
		start_experience_factor = 0.5
	}
	"USS George Washington (CVN-73)" = {
		cv_medium_plane_fighter_airframe_1 = { owner = "USA" amount = 10 version_name = "F-14B Tomcat" } #VF-102
		start_experience_factor = 0.5
		cv_medium_plane_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18C Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18C Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_cas_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18D Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_scout_airframe_2 = { owner = "USA" amount = 10 version_name = "E-2 Hawkeye" }
		start_experience_factor = 0.5
		cv_medium_plane_maritime_patrol_airframe_1 = { owner = "USA" amount = 10 version_name = "S-3B Viking" }
		start_experience_factor = 0.5
	}
	"USS John C. Stennis (CVN-74)" = {
		cv_medium_plane_fighter_airframe_1 = { owner = "USA" amount = 10 version_name = "F-14B Tomcat" } #VF-103
		start_experience_factor = 0.5
		cv_medium_plane_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18C Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18C Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_cas_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18D Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_scout_airframe_2 = { owner = "USA" amount = 10 version_name = "E-2 Hawkeye" }
		start_experience_factor = 0.5
		cv_medium_plane_maritime_patrol_airframe_1 = { owner = "USA" amount = 10 version_name = "S-3B Viking" }
		start_experience_factor = 0.5
	}
	"USS Harry S. Truman (CVN-75)" = {
		cv_medium_plane_fighter_airframe_1 = { owner = "USA" amount = 10 version_name = "F-14D Tomcat" } #VF-143
		start_experience_factor = 0.5
		cv_medium_plane_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18C Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18C Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_cas_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18D Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_scout_airframe_2 = { owner = "USA" amount = 10 version_name = "E-2 Hawkeye" }
		start_experience_factor = 0.5
		cv_medium_plane_maritime_patrol_airframe_1 = { owner = "USA" amount = 10 version_name = "S-3B Viking" }
		start_experience_factor = 0.5
	}
	811 = {
		##### US NAVY squadrons out of CVW West Coast #####
		cv_medium_plane_fighter_airframe_1 = { owner = "USA" amount = 10 version_name = "F-14A Tomcat" } #VF-154
		start_experience_factor = 0.5
		cv_medium_plane_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18C Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18C Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18C Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18C Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_scout_airframe_2 = { owner = "USA" amount = 10 version_name = "E-2 Hawkeye" }
		start_experience_factor = 0.5
	}
	773 = {
		##### US NAVY squadrons out of CVW East Coast #####
		cv_medium_plane_fighter_airframe_1 = { owner = "USA" amount = 10 version_name = "F-14A Tomcat" } #VF-211
		start_experience_factor = 0.5
		cv_medium_plane_fighter_airframe_1 = { owner = "USA" amount = 10 version_name = "F-14D Tomcat" } #VF-213
		start_experience_factor = 0.5
		cv_medium_plane_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18C Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18C Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18C Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_airframe_2 = { owner = "USA" amount = 10 version_name = "F/A-18C Hornet" }
		start_experience_factor = 0.5
		cv_medium_plane_scout_airframe_2 = { owner = "USA" amount = 10 version_name = "E-2 Hawkeye" }
		start_experience_factor = 0.5
		cv_medium_plane_scout_airframe_2 = { owner = "USA" amount = 10 version_name = "E-2 Hawkeye" }
		start_experience_factor = 0.5
		cv_medium_plane_maritime_patrol_airframe_1 = { owner = "USA" amount = 10 version_name = "S-3B Viking" }
		start_experience_factor = 0.5
	}
	806 = {
		#AV-8B Squadrons YUMA#
		cv_small_plane_strike_airframe_2 = { owner = "USA" amount = 10 version_name = "AV-8B Harrier II" }
		start_experience_factor = 0.5
		cv_small_plane_strike_airframe_2 = { owner = "USA" amount = 10 version_name = "AV-8B Harrier II" }
		start_experience_factor = 0.5
		cv_small_plane_strike_airframe_2 = { owner = "USA" amount = 10 version_name = "AV-8B Harrier II" }
		start_experience_factor = 0.5
		cv_small_plane_strike_airframe_2 = { owner = "USA" amount = 10 version_name = "AV-8B Harrier II" }
		start_experience_factor = 0.5
		cv_small_plane_strike_airframe_2 = { owner = "USA" amount = 10 version_name = "AV-8B Harrier II" }
		start_experience_factor = 0.5
		cv_small_plane_strike_airframe_2 = { owner = "USA" amount = 10 version_name = "AV-8B Harrier II" }
		start_experience_factor = 0.5
		cv_small_plane_strike_airframe_2 = { owner = "USA" amount = 10 version_name = "AV-8B Harrier II" }
		start_experience_factor = 0.5
	}
}