﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "C-130 Hercules"
		amount = 21
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "Fokker F27 Friendship"
		amount = 13
		producer = HOL
	}
	add_equipment_to_stockpile = {
		type = cv_medium_plane_scout_airframe_2
		variant_name = "E-2 Hawkeye"
		amount = 4
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = small_plane_airframe_2
		amount = 58
		variant_name = "Mirage 2000-5F"
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = small_plane_airframe_1
		amount = 272
		variant_name = "F-5E Tiger II"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = cv_medium_plane_maritime_patrol_airframe_1
		amount = 31
		variant_name = "S-2 Tracker"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_2
		variant_name = "F-CK-1 Ching-kuo"
		amount = 128
		producer = TAI
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "AT-3"
		amount = 58
		producer = TAI
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_2
		amount = 126
		variant_name = "F-16C Blk 40/42"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1			#in place of T-34C as we don't have variant
		amount = 42
		variant_name = "T-38C Talon"
		producer = USA
	}
}