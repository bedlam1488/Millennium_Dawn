﻿division_template = {
	name = "Brigada Acorazada"
	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		armor_Bat = { x = 1 y = 0 }
		SP_Arty_Bat = { x = 1 y = 1 }
	}

	support = {
		L_Engi_Comp = { x = 0 y = 0 }
	}
}
division_template = {
	name = "Regimiento de Infantería Mecanizada"
	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Regimiento de Infantería de Montaña"
	regiments = {
		Mot_Inf_Bat = { x = 0 y = 0 }
		Mot_Inf_Bat = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Brigada de Operaciones Especiales"
	regiments = {
		Special_Forces = { x = 0 y = 0 }
		Special_Forces = { x = 0 y = 1 }
		Special_Forces = { x = 0 y = 2 }
	}

	priority = 2
}
division_template = {
	name = "Brigada Motorizada"
	regiments = {
		Mot_Inf_Bat = { x = 0 y = 0 }
		Mot_Inf_Bat = { x = 0 y = 1 }
		Arty_Bat = { x = 1 y = 0 }
		L_Inf_Bat = { x = 2 y = 0 }
	}
}

units = {
	division = {
		name = "Brigada Acorazada Nº3 'La Concepción'"
		location = 12994		#Antofagasta
		division_template = "Brigada Acorazada"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "Brigada de Operaciones Especiales 'Lautaro'"
		location = 650		#Peldehue
		division_template = "Brigada de Operaciones Especiales"
		start_experience_factor = 0.7
		start_equipment_factor = 0.01
	}
	division = {
		name = "Regimiento de Infantería Nº 1 'Buin'"
		location = 650		#Santiago
		division_template = "Regimiento de Infantería Mecanizada"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "Regimiento Reforzado Nº3 'Yungay'"
		location = 8222		#Los Andes
		division_template = "Regimiento de Infantería de Montaña"
		start_experience_factor = 0.6
		start_equipment_factor = 0.01
	}
	division = {
		name = "Regimiento Reforzado Nº14 'Aysén'"
		location = 2161		#Coyhaique
		division_template = "Brigada Motorizada"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "Brigada Acorazada Nº4 'Chorrillos'"
		location = 5201		#Punta Arenas
		division_template = "Brigada Acorazada"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "Regimiento Reforzado Nº11 'Caupolicán'"
		location = 5209		#Porvenir
		division_template = "Brigada Motorizada"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "Brigada Acorazada Nº 1 'Coraceros'"
		location = 5135		#Arica
		division_template = "Brigada Acorazada"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons2 #FAMAE
		amount = 5100
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons4 #M4 Carbine
		amount = 1300
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = command_control_equipment2
		amount = 650
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_2 #Spike LR
		amount = 90
		producer = ISR
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_2 #Spike MR
		amount = 230
		producer = ISR
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_1
		amount = 160
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_2 #Humvee
		amount = 400
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "AMX-30B2"
		amount = 51
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "Leopard 1A3"
		amount = 200
		producer = GER
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_1
		amount = 50
		producer = BRA
		variant_name = "EE-9 Cascavel"
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "MOWAG Piranha"
		amount = 20
		producer = SWI
	}
	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "M113"
		amount = 355
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = artillery_0 #M102
		amount = 66
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = artillery_0 #OTO-Melara Mod 56
		amount = 54
		producer = ITA
	}
	add_equipment_to_stockpile = {
		type = artillery_1 #Soltam M-71
		amount = 12
		producer = ISR
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "Mk F3"
		amount = 12
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = transport_plane2 #CASA CN-235
		amount = 5
		producer = IND
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Bell 206
		amount = 2
		producer = USA
		#version_name = "Bell 206"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Eurocopter AS332 Super Puma
		amount = 3
		producer = FRA
		#version_name = "Eurocopter AS332 Super Puma"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Aérospatiale SA-316 Alouette III
		amount = 27
		producer = FRA
		#version_name = "Aérospatiale SA-316 Alouette III"
	}
	add_equipment_to_stockpile = {
		type = naval_plane1 #P-3 Orion
		amount = 4
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter2 #Eurocopter AS532 Cougar
		amount = 6
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = transport_plane2 #C-212 Aviocar
		amount = 7
		producer = SPR
	}
	add_equipment_to_stockpile = {
		type = attack_helicopter1 #MBB Bo 105
		amount = 16
		producer = SPR
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Bell 206
		amount = 6
		producer = USA
		#version_name = "Bell 206"
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter1 #Pilatus PC-7
		amount = 10
		producer = SWI
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_1
		amount = 30
		producer = ENG
		variant_name = "FV101 Scorpion"
	}
	add_equipment_to_stockpile = {
		type = APC_1 #Mowag Roland
		amount = 30
		producer = SWI
	}
	add_equipment_to_stockpile = {
		type = artillery_1 #KH-178
		amount = 16
		producer = KOR
	}
	add_equipment_to_stockpile = {
		type = artillery_1 #G5
		amount = 28
		producer = SAF
	}
}