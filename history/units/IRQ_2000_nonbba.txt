﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = L_Strike_fighter2		#L-39
		amount = 81
		producer = CZE
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter1	#MiG-21
		amount = 30
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = AS_Fighter1 #MiG-23
		amount = 30
		producer = SOV
	}
}