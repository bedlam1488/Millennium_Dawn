﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = MR_Fighter1 #F-5 Freedom Fighter
		amount = 78
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = AS_Fighter3 #F-15C Eagle
		amount = 94
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = Strike_fighter2 #Panavia Tornado IDS
		amount = 86
		producer = GER
	}
	add_equipment_to_stockpile = {
		type = Strike_fighter3 #F-15S Strike Eagle
		amount = 70
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = AS_Fighter2 #Panavia Tornado AVD
		amount = 24
		producer = ITA
	}
	add_equipment_to_stockpile = {
		type = transport_plane1 #C-130J Super Hercules
		amount = 52
		producer = USA
	}
}