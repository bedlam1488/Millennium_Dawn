﻿#Author: Borgers

division_template = {
	name = "Gebirgsbrigade"

	division_names_group = GER_Mnt_02

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		Mech_Inf_Bat = { x = 0 y = 2 }
		SP_Arty_Bat = { x = 0 y = 3 }
	}

	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 2 }
	}
}

division_template = {
	name = "Jägerbrigade"

	division_names_group = GER_Mec_03

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
		SP_Arty_Bat = { x = 0 y = 3 }
	}

	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 2 }
	}
}
division_template = {
	name = "Panzerbrigade"

	division_names_group = GER_Arm_02

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		armor_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
		SP_Arty_Bat = { x = 0 y = 3 }
	}

	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
		SP_AA_Battery = { x = 0 y = 2 }
		combat_service_support_company = { x = 0 y = 3 }
	}
}

division_template = { #transfer to focus
	name = "Panzergrenadierbrigade"

	division_names_group = GER_MEC_02

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
		SP_Arty_Bat = { x = 0 y = 3 }

	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
		SP_AA_Battery = { x = 0 y = 2 }
		combat_service_support_company = { x = 0 y = 3 }
	}
}

#division_template = {
#	name = "Panzergrenadierdivision"
#
#	division_names_group = GER_MEC_01
#
#	regiments = {
#		Arm_Inf_Bat = { x = 0 y = 0 }
#		Arm_Inf_Bat = { x = 0 y = 1 }
#		armor_Bat = { x = 0 y = 2 }
#		SP_Arty_Bat = { x = 0 y = 3 }
#		Arm_Inf_Bat = { x = 1 y = 0 }
#		Arm_Inf_Bat = { x = 1 y = 1 }
#		armor_Bat = { x = 1 y = 2 }
#		SP_Arty_Bat = { x = 1 y = 3 }
#		armor_Bat = { x = 2 y = 0 }
#		armor_Bat = { x = 2 y = 1 }
#		Arm_Inf_Bat = { x = 2 y = 2 }
#		SP_Arty_Bat = { x = 2 y = 3 }
#
#	}
#	support = {
#		H_Engi_Comp = { x = 0 y = 0 }
#		armor_Recce_Comp = { x = 0 y = 1 }
#		SP_AA_Battery = { x = 0 y = 2 }
#		combat_service_support_company = { x = 0 y = 3 }
#	}
#}

#division_template = {
#	name = "Panzerdivision"
#
#	division_names_group = GER_Arm_01
#
#	regiments = {
#		armor_Bat = { x = 0 y = 0 }
#		armor_Bat = { x = 0 y = 1 }
#		Arm_Inf_Bat = { x = 0 y = 2 }
#		SP_Arty_Bat = { x = 0 y = 3 }
#		armor_Bat = { x = 1 y = 0 }
#		armor_Bat = { x = 1 y = 1 }
#		Arm_Inf_Bat = { x = 1 y = 2 }
#		SP_Arty_Bat = { x = 1 y = 3 }
#		armor_Bat = { x = 2 y = 0 }
#		Arm_Inf_Bat = { x = 2 y = 1 }
#		Arm_Inf_Bat = { x = 2 y = 2 }
#		SP_Arty_Bat = { x = 2 y = 3 }
#
#	}
#	support = {
#		H_Engi_Comp = { x = 0 y = 0 }
#		armor_Recce_Comp = { x = 0 y = 1 }
#		SP_AA_Battery = { x = 0 y = 2 }
#		combat_service_support_company = { x = 0 y = 3 }
#	}
#}

division_template = {
	name = "Panzeraufklärungsbatallion"

	division_names_group = GER_REC_01

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
	}

	support = {
		armor_Recce_Comp = { x = 0 y = 0 }
		armor_Comp = { x = 0 y = 1 }
	}
}

division_template = { #need to integrate Wiesel 1
	name = "Luftlandebrigade"

	division_names_group = GER_PAR_03

	regiments = {
		Mot_Air_Inf_Bat = { x = 0 y = 0 }
		Mot_Air_Inf_Bat = { x = 0 y = 1 }
	}

	support = {
		L_Engi_Comp = { x = 0 y = 0 }
		L_Recce_Comp = { x = 0 y = 1 }
		helicopter_combat_service_support = { x = 0 y = 2 }
	}
}

division_template = {
	name = "Kommandobataillon"

	division_names_group = GER_SOF_01

	regiments = {
		Special_Forces = { x = 0 y = 0 }
		Special_Forces = { x = 0 y = 1 }
	}

	Support = {
		L_Recce_Comp = { x = 0 y = 0 }
		helicopter_combat_service_support = { x = 0 y = 1 }
	}

	priority = 2
}

division_template = {
	name = "Marinekommandobattalion"

	division_names_group = GER_MAR_01

	regiments = {
		L_Marine_Bat = { x = 0 y = 0 }
		L_Marine_Bat = { x = 0 y = 1 }
	}

	Support = {
		helicopter_combat_service_support = { x = 0 y = 0 }
	}

	priority = 2
}

division_template = {
	name = "Luftbewegliche Brigade"

	division_names_group = GER_PAR_05

	regiments = {
		L_Air_assault_Bat = { x = 0 y = 0 }
		L_Air_assault_Bat = { x = 0 y = 1 }
		attack_helo_bat = { x = 0 y = 2 }
	}
	support = {
		L_Recce_Comp = { x = 0 y = 0 }
		helicopter_combat_service_support = { x = 0 y = 1 }
	}

}

division_template = {
	name = "Heeresfliegerbrigade"

	regiments = {
		attack_helo_bat = { x = 0 y = 0 }
		attack_helo_bat = { x = 0 y = 1 }
		attack_helo_bat = { x = 0 y = 2 }
		L_Air_assault_Bat = { x = 1 y = 0 }
		L_Air_assault_Bat = { x = 1 y = 1 }
		L_Air_assault_Bat = { x = 1 y = 2 }
	}
	support = {
		L_Recce_Comp = { x = 0 y = 0 }
		helicopter_combat_service_support = { x = 0 y = 1 }
	}

}

division_template = {
	name = "Deutsch-Französische Brigade"

	division_names_group = GER_DF_BG

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		SP_Arty_Bat = { x = 0 y = 2 }
	}

	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		mech_Recce_Comp = { x = 0 y = 1 }
		helicopter_combat_service_support = { x = 0 y = 2 }
	}
}

units = {
	division = {
		division_name = {
				is_name_ordered = yes
				name_order = 21
		}
		officer = {
			name = "Wolf-Dieter Langheld"

		portraits = {
			army = {
				small = "gfx/leaders/GER/small/Wolf-Dieter_Langheld_small.dds"
				large = "gfx/leaders/GER/Wolf-Dieter_Langheld.dds"
			}
		}
	}
		location = 3355
		division_template = "Panzerbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
		force_equipment_variants = {
			apc_hull_1 = { owner = "GER" version_name = "Tpz-1 Fuchs" }
			ifv_hull_1 = { owner = "GER" version_name = "Marder 1A3" }
			spart_hull_1 = { owner = "GER" version_name = "PZH 2000" }
			mbt_hull_1 = { owner = "GER" version_name = "Leopard 2A4" }
			spaa_hull_0 = { owner = "GER" version_name = "FlakPz Gepard 1A2" }
		}
	}

	division = {
		division_name = {
				is_name_ordered = yes
				name_order = 7
		}
		officer = {
			name = "Manfred Engelhardt"

		portraits = {
			army = {
				small = "gfx/leaders/GER/small/Manfred_Engelhardt_small.dds"
				large = "gfx/leaders/GER/Manfred_Engelhardt.dds"
			}
		}
	}
		location = 9347
		division_template = "Panzergrenadierbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
		force_equipment_variants = {
			apc_hull_1 = { owner = "GER" version_name = "Tpz-1 Fuchs" }
			ifv_hull_1 = { owner = "GER" version_name = "Marder 1A3" }
			spart_hull_1 = { owner = "GER" version_name = "PZH 2000" }
			mbt_hull_1 = { owner = "GER" version_name = "Leopard 2A4" }
			spaa_hull_0 = { owner = "GER" version_name = "FlakPz Gepard 1A2" }
		}
	}

	division = {
		division_name = {
				is_name_ordered = yes
				name_order = 39
		}
		officer = {
			name = "Günter Weiler"

		portraits = {
			army = {
				small = "gfx/leaders/GER/small/Günter_Weiler_small.dds"
				large = "gfx/leaders/GER/Günter_Weiler.dds"
			}
		}
	}
		location = 3561
		division_template = "Panzerbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
		force_equipment_variants = {
			apc_hull_1 = { owner = "GER" version_name = "Tpz-1 Fuchs" }
			ifv_hull_1 = { owner = "GER" version_name = "Marder 1A3" }
			spart_hull_1 = { owner = "GER" version_name = "PZH 2000" }
			mbt_hull_1 = { owner = "GER" version_name = "Leopard 2A4" }
			spaa_hull_0 = { owner = "GER" version_name = "FlakPz Gepard 1A2" }
		}
	}

	division = {
		division_name = {
				is_name_ordered = yes
				name_order = 34
		}
		officer = {
			name = "Erhard Drews"

		portraits = {
			army = {
				small = "gfx/leaders/GER/small/Erhard_Drews_small.dds"
				large = "gfx/leaders/GER/Erhard_Drews.dds"
			}
		}
	}
		location = 3423
		division_template = "Panzerbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
		force_equipment_variants = {
			apc_hull_1 = { owner = "GER" version_name = "Tpz-1 Fuchs" }
			ifv_hull_1 = { owner = "GER" version_name = "Marder 1A3" }
			spart_hull_1 = { owner = "GER" version_name = "PZH 2000" }
			mbt_hull_1 = { owner = "GER" version_name = "Leopard 2A4" }
			spaa_hull_0 = { owner = "GER" version_name = "FlakPz Gepard 1A2" }
		}
	}

	division = {
		division_name = {
				is_name_ordered = yes
				name_order = 38
		}
		officer = {
			name = "Alois Bach"

		portraits = {
			army = {
				small = "gfx/leaders/GER/small/Alois_Bach_small.dds"
				large = "gfx/leaders/GER/Alois_Bach.dds"
			}
		}
	}
		location = 3561
		division_template = "Panzergrenadierbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
		force_equipment_variants = {
			apc_hull_1 = { owner = "GER" version_name = "Tpz-1 Fuchs" }
			ifv_hull_1 = { owner = "GER" version_name = "Marder 1A3" }
			spart_hull_1 = { owner = "GER" version_name = "PZH 2000" }
			mbt_hull_1 = { owner = "GER" version_name = "Leopard 2A4" }
			spaa_hull_0 = { owner = "GER" version_name = "FlakPz Gepard 1A2" }
		}
	}

	division = {
		division_name = {
				is_name_ordered = yes
				name_order = 40
		}
		officer = {
			name = "Volker Wieker"

		portraits = {
			army = {
				small = "gfx/leaders/GER/small/Volker_Wieker_small.dds"
				large = "gfx/leaders/GER/Volker_Wieker.dds"
			}
		}
	}
		location = 321
		division_template = "Panzergrenadierbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
		force_equipment_variants = {
			apc_hull_1 = { owner = "GER" version_name = "Tpz-1 Fuchs" }
			ifv_hull_1 = { owner = "GER" version_name = "Marder 1A3" }
			spart_hull_1 = { owner = "GER" version_name = "PZH 2000" }
			mbt_hull_1 = { owner = "GER" version_name = "Leopard 2A4" }
			spaa_hull_0 = { owner = "GER" version_name = "FlakPz Gepard 1A2" }
		}
	}

	division = {
		division_name = {
				is_name_ordered = yes
				name_order = 19
		}
		officer = {
			name = "Robert Bergmann"

		portraits = {
			army = {
				small = "gfx/leaders/GER/small/Robert_Bergmann_small.dds"
				large = "gfx/leaders/GER/Robert_Bergmann.dds"
			}
		}
	}
		location = 6535
		division_template = "Panzergrenadierbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
		force_equipment_variants = {
			apc_hull_1 = { owner = "GER" version_name = "Tpz-1 Fuchs" }
			ifv_hull_1 = { owner = "GER" version_name = "Marder 1A3" }
			spart_hull_1 = { owner = "GER" version_name = "PZH 2000" }
			mbt_hull_1 = { owner = "GER" version_name = "Leopard 2A4" }
			spaa_hull_0 = { owner = "GER" version_name = "FlakPz Gepard 1A2" }
		}
	}

	division = {
		division_name = {
				is_name_ordered = yes
				name_order = 1
		}
		officer = {
			name = "Dieter Skodowski"

		portraits = {
			army = {
				small = "gfx/leaders/GER/small/Dieter_Skodowski_small.dds"
				large = "gfx/leaders/GER/Dieter_Skodowski.dds"
			}
		}
	}
		location = 6377
		division_template = "Panzergrenadierbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
		force_equipment_variants = {
			apc_hull_1 = { owner = "GER" version_name = "Tpz-1 Fuchs" }
			ifv_hull_1 = { owner = "GER" version_name = "Marder 1A3" }
			spart_hull_1 = { owner = "GER" version_name = "PZH 2000" }
			mbt_hull_1 = { owner = "GER" version_name = "Leopard 2A4" }
			spaa_hull_0 = { owner = "GER" version_name = "FlakPz Gepard 1A2" }
		}
	}

	division = {
		division_name = {
				is_name_ordered = yes
				name_order = 9
		}
		officer = {
			name = "Wolf-Joachim Clauß"

		portraits = {
			army = {
				small = "gfx/leaders/GER/small/Wolf-Joachim_Clauß_small.dds"
				large = "gfx/leaders/GER/Wolf-Joachim_Clauß.dds"
			}
		}
	}
		location = 6263
		division_template = "Panzerbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
		force_equipment_variants = {
			apc_hull_1 = { owner = "GER" version_name = "Tpz-1 Fuchs" }
			ifv_hull_1 = { owner = "GER" version_name = "Marder 1A3" }
			spart_hull_1 = { owner = "GER" version_name = "PZH 2000" }
			mbt_hull_1 = { owner = "GER" version_name = "Leopard 2A4" }
			spaa_hull_0 = { owner = "GER" version_name = "FlakPz Gepard 1A2" }
		}
	}

	division = {
		division_name = {
				is_name_ordered = yes
				name_order = 42
		}
		officer = {
			name = "Roland Kather"

		portraits = {
			army = {
				small = "gfx/leaders/GER/small/Roland_Kather_small.dds"
				large = "gfx/leaders/GER/Roland_Kather.dds"
			}
		}
	}
		location = 3499
		division_template = "Panzerbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
		force_equipment_variants = {
			apc_hull_1 = { owner = "GER" version_name = "Tpz-1 Fuchs" }
			ifv_hull_1 = { owner = "GER" version_name = "Marder 1A3" }
			spart_hull_1 = { owner = "GER" version_name = "PZH 2000" }
			mbt_hull_1 = { owner = "GER" version_name = "Leopard 2A4" }
			spaa_hull_0 = { owner = "GER" version_name = "FlakPz Gepard 1A2" }
		}
	}

	division = {
		division_name = {
				is_name_ordered = yes
				name_order = 18
		}
		officer = {
			name = "Bernd Kiesheyer"

		portraits = {
			army = {
				small = "gfx/leaders/GER/small/Bernd_Kiesheyer_small.dds"
				large = "gfx/leaders/GER/Bernd_Kiesheyer.dds"
			}
		}
	}
		location = 11331
		division_template = "Panzerbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
		force_equipment_variants = {
			apc_hull_1 = { owner = "GER" version_name = "Tpz-1 Fuchs" }
			ifv_hull_1 = { owner = "GER" version_name = "Marder 1A3" }
			spart_hull_1 = { owner = "GER" version_name = "PZH 2000" }
			mbt_hull_1 = { owner = "GER" version_name = "Leopard 2A4" }
			spaa_hull_0 = { owner = "GER" version_name = "FlakPz Gepard 1A2" }
		}
	}

	division = {
		division_name = {
				is_name_ordered = yes
				name_order = 26
		}
		officer = {
			name = "Manfred Schlenker"

		portraits = {
			army = {
				small = "gfx/leaders/GER/small/Manfred_Schlenker_small.dds"
				large = "gfx/leaders/GER/Manfred_Schlenker.dds"
			}
		}
	}
		officer = {
			name = "Hans-Werner Fritz"

		portraits = {
			army = {
				small = "gfx/leaders/GER/small/Portrait_Hans_Werner_Fritz_small.dds"
				large = "gfx/leaders/GER/Portrait_Hans_Werner_Fritz.dds"
			}
		}
	}
		location = 11531
		division_template = "Luftlandebrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
		force_equipment_variants = {
			util_vehicle_1 = { owner = "GER" }
			infantry_weapons1 = { owner = "GER" }
			transport_helicopter1 = { owner = "GER" }
		}
	}

	division = {
		division_name = {
				is_name_ordered = yes
				name_order = 41
		}
		officer = {
			name = "Hans-Lothar Domröse"

		portraits = {
			army = {
				small = "gfx/leaders/GER/small/Hans_Lothar_Domrose_small.dds"
				large = "gfx/leaders/GER/Hans_Lothar_Domrose.dds"
			}
		}
	}
		location = 349
		division_template = "Panzergrenadierbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
		force_equipment_variants = {
			apc_hull_1 = { owner = "GER" version_name = "Tpz-1 Fuchs" }
			ifv_hull_1 = { owner = "GER" version_name = "Marder 1A3" }
			spart_hull_1 = { owner = "GER" version_name = "PZH 2000" }
			mbt_hull_1 = { owner = "GER" version_name = "Leopard 2A4" }
			spaa_hull_0 = { owner = "GER" version_name = "FlakPz Gepard 1A2" }
		}
	}

	division = {
		division_name = {
				is_name_ordered = yes
				name_order = 12
		}
		officer = {
			name = "Fritz von Korff"

		portraits = {
			army = {
				small = "gfx/leaders/GER/small/Fritz_von_Korff_small.dds"
				large = "gfx/leaders/GER/Fritz_von_Korff.dds"
			}
		}
	}
		location = 11544
		division_template = "Panzerbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
		force_equipment_variants = {
			apc_hull_1 = { owner = "GER" version_name = "Tpz-1 Fuchs" }
			ifv_hull_1 = { owner = "GER" version_name = "Marder 1A3" }
			spart_hull_1 = { owner = "GER" version_name = "PZH 2000" }
			mbt_hull_1 = { owner = "GER" version_name = "Leopard 2A4" }
			spaa_hull_0 = { owner = "GER" version_name = "FlakPz Gepard 1A2" }
		}
	}

	division = {
		division_name = {
				is_name_ordered = yes
				name_order = 14
		}
		officer = {
			name = "Gertmann Sude"

		portraits = {
			army = {
				small = "gfx/leaders/GER/small/Gertmann_Sude_small.dds"
				large = "gfx/leaders/GER/Gertmann_Sude.dds"
			}
		}
	}
		location = 6488
		division_template = "Panzerbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
		force_equipment_variants = {
			apc_hull_1 = { owner = "GER" version_name = "Tpz-1 Fuchs" }
			ifv_hull_1 = { owner = "GER" version_name = "Marder 1A3" }
			spart_hull_1 = { owner = "GER" version_name = "PZH 2000" }
			mbt_hull_1 = { owner = "GER" version_name = "Leopard 2A4" }
			spaa_hull_0 = { owner = "GER" version_name = "FlakPz Gepard 1A2" }
		}
	}

	division = {
		division_name = {
				is_name_ordered = yes
				name_order = 36
		}
		officer = {
			name = "Werner Kullack"

		portraits = {
			army = {
				small = "gfx/leaders/GER/small/Werner_Kullack_small.dds"
				large = "gfx/leaders/GER/Werner_Kullack.dds"
			}
		}
	}
		location = 6555
		division_template = "Panzerbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
		force_equipment_variants = {
			apc_hull_1 = { owner = "GER" version_name = "Tpz-1 Fuchs" }
			ifv_hull_1 = { owner = "GER" version_name = "Marder 1A3" }
			spart_hull_1 = { owner = "GER" version_name = "PZH 2000" }
			mbt_hull_1 = { owner = "GER" version_name = "Leopard 2A4" }
			spaa_hull_0 = { owner = "GER" version_name = "FlakPz Gepard 1A2" }
		}
	}

	division = {
		division_name = {
				is_name_ordered = yes
				name_order = 23
		}
		officer = {
			name = "Johann Berger"

		portraits = {
			army = {
				small = "gfx/leaders/GER/small/Johann_Berger_small.dds"
				large = "gfx/leaders/GER/Johann_Berger.dds"
			}
		}
	}
		location = 707
		division_template = "Gebirgsbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
		force_equipment_variants = {
			apc_hull_1 = { owner = "GER" version_name = "Tpz-1 Fuchs" }
			ifv_hull_1 = { owner = "GER" version_name = "Marder 1A3" }
			spart_hull_1 = { owner = "GER" version_name = "PZH 2000" }
			spaa_hull_0 = { owner = "GER" version_name = "FlakPz Gepard 1A2" }
		}
	}

	division = {
		division_name = {
				is_name_ordered = yes
				name_order = 30
		}
		officer = {
			name = "Klaus Feldmann"

		portraits = {
			army = {
				small = "gfx/leaders/GER/small/Klaus_Feldmann_small.dds"
				large = "gfx/leaders/GER/Klaus_Feldmann.dds"
			}
		}
	}
		location = 11499
		division_template = "Panzergrenadierbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
		force_equipment_variants = {
			apc_hull_1 = { owner = "GER" version_name = "Tpz-1 Fuchs" }
			ifv_hull_1 = { owner = "GER" version_name = "Marder 1A3" }
			spart_hull_1 = { owner = "GER" version_name = "PZH 2000" }
			mbt_hull_1 = { owner = "GER" version_name = "Leopard 2A4" }
			spaa_hull_0 = { owner = "GER" version_name = "FlakPz Gepard 1A2" }
		}
	}

	division = {
		division_name = {
				is_name_ordered = yes
				name_order = 37
		}
		officer = {
			name = "Rainer Glatz"

		portraits = {
			army = {
				small = "gfx/leaders/GER/small/Rainer_Glatz_small.dds"
				large = "gfx/leaders/GER/Rainer_Glatz.dds"
			}
		}
	}
		location = 11481 #Frankenburg, Saxony
		division_template = "Jägerbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
		force_equipment_variants = {
			apc_hull_1 = { owner = "GER" version_name = "Tpz-1 Fuchs" }
			ifv_hull_1 = { owner = "GER" version_name = "Marder 1A3" }
			spart_hull_1 = { owner = "GER" version_name = "PZH 2000" }
			spaa_hull_0 = { owner = "GER" version_name = "FlakPz Gepard 1A2" }
		}
	}

	division = {
		division_name = {
				is_name_ordered = yes
				name_order = 31
		}
		officer = {
			name = "Carl-Hubertus von Butler"

		portraits = {
			army = {
				small = "gfx/leaders/GER/small/Carl-Hubertus_von_Butler_small.dds"
				large = "gfx/leaders/GER/Carl-Hubertus_von_Butler.dds"
			}
		}
	}
		location = 6325
		division_template = "Luftlandebrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
		force_equipment_variants = {
			util_vehicle_1 = { owner = "GER" }
			infantry_weapons1 = { owner = "GER" }
			transport_helicopter1 = { owner = "GER" }
		}
	}

	division = {
		division_name = {
				is_name_ordered = yes
				name_order = 1
		}
		officer = {
			name = "Georg Nachtsheim"

		portraits = {
			army = {
				small = "gfx/leaders/GER/small/Georg_Nachtsheim_small.dds"
				large = "gfx/leaders/GER/Georg_Nachtsheim.dds"
			}
		}
	}
		location = 11640
		division_template = "Deutsch-Französische Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
		force_equipment_variants = {
			apc_hull_1 = { owner = "GER" version_name = "Tpz-1 Fuchs" }
			ifv_hull_1 = { owner = "GER" version_name = "Marder 1A3" }
			spart_hull_1 = { owner = "GER" version_name = "PZH 2000" }
			spaa_hull_0 = { owner = "GER" version_name = "FlakPz Gepard 1A2" }
		}
	}

	division = {
		division_name = {
				is_name_ordered = yes
				name_order = 1
		}
		officer = {
			name = "Reinhard Günzel"

		portraits = {
			army = {
				small = "gfx/leaders/GER/small/Reinhard_Günzel_small.dds"
				large = "gfx/leaders/GER/Reinhard_Günzel.dds"
			}
		}
	}
		location = 11486 #Calw, #Baden-Württemberg
		division_template = "Kommandobataillon"
		start_experience_factor = 0.9
		start_equipment_factor = 0.01
	}

	division = {
		division_name = {
				is_name_ordered = yes
				name_order = 3
		}
		officer = {
			name = "Jürgen Karl Uchtmann"

		portraits = {
			army = {
				small = "gfx/leaders/GER/small/Jürgen_Karl_Uchtmann_small.dds"
				large = "gfx/leaders/GER/Jürgen_Karl_Uchtmann.dds"
			}
		}
	}
		location = 3271
		division_template = "Panzeraufklärungsbatallion"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
		force_equipment_variants = {
			apc_hull_1 = { owner = "GER" version_name = "Tpz-1 Fuchs" }
			ifv_hull_1 = { owner = "GER" version_name = "Marder 1A3" }
			spart_hull_1 = { owner = "GER" version_name = "PZH 2000" }
			mbt_hull_1 = { owner = "GER" version_name = "Leopard 2A4" }
			spaa_hull_0 = { owner = "GER" version_name = "FlakPz Gepard 1A2" }
		}
	}

	division = {
		division_name = {
				is_name_ordered = yes
				name_order = 5
		}
		officer = {
			name = "Michael Westermann"

	#	portraits = {
	#		army = {
	#			small = "gfx/leaders/GER/small/Jürgen_Karl_Uchtmann_small.dds"
	#			large = "gfx/leaders/GER/Jürgen_Karl_Uchtmann.dds"
	#		}
	#	}
	}
		location = 564
		division_template = "Panzeraufklärungsbatallion"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
		force_equipment_variants = {
			apc_hull_1 = { owner = "GER" version_name = "Tpz-1 Fuchs" }
			ifv_hull_1 = { owner = "GER" version_name = "Marder 1A3" }
			spart_hull_1 = { owner = "GER" version_name = "PZH 2000" }
			mbt_hull_1 = { owner = "GER" version_name = "Leopard 2A4" }
			spaa_hull_0 = { owner = "GER" version_name = "FlakPz Gepard 1A2" }
		}
	}

	division = {
		division_name = {
				is_name_ordered = yes
				name_order = 6
		}
		officer = {
			name = "Maeker"

		portraits = {
			army = {
				small = "gfx/leaders/GER/small/Maeker_small.dds"
				large = "gfx/leaders/GER/Maeker.dds"
			}
		}
	}
		location = 11331
		division_template = "Panzeraufklärungsbatallion"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
		force_equipment_variants = {
			apc_hull_1 = { owner = "GER" version_name = "Tpz-1 Fuchs" }
			ifv_hull_1 = { owner = "GER" version_name = "Marder 1A3" }
			spart_hull_1 = { owner = "GER" version_name = "PZH 2000" }
			mbt_hull_1 = { owner = "GER" version_name = "Leopard 2A4" }
			spaa_hull_0 = { owner = "GER" version_name = "FlakPz Gepard 1A2" }
		}
	}

	division = {
		division_name = {
				is_name_ordered = yes
				name_order = 7
		}
		officer = {
			name = "Hans-Jürgen Wöhlermann"

	#	portraits = {
	#		army = {
	#			small = "gfx/leaders/GER/small/Jürgen_Karl_Uchtmann_small.dds"
	#			large = "gfx/leaders/GER/Jürgen_Karl_Uchtmann.dds"
	#		}
	#	}
	}
		location = 6535
		division_template = "Panzeraufklärungsbatallion"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
		force_equipment_variants = {
			apc_hull_1 = { owner = "GER" version_name = "Tpz-1 Fuchs" }
			ifv_hull_1 = { owner = "GER" version_name = "Marder 1A3" }
			spart_hull_1 = { owner = "GER" version_name = "PZH 2000" }
			mbt_hull_1 = { owner = "GER" version_name = "Leopard 2A4" }
			spaa_hull_0 = { owner = "GER" version_name = "FlakPz Gepard 1A2" }
		}
	}

	division = {
		division_name = {
				is_name_ordered = yes
				name_order = 8
		}
		officer = {
			name = "Jung"

	#	portraits = {
	#		army = {
	#			small = "gfx/leaders/GER/small/Uwe_Nerger_small.dds"
	#			large = "gfx/leaders/GER/Uwe_Nerger.dds"
	#		}
	#	}
	}
		location = 9681
		division_template = "Panzeraufklärungsbatallion"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
		force_equipment_variants = {
			apc_hull_1 = { owner = "GER" version_name = "Tpz-1 Fuchs" }
			ifv_hull_1 = { owner = "GER" version_name = "Marder 1A3" }
			spart_hull_1 = { owner = "GER" version_name = "PZH 2000" }
			mbt_hull_1 = { owner = "GER" version_name = "Leopard 2A4" }
			spaa_hull_0 = { owner = "GER" version_name = "FlakPz Gepard 1A2" }
		}
	}

	division = {
		division_name = {
				is_name_ordered = yes
				name_order = 12
		}
		officer = {
			name = "Uwe Nerger"

		portraits = {
			army = {
				small = "gfx/leaders/GER/small/Uwe_Nerger_small.dds"
				large = "gfx/leaders/GER/Uwe_Nerger.dds"
			}
		}
	}
		location = 11417
		division_template = "Panzeraufklärungsbatallion"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
		force_equipment_variants = {
			apc_hull_1 = { owner = "GER" version_name = "Tpz-1 Fuchs" }
			ifv_hull_1 = { owner = "GER" version_name = "Marder 1A3" }
			spart_hull_1 = { owner = "GER" version_name = "PZH 2000" }
			mbt_hull_1 = { owner = "GER" version_name = "Leopard 2A4" }
			spaa_hull_0 = { owner = "GER" version_name = "FlakPz Gepard 1A2" }
		}
	}

	division = {
		division_name = {
				is_name_ordered = yes
				name_order = 13
		}
		officer = {
			name = "Dieter Meyerhoff"

		portraits = {
			army = {
				small = "gfx/leaders/GER/small/Dieter_Meyerhoff_small.dds"
				large = "gfx/leaders/GER/Dieter_Meyerhoff.dds"
			}
		}
	}
		location = 6524
		division_template = "Panzeraufklärungsbatallion"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
		force_equipment_variants = {
			apc_hull_1 = { owner = "GER" version_name = "Tpz-1 Fuchs" }
			ifv_hull_1 = { owner = "GER" version_name = "Marder 1A3" }
			spart_hull_1 = { owner = "GER" version_name = "PZH 2000" }
			mbt_hull_1 = { owner = "GER" version_name = "Leopard 2A4" }
			spaa_hull_0 = { owner = "GER" version_name = "FlakPz Gepard 1A2" }
		}
		}

	division = {
		division_name = {
				is_name_ordered = yes
				name_order = 1
		}
		officer = {
			name = "Reinhard Kammerer"

		portraits = {
			army = {
				small = "gfx/leaders/GER/small/Reinhard_Kammerer_small.dds"
				large = "gfx/leaders/GER/Reinhard_Kammerer.dds"
			}
		}
	}
		location = 6263
		division_template = "Luftbewegliche Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
		force_equipment_variants = {
			util_vehicle_1 = { owner = "GER" }
			infantry_weapons1 = { owner = "GER" }
			transport_helicopter1 = { owner = "GER" }
			attack_helicopter_hull_0 = { owner = "GER" version_name = "MBB Bo 105" }
		}
	}

	division = {
		name = "Heeresfliegerbrigade 3"
		officer = {
			name = "Dieter Budde"

		portraits = {
			army = {
				small = "gfx/leaders/GER/small/Dieter_Budde_small.dds"
				large = "gfx/leaders/GER/Dieter_Budde.dds"
			}
		}
	}
		location = 3423
		division_template = "Heeresfliegerbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
		force_equipment_variants = {
			util_vehicle_1 = { owner = "GER" }
			infantry_weapons1 = { owner = "GER" }
			transport_helicopter1 = { owner = "GER" }
			attack_helicopter_hull_0 = { owner = "GER" version_name = "MBB Bo 105" }
		}
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons3 #Heckler & Koch G36
		amount = 16100
	}
	add_equipment_to_stockpile = {
		type = infantry_weapons2
		amount = 12000
	}
	add_equipment_to_stockpile = {
		type = command_control_equipment2
		amount = 1250
	}
	add_equipment_to_stockpile = {
		type = command_control_equipment3
		amount = 2750
	}
	add_equipment_to_stockpile = {
		type = Anti_tank_1 #Panzerfaust 3
		amount = 1300
	}
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_2 #Milan
		amount = 385
	}
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1
		amount = 300
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_1 #Stinger
		amount = 900
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_0	 #VW Iltis
		amount = 1500
		producer = GER
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_1	 #Mercedes-Benz G-Class
		amount = 2500
		producer = GER
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_2	 #Mercedes-Benz G-Class
		amount = 450
		producer = GER
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_1 #SpPz 2
		variant_name = "Spähpanzer 2 Luchs"
		amount = 409
		producer = GER
		#version_name = "Spähpanzer 2 Luchs"
	}
	add_equipment_to_stockpile = {
		type = apc_hull_1 #Tpz-1 Fuchs
		variant_name = "Tpz-1 Fuchs"
		amount = 1023
		producer = GER
		#version_name = "TPz-1"
	}
	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "M113"
		amount = 2167
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = artillery_0 #M101
		amount = 118
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = artillery_1 #FH-70
		amount = 196
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		amount = 130
		variant_name = "M109A3"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = spart_hull_1
		variant_name = "M270"
		amount = 150
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = spaa_hull_0 #Flakpanzer Gepard
		variant_name = "FlakPz Gepard 1A2"
		amount = 143
		producer = GER
	}
	add_equipment_to_stockpile = {
		type = spaa_hull_0 #Flakpanzer Gepard
		variant_name = "FlakPz Gepard 1A"
		amount = 114
		producer = GER
	}
	add_equipment_to_stockpile = {
		type = spaa_hull_0 #FlaRakPz 1 Roland II
		variant_name = "FlaRakPz 1 Roland II"
		amount = 80
		producer = GER
	}
	add_equipment_to_stockpile = {
		type = attack_helicopter_hull_0
		amount = 199
		producer = GER
		variant_name = "MBB Bo 105"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Bell UH-1 Iroquois
		amount = 188
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Sikorsky CH-53 Sea Stallion
		amount = 107
		producer = USA
		#version_name = "Sikorsky CH-53 Sea Stallion"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Bo 105
		amount = 60
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Aérospatiale SA-316
		amount = 28
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter3
		amount = 13
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter2 #Westland WG-13 Lynx
		amount = 22
		producer = ENG
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Sea King HU5
		amount = 21
		#version_name = "Sea King HU5"
		producer = USA
	}
}