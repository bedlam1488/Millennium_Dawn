﻿division_template = {
	name = "Motor Rifle Brigade"

	division_names_group = UZB_MECHANIZED_DIVISIONS

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
		Mot_Inf_Bat = { x = 1 y = 0 }
		armor_Bat = { x = 2 y = 0 }
		SP_Arty_Bat = { x = 3 y = 0 }
		SP_Arty_Bat = { x = 3 y = 1 }
	}

	support = {
		Mech_Recce_Comp = { x = 0 y = 0 }
		SP_AA_Battery = { x = 0 y = 1 }
		H_Engi_Comp = { x = 0 y = 2 }
	}
}

division_template = {
	name = "Motor Rifle Brigade 2"

	division_names_group = UZB_MECHANIZED_DIVISIONS

	regiments = {
		Mot_Inf_Bat = { x = 0 y = 0 }
		Mot_Inf_Bat = { x = 0 y = 1 }
		Mot_Inf_Bat = { x = 0 y = 2 }
		Arty_Bat = { x = 1 y = 0 }
		Arty_Bat = { x = 1 y = 1 }
	}
	support = {
		armor_Comp = { x = 0 y = 0 }
		Mech_Recce_Comp = { x = 0 y = 1 }
		H_Engi_Comp = { x = 0 y = 2 }
	}
}

division_template = {
	name = "Air Assault Brigade"

	division_names_group = UZB_AIR_CAV_BRIGADES

	regiments = {
		Arm_Air_assault_Bat = { x = 0 y = 0 }
		Arm_Air_assault_Bat = { x = 0 y = 1 }
		Arm_Air_assault_Bat = { x = 0 y = 2 }
		L_Air_assault_Bat = { x = 0 y = 3 }
	}
}

division_template = {
	name = "Airborne Brigade"

	division_names_group = UZB_AIR_CAV_BRIGADES

	regiments = {
		Mech_Air_Inf_Bat = { x = 0 y = 0 }
		Mech_Air_Inf_Bat = { x = 0 y = 1 }
		Arty_Bat = { x = 1 y = 0 }
	}
}

units = {
	division = {
		name = "36th Motor Rifle Brigade"
		location = 11687 	#Jizzakh
		division_template = "Motor Rifle Brigade"
		start_experience_factor = 0.25
		start_equipment_factor = 0.01
	}
	division = {
		name = "4th Airborne Brigade"
		location = 1216 	#Samarqand
		division_template = "Airborne Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "48th Motor Rifle Brigade"
		location = 1479	#Nukus
		division_template = "Motor Rifle Brigade"
		start_experience_factor = 0.25
		start_equipment_factor = 0.01
	}
	division = {
		name = "3rd Motor Rifle Brigade"
		location = 7775		#Navoi
		division_template = "Motor Rifle Brigade 2"
		start_experience_factor = 0.25
		start_equipment_factor = 0.01
	}
	division = {
		name = "7th Motor Rifle Brigade"
		location = 10205	#Kokayty
		division_template = "Motor Rifle Brigade 2"
		start_experience_factor = 0.25
		start_equipment_factor = 0.01
	}
	division = {
		name = "21st Motor Rifle Brigade"
		location = 12239		#Xeyrabad
		division_template = "Motor Rifle Brigade 2"
		start_experience_factor = 0.25
		start_equipment_factor = 0.01
	}
	division = {
		name = "22nd Motor Rifle Brigade"
		location = 12722		#Sherabad
		division_template = "Motor Rifle Brigade 2"
		start_experience_factor = 0.25
		start_equipment_factor = 0.01
	}
	division = {
		name = "25th Motor Rifle Brigade"
		location = 7639	#Qarshi
		division_template = "Motor Rifle Brigade 2"
		start_experience_factor = 0.25
		start_equipment_factor = 0.01
	}
	division = {
		name = "11th Motor Rifle Brigade" #fake name
		location = 1459	#Chirchik
		division_template = "Motor Rifle Brigade 2"
		start_experience_factor = 0.25
		start_equipment_factor = 0.01
	}
	division = {
		name = "15th Motor Rifle Brigade" #fake name
		location = 1216	#Samarqand
		division_template = "Motor Rifle Brigade 2"
		start_experience_factor = 0.25
		start_equipment_factor = 0.01
	}
	division = {
		name = "37th Motor Rifle Brigade"
		location = 1483		#Andijan
		division_template = "Motor Rifle Brigade"
		start_experience_factor = 0.25
		start_equipment_factor = 0.01
	}
	division = {
		name = "17th Air Assault Brigade"
		location = 2058	#Ferghana
		division_template = "Air Assault Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons1
		amount = 10000
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = command_control_equipment2
		amount = 1400
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Anti_tank_1
		amount = 450
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1
		amount = 185
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_0
		amount = 700
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_2
		amount = 670
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_0
		amount = 1000
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = mbt_hull_1 #T-72
		variant_name = "T-72A"
		amount = 70
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_1 #T-64
		variant_name = "T-64"
		amount = 100
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_0 #T-62
		variant_name = "T-62"
		amount = 60
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = ifv_hull_0
		variant_name = "BMP-1"
		amount = 146
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = ifv_hull_1
		variant_name = "BMP-3"
		amount = 9
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "MT-LB"
		amount = 120
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = ifv_hull_0
		variant_name = "BMP-2"
		amount = 160
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "BRDM-2"
		amount = 13
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "BTR-60"
		amount = 24
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "BTR-70"
		amount = 25
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "BTR-80"
		amount = 210
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "2S1 Gvozdika"
		amount = 36
		producer = SOV
		#version_name = "2S1 Gvozdika"
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "2S3 Akatsiya"
		amount = 48
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = artillery_0 #D-30
		amount = 140
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = artillery_0 #2A36 Giatsint-B
		amount = 140
		producer = SOV
		#version_name = "2A36 Giatsint-B"
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "BM-21 Grad"
		amount = 36
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "BM-27 Uragan"
		amount = 48
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = spaa_hull_0
		amount = 45
		producer = SOV
		variant_name = "SA-3 Goa"
	}

	#Helicopters
	add_equipment_to_stockpile = {
		type = attack_helicopter_hull_0
		amount = 42
		producer = SOV
		variant_name = "Mil Mi-24"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Mil Mi-6
		amount = 28
		producer = SOV
		#version_name = "Mil Mi-6"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter2 #Mil Mi-26 Halo
		amount = 1
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Mil Mi-8
		amount = 158	#100 extra
		producer = SOV
	}
}