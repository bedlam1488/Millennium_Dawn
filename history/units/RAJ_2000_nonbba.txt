﻿instant_effect = {
	#Aircraft
	add_equipment_to_stockpile = {
		type = transport_plane3 #Il-76MD
		amount = 31
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = transport_plane1 #An-32
		amount = 105
		producer = UKR
	}

	add_equipment_to_stockpile = {
		type = naval_plane1 #Il-76
		amount = 5
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = naval_plane2 #Tu-142
		amount = 8
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = CV_MR_Fighter2 #Sea Harrier
		amount = 23
		producer = ENG
	}

	add_equipment_to_stockpile = {
		type = CV_MR_Fighter2 #Su-30MKI
		amount = 10
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = MR_Fighter2 #MIG-29
		amount = 73
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = MR_Fighter1 #MIG-21
		amount = 304
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = AS_Fighter1 #MIG-23
		amount = 79
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Strike_fighter2 #MIG-27
		amount = 147
	}

	add_equipment_to_stockpile = {
		type = Strike_fighter2 #Mirage 2000
		amount = 35
		producer = FRA
	}
}