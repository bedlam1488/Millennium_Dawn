﻿instant_effect = {
	add_equipment_to_stockpile = {
		variant_name = "Aero L-39"
		type = small_plane_strike_airframe_1
		amount = 12
		producer = CZE
	}
	add_equipment_to_stockpile = {
		variant_name = "MiG-23"
		type = small_plane_strike_airframe_1
		amount = 45
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "MiG-25 Foxbat"
		type = medium_plane_fighter_airframe_1
		amount = 16
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "MiG-27"
		type = medium_plane_cas_airframe_1
		amount = 15
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "MiG-29 Fulcrum"
		type = small_plane_airframe_2
		amount = 15
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "Su-24M"
		type = medium_plane_airframe_2
		amount = 39
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "MiG-31 Foxhound"
		type = medium_plane_fighter_airframe_2
		amount = 43
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "Su-25"
		type = medium_plane_cas_airframe_2
		amount = 14
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "Su-27M"
		type = medium_plane_fighter_airframe_2
		amount = 14
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "An-32"
		type = large_plane_air_transport_airframe_1
		amount = 10
		producer = SOV
	}
}