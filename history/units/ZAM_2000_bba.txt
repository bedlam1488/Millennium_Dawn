﻿instant_effect = {

	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "J-6"					#added airframes to replace missing K-8 aircraft
		amount = 28
		producer = CHI
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "MiG-21s Bis"
		amount = 12
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "An-26"					#added airframes to replace missing C-47
		amount = 8
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "DHC-4 Caribou"				#in place of DHC-5
		amount = 4
		producer = CAN
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "Harbin Y-12"
		amount = 4
		producer = CHI
	}
}