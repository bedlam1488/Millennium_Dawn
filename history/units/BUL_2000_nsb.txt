﻿division_template = {
	name = "Mekhanizirana Brigada"

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
		Arm_Inf_Bat = { x = 0 y = 3 }
		SP_Arty_Bat = { x = 1 y = 0 }
		SP_Arty_Bat = { x = 1 y = 1 }
	}

	support = {
		armor_Comp = { x = 0 y = 0 }
	}
}
division_template = {
	name = "Spetsialni Sili Polk"

	regiments = {
		Special_Forces = { x = 0 y = 0 }
		Mot_Air_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
	}

	support = {
		L_Engi_Comp = { x = 0 y = 0 }
	}

	priority = 2
}
division_template = {
	name = "Nezavisima Mekhanizirana Brigada"

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
	}

	support = {
		armor_Comp = { x = 0 y = 0 }
	}
}

units = {
	division = {
		name = "2-ra Mekhanizirana Brigada"
		location = 6814		#Stara Zagora
		division_template = "Mekhanizirana Brigada"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "61-ta Mekhanizirana Brigada"
		location = 6982		#Karlovo
		division_template = "Mekhanizirana Brigada"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "68-i Spetsialni Sili Polk"
		location = 6923			#Plovdiv
		division_template = "Spetsialni Sili Polk"
		start_experience_factor = 0.7
		start_equipment_factor = 0.01
	}
	division = {
		name = "3-ta Nezavisima Mekhanizirana Brigada"
		location = 935		#Stara Zagora
		division_template = "Nezavisima Mekhanizirana Brigada"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = command_control_equipment	 #
		amount = 400
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons	 #AKMS
		amount = 1000
		producer = BUL
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons1	 #AR-M1
		amount = 2000
		producer = BUL
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_0		 #AT-3
		amount = 200
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1		 #AT-5
		amount = 300
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1		 #AT-6
		#version_name = "AT-6 Spiral"
		amount = 500
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_1		 #AT-4
		amount = 222
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_1		 #AT-7
		#version_name = "AT-7 Metis"
		amount = 50
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_0		 #SA-7
		amount = 100
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_1		 #SA-16
		amount = 100
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = mbt_hull_1
		variant_name = "T-72A"
		amount = 433
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "T-55"
		amount = 1042
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = apc_hull_1
		amount = 58
		producer = SOV
		variant_name = "BRDM-2"
	}

	add_equipment_to_stockpile = {
		type = ifv_hull_0
		variant_name = "BMP-1"
		amount = 100
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = ifv_hull_0
		variant_name = "BMP-23"
		amount = 114
		producer = BUL
	}

	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "BTR-60"
		amount = 737
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "MT-LB"
		amount = 1013
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "2S1 Gvozdika"
		amount = 692
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "BM-21 Grad"
		amount = 222
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = util_vehicle_1		 #UAZ-469
		amount = 500
		producer = SOV
	}

	#Helicopters
	add_equipment_to_stockpile = {
		type = attack_helicopter_hull_0
		amount = 43
		producer = SOV
		variant_name = "Mil Mi-24"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1		 #Mi-8
		amount = 8
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1		 #Mi-17
		#version_name = "Mil Mi-17"
		amount = 31
		producer = SOV
	}
}