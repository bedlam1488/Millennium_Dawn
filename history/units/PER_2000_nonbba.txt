﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = MR_Fighter1 #F-5
		amount = 80
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter1 #J-7
		amount = 24
		producer = CHI
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter2 #MIG-29
		amount = 36
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = AS_Fighter2 #F-14
		amount = 43
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = AS_Fighter1 #F-4
		amount = 81
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = Strike_fighter2 #Su-24
		amount = 30
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = naval_plane2 #P-3C
		amount = 5
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_plane1 #C-130 and var boeing
		amount = 30
		producer = USA
	}
}