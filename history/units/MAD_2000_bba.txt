instant_effect = {

	add_equipment_to_stockpile = {
		variant_name = "Su-17"					#in place of MiG-17's
		type = small_plane_strike_airframe_1
		amount = 4
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "MiG-21s Bis"
		amount = 8
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "An-26"
		amount = 4
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "C-212 Aviocar"
		amount = 2
		producer = SPR
	}

	add_equipment_to_stockpile = {
		type = medium_plane_maritime_patrol_airframe_1
		variant_name = "BN-2 Defender"
		amount = 1
		producer = ENG
	}

}