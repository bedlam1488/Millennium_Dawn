﻿division_template = {
	name = "Counter Revolutionary Warfare Unit"

	regiments = {
		Special_Forces = { x = 0 y = 0 }
		Special_Forces = { x = 1 y = 0 }
		Special_Forces = { x = 2 y = 0 }
	}

	priority = 2
}

division_template = {
	name = "Light Infantry Battalion"

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 1 y = 0 }
	}

	support = {
		L_Engi_Comp = { x = 0 y = 0 }
		Arty_Battery = { x = 0 y = 1 }
	}

	priority = 1
}

units = {
	division = {
		name = "Counter Revolutionary Warfare Unit"
		location = 7302
		division_template = "Counter Revolutionary Warfare Unit"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "1st Reg/1st Battalion"
		location = 7302
		division_template = "Light Infantry Battalion"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {
		name = "1st Reg/2nd Battalion"
		location = 7302
		division_template = "Light Infantry Battalion"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {
		name = "1st Reg/3rd Battalion"
		location = 7302
		division_template = "Light Infantry Battalion"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons1
		amount = 5000
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = Anti_tank_0
		amount = 200
		producer = ENG
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_1
		amount = 150
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = command_control_equipment1
		amount = 500
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = artillery_0
		amount = 20
		producer = GER
	}
}