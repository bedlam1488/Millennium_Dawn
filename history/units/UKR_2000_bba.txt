﻿instant_effect = {

	add_equipment_to_stockpile = {
		type = medium_plane_airframe_2
		variant_name = "Su-24M"
		amount = 218
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = medium_plane_cas_airframe_2
		variant_name = "Su-25"
		amount = 73
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "MiG-23"
		amount = 51
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = small_plane_airframe_2
		variant_name = "MiG-29 Fulcrum"
		amount = 224
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = medium_plane_fighter_airframe_2
		variant_name = "Su-27"
		amount = 66
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "Su-17"
		amount = 59
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "Il-76"
		amount = 78
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "An-26"
		amount = 45
		producer = UKR
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "Aero L-39"
		amount = 337
		producer = CZE
	}
	add_equipment_to_stockpile = {
		type = large_plane_airframe_2
		variant_name = "Tu-22M"
		amount = 22
		producer = SOV
	}
}