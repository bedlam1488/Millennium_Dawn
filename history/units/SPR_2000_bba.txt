﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = cv_small_plane_strike_airframe_1
		variant_name = "AV-8B Harrier II"
		amount = 17
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = cv_small_plane_strike_airframe_1
		variant_name = "Sea Harrier"
		amount = 9
		producer = ENG
	}
	add_equipment_to_stockpile = {
		type = cv_medium_plane_airframe_2
		variant_name = "F/A-18A/B Hornet"
		amount = 90
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = small_plane_airframe_1
		variant_name = "F-5 Freedom Fighter"
		amount = 35
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = cv_medium_plane_airframe_1
		variant_name = "F-4 Phantom II"
		amount = 14
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = small_plane_airframe_1
		variant_name = "Mirage F1"
		amount = 65
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = large_plane_maritime_patrol_airframe_1
		variant_name = "P-3 Orion"
		amount = 7
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "C-130 Hercules"
		amount = 11
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "C-212 Aviocar"
		amount = 78
		producer = SPR
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "CASA C-101"
		amount = 74
		producer = SPR
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "Dassault Falcon 20"
		amount = 2
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "CASA/IPTN CN-235"
		amount = 20
		producer = SPR
	}

	###################
	### Helicopters ###
	###################

	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Eurocopter AS332 Super Puma
		amount = 78
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter2 #Eurocopter AS532 Cougar
		amount = 23
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Bell UH-1 Iroquois
		amount = 93
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Sea King HU5
		amount = 2
		producer = USA
		#version_name = "Sea King HU5"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Bell UH-1 Iroquois
		amount = 10
		producer = USA
	}
	#11SH-3D (8 -H ASW,
	add_equipment_to_stockpile = {
		type = transport_helicopter2 #Sikorsky UH-60 Black Hawk
		amount = 6
		producer = USA
	}
}