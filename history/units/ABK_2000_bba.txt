﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = medium_plane_cas_airframe_1
		variant_name = "SU-25"
		amount = 1
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "L-39"
		amount = 2
		producer = CZE
	}
}