﻿instant_effect = {
	
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "Su-22M5 Fitter"
		amount = 28
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "Mirage 5"
		amount = 11
		producer = FRA
	}
	add_equipment_to_stockpile = {
		variant_name = "Su-25"
		type = medium_plane_cas_airframe_2
		amount = 18
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "A-37 Dragonfly"
		type = small_plane_strike_airframe_1
		amount = 23
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "T-38C Talon"			#in place of T-37
		type = small_plane_strike_airframe_1
		amount = 20
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "Mirage 2000C"
		type = small_plane_airframe_2
		amount = 12
		producer = FRA
	}
	add_equipment_to_stockpile = {
		variant_name = "MiG-29 Fulcrum"
		type = small_plane_airframe_2
		amount = 21
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "Embraer EMB-312 Tucano"
		amount = 25
		producer = BRA
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "An-32"
		amount = 23
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "Harbin Y-12"
		amount = 7
		producer = CHI
	}
	add_equipment_to_stockpile = {
		variant_name = "C-130 Hercules"
		type = large_plane_air_transport_airframe_1
		amount = 10
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "DHC-4 Caribou"			#in place of DHC-5
		type = large_plane_air_transport_airframe_1
		amount = 12
		producer = CAN
	}
	add_equipment_to_stockpile = {
		variant_name = "C-26 Metroliner"
		type = large_plane_awacs_airframe_1
		amount = 3
		producer = USA
	}
}