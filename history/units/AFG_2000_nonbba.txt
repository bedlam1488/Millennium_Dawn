instant_effect = {
	add_equipment_to_stockpile = {
		type = L_Strike_fighter2		#L-39
		amount = 5
		producer = CZE
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter1		#MiG-21
		amount = 5
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Strike_fighter1		#Su-17
		amount = 3
		producer = SOV
	}
}
