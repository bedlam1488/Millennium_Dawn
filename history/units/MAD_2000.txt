﻿division_template = {
	name = "Régiment Régional d'Infanterie"

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
	}
	support = {
		Arty_Battery = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
		L_Engi_Comp = { x = 0 y = 2 }
	}
}
units = {
	division = {
		name = "1 Régiment Régional d'Infanterie"
		location = 11015
		division_template = "Régiment Régional d'Infanterie"
		start_experience_factor = 0.01
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = command_control_equipment
		amount = 200
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons		#AKM
		amount = 1400
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 12
		producer = SOV
		variant_name = "PT-76"
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 10
		producer = ENG
		variant_name = "Ferret"
	}
	add_equipment_to_stockpile = {
		type = apc_hull_1
		amount = 35
		producer = SOV
		variant_name = "BRDM-2"
	}

	add_equipment_to_stockpile = {
		type = artillery_0		#D-30
		amount = 12
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_0
		amount = 50
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_0
		amount = 50
		producer = SOV
	}
}