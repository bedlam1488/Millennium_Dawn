﻿instant_effect = {
	
	add_equipment_to_stockpile = {
		type = MR_Fighter1			#J-7
		amount = 25
		producer = CHI
	}

	add_equipment_to_stockpile = {
		type = transport_helicopter1			#Mi-17
		amount = 12
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = transport_helicopter1			#UH-1
		amount = 14
		producer = USA
	}
}