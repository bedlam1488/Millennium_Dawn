﻿division_template = {
	name = "Brigada Blindada"

	regiments = {
		L_arm_Bat = { x = 0 y = 0 }
		L_arm_Bat = { x = 0 y = 1 }
		Mech_Inf_Bat = { x = 0 y = 2 }
	}
	support = {
		Arty_Battery = { x = 0 y = 0 }
		L_Engi_Comp = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Regimiento de Infantería Motorizada"

	regiments = {
		Mot_Inf_Bat = { x = 0 y = 0 }
		Mot_Inf_Bat = { x = 0 y = 1 }
	}
	support = {
		Arty_Battery = { x = 0 y = 0 }
		L_Engi_Comp = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Brigada de Asalto Aéreo"

	regiments = {
		L_Air_Inf_Bat = { x = 0 y = 0 }
		L_Air_Inf_Bat = { x = 0 y = 1 }
	}
	support = {
		Arty_Battery = { x = 0 y = 0 }
		L_Engi_Comp = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Brigada de Infanteria"

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
	}
	support = {
		Arty_Battery = { x = 0 y = 0 }
		L_Engi_Comp = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Brigada Marina"

	regiments = {
		L_Marine_Bat = { x = 0 y = 0 }
		L_Marine_Bat = { x = 0 y = 1 }
		L_Marine_Bat = { x = 0 y = 2 }
	}
	support = {
		L_Engi_Comp = { x = 0 y = 1 }
	}
}

units = {
	division = {
		name = "Brigada Blindada"
		location = 5230
		division_template = "Brigada Blindada"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}

	division = {
		name = "Regimiento de Infantería Motorizada"
		location = 5230
		division_template = "Regimiento de Infantería Motorizada"
		start_experience_factor = 0.4
		start_equipment_factor = 0.01
	}

	division = {
		name = "1. Brigada de Asalto Aéreo"
		location = 10987
		division_template = "Brigada de Asalto Aéreo"
		start_experience_factor = 0.6
		start_equipment_factor = 0.01
	}

	division = {
		name = "2. Brigada de Asalto Aéreo"
		location = 7231
		division_template = "Brigada de Asalto Aéreo"
		start_experience_factor = 0.6
		start_equipment_factor = 0.01
	}

	division = {
		name = "1. Brigada de Infanteria"
		location = 5230
		division_template = "Brigada de Infanteria"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "2. Brigada de Infanteria"
		location = 12930
		division_template = "Brigada de Infanteria"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "3. Brigada de Infanteria"
		location = 10987
		division_template = "Brigada de Infanteria"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "4. Brigada de Infanteria"
		location = 5179
		division_template = "Brigada de Infanteria"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "5. Brigada de Infanteria"
		location = 7412
		division_template = "Brigada de Infanteria"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "6. Brigada de Infanteria"
		location = 7335
		division_template = "Brigada de Infanteria"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "1. Brigada Marina"
		location = 7335
		division_template = "Brigada Marina"
		start_experience_factor = 0.6
		start_equipment_factor = 0.01
	}

	division = {
		name = "2. Brigada Marina"
		location = 5230
		division_template = "Brigada Marina"
		start_experience_factor = 0.6
		start_equipment_factor = 0.01
	}

}
instant_effect = {
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 36
		producer = AUS
		variant_name = "SK-105 Kurassier"
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_1
		amount = 24
		producer = BRA
		variant_name = "EE-9 Cascavel"
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 10
		producer = USA
		variant_name = "V150 Cadillac Commando"
	}

	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "M113"
		amount = 18
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "MOWAG Roland"
		amount = 20
		producer = SWI
	}

	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "EE-11 Urutu"
		amount = 24
		producer = BRA
	}

	add_equipment_to_stockpile = {
		type = artillery_0		#M101
		amount = 30
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = artillery_0		#Type 54
		amount = 18
		producer = CHI
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_0				#HN-5
		amount = 300
		producer = CHI
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1				#HJ-8B
		amount = 200
		producer = CHI
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_0				#
		amount = 400
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = util_vehicle_0				#ZIL-131
		amount = 450
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = util_vehicle_0				#M151 Jeep
		amount = 600
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons1					#Galil AR
		amount = 1200
		producer = ISR
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons1					#M16
		amount = 2200
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons1					#Steyr Aug
		amount = 1200
		producer = AUS
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons2					#SA80
		amount = 1200
		producer = ENG
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons1					#SIG SG 540
		amount = 1200
		producer = SWI
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons					#Type 56
		amount = 1200
		producer = CHI
	}

	add_equipment_to_stockpile = {
		type = command_control_equipment
		amount = 900
		producer = BOL
	}
}