﻿instant_effect = {

	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "A-5"					#in place of J-5 & MiG-15
		amount = 5
		producer = CHI
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "J-6"
		amount = 10
		producer = CHI
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "J-7"
		amount = 6
		producer = CHI
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "DHC-4 Caribou"				#in place of DHC-5
		amount = 3
		producer = CAN
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "Shaanxi Y-7"				#in place of Y-5
		amount = 1
		producer = CHI
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "Harbin Y-12"
		amount = 2
		producer = CHI
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "Fokker F27 Friendship"			#in place of F-28 & HS-125-700
		amount = 3
		producer = HOL
	}
}