﻿instant_effect = {
	add_equipment_to_stockpile = {
		variant_name = "F/A-18C Hornet"
		type = cv_medium_plane_airframe_2
		amount = 32
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = cv_medium_plane_cas_airframe_2
		amount = 8
		variant_name = "F/A-18D Hornet"
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "Mirage F1"
		type = small_plane_airframe_1
		amount = 14
		producer = FRA
	}
	add_equipment_to_stockpile = {
		variant_name = "BAE Systems Hawk"
		type = small_plane_strike_airframe_2
		amount = 12
		producer = ENG
	}
	add_equipment_to_stockpile = {
		variant_name = "Embraer EMB-312 Tucano"
		type = small_plane_strike_airframe_1
		amount = 16
		producer = BRA
	}
	add_equipment_to_stockpile = {
		variant_name = "C-130 Hercules"
		type = large_plane_air_transport_airframe_1
		amount = 4
		producer = USA
	}
}