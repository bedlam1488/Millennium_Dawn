instant_effect = {
	
	add_equipment_to_stockpile = {
		#variant_name = "CASA/IPTN CN-235"
		type = transport_plane1
		amount = 2
		producer = SPR
	}

	add_equipment_to_stockpile = {
		#variant_name = "C-212 Aviocar"
		type = transport_plane1
		amount = 3
		producer = SPR
	}

	add_equipment_to_stockpile = {
		#variant_name = "CASA C-101"
		type = L_Strike_fighter1
		amount = 6
		producer = SPR
	}

}