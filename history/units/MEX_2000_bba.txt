﻿instant_effect = {
	add_equipment_to_stockpile = {
		variant_name = "F-5 Freedom Fighter"
		type = small_plane_airframe_1
		amount = 8
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "Pilatus PC-7"
		type = small_plane_strike_airframe_1
		amount = 70
		producer = SWI
	}
	add_equipment_to_stockpile = {
		variant_name = "F-101 Voodoo"			#in place of AT-33 aircraft
		type = small_plane_strike_airframe_1
		amount = 17
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "C-26 Metroliner"
		type = large_plane_awacs_airframe_1
		amount = 4
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "C-130 Hercules"
		type = large_plane_air_transport_airframe_1
		amount = 10
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "C-212 Aviocar"
		type = large_plane_air_transport_airframe_1
		amount = 10
		producer = SPR
	}
	add_equipment_to_stockpile = {
		variant_name = "An-32"
		type = large_plane_air_transport_airframe_1
		amount = 3
		producer = SOV
	}
}