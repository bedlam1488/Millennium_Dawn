instant_effect = {

	add_equipment_to_stockpile = {
		type = L_Strike_fighter1
		amount = 5
		#variant_name = "A-37 Dragonfly"
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = L_Strike_fighter1		#in place of OV-37
		amount = 4
		#variant_name = "OV-10 Bronco"
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = transport_plane1	#in place of all other smaller variants they actually use and we do not have
		amount = 3
		#variant_name = "C-130 Hercules"
		producer = USA
	}

}