﻿division_template = {
	name = "Combined Arms Division"

	regiments = {
		Mot_Inf_Bat = { x = 0 y = 0 }
		Mot_Inf_Bat = { x = 0 y = 1 }
		Mot_Inf_Bat = { x = 0 y = 2 }

		Arm_Inf_Bat = { x = 1 y = 0 }
		Mech_Inf_Bat = { x = 1 y = 1 }
		Mech_Inf_Bat = { x = 1 y = 2 }
		Mech_Inf_Bat = { x = 1 y = 3 }

		L_arm_Bat = { x = 2 y = 0 }
		L_arm_Bat = { x = 2 y = 1 }
		L_arm_Bat = { x = 2 y = 2 }

		Arty_Bat = { x = 3 y = 0 }
		Arty_Bat = { x = 3 y = 1 }
	}
	support = {
		SP_AA_Battery = { x = 0 y = 0 }
		armor_Comp = { x = 0 y = 1 }
		Mot_Recce_Comp = { x = 0 y = 2 }
	}
}
division_template = {
	name = "Infantry Brigade"

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
		Arty_Bat = { x = 0 y = 3 }
		L_Inf_Bat = { x = 0 y = 4 }
	}
}
division_template = {
	name = "Armoured Brigade"

	regiments = {
		Arm_Inf_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
		armor_Bat = { x = 1 y = 0 }
		SP_Arty_Bat = { x = 1 y = 1 }
		L_Inf_Bat = { x = 1 y = 2 }
	}
}

units = {
	division = {
		name = "3rd Division"
		location = 12299
		division_template = "Combined Arms Division"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "6th Division"
		location = 12299
		division_template = "Combined Arms Division"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "9th Division"
		location = 12299
		division_template = "Combined Arms Division"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	#diesel_attack_submarine_1 = Archer-Class
	#diesel_attack_submarine_3 = Archer-Class
	#missile_frigate_3 = Formidable-Class
	#missile_corvette_1 = Victory-Class
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons3
		amount = 7750
		#producer = SOV
	}
	add_equipment_to_stockpile = {
		type = command_control_equipment3
		amount = 800
		#producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Anti_tank_2
		amount = 430
		producer = ISR
	}
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1
		amount = 200
		producer = ISR
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_1
		amount = 350
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = mbt_hull_1
		variant_name = "Leopard 2A4"
		amount = 96
		producer = GER
	}
	add_equipment_to_stockpile = {
		type = IFV_2 #AMX-10P
		amount = 44
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = APC_5 #Terrex AV-81
		amount = 135
	}
	add_equipment_to_stockpile = {
		type = IFV_4 #Bionix 25
		amount = 500
	}
	add_equipment_to_stockpile = {
		type = APC_2 #M113 APC
		amount = 750
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = APC_4 #Bronco ATTC
		amount = 400
	}
	add_equipment_to_stockpile = {
		type = Rec_tank_0 #Commando 150
		amount = 250
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_5 #MaxxPro MRAP
		amount = 15
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = SP_arty_2 #SSPH Primus
		amount = 54
	}
	add_equipment_to_stockpile = {
		type = artillery_1 #FH-2000
		amount = 18
	}
	add_equipment_to_stockpile = {
		type = artillery_2 #SLWH Pegasus
		amount = 18
	}
	add_equipment_to_stockpile = {
		type = artillery_1 #FH-88
		amount = 52
		#version_name = "FH-88"
	}
	add_equipment_to_stockpile = {
		type = SP_R_arty_2 #HIMARS
		amount = 18
		producer = USA
		#version_name = "HIMARS"
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter1 #F-5S Tiger II
		amount = 29
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = Strike_fighter3 #F-15E Strike Eagle
		amount = 32
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter3 #F-16C Fighting Falcon
		amount = 60
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_plane1 #C-130 Hercules
		amount = 4
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_plane1 #C-130 Hercules
		amount = 9
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = attack_helicopter3 #AH-64D Apache Longbow
		amount = 19
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter2 #Boeing CH-47D Chinook
		amount = 16
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Eurocopter AS332 Super Puma
		amount = 18
		producer = FRA
		#version_name = "Eurocopter AS332 Super Puma"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter2 #Eurocopter AS532 Cougar
		amount = 12
		producer = FRA
	}
}