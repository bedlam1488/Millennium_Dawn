instant_effect = {
	add_equipment_to_stockpile = {
		type = medium_plane_fighter_airframe_1
		variant_name = "MiG-25 Foxbat"
		amount = 14
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "MiG-23"
		amount = 70
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "MiG-21s Bis"
		amount = 76
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = small_plane_airframe_2
		variant_name = "MiG-29 Fulcrum"
		amount = 12
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = medium_plane_airframe_2
		variant_name = "Su-24M"
		amount = 13
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "C-130 Hercules"
		amount = 16
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "Il-76"
		amount = 6
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_2
		variant_name = "Il-76M"
		amount = 3
		producer = SOV
	}
}