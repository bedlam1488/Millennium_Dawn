﻿
instant_effect = {
	add_equipment_to_stockpile = {
		type = MR_Fighter1		#F-5
		amount = 20
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = transport_plane1		#C-130
		amount = 9
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = naval_plane1
		#variant_name = "BN-2 Defender"
		amount = 12
		producer = ENG
	}
}