﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = transport_plane2 #CASA CN-235
		amount = 5
		producer = IND
	}
	add_equipment_to_stockpile = {
		type = naval_plane1 #P-3 Orion
		amount = 4
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter1 #Pilatus PC-7
		amount = 10
		producer = SWI
	}
	add_equipment_to_stockpile = {
		type = Strike_fighter1 #Dassault Mirage 5
		amount = 26
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter1 #F-5 Freedom Fighter
		amount = 16
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter1 #A-37 Dragonfly
		amount = 24
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter1 #A-37 Dragonfly
		amount = 8
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter1 #Dassault Mirage F1
		amount = 15
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = transport_plane1 #C-130 Hercules
		amount = 6
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter2 #T-38C Talon
		amount = 42
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = awacs_equipment_1 #E-3 Sentry
		amount = 3
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_plane2 #C-212 Aviocar
		amount = 7
		producer = SPR
	}
}