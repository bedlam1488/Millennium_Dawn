﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = MR_Fighter2 #MiG-29 Fulcrum
		amount = 24
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter1 #MiG-21
		amount = 38
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter2 #L-39
		amount = 34
		producer = CZE
	}
}