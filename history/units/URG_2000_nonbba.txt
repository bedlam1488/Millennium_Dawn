﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = L_Strike_fighter1 #A-37 Dragonfly
		amount = 10
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter1 #IA 58 Pucará
		amount = 5
		producer = ARG
	}
	add_equipment_to_stockpile = {
		type = transport_plane1			#C130
		amount = 3
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1			#Huey
		amount = 6
		producer = USA
	}
}