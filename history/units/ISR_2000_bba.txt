﻿instant_effect = {
	add_equipment_to_stockpile = {
		variant_name = "F-4 Phantom II"
		type = cv_medium_plane_airframe_1
		amount = 70
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = medium_plane_fighter_airframe_2
		amount = 73
		variant_name = "F-15C Eagle"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = medium_plane_airframe_2
		amount = 25
		variant_name = "F-15I Ra am"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_2
		amount = 237
		variant_name = "F-16C Blk 40/42"
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "C-130 Hercules"
		type = large_plane_air_transport_airframe_1
		amount = 25
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "C-130J Super Hercules"		#To cover C-47 and Boeing 707's we dont have variants for
		type = large_plane_air_transport_airframe_1
		amount = 17
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "Boeing EL/M-2075 Phalcon"
		type = large_plane_awacs_airframe_2
		amount = 6
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "IAI Sea Scan"
		type = medium_plane_maritime_patrol_airframe_1
		amount = 3
		producer = ISR
	}
}