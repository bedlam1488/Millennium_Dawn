instant_effect = {

	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "An-26"
		amount = 2
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1		#in place of C-47
		variant_name = "C-130 Hercules"
		amount = 2
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1		#2x additional to cover Do-128 airframes
		variant_name = "DHC-4 Caribou"
		amount = 3
		producer = CAN
	}

}