﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = Strike_fighter1		 #Jaguar
		amount = 8
		producer = ENG
	}

	add_equipment_to_stockpile = {
		type = L_Strike_fighter3		 #Hawk 200
		amount = 12
		producer = ENG
	}
}