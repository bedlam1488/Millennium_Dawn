﻿division_template = {
	name = "Liwa Mechaniqui"

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 1 y = 0 }
		Mech_Inf_Bat = { x = 1 y = 1 }
		SP_Arty_Bat = { x = 2 y = 0 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Liwa Moudar'a"

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 1 y = 0 }
		Arm_Inf_Bat = { x = 1 y = 1 }
		SP_Arty_Bat = { x = 2 y = 0 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Liwa Mushat"

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		Mot_Inf_Bat = { x = 1 y = 0 }
		Mot_Inf_Bat = { x = 1 y = 1 }
		SP_Arty_Bat = { x = 2 y = 0 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Liwa Haris Milkiun"

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		Mech_Inf_Bat = { x = 0 y = 2 }
		Mech_Inf_Bat = { x = 1 y = 0 }
		Mech_Inf_Bat = { x = 1 y = 1 }
	}

	support = {
		L_Engi_Comp = { x = 0 y = 0 }
	}

	priority = 2
}

division_template = {
	name = "Special Operations Command"

	regiments = {
		Special_Forces = { x = 0 y = 0 }
	}
	priority = 2
}

division_template = {
	name = "Marine Battalion"

	regiments = {
		L_Marine_Bat = { x = 0 y = 0 }
	}

	priority = 2
}

units = {
	division = {
		name = "1. Liwa Mechaniqui"
		location = 3725
		division_template = "Liwa Mechaniqui"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {
		name = "2. Liwa Mechaniqui"
		location = 3689
		division_template = "Liwa Mechaniqui"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {
		name = "3. Liwa Mechaniqui"
		location = 3689
		division_template = "Liwa Mechaniqui"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {
		name = "1. Liwa Moudar'a"
		location = 3689
		division_template = "Liwa Moudar'a"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {
		name = "2. Liwa Moudar'a"
		location = 3585
		division_template = "Liwa Moudar'a"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {
		name = "1. Liwa Mushat"
		location = 7960
		division_template = "Liwa Mechaniqui"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {
		name = "2. Liwa Mushat"
		location = 7960
		division_template = "Liwa Mechaniqui"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {
		name = "Liwa Haris Milkiun"
		location = 3689
		division_template = "Liwa Haris Milkiun"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "Marine Battalion"
		location = 3689
		division_template = "Marine Battalion"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "Special Operations Command"
		location = 3689
		division_template = "Special Operations Command"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons3
		amount = 5250
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = command_control_equipment2
		amount = 800
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = Anti_tank_1
		amount = 240
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_2
		amount = 110
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_0
		amount = 200
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_1
		amount = 170
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = mbt_hull_2 #Leclerc
		variant_name = "AMX-56 Leclerc"
		amount = 250
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_1 #OF-40
		variant_name = "OF-40 Mk.2"
		amount = 36
		producer = ITA
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_0 #AMX-30
		variant_name = "AMX-30B2"
		amount = 45
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_1
		amount = 76
		producer = ENG
		variant_name = "FV101 Scorpion"
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 49
		producer = FRA
		variant_name = "Panhard AML 90"
	}
	add_equipment_to_stockpile = {
		type = ifv_hull_1
		variant_name = "AMX-10P"
		amount = 18
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = ifv_hull_1
		variant_name = "BMP-3"
		amount = 415
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "M3 Panhard"
		amount = 370
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "EE-11 Urutu"
		amount = 120
		producer = BRA
	}
	add_equipment_to_stockpile = {
		type = artillery_0 #type-59
		amount = 20
		producer = CHI
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "G6 SP Arty"
		amount = 72
		producer = SAF
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "M109A3"
		amount = 87
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "Mk F3"
		amount = 18
		producer = FRA
	}

	#Helicopters
	add_equipment_to_stockpile = {
		type = attack_helicopter_hull_1
		amount = 20
		producer = USA
		variant_name = "AH-64 A/D Apache Longbow"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Aérospatiale SA-330 Puma
		amount = 13
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Bell UH-1 Iroquois
		amount = 27
		producer = USA
	}
}