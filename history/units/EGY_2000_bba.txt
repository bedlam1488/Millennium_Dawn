﻿instant_effect = {

	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "Aero L-39"
		amount = 48
		producer = CZE
	}

	### Airforce ###

	add_equipment_to_stockpile = {
		type = cv_medium_plane_scout_airframe_2
		amount = 5
		variant_name = "E-2 Hawkeye"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		amount = 21
		variant_name = "C-130 Hercules"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		amount = 4
		variant_name = "C-20 Gulfstream"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		amount = 3
		variant_name = "Dassault Falcon 20"
		producer = FRA
	}

	#Combat aicraft

	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_2
		amount = 41
		variant_name = "Dassault Alpha Jet"
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		amount = 79
		variant_name = "Mirage 5"
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_2
		amount = 189
		variant_name = "F-16C Blk 40/42"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		amount = 44
		variant_name = "J-6"
		producer = CHI
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		amount = 53
		variant_name = "J-7"
		producer = CHI
	}
	add_equipment_to_stockpile = {
		type = small_plane_airframe_2
		amount = 18
		variant_name = "Mirage 2000C"
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		amount = 54
		variant_name = "MiG-21s Bis"
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = cv_medium_plane_airframe_1
		amount = 28
		variant_name = "F-4 Phantom II"
		producer = USA
	}
}