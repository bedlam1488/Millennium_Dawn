﻿division_template = {
	name = "Motor Rifle Division" #Mechanized

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		Mech_Inf_Bat = { x = 0 y = 2 }

		Arm_Inf_Bat = { x = 1 y = 0 }
		Arm_Inf_Bat = { x = 1 y = 1 }
		L_arm_Bat = { x = 1 y = 2 }

		armor_Bat = { x = 2 y = 0 }
		armor_Bat = { x = 2 y = 1 }
		armor_Bat = { x = 2 y = 2 }
		SP_AA_Bat = { x = 2 y = 3 }
		SP_Arty_Bat = { x = 2 y = 4 }

		Arty_Bat = { x = 3 y = 0 }
		Arty_Bat = { x = 3 y = 1 }
		Arty_Bat = { x = 3 y = 2 }
	}
	support = {
		Mech_Recce_Comp = { x = 0 y = 0 }
	}
}
division_template = {
	name = "Motor Rifle Brigade" #Mechanized

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }

		armor_Bat = { x = 1 y = 0 }

		Arty_Bat = { x = 2 y = 0 }
		Arty_Bat = { x = 2 y = 1 }
	}

	support = {
		Mech_Recce_Comp = { x = 0 y = 0 }
		SP_Arty_Battery = { x = 0 y = 1 }
		SP_AA_Battery = { x = 0 y = 2 }
		H_Engi_Comp = { x = 0 y = 3 }
	}
}
division_template = {
	name = "Special Forces"

	regiments = {
		Special_Forces = { x = 0 y = 0 }
		Special_Forces = { x = 0 y = 1 }
	}
	priority = 2
}

units = {
	division = {
		name = "Special Forces"
		location = 12333
		division_template = "Special Forces"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {
		name = "3rd Motor Rifle Division"
		location = 4549
		division_template = "Motor Rifle Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "22nd Motor Rifle Division"
		location = 5003
		division_template = "Motor Rifle Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "1st Motor Rifle Brigade"
		location = 3551
		division_template = "Motor Rifle Brigade"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "2nd Motor Rifle Brigade"
		location = 4958
		division_template = "Motor Rifle Brigade"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "3rd Motor Rifle Brigade"
		location = 7924
		division_template = "Motor Rifle Brigade"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "4th Motor Rifle Brigade"
		location = 10485
		division_template = "Motor Rifle Brigade"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons1
		amount = 6400
		producer = SOV
	}
		add_equipment_to_stockpile = {
		type = command_control_equipment2
		amount = 1000
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_0
		amount = 200
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Anti_tank_1
		amount = 510
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1
		amount = 255
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_1
		amount = 325
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = mbt_hull_1
		variant_name = "T-72B"
		amount = 690
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = apc_hull_1
		amount = 170
		producer = SOV
		variant_name = "BRDM-2"
	}
	add_equipment_to_stockpile = {
		type = ifv_hull_0
		variant_name = "BMP-1"
		amount = 660 #+60 BRM-1
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = ifv_hull_0
		variant_name = "BMP-2"
		amount = 430
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "BTR-60"
		amount = 120
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "BTR-70"
		amount = 300
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "BTR-80"
		amount = 450
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "2S1 Gvozdika"
		amount = 40
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = artillery_0 #D-30
		amount = 350
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = artillery_0 #2A36 Giatsint-B
		amount = 6
		producer = SOV
		#version_name = "2A36 Giatsint-B"
	}
	add_equipment_to_stockpile = {
		type = artillery_1 #2A65
		amount = 6
		producer = SOV
		#version_name = "2A65"
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "BM-27 Uragan"
		amount = 18
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "BM-21 Grad"
		amount = 56
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = spaa_hull_0
		amount = 40
		producer = SOV
		variant_name = "9K33 Osa"
	}
	add_equipment_to_stockpile = {
		type = spaa_hull_1
		amount = 13
		producer = SOV
		variant_name = "SA-13 Strela-10"
	}

	#Helicopters

	add_equipment_to_stockpile = {
		type = attack_helicopter_hull_0
		amount = 10
		producer = SOV
		variant_name = "Mil Mi-24"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Mil Mi-8
		amount = 8
		producer = SOV
	}
}