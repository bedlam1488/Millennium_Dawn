units = {
	fleet = {
		name = "Western Fleet"
		naval_base = 9833
		task_force = {
			name = "Western Fleet 2"
			location = 9833
			ship = { name = "Istanbul" definition = frigate start_experience_factor = 1.00 equipment = { frigate_hull_3 = { amount = 1 owner = TUR creator = TUR version_name = "Istanbul Class" } } }
		}
	}
}