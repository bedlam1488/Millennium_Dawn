units = {
	fleet = {
		name = "Gamal Abdel Nasser"
		naval_base = 4076
		task_force = {
			name = "Gamal Abdel Nasser"
			location = 4076
			ship = { name = "Gamal Abdel Nasser" definition = helicopter_operator start_experience_factor = 0.65 equipment = { helicopter_operator_hull_2 = { amount = 1 owner = EGY creator = FRA version_name = "Charles de Gaulle Class 2" } } }
		}
	}
}