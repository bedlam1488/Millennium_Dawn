﻿division_template = {
	name = "Piechota" #territorial brigade

	division_names_group = POL_ARM_02

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
	}
	support = {
		Arty_Battery = { x = 0 y = 0 }
	}
}
division_template = {
	name = "Brygada P.Gorskiej" #Mountaineer brigade

	division_names_group = POL_ARM_07

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
	}

}
division_template = {
	name = "Dywizja Zmechanizowana" #Armored Cavalry Division

	division_names_group = POL_ARM_01

	regiments = {
		Arm_Inf_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
		armor_Bat = { x = 0 y = 2 }
		Mech_Inf_Bat = { x = 1 y = 0 }
		Mech_Inf_Bat = { x = 1 y = 1 }
		armor_Bat = { x = 1 y = 2 }
		SP_Arty_Bat = { x = 2 y = 0 }
		SP_AA_Bat = { x = 2 y = 1 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		Mech_Recce_Comp = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Dywizja Kawalerii Pancernej" #Mechanized Division

	division_names_group = POL_ARM_03

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		armor_Bat = { x = 0 y = 1 }
		Mech_Inf_Bat = { x = 0 y = 2 }
		SP_Arty_Bat = { x = 0 y = 3 }
		armor_Bat = { x = 1 y = 0 }
		armor_Bat = { x = 1 y = 1 }
		Mech_Inf_Bat = { x = 1 y = 2 }
		SP_Arty_Bat = { x = 2 y = 0 }
		SP_AA_Bat = { x = 2 y = 1 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		Mech_Recce_Comp = { x = 0 y = 1 }
	}

	priority = 2
}
division_template = {
	name = "Strzelcy Podhalanscy" #Mechanized Brigade

	division_names_group = POL_ARM_05

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
		Arm_Inf_Bat = { x = 0 y = 3 }
		SP_Arty_Bat = { x = 0 y = 4 }
		SP_AA_Bat = { x = 1 y = 0 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		Mech_Recce_Comp = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Kawaleria Powietrzna"

	division_names_group = POL_ARM_04

	regiments = {
		L_Air_assault_Bat = { x = 0 y = 0 }
		L_Air_assault_Bat = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Pulk Rozpoznawczy"

	division_names_group = POL_ARM_09

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Komandosi"

	division_names_group = POL_ARM_08

	regiments = {
		Special_Forces = { x = 0 y = 0 }
	}

	priority = 2
}
division_template = {
	name = "Coastal Brigade"

	division_names_group = POL_ARM_10

	regiments = {
		Arm_Marine_Bat = { x = 0 y = 0 }
		Arm_Marine_Bat = { x = 0 y = 1 }
		Arm_Marine_Bat = { x = 0 y = 2 }
		SP_Arty_Bat = { x = 0 y = 3 }
		SP_AA_Bat = { x = 0 y = 4 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		Mech_Recce_Comp = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Airborne Brigade"

	division_names_group = POL_ARM_06

	regiments = {
		L_Air_Inf_Bat = { x = 0 y = 0 }
		L_Air_Inf_Bat = { x = 0 y = 1 }
		L_Air_Inf_Bat = { x = 0 y = 2 }
	}
}

#This is technically OOB from 1999 not 2000. in 1999 reform ne number of military disctricts decreased from 4 to 2. ~~ Salamin
units = {
	division = {
		#name = "2 Pomorska Dywizja Zmechanizowana"
		division_name = {
			is_name_ordered = yes
			name_order = 3
		}

		officer = {
			name =	"Waldemar Skrzypczak"

		portraits = {
		 	army = {
		 		large = "GFX_waldemar_skrypczak_large"
		 		small = "GFX_waldemar_skrypczak_small"
		 	}
		}
	}

		location = 11260		#Szczecinek
		division_template = "Dywizja Zmechanizowana"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		#name = "12 Szczecinska Dywizja Zmechanizowana"
		division_name = {
			is_name_ordered = yes
			name_order = 14
		}

		officer = {
			name =	"Mirosław Różański"

		portraits = {
		 	army = {
		 		large = "GFX_miroslaw_wozanski_large"
				small = "GFX_miroslaw_wozanski_small"
		 	}
		}
	}

		location = 6282		#Szczecin
		division_template = "Dywizja Zmechanizowana"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		#name = "16 Pomorska Dywizja Zmechanizowana"
		division_name = {
			is_name_ordered = yes
			name_order = 17
		}

		officer = {
			name = "Wiesław Michnowicz"

		portraits = {
		 	army = {
		 		large = "GFX_wieslaw_michnowicz_large"
		 		small = "GFX_wieslaw_michnowicz_small"
		 	}
		}
	}

		location = 3380		#Elbl�g
		division_template = "Dywizja Zmechanizowana"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		#name = "8 Baltycka Dywizja Obrony Wybrzeza"
		division_name = {
			is_name_ordered = yes
			name_order = 10
		}

		officer = {
			name = "Zbigniew Glowienka"

		portraits = {
		 	army = {
		 		large = "GFX_zbigniew_glowienka_large"
		 		small = "GFX_zbigniew_glowienka_small"
		 	}
		}
	}

		location = 11372		#S�upsk/Koszalin
		division_template = "Dywizja Zmechanizowana"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "1 Gdanska Brygada Obrony Terytorialnej" #brygaga - 50%-33% str.

		officer = {
			name = "Wieslaw Kukula"

		portraits = {
		 	army = {
		 		large = "GFX_wieslaw_kukula_large"
		 		small = "GFX_wieslaw_kukula_small"
		 	}
		}
	}

		location = 362		#Danzig
		division_template = "Piechota"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		#name = "4 Lubuska Dywizja Zmechanizowana"
		division_name = {
			is_name_ordered = yes
			name_order = 6
		}

		officer = {
			name = "Jarosław Mika"

		portraits = {
		 	army = {
		 		large = "GFX_jaroslaw_mika_large"
		 		small = "GFX_jaroslaw_mika_small"
		 	}
		}
	}
		location = 243		#Mi�dzyrzecz
		division_template = "Dywizja Zmechanizowana"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		#name = "5 Kresowa Dywizja Zmechanizowana"
		division_name = {
			is_name_ordered = yes
			name_order = 7
		}

		officer = {
			name = "Zbigniew Smok"

		portraits = {
			army = {
		 		large = "GFX_zbigniew_smok_large"
		 		small = "GFX_zbigniew_smok_small"
			}
		}
	}
		location = 3572		#Gubin
		division_template = "Dywizja Zmechanizowana"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		#name = "10 Sudecka Dywizja Zmechanizowana"
		division_name = {
			is_name_ordered = yes
			name_order = 11
		}

		officer = {
			name = "Kazimierz Gilarski"

		portraits = {
		 	army = {
		 		large = "GFX_kazimierz_gilarski_large"
		 		small = "GFX_kazimierz_gilarski_small"
		 	}
		}
	}

		location = 9511		#Opole
		division_template = "Dywizja Zmechanizowana"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		#name = "11 Lubuska Dywizja Kawalerii Pancernej"
		division_name = {
			is_name_ordered = yes
			name_order = 8
		}

		officer = {
			name = "Stanisław Nowakowicz"

		portraits = {
		 	army = {
		 		large = "GFX_stanislaw_nowakowicz_large"
		 		small = "GFX_stanislaw_nowakowicz_small"
		 	}
		}
	}
		location = 3438		#Zagan
		division_template = "Dywizja Kawalerii Pancernej"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		#name = "1 Warszawska Dywizja Zmechanizowana"
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}

		officer = {
			name = "Marek Sokolowski"

		portraits = {
		 	army = {
		 		large = "GFX_marek_sokolowski_large"
		 		small = "GFX_marek_sokolowski_small"
		 	}
		}
	}

		location = 524		#Legionowo
		division_template = "Dywizja Zmechanizowana"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		#name = "15 Warminsko-Mazurska Dywizja Zmechanizowana"
		division_name = {
			is_name_ordered = yes
			name_order = 16
		}

		officer = {
			name = "Rajmund Andrzejczak"

		portraits = {
		 	army = {
		 		large = "GFX_rajmund_andrzejczak_large"
		 		small = "GFX_rajmund_andrzejczak_small"
		 	}
		}
	}

		location = 6375		#Olsztyn
		division_template = "Dywizja Zmechanizowana"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		#name = "25 Dywizja Kawalerii Powietrznej"
		division_name = {
			is_name_ordered = yes
			name_order = 3
		}

		officer = {
			name = "Mieczyslaw Bieniek"

		portraits = {
		 	army = {
		 		large = "GFX_mieczyslaw_bieniek_large"
		 		small = "GFX_mieczyslaw_bieniek_small"
		 	}
		}
	}

		location = 9508		#Lodz
		division_template = "Kawaleria Powietrzna"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		#name = "21 Brygada Strzelcow Podhalanskich"
		division_name = {
			is_name_ordered = yes
			name_order = 9
		}

		officer = {
			name = "Jerzy Gut"

		portraits = {
		 	army = {
		 		large = "GFX_jerzy_gut_large"
		 		small = "GFX_jerzy_gut_small"
		 	}
		}
	}

		location = 6499		#Rzeszow
		division_template = "Strzelcy Podhalanscy"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		#name = "6 Brygada Desantowo-Szturmowa"
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}

		officer = {
			name = "Tadeusz Mikutel"

		portraits = {
		 	army = {
		 		large = "GFX_tadeusz_mikutel_large"
		 		small = "GFX_tadeusz_mikutel_small"
		 	}
		}
	}

		location = 9427		#Krakow
		division_template = "Airborne Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		#name = "22 Karpacka Brygada Piechoty Gorskiej"
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}

		officer = {
			name = "Michal Strzelecki"

		portraits = {
		 	army = {
				large = "GFX_michal_strzelecki_large"
		 		small = "GFX_michal_strzelecki_small"
		 	}
		}
	}

		location = 6512		#Nysa
		division_template = "Brygada P.Gorskiej"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		#name = "1 Pulk Specjalny Komandosow"
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}

		officer = {
			name = "Piotr Patalong"

		portraits = {
		 	army = {
		 		large = "GFX_piotr_patalong_large"
		 		small = "GFX_piotr_patalong_small"
		 	}
		}
	}

		location = 3544		#Warsaw
		division_template = "Komandosi"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		#name = "2 Hrubieszowski Pulk Rozpoznawczy"
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}

		officer = {
			name = "Stanisław Czosnek"

		portraits = {
		 	army = {
		 		large = "GFX_stanislaw_czosnek"
		 		small = "GFX_stanislaw_czosnek_small"
		 	}
		}
	}

		location = 6580		#Hrubiesz�w
		division_template = "Pulk Rozpoznawczy"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		#name = "Formoza Gdansk"
		division_name = {
			is_name_ordered = yes
			name_order = 3
		}

		officer = {
			name = "Adam Joks"

		portraits = {
		 	army = {
		 		large = "GFX_adam_joks"
		 		small = "GFX_adam_joks_small"
		 	}
		}
	}

		location = 362		#Danzig
		division_template = "Coastal Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons3 #Kbz Wz.96
		amount = 8000
	}
	add_equipment_to_stockpile = {
		type = infantry_weapons2 #Kbz Wz.88
		amount = 20000
	}
	add_equipment_to_stockpile = {
		type = infantry_weapons1 #AKM
		amount = 10000
	}
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_0 #SPG-9
		amount = 420
	}
	add_equipment_to_stockpile = {
		type = Anti_tank_1 #RPG-76 Komar
		amount = 3000
	}
	add_equipment_to_stockpile = {
		type = command_control_equipment2 #C4ISTAR
		amount = 2600
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_2 #Grom
		amount = 700
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_0
		amount = 500
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = apc_hull_1
		amount = 510
		producer = SOV
		variant_name = "BRDM-2"
	}
	add_equipment_to_stockpile = {
		type = ifv_hull_0
		variant_name = "BMP-1"
		amount = 1405
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = artillery_0 #M-30
		amount = 277
		producer = POL
	}
	add_equipment_to_stockpile = {
		type = artillery_0 #D-44
		amount = 711
		producer = POL
		variant_name = "D-44"
	}
	add_equipment_to_stockpile = {
		type = artillery_1 #ML-20 # oh boi its ww2 artillery, but modernised to 1985 standard? at least from my knowledge ~~Salamin
		amount = 135
		producer = POL
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "2S1 Gvozdika"
		amount = 541
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "2S7 Pion"
		amount = 8
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = spart_hull_1
		variant_name = "Dana"
		amount = 111
		producer = CZE
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "BM-21 Grad"
		amount = 228
		producer = POL
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "RM-70"
		amount = 30
		producer = CZE
	}
	add_equipment_to_stockpile = {
		type = spaa_hull_0
		amount = 395
		producer = POL
		variant_name = "ZSU-23-2 Shilka"
	}
	add_equipment_to_stockpile = {
		type = spaa_hull_0
		amount = 70
		producer = SOV
		variant_name = "ZSU-23-4 Shilka"
	}
	add_equipment_to_stockpile = {
		type = spaa_hull_0
		amount = 406
		producer = SOV
		variant_name = "S-60"
	}
	add_equipment_to_stockpile = {
		type = spaa_hull_0
		amount = 115
		producer = SOV
		variant_name = "SA-6 Gainful"
	}
	add_equipment_to_stockpile = {
		type = spaa_hull_0
		amount = 64
		producer = SOV
		variant_name = "9K33 Osa"
	}
	add_equipment_to_stockpile = {
		type = spaa_hull_0
		amount = 168
		producer = SOV
		variant_name = "SA-9 Gaskin"
	}
	add_equipment_to_stockpile = {
		type = spaa_hull_1
		amount = 4
		producer = SOV
		variant_name = "SA-13 Strela-10"
	}

	#Helicopters
	add_equipment_to_stockpile = {
		type = attack_helicopter_hull_0
		amount = 46
		producer = SOV
		variant_name = "Mil Mi-24"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Mil Mi-8
		amount = 45
		producer = SOV
	}
}