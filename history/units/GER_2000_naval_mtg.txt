﻿units = {

	### Naval OOB ###
	fleet = {
		name = "Ubootsflottille"
		naval_base = 6389
		task_force = {
			name = "1. Ubootgeschwader"
			location = 6389
			ship = { name = "U-11" definition = attack_submarine start_experience_factor = 0.65 equipment = { attack_submarine_hull_1 = { amount = 1 owner = GER version_name = "Type 205 Class" } } }
			ship = { name = "U-12" definition = attack_submarine start_experience_factor = 0.65 equipment = { attack_submarine_hull_1 = { amount = 1 owner = GER version_name = "Type 205 Class" } } }
			ship = { name = "U-30" definition = attack_submarine start_experience_factor = 0.65 equipment = { attack_submarine_hull_1 = { amount = 1 owner = GER version_name = "Type 206 Class" } } }
			ship = { name = "U-23" definition = attack_submarine start_experience_factor = 0.65 equipment = { attack_submarine_hull_1 = { amount = 1 owner = GER version_name = "Type 206 Class" } } }
		}
		task_force = {
			name = "3. Ubootgeschwader"
			location = 6389
			ship = { name = "U-15" definition = attack_submarine start_experience_factor = 0.65 equipment = { attack_submarine_hull_1 = { amount = 1 owner = GER version_name = "Type 206 Class" } } }
			ship = { name = "U-16" definition = attack_submarine start_experience_factor = 0.65 equipment = { attack_submarine_hull_1 = { amount = 1 owner = GER version_name = "Type 206 Class" } } }
			ship = { name = "U-17" definition = attack_submarine start_experience_factor = 0.65 equipment = { attack_submarine_hull_1 = { amount = 1 owner = GER version_name = "Type 206 Class" } } }
			ship = { name = "U-18" definition = attack_submarine start_experience_factor = 0.65 equipment = { attack_submarine_hull_1 = { amount = 1 owner = GER version_name = "Type 206 Class" } } }
			ship = { name = "U-22" definition = attack_submarine start_experience_factor = 0.65 equipment = { attack_submarine_hull_1 = { amount = 1 owner = GER version_name = "Type 206 Class" } } }
			ship = { name = "U-24" definition = attack_submarine start_experience_factor = 0.65 equipment = { attack_submarine_hull_1 = { amount = 1 owner = GER version_name = "Type 206 Class" } } }
			ship = { name = "U-25" definition = attack_submarine start_experience_factor = 0.65 equipment = { attack_submarine_hull_1 = { amount = 1 owner = GER version_name = "Type 206 Class" } } }
			ship = { name = "U-26" definition = attack_submarine start_experience_factor = 0.65 equipment = { attack_submarine_hull_1 = { amount = 1 owner = GER version_name = "Type 206 Class" } } }
			ship = { name = "U-28" definition = attack_submarine start_experience_factor = 0.65 equipment = { attack_submarine_hull_1 = { amount = 1 owner = GER version_name = "Type 206 Class" } } }
			ship = { name = "U-29" definition = attack_submarine start_experience_factor = 0.65 equipment = { attack_submarine_hull_1 = { amount = 1 owner = GER version_name = "Type 206 Class" } } }
		}
	}
	fleet = {
		name = "Zerstörerflottille"
		naval_base = 241
		task_force = {
			name = "2. Fregattengeschwader"
			location = 241
			ship = { name = "Brandenburg" definition = frigate start_experience_factor = 0.65 equipment = { frigate_hull_3 = { amount = 1 owner = GER version_name = "Brandenburg Class" } } }
			ship = { name = "Schleswig-Holstein" definition = frigate start_experience_factor = 0.65 equipment = { frigate_hull_3 = { amount = 1 owner = GER version_name = "Brandenburg Class" } } }
			ship = { name = "Bayern" definition = frigate start_experience_factor = 0.65 equipment = { frigate_hull_3 = { amount = 1 owner = GER version_name = "Brandenburg Class" } } }
			ship = { name = "Mecklenburg-Vorpommern" definition = frigate start_experience_factor = 0.65 equipment = { frigate_hull_3 = { amount = 1 owner = GER version_name = "Brandenburg Class" } } }
		}
		task_force = {
			name = "4. Fregattengeschwader"
			location = 241
			ship = { name = "Bremen" definition = frigate start_experience_factor = 0.65 equipment = { frigate_hull_2 = { amount = 1 owner = GER version_name = "Bremen Class" } } }
			ship = { name = "Niedersachsen" definition = frigate start_experience_factor = 0.65 equipment = { frigate_hull_2 = { amount = 1 owner = GER version_name = "Bremen Class" } } }
			ship = { name = "Rheinland-Pfalz" definition = frigate start_experience_factor = 0.65 equipment = { frigate_hull_2 = { amount = 1 owner = GER version_name = "Bremen Class" } } }
			ship = { name = "Emden" definition = frigate start_experience_factor = 0.65 equipment = { frigate_hull_2 = { amount = 1 owner = GER version_name = "Bremen Class" } } }
			ship = { name = "Köln" definition = frigate start_experience_factor = 0.65 equipment = { frigate_hull_2 = { amount = 1 owner = GER version_name = "Bremen Class" } } }
			ship = { name = "Karlsruhe" definition = frigate start_experience_factor = 0.65 equipment = { frigate_hull_2 = { amount = 1 owner = GER version_name = "Bremen Class" } } }
			ship = { name = "Augsburg" definition = frigate start_experience_factor = 0.65 equipment = { frigate_hull_2 = { amount = 1 owner = GER version_name = "Bremen Class" } } }
			ship = { name = "Lübeck" definition = frigate start_experience_factor = 0.65 equipment = { frigate_hull_2 = { amount = 1 owner = GER version_name = "Bremen Class" } } }
		}
		task_force = {
			name = "1. Zerstörergeschwader"
			location = 6389
			ship = { name = "Lütjens" definition = destroyer start_experience_factor = 0.65 equipment = { destroyer_hull_1 = { amount = 1 owner = GER version_name = "Lütjens Class" } } }
			ship = { name = "Mölders" definition = destroyer start_experience_factor = 0.65 equipment = { destroyer_hull_1 = { amount = 1 owner = GER version_name = "Lütjens Class" } } }
			ship = { name = "Rommel" definition = destroyer start_experience_factor = 0.65 equipment = { destroyer_hull_1 = { amount = 1 owner = GER version_name = "Lütjens Class" } } }
		}
	}
	fleet = {
		name = "Flottille der Minenstreitkräfte"
		naval_base = 6389
		task_force = {
			name = "5. Minensuchgeschwader"
			location = 6389
			ship = { name = "Frankenthal (M1066)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = GER version_name = "Frankenthal Class" } } }
			ship = { name = "Weiden (M1060)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = GER version_name = "Frankenthal Class" } } }
			ship = { name = "Rottweil (M1061)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = GER version_name = "Frankenthal Class" } } }
			ship = { name = "Bad Bevensen (M1063)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = GER version_name = "Frankenthal Class" } } }
			ship = { name = "Bad Rappenau (M1067)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = GER version_name = "Frankenthal Class" } } }
			ship = { name = "Gromitz (M1064)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = GER version_name = "Frankenthal Class" } } }
			ship = { name = "Datteln (M1068)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = GER version_name = "Frankenthal Class" } } }
			ship = { name = "Dillingen (M1065)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = GER version_name = "Frankenthal Class" } } }
			ship = { name = "Homburg (M1069)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = GER version_name = "Frankenthal Class" } } }
			ship = { name = "Sulzbach-Rosenberg (M1062)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = GER version_name = "Frankenthal Class" } } }
			ship = { name = "Fulda (M1058)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = GER version_name = "Frankenthal Class" } } }
			ship = { name = "Weilheim (M1059)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = GER version_name = "Frankenthal Class" } } }
		}
		task_force = {
			name = "3. Minensuchgeschwader"
			location = 6389
			ship = { name = "Ensdorf (M1094)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = GER version_name = "Frankenthal Class" } } }
			ship = { name = "Auerbach (M1093)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = GER version_name = "Frankenthal Class" } } }
			ship = { name = "Hameln (M1092)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = GER version_name = "Frankenthal Class" } } }
			ship = { name = "Pegnitz (M1090)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = GER version_name = "Frankenthal Class" } } }
			ship = { name = "Siegburg (M1098)" definition = corvette start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = GER version_name = "Frankenthal Class" } } }
		}
	}
}
