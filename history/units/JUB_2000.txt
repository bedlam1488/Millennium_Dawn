﻿division_template = {
	name = "Darod Militia"

	regiments = {
		Militia_Bat = { x = 0 y = 0 }
		Militia_Bat = { x = 0 y = 1 }
		Militia_Bat = { x = 0 y = 2 }
		Militia_Bat = { x = 0 y = 3 }
	}
}

units = {
	division = {
		name = "1st SPM Brigade"
		location = 12941
		division_template = "Darod Militia"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5
	}
	division = {
		name = "2nd SPM Brigade"
		location = 12941
		division_template = "Darod Militia"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5
	}
	division = {
		name = "3rd SPM Brigade"
		location = 11079
		division_template = "Darod Militia"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5
	}
	division = {
		name = "4th SPM Brigade"
		location = 14706
		division_template = "Darod Militia"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5
	}
	division = {
		name = "5th SPM Brigade"
		location = 13877
		division_template = "Darod Militia"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons
		amount = 2500
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Anti_tank_0
		amount = 90
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_0
		amount = 60
		producer = SOV
	}
}