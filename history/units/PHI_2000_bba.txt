﻿instant_effect = {
	
	add_equipment_to_stockpile = {
		variant_name = "F-5 Freedom Fighter"
		type = small_plane_airframe_1
		amount = 11
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "C-130 Hercules"
		type = large_plane_air_transport_airframe_1
		amount = 12
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = medium_plane_maritime_patrol_airframe_1
		variant_name = "BN-2 Defender"
		amount = 6
		producer = ENG
	}

	#Helicopters

	add_equipment_to_stockpile = {
		type = transport_helicopter1 #UH-1, B205
		amount = 201
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Do-105
		amount = 15
		producer = GER
	}
}