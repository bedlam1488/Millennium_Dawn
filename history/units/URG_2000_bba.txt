﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "A-37 Dragonfly"
		amount = 10
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "IA 58 Pucará"
		amount = 5
		producer = ARG
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "Pilatus PC-7"
		amount = 5
		producer = SWI
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "C-130 Hercules"
		amount = 3
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "C-212 Aviocar"
		amount = 3
		producer = SPR
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1			#Huey
		amount = 6
		producer = USA
	}
}