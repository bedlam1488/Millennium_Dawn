instant_effect = {

	add_equipment_to_stockpile = {
		type = small_plane_airframe_1
		amount = 23
		producer = SWE
		variant_name = "JA-35 Draken"
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		amount = 2
		producer = ENG
		variant_name = "Short SC.7 Skyvan"
	}

}