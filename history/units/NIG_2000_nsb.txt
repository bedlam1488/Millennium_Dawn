﻿division_template = {
	name = "Mechanized Brigade"

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		L_arm_Bat = { x = 0 y = 2 }
	}
	support = {
		L_Engi_Comp = { x = 0 y = 0 }
		SP_Arty_Battery = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Mechanized Division"

	regiments = {
		Arm_Inf_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
		Mech_Inf_Bat = { x = 0 y = 2 }

		armor_Bat = { x = 1 y = 0 }
		L_arm_Bat = { x = 1 y = 1 }
		L_arm_Bat = { x = 1 y = 2 }

		Mot_Inf_Bat = { x = 2 y = 0 }
		Mot_Inf_Bat = { x = 2 y = 1 }
		Mot_Inf_Bat = { x = 2 y = 2 }

		SP_Arty_Bat = { x = 3 y = 0 }
	}
	support = {
		L_Engi_Comp = { x = 0 y = 0 }
		SP_AA_Battery = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Motorised Division"

	regiments = {
		Mot_Inf_Bat = { x = 0 y = 0 }
		Mot_Inf_Bat = { x = 0 y = 1 }
		Mot_Inf_Bat = { x = 0 y = 2 }

		Mot_Inf_Bat = { x = 1 y = 0 }
		Mot_Inf_Bat = { x = 1 y = 1 }
		Mot_Inf_Bat = { x = 1 y = 2 }

		Mot_Inf_Bat = { x = 2 y = 0 }
		Mot_Inf_Bat = { x = 2 y = 1 }
		Mot_Inf_Bat = { x = 2 y = 2 }
		Mot_Inf_Bat = { x = 2 y = 2 }

		Arty_Bat = { x = 3 y = 0 }
		Arty_Bat = { x = 3 y = 1 }
		L_Inf_Bat = { x = 3 y = 2 }
	}
}

division_template = {
	name = "Armoured Division"

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		armor_Bat = { x = 0 y = 1 }
		armor_Bat = { x = 0 y = 2 }
		armor_Bat = { x = 0 y = 3 }

		Arm_Inf_Bat = { x = 1 y = 0 }
		Arm_Inf_Bat = { x = 1 y = 1 }
		L_arm_Bat = { x = 1 y = 2 }
		SP_Arty_Bat = { x = 1 y = 2 }

		L_Inf_Bat = { x = 2 y = 0 }
	}
	support = {
		SP_AA_Battery = { x = 0 y = 0 }
	}
}

division_template = {
	name = "Armoured Brigade"

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		armor_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
	}
	support = {
		armor_Recce_Comp = { x = 0 y = 0 }
		L_Engi_Comp = { x = 0 y = 1 }
		SP_Arty_Battery = { x = 0 y = 2 }
		SP_AA_Battery = { x = 0 y = 3 }
	}
}

division_template = {
	name = "Motorised Brigade"

	regiments = {
		Mot_Inf_Bat = { x = 0 y = 0 }
		Mot_Inf_Bat = { x = 0 y = 1 }
		Mot_Inf_Bat = { x = 0 y = 2 }

		Arty_Bat = { x = 1 y = 0 }
	}
	support = {
		Mot_Recce_Comp = { x = 0 y = 0 }
		L_Engi_Comp = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Airborne Brigade"

	regiments = {
		Special_Forces = { x = 0 y = 0 }
		L_Air_Inf_Bat = { x = 0 y = 1 }
		L_Air_Inf_Bat = { x = 0 y = 2 }
	}
	support = {
		L_Recce_Comp = { x = 0 y = 0 }
		L_Engi_Comp = { x = 0 y = 1 }
		Arty_Battery = { x = 0 y = 2 }
	}
}

division_template = {
	name = "Amphibious Brigade"

	regiments = {
		Mot_Marine_Bat = { x = 0 y = 0 }
		Mot_Marine_Bat = { x = 0 y = 1 }
		Mot_Marine_Bat = { x = 0 y = 2 }

		Arty_Bat = { x = 1 y = 0 }
	}
	support = {
		L_Recce_Comp = { x = 0 y = 0 }
	}

	priority = 2
}

division_template = {
	name = "Christian Militias"
	is_locked = yes

	regiments = {
		Militia_Bat = { x = 0 y = 0 }
		Militia_Bat = { x = 0 y = 0 }
	}
	priority = 0
}

division_template = {
	name = "Islamic Militias"
	is_locked = yes

	regiments = {
		Militia_Bat = { x = 0 y = 0 }
		Militia_Bat = { x = 0 y = 0 }
	}
	priority = 0
}

units = {
	division = {
		name = "1st Division"
		location = 5028		#Nguru
		division_template = "Mechanized Division"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}

	division = {
		name = "2nd Division"
		location = 12731
		division_template = "Mechanized Division"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}

	division = {
		name = "3rd Armoured Division"
		location = 4997		#Maiduguri
		division_template = "Armoured Division"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}

	#82nd Division
	division = {
		name = "82nd Division - 1st Bde"
		location = 2050		#
		division_template = "Motorised Brigade"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "82nd Division - Airborne Bde"
		location = 2050		#
		division_template = "Airborne Brigade"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {
		name = "81st Amphibious Brigade"
		location = 8034		#?
		division_template = "Amphibious Brigade"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}

	division = {
		name = "Kaduna Militias"
		location = 1969 #Kaduna
		division_template = "Islamic Militias"
		start_equipment_factor = 0.45
		start_experience_factor = 0.05
	}

	division = {
		name = "Maiduguri Militias"
		location = 4997 #Kaduna
		division_template = "Islamic Militias"
		start_equipment_factor = 0.45
		start_experience_factor = 0.05
	}

	division = {
		name = "Abuja Militias"
		location = 2025 #Abuja
		division_template = "Christian Militias"
		start_equipment_factor = 0.45
		start_experience_factor = 0.05
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons1			#M16
		amount = 2000
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons			#AKM local
		amount = 3000
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons1			#SIG SG 540
		amount = 1000
		producer = SWI
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons1			#NR1
		amount = 3000
	}

	add_equipment_to_stockpile = {
		type = command_control_equipment
		amount = 1000
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1			#Swingfire
		amount = 225
		producer = ENG
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_1			#
		amount = 400
		producer = SOV
	}

	#Vehicles
	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "T-55"
		amount = 75
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "Vickers MBT Mk 3"
		amount = 150
		producer = ENG
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_1
		amount = 140
		producer = ENG
		variant_name = "FV101 Scorpion"
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 55
		producer = ENG
		variant_name = "Ferret"
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 180
		producer = FRA
		variant_name = "Panhard AML 90"
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_1
		amount = 75
		producer = BRA
		variant_name = "EE-9 Cascavel"
	}

	add_equipment_to_stockpile = {
		type = ifv_hull_0
		variant_name = "Saurer 4K"
		amount = 300
		producer = AUS
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "MOWAG Piranha"
		amount = 70
		producer = SWI
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "EE-11 Urutu"
		amount = 40
		producer = BRA
	}

	add_equipment_to_stockpile = {
		type = util_vehicle_3			#VBL
		amount = 72
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_0		#Pinzgauer
		amount = 800
		producer = AUS
	}

	#Artillery
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "Palmaria"
		amount = 27
		producer = ITA
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "APR-40"
		amount = 30
		producer = ROM
	}

	add_equipment_to_stockpile = {
		type = artillery_0			#D-30
		amount = 90
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = artillery_0			#M-56
		amount = 100
		producer = SER
	}
	add_equipment_to_stockpile = {
		type = artillery_1			#FH77
		amount = 24
		producer = SWE
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_0				#Blowpipe
		amount = 200		#48
		producer = ENG
	}
	add_equipment_to_stockpile = {
		type = spaa_hull_0
		amount = 16
		producer = GER
		variant_name = "Roland II"
	}

	#Helicopters
	add_equipment_to_stockpile = {
		type = attack_helicopter_hull_0
		amount = 6
		producer = FRA
		variant_name = "SA-342 Gazelle"
	}
	add_equipment_to_stockpile = {
		type = attack_helicopter1
		amount = 15
		producer = SPR
		variant_name = "MBB Bo 105"
	}
}