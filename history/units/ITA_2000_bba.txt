﻿instant_effect = {
	add_equipment_to_stockpile = {
		variant_name = "AV-8B Harrier II"
		type = cv_small_plane_strike_airframe_1
		amount = 18
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "Panavia Tornado IDS"
		type = medium_plane_airframe_2
		amount = 92
		producer = ITA
	}
	add_equipment_to_stockpile = {
		variant_name = "P-3 Orion"
		type = large_plane_maritime_patrol_airframe_1
		amount = 18
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "Aeritalia F-104S" #and G
		type = small_plane_airframe_1
		amount = 60
		producer = ITA
	}
	add_equipment_to_stockpile = {
		variant_name = "Aeritalia F-104S ASA"
		type = small_plane_airframe_1
		amount = 98
		producer = ITA
	}
	add_equipment_to_stockpile = {
		variant_name = "Aeritalia F-104S ASA-M"
		type = small_plane_airframe_1
		amount = 64
		producer = ITA
	}
	add_equipment_to_stockpile = {
		variant_name = "Panavia Tornado ADV"
		type = medium_plane_fighter_airframe_2
		amount = 24
		producer = GER
	}
	add_equipment_to_stockpile = {
		variant_name = "AMX International"
		type = small_plane_cas_airframe_2
		amount = 104
		producer = ITA
	}
	add_equipment_to_stockpile = {
		variant_name = "Aermacchi MB-326"
		type = small_plane_strike_airframe_1
		amount = 26
		producer = ITA
	}
	add_equipment_to_stockpile = {
		variant_name = "Aermacchi MB-339"
		type = small_plane_strike_airframe_1
		amount = 126
		producer = ITA
	}
	add_equipment_to_stockpile = {
		variant_name = "C-130J Super Hercules"
		type = large_plane_air_transport_airframe_1
		amount = 19
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "Aeritalia G.222"
		type = large_plane_air_transport_airframe_1
		amount = 56
		producer = ITA
	}
	add_equipment_to_stockpile = {
		variant_name = "SIAI-Marchetti SF.260"
		type = small_plane_strike_airframe_1
		amount = 45
		producer = ITA
	}
	add_equipment_to_stockpile = {
		variant_name = "C-20 Gulfstream"
		type = large_plane_air_transport_airframe_1
		amount = 2
		producer = USA
	}
}