instant_effect = {

	add_equipment_to_stockpile = {
		variant_name = "MiG-21s Bis"
		type = small_plane_strike_airframe_1
		amount = 12
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "An-26"
		type = large_plane_air_transport_airframe_1
		amount = 8
		producer = UKR
	}
}