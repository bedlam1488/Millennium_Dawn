﻿instant_effect = {

	############################################
	################STOCKPILE###################
	############################################

	################################
	### Land Based Fighters ########
	################################

	add_equipment_to_stockpile = {
		type = MR_Fighter2
		amount = 948
	}

	add_equipment_to_stockpile = {
		type = AS_Fighter3
		amount = 481
	}

	add_equipment_to_stockpile = {
		type = Strike_fighter3
		amount = 210
	}

	add_equipment_to_stockpile = {
		type = Strike_fighter2
		amount = 52
	}

	add_equipment_to_stockpile = {
		type = cas1
		amount = 212
	}

	####################
	## Transports ######
	####################

	add_equipment_to_stockpile = {
		type = transport_plane1 #C-12/C = 20/C23/C-26/C-31/C-37/C-182
		amount = 222
	}

	####################
	###Naval Aviation###
	####################
	add_equipment_to_stockpile = {
		type = AS_Fighter2 #F-14 Tomcat
		amount = 193
	}
	add_equipment_to_stockpile = {
		type = CV_MR_Fighter2 #Boeing F/A-18 Hornet = Removed 200
		amount = 571
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter1 #F-5 Freedom Fighter
		amount = 36
	}
	add_equipment_to_stockpile = {
		type = CV_L_Strike_fighter2 #AV8B
		amount = 119
	}
	add_equipment_to_stockpile = {
		type = naval_plane2 #P-3C Orion
		amount = 260
	}
	add_equipment_to_stockpile = {
		type = naval_plane1 #P-3 Orion
		amount = 112
		#version_name = "S-3 Viking"
	}
	add_equipment_to_stockpile = {
		type = transport_plane1 #C-20
		amount = 6
	}

	##########
	###USMC###
	##########
	add_equipment_to_stockpile = {
		type = CV_MR_Fighter2 #Boeing F/A-18 Hornet
		amount = 245
	}
	add_equipment_to_stockpile = {
		type = CV_L_Strike_fighter2 #AV8B = Removed 100
		amount = 19
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter1 #F-5 Freedom Fighter
		amount = 2
	}
	add_equipment_to_stockpile = {
		type = transport_plane1 #C-20
		amount = 4
	}

	##########################
	###US Strategic Command###
	##########################

	add_equipment_to_stockpile = {
		type = strategic_bomber3 #B1-B
		amount = 7
	}
	add_equipment_to_stockpile = {
		type = strategic_bomber1 #b52
		amount = 5
	}
	add_equipment_to_stockpile = {
		type = strategic_bomber4 #B2
		amount = 2
	}
	add_equipment_to_stockpile = {
		type = strategic_bomber3 #B1-B
		amount = 91
	}
	add_equipment_to_stockpile = {
		type = strategic_bomber1 #b52
		amount = 92
	}

	add_equipment_production = {
		equipment = {
			type = transport_plane3 #C-130J Super Hercules
			creator = "USA"
		}
		requested_factories = 1
		progress = 0.5
		efficiency = 50
	}
	add_equipment_production = {
		equipment = {
			type = Air_UAV1 #MQ-1 Predator
			creator = "USA"
		}
		requested_factories = 1
		progress = 0.5
		efficiency = 50
	}
	add_equipment_production = {
		equipment = {
			type = strategic_bomber4 #B2
			creator = "USA"
		}
		requested_factories = 1
		progress = 0.5
		efficiency = 50
	}
	add_equipment_production = {
		equipment = {
			type = MR_Fighter3 #F-16C
			creator = "USA"
		}
		requested_factories = 1
		progress = 0.5
		efficiency = 50
	}
	add_equipment_production = {
		equipment = {
			type = Strike_fighter3 #F-15E Strike Eagle
			creator = "USA"
		}
		requested_factories = 1
		progress = 0.5
		efficiency = 50
	}
	add_equipment_production = {
		equipment = {
			type = transport_plane4 #C-17 Globemaster III
			creator = "USA"
		}
		requested_factories = 1
		progress = 0.5
		efficiency = 50
	}
	add_equipment_production = {
		equipment = {
			type = CV_MR_Fighter3 #F/A-18E/F
			creator = "USA"
		}
		requested_factories = 1
		progress = 0.5
		efficiency = 50
	}
	add_equipment_production = {
		equipment = {
			type = CV_L_Strike_fighter2 #AV-8B Harrier II
			creator = "USA"
		}
		requested_factories = 1
		progress = 0.5
		efficiency = 50
	}
}