guiTypes = {

	containerWindowType = {
		name = "state_energy_ui_view"
		position = { x = 40 y = 335 }
		instantTextBoxType = {
			name = "state_productivity_text"
			position = { x = 0 y = 0 }
			font = "hoi_18mbs"
			format = left
			maxWidth = 84
			maxHeight = 24
			fixedsize = yes
			text = "PRODUCTIVITY"
			pdx_tooltip = "PRODUCTIVITY_STATE_DESC"
		}
		instantTextBoxType = {
			name = "state_productivity_amount"
			position = { x = 12 y = 20 }
			font = "hoi_18mbs"
			format = left
			maxWidth = 80
			maxHeight = 24
			fixedsize = yes
			text = "NUM_STATE_PRODUCTIVITY"
			pdx_tooltip = "NUM_STATE_PRODUCTIVITY_DESC"
			pdx_tooltip_delayed = "NUM_STATE_PRODUCTIVITY_DELAYED_DESC"
		}
	}

	containerWindowType = {
		name = "open_energy_gui"
		position = { x = 410 y = 10	 }
		buttonType = {
			name = "open_energy_window"
			spriteType = "GFX_energy_button"
			position = { x = 0 y = 0 }
			pdx_tooltip = "OPEN_ENERGY_BUTTON"
			pdx_tooltip_delayed = "OPEN_ENERGY_BUTTON_TT"
		}
	}

	containerWindowType = {
		name = "energy_scripted_gui"
		position = { x = 545 y = 0 }
		size = { width = 557 height = 409 }
		background = {
			name = "Background"
			spriteType ="GFX_energy_ui"
		}

		instantTextBoxType = {
			name = "header_text_title"
			position = { x = 230 y = 28 }
			font = "hoi_36header"
			text = "HEADER_TEXT_TITLE"
			maxWidth = 100
			maxHeight = 20
			format = center
		}

		# Energy Producing Building Grid
		containerWindowType = {
			name = "total_energy_production_buildings_grid"
			position = { x = 63 y = 95 }

			# Fossil Fuel Power Plant
			iconType = {
				name = "total_ffp_bg"
				spriteType = "GFX_generic_box_smallest"
				position = { x = 30	y = 2 }
			}
			instantTextBoxType = {
				name = "total_ffp_amount"
				position = { x = 38 y = 6 }
				font = "hoi_18mbs"
				text = "[?number_of_fossil_pps]"
				format = centre
				maxWidth = 55
				maxHeight = 24
				fixedsize = yes
				pdx_tooltip = "total_ffp_amount_tt"
				pdx_tooltip_delayed = "total_ffp_amount_tt_delayed"
			}
			iconType = {
				name = "total_ffp"
				spriteType = "GFX_AC_building_fossil_powerplant"
				position = { x = 0 y = 0 }
			}

			# Nuclear Reactor
			iconType = {
				name = "total_nuclear_reactor_bg"
				spriteType = "GFX_generic_box_smallest"
				position = { x = 140	y = 2 }
			}
			instantTextBoxType = {
				name = "total_nuclear_reactor_amount"
				position = { x = 148 y = 6 }
				font = "hoi_18mbs"
				text = "[?nuclear_reactors]"
				format = centre
				maxWidth = 55
				maxHeight = 24
				fixedsize = yes
				pdx_tooltip = "total_nuclear_reactor_amount_tt"
				pdx_tooltip_delayed = "total_nuclear_reactor_amount_tt_delayed"
			}
			iconType = {
				name = "total_nuclear_reactor"
				spriteType = "GFX_AC_building_nuclear"
				position = { x = 110 y = 0 }
			}

			# Renewable Energy Infrastructure
			iconType = {
				name = "total_renewable_energy_bg"
				spriteType = "GFX_generic_box_smallest"
				position = { x = 250	y = 2 }
			}
			instantTextBoxType = {
				name = "total_renewable_energy_amount"
				position = { x = 258 y = 6 }
				font = "hoi_18mbs"
				text = "[?biofuel_refineries]"
				format = centre
				maxWidth = 55
				maxHeight = 24
				fixedsize = yes
				pdx_tooltip = "total_renewable_energy_amount_tt"
				pdx_tooltip_delayed = "total_renewable_energy_amount_tt_delayed"
			}
			iconType = {
				name = "total_renewable_energy"
				spriteType = "GFX_AC_building_ref"
				position = { x = 220 y = 0 }
			}

			# Fuel Silo
			iconType = {
				name = "total_fuel_silo_bg"
				spriteType = "GFX_generic_box_smallest"
				position = { x = 360 y = 2 }
			}
			instantTextBoxType = {
				name = "total_fuel_silo_amount"
				position = { x = 368 y = 6 }
				font = "hoi_18mbs"
				text = "[?fuel_silos]"
				format = centre
				maxWidth = 55
				maxHeight = 24
				fixedsize = yes
				pdx_tooltip = "total_fuel_silo_amount_tt"
				pdx_tooltip_delayed = "total_fuel_silo_amount_tt_delayed"
			}
			iconType = {
				name = "total_fuel_silo"
				spriteType = "GFX_AC_building_fuel_silo"
				position = { x = 330 y = 0 }
			}
		}

		# Generation
		containerWindowType = {
			name = "energy_generation_values"
			position = { x = 22 y = 130 }

			instantTextBoxType = {
				name = "energy_generation_total"
				position = { x = 0 y = 0 }
				font = "hoi_24header"
				text = "TOTAL_GENERATION_ENERGY"
				maxWidth = 375
				maxHeight = 20
				format = left
				pdx_tooltip = "ENERGY_GENERATION_TOTAL_TT"
			}
		}

		# Consumption
		containerWindowType = {
			name = "energy_consumption_values"
			position = { x = 22 y = 150 }

			instantTextBoxType = {
				name = "energy_consumption_total"
				position = { x = 0 y = 0 }
				font = "hoi_24header"
				text = "TOTAL_CONSUMPTION_ENERGY"
				maxWidth = 375
				maxHeight = 20
				format = left
				pdx_tooltip = "TOTAL_CONSUMPTION_ENERGY_TT"
			}
		}

		# Total
		containerWindowType = {
			name = "misc_values"
			position = { x = 22 y = 170 }

			instantTextBoxType = {
				name = "energy_fulfillment_total"
				position = { x = 0 y = 10 }
				font = "hoi_24header"
				text = "MISSING_FULFILLMENT_VALUE"
				maxWidth = 375
				maxHeight = 20
				format = left
				pdx_tooltip = "MISSING_FULFILLMENT_VALUE_TT"
			}

			instantTextBoxType = {
				name = "net_energy_balance"
				position = { x = 0 y = 30 }
				font = "hoi_24header"
				text = "NET_ENERGY_BALANCE"
				maxWidth = 375
				maxHeight = 20
				format = left
				pdx_tooltip = "NET_ENERGY_BALANCE_TT"
			}

			instantTextBoxType = {
				name = "total_energy_misc_value_headers"
				position = { x = 0 y = 55 }
				font = "hoi_24header"
				text = "TOTAL_ENERGY_MISC_VALUE_HEADER"
				maxWidth = 375
				maxHeight = 20
				format = left
			}

			instantTextBoxType = {
				name = "total_hourly_fuel_consumption_values"
				position = { x = 0 y = 80 }
				font = "hoi_18mbs"
				text = "TOTAL_HOURLY_FUEL_CONSUMPTION_VALUES"
				maxWidth = 375
				maxHeight = 20
				format = left
				pdx_tooltip = "TOTAL_HOURLY_FUEL_CONSUMPTION_VALUES_TT"
			}

			instantTextBoxType = {
				name = "total_hourly_population_fuel_consumption_values"
				position = { x = 0 y = 100 }
				font = "hoi_18mbs"
				text = "TOTAL_HOURLY_POPULATION_FUEL_CONSUMPTION_VALUES"
				maxWidth = 375
				maxHeight = 20
				format = left
				pdx_tooltip = "TOTAL_HOURLY_POPULATION_FUEL_CONSUMPTION_VALUES_TT"
			}

			instantTextBoxType = {
				name = "total_weekly_nuclear_material_production_values"
				position = { x = 0 y = 120 }
				font = "hoi_18mbs"
				text = "TOTAL_WEEKLY_NUCLEAR_MATERIAL_PRODUCTION"
				maxWidth = 375
				maxHeight = 40
				format = left
				pdx_tooltip = "TOTAL_WEEKLY_NUCLEAR_MATERIAL_PRODUCTION_TT"
			}

			instantTextBoxType = {
				name = "stored_energy"
				position = { x = 0 y = 140 }
				font = "hoi_18mbs"
				text = "TOTAL_STORED_ENERGY"
				maxWidth = 440
				maxHeight = 40
				format = left
				pdx_tooltip = "TOTAL_STORED_ENERGY_TT"
			}
		}

		containerWindowType = {
			name = "energy_screen_buttons"
			position = { x = 12 y = 350 }

			buttonType = {
				name = "close_energy_screen_button"
				position = { x = 495 y = -340 }
				quadTextureSprite ="GFX_closebutton"
				buttonFont = "Main_14_black"
				shortcut = "ESCAPE"
				Orientation = "UPPER_RIGHT"
				clicksound = click_close
				pdx_tooltip = "CLOSE"
			}

			containerWindowType = {
				name = "refresh_energy_screen_bg"
				position = { x = 0 y = -17 }

				buttonType = {
					name = "refresh_energy_screen_bg"
					spriteType = "GFX_generic_button_wide"
					position = { x=0 y=0 }
					pdx_tooltip = "REFRESH_TAX_RATES_TOOLTIP"
				}

				instantTextBoxType = {
					name ="refresh_energy_screen_text"
					position = { x= 30 y = 0 }
					font = "hoi_18mbs"
					text = "REFRESH_TAX_RATE"
					format = centre
					maxWidth = 100
					maxHeight = 24
					fixedsize = yes

					pdx_tooltip = "REFRESH_TAX_RATES_TOOLTIP"
				}
			}

			buttonType = {
				name = "heavy_power_restrictions_button"
				position = {x=0 y=0}
				spriteType = "GFX_double_negative_power"
				orientation = "UPPER_RIGHT"
				clicksound = click_default
				pdx_tooltip = heavy_power_restrictions_button_tt
				pdx_tooltip_delayed = heavy_power_restrictions_button_delayed_tt
			}
			buttonType = {
				name = "some_power_restrictions_button"
				position = {x=44 y=0}
				spriteType = "GFX_negative_power"
				orientation = "UPPER_RIGHT"
				clicksound = click_default
				pdx_tooltip = some_power_restrictions_button_tt
				pdx_tooltip_delayed = some_power_restrictions_button_delayed_tt
			}
			buttonType = {
				name = "no_power_rules_button"
				position = {x=88 y=0}
				spriteType = "GFX_neutral_power"
				orientation = "UPPER_RIGHT"
				clicksound = click_default
				pdx_tooltip = no_power_restrictions_button_tt
				pdx_tooltip_delayed = no_power_restrictions_button_delayed_tt
			}
			buttonType = {
				name = "some_additional_consumption_button"
				position = {x=132 y=0}
				spriteType = "GFX_positive_power"
				orientation = "UPPER_RIGHT"
				clicksound = click_default
				pdx_tooltip = some_additional_consumption_button_tt
				pdx_tooltip_delayed = some_additional_consumption_button_delayed_tt
			}
			buttonType = {
				name = "heavy_additional_consumption_button"
				position = {x=176 y=0}
				spriteType = "GFX_double_positive_power"
				orientation = "UPPER_RIGHT"
				clicksound = click_default
				pdx_tooltip = heavy_additional_consumption_button_tt
				pdx_tooltip_delayed = heavy_additional_consumption_button_delayed_tt
			}
			buttonType = {
				name = "build_enrichment_facility_button"
				position = {x=392 y=0}
				spriteType = "GFX_build_enrichment_facility"
				orientation = "UPPER_RIGHT"
				clicksound = click_default
				pdx_tooltip = build_enrichment_facility_button_tt
				pdx_tooltip_delayed = build_enrichment_facility_button_delayed_tt
			}
			buttonType = {
				name = "build_battery_park_button"
				position = {x=436 y=0}
				spriteType = "GFX_battery_park_build"
				orientation = "UPPER_RIGHT"
				clicksound = click_default
				pdx_tooltip = build_battery_park_button_tt
				pdx_tooltip_delayed = build_battery_park_button_delayed_tt
			}
			buttonType = {
				name = "buy_fuel_for_money_button"
				position = {x=480 y=0}
				spriteType = "GFX_fuel_from_market"
				orientation = "UPPER_RIGHT"
				clicksound = click_default
				pdx_tooltip = buy_fuel_for_money_button_tt
				pdx_tooltip_delayed = buy_fuel_for_money_button_delayed_tt
			}
		}
	}
}