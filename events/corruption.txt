add_namespace = corruption

# Header event that then rolls/triggers the others
country_event = {
	id = corruption.0
	hidden = yes
	is_triggered_only = yes
	trigger = {
		NOT = {
			has_country_flag = auto_decline_corruption
			has_country_flag = recently_rolled_corruption_event
		}
	}

	mean_time_to_happen = {
		modifier = {
			factor = 6.5
			has_idea = paralyzing_corruption
		}
		modifier = {
			factor = 6
			has_idea = crippling_corruption
		}
		modifier = {
			factor = 4.5
			has_idea = rampant_corruption
		}
		modifier = {
			factor = 3.5
			has_idea = unrestrained_corruption
		}
		modifier = {
			factor = 2.75
			has_idea = systematic_corruption
		}
		modifier = {
			factor = 2.5
			has_idea = widespread_corruption
		}
		modifier = {
			factor = 1.75
			has_idea = medium_corruption
		}
		modifier = {
			factor = 1.5
			has_idea = modest_corruption
		}
		modifier = {
			factor = 1.25
			has_idea = slight_corruption
		}
		modifier = {
			factor = 1.0
			has_idea = negligible_corruption
		}
		modifier = {
			factor = 1.5
			OR = {
				has_idea = oligarchs
				neutrality_oligarchism_in_power_or_coalition = yes
			}
		}
	}

	immediate = {
		roll_corruption_event = yes
	}
}

country_event = {
	id = corruption.1
	title = corruption.1.t
	desc = corruption.1.d
	picture = GFX_politics_negotiations

	is_triggered_only = yes

	trigger = {
		NOT = {
			has_country_flag = auto_decline_corruption
		}
	}

	option = {
		name = corruption.1.a
		log = "[GetDateText]: [This.GetName]: corruption.1.a executed"

		add_timed_idea = {
			idea = corruption_decreased_adm_spending
			days = 200
		}

		random_list = {
			20 = {
				modifier = {
					factor = 1.5
					OR = {
						has_idea = oligarchs
						neutrality_neutral_oligarch_are_in_power = yes
					}
				}
				increase_corruption = yes
			}
			80 = {
				# nothing
			}
		}

		ai_chance = {
			base = 10
			modifier = {
				factor = 2
				OR = {
					has_idea = oligarchs
					neutrality_neutral_oligarch_are_in_power = yes
				}
			}
			modifier = {
				factor = 0
				original_tag = BOS
				has_stability < 0.40
				date < 2000.6.01
			}
		}
	}

	option = {
		name = corruption.1.b
		log = "[GetDateText]: [This.GetName]: corruption.1.b executed"
		ai_chance = {
			base = 30
			modifier = {
				factor = 1.25
				has_stability < 0.30
			}
			modifier = {
				factor = 1.5
				check_variable = { interest_rate > 8 }
			}
			modifier = {
				factor = 10
				OR = {
					has_idea = superpower
					has_idea = great_power
					has_idea = large_power
				}
			}
		}
	}
}

country_event = {
	id = corruption.2
	title = corruption.2.t
	desc = corruption.2.d
	picture = GFX_politics_negotiations

	is_triggered_only = yes

	trigger = {
		NOT = {
			has_country_flag = auto_decline_corruption
		}
	}

	option = {
		name = corruption.2.a
		log = "[GetDateText]: [This.GetName]: corruption.2.a executed"

		set_temp_variable = { temp_opinion = -3 }
		change_intelligence_community_opinion = yes

		add_timed_idea = {
			idea = corruption_decreased_police_spending
			days = 200
		}

		random_list = {
			20 = {
				modifier = {
					factor = 1.5
					OR = {
						has_idea = oligarchs
						neutrality_neutral_oligarch_are_in_power = yes
					}
				}
				increase_corruption = yes
			}
			80 = {
				# nothing
			}
		}

		ai_chance = {
			base = 10
			modifier = {
				factor = 2
				OR = {
					has_idea = oligarchs
					neutrality_neutral_oligarch_are_in_power = yes
				}
			}
			modifier = {
				factor = 0
				original_tag = BOS
				has_stability < 0.40
				date < 2000.6.01
			}
		}
	}

	option = {
		name = corruption.2.b
		log = "[GetDateText]: [This.GetName]: corruption.2.b executed"
		ai_chance = {
			base = 30
			modifier = {
				factor = 1.25
				has_stability < 0.30
			}
			modifier = {
				factor = 1.5
				check_variable = { interest_rate > 8 }
			}
			modifier = {
				factor = 10
				OR = {
					has_idea = superpower
					has_idea = great_power
					has_idea = large_power
				}
			}
		}
	}
}

country_event = {
	id = corruption.3
	title = corruption.3.t
	desc = corruption.3.d
	picture = GFX_politics_negotiations

	is_triggered_only = yes

	trigger = {
		NOT = {
			has_country_flag = auto_decline_corruption
		}
	}

	option = {
		name = corruption.3.a
		log = "[GetDateText]: [This.GetName]: corruption.3.a executed"

		add_timed_idea = {
			idea = corruption_decreased_mil_spending
			days = 200
		}

		set_temp_variable = { temp_opinion = -3 }
		change_the_military_opinion = yes

		random_list = {
			20 = {
				modifier = {
					factor = 1.5
					OR = {
						has_idea = oligarchs
						neutrality_neutral_oligarch_are_in_power = yes
					}
				}
				increase_corruption = yes
			}
			80 = {
				# nothing
			}
		}

		ai_chance = {
			base = 10
			modifier = {
				factor = 2
				OR = {
					has_idea = oligarchs
					neutrality_neutral_oligarch_are_in_power = yes
				}
			}
			modifier = {
				factor = 0
				original_tag = BOS
				has_stability < 0.40
				date < 2000.6.01
			}
		}
	}

	option = {
		name = corruption.3.b
		log = "[GetDateText]: [This.GetName]: corruption.3.b executed"
		ai_chance = {
			base = 30
			modifier = {
				factor = 1.25
				has_stability < 0.30
			}
			modifier = {
				factor = 1.5
				check_variable = { interest_rate > 8 }
			}
			modifier = {
				factor = 10
				OR = {
					has_idea = superpower
					has_idea = great_power
					has_idea = large_power
				}
			}
		}
	}
}

country_event = {
	id = corruption.4
	title = corruption.4.t
	desc = corruption.4.d
	picture = GFX_politics_negotiations

	is_triggered_only = yes

	trigger = {
		NOT = {
			has_country_flag = auto_decline_corruption
		}
	}

	option = {
		name = corruption.4.a
		log = "[GetDateText]: [This.GetName]: corruption.4.a executed"

		add_timed_idea = {
			idea = corruption_decreased_edu_spending
			days = 200
		}

		random_list = {
			20 = {
				modifier = {
					factor = 1.5
					OR = {
						has_idea = oligarchs
						neutrality_neutral_oligarch_are_in_power = yes
					}
				}
				increase_corruption = yes
			}
			80 = {
				# nothing
			}
		}

		ai_chance = {
			base = 10
			modifier = {
				factor = 2
				OR = {
					has_idea = oligarchs
					neutrality_neutral_oligarch_are_in_power = yes
				}
			}
			modifier = {
				factor = 0
				original_tag = BOS
				has_stability < 0.40
				date < 2000.6.01
			}
		}
	}

	option = {
		name = corruption.4.b
		log = "[GetDateText]: [This.GetName]: corruption.4.b executed"
		ai_chance = {
			base = 30
			modifier = {
				factor = 1.25
				has_stability < 0.30
			}
			modifier = {
				factor = 1.5
				check_variable = { interest_rate > 8 }
			}
			modifier = {
				factor = 10
				OR = {
					has_idea = superpower
					has_idea = great_power
					has_idea = large_power
				}
			}
		}
	}
}

country_event = {
	id = corruption.5
	title = corruption.5.t
	desc = corruption.5.d
	picture = GFX_politics_negotiations

	is_triggered_only = yes

	trigger = {
		NOT = {
			has_country_flag = auto_decline_corruption
		}
	}

	option = {
		name = corruption.5.a
		log = "[GetDateText]: [This.GetName]: corruption.5.a executed"

		add_timed_idea = {
			idea = corruption_decreased_health_spending
			days = 200
		}

		random_list = {
			20 = {
				modifier = {
					factor = 1.5
					OR = {
						has_idea = oligarchs
						neutrality_neutral_oligarch_are_in_power = yes
					}
				}
				increase_corruption = yes
			}
			80 = {
				# nothing
			}
		}

		ai_chance = {
			base = 10
			modifier = {
				factor = 2
				OR = {
					has_idea = oligarchs
					neutrality_neutral_oligarch_are_in_power = yes
				}
			}
			modifier = {
				factor = 0
				original_tag = BOS
				has_stability < 0.40
				date < 2000.6.01
			}
		}
	}

	option = {
		name = corruption.5.b
		log = "[GetDateText]: [This.GetName]: corruption.5.b executed"
		ai_chance = {
			base = 30
			modifier = {
				factor = 1.25
				has_stability < 0.30
			}
			modifier = {
				factor = 1.5
				check_variable = { interest_rate > 8 }
			}
			modifier = {
				factor = 10
				OR = {
					has_idea = superpower
					has_idea = great_power
					has_idea = large_power
				}
			}
		}
	}
}

country_event = {
	id = corruption.6
	title = corruption.6.t
	desc = corruption.6.d
	picture = GFX_politics_negotiations

	is_triggered_only = yes

	trigger = {
		NOT = {
			has_country_flag = auto_decline_corruption
		}
	}

	option = {
		name = corruption.6.a
		log = "[GetDateText]: [This.GetName]: corruption.6.a executed"

		add_timed_idea = {
			idea = corruption_decreased_social_spending
			days = 200
		}

		set_temp_variable = { temp_opinion = -3 }
		change_labour_unions_opinion = yes

		random_list = {
			20 = {
				modifier = {
					factor = 1.5
					OR = {
						has_idea = oligarchs
						neutrality_neutral_oligarch_are_in_power = yes
					}
				}
				increase_corruption = yes
			}
			80 = {
				# nothing
			}
		}

		ai_chance = {
			base = 10
			modifier = {
				factor = 2
				OR = {
					has_idea = oligarchs
					neutrality_neutral_oligarch_are_in_power = yes
				}
			}
			modifier = {
				factor = 0
				original_tag = BOS
				has_stability < 0.40
				date < 2000.6.01
			}
		}
	}

	option = {
		name = corruption.6.b
		log = "[GetDateText]: [This.GetName]: corruption.6.b executed"
		ai_chance = {
			base = 30
			modifier = {
				factor = 1.25
				has_stability < 0.30
			}
			modifier = {
				factor = 1.5
				check_variable = { interest_rate > 8 }
			}
			modifier = {
				factor = 10
				OR = {
					has_idea = superpower
					has_idea = great_power
					has_idea = large_power
				}
			}
		}
	}
}

country_event = {
	id = corruption.7
	title = corruption.7.t
	desc = corruption.7.d
	picture = GFX_politics_negotiations

	is_triggered_only = yes

	trigger = {
		NOT = {
			has_country_flag = auto_decline_corruption
		}
	}

	option = {
		name = corruption.7.a
		log = "[GetDateText]: [This.GetName]: corruption.7.a executed"

		set_party_index_to_ruling_party = yes
		set_temp_variable = { party_popularity_increase = 0.05 }
		set_temp_variable = { temp_outlook_increase = 0.05 }
		add_relative_party_popularity = yes

		set_temp_variable = { treasury_change = gdp_total }
		multiply_temp_variable = { treasury_change = -0.02 }
		modify_treasury_effect = yes

		ai_chance = {
			base = 10
			modifier = {
				factor = 2
				OR = {
					has_idea = oligarchs
					neutrality_neutral_oligarch_are_in_power = yes
				}
			}
			modifier = {
				factor = 0
				original_tag = BOS
				has_stability < 0.40
				date < 2000.6.01
			}
		}
	}

	option = {
		name = corruption.7.b
		log = "[GetDateText]: [This.GetName]: corruption.7.b executed"
		ai_chance = {
			base = 30
			modifier = {
				factor = 1.25
				has_stability < 0.30
			}
			modifier = {
				factor = 1.5
				check_variable = { interest_rate > 8 }
			}
			modifier = {
				factor = 10
				OR = {
					has_idea = superpower
					has_idea = great_power
					has_idea = large_power
				}
			}
		}
	}
}

country_event = {
	id = corruption.8
	title = corruption.8.t
	desc = corruption.8.d
	picture = GFX_politics_negotiations

	is_triggered_only = yes

	trigger = {
		NOT = {
			has_country_flag = auto_decline_corruption
		}
	}

	option = {
		name = corruption.8.a
		log = "[GetDateText]: [This.GetName]: corruption.8.a executed"

		# Makes PP reward for accepting this corruption event dynamic based on power_ranking. Roughly equals 55% of the cost to change corruption level. Used to be a static 200PP for all.
		if = {
			limit = { has_idea = non_power }
			add_political_power = 140
		}
		else_if = {
			limit = { has_idea = minor_power }
			add_political_power = 170
		}
		else_if = {
			limit = { has_idea = regional_power }
			add_political_power = 190
		}
		else_if = {
			limit = { has_idea = large_power }
			add_political_power = 210
		}
		else_if = {
			limit = { has_idea = great_power }
			add_political_power = 240
		}
		else = { # Captures the rest of power rankings not mentioned which is superpower
			add_political_power = 300
		}

		random_list = {
			20 = {
				modifier = {
					factor = 1.5
					OR = {
						has_idea = oligarchs
						neutrality_neutral_oligarch_are_in_power = yes
					}
				}
				increase_corruption = yes
			}
			80 = {
				# nothing
			}
		}

		ai_chance = {
			base = 10
			modifier = {
				factor = 2
				OR = {
					has_idea = oligarchs
					neutrality_neutral_oligarch_are_in_power = yes
				}
			}
			modifier = {
				factor = 0
				original_tag = BOS
				has_stability < 0.40
				date < 2000.6.01
			}
		}
	}

	option = {
		name = corruption.8.b
		log = "[GetDateText]: [This.GetName]: corruption.8.b executed"
		ai_chance = {
			base = 30
			modifier = {
				factor = 1.25
				has_stability < 0.30
			}
			modifier = {
				factor = 1.5
				check_variable = { interest_rate > 8 }
			}
			modifier = {
				factor = 10
				OR = {
					has_idea = superpower
					has_idea = great_power
					has_idea = large_power
				}
			}
		}
	}
}

country_event = {
	id = corruption.9
	title = corruption.9.t
	desc = corruption.9.d
	picture = GFX_politics_negotiations

	is_triggered_only = yes

	trigger = {
		NOT = {
			has_country_flag = auto_decline_corruption
		}
	}

	option = {
		name = corruption.9.a
		log = "[GetDateText]: [This.GetName]: corruption.9.a executed"

		add_timed_idea = {
			idea = corruption_civ_construction
			days = 200
		}

		set_temp_variable = { temp_opinion = -3 }
		change_industrial_conglomerates_opinion = yes
		change_labour_unions_opinion = yes

		random_list = {
			20 = {
				modifier = {
					factor = 1.5
					OR = {
						has_idea = oligarchs
						neutrality_neutral_oligarch_are_in_power = yes
					}
				}
				increase_corruption = yes
			}
			80 = {
				# nothing
			}
		}

		ai_chance = {
			base = 10
			modifier = {
				factor = 2
				OR = {
					has_idea = oligarchs
					neutrality_neutral_oligarch_are_in_power = yes
				}
			}
			modifier = {
				factor = 0
				original_tag = BOS
				has_stability < 0.40
				date < 2000.6.01
			}
		}
	}

	option = {
		name = corruption.9.b
		log = "[GetDateText]: [This.GetName]: corruption.9.b executed"
		ai_chance = {
			base = 30
			modifier = {
				factor = 1.25
				has_stability < 0.30
			}
			modifier = {
				factor = 1.5
				check_variable = { interest_rate > 8 }
			}
			modifier = {
				factor = 10
				OR = {
					has_idea = superpower
					has_idea = great_power
					has_idea = large_power
				}
			}
		}
	}
}

country_event = {
	id = corruption.10
	title = corruption.10.t
	desc = corruption.10.d
	picture = GFX_politics_negotiations

	is_triggered_only = yes

	trigger = {
		NOT = {
			has_country_flag = auto_decline_corruption
		}
	}

	option = {
		name = corruption.10.a
		log = "[GetDateText]: [This.GetName]: corruption.10.a executed"

		add_timed_idea = {
			idea = corruption_mil_construction
			days = 200
		}

		set_temp_variable = { temp_opinion = -3 }
		change_industrial_conglomerates_opinion = yes
		change_labour_unions_opinion = yes

		random_list = {
			20 = {
				modifier = {
					factor = 1.5
					OR = {
						has_idea = oligarchs
						neutrality_neutral_oligarch_are_in_power = yes
					}
				}
				increase_corruption = yes
			}
			80 = {
				# nothing
			}
		}

		ai_chance = {
			base = 10
			modifier = {
				factor = 2
				OR = {
					has_idea = oligarchs
					neutrality_neutral_oligarch_are_in_power = yes
				}
			}
			modifier = {
				factor = 0
				original_tag = BOS
				has_stability < 0.40
				date < 2000.6.01
			}
		}
	}

	option = {
		name = corruption.10.b
		log = "[GetDateText]: [This.GetName]: corruption.10.b executed"
		ai_chance = {
			base = 30
			modifier = {
				factor = 1.25
				has_stability < 0.30
			}
			modifier = {
				factor = 1.5
				check_variable = { interest_rate > 8 }
			}
			modifier = {
				factor = 10
				OR = {
					has_idea = superpower
					has_idea = great_power
					has_idea = large_power
				}
			}
		}
	}
}

country_event = {
	id = corruption.11
	title = corruption.11.t
	desc = corruption.11.d
	picture = GFX_politics_negotiations

	is_triggered_only = yes

	trigger = {
		NOT = {
			has_country_flag = auto_decline_corruption
		}
	}

	option = {
		name = corruption.11.a
		log = "[GetDateText]: [This.GetName]: corruption.11.a executed"

		add_timed_idea = {
			idea = corruption_construction_buff_idea
			days = 200
		}

		set_temp_variable = { temp_opinion = -3 }
		change_farmers_opinion = yes

		random_list = {
			20 = {
				modifier = {
					factor = 1.5
					OR = {
						has_idea = oligarchs
						neutrality_neutral_oligarch_are_in_power = yes
					}
				}
				increase_corruption = yes
			}
			80 = {
				# nothing
			}
		}

		ai_chance = {
			base = 10
			modifier = {
				factor = 2
				OR = {
					has_idea = oligarchs
					neutrality_neutral_oligarch_are_in_power = yes
				}
			}
			modifier = {
				factor = 0
				original_tag = BOS
				has_stability < 0.40
				date < 2000.6.01
			}
		}
	}

	option = {
		name = corruption.11.b
		log = "[GetDateText]: [This.GetName]: corruption.11.b executed"
		ai_chance = {
			base = 30
			modifier = {
				factor = 1.25
				has_stability < 0.30
			}
			modifier = {
				factor = 1.5
				check_variable = { interest_rate > 8 }
			}
			modifier = {
				factor = 10
				OR = {
					has_idea = superpower
					has_idea = great_power
					has_idea = large_power
				}
			}
		}
	}
}

country_event = {
	id = corruption.12
	title = corruption.12.t
	desc = corruption.12.d
	picture = GFX_politics_negotiations

	is_triggered_only = yes

	trigger = {
		NOT = {
			has_country_flag = auto_decline_corruption
		}
	}

	option = {
		name = corruption.12.a
		log = "[GetDateText]: [This.GetName]: corruption.12.a executed"

		add_timed_idea = {
			idea = corruption_military_prod_buff_idea
			days = 200
		}

		set_temp_variable = { temp_opinion = -3 }
		change_farmers_opinion = yes

		random_list = {
			20 = {
				modifier = {
					factor = 1.5
					OR = {
						has_idea = oligarchs
						neutrality_neutral_oligarch_are_in_power = yes
					}
				}
				increase_corruption = yes
			}
			80 = {
				# nothing
			}
		}

		ai_chance = {
			base = 10
			modifier = {
				factor = 2
				OR = {
					has_idea = oligarchs
					neutrality_neutral_oligarch_are_in_power = yes
				}
			}
			modifier = {
				factor = 0
				original_tag = BOS
				has_stability < 0.40
				date < 2000.6.01
			}
		}
	}

	option = {
		name = corruption.12.b
		log = "[GetDateText]: [This.GetName]: corruption.12.b executed"
		ai_chance = {
			base = 30
			modifier = {
				factor = 1.25
				has_stability < 0.30
			}
			modifier = {
				factor = 1.5
				check_variable = { interest_rate > 8 }
			}
			modifier = {
				factor = 10
				OR = {
					has_idea = superpower
					has_idea = great_power
					has_idea = large_power
				}
			}
		}
	}
}

country_event = {
	id = corruption.13
	title = corruption.13.t
	desc = corruption.13.d
	picture = GFX_politics_negotiations

	is_triggered_only = yes

	trigger = {
		NOT = {
			has_country_flag = auto_decline_corruption
		}
	}

	option = {
		name = corruption.13.a
		log = "[GetDateText]: [This.GetName]: corruption.13.a executed"

		set_temp_variable = { int_investment_change = gdp_total }
		multiply_temp_variable = { int_investment_change = 0.05 }
		modify_international_investment_effect = yes

		set_temp_variable = { temp_opinion = -3 }
		change_international_bankers_opinion = yes

		random_list = {
			20 = {
				modifier = {
					factor = 1.5
					OR = {
						has_idea = oligarchs
						neutrality_neutral_oligarch_are_in_power = yes
					}
				}
				increase_corruption = yes
			}
			80 = {
				# nothing
			}
		}

		ai_chance = {
			base = 10
			modifier = {
				factor = 2
				OR = {
					has_idea = oligarchs
					neutrality_neutral_oligarch_are_in_power = yes
				}
			}
			modifier = {
				factor = 0
				original_tag = BOS
				has_stability < 0.40
				date < 2000.6.01
			}
		}
	}

	option = {
		name = corruption.13.b
		log = "[GetDateText]: [This.GetName]: corruption.13.b executed"
		ai_chance = {
			base = 30
			modifier = {
				factor = 1.25
				has_stability < 0.30
			}
			modifier = {
				factor = 1.5
				check_variable = { interest_rate > 8 }
			}
			modifier = {
				factor = 10
				OR = {
					has_idea = superpower
					has_idea = great_power
					has_idea = large_power
				}
			}
		}
	}
}

country_event = {
	id = corruption.14
	title = corruption.14.t
	desc = corruption.14.d
	picture = GFX_politics_negotiations

	is_triggered_only = yes

	trigger = {
		NOT = {
			has_country_flag = auto_decline_corruption
		}
	}

	option = {
		name = corruption.14.a
		log = "[GetDateText]: [This.GetName]: corruption.14.a executed"

		set_temp_variable = { temp_opinion = 5 }
		change_oligarchs_opinion = yes

		random_list = {
			20 = {
				modifier = {
					factor = 1.5
					OR = {
						has_idea = oligarchs
						neutrality_neutral_oligarch_are_in_power = yes
					}
				}
				increase_corruption = yes
			}
			80 = {
				# nothing
			}
		}

		ai_chance = {
			base = 10
			modifier = {
				factor = 2
				OR = {
					has_idea = oligarchs
					neutrality_neutral_oligarch_are_in_power = yes
				}
			}
			modifier = {
				factor = 0
				original_tag = BOS
				has_stability < 0.40
				date < 2000.6.01
			}
		}
	}

	option = {
		name = corruption.14.b
		log = "[GetDateText]: [This.GetName]: corruption.14.b executed"
		ai_chance = {
			base = 30
			modifier = {
				factor = 1.25
				has_stability < 0.30
			}
			modifier = {
				factor = 1.5
				check_variable = { interest_rate > 8 }
			}
			modifier = {
				factor = 10
				OR = {
					has_idea = superpower
					has_idea = great_power
					has_idea = large_power
				}
			}
		}
	}
}

country_event = {
	id = corruption.15
	title = corruption.15.t
	desc = corruption.15.d
	picture = GFX_politics_negotiations

	is_triggered_only = yes

	trigger = {
		NOT = {
			has_country_flag = auto_decline_corruption
		}
	}

	option = {
		name = corruption.15.a
		log = "[GetDateText]: [This.GetName]: corruption.15.a executed"

		add_timed_idea = {
			idea = corruption_resource_buff_idea
			days = 200
		}

		set_temp_variable = { temp_opinion = -3 }
		change_fossil_fuel_industry_opinion = yes

		random_list = {
			20 = {
				modifier = {
					factor = 1.5
					OR = {
						has_idea = oligarchs
						neutrality_neutral_oligarch_are_in_power = yes
					}
				}
				increase_corruption = yes
			}
			80 = {
				# nothing
			}
		}

		ai_chance = {
			base = 10
			modifier = {
				factor = 2
				OR = {
					has_idea = oligarchs
					neutrality_neutral_oligarch_are_in_power = yes
				}
			}
			modifier = {
				factor = 0
				original_tag = BOS
				has_stability < 0.40
				date < 2000.6.01
			}
		}
	}

	option = {
		name = corruption.15.b
		log = "[GetDateText]: [This.GetName]: corruption.14.b executed"
		ai_chance = {
			base = 30
			modifier = {
				factor = 1.25
				has_stability < 0.30
			}
			modifier = {
				factor = 1.5
				check_variable = { interest_rate > 8 }
			}
			modifier = {
				factor = 10
				OR = {
					has_idea = superpower
					has_idea = great_power
					has_idea = large_power
				}
			}
		}
	}
}
