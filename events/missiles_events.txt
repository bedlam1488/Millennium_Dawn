add_namespace = satellites
add_namespace = nukes

### downgrade satellite systems
news_event = {
	id = satellites.1
	title = satellites.1.t
	desc = satellites.1.d
	picture = GFX_space_debris

	is_triggered_only = yes ### update_sat_systems_stats = yes
	option = {
		name = satellites.1.a
		log = "[GetDateText]: [Root.GetName]: event satellites.1.a"
	}
}

news_event = {
	id = satellites.2
	title = satellites.2.t
	desc = satellites.2.d
	picture = GFX_space_debris

	is_triggered_only = yes ### update_sat_systems_stats = yes
	option = {
		name = satellites.2.a
		log = "[GetDateText]: [Root.GetName]: event satellites.2.a"
		update_sat_systems_stats = yes
	}
}

news_event = {
	id = satellites.3
	title = satellites.3.t
	desc = satellites.3.d
	picture = GFX_space_debris

	is_triggered_only = yes ### update_sat_systems_stats = yes
	option = {
		name = satellites.3.a
		log = "[GetDateText]: [Root.GetName]: event satellites.3.a"
		update_sat_systems_stats = yes
	}
}

news_event = {
	id = satellites.4
	title = satellites.4.t
	desc = satellites.4.d
	picture = GFX_space_debris

	is_triggered_only = yes ### update_sat_systems_stats = yes
	option = {
		name = satellites.4.a
		log = "[GetDateText]: [Root.GetName]: event satellites.4.a"
		update_sat_systems_stats = yes
	}
}

news_event = {
	id = satellites.5
	title = satellites.5.t
	desc = satellites.5.d
	picture = GFX_space_debris

	is_triggered_only = yes ### update_sat_systems_stats = yes
	option = {
		name = satellites.5.a
		log = "[GetDateText]: [Root.GetName]: event satellites.5.a"
		update_sat_systems_stats = yes
	}
}

news_event = {
	id = satellites.6
	title = satellites.6.t
	desc = satellites.6.d
	picture = GFX_space_debris

	is_triggered_only = yes ### update_sat_systems_stats = yes
	option = {
		name = satellites.6.a
		log = "[GetDateText]: [Root.GetName]: event satellites.6.a"
		update_sat_systems_stats = yes
	}
}

news_event = {
	id = satellites.7
	title = satellites.7.t
	desc = satellites.7.d
	picture = GFX_space_debris

	is_triggered_only = yes ### update_sat_systems_stats = yes
	option = {
		name = satellites.7.a
		log = "[GetDateText]: [Root.GetName]: event satellites.7.a"
		update_sat_systems_stats = yes
	}
}

news_event = {
	id = satellites.8
	title = satellites.8.t
	desc = satellites.8.d
	picture = GFX_space_debris

	is_triggered_only = yes ### update_sat_systems_stats = yes
	option = {
		name = satellites.8.a
		log = "[GetDateText]: [Root.GetName]: event satellites.8.a"

	}
}

### Kinetic Bombardment
news_event = {
	id = satellites.9
	title = satellites.9.t
	desc = satellites.9.d
	picture = GFX_kinetic_bombardment

	is_triggered_only = yes ### update_sat_systems_stats = yes
	option = {
		name = satellites.9.a
		log = "[GetDateText]: [Root.GetName]: event satellites.9.a"
	}
}

country_event = {
	id = nukes.1
	title = nukes.1.t
	desc = nukes.1.d

	#picture = GFX_eu
	hidden = yes
	is_triggered_only = yes

	option = { #a yes
		name = nukes.1.a
		log = "[GetDateText]: [Root.GetName]: event nukes.1.a"
		clear_nuke_vars = yes

		ai_chance = {
			factor = 1
		}
	}
}

country_event = {
	id = nukes.2
	title = nukes.2.t
	desc = nukes.2.d

	picture = GFX_retaliatory_strike

	hidden = yes
	is_triggered_only = yes

	immediate = {
		log = "[GetDateText]: [Root.GetName]: event nukes.2"
		if = { limit = { alert_conditions = yes }
			ROOT = {
				country_event = nukes.3
			}
		}
	}

	option = { #a yes
		name = nukes.2.a
		log = "[GetDateText]: [This.GetName]: nukes.2.a executed"
		ai_chance = {
			factor = 1
		}
	}
}

country_event = {
	id = nukes.3
	title = nukes.3.t
	desc = nukes.3.d

	picture = GFX_retaliatory_strike

	is_triggered_only = yes

	option = { #a yes
		name = nukes.3.a
		log = "[GetDateText]: [Root.GetName]: event nukes.3.a"
		fire_on_alert = yes

		ai_chance = {
			factor = 1
		}
	}

	option = { #b no
		name = nukes.3.b
		log = "[GetDateText]: [Root.GetName]: event nukes.3.b"
	}
}

country_event = {
	id = nukes.4
	title = nukes.4.t
	desc = nukes.4.d

	picture = GFX_nuclear_test

	is_triggered_only = yes

	option = { #a yes
		name = nukes.4.a
		log = "[GetDateText]: [Root.GetName]: event nukes.4.a"


		ai_chance = {
			factor = 1
		}
	}

	option = { #b no
		name = nukes.4.b
		log = "[GetDateText]: [Root.GetName]: event nukes.4.b"
		set_country_flag = NOT_show_nuclare_test_info
	}
}
