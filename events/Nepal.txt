add_namespace = nepal

#Election event if both sides truths
country_event = {
	id = nepal.1
	title = nepal.1.t
	desc = nepal.1.d
	picture = GFX_nep_election

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		#Nepali Congress
		name = nepal.1.a
		custom_effect_tooltip = Nepali_Congress_wins_tt
		hidden_effect = {
			set_politics = {
				ruling_party = democratic
				elections_allowed = yes
			}
			set_temp_variable = { party_index = 3 }
			set_temp_variable = { party_popularity_increase = 0.06 }
			set_temp_variable = { temp_outlook_increase = 0.07 }
			add_relative_party_popularity = yes
			set_temp_variable = { rul_party_temp = 3 }
			change_ruling_party_effect = yes
		}
		log = "[GetDateText]: [This.GetName]: nepal.1.a executed"
		ai_chance = {
			factor = 10
			modifier = {
				is_historical_focus_on = yes
				factor = 100
			}
		}
	}
	option = {
		#CPN (Maoist)
		name = nepal.1.b
		custom_effect_tooltip = Communist_Party_of_Nepal_wins_tt
		hidden_effect = {
			set_politics = {
				ruling_party = neutrality
				elections_allowed = yes
			}
			set_temp_variable = { party_index = 19 }
			set_temp_variable = { party_popularity_increase = 0.06 }
			set_temp_variable = { temp_outlook_increase = 0.07 }
			add_relative_party_popularity = yes
			set_temp_variable = { rul_party_temp = 19 }
			change_ruling_party_effect = yes
		}
		log = "[GetDateText]: [This.GetName]: nepal.1.b executed"
		ai_chance = {
			factor = 20
		}
	}
}
