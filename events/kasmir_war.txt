﻿add_namespace = kasmir_war

country_event = {
	id = kasmir_war.1
	title = kasmir_war.1.t
	desc = kasmir_war.1.d
	picture = GFX_terrorist_attack

	is_triggered_only = yes

	option = {
		name = kasmir_war.1.a
		log = "[GetDateText]: [This.GetName]: kasmir_war.1.a executed"
		add_war_support = -0.01
		add_manpower = -20
	}
}

country_event = {
	id = kasmir_war.2
	title = kasmir_war.2.t
	desc = kasmir_war.2.d
	picture = GFX_terrorist_attack

	is_triggered_only = yes

	option = {
		name = kasmir_war.2.a
		log = "[GetDateText]: [This.GetName]: kasmir_war.2.a executed"
		add_war_support = -0.02
		add_manpower = -10
	}
}

country_event = {
	id = kasmir_war.3
	title = kasmir_war.3.t
	desc = kasmir_war.3.d
	picture = GFX_terrorist_attack

	is_triggered_only = yes

	option = {
		name = kasmir_war.3.a
		log = "[GetDateText]: [This.GetName]: kasmir_war.2.a executed"
		custom_effect_tooltip = kasmir_readiness_minus_10_TT
		add_to_variable = { kasmir_readiness = -10 }
		clamp_variable = {
			var = THIS.kasmir_readiness
			min = 0
			max = 100
		}
	}
}

country_event = {
	id = kasmir_war.10
	title = kasmir_war.10.t
	desc = kasmir_war.10.d
	picture = GFX_Military_Reform
	is_triggered_only = yes
	option = {
		name = kasmir_war.10.a
		log = "[GetDateText]: [This.GetName]: kasmir_war.10.a"
		add_to_variable = { THIS.Kasmir_war_progress = 10 }
		clamp_variable = { var = THIS.Kasmir_war_progress min = 0 max = 100 }
		custom_effect_tooltip = Kasmir_war_increase_progress_10_TT
		clr_country_flag = Attacking_in_Kasmir
		ai_chance = { factor = 10 }
	}
}

country_event = {
	id = kasmir_war.11
	title = kasmir_war.11.t
	desc = kasmir_war.11.d
	picture = GFX_Military_Reform
	is_triggered_only = yes
	option = {
		name = kasmir_war.11.a
		log = "[GetDateText]: [This.GetName]: kasmir_war.11.a"
		subtract_from_variable = { THIS.Kasmir_war_progress = 5 }
		clamp_variable = { var = THIS.Kasmir_war_progress min = 0 max = 100 }
		custom_effect_tooltip = Kasmir_war_decrease_progress_5_TT
		clr_country_flag = Attacking_in_Kasmir
		ai_chance = { factor = 10 }
	}
}

country_event = {
	id = kasmir_war.12
	title = kasmir_war.12.t
	desc = kasmir_war.12.d
	picture = GFX_Military_Reform
	is_triggered_only = yes
	option = {
		name = kasmir_war.12.a
		log = "[GetDateText]: [This.GetName]: kasmir_war.12.a"
		subtract_from_variable = { THIS.Kasmir_war_progress = 3 }
		clamp_variable = { var = THIS.Kasmir_war_progress min = 0 max = 100 }
		custom_effect_tooltip = Kasmir_war_decrease_progress_3_TT
		clr_country_flag = Attacking_in_Kasmir
		ai_chance = { factor = 10 }
	}
}

country_event = {
	id = kasmir_war.13
	title = kasmir_war.13.t
	desc = kasmir_war.13.d
	picture = GFX_Military_Reform
	is_triggered_only = yes
	option = {
		name = kasmir_war.13.a
		log = "[GetDateText]: [This.GetName]: kasmir_war.13.a"
		subtract_from_variable = { THIS.Kasmir_war_progress = 10 }
		clamp_variable = { var = THIS.Kasmir_war_progress min = 0 max = 100 }
		custom_effect_tooltip = Kasmir_war_decrease_progress_10_TT
		ai_chance = { factor = 10 }
	}
}

country_event = {
	id = kasmir_war.14
	title = kasmir_war.14.t
	desc = kasmir_war.14.d
	picture = GFX_Military_Reform
	is_triggered_only = yes
	option = {
		name = kasmir_war.14.a
		log = "[GetDateText]: [This.GetName]: kasmir_war.14.a"
		add_to_variable = { THIS.Kasmir_war_progress = 5 }
		clamp_variable = { var = THIS.Kasmir_war_progress min = 0 max = 100 }
		custom_effect_tooltip = Kasmir_war_increase_progress_5_TT
		ai_chance = { factor = 10 }
	}
}

country_event = {
	id = kasmir_war.15
	title = kasmir_war.15.t
	desc = kasmir_war.15.d
	picture = GFX_Military_Reform
	is_triggered_only = yes
	option = {
		name = kasmir_war.15.a
		log = "[GetDateText]: [This.GetName]: kasmir_war.15.a"
		subtract_from_variable = { THIS.Kasmir_war_progress = 3 }
		clamp_variable = { var = THIS.Kasmir_war_progress min = 0 max = 100 }
		custom_effect_tooltip = Kasmir_war_decrease_progress_3_TT
		ai_chance = { factor = 10 }
	}
}

country_event = {
	id = kasmir_war.6
	title = kasmir_war.6.t
	desc = kasmir_war.6.d
	picture = GFX_Military_Reform
	is_triggered_only = yes
	option = {
		name = kasmir_war.6.a
		log = "[GetDateText]: [This.GetName]: kasmir_war.6.a"
		ai_chance = { factor = 10 }
	}
}

country_event = {
	id = kasmir_war.20
	title = kasmir_war.20.t
	desc = kasmir_war.20.d
	picture = GFX_Military_Reform
	is_triggered_only = yes
	option = {
		name = kasmir_war.20.a
		log = "[GetDateText]: [This.GetName]: kasmir_war.20.a"
		add_to_variable = { THIS.Kasmir_war_progress_china = 10 }
		clamp_variable = { var = THIS.Kasmir_war_progress_china min = 0 max = 100 }
		custom_effect_tooltip = Kasmir_war_increase_progress_10_TT
		clr_country_flag = Attacking_in_Kasmir_China
		ai_chance = { factor = 10 }
	}
}

country_event = {
	id = kasmir_war.21
	title = kasmir_war.21.t
	desc = kasmir_war.21.d
	picture = GFX_Military_Reform
	is_triggered_only = yes
	option = {
		name = kasmir_war.21.a
		log = "[GetDateText]: [This.GetName]: kasmir_war.21.a"
		subtract_from_variable = { THIS.Kasmir_war_progress_china = 5 }
		clamp_variable = { var = THIS.Kasmir_war_progress_china min = 0 max = 100 }
		custom_effect_tooltip = Kasmir_war_decrease_progress_5_TT
		clr_country_flag = Attacking_in_Kasmir_China
		ai_chance = { factor = 10 }
	}
}

country_event = {
	id = kasmir_war.22
	title = kasmir_war.22.t
	desc = kasmir_war.22.d
	picture = GFX_Military_Reform
	is_triggered_only = yes
	option = {
		name = kasmir_war.22.a
		log = "[GetDateText]: [This.GetName]: kasmir_war.22.a"
		subtract_from_variable = { THIS.Kasmir_war_progress_china = 3 }
		clamp_variable = { var = THIS.Kasmir_war_progress_china min = 0 max = 100 }
		custom_effect_tooltip = Kasmir_war_decrease_progress_3_TT
		clr_country_flag = Attacking_in_Kasmir_China
		ai_chance = { factor = 10 }
	}
}

country_event = {
	id = kasmir_war.23
	title = kasmir_war.23.t
	desc = kasmir_war.23.d
	picture = GFX_Military_Reform
	is_triggered_only = yes
	option = {
		name = kasmir_war.23.a
		log = "[GetDateText]: [This.GetName]: kasmir_war.23.a"
		subtract_from_variable = { THIS.Kasmir_war_progress_china = 10 }
		clamp_variable = { var = THIS.Kasmir_war_progress_china min = 0 max = 100 }
		custom_effect_tooltip = Kasmir_war_decrease_progress_10_TT
		ai_chance = { factor = 10 }
	}
}

country_event = {
	id = kasmir_war.24
	title = kasmir_war.24.t
	desc = kasmir_war.24.d
	picture = GFX_Military_Reform
	is_triggered_only = yes
	option = {
		name = kasmir_war.24.a
		log = "[GetDateText]: [This.GetName]: kasmir_war.24.a"
		add_to_variable = { THIS.Kasmir_war_progress_china = 5 }
		clamp_variable = { var = THIS.Kasmir_war_progress_china min = 0 max = 100 }
		custom_effect_tooltip = Kasmir_war_increase_progress_5_TT
		ai_chance = { factor = 10 }
	}
}

country_event = {
	id = kasmir_war.25
	title = kasmir_war.25.t
	desc = kasmir_war.25.d
	picture = GFX_Military_Reform
	is_triggered_only = yes
	option = {
		name = kasmir_war.25.a
		log = "[GetDateText]: [This.GetName]: kasmir_war.25.a"
		subtract_from_variable = { THIS.Kasmir_war_progress_china = 3 }
		clamp_variable = { var = THIS.Kasmir_war_progress_china min = 0 max = 100 }
		custom_effect_tooltip = Kasmir_war_decrease_progress_3_TT
		ai_chance = { factor = 10 }
	}
}

country_event = {
	id = kasmir_war.30
	title = kasmir_war.30.t
	desc = kasmir_war.30.d
	picture = GFX_Military_Reform
	is_triggered_only = yes
	option = {
		name = kasmir_war.30.a
		log = "[GetDateText]: [This.GetName]: kasmir_war.30.a"
		add_war_support = 0.1
		add_stability = -0.1
		ai_chance = { factor = 5 }
	}
	option = {
		name = kasmir_war.30.b
		log = "[GetDateText]: [This.GetName]: kasmir_war.30.b"
		create_wargoal = {
			type = annex_everything
			target = PREV
		}
	}
}