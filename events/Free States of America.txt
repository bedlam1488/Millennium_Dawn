# Author(s): Luigi IV
add_namespace = post_united_event

##### Free States of America's TAG is USB
###################
# Post United Era #
###################
country_event = {
	id = post_united_event.1
	title = post_united_event.1.t
	desc = post_united_event.1.d
	picture = GFX_politics_negotiations
	is_triggered_only = yes
	trigger = {
		original_tag = USB
	}

	option = {
		name = post_united_event.1.o1
		log = "[GetDateText]: [This.GetName]: post_united_event.1.o1 executed"
		set_temp_variable = { temp_change = 0.05 }
		FS_change_domestic_monthly_pop_change = yes
		set_temp_variable = { temp_change = -0.01 }
		FS_change_industrial_consumer_goods_change = yes
		set_temp_variable = { temp_change = 0.05 }
		FS_change_domestic_offensive_foreign_influence_change = yes
		set_temp_variable = { temp_change = -0.05 }
		FS_change_domestic_foreign_influence_defense_change = yes
		set_temp_variable = { party_index = 0 }
		set_temp_variable = { party_popularity_increase = 0.10 }
		set_temp_variable = { temp_outlook_increase = 0.10 }
		add_relative_party_popularity = yes

		set_temp_variable = { party_index = 6 }
		set_temp_variable = { party_popularity_increase = 0.10 }
		add_relative_party_popularity = yes

		set_temp_variable = { party_index = 14 }
		set_temp_variable = { party_popularity_increase = 0.10 }
		add_relative_party_popularity = yes

		set_temp_variable = { party_index = 20 }
		set_temp_variable = { party_popularity_increase = 0.10 }
		add_relative_party_popularity = yes

		country_event = {
			id = post_united_event.2
			days = 7
		}
	}
}

country_event = {
	id = post_united_event.2
	title = post_united_event.2.t
	desc = post_united_event.2.d
	picture = GFX_politics_negotiations
	is_triggered_only = yes
	trigger = {
		original_tag = USB
	}

	option = {
		name = post_united_event.2.o1
		log = "[GetDateText]: [This.GetName]: post_united_event.2.o1 executed"

		set_temp_variable = { party_index = 2 }
		set_temp_variable = { party_popularity_increase = 0.15 }
		set_temp_variable = { temp_outlook_increase = 0.15 }
		add_relative_party_popularity = yes

		set_temp_variable = { party_index = 4 }
		set_temp_variable = { party_popularity_increase = 0.15 }
		add_relative_party_popularity = yes

		set_temp_variable = { party_index = 18 }
		set_temp_variable = { party_popularity_increase = 0.15 }
		add_relative_party_popularity = yes

		set_temp_variable = { party_index = 23 }
		set_temp_variable = { party_popularity_increase = 0.15 }
		add_relative_party_popularity = yes

		country_event = {
			id = post_united_event.3
			days = 7
		}
	}
}

country_event = {
	id = post_united_event.3
	title = post_united_event.3.t
	desc = post_united_event.3.d
	picture = GFX_politics_negotiations
	is_triggered_only = yes
	trigger = {
		original_tag = USB
	}

	option = {
		name = post_united_event.3.o1
		log = "[GetDateText]: [This.GetName]: post_united_event.3.o1 executed"

		set_temp_variable = { party_index = 0 }
		set_temp_variable = { party_popularity_increase = 0.15 }
		set_temp_variable = { temp_outlook_increase = 0.15 }
		add_relative_party_popularity = yes

		set_temp_variable = { party_index = 5 }
		set_temp_variable = { party_popularity_increase = 0.15 }
		add_relative_party_popularity = yes

		set_temp_variable = { party_index = 15 }
		set_temp_variable = { party_popularity_increase = 0.15 }
		add_relative_party_popularity = yes

		set_temp_variable = { party_index = 22 }
		set_temp_variable = { party_popularity_increase = 0.15 }
		add_relative_party_popularity = yes

	}
}

country_event = {
	id = post_united_event.4
	title = post_united_event.4.t
	desc = post_united_event.4.d
	picture = GFX_election_day
	is_triggered_only = yes
	trigger = {
		original_tag = USB
	}

	option = {
		name = post_united_event.4.o1
		log = "[GetDateText]: [This.GetName]: post_united_event.4.o1 executed"
		set_politics = {
			ruling_party = democratic
			elections_allowed = no
		}
		clear_array = ruling_party
		clear_array = gov_coalition_array
		add_to_array = { ruling_party = 0 } #Federalist Right
		recalculate_party = yes
		set_ruling_leader = yes
		set_leader = yes
		set_variable = { current_term = 1 }
		set_temp_variable = { party_index = 0 }
		set_temp_variable = { party_popularity_increase = 0.55 }
		set_temp_variable = { temp_outlook_increase = 0.35 }
		add_relative_party_popularity = yes
		ai_chance = {
			base = 1
		}

	}
	option = {
		name = post_united_event.4.o2
		log = "[GetDateText]: [This.GetName]: post_united_event.4.o2 executed"
		set_politics = {
			ruling_party = communism
			elections_allowed = no
		}
		clear_array = ruling_party
		clear_array = gov_coalition_array
		add_to_array = { ruling_party = 4 } #Free Socialist Party Statist
		recalculate_party = yes
		set_ruling_leader = yes
		set_leader = yes
		set_variable = { current_term = 1 }
		set_temp_variable = { party_index = 4 }
		set_temp_variable = { party_popularity_increase = 0.55 }
		set_temp_variable = { temp_outlook_increase = 0.35 }
		add_relative_party_popularity = yes
		ai_chance = {
			base = 1
		}

	}
	option = {
		name = post_united_event.4.o3
		log = "[GetDateText]: [This.GetName]: post_united_event.4.o3 executed"
		set_politics = {
			ruling_party = neutrality
			elections_allowed = no
		}
		clear_array = ruling_party
		clear_array = gov_coalition_array
		add_to_array = { ruling_party = 15 } #Mid Atlantic Party Isolationist
		recalculate_party = yes
		set_ruling_leader = yes
		set_leader = yes
		set_variable = { current_term = 1 }
		set_temp_variable = { party_index = 15 }
		set_temp_variable = { party_popularity_increase = 0.55 }
		set_temp_variable = { temp_outlook_increase = 0.35 }
		add_relative_party_popularity = yes
		ai_chance = {
			base = 1
		}

	}
	option = {
		name = post_united_event.4.o4
		log = "[GetDateText]: [This.GetName]: post_united_event.4.o4 executed"
		set_politics = {
			ruling_party = nationalist
			elections_allowed = no
		}
		clear_array = ruling_party
		clear_array = gov_coalition_array
		add_to_array = { ruling_party = 22 } #National Service Party Militarist
		recalculate_party = yes
		set_ruling_leader = yes
		set_leader = yes
		set_variable = { current_term = 1 }
		set_temp_variable = { party_index = 22 }
		set_temp_variable = { party_popularity_increase = 0.55 }
		set_temp_variable = { temp_outlook_increase = 0.35 }
		add_relative_party_popularity = yes
		ai_chance = {
			base = 1
		}

	}
}

country_event = {
	id = post_united_event.5
	title = post_united_event.5.t
	desc = post_united_event.5.d
	picture = GFX_election_day
	is_triggered_only = yes
	trigger = {
		original_tag = USB
	}

	option = {
		name = post_united_event.5.o1
		log = "[GetDateText]: [This.GetName]: post_united_event.5.o1 executed"
		set_politics = {
			ruling_party = democratic
			elections_allowed = yes
		}
		clear_array = ruling_party
		clear_array = gov_coalition_array
		add_to_array = { ruling_party = 2 } #Federalist left
		recalculate_party = yes
		set_ruling_leader = yes
		set_leader = yes
		set_variable = { current_term = 1 }
		set_temp_variable = { party_index = 2 }
		set_temp_variable = { party_popularity_increase = 0.55 }
		set_temp_variable = { temp_outlook_increase = 0.35 }
		add_relative_party_popularity = yes
		ai_chance = {
			base = 1
		}

	}
	option = {
		name = post_united_event.5.o2
		log = "[GetDateText]: [This.GetName]: post_united_event.5.o2 executed"
		set_politics = {
			ruling_party = communism
			elections_allowed = yes
		}
		clear_array = ruling_party
		clear_array = gov_coalition_array
		add_to_array = { ruling_party = 5 } #Free Socialist Party anarchist
		recalculate_party = yes
		set_ruling_leader = yes
		set_leader = yes
		set_variable = { current_term = 1 }
		set_temp_variable = { party_index = 5 }
		set_temp_variable = { party_popularity_increase = 0.55 }
		set_temp_variable = { temp_outlook_increase = 0.35 }
		add_relative_party_popularity = yes
		ai_chance = {
			base = 1
		}

	}
	option = {
		name = post_united_event.5.o3
		log = "[GetDateText]: [This.GetName]: post_united_event.5.o3 executed"
		set_politics = {
			ruling_party = neutrality
			elections_allowed = yes
		}
		clear_array = ruling_party
		clear_array = gov_coalition_array
		add_to_array = { ruling_party = 16 } #Mid Atlantic Party Idenitarians
		recalculate_party = yes
		set_ruling_leader = yes
		set_leader = yes
		set_variable = { current_term = 1 }
		set_temp_variable = { party_index = 16 }
		set_temp_variable = { party_popularity_increase = 0.55 }
		set_temp_variable = { temp_outlook_increase = 0.35 }
		add_relative_party_popularity = yes
		ai_chance = {
			base = 1
		}

	}
	option = {
		name = post_united_event.5.o4
		log = "[GetDateText]: [This.GetName]: post_united_event.5.o4 executed"
		set_politics = {
			ruling_party = nationalist
			elections_allowed = yes
		}
		clear_array = ruling_party
		clear_array = gov_coalition_array
		add_to_array = { ruling_party = 23 } #National Service Party Monarchists
		recalculate_party = yes
		set_ruling_leader = yes
		set_leader = yes
		set_variable = { current_term = 1 }
		set_temp_variable = { party_index = 23 }
		set_temp_variable = { party_popularity_increase = 0.55 }
		set_temp_variable = { temp_outlook_increase = 0.35 }
		add_relative_party_popularity = yes
		ai_chance = {
			base = 1
		}

	}
}


country_event = {
	id = post_united_event.1000
	title = post_united_event.1000.t
	desc = post_united_event.1000.d
	picture = GFX_court
	is_triggered_only = yes
	trigger = {
		original_tag = USB
	}

	option = {
		name = post_united_event.1000.o1
		log = "[GetDateText]: [This.GetName]: post_united_event.1000.o1 executed"
		add_political_power = -300
		add_stability = 0.25
		add_war_support = 0.10
		set_cosmetic_tag = USB_Arcadia

		set_temp_variable = { party_index = 23 }
		set_temp_variable = { party_popularity_increase = 0.50 }
		set_temp_variable = { temp_outlook_increase = 0.30 }
		add_relative_party_popularity = yes
		set_temp_variable = { temp_change = 0.10 }
		FS_change_domestic_nationalist_drift_change = yes

		ai_chance = { base = 1 }

	}

	option = {
		name = post_united_event.1000.o2
		log = "[GetDateText]: [This.GetName]: post_united_event.1000.o2 executed"
		add_political_power = 150
		add_stability = 0.10
		add_war_support = 0.25
		set_temp_variable = { party_index = 2 }
		set_temp_variable = { party_popularity_increase = 0.10 }
		set_temp_variable = { temp_outlook_increase = 0.10 }
		set_temp_variable = { party_index = 5 }
		set_temp_variable = { party_popularity_increase = 0.10 }
		set_temp_variable = { temp_outlook_increase = 0.10 }
		set_temp_variable = { party_index = 16 }
		set_temp_variable = { party_popularity_increase = 0.10 }
		set_temp_variable = { temp_outlook_increase = 0.10 }
		add_relative_party_popularity = yes
		set_temp_variable = { temp_change = -0.05 }
		FS_change_domestic_nationalist_drift_change = yes
		set_temp_variable = { temp_change = 0.03 }
		FS_change_domestic_democratic_drift_change = yes
		FS_change_domestic_communist_drift_change = yes
		FS_change_domestic_neutrality_drift_change = yes

		ai_chance = { base = 1 }

	}
}

country_event = {
	id = post_united_event.2000
	title = post_united_event.2000.t
	desc = post_united_event.2000.d
	picture = GFX_court
	is_triggered_only = yes
	trigger = {
		original_tag = USB
	}

	option = {
		name = post_united_event.2000.o1
		log = "[GetDateText]: [This.GetName]: post_united_event.2000.o1 executed"
		804 = { add_extra_state_shared_building_slots = 2 }
		784 = { add_extra_state_shared_building_slots = 2 }
		783 = { add_extra_state_shared_building_slots = 2 }
		country_event = {
			id = post_united_event.2001
			days = 7
		}

		ai_chance = { base = 1 }
	}
}

country_event = {
	id = post_united_event.2001
	title = post_united_event.2001.t
	desc = post_united_event.2001.d
	picture = GFX_court
	is_triggered_only = yes
	trigger = {
		original_tag = USB
	}

	option = {
		name = post_united_event.2001.o1
		log = "[GetDateText]: [This.GetName]: post_united_event.2001.o1 executed"
		set_victory_points = {
			province = 3869
			value = 3
		}
		country_event = {
			id = post_united_event.2002
			days = 7
		}

		ai_chance = { base = 1 }
	}
}

country_event = {
	id = post_united_event.2002
	title = post_united_event.2002.t
	desc = post_united_event.2002.d
	picture = GFX_court
	is_triggered_only = yes
	trigger = {
		original_tag = USB
	}

	option = {
		name = post_united_event.2002.o1
		log = "[GetDateText]: [This.GetName]: post_united_event.2002.o1 executed"
		set_victory_points = {
			province = 1753
			value = 3
		}
		country_event = {
			id = post_united_event.2003
			days = 7
		}

		ai_chance = { base = 1 }
	}
}

country_event = {
	id = post_united_event.2003
	title = post_united_event.2003.t
	desc = post_united_event.2003.d
	picture = GFX_court
	is_triggered_only = yes
	trigger = {
		original_tag = USB
	}

	option = {
		name = post_united_event.2003.o1
		log = "[GetDateText]: [This.GetName]: post_united_event.2003.o1 executed"
		set_victory_points = {
			province = 1523
			value = 3
		}
		country_event = {
			id = post_united_event.2004
			days = 7
		}

		ai_chance = { base = 1 }
	}
}

country_event = {
	id = post_united_event.2004
	title = post_united_event.2004.t
	desc = post_united_event.2004.d
	picture = GFX_court
	is_triggered_only = yes
	trigger = {
		original_tag = USB
	}

	option = {
		name = post_united_event.2004.o1
		log = "[GetDateText]: [This.GetName]: post_united_event.2004.o1 executed"
		add_political_power = -100
		add_war_support = -0.07
		set_province_name = {
			id = 3869
			name = USA_colorado_capital
		}
		set_province_name = {
			id = 1753
			name = USA_kansas_capital
		}
		set_province_name = {
			id = 1523
			name = USA_missouri_capital
		}
		ai_chance = { base = 1 }
	}

	option = {
		name = post_united_event.2004.o2
		log = "[GetDateText]: [This.GetName]: post_united_event.2004.o2 executed"
		add_political_power = 50
		add_stability = 0.03
		set_victory_points = {
			province = 3869
			value = 25
		}
		set_province_name = {
			id = 3869
			name = USA_colorado_capital
		}
		set_province_name = {
			id = 1753
			name = USA_kansas_capital
		}
		set_province_name = {
			id = 1523
			name = USA_missouri_capital
		}
		set_capital = { state = 804 }

		ai_chance = { base = 1 }

	}
	option = {
		name = post_united_event.2004.o3
		log = "[GetDateText]: [This.GetName]: post_united_event.2004.o3 executed"
		add_political_power = 25
		add_stability = 0.07
		set_victory_points = {
			province = 1753
			value = 25
		}
		set_province_name = {
			id = 1753
			name = USA_kansas_capital
		}
		set_province_name = {
			id = 3869
			name = USA_colorado_capital
		}
		set_province_name = {
			id = 1523
			name = USA_missouri_capital
		}
		set_capital = { state = 784 }

		ai_chance = { base = 1 }

	}
	option = {
		name = post_united_event.2004.o4
		log = "[GetDateText]: [This.GetName]: post_united_event.2004.o4 executed"
		add_political_power = 50
		add_stability = 0.03
		set_victory_points = {
			province = 1523
			value = 25
		}
		set_province_name = {
			id = 1523
			name = USA_missouri_capital
		}
		set_province_name = {
			id = 3869
			name = USA_colorado_capital
		}
		set_province_name = {
			id = 1753
			name = USA_kansas_capital
		}
		set_capital = { state = 783 }

		ai_chance = { base = 1 }

	}
}

country_event = {
	id = post_united_event.2005
	title = post_united_event.2005.t
	desc = post_united_event.2005.d
	picture = GFX_american_flag
	is_triggered_only = yes
	trigger = {
		original_tag = USB
	}

	option = {
		name = post_united_event.2005.o1
		log = "[GetDateText]: [This.GetName]: post_united_event.2005.o1 executed"

			###################################
			##### Domestic Policies ###########
			###################################
			add_dynamic_modifier = { modifier = free_states_domestic_policies }
			set_temp_variable = { temp_change = 0.10 }
			FS_change_domestic_unemployment_tolerance_change = yes
			set_temp_variable = { temp_change = 0.14 }
			FS_change_domestic_research_speed_change = yes
			set_temp_variable = { temp_change = 0.01 }
			FS_change_domestic_democratic_drift_change = yes
			FS_change_domestic_neutrality_drift_change = yes
			FS_change_domestic_communist_drift_change = yes
			FS_change_domestic_nationalist_drift_change = yes


			###################################################
			##### Department of Resource Management ###########
			###################################################
			add_dynamic_modifier = { modifier = free_states_resource_management_policies }
			set_temp_variable = { temp_change = -0.50 }
			FS_change_resource_management_mil_factory_workforce_change = yes
			set_temp_variable = { temp_change = -0.23 }
			FS_change_resource_management_office_workforce_change = yes
			set_temp_variable = { temp_change = 0.33 }
			FS_change_resource_management_civ_factory_workforce_change = yes
			set_temp_variable = { temp_change = -0.28 }
			FS_change_resource_management_agriculture_workforce_change = yes
			set_temp_variable = { temp_change = 0.73 }
			FS_change_resource_management_resource_extraction_workforce_change = yes
			set_temp_variable = { temp_change = 0.58 }
			FS_change_resource_management_resource_extraction_change = yes
			set_temp_variable = { temp_change = 1.225 }
			FS_change_resource_management_productivity_change = yes
			set_temp_variable = { temp_change = 0.10 }
			FS_change_resource_management_workforce_change = yes


			###################################################
			##### Department of Industrial Affairs ############
			###################################################
			add_dynamic_modifier = { modifier = free_states_industrial_policies }
			set_temp_variable = { temp_change = 0.34 }
			FS_change_industrial_construction_speed_change = yes
			set_temp_variable = { temp_change = 0.29 }
			FS_change_industrial_factory_output_change = yes
			set_temp_variable = { temp_change = 0.41 }
			FS_change_industrial_dockyard_output_change = yes
			set_temp_variable = { temp_change = 0.50 }
			FS_change_industrial_production_efficiency_cap_change = yes
			set_temp_variable = { temp_change = 0.46 }
			FS_change_industrial_energy_production_change = yes
			set_temp_variable = { temp_change = -0.37 }
			FS_change_industrial_energy_consumption_change = yes
			set_temp_variable = { temp_change = -0.05 }
			FS_change_industrial_consumer_goods_change = yes
			set_temp_variable = { temp_change = -0.25 }
			FS_change_industrial_population_energy_consumption_change = yes


			###########################
			##### Tax Code ############
			###########################
			add_dynamic_modifier = { modifier = free_states_tax_policies }
			set_temp_variable = { temp_change = 0.10 }
			FS_change_tax_military_tax_collection_efficiency_change = yes
			set_temp_variable = { temp_change = 0.17 }
			FS_change_tax_dockyard_tax_collection_efficiency_change = yes
			set_temp_variable = { temp_change = 0.05 }
			FS_change_tax_civilian_tax_collection_efficiency_change = yes
			set_temp_variable = { temp_change = 0.07 }
			FS_change_tax_office_park_tax_collection_efficiency_change = yes
			set_temp_variable = { temp_change = 0.13 }
			FS_change_tax_corporate_tax_collection_efficiency_change = yes
			set_temp_variable = { temp_change = -1.00 }
			FS_change_tax_interest_rate_change = yes


			##################################
			##### Federal Budgets ############
			##################################
			add_dynamic_modifier = { modifier = free_states_federal_budgets }
			set_temp_variable = { temp_change = 0.84 }
			FS_change_budget_bureaucracy_cost_change = yes
			set_temp_variable = { temp_change = 0.27 }
			FS_change_budget_personnel_cost_change = yes
			set_temp_variable = { temp_change = 0.14 }
			FS_change_budget_police_cost_change = yes
			set_temp_variable = { temp_change = 0.33 }
			FS_change_budget_education_cost_change = yes
			set_temp_variable = { temp_change = -0.07 }
			FS_change_budget_healthcare_cost_change = yes
			set_temp_variable = { temp_change = -0.12 }
			FS_change_budget_welfare_cost_change = yes


			##################################
			##### Army Branch ################
			##################################
			add_dynamic_modifier = { modifier = free_states_army_service_branch }
			set_temp_variable = { temp_change = 0.22 }
			FS_change_army_supply_consumption_factor_change = yes
			set_temp_variable = { temp_change = 0.10 }
			FS_change_army_army_training_time_change = yes
			set_temp_variable = { temp_change = 0.07 }
			FS_change_army_army_attack_change = yes
			set_temp_variable = { temp_change = 0.13 }
			FS_change_army_army_defence_change = yes
			set_temp_variable = { temp_change = 0.18 }
			FS_change_army_army_org_change = yes
			set_temp_variable = { temp_change = 0.16 }
			FS_change_army_breakthrough_factor_change = yes
			set_temp_variable = { temp_change = 0.010 }
			FS_change_army_conscription_change = yes
			set_temp_variable = { temp_change = 0.50 }
			FS_change_army_max_planning_factor_change = yes


			##################################
			##### Navy Branch ################
			##################################
			add_dynamic_modifier = { modifier = free_states_navy_service_branch }
			set_temp_variable = { temp_change = 0.37 }
			FS_change_navy_repair_speed_factor_change = yes
			set_temp_variable = { temp_change = 0.18 }
			FS_change_navy_naval_morale_factor_change = yes
			set_temp_variable = { temp_change = 0.10 }
			FS_change_navy_anti_air_attack_factor_change = yes
			set_temp_variable = { temp_change = 0.14 }
			FS_change_navy_naval_damage_change = yes
			set_temp_variable = { temp_change = 0.07 }
			FS_change_navy_naval_defense_change = yes
			set_temp_variable = { temp_change = 0.20 }
			FS_change_navy_naval_speed_change = yes


			##################################
			##### Air Force Branch ###########
			##################################
			add_dynamic_modifier = { modifier = free_states_air_force_service_branch }
			set_temp_variable = { temp_change = 0.25 }
			FS_change_airforce_air_mission_efficiency_change = yes
			set_temp_variable = { temp_change = 0.09 }
			FS_change_airforce_air_attack_change = yes
			set_temp_variable = { temp_change = 0.14 }
			FS_change_airforce_air_defence_change = yes
			set_temp_variable = { temp_change = 0.20 }
			FS_change_airforce_air_agility_change = yes
			set_temp_variable = { temp_change = 0.31 }
			FS_change_airforce_ground_attack_factor_change = yes

		ai_chance = { base = 1 }
	}
}

country_event = {
	id = post_united_event.2006
	title = post_united_event.2006.t
	desc = post_united_event.2006.d
	picture = GFX_court
	is_triggered_only = yes
	trigger = {
		original_tag = USB
	}

	option = {
		name = post_united_event.2006.o1
		log = "[GetDateText]: [This.GetName]: post_united_event.2006.o1 executed"
		transfer_state = 1083
		add_state_core = 1083

		ai_chance = { base = 1 }
	}
}

