add_namespace = migrant_event

# Migrant Slums
country_event = {
	id = migrant_event.1
	title = migrant_event.1.t
	desc = migrant_event.1.d
	# TODO: Add a picture
	is_triggered_only = yes
	trigger = {
		NOT = { has_country_flag = recently_had_migrant_slums_event }
		NOT = { has_idea = closed_borders }
		check_variable = { net_immigration_rate > 0 }
	}

	mean_time_to_happen = {
		modifier = {
			factor = 1.15
			has_idea = heavily_regulated_immigration
		}
		modifier = {
			factor = 1.25
			has_idea = regulated_immigration
		}
		modifier = {
			factor = 1.5
			has_idea = slightly_regulated_immigration
		}
		modifier = {
			factor = 1.75
			has_idea = open_borders
		}
		modifier = {
			factor = months_at_open_borders
			always = yes
		}
	}

	immediate = {
		hidden_effect = {
			set_country_flag = { flag = recently_had_migrant_slums_event value = 1 days = 180 }
		}
	}

	# Breakup the Slums
	option = {
		name = migrant_event.1.a
		log = "[GetDateText]: [ROOT.GetName]: event migrant_event.1.a executed"
		set_temp_variable = { party_index = 21 }
		set_temp_variable = { party_popularity_increase = 0.02 }
		set_temp_variable = { temp_outlook_increase = 0.02 }
		add_relative_party_popularity = yes
		add_stability = -0.04
		ai_chance = {
			base = 1
			modifier = {
				factor = 0
				OR = {
					has_liberal_government = yes
					has_environmentalist_government = yes
					has_socialist_government = yes
				}
			}
			modifier = {
				factor = 1.25
				has_autocratic_government = yes
			}
		}
	}

	# Provide Assistance
	option = {
		name = migrant_event.1.b
		log = "[GetDateText]: [ROOT.GetName]: event migrant_event.1.b executed"
		add_political_power = -75

		set_party_index_to_ruling_party = yes
		set_temp_variable = { party_popularity_increase = 0.02 }
		set_temp_variable = { temp_outlook_increase = 0.02 }
		add_relative_party_popularity = yes
		set_temp_variable = { party_index = 21 }
		set_temp_variable = { party_popularity_increase = -0.03 }
		set_temp_variable = { temp_outlook_increase = -0.03 }
		add_relative_party_popularity = yes
		ai_chance = {
			base = 1
			modifier = {
				factor = 1.25
				OR = {
					has_liberal_government = yes
					has_environmentalist_government = yes
					has_socialist_government = yes
				}
			}
			modifier = {
				factor = 0
				has_autocratic_government = yes
			}
		}
	}

	# Reduce Migration Targets
	option = {
		name = migrant_event.1.c
		log = "[GetDateText]: [ROOT.GetName]: event migrant_event.1.c executed"
		add_political_power = -75
		set_party_index_to_ruling_party = yes
		set_temp_variable = { party_popularity_increase = 0.02 }
		set_temp_variable = { temp_outlook_increase = 0.02 }
		add_relative_party_popularity = yes

		increase_migration_law = yes
		block_migration_law_decrease = yes
		ai_chance = {
			base = 1
			modifier = {
				factor = 1.25
				has_totalitarian_government = yes
			}
		}
	}
}

# Nationalists Protest Migrant Policy
country_event = {
	id = migrant_event.2
	title = migrant_event.2.t
	desc = migrant_event.2.d
	# TODO: Add a picture
	is_triggered_only = yes
	trigger = {
		NOT = { has_country_flag = recently_had_right_wing_protest }
		NOT = { has_idea = closed_borders }
		check_variable = { net_immigration_rate > 0 }
	}

	mean_time_to_happen = {
		modifier = {
			factor = 0.45
			has_idea = heavily_regulated_immigration
		}
		modifier = {
			factor = 0.75
			has_idea = regulated_immigration
		}
		modifier = {
			factor = 1.15
			has_idea = slightly_regulated_immigration
		}
		modifier = {
			factor = 1.5
			has_idea = open_borders
		}
		modifier = {
			factor = 1.05
			check_variable = { net_immigration_rate > 0.05 }
		}
		modifier = {
			factor = 1.15
			check_variable = { net_immigration_rate > 0.10 }
		}
		modifier = {
			factor = 1.25
			check_variable = { net_immigration_rate > 0.15 }
		}
		modifier = {
			factor = 1.35
			check_variable = { net_immigration_rate > 0.20 }
		}
		modifier = {
			factor = 1.5
			check_variable = { net_immigration_rate > 0.25 }
		}
		modifier = {
			factor = 1.2
			nationalist > 0.1
		}
		modifier = {
			factor = 1.2
			nationalist > 0.2
		}
		modifier = {
			factor = months_at_open_borders
			always = yes
		}
	}

	immediate = {
		hidden_effect = {
			set_country_flag = { flag = recently_had_right_wing_protest value = 1 days = 90 }
		}
	}

	# Allow the Demonstration
	option = {
		name = migrant_event.2.a
		log = "[GetDateText]: [ROOT.GetName]: event migrant_event.2.a executed"
		set_temp_variable = { party_index = 21 }
		set_temp_variable = { party_popularity_increase = 0.02 }
		set_temp_variable = { temp_outlook_increase = 0.02 }
		add_relative_party_popularity = yes
		add_stability = -0.03

		ai_chance = {
			base = 1
			modifier = {
				factor = 0
				OR = {
					has_liberal_government = yes
					has_environmentalist_government = yes
					has_socialist_government = yes
				}
			}
			modifier = {
				factor = 1.25
				has_autocratic_government = yes
			}
		}
	}

	# Breakup the Protest
	option = {
		name = migrant_event.2.b
		log = "[GetDateText]: [ROOT.GetName]: event migrant_event.2.b executed"
		add_political_power = -50
		add_stability = -0.015

		set_party_index_to_ruling_party = yes
		set_temp_variable = { party_popularity_increase = -0.02 }
		set_temp_variable = { temp_outlook_increase = -0.02 }
		add_relative_party_popularity = yes
		set_temp_variable = { party_index = 21 }
		set_temp_variable = { party_popularity_increase = -0.02 }
		set_temp_variable = { temp_outlook_increase = -0.02 }
		add_relative_party_popularity = yes
		ai_chance = {
			base = 1
			modifier = {
				factor = 1.25
				OR = {
					has_liberal_government = yes
					has_environmentalist_government = yes
					has_socialist_government = yes
				}
			}
			modifier = {
				factor = 0
				has_autocratic_government = yes
			}
		}
	}
}

# Migrants Accused of Violent Crimes
country_event = {
	id = migrant_event.3
	title = migrant_event.3.t
	desc = migrant_event.3.d
	# TODO: Add a picture
	is_triggered_only = yes
	trigger = {
		NOT = { has_country_flag = recently_had_migrant_crime }
		NOT = { has_idea = closed_borders }
		check_variable = { net_immigration_rate > 0 }
	}

	mean_time_to_happen = {
		modifier = {
			factor = 0.5
			has_idea = police_05
		}
		modifier = {
			factor = 0.75
			has_idea = police_04
		}
		modifier = {
			factor = 1.15
			has_idea = police_03
		}
		modifier = {
			factor = 1.25
			has_idea = police_02
		}
		modifier = {
			factor = 1.50
			has_idea = police_01
		}
		modifier = {
			factor = 1.25
			has_idea = open_borders
		}
		modifier = {
			factor = 1.15
			has_idea = slightly_regulated_immigration
		}
		modifier = {
			factor = months_at_open_borders
			always = yes
		}
	}

	immediate = {
		hidden_effect = {
			set_country_flag = { flag = recently_had_migrant_crime value = 1 days = 180 }
		}
	}

	# They told us so!
	option = {
		name = migrant_event.3.a
		log = "[GetDateText]: [ROOT.GetName]: event migrant_event.3.a executed"
		set_temp_variable = { party_index = 21 }
		set_temp_variable = { party_popularity_increase = 0.04 }
		set_temp_variable = { temp_outlook_increase = 0.04 }
		add_relative_party_popularity = yes
		add_stability = -0.01

		ai_chance = {
			base = 1
			modifier = {
				factor = 0
				OR = {
					has_liberal_government = yes
					has_environmentalist_government = yes
					has_socialist_government = yes
				}
			}
			modifier = {
				factor = 1.25
				has_autocratic_government = yes
			}
		}
	}

	# Increasing police funding may be the way to go...
	option = {
		name = migrant_event.3.b
		log = "[GetDateText]: [ROOT.GetName]: event migrant_event.3.b executed"
		increase_policing_budget = yes
		block_police_decrease = yes

		ai_chance = {
			base = 1
			modifier = {
				factor = 1.25
				OR = {
					has_liberal_government = yes
					has_environmentalist_government = yes
					has_socialist_government = yes
				}
			}
			modifier = {
				factor = 0
				has_autocratic_government = yes
			}
		}
	}
}

