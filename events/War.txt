﻿#Written by hiddengearz, edited and improved by Angriest Bird
add_namespace = anti_bully
add_namespace = civil_war_debuff

################### WAR 1 ############################
#Used just to trigger event 2, event 1 does not seem to work immediately via on action
country_event = {
	id = anti_bully.1
	immediate = { log = "[GetDateText]: [Root.GetName]: event anti_bully.1" }
	title = anti_bully.1.t
	desc = anti_bully.1.d
	hidden = yes
	is_triggered_only = yes
	immediate = {
		country_event = { id = anti_bully.2 days = 1 }
	}
}

#Triggers the Public War Weariness System
country_event = {
	id = anti_bully.2
	title = anti_bully.2.t
	desc = anti_bully.2.d
	is_triggered_only = yes
	trigger = {
		THIS = {
			has_offensive_war_with = event_target:AB_DEFF
		}
	}
	immediate = {
		hidden_effect = {
			if = { limit = { check_variable = { anti_bully_wars = 0 } } #Has 0 AB wars
				event_target:AB_ATT = {
					add_to_variable = { anti_bully_wars = 1 }
					AB_war_main = yes
				}
				else = {
					event_target:AB_ATT = {
						add_to_variable = { anti_bully_wars = 1 }
					}
				}
			}
			event_target:AB_ATT = {
				AB_war_second = yes
			}
		}
	}
	option = {
		name = anti_bully.2.a
		log = "[GetDateText]: [This.GetName]: anti_bully.2.a executed"
	}
}

country_event = {
	id = anti_bully.3
	title = anti_bully.3.t
	desc = anti_bully.3.d
	hidden = yes
	is_triggered_only = yes
	immediate = {
		AB_war_main = yes
	}
	option = {
		name = anti_bully.3.a
		log = "[GetDateText]: [This.GetName]: anti_bully.3.a executed" # War Support is Dwindling
	}
}

#Attacker offers white peace
country_event = {
	id = anti_bully.4
	title = anti_bully.4.t
	desc = anti_bully.4.d
	hidden = yes
	is_triggered_only = yes
	immediate = {
		AB_war_second = yes
	}
	option = { #attacker offers white peace
		name = anti_bully.4.a
		log = "[GetDateText]: [This.GetName]: anti_bully.4.a executed"
	}
}

#Defender accepts/declines white peace
country_event = {
	id = anti_bully.5
	title = anti_bully.5.t
	desc = anti_bully.5.d
	picture = GFX_politics_talks
	is_triggered_only = yes

	option = {
		name = anti_bully.5.a
		log = "[GetDateText]: [This.GetName]: anti_bully.5.a executed" #Accept & war ends
		every_state = {
			limit = {
				is_owned_by = ROOT
				is_controlled_by = FROM
			}
			FROM = { transfer_state = PREV }
		}
		FROM = { country_event = { id = anti_bully.6 } }
		white_peace = FROM
		#hidden_effect = { clr_country_flag = stalemate@FROM }
		if = {
			limit = { #If you're in a faction, not a major and not the leader
				event_target:AB_event_target = {
					is_in_faction = yes
					is_faction_leader = no
				}
				any_country = { #Faction leader must have capitulated for me to see this decision
					is_in_faction_with = event_target:AB_event_target
					has_war_together_with = event_target:AB_event_target
					is_faction_leader = yes
					has_capitulated = no
				}
			}
			event_target:AB_event_target = { leave_faction = yes }
			if = {
				limit = { event_target:AB_event_target = { has_idea = NATO_member } }
				event_target:AB_event_target = { NATO_leave = yes }
			}
		}
		ai_chance = {
			factor = 1
			modifier = {
				add = 2
				ROOT = { surrender_progress > 0.20 }
			}
			modifier = {
				add = 2
				ROOT = { surrender_progress > 0.30 }
			}
			modifier = {
				add = 2
				ROOT = { surrender_progress > 0.40 }
			}
			modifier = {
				add = 2
				ROOT = { surrender_progress > 0.50 }
			}
			modifier = {
				add = 2
				ROOT = { surrender_progress > 0.60 }
			}
			modifier = {
				add = 2
				ROOT = { surrender_progress > 0.70 }
			}
			modifier = {
				add = 2
				ROOT = { surrender_progress > 0.80 }
			}
			modifier = {
				add = 2
				ROOT = { surrender_progress > 0.90 }
			}
			modifier = {
				factor = 5
				OR = {
					strength_ratio = { tag = FROM ratio > 1.5 }
					has_stability > 0.35
					has_war_support < 0.30
				}
			}
		}
	}

	option = {
		name = anti_bully.5.b
		log = "[GetDateText]: [This.GetName]: anti_bully.5.b executed" #Decline the Ceasefire!
		FROM = { country_event = { id = anti_bully.7 days = 1 } }
		ai_chance = {
			factor = 1
			modifier = {
				add = 2
				surrender_progress < 0.20
			}
			modifier = {
				add = 2
				FROM = { surrender_progress > 0.20 }
			}
			modifier = {
				add = 4
				FROM = { surrender_progress > 0.40 }
			}
			modifier = {
				add = 4
				FROM = { surrender_progress > 0.60 }
			}
			modifier = {
				add = 4
				FROM = { surrender_progress > 0.80 }
			}
			modifier = {
				factor = 5
				NOT = {
					has_stability > 0.35
					has_war_support < 0.30
					strength_ratio = { tag = FROM ratio > 1.5 }
				}
			}
		}
	}
}

country_event = { #Accepts Ceasefire
	id = anti_bully.6
	title = anti_bully.6.t
	desc = anti_bully.6.d
	picture = GFX_politics_talks
	is_triggered_only = yes

	option = {
		name = anti_bully.6.a
		log = "[GetDateText]: [This.GetName]: anti_bully.7.a executed"
		custom_effect_tooltip = accepted_ceasefire_tt
	}
}

country_event = { #Decline Ceasefire
	id = anti_bully.7
	title = anti_bully.7.t
	desc = anti_bully.7.d
	picture = GFX_treaty_rejected
	is_triggered_only = yes

	option = {
		name = anti_bully.7.a
		log = "[GetDateText]: [This.GetName]: anti_bully.7.a executed"
		FROM = { add_timed_idea = { idea = recent_ceasefire_attempt days = 30 } }
		add_timed_idea = { idea = recent_ceasefire_attempt days = 30 }
	}
}

#Civil war debuff improvements
country_event = { 
	id = civil_war_debuff.1
	title = civil_war_debuff.1.t
	desc = civil_war_debuff.1.d
	picture = GFX_treaty_rejected

	trigger = {
		NOT = { has_country_flag = manual_civil_war_debuff_removal }
	}
	is_triggered_only = yes

	option = {
		name = civil_war_debuff.1.a
		log = "[GetDateText]: [This.GetName]: civil_war_debuff.1.a"
		add_timed_idea = {
			idea = civil_war_disorganised_military_2_military
			days = 150
		}
	}
	option = {
		name = civil_war_debuff.1.b
		log = "[GetDateText]: [This.GetName]: civil_war_debuff.1.b"
		add_timed_idea = {
			idea = civil_war_disorganised_military_2_production
			days = 150
		}
	}
	option = {
		name = civil_war_debuff.1.c
		log = "[GetDateText]: [This.GetName]: civil_war_debuff.1.c"
		add_timed_idea = {
			idea = civil_war_disorganised_military_2_experience
			days = 150
		}
	}
}
country_event = { 
	id = civil_war_debuff.2
	title = civil_war_debuff.2.t
	desc = civil_war_debuff.2.d
	picture = GFX_treaty_rejected

	trigger = {
		NOT = { has_country_flag = manual_civil_war_debuff_removal }
	}
	is_triggered_only = yes

	option = {
		name = civil_war_debuff.2.a
		log = "[GetDateText]: [This.GetName]: civil_war_debuff.2.a"
		add_timed_idea = {
			idea = civil_war_disorganised_military_3_military
			days = 150
		}
	}
	option = {
		name = civil_war_debuff.2.b
		log = "[GetDateText]: [This.GetName]: civil_war_debuff.2.b"
		add_timed_idea = {
			idea = civil_war_disorganised_military_3_production
			days = 150
		}
	}
	option = {
		name = civil_war_debuff.2.c
		log = "[GetDateText]: [This.GetName]: civil_war_debuff.2.c"
		add_timed_idea = {
			idea = civil_war_disorganised_military_3_experience
			days = 150
		}
	}
}
country_event = { 
	id = civil_war_debuff.3
	title = civil_war_debuff.3.t
	desc = civil_war_debuff.3.d
	picture = GFX_treaty_rejected

	trigger = {
		NOT = { has_country_flag = manual_civil_war_debuff_removal }
	}
	is_triggered_only = yes

	option = {
		name = civil_war_debuff.3.a
		log = "[GetDateText]: [This.GetName]: civil_war_debuff.3.a"
		add_timed_idea = {
			idea = civil_war_disorganised_military_4_military
			days = 150
		}
	}
	option = {
		name = civil_war_debuff.3.b
		log = "[GetDateText]: [This.GetName]: civil_war_debuff.3.b"
		add_timed_idea = {
			idea = civil_war_disorganised_military_4_production
			days = 150
		}
	}
	option = {
		name = civil_war_debuff.3.c
		log = "[GetDateText]: [This.GetName]: civil_war_debuff.3.c"
		add_timed_idea = {
			idea = civil_war_disorganised_military_4_experience
			days = 150
		}
	}
}
country_event = { 
	id = civil_war_debuff.4
	title = civil_war_debuff.4.t
	desc = civil_war_debuff.4.d
	picture = GFX_treaty_rejected

	trigger = {
		NOT = { has_country_flag = manual_civil_war_debuff_removal }
	}
	is_triggered_only = yes

	option = {
		name = civil_war_debuff.4.a
		log = "[GetDateText]: [This.GetName]: civil_war_debuff.4.a"
		add_timed_idea = {
			idea = civil_war_disorganised_military_5_military
			days = 150
		}
	}
	option = {
		name = civil_war_debuff.4.b
		log = "[GetDateText]: [This.GetName]: civil_war_debuff.4.b"
		add_timed_idea = {
			idea = civil_war_disorganised_military_5_production
			days = 150
		}
	}
	option = {
		name = civil_war_debuff.4.c
		log = "[GetDateText]: [This.GetName]: civil_war_debuff.4.c"
		add_timed_idea = {
			idea = civil_war_disorganised_military_5_experience
			days = 150
		}
	}
}
country_event = { 
	id = civil_war_debuff.5
	title = civil_war_debuff.5.t
	desc = civil_war_debuff.5.d
	picture = GFX_Military_Reform

	trigger = {
		NOT = { has_country_flag = manual_civil_war_debuff_removal }
	}
	is_triggered_only = yes

	option = {
		name = civil_war_debuff.5.a
		log = "[GetDateText]: [This.GetName]: civil_war_debuff.5.a"
	}
}