﻿add_namespace = lrp
#LRP wants to join Poland
country_event = {
	id = lrp.1
	title = lrp.1.t
	desc = lrp.1.d
	is_triggered_only = yes
	picture = GFX_plc_proposal
	option = {
		name = lrp.1.a
		log = "[GetDateText]: [This.GetName]: lrp.1.a executed"
		set_autonomy = {
			target = LRP
			autonomy_state = autonomy_autonomous_state
		}
		LRP = {
			add_opinion_modifier = { target = ROOT modifier = has_expressed_loyalty }
			reverse_add_opinion_modifier = { target = ROOT modifier = has_expressed_loyalty }
		}
		ai_chance = {
			base = 100
		}
	}
	option = {
		name = lrp.1.b
		log = "[GetDateText]: [This.GetName]: lrp.1.b executed"
		ai_chance = {
			base = 0
		}
	}
}