add_namespace = pmc_events

country_event = {
	id = pmc_events.1
	title = pmc_events.1.t
	desc = pmc_events.1.d
	picture = GFX_news_generic_soldiers

	is_triggered_only = yes

	option = {
		name = pmc_events.1.a
		log = "[GetDateText]: [This.GetName]: pmc_events.1.a executed"
	}
}
country_event = {
	id = pmc_events.2
	title = pmc_events.2.t
	desc = pmc_events.2.d
	picture = GFX_event_pmcs
	is_triggered_only = yes

	option = {
		name = pmc_events.2.a
		log = "[GetDateText]: [This.GetName]: pmc_events.2.a executed"
		set_temp_variable = { treasury_change = 5.5 }
		modify_treasury_effect = yes
		set_temp_variable = { percent_change = 1.5 }
		set_temp_variable = { tag_index = ROOT }
		set_temp_variable = { influence_target = FROM }
		change_influence_percentage = yes
	}
}
country_event = {
	id = pmc_events.3
	title = pmc_events.3.t
	desc = pmc_events.3.d
	picture = GFX_event_pmcs
	is_triggered_only = yes

	option = {
		name = pmc_events.3.a
		log = "[GetDateText]: [This.GetName]: pmc_events.3.a executed"
		set_temp_variable = { treasury_change = 2.5 }
		modify_treasury_effect = yes
		set_temp_variable = { percent_change = 1.5 }
		set_temp_variable = { tag_index = ROOT }
		set_temp_variable = { influence_target = FROM }
		change_influence_percentage = yes
	}
}
country_event = {
	id = pmc_events.4
	title = pmc_events.4.t
	desc = pmc_events.4.d
	picture = GFX_event_pmcs
	is_triggered_only = yes

	option = {
		name = pmc_events.4.a
		log = "[GetDateText]: [This.GetName]: pmc_events.4.a executed"
		set_temp_variable = { treasury_change = 3.5 }
		modify_treasury_effect = yes
		set_temp_variable = { percent_change = 1.5 }
		set_temp_variable = { tag_index = ROOT }
		set_temp_variable = { influence_target = FROM }
		change_influence_percentage = yes
	}
}
country_event = {
	id = pmc_events.5
	title = pmc_events.5.t
	desc = pmc_events.5.d
	picture = GFX_event_pmcs
	is_triggered_only = yes

	option = {
		name = pmc_events.5.a
		log = "[GetDateText]: [This.GetName]: pmc_events.5.a executed"
		set_temp_variable = { treasury_change = 4 }
		modify_treasury_effect = yes
		set_temp_variable = { percent_change = 1.5 }
		set_temp_variable = { tag_index = ROOT }
		set_temp_variable = { influence_target = FROM }
		change_influence_percentage = yes
	}
}
country_event = {
	id = pmc_events.6
	title = pmc_events.6.t
	desc = pmc_events.6.d
	picture = GFX_event_pmcs
	is_triggered_only = yes

	option = {
		name = pmc_events.6.a
		log = "[GetDateText]: [This.GetName]: pmc_events.6.a executed"
		set_temp_variable = { treasury_change = 1.5 }
		modify_treasury_effect = yes
		set_temp_variable = { percent_change = 1.5 }
		set_temp_variable = { tag_index = ROOT }
		set_temp_variable = { influence_target = FROM }
		change_influence_percentage = yes
	}
}

# Constellis Forms
news_event = {
	id = pmc_events.7
	title = pmc_events.7.t
	desc = pmc_events.7.d
	picture = GFX_trade_agreement # TODO: Need a better event picture here
	is_triggered_only = yes
	major = yes
	trigger = {
		NOT = { USA = { has_country_flag = collapsed_nation } }
		NOT = { has_global_flag = constellis_pmc_formed }
	}

	option = {
		name = pmc_events.7.a
		log = "[GetDateText]: [This.GetName]: pmc_events.7.a executed"
		trigger = { original_tag = USA }
		set_global_flag = constellis_pmc_formed
	}

	option = {
		name = pmc_events.7.a
		log = "[GetDateText]: [This.GetName]: pmc_events.7.a executed"
		trigger = { NOT = { original_tag = USA } }
	}
}
