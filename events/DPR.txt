﻿add_namespace = dpr
add_namespace = lpr
add_namespace = hpr
add_namespace = opr
#OPR wants to join Russia
country_event = {
	id = opr.1
	title = opr.1.t
	desc = opr.1.d
	is_triggered_only = yes
	picture = GFX_hpr_referendums
	option = {
		name = opr.1.a
		log = "[GetDateText]: [This.GetName]: opr.1.a executed"
		OPR = { add_ideas = CHE_republic_rf }
		set_autonomy = {
			target = OPR
			autonomy_state = autonomy_republic_rf
		}
		OPR = {
			set_country_flag = SOV_subject_agree
			add_opinion_modifier = { target = ROOT modifier = has_expressed_loyalty }
			reverse_add_opinion_modifier = { target = ROOT modifier = has_expressed_loyalty }
			set_country_flag = SUB_russ_polit
			update_party_name = yes
		}
		ai_chance = {
			base = 100
		}
	}
	option = {
		name = opr.1.b
		log = "[GetDateText]: [This.GetName]: opr.1.b executed"
		ai_chance = {
			base = 0
		}
	}
}
#OPR wants to join Transnistria
country_event = {
	id = opr.2
	title = opr.2.t
	desc = opr.2.d
	is_triggered_only = yes
	picture = GFX_hpr_referendums
	option = {
		name = opr.2.a
		log = "[GetDateText]: [This.GetName]: opr.1.a executed"
		set_autonomy = {
			target = OPR
			autonomy_state = autonomy_moldovian_federation
		}
		OPR = {
			set_country_flag = PMR_subject_agree
			add_opinion_modifier = { target = ROOT modifier = has_expressed_loyalty }
			reverse_add_opinion_modifier = { target = ROOT modifier = has_expressed_loyalty }
			update_party_name = yes
		}
		ai_chance = {
			base = 100
		}
	}
	option = {
		name = opr.1.b
		log = "[GetDateText]: [This.GetName]: opr.1.b executed"
		ai_chance = {
			base = 0
		}
	}
}
#HPR wants to join Russia
country_event = {
	id = hpr.1
	title = hpr.1.t
	desc = hpr.1.d
	is_triggered_only = yes
	picture = GFX_hpr_referendum
	option = {
		name = hpr.1.a
		log = "[GetDateText]: [This.GetName]: hpr.1.a executed"
		HPR = { add_ideas = CHE_republic_rf }
		set_autonomy = {
			target = HPR
			autonomy_state = autonomy_republic_rf
		}
		HPR = {
			set_cosmetic_tag = HPR_russia_subject
			set_country_flag = SOV_subject_agree
			add_opinion_modifier = { target = ROOT modifier = has_expressed_loyalty }
			reverse_add_opinion_modifier = { target = ROOT modifier = has_expressed_loyalty }
			set_country_flag = SUB_russ_polit
			update_party_name = yes
		}
		ai_chance = {
			base = 100
		}
	}
	option = {
		name = hpr.1.b
		log = "[GetDateText]: [This.GetName]: hpr.1.b executed"
		ai_chance = {
			base = 0
		}
	}
}
#DPR wants to join Russia
country_event = {
	id = dpr.4
	title = dpr.4.t
	desc = dpr.4.d
	is_triggered_only = yes
	picture = GFX_dpr_referendum
	option = {
		name = dpr.4.a
		log = "[GetDateText]: [This.GetName]: dpr.4.a executed"
		DPR = { add_ideas = CHE_republic_rf }
		set_autonomy = {
			target = DPR
			autonomy_state = autonomy_republic_rf
		}
		DPR = {
			set_country_flag = SOV_subject_agree
			add_opinion_modifier = { target = ROOT modifier = has_expressed_loyalty }
			reverse_add_opinion_modifier = { target = ROOT modifier = has_expressed_loyalty }
			set_country_flag = SUB_russ_polit
			update_party_name = yes
		}
		ai_chance = {
			base = 100
		}
	}
	option = {
		name = dpr.4.b
		log = "[GetDateText]: [This.GetName]: dpr.4.b executed"
		ai_chance = {
			base = 0
		}
	}
}
country_event = {
	id = dpr.5
	title = dpr.5.t
	desc = dpr.5.d
	is_triggered_only = yes
	option = {
		name = dpr.5.a
		log = "[GetDateText]: [This.GetName]: dpr.5.a executed"
		
	}
}
country_event = {
	id = dpr.6
	title = dpr.6.t
	desc = dpr.6.d
	is_triggered_only = yes
	option = {
		name = dpr.6.a
		log = "[GetDateText]: [This.GetName]: dpr.6.a executed"
		set_temp_variable = { corp_change = 35 }
		modify_corporate_tax_rate_effect = yes
		set_temp_variable = { pop_change = 35 }
		modify_population_tax_rate_effect = yes
	}
}
country_event = {
	id = dpr.7
	title = dpr.7.t
	desc = dpr.7.d
	is_triggered_only = yes
	option = {
		name = dpr.7.a
		log = "[GetDateText]: [This.GetName]: dpr.7.a executed"
	}
}
#LPR wants to join Russia
country_event = {
	id = lpr.1
	title = lpr.1.t
	desc = lpr.1.d
	is_triggered_only = yes
	picture = GFX_dpr_referendum
	option = {
		name = lpr.1.a
		log = "[GetDateText]: [This.GetName]: lpr.1.a executed"
		LPR = { add_ideas = CHE_republic_rf }
		set_autonomy = {
			target = LPR
			autonomy_state = autonomy_republic_rf
		}
		LPR = {
			set_country_flag = SOV_subject_agree
			add_opinion_modifier = { target = ROOT modifier = has_expressed_loyalty }
			reverse_add_opinion_modifier = { target = ROOT modifier = has_expressed_loyalty }
			set_country_flag = SUB_russ_polit
			update_party_name = yes
		}
		ai_chance = {
			base = 100
		}
	}
	option = {
		name = lpr.1.b
		log = "[GetDateText]: [This.GetName]: lpr.1.b executed"
		ai_chance = {
			base = 0
		}
	}
}
#Stakhanov Republic
country_event = {
	id = lpr.2
	title = lpr.2.t
	desc = lpr.2.d
	is_triggered_only = yes
	option = {
		name = lpr.2.a
		log = "[GetDateText]: [This.GetName]: lpr.2.a executed"
		hidden_effect = {
		delete_unit_template_and_units = {
			division_template = "Kazachya Brigada"
			disband = yes
		}
	create_dynamic_country = {
		original_tag = LPR
		copy_tag = LPR
		transfer_state = 1075
		set_province_controller = 6505
		set_politics = {
			ruling_party = nationalist
			elections_allowed = no
		}
		set_popularities = {
			democratic = 0
			neutrality = 0
			fascism = 0
			communism = 0
			nationalist = 100
		}
		inherit_technology = LPR
		clear_array = ruling_party
		clear_array = gov_coalition_array
		add_to_array = {
			ruling_party = 22
		}
		set_temp_variable = {
			current_cons_popularity = party_pop_array^22
		}
		subtract_from_variable = {
			party_pop_array^22 = current_cons_popularity
		}
		recalculate_party = yes
		update_government_coalition_strength = yes
		update_party_name = yes
		set_coalition_drift = yes
		set_ruling_leader = yes
		set_leader = yes
		retire_country_leader = yes
		set_cosmetic_tag = LPR_stakhanov
		clr_country_flag = dynamic_flag
		create_country_leader = {
			name = "Pavel Dremov"
			picture = "gfx/leaders/LPR/Pavel_Dremov.dds"
			expire = "2045.1.1"
			ideology = Nat_Autocracy
			traits = {
				nationalist_Nat_Autocracy
			}
		}
		delete_unit_template_and_units = {
			division_template = "National Militia"
			disband = yes
		}
		country_event = { id = lpr.3 }
		declare_war_on = {
			target = LPR
			type = puppet_wargoal_focus
		}
	}
	set_province_controller = 9461
	set_province_controller = 9446
	set_province_controller = 14536
	}
	}
}
#Stakhanov Republic
country_event = {
	id = lpr.3
	title = lpr.3.t
	desc = lpr.3.d
	is_triggered_only = yes

	option = {
		name = lpr.3.a
		log = "[GetDateText]: [This.GetName]: lpr.3.a executed"
		declare_war_on = {
			target = LPR
			type = puppet_wargoal_focus
		}
		declare_war_on = {
			target = UKR
			type = puppet_wargoal_focus
		}
		declare_war_on = {
			target = DPR
			type = puppet_wargoal_focus
		}
	}
}




