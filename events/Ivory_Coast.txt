﻿add_namespace = ivory_coast_md

# 2000 Election events
country_event = {
	id = ivory_coast_md.1
	immediate = { log = "[GetDateText]: [Root.GetName]: event ivory_coast_md.1" }
	title = ivory_coast_md.1.t
	desc = ivory_coast_md.1.d
	picture = GFX_ivory_coast_elections
	is_triggered_only = yes
	fire_only_once = yes
	trigger = {
		tag = CDI
		has_civil_war = no
		is_in_array = { ruling_party = 22 }
		date > 2000.6.1
	}

	# Allow Elections With Certain Parties
	option = {
		name = ivory_coast_md.1.o1
		log = "[GetDateText]: [This.GetName]: ivory_coast_md.1.o1 executed"

		set_temp_variable = { rul_party_temp = 5 }
		change_ruling_party_effect = yes
		set_politics = {
			ruling_party = communism
			last_election = "2001.1.1"
			election_frequency = 60
			elections_allowed = yes
		}

		create_country_leader = {
			name = "Laurent Gbagbo"
			picture = "laurent_gbagbo.dds"
			expire = "2050.1.1"
			ideology = anarchist_communism
			traits = {
				emerging_anarchist_communism
				career_politician
			}
		}
		country_event = {
			id = ivory_coast_md.3
			days = 300
			random_hours = 24
			random_days = 100
		}
		ai_chance = {
			factor = 60
		}
	}

	# Allow Free Elections
	option = {
		name = ivory_coast_md.1.o2
		log = "[GetDateText]: [This.GetName]: ivory_coast_md.1.o2 executed"
		add_stability = -0.05
		add_political_power = -50
		set_temp_variable = { rul_party_temp = 5 }
		change_ruling_party_effect = yes
		set_politics = {
			ruling_party = communism
			last_election = "2001.1.1"
			election_frequency = 60
			elections_allowed = yes
		}

		create_country_leader = {
			name = "Laurent Gbagbo"
			picture = "laurent_gbagbo.dds"
			expire = "2050.1.1"
			ideology = anarchist_communism
			traits = {
				emerging_anarchist_communism
				career_politician
			}
		}
		country_event = {
			id = ivory_coast_md.2
			days = 730
			random_hours = 24
			random_days = 364
		}
		ai_chance = {
			factor = 10
		}
	}

	# Keep the Junta
	option = {
		name = ivory_coast_md.1.o3
		log = "[GetDateText]: [This.GetName]: ivory_coast_md.1.o3 executed"
		add_stability = -0.10
		add_political_power = -100
		country_event = {
			id = ivory_coast_md.4
			days = 200
			random_hours = 24
			random_days = 50
		}

		swap_ideas = {
			remove_idea = international_bankers
			add_idea = the_military
		}

		ai_chance = {
			factor = 30
		}
	}
}

# Escalating Violence
country_event = {
	id = ivory_coast_md.2
	title = ivory_coast_md.2.t
	desc = ivory_coast_md.2.d
	picture = GFX_ivory_coast_violence
	is_triggered_only = yes

	option = {
		name = ivory_coast_md.2.o1
		add_stability = -0.05
		log = "[GetDateText]: [This.GetName]: ivory_coast_md.2.o1 executed"
		random_list = {
			60 = {
				country_event = {
					id = ivory_coast_md.5
					days = 300
					random_hours = 24
					random_days = 100
				}
			}
			40 = {
				country_event = {
					id = ivory_coast_md.9
					days = 300
					random_hours = 24
					random_days = 100
				}
			}
		}
	}
}

# Northern Protests
country_event = {
	id = ivory_coast_md.3
	title = ivory_coast_md.3.t
	desc = ivory_coast_md.3.d
	picture = GFX_ivory_coast_protests
	is_triggered_only = yes

	option = { #Suppress
		name = ivory_coast_md.3.o1
		log = "[GetDateText]: [This.GetName]: ivory_coast_md.3.o1 executed"
		add_stability = -0.10
		random_list = {
			70 = {
				modifier = {
					add = -20
					has_country_flag = CDI_1st_protests
				}
				country_event = {
					id = ivory_coast_md.6
					days = 50
					random_hours = 24
					random_days = 50
				}
			}
			30 = {
				modifier = {
					add = 20
					has_country_flag = CDI_1st_protests
				}
				country_event = {
					id = ivory_coast_md.8
					days = 2
					random_hours = 24
					random_days = 2
				}

				country_event = {
					id = ivory_coast_md.3
					days = 50
					random_hours = 24
					random_days = 50
				}
			}
		}
		news_event = {
			id = ivory_coast_md.100
			days = 2
			random_hours = 24
		}
		set_country_flag = CDI_1st_protests

		ai_chance = {
			factor = 70
		}
	}
	option = { #Concessions
		name = ivory_coast_md.3.o2
		log = "[GetDateText]: [This.GetName]: ivory_coast_md.3.o2 executed"
		add_political_power = -50
		random_list = {
			70 = {
				country_event = {
					id = ivory_coast_md.5
					days = 300
					random_hours = 24
					random_days = 100
				}
			}
			30 = {
				country_event = {
					id = ivory_coast_md.9
					days = 300
					random_hours = 24
					random_days = 100
				}
			}

		}
		ai_chance = {
			factor = 30
		}
	}
}


# Widespread Protests
country_event = {
	id = ivory_coast_md.4
	title = ivory_coast_md.4.t
	desc = ivory_coast_md.4.d
	picture = GFX_ivory_coast_protests
	is_triggered_only = yes

	option = { #supress protests
		name = ivory_coast_md.4.o1
		log = "[GetDateText]: [This.GetName]: ivory_coast_md.4.o1 executed"
		add_stability = -0.05
		country_event = {
			id = ivory_coast_md.7
			days = 7
			random_hours = 24
		}
		news_event = {
			id = ivory_coast_md.100
			days = 2
			random_hours = 24
		}
		ai_chance = {
			factor = 70
		}
	}

	# Concede power to FPI
	option = {
		name = ivory_coast_md.4.o2
		log = "[GetDateText]: [This.GetName]: ivory_coast_md.4.o2 executed"
		set_temp_variable = { rul_party_temp = 5 }
		change_ruling_party_effect = yes
		set_politics = {
			ruling_party = communism
			last_election = "2001.1.1"
			election_frequency = 60
			elections_allowed = yes
		}
		create_country_leader = {
			name = "Laurent Gbagbo"
			picture = "laurent_gbagbo.dds"
			expire = "2050.1.1"
			ideology = anarchist_communism
			traits = {
				emerging_anarchist_communism
				career_politician
			}
		}
		country_event = {
			id = ivory_coast_md.3
			days = 50
			random_hours = 24
			random_days = 50
		}

		ai_chance = {
			factor = 30
		}
	}
}

# North Secession (Peaceful)
country_event = {
	id = ivory_coast_md.5
	title = ivory_coast_md.5.t
	desc = ivory_coast_md.5.d
	picture = GFX_ivory_coast_rebels
	is_triggered_only = yes

	immediate = {
		log = "[GetDateText]: [Root.GetName]: event ivory_coast_md.5"
		set_popularities = {
			democratic = 10.9
			communism = 78.8
			fascism = 0.0
			neutrality = 0.0
			nationalist = 10.3
		}
		set_cosmetic_tag = CDI_COJEP

		division_template = {
			name = "Militia"
			regiments = {
				Militia_Bat = { x = 0 y = 0 }
				Militia_Bat = { x = 0 y = 1 }
				Militia_Bat = { x = 0 y = 2 }
				Militia_Bat = { x = 0 y = 3 }
			}
		}

		354 = {
			create_unit = {
				division = "name = \"1. Popular Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
				owner = CDI
			}
			create_unit = {
				division = "name = \"2. Popular Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
				owner = CDI
			}
			create_unit = {
				division = "name = \"3. Popular Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
				owner = CDI
			}
		}
		352 = {
			create_unit = {
				division = "name = \"4. Popular Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
				owner = CDI
			}
			create_unit = {
				division = "name = \"5. Popular Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
				owner = CDI
			}
			create_unit = {
				division = "name = \"6. Popular Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
				owner = CDI
			}
		}

		FNC = {
			set_state_owner = 353
			set_state_controller = 353
			add_stability = 0.2
			division_template = {
				name = "Militia"
				regiments = {
					Militia_Bat = { x = 0 y = 0 }
					Militia_Bat = { x = 0 y = 1 }
					Militia_Bat = { x = 0 y = 2 }
					Militia_Bat = { x = 0 y = 3 }
				}
			}
		}
		353 = {
			add_core_of = FNC
			create_unit = {
				division = "name = \"Bouna Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
				owner = FNC
			}
			create_unit = {
				division = "name = \"Katiola Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
				owner = FNC
			}
			create_unit = {
				division = "name = \"Bouaké Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
				owner = FNC
			}
			create_unit = {
				division = "name = \"Mankono Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
				owner = FNC
			}
			create_unit = {
				division = "name = \"Séguéla Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
				owner = FNC
			}
			create_unit = {
				division = "name = \"Man Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
				owner = FNC
			}
			create_unit = {
				division = "name = \"Touba Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
				owner = FNC
			}
		}
		352 = { add_core_of = FNC }
		354 = { add_core_of = FNC }
	}

	option = { # Attack Rebel
		name = ivory_coast_md.5.o1
		log = "[GetDateText]: [This.GetName]: ivory_coast_md.5.o1 executed"
		add_stability = 0.2
		declare_war_on = {
			target = FNC
			type = civil_war
		}
		hidden_effect = {
			news_event = ivory_coast_md.101
		}
		ai_chance = {
			factor = 70
		}
	}
	option = { # Peaceful
		name = ivory_coast_md.5.o2
		log = "[GetDateText]: [This.GetName]: ivory_coast_md.5.o2 executed"
		add_stability = 0.1
		create_wargoal = {
			target = FNC
			type = civil_war
		}
		hidden_effect = {
			news_event = ivory_coast_md.102
		}
		ai_chance = {
			factor = 30
		}
	}
	option = { # Become Rebel
		name = ivory_coast_md.5.o3
		log = "[GetDateText]: [This.GetName]: ivory_coast_md.5.o3 executed"
		hidden_effect = {
			add_stability = 0.2
			news_event = ivory_coast_md.101
		}
		FNC = {
			change_tag_from = CDI
		}
		ai_chance = {
			factor = 0
		}
	}
}

# 2-Way Civil War
country_event = {
	id = ivory_coast_md.6
	title = ivory_coast_md.6.t
	desc = ivory_coast_md.6.d
	picture = GFX_ivory_coast_rebels
	is_triggered_only = yes
	immediate = {
		log = "[GetDateText]: [This.GetName]: ivory_coast_md.6 executed"
		set_global_flag = CDI_first_ivory_coast_civil_war
		hidden_effect = {
			news_event = ivory_coast_md.101
			set_popularities = {
				democratic = 10.9
				communism = 78.8
				fascism = 0.0
				neutrality = 0.0
				nationalist = 10.3
			}
			set_cosmetic_tag = CDI_COJEP
			division_template = {
				name = "Militia"
				regiments = {
					Militia_Bat = { x = 0 y = 0 }
					Militia_Bat = { x = 0 y = 1 }
					Militia_Bat = { x = 0 y = 2 }
					Militia_Bat = { x = 0 y = 3 }
				}
			}

			354 = {
				create_unit = {
					division = "name = \"1. Popular Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = CDI
				}
				create_unit = {
					division = "name = \"2. Popular Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = CDI
				}
				create_unit = {
					division = "name = \"3. Popular Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = CDI
				}
			}
			352 = {
				create_unit = {
					division = "name = \"4. Popular Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = CDI
				}
				create_unit = {
					division = "name = \"5. Popular Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = CDI
				}
				create_unit = {
					division = "name = \"6. Popular Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = CDI
				}
			}

			FNC = {
				inherit_technology = CDI
				set_state_owner = 353
				set_state_controller = 353
				add_stability = 0.2
				division_template = {
					name = "Militia"
					regiments = {
						Militia_Bat = { x = 0 y = 0 }
						Militia_Bat = { x = 0 y = 1 }
						Militia_Bat = { x = 0 y = 2 }
						Militia_Bat = { x = 0 y = 3 }
					}
				}
			}
			353 = {
				add_core_of = FNC
				create_unit = {
					division = "name = \"Bouna Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = FNC
				}
				create_unit = {
					division = "name = \"Katiola Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = FNC
				}
				create_unit = {
					division = "name = \"Bouaké Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = FNC
				}
				create_unit = {
					division = "name = \"Mankono Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = FNC
				}
				create_unit = {
					division = "name = \"Séguéla Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = FNC
				}
				create_unit = {
					division = "name = \"Man Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = FNC
				}
				create_unit = {
					division = "name = \"Touba Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = FNC
				}
			}
			352 = { add_core_of = FNC }
			354 = { add_core_of = FNC }

			declare_war_on = {
				target = FNC
				type = civil_war
			}
			add_stability = 0.2
		}
	}


	option = {
		name = ivory_coast_md.6.o1
		log = "[GetDateText]: [This.GetName]: ivory_coast_md.6.o1 executed"
	}

	option = {
		name = ivory_coast_md.6.o2
		log = "[GetDateText]: [This.GetName]: ivory_coast_md.6.o2 executed"
		FNC = {
			change_tag_from = CDI
		}
	}
}

#3-Way Civil War
country_event = {
	id = ivory_coast_md.7
	title = ivory_coast_md.7.t
	desc = ivory_coast_md.7.d
	picture = GFX_ivory_coast_uprising
	is_triggered_only = yes
	immediate = {
		log = "[GetDateText]: [This.GetName]: ivory_coast_md.7 executed"
		set_global_flag = CDI_first_ivory_coast_civil_war
		hidden_effect = {
			set_capital = { state = 354 }
			set_cosmetic_tag = CDI_REB_S
			news_event = ivory_coast_md.101

			division_template = {
				name = "Militia"
				regiments = {
					Militia_Bat = { x = 0 y = 0 }
					Militia_Bat = { x = 0 y = 1 }
					Militia_Bat = { x = 0 y = 2 }
					Militia_Bat = { x = 0 y = 3 }
				}
			}

			start_civil_war = {
				ideology = communism
				size = 0.66
				states = { 352 }
			}

			set_variable = { party_pop_array^20 = 0.0 } #Nat_Populism
			set_variable = { party_pop_array^22 = 0.5 } #Nat_Autocracy
			update_government_coalition_strength = yes
			update_party_name = yes
			set_coalition_drift = yes


			354 = {
				create_unit = {
					division = "name = \"1. Loyalist Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = CDI
				}
				create_unit = {
					division = "name = \"2. Loyalist Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = CDI
				}
				create_unit = {
					division = "name = \"3. Loyalist Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = CDI
				}
				create_unit = {
					division = "name = \"4. Loyalist Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = CDI
				}
			}

			FNC = {
				inherit_technology = CDI
				set_state_owner = 353
				set_state_controller = 353
				division_template = {
					name = "Militia"
					regiments = {
						Militia_Bat = { x = 0 y = 0 }
						Militia_Bat = { x = 0 y = 1 }
						Militia_Bat = { x = 0 y = 2 }
						Militia_Bat = { x = 0 y = 3 }
					}
				}
			}
			353 = {
				add_core_of = FNC
				create_unit = {
					division = "name = \"Bouna Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = FNC
				}
				create_unit = {
					division = "name = \"Katiola Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = FNC
				}
				create_unit = {
					division = "name = \"Bouaké Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = FNC
				}
				create_unit = {
					division = "name = \"Mankono Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = FNC
				}
				create_unit = {
					division = "name = \"Séguéla Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = FNC
				}
				create_unit = {
					division = "name = \"Man Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = FNC
				}
				create_unit = {
					division = "name = \"Touba Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = FNC
				}
			}
			352 = { add_core_of = FNC }
			354 = { add_core_of = FNC }

			declare_war_on = {
				target = FNC
				type = civil_war
			}

			random_other_country = {
				limit = {
					original_tag = CDI
					has_government = communism
				}
				set_cosmetic_tag = CDI_COJEP
				inherit_technology = CDI

				set_popularities = {
					democratic = 18.0
					communism = 67.0
					fascism = 0.0
					neutrality = 10.0
					nationalist = 5.0
				}

				create_country_leader = {
					name = "Laurent Gbagbo"
					picture = "laurent_gbagbo.dds"
					expire = "2050.1.1"
					ideology = anarchist_communism
					traits = {
						emerging_anarchist_communism
						career_politician
					}
				}
				352 = {
					create_unit = {
						division = "name = \"1. Popular Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
						owner = PREV
					}
					create_unit = {
						division = "name = \"2. Popular Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
						owner = PREV
					}
					create_unit = {
						division = "name = \"3. Popular Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
						owner = PREV
					}
					create_unit = {
						division = "name = \"4. Popular Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
						owner = PREV
					}
					create_unit = {
						division = "name = \"5. Popular Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
						owner = PREV
					}
				}

				declare_war_on = {
					target = FNC
					type = civil_war
				}
			}
		}

		# News Event to Inform the World
		news_event = ivory_coast_md.103
	}

	option = {
		name = ivory_coast_md.7.o1
		log = "[GetDateText]: [This.GetName]: ivory_coast_md.7.o1 executed"
	}

	option = {
		name = ivory_coast_md.7.o2
		log = "[GetDateText]: [This.GetName]: ivory_coast_md.7.o2 executed"
		FNC = {
			change_tag_from = CDI
		}
	}
}

# Protests Suppressed
country_event = {
	id = ivory_coast_md.8

	title = ivory_coast_md.8.t
	desc = ivory_coast_md.8.d
	picture = GFX_ivory_coast_protest_supressed
	is_triggered_only = yes
	option = {
		name = ivory_coast_md.8.o1
		log = "[GetDateText]: [This.GetName]: ivory_coast_md.8.o1 executed"
		add_stability = 0.1
	}
}

# Unrest Dies Down (peaceful ending)
country_event = {
	id = ivory_coast_md.9
	title = ivory_coast_md.9.t
	desc = ivory_coast_md.9.d
	picture = GFX_ivory_coast_protest_supressed
	is_triggered_only = yes
	option = {
		name = ivory_coast_md.9.o1
		log = "[GetDateText]: [This.GetName]: ivory_coast_md.9.o1 executed"
		add_stability = 0.1
	}
}

# End of the First Ivory Coast Civil War
country_event = {
	id = ivory_coast_md.10
	title = ivory_coast_md.10.t
	desc = ivory_coast_md.10.d
	picture = GFX_ivory_coast_protest_supressed
	is_triggered_only = yes
	trigger = {
		OR = {
			original_tag = CDI
			original_tag = FNC
		}
	}

	option = {
		name = ivory_coast_md.10.o1
		log = "[GetDateText]: [This.GetName]: ivory_coast_md.10.o1 executed"
		add_stability = 0.05
		set_party_index_to_ruling_party = yes
		set_temp_variable = { party_popularity_increase = 0.10 }
		set_temp_variable = { temp_outlook_increase = 0.10 }
		add_relative_party_popularity = yes
		news_event = ivory_coast_md.104
		delete_unit_template_and_units = {
			division_template = "Militia"
		}

		# Reassumes the Mantle of Ivory Coast
		set_cosmetic_tag = CDI
		hidden_effect = {
			if = { limit = { tag = FNC }
				for_each_scope_loop = {
					array = FNC.core_states
					remove_core_of = CDI
				}
			}
			if = { limit = { original_tag = CDI }
				for_each_scope_loop = {
					array = CDI.core_states
					remove_core_of = FNC
				}
			}
			set_country_flag = CDI_victor_of_the_ivory_coast_war
			if = {
				limit = {
					FRA = {
						has_relation_modifier = {
							target = CDI
							modifier = generic_increased_military_support
						}
					}
				}
				FRA = {
					remove_relation_modifier = {
						target = CDI
						modifier = generic_increased_military_support
					}
				}
			}
			if = {
				limit = {
					FRA = {
						has_relation_modifier = {
							target = FNC
							modifier = generic_increased_military_support
						}
					}
				}
				FRA = {
					remove_relation_modifier = {
						target = FNC
						modifier = generic_increased_military_support
					}
				}
			}
			if = {
				limit = {
					any_country_with_original_tag = {
						original_tag_to_check = CDI
						FRA = {
							has_relation_modifier = {
								target = PREV
								modifier = generic_increased_military_support
							}
						}
					}
				}
				random_country = {
					limit = {
						original_tag = CDI
						FRA = {
							has_relation_modifier = {
								target = THIS
								modifier = generic_increased_military_support
							}
						}
					}
					FRA = {
						remove_relation_modifier = {
							target = PREV
							modifier = generic_increased_military_support
						}
					}
				}
			}
		}

		set_global_flag = CDI_first_ivory_coast_civil_war_cleanedup
	}
}

# The 2010 Presidential Elections
country_event = {
	id = ivory_coast_md.11
	title = ivory_coast_md.11.t
	desc = ivory_coast_md.11.d
	picture = GFX_ivory_coast_elections
	is_triggered_only = yes
	trigger = {
		original_tag = CDI
		has_country_leader = { name = "Alassane Ouattara" ruling_only = yes }
	}

	# Alassane Ouattra Wins
	option = {
		name = ivory_coast_md.11.o1
		log = "[GetDateText]: [This.GetName]: ivory_coast_md.11.o1 executed"

		set_temp_variable = { rul_party_temp = 1 }
		change_ruling_party_effect = yes
		set_politics = {
			ruling_party = democratic
			last_election = "2010.11.1"
			election_frequency = 60
			elections_allowed = yes
		}
		create_country_leader = {
			name = "Alassane Ouattara"
			picture = "Alassane_Ouattara.dds"
			expire = "2050.1.1"
			ideology = liberalism
			traits = {
				western_liberalism
				career_politician
				economist
			}
		}
	}

	# Nullify the Results
	option = {
		name = ivory_coast_md.11.o2
		log = "[GetDateText]: [This.GetName]: ivory_coast_md.11.o2 executed"
		set_country_flag = CDI_nullified_the_results

		hidden_effect = { country_event = { id = ivory_coast_md.12 days = 7 random_days = 4 } }
	}
}

# Alassane Ouattara States Foulplay
country_event = {
	id = ivory_coast_md.12
	title = ivory_coast_md.12.t
	desc = ivory_coast_md.12.d
	picture = GFX_ivory_coast_elections
	is_triggered_only = yes

	# Step Down From Office
	option = {
		name = ivory_coast_md.12.a
		log = "[GetDateText]: [This.GetName]: ivory_coast_md.12.a executed"
		set_temp_variable = { rul_party_temp = 1 }
		change_ruling_party_effect = yes
		set_politics = {
			ruling_party = democratic
			last_election = "2010.11.1"
			election_frequency = 60
			elections_allowed = yes
		}
		create_country_leader = {
			name = "Alassane Ouattara"
			picture = "Alassane_Ouattara.dds"
			expire = "2050.1.1"
			ideology = liberalism
			traits = {
				western_liberalism
				career_politician
				economist
			}
		}
	}

	# Take the Oath of Office
	option = {
		name = ivory_coast_md.12.b
		log = "[GetDateText]: [This.GetName]: ivory_coast_md.12.b executed"

		hidden_effect = { country_event = { id = ivory_coast_md.13 days = 7 random_days = 4 } }
	}
}

# Pro-Ouattara Protests
country_event = {
	id = ivory_coast_md.13
	title = ivory_coast_md.13.t
	desc = ivory_coast_md.13.d
	picture = GFX_ivory_coast_protests
	is_triggered_only = yes

	# Shut the Protests Down
	option = {
		name = ivory_coast_md.13.a
		log = "[GetDateText]: [This.GetName]: ivory_coast_md.13.a executed"

		add_political_power = -75
		set_party_index_to_ruling_party = yes
		set_temp_variable = { party_popularity_increase = 0.02 }
		set_temp_variable = { temp_outlook_increase = 0.02 }
		add_stability = -0.03

		hidden_effect = {
			country_event = { id = ivory_coast_md.14 days = 14 random_days = 30 }
		}
	}

	# Let them Bleat
	option = {
		name = ivory_coast_md.13.b
		log = "[GetDateText]: [This.GetName]: ivory_coast_md.13.b executed"

		set_party_index_to_ruling_party = yes
		set_temp_variable = { party_popularity_increase = -0.02 }
		set_temp_variable = { temp_outlook_increase = -0.02 }
		add_stability = -0.05
		hidden_effect = {
			country_event = { id = ivory_coast_md.14 days = 14 random_days = 30 }
		}
	}
}

# The Second Ivorian Civil War Begins
country_event = {
	id = ivory_coast_md.14
	title = ivory_coast_md.14.t
	desc = ivory_coast_md.14.d
	picture = GFX_ivory_coast_rebels
	is_triggered_only = yes
	immediate = {
		# START WAR WHERE YOU ARE GBAGBO
		hidden_effect = {
			set_cosmetic_tag = CDI_COJEP
			division_template = {
				name = "Militia"
				regiments = {
					Militia_Bat = { x = 0 y = 0 }
					Militia_Bat = { x = 0 y = 1 }
					Militia_Bat = { x = 0 y = 2 }
					Militia_Bat = { x = 0 y = 3 }
				}
			}

			354 = {
				create_unit = {
					division = "name = \"1. Popular Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = CDI
				}
				create_unit = {
					division = "name = \"2. Popular Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = CDI
				}
				create_unit = {
					division = "name = \"3. Popular Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = CDI
				}
			}
			352 = {
				create_unit = {
					division = "name = \"4. Popular Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = CDI
				}
				create_unit = {
					division = "name = \"5. Popular Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = CDI
				}
			}

			FNC = {
				inherit_technology = CDI
				set_state_owner = 353
				set_state_controller = 353
				add_stability = 0.2
				set_temp_variable = { rul_party_temp = 1 }
				change_ruling_party_effect = yes
				set_politics = {
					ruling_party = democratic
					last_election = "2010.11.1"
					election_frequency = 60
					elections_allowed = yes
				}
				create_country_leader = {
					name = "Alassane Ouattara"
					picture = "Alassane_Ouattara.dds"
					expire = "2050.1.1"
					ideology = liberalism
					traits = {
						western_liberalism
						career_politician
						economist
					}
				}
				division_template = {
					name = "Militia"
					regiments = {
						Militia_Bat = { x = 0 y = 0 }
						Militia_Bat = { x = 0 y = 1 }
						Militia_Bat = { x = 0 y = 2 }
						Militia_Bat = { x = 0 y = 3 }
					}
				}
			}
			353 = {
				add_core_of = FNC
				create_unit = {
					division = "name = \"Bouna Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = FNC
				}
				create_unit = {
					division = "name = \"Katiola Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = FNC
				}
				create_unit = {
					division = "name = \"Bouaké Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = FNC
				}
				create_unit = {
					division = "name = \"Mankono Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = FNC
				}
				create_unit = {
					division = "name = \"Séguéla Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = FNC
				}
				create_unit = {
					division = "name = \"Man Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = FNC
				}
				create_unit = {
					division = "name = \"1. Touba Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = FNC
				}
				create_unit = {
					division = "name = \"2. Touba Militia\" division_template = \"Militia\" start_experience_factor = 0.5 start_equipment_factor = 1"
					owner = FNC
				}
			}
			352 = { add_core_of = FNC }
			354 = { add_core_of = FNC }

			declare_war_on = {
				target = FNC
				type = civil_war
			}
			add_stability = 0.2
		}
	}

	# Gbagbo
	option = {
		name = ivory_coast_md.14.a
		log = "[GetDateText]: [This.GetName]: ivory_coast_md.14.a executed"
		set_global_flag = CDI_second_ivory_coast_civil_war
	}

	# Ouattara
	option = {
		name = ivory_coast_md.14.b
		log = "[GetDateText]: [This.GetName]: ivory_coast_md.14.b executed"
		set_global_flag = CDI_second_ivory_coast_civil_war
		FNC = {
			change_tag_from = CDI
		}
	}
}

# The Victor of the Second Ivorian Civil War
country_event = {
	id = ivory_coast_md.15
	title = ivory_coast_md.15.t
	desc = ivory_coast_md.15.d
	picture = GFX_ivory_coast_protest_supressed
	is_triggered_only = yes
	trigger = {
		OR = {
			original_tag = CDI
			original_tag = FNC
		}
	}

	# To the Victor the Spoils
	option = {
		name = ivory_coast_md.15.a
		log = "[GetDateText]: [This.GetName]: ivory_coast_md.15.a executed"
		set_global_flag = CDI_second_ivory_coast_civil_war_cleanedup
		add_stability = 0.05
		set_party_index_to_ruling_party = yes
		set_temp_variable = { party_popularity_increase = 0.10 }
		set_temp_variable = { temp_outlook_increase = 0.10 }
		add_relative_party_popularity = yes
		news_event = ivory_coast_md.105
		delete_unit_template_and_units = {
			division_template = "Militia"
		}

		# Reassumes the Mantle of Ivory Coast
		set_cosmetic_tag = CDI
		hidden_effect = {
			if = { limit = { tag = FNC }
				for_each_scope_loop = {
					array = FNC.core_states
					remove_core_of = CDI
				}
			}
			if = { limit = { original_tag = CDI }
				for_each_scope_loop = {
					array = CDI.core_states
					remove_core_of = FNC
				}
			}
			set_country_flag = CDI_victor_of_the_second_ivory_coast_war
			if = {
				limit = {
					FRA = {
						has_relation_modifier = {
							target = CDI
							modifier = generic_increased_military_support
						}
					}
				}
				FRA = {
					remove_relation_modifier = {
						target = CDI
						modifier = generic_increased_military_support
					}
				}
			}
			if = {
				limit = {
					FRA = {
						has_relation_modifier = {
							target = FNC
							modifier = generic_increased_military_support
						}
					}
				}
				FRA = {
					remove_relation_modifier = {
						target = FNC
						modifier = generic_increased_military_support
					}
				}
			}
		}

		set_global_flag = CDI_second_ivory_coast_civil_war_cleanedup
	}
}

### News ###
# Protests
news_event = {
	id = ivory_coast_md.100
	is_triggered_only = yes
	major = yes

	title = ivory_coast_md.100.t
	desc = ivory_coast_md.100.d
	picture = GFX_ivory_coast_police

	option = {
		trigger = { tag = CDI }
		name = ivory_coast_md.100.o1
		log = "[GetDateText]: [This.GetName]: ivory_coast_md.100.o1 executed"
	}
	option = {
		trigger = { NOT = { tag = CDI } }
		name = ivory_coast_md.100.o2
		log = "[GetDateText]: [This.GetName]: ivory_coast_md.100.o2 executed"
	}
}

# First Ivorian Civil War
news_event = {
	id = ivory_coast_md.101
	is_triggered_only = yes
	major = yes

	title = ivory_coast_md.101.t
	desc = ivory_coast_md.101.d
	picture = GFX_ivory_coast_civil_war

	option = {
		trigger = { tag = CDI }
		name = ivory_coast_md.101.o1
		log = "[GetDateText]: news event ivory_coast_md.101.o1"
	}
	option = {
		trigger = { tag = FNC }
		name = ivory_coast_md.101.o2
		log = "[GetDateText]: news event ivory_coast_md.101.o2"
	}
	option = {
		trigger = {
			OR = {
				tag = LIB
				tag = BFA
			}
		}
		name = ivory_coast_md.101.o3
		log = "[GetDateText]: news event ivory_coast_md.101.o3"
		add_political_power = -100
		hidden_effect = { set_country_flag = intervened_FNCI }
		ai_chance = { factor = 100 }
	}
	option = {
		trigger = {
			NOT = {
				tag = CDI
				tag = FNC
			}
		}
		name = ivory_coast_md.101.o4
		log = "[GetDateText]: news event ivory_coast_md.101.o4"
	}
}

#Peaceful uprising
news_event = {
	id = ivory_coast_md.102
	is_triggered_only = yes
	major = yes

	title = ivory_coast_md.102.t
	desc = ivory_coast_md.102.d
	picture = GFX_ivory_coast_civil_war

	option = {
		trigger = { tag = CDI }
		name = ivory_coast_md.102.o1
		log = "[GetDateText]: news event ivory_coast_md.102.o1"
	}
	option = {
		trigger = { tag = FNC }
		name = ivory_coast_md.102.o2
		log = "[GetDateText]: news event ivory_coast_md.102.o2"
	}
	option = {
		trigger = {
			OR = {
				tag = LIB
				tag = BFA
			}
		}
		name = ivory_coast_md.102.o3
		log = "[GetDateText]: news event ivory_coast_md.102.o3"
		add_political_power = -100
		hidden_effect = { set_country_flag = intervened_FNCI }
		ai_chance = { factor = 100 }
	}
	option = {
		trigger = {
			NOT = {
				tag = CDI
				tag = FNC
			}
		}
		name = ivory_coast_md.102.o4
		log = "[GetDateText]: news event ivory_coast_md.102.o4"
	}
}

#3 Way Civil War
news_event = {
	id = ivory_coast_md.103
	is_triggered_only = yes
	major = yes

	title = ivory_coast_md.103.t
	desc = ivory_coast_md.103.d
	picture = GFX_ivory_coast_civil_war

	option = {
		trigger = { tag = CDI }
		name = ivory_coast_md.103.o1
		log = "[GetDateText]: news event ivory_coast_md.103.o1"
	}
	option = {
		trigger = {
			OR = {
				tag = FNC
				AND = {
					original_tag = CDI
					NOT = { tag = CDI }
				}
			}
		}
		name = ivory_coast_md.103.o2
		log = "[GetDateText]: news event ivory_coast_md.103.o2"
	}
	option = {
		trigger = { tag = LIB }
		name = ivory_coast_md.103.o3
		log = "[GetDateText]: news event ivory_coast_md.103.o3"
		add_political_power = -100
		hidden_effect = { set_country_flag = intervened_Junta }
		ai_chance = { factor = 100 }

	}
	option = {
		trigger = {
			OR = {
				tag = LIB
				tag = BFA
			}
		}
		name = ivory_coast_md.103.o4
		log = "[GetDateText]: news event ivory_coast_md.103.o4"
		add_political_power = -100
		hidden_effect = { set_country_flag = intervened_FNCI }
		ai_chance = { factor = 100 }
	}
	option = {
		trigger = {
			NOT = {
				original_tag = CDI
				tag = FNC
			}
		}
		name = ivory_coast_md.101.o5
		log = "[GetDateText]: news event ivory_coast_md.103.o5"
	}
}

news_event = {
	id = ivory_coast_md.104

	is_triggered_only = yes
	major = yes

	title = ivory_coast_md.104.t
	desc = ivory_coast_md.104.d
	picture = GFX_ivory_coast_civil_war

	option = {
		trigger = { original_tag = CDI }
		name = ivory_coast_md.104.o1
		log = "[GetDateText]: news event ivory_coast_md.104.o1"
	}

	option = {
		trigger = { original_tag = FNC }
		name = ivory_coast_md.104.o2
		log = "[GetDateText]: news event ivory_coast_md.104.o2"
	}

	option = {
		trigger = { NOT = { original_tag = FNC original_tag = CDI } }
		name = ivory_coast_md.104.o3
		log = "[GetDateText]: news event ivory_coast_md.104.o3"
	}
}

# Second Ivorian Civil War
news_event = {
	id = ivory_coast_md.105

	is_triggered_only = yes
	major = yes

	title = ivory_coast_md.105.t
	desc = ivory_coast_md.105.d
	picture = GFX_ivory_coast_civil_war

	option = {
		trigger = { original_tag = CDI }
		name = ivory_coast_md.105.a
		log = "[GetDateText]: news event ivory_coast_md.105.a"
	}

	option = {
		trigger = { original_tag = FNC }
		name = ivory_coast_md.105.b
		log = "[GetDateText]: news event ivory_coast_md.105.b"
	}

	option = {
		trigger = { NOT = { original_tag = FNC original_tag = CDI } }
		name = ivory_coast_md.105.c
		log = "[GetDateText]: news event ivory_coast_md.105.c"
	}
}
