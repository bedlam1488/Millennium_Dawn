# Welcome to Millennium Dawn's Dev Build

Millennium Dawn is a multi-mod project set in 2000 and continues to the modern day and beyond. The mod boasts new and unique tech trees, focus trees, events, and decisions to immerse you in the intricacies of the modern era.

Further, it offers a new and unique economic system, a new political system, national taxation and debt, custom internal political factions, international influence mechanics, custom 3D models, a custom soundtrack, and much more!

## Important Links

[Link to MD's Discord](http://discord.gg/millenniumdawn)

[Bugreporting Form](https://forms.gle/2zAWhggnYbTcmG1b6)

[MD's Git Instructions](https://docs.google.com/document/d/1V8DLowqEOSmlgazlHeC-hLZzLki5e6cWhQO_ZK6HVYs)

## Access Rules

Developer/Junior Developer Access:

- If you are a new recruit you will be given access for two months from the date you are added. i.e. if you join on Jan 1st. You get access until Feb 1st.
- You are granted access based on your role. Playtesters get the reporter rule meaning they cannot do any pushing to branches. However, they may pull and update their local builds
- Contributions of any kind show activity. Please be as active as you reasonably can.
- Inactive users will be removed based on whether or not they are present on Discord or at the discretion of the Lead Developers.

Playtester Access:

- The first time you are granted access require your first playtest within 48 hours, after the first you get monthly access and are expected to do at least one playtest a month.
- Playtesting for MD requires some pretty extensive notes and opinions regarding the content you're playtesting

## Maintainers & Approvers

MD has a number of maintainers who can push and pull from master. Most lead devs and a handful of select developers can also approve merge requests. If you need a merge request approved, please start by requesting a council member to look at the merge request in the discord.

## Fork Users

Thanks for wanting to help with MD! Please make sure to add yourself to [Authors](./AUTHORS.txt) and that your change is in the [Changelog](./Changelog.txt) so we can give you credit and that your update is added to the changelog.

Thanks for reading!

Sincerely, Bird
