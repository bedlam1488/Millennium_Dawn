# Style Guide

It helps to read code if it is written in a consistent style. Contributors to Millennium Dawn should agree to and adhere to our stylization and keep files consistent.
Stylization and other clean up contributions are more than welcome if they fix inconsistencies. The Millennium Dawn team does run CWTools and pipeline formatters that keep stylization consistent for the team.

## Localization files (.yml)

- indent 1 space
- Remove all 0/1 after the : in string pairs

## Code/Script Files

- indent: 1 tab (4 spaces)
- Comments go above or below the code

## Changelog

- All changes should be documented in the changelog where applicable

## Resources

- Resources or useful PDFs can be stored in Modding Resources for all Millennium Dawn team members
