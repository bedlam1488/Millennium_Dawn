﻿###########################
# 	  Taiwan Events	  #
###########################

add_namespace = taiwan
add_namespace = taiwan_news

#228 Hand-in-Hand rally
country_event = {
	id = taiwan.1

	title = taiwan.1.t
	desc = taiwan.1.d
	picture = GFX_TAI_228_hand

	fire_only_once = yes

	trigger = {
		tag = TAI
		date > 2004.2.28
	}

	option = {
		name = taiwan.1.a
		log = "[GetDateText]: [This.GetName]: taiwan.1.a executed"
		add_popularity = { ideology = democratic popularity = 0.03 }
	}
}

#Typhoon Bilis
country_event = {
	id = taiwan.2

	title = taiwan.2.t
	desc = taiwan.2.d
	picture = GFX_typhoon

	fire_only_once = yes

	trigger = {
		tag = TAI
		date > 2000.8.20
	}

	option = {
		name = taiwan.2.a
		log = "[GetDateText]: [This.GetName]: taiwan.2.a executed"
		set_temp_variable = { treasury_change = -0.13 }
		modify_treasury_effect = yes
		add_manpower = -14
		598 = { damage_building = { type = infrastructure damage = 0.2 } damage_building = { type = industrial_complex damage = 0.3 } }
		600 = { damage_building = { type = infrastructure damage = 0.2 } damage_building = { type = industrial_complex damage = 0.3 } }
		hidden_effect = { CHI = { country_event = { id = china.5 days = 2 } } }
	}
}

#2000 presidential election
country_event = {

	id = taiwan.3
	title = taiwan.3.t
	desc = taiwan.3.d
	picture = GFX_TAI_taiwan

	fire_only_once = yes

	is_triggered_only = yes

	trigger = {
		tag = TAI
		date > 2000.1.1
		date < 2000.1.18
		has_country_leader = {
				name = "Lee Teng-hui"
				ruling_only = yes
			}
	}

	option = {
		name = taiwan.3.a
		ai_chance = {
			base = 90
			modifier = {
				is_historical_focus_on = yes
				factor = 100
			}
		}
		log = "[GetDateText]: [This.GetName]: taiwan.3.a executed"
		set_politics = {
			ruling_party = democratic
			elections_allowed = yes
		}
		add_to_variable = { liberalism_leader = 1 }
		hidden_effect = { kill_country_leader = yes }
		create_country_leader = {
				name = "Chen Shui-bian"
				picture = "Chen_Shui-bian.dds"
				ideology = liberalism
				expire = "2008.5.20"
				traits = {
					western_liberalism
					rash
					pro_american
					pro_independence_taiwan
				}
			}
	}
	option = {
		name = taiwan.3.b
		log = "[GetDateText]: [This.GetName]: taiwan.3.b executed"
		create_country_leader = {
			name = "Lien Chan"
			desc = ""
			picture = "Lien_Chan.dds"
			expire = "2055.1.1"
			ideology = Neutral_conservatism
		}
	}
	option = {
		name = taiwan.3.c
		log = "[GetDateText]: [This.GetName]: taiwan.3.c executed"
		create_country_leader = {
			name = "James Soong"
			desc = ""
			picture = "James_Soong.dds"
			expire = "2055.1.1"
			ideology = Neutral_conservatism
		}
	}
}

#Coast Guard created
country_event = {
	id = taiwan.4

	title = taiwan.4.t
	desc = taiwan.4.d
	picture = GFX_TAI_coast_guard

	fire_only_once = yes

	is_triggered_only = yes

	option = {
		name = taiwan.4.a
		log = "[GetDateText]: [This.GetName]: taiwan.4.a executed"
		navy_experience = 5
	}
}

#National University of Kaohsiung
country_event = {
	id = taiwan.5

	title = taiwan.5.t
	desc = taiwan.5.d
	picture = GFX_TAI_kaoshiung_university

	fire_only_once = yes

	is_triggered_only = yes

	option = {
		name = taiwan.5.a
		log = "[GetDateText]: [This.GetName]: taiwan.5.a executed"
		### TODO research bonus for industry
	}
}

#CSSTA passed
country_event = {
	id = taiwan.6

	title = taiwan.6.t
	desc = taiwan.6.d
	picture = GFX_political_deal

	fire_only_once = yes

	is_triggered_only = yes

	option = {
		name = taiwan.6.a
		add_opinion_modifier = {
			target = CHI
			modifier = CSSTA
		}
		add_opinion_modifier = {
			target = CHI
			modifier = large_commercial_relations
		}
		custom_effect_tooltip = TAI_influence_CHI_tt
		set_temp_variable = { percent_change = 2 }
		set_temp_variable = { tag_index = CHI.id }
		set_temp_variable = { influence_target = THIS.id }
		change_influence_percentage = yes
		CHI = {
			add_opinion_modifier = {
				target = TAI
				modifier = CSSTA
			}
			add_opinion_modifier = {
				target = TAI
				modifier = large_commercial_relations
			}
		}
		log = "[GetDateText]: [This.GetName]: taiwan.6.a executed"
	}
}

#The Sunflower Movement
country_event = {
	id = taiwan.7

	title = taiwan.7.t
	desc = taiwan.7.d
	picture = GFX_TAI_sunflower

	fire_only_once = yes

	is_triggered_only = yes

	option = {
		name = taiwan.7.a
		add_political_power = -100
		# add_timed_idea = {
		# 	idea = legislative_yuan_occupied
		# 	days = 30
		# }
		set_temp_variable = { party_index = 1 }
		set_temp_variable = { party_popularity_increase = -0.05 }
		add_relative_party_popularity = yes
		set_temp_variable = { party_index = 2 }
		set_temp_variable = { party_popularity_increase = 0.05 }
		add_relative_party_popularity = yes
		log = "[GetDateText]: [This.GetName]: taiwan.7.a executed"
	}
}

#The PRC's "Roadmap to Reunification" Released
country_event = {
	id = taiwan.8
	title = taiwan.8.t
	desc = taiwan.8.d

	picture = GFX_politics_negotiations

	is_triggered_only = yes

	option = {
		name = taiwan.8.a #We should accept the plan.
		if = {
			limit = { has_country_flag = rejected_roadmap }
			clr_country_flag = rejected_roadmap
		}
		set_country_flag = accepted_roadmap
		CHI = { country_event = { days = 2 id = taiwan.11 } }
		log = "[GetDateText]: [This.GetName]: taiwan.8.a executed"
		ai_chance = {
			factor = 10
			modifier = {
				factor = 2
				TAI = {
					OR = {
						AND = {
							check_variable = { influence_array^0 = CHI }
							check_variable = { influence_array_val^0 > 29.999 }
						}
						AND = {
							check_variable = { influence_array^1 = CHI }
							check_variable = { influence_array_val^1 > 29.999 }
						}
						AND = {
							check_variable = { influence_array^2 = CHI }
							check_variable = { influence_array_val^2 > 29.999 }
						}
					}
				}
			}
			modifier = {
				factor = 2
				TAI = {
					OR = {
						AND = {
							check_variable = { influence_array^0 = CHI }
							check_variable = { influence_array_val^0 > 39.999 }
						}
						AND = {
							check_variable = { influence_array^1 = CHI }
							check_variable = { influence_array_val^1 > 39.999 }
						}
					}
				}
			}
			modifier = {
				factor = 2
				OR = {
					is_in_array = { ruling_party = 1 }
					is_in_array = { ruling_party = 6 }
					is_in_array = { ruling_party = 14 }
				}
			}
			modifier = {
				factor = 10
				if = {
					limit = { has_government = communism }
					CHI = { has_government = communism }
				}
				else_if = {
					limit = { has_government = democratic }
					CHI = { has_government = democratic }
				}
				else_if = {
					limit = { has_government = neutrality }
					CHI = { has_government = neutrality }
				}
				else_if = {
					limit = { has_government = nationalist }
					CHI = { has_government = nationalist }
				}
				else_if = {
					limit = { has_government = fascism }
					CHI = { has_government = fascism }
				}
			}
			modifier = {
				factor = 2
				TAI = {
					has_opinion = {
						target = CHI
						value > 0
					}
				}
			}
			modifier = {
				factor = 2
				TAI = {
					has_opinion = {
						target = CHI
						value > 24
					}
				}
			}
			modifier = {
				factor = 2
				TAI = {
					has_opinion = {
						target = CHI
						value > 49
					}
				}
			}
			modifier = {
				factor = 2
				HKG = {
					has_country_flag = continuation
				}
			}
		}
	}
	option = {
		name = taiwan.8.b #Taiwan is not China!
		if = {
			limit = { NOT = { has_country_flag = rejected_roadmap } }
			set_country_flag = rejected_roadmap
		}
		CHI = { country_event = { days = 2 id = taiwan.12 } }
		log = "[GetDateText]: [This.GetName]: taiwan.8.b executed"
		ai_chance = {
			factor = 80
			modifier = {
				factor = 2
				HKG = {
					has_country_flag = reunification
				}
			}
			modifier = {
				factor = 2
				CHI = { check_variable = { chinese_aggression > 0 } }
			}
			modifier = {
				factor = 2
				CHI = { check_variable = { chinese_aggression > 10 } }
			}
			modifier = {
				factor = 2
				CHI = { check_variable = { chinese_aggression > 20 } }
			}
			modifier = {
				factor = 2
				CHI = { check_variable = { chinese_aggression > 30 } }
			}
		}
	}
}

#Suspicious Chinese intelligence activity
country_event = {
	id = taiwan.9

	title = taiwan.9.t
	desc = taiwan.9.d
	picture = GFX_politics_eye

	fire_only_once = yes

	is_triggered_only = yes

	option = {
		name = taiwan.9.a #Seems harmless
		set_variable = {
			var = TAI_preparations
			value = 0
		}
		log = "[GetDateText]: [This.GetName]: taiwan.9.a executed"
	}

	option = {
		name = taiwan.9.b #We should investigate
		add_political_power = -100
		set_variable = {
			var = TAI_preparations
			value = 5
		}
		set_country_flag = TAI_investigated_suspicious_activity
		log = "[GetDateText]: [This.GetName]: taiwan.9.b executed"
	}
}


#China Demands an End to the Taiwan Relations Act!
country_event = {
	id = taiwan.10

	title = taiwan.10.t
	desc = taiwan.10.d
	picture = GFX_CHI_generic

	is_triggered_only = yes

	option = {
		name = taiwan.10.a #This demand is ludicrous!
		add_political_power = 25
		ai_chance = {
			factor = 50
		}
		log = "[GetDateText]: [This.GetName]: taiwan.10.a executed"
	}

	option = {
		name = taiwan.10.b #It is better for us to concede...
		diplomatic_relation = {
			country = TAI
			relation = guarantee
			active = no
		}
		ai_chance = {
			factor = 1
			modifier = {
				factor = 2
				USA = {
					OR = {
						AND = {
							check_variable = { influence_array^0 = CHI }
							check_variable = { influence_array_val^0 > 24.999 }
						}
						AND = {
							check_variable = { influence_array^1 = CHI }
							check_variable = { influence_array_val^1 > 24.999 }
						}
						AND = {
							check_variable = { influence_array^2 = CHI }
							check_variable = { influence_array_val^2 > 24.999 }
						}
					}
				}
			}
			modifier = {
				factor = 2
				USA = {
					OR = {
						AND = {
							check_variable = { influence_array^0 = CHI }
							check_variable = { influence_array_val^0 > 39.999 }
						}
						AND = {
							check_variable = { influence_array^1 = CHI }
							check_variable = { influence_array_val^1 > 39.999 }
						}
					}
				}
			}
			modifier = {
				factor = 4
				CHI = { has_completed_focus = CHI_End_US_Hegemony }
			}
			modifier = {
				factor = 2
				CHI = { check_variable = { chinese_aggression < 0 } }
			}
			modifier = {
				factor = 2
				CHI = { check_variable = { chinese_aggression < -10 } }
			}
			modifier = {
				factor = 2
				CHI = { check_variable = { chinese_aggression < -20 } }
			}
			modifier = {
				factor = 2
				CHI = { check_variable = { chinese_aggression < -30 } }
			}
		}
		log = "[GetDateText]: [This.GetName]: taiwan.10.b executed"
	}
}

#The Roadmap has been Accepted!
country_event = {
	id = taiwan.11

	title = taiwan.11.t
	desc = taiwan.11.d
	picture = GFX_politics_negotiations

	is_triggered_only = yes

	option = {
		name = taiwan.11.a #Rejoice!
		log = "[GetDateText]: [This.GetName]: taiwan.11.a executed"
	}
}

#The Roadmap has been Rejected!
country_event = {
	id = taiwan.12

	title = taiwan.12.t
	desc = taiwan.12.d
	picture = GFX_treaty_rejected

	is_triggered_only = yes

	option = {
		name = taiwan.12.a #They will regret this...
		log = "[GetDateText]: [This.GetName]: taiwan.12.a executed"
	}
}

# Taoyuan Flight Attendants Union strike
country_event = {
	id = taiwan.13
	title = taiwan.13.t
	desc = taiwan.13.d
	picture = GFX_TAI_flight_crew_strike

	fire_only_once = yes

	trigger = {
		tag = TAI
		date > 2016.6.24
	}

	option = {
		name = taiwan.13.a
		add_political_power = -50
		ai_chance = {
			factor = 10
		}
		log = "[GetDateText]: [This.GetName]: taiwan.13.a executed"
	}
  option = {
	name = taiwan.13.b
		random_list = {
			30 = { country_event = { days = 1 id = taiwan.14 } }
			70 = { country_event = { days = 1 id = taiwan.15 } }
		}
	log = "[GetDateText]: [This.GetName]: taiwan.13.b executed"
  }
}

# Taoyuan Flight Attendants Union strike - sucessful
country_event = {
	id = taiwan.14
	title = taiwan.14.t
	desc = taiwan.14.d
	picture = GFX_TAI_flight_crew_strike_solved

	is_triggered_only = yes

	option = {
		name = taiwan.14.a
		log = "[GetDateText]: [This.GetName]: taiwan.14.a executed"
	}
}

# Taoyuan Flight Attendants Union strike - failed
country_event = {
	id = taiwan.15
	title = taiwan.15.t
	desc = taiwan.15.d
	picture = GFX_TAI_flight_crew_strike

	is_triggered_only = yes

	option = {
		name = taiwan.15.a
		add_timed_idea = { idea = TAI_flight_crew_strike days = 31 }
		add_political_power = -25
		log = "[GetDateText]: [This.GetName]: taiwan.15.a executed"
	}
}

# Tank fell into river
country_event = {
	id = taiwan.16
	title = taiwan.16.t
	desc = taiwan.16.d
	picture = GFX_TAI_tank_bridge

	fire_only_once = yes

	trigger = {
		tag = TAI
		date > 2016.8.16
	}

	option = {
		name = taiwan.16.a
		600 = { add_manpower = -3 }
		log = "[GetDateText]: [This.GetName]: taiwan.16.a executed"
	}
}

#Move the capital?
country_event = {
	id = taiwan.17
	title = taiwan.17.t
	desc = taiwan.17.d
	picture = GFX_CHI_beijing
	
	fire_only_once = yes
	
	trigger = {
		original_tag = TAI
		has_war = no
		OR = {
			has_full_control_of_state = 547
			has_full_control_of_state = 536
		}
	}
	
	mean_time_to_happen = {
		days = 20
	}
	
	#Taiwan
	option = {
		name = taiwan.17.a
		trigger = {
			has_full_control_of_state = 598
		}
		set_capital = { state = 598 }
		log = "[GetDateText]: [This.GetName]: taiwan.17.a executed"
	}
	
	#Beijing
	option = {
		name = taiwan.17.b
		trigger = {
			has_full_control_of_state = 547
		}
		set_capital = { state = 547 }
		add_stability = 0.05
		log = "[GetDateText]: [This.GetName]: taiwan.17.b executed"
	}
	
	#Nanjing
	option = {
		name = taiwan.17.c
		trigger = {
			has_full_control_of_state = 536
		}
		set_capital = { state = 536 }
		log = "[GetDateText]: [This.GetName]: taiwan.17.c executed"
	}
}

# Taiwan Democratic Uprising
country_event = {
	id = taiwan.18
	title = taiwan.18.t
	desc = taiwan.18.d
	picture = GFX_internal_conflict
	
	is_triggered_only = yes
	
	option = {
		name = taiwan.18.a
		TAI = {
		start_civil_war = {
		ruling_party = democratic
		ideology = var:current_party_ideology_group
		size = 0.8
		capital = 598
		states = { 
	  598 599 430 1109
	}
	} }
	log = "[GetDateText]: [This.GetName]: taiwan.18.a executed"
	}
}

# ROC recover mainland
country_event = {
	id = taiwan.19
	title = taiwan.19.t
	desc = taiwan.19.d
	picture = GFX_TAI_roc

	fire_only_once = yes

	is_triggered_only = yes
	
	option = {
		name = taiwan.19.a
		swap_ideas = {
		remove_idea = partially_recognized_state	
		add_idea = p5_member }
		set_country_flag = TAI_roc_recover_mainland
		log = "[GetDateText]: [This.GetName]: taiwan.19.a executed"
		}
}

# Taiwan back to china
country_event = {
	id = taiwan.20
	title = taiwan.20.t
	desc = taiwan.20.d
	picture = GFX_CHI_generic
	
	is_triggered_only = yes
	
	option = {
		name = taiwan.20.a
		ai_chance = {
			base = 50
			modifier = {
				factor = 100
				TAI = {
					has_opinion = {
						target = CHI
						value < 74
					}
				}
			}
		}
		CHI = { 
			complete_national_focus = CHI_Establish_Taiwan_SAR
			}
		log = "[GetDateText]: [This.GetName]: taiwan.20.a executed"
		}
	option = {		
		name = taiwan.20.b
		ai_chance = {
			base = 0
		}
		CHI = { annex_country = {
		 target = TAI
			transfer_troops = yes
			}
			complete_national_focus = CHI_End_of_ROC
			change_tag_from = ROOT 
		}
		log = "[GetDateText]: [This.GetName]: taiwan.20.b executed"
	}
	option = {		
		name = taiwan.20.c
		ai_chance = {
			base = 50
			modifier = {
				factor = 100
				MON = {
					has_opinion = {
						target = CHI
						value > 74
					}
				}
			}
		}
		CHI = { annex_country = {
		 target = TAI
			transfer_troops = yes
			}
			complete_national_focus = CHI_End_of_ROC
		}
		log = "[GetDateText]: [This.GetName]: taiwan.20.b executed"
	}
}

# US Navy Seven Fleet Cross Taiwan Strait
country_event = {
	id = taiwan.21
	title = taiwan.21.t
	desc = taiwan.21.d
	picture = GFX_usn_seven_fleet

	fire_only_once = yes

	is_triggered_only = yes
	
	option = {
		name = taiwan.21.a
		add_stability = 0.05
		hidden_effect = {
		CHI = { country_event = china.61 } }
		log = "[GetDateText]: [This.GetName]: taiwan.21.a executed"
	}
}

# Allow gay marriages
country_event = {
	id = taiwan.22
	title = taiwan.22.t
	desc = taiwan.22.d
	picture = GFX_TAI_gay_pride

	fire_only_once = yes

	is_triggered_only = yes

	option = {
		name = taiwan.22.a
		add_ideas = TAI_gay_marriage
		add_political_power = -25
		log = "[GetDateText]: [This.GetName]: taiwan.22.a executed"
	}
}

# USA Claim Taiwan Strait Policy 
country_event = {
	id = taiwan.23
	title = taiwan.23.t
	desc = taiwan.23.d
	picture = GFX_USA_generic

	fire_only_once = yes

	is_triggered_only = yes
	
	option = {
		name = taiwan.23.a
		add_war_support = 0.1
		set_global_flag = usa_intervene_taiwan
		log = "[GetDateText]: [This.GetName]: taiwan.23.a executed"
	}

}

#puppet Philippine demand
country_event = {
	id = taiwan.24
	title = taiwan.24.t
	desc = taiwan.24.d
	picture = GFX_TAI_roc
	is_triggered_only = yes
	fire_only_once = yes

	option = { #Yes
		name = taiwan.24.a
		log = "[GetDateText]: [This.GetName]: taiwan.24.a executed"
		TAI = { puppet = PHI }
		ai_chance = {
			base = 25
			modifier = {
				factor = 80
				TAI = { num_divisions > 60 }
			}
		}
	}
	option = { #No
		name = taiwan.24.b
		log = "[GetDateText]: [This.GetName]: taiwan.24.b executed"
		TAI = { create_wargoal = {
				target = PHI
				type = puppet_wargoal_focus
				}
			}
		ai_chance = {
			base = 75
		}
	}
}

#puppet Malaysia demand
country_event = {
	id = taiwan.25
	title = taiwan.25.t
	desc = taiwan.25.d
	picture = GFX_TAI_roc
	is_triggered_only = yes
	fire_only_once = yes

	option = { #Yes
		name = taiwan.25.a
		log = "[GetDateText]: [This.GetName]: taiwan.25.a executed"
		TAI = { puppet = MAY }
		TAI = { puppet = BRU }
		TAI = { puppet = SIN }
		ai_chance = {
			base = 25
			modifier = {
				factor = 80
				TAI = { num_divisions > 60 }
			}
		}
	}
	option = { #No
		name = taiwan.25.b
		log = "[GetDateText]: [This.GetName]: taiwan.25.b executed"
		TAI = { create_wargoal = {
				target = MAY
				type = puppet_wargoal_focus
				}
			}
		TAI = { create_wargoal = {
				target = BRU
				type = puppet_wargoal_focus
				}
			}
		TAI = { create_wargoal = {
				target = SIN
				type = puppet_wargoal_focus
				}
			}
		ai_chance = {
			base = 75
		}
	}
}

#puppet Vietnam demand
country_event = {
	id = taiwan.26
	title = taiwan.26.t
	desc = taiwan.26.d
	picture = GFX_TAI_roc
	is_triggered_only = yes
	fire_only_once = yes

	option = { #Yes
		name = taiwan.26.a
		log = "[GetDateText]: [This.GetName]: taiwan.26.a executed"
		TAI = { puppet = VIE }
		TAI = { puppet = LAO }
		TAI = { puppet = CBD }
		ai_chance = {
			base = 25
			modifier = {
				factor = 80
				TAI = { num_divisions > 60 }
			}
		}
	}
	option = { #No
		name = taiwan.26.b
		log = "[GetDateText]: [This.GetName]: taiwan.26.b executed"
		TAI = { create_wargoal = {
				target = VIE
				type = puppet_wargoal_focus
				}
			}
		TAI = { create_wargoal = {
				target = LAO
				type = puppet_wargoal_focus
				}
			}
		TAI = { create_wargoal = {
				target = CBD
				type = puppet_wargoal_focus
				}
			}
		ai_chance = {
			base = 75
		}
	}
}

#puppet Thailand demand
country_event = {
	id = taiwan.27
	title = taiwan.27.t
	desc = taiwan.27.d
	picture = GFX_TAI_roc
	is_triggered_only = yes
	fire_only_once = yes

	option = { #Yes
		name = taiwan.27.a
		log = "[GetDateText]: [This.GetName]: taiwan.27.a executed"
		TAI = { puppet = SIA }
		ai_chance = {
			base = 25
			modifier = {
				factor = 80
				TAI = { num_divisions > 60 }
			}
		}
	}
	option = { #No
		name = taiwan.27.b
		log = "[GetDateText]: [This.GetName]: taiwan.27.b executed"
		TAI = { create_wargoal = {
				target = SIA
				type = puppet_wargoal_focus
				}
			}
		ai_chance = {
			base = 75
		}
	}
}

#puppet Indonesia demand
country_event = {
	id = taiwan.28
	title = taiwan.28.t
	desc = taiwan.28.d
	picture = GFX_TAI_roc
	is_triggered_only = yes
	fire_only_once = yes

	option = { #Yes
		name = taiwan.28.a
		log = "[GetDateText]: [This.GetName]: taiwan.28.a executed"
		TAI = { puppet = IND }
		ai_chance = {
			base = 25
			modifier = {
				factor = 80
				TAI = { num_divisions > 60 }
			}
		}
	}
	option = { #No
		name = taiwan.28.b
		log = "[GetDateText]: [This.GetName]: taiwan.28.b executed"
		TAI = { create_wargoal = {
				target = IND
				type = puppet_wargoal_focus
				}
			}
		ai_chance = {
			base = 75
		}
	}
}
###Taiwan regular news events

#CSSTA passed
news_event = {
	id = taiwan_news.1

	title = taiwan_news.1.t
	desc = taiwan_news.1.d
	picture = GFX_news_press_conference

	fire_only_once = yes

	is_triggered_only = yes
	
	major = yes

	option = {
		name = taiwan_news.1.a
		log = "[GetDateText]: [This.GetName]: taiwan_news.1.a executed"
	}
}

#The Sunflower Movement
news_event = {
	id = taiwan_news.2

	title = taiwan_news.2.t
	desc = taiwan_news.2.d
	picture = GFX_TAI_sunflower_news

	fire_only_once = yes

	is_triggered_only = yes
	
	major = yes

	option = {
		name = taiwan_news.2.a
		log = "[GetDateText]: [This.GetName]: taiwan_news.2.a executed"
	}
}

#Betrayed!
news_event = {
	id = taiwan_news.3

	title = taiwan_news.3.t
	desc = taiwan_news.3.d
	picture = GFX_news_generic_soldiers

	is_triggered_only = yes
	
	major = yes

	option = {
		name = taiwan_news.3.a
		effect_tooltip = {
			TAI = {
				activate_mission = prevent_coup_taiwan
				add_ideas = taiwanese_coup
			}
		}
		log = "[GetDateText]: [This.GetName]: taiwan_news.3.a executed"
	}
}

#The coup succeeds
news_event = {
	id = taiwan_news.4

	title = taiwan_news.4.t
	desc = taiwan_news.4.d
	picture = GFX_news_generic_soldiers

	is_triggered_only = yes
	
	major = yes

	option = {
		name = taiwan_news.4.a
		log = "[GetDateText]: [This.GetName]: taiwan_news.4.a executed"
	}
}

#The coup fails!
news_event = {
	id = taiwan_news.5

	title = taiwan_news.5.t
	desc = taiwan_news.5.d
	picture = GFX_news_generic_soldiers

	is_triggered_only = yes
	
	major = yes

	option = {
		name = taiwan_news.5.a
		trigger = { NOT = { original_tag = CHI } }
		log = "[GetDateText]: [This.GetName]: taiwan_news.5.a executed"
	}

	option = {
		name = taiwan_news.5.b
		trigger = { original_tag = CHI }
		log = "[GetDateText]: [This.GetName]: taiwan_news.5.b executed"
	}
}

#Taiwan Passes Bill to Counter Chinese Influence
news_event = {
	id = taiwan_news.6

	title = taiwan_news.6.t
	desc = taiwan_news.6.d
	picture = GFX_news_crime_fighting

	is_triggered_only = yes
	
	major = yes

	option = {
		name = taiwan_news.6.a #Taiwan fights back.
		log = "[GetDateText]: [This.GetName]: taiwan_news.6.a executed"
	}
}