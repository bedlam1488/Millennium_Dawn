	ITA_leonardo_di_marco={
		name="Leonardo Di Marco"
		portraits={
			army={
				small="gfx/leaders/ITA/Portrait_Leonardo_di_Marco.dds"
			}
			army={
				large="gfx/leaders/ITA/Portrait_Leonardo_di_Marco.dds"
			}
		}
		corps_commander={
			traits={ logistics_wizard }
			skill=3
			attack_skill=3
			defense_skill=3
			planning_skill=3
			logistics_skill=3
			legacy_id=30904
		}
	}
	ITA_stefano_mannino={
		name="Stefano Mannino"
		portraits={
			army={
				small="gfx/leaders/ITA/small/Small_Portrait_Stefano_Mannino.dds"
			}
			army={
				large="gfx/leaders/ITA/Portrait_Stefano_Mannino.dds"
			}
		}
		corps_commander={
			traits={ commando urban_assault_specialist }
			skill=1
			attack_skill=1
			defense_skill=1
			planning_skill=1
			logistics_skill=1
			legacy_id=30906
		}
	}
	ITA_bruno_stano={
		name="Bruno Stano"
		portraits={
			army={
				small="gfx/leaders/ITA/small/Small_Portrait_Bruno_Stano.dds"
			}
			army={
				large="gfx/leaders/ITA/Portrait_Bruno_Stano.dds"
			}
		}
		corps_commander={
			traits={ fortress_buster }
			skill=3
			attack_skill=3
			defense_skill=3
			planning_skill=3
			logistics_skill=3
			legacy_id=30907
		}
	}
	ITA_luigi_francesco_de_leverano={
		name="Luigi Francesco de Leverano"
		portraits={
			army={
				small="gfx/leaders/ITA/small/Small_Portrait_Luigi_de_Leverano.dds"
			}
			army={
				large="gfx/leaders/ITA/Portrait_Luigi_de_Leverano.dds"
			}
		}
		corps_commander={
			traits={ fortress_buster }
			skill=3
			attack_skill=3
			defense_skill=3
			planning_skill=3
			logistics_skill=3
			legacy_id=30908
		}
	}
	ITA_amedeo_sperotto={
		name="Amedeo Sperotto"
		portraits={
			army={
				small="gfx/leaders/ITA/small/Small_Portrait_Amedeo_Sperotto.dds"
			}
			army={
				large="gfx/leaders/ITA/Portrait_Amedeo_Sperotto.dds"
			}
		}
		corps_commander={
			traits={ trait_engineer }
			skill=3
			attack_skill=3
			defense_skill=3
			planning_skill=3
			logistics_skill=3
			legacy_id=30909
		}
	}
	ITA_marcello_bellacicco={
		name="Marcello Bellacicco"
		portraits={
			army={
				small="gfx/leaders/ITA/small/Small_Portrait_Marcelo_Bellacicco.dds"
			}
			army={
				large="gfx/leaders/ITA/Portrait_Marcelo_Bellacicco.dds"
			}
		}
		corps_commander={
			traits={ trait_mountaineer }
			skill=2
			attack_skill=2
			defense_skill=2
			planning_skill=2
			logistics_skill=2
			legacy_id=30910
		}
	}
	ITA_luca_felicissimo={
		name="Luca Felicissimo"
		portraits={
			army={
				small="gfx/leaders/ITA/small/Small_Portrait_Luca_Felicissimo.dds"
			}
			army={
				large="gfx/leaders/ITA/Portrait_Luca_Felicissimo.dds"
			}
		}
		corps_commander={
			traits={ commando }
			skill=1
			attack_skill=1
			defense_skill=1
			planning_skill=1
			logistics_skill=1
			legacy_id=30911
		}
	}
	ITA_marco_bertolini={
		name="Marco Bertolini"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Marco_Bertolini.dds"
			}
		}
		corps_commander={
			traits={ commando urban_assault_specialist }
			skill=3
			attack_skill=3
			defense_skill=3
			planning_skill=3
			logistics_skill=3
			legacy_id=30912
		}
	}
	ITA_paolo_fortezza={
		name="Paolo Fortezza"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Paolo_Fortezza.dds"
			}
		}
		corps_commander={
			traits={ commando }
			skill=1
			attack_skill=1
			defense_skill=1
			planning_skill=1
			logistics_skill=1
			legacy_id=30913
		}
	}
	ITA_alessandro_grassano={
		name="Alessandro Grassano"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Alesandro_Grassano.dds"
			}
		}
		corps_commander={
			traits={ commando }
			skill=1
			attack_skill=1
			defense_skill=1
			planning_skill=1
			logistics_skill=1
			legacy_id=30914
		}
	}
	ITA_alberto_primicerj={
		name="Alberto Primicerj"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Alberto_Primicerj.dds"
			}
		}
		corps_commander={
			traits={ trickster }
			skill=3
			attack_skill=3
			defense_skill=3
			planning_skill=3
			logistics_skill=3
			legacy_id=30915
		}
	}
	ITA_antonio_vittiglio={
		name="Antonio Vittiglio"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Antonio_Vittiglio.dds"
			}
		}
		corps_commander={
			traits={ trait_engineer }
			skill=2
			attack_skill=2
			defense_skill=2
			planning_skill=2
			logistics_skill=2
			legacy_id=30916
		}
	}
	ITA_antonino_poma={
		name="Antonino Poma"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Antonino_Poma.dds"
			}
		}
		corps_commander={
			traits={ ranger }
			skill=1
			attack_skill=1
			defense_skill=1
			planning_skill=1
			logistics_skill=1
			legacy_id=30917
		}
	}
	ITA_gianpaolo_mirra={
		name="Gianpaolo Mirra"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Gianpaolo_Mirra.dds"
			}
		}
		corps_commander={
			traits={ panzer_leader }
			skill=1
			attack_skill=1
			defense_skill=1
			planning_skill=1
			logistics_skill=1
			legacy_id=30918
		}
	}
	ITA_gianluca_carai={
		name="Gianluca Carai"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Gianluca_Carai.dds"
			}
		}
		corps_commander={
			traits={ panzer_leader }
			skill=1
			attack_skill=1
			defense_skill=1
			planning_skill=1
			logistics_skill=1
			legacy_id=30919
		}
	}
	ITA_maurizio_settesoldi={
		name="Maurizio Settesoldi"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Maurizio_Setttesoldi.dds"
			}
		}
		corps_commander={
			traits={ hill_fighter }
			skill=1
			attack_skill=1
			defense_skill=1
			planning_skill=1
			logistics_skill=1
			legacy_id=30920
		}
	}
	ITA_nicola_terzano={
		name="Nicola Terzano"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Nicola_Terzano.dds"
			}
		}
		corps_commander={
			traits={ ranger }
			skill=1
			attack_skill=1
			defense_skill=1
			planning_skill=1
			logistics_skill=1
			legacy_id=30921
		}
	}
	ITA_federico_bonato={
		name="Federico Bonato"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Federico_Bonato.dds"
			}
		}
		corps_commander={
			traits={ trait_mountaineer }
			skill=3
			attack_skill=3
			defense_skill=3
			planning_skill=3
			logistics_skill=3
			legacy_id=30922
		}
	}
	ITA_paolo_fabbri={
		name="Paolo Fabbri"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Paolo_Fabbri.dds"
			}
		}
		corps_commander={
			traits={ trait_mountaineer }
			skill=1
			attack_skill=1
			defense_skill=1
			planning_skill=1
			logistics_skill=1
			legacy_id=30923
		}
	}
	ITA_allessandro_scano={
		name="Allessandro Scano"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Allessandro_Scano.dds"
			}
		}
		corps_commander={
			traits={ trait_mountaineer }
			skill=1
			attack_skill=1
			defense_skill=1
			planning_skill=1
			logistics_skill=1
			legacy_id=30924
		}
	}
	ITA_stefano_santoro={
		name="Stefano Santoro"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Stefano_Santoro.dds"
			}
		}
		corps_commander={
			traits={ trait_mountaineer }
			skill=1
			attack_skill=1
			defense_skill=1
			planning_skill=1
			logistics_skill=1
			legacy_id=30925
		}
	}
	ITA_luigi_musti={
		name="Luigi Musti"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Luigi_Musti.dds"
			}
		}
		corps_commander={
			traits={ trickster trait_mountaineer }
			skill=1
			attack_skill=1
			defense_skill=1
			planning_skill=1
			logistics_skill=1
			legacy_id=30926
		}
	}
	ITA_aldo_mezzalana={
		name="Aldo Mezzalana"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Aldo_Mezzalana.dds"
			}
		}
		corps_commander={
			traits={ commando }
			skill=1
			attack_skill=1
			defense_skill=1
			planning_skill=1
			logistics_skill=1
			legacy_id=30927
		}
	}
	ITA_gianluigi_de_matteis={
		name="Gianluigi De Matteis"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Gianluigi_De_Matteis.dds"
			}
		}
		corps_commander={
			traits={ commando }
			skill=1
			attack_skill=1
			defense_skill=1
			planning_skill=1
			logistics_skill=1
			legacy_id=30928
		}
	}
	ITA_maurizio_scardino={
		name="Maurizio Scardino"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Maurizio_Scardino.dds"
			}
		}
		corps_commander={
			traits={ trickster }
			skill=2
			attack_skill=2
			defense_skill=2
			planning_skill=2
			logistics_skill=2
			legacy_id=30929
		}
	}
	ITA_marco_piacentini={
		name="Marco Piacentini"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Marco_Piacentini.dds"
			}
		}
		corps_commander={
			traits={ trickster }
			skill=1
			attack_skill=1
			defense_skill=1
			planning_skill=1
			logistics_skill=1
			legacy_id=30930
		}
	}
	ITA_andrea_di_stasio={
		name="Andrea Di Stasio"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Andrea_Di_Stasio.dds"
			}
		}
		corps_commander={
			traits={ trait_engineer commando }
			skill=1
			attack_skill=1
			defense_skill=1
			planning_skill=1
			logistics_skill=1
			legacy_id=30931
		}
	}
	ITA_gianfranco_lauria_pantano={
		name="Gianfranco Lauria Pantano"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Gianfranco_Pantano.dds"
			}
		}
		corps_commander={
			traits={ trait_engineer commando }
			skill=1
			attack_skill=1
			defense_skill=1
			planning_skill=1
			logistics_skill=1
			legacy_id=30932
		}
	}
	ITA_roberto_perretti={
		name="Roberto Perretti"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Roberto_Perretti.dds"
			}
		}
		corps_commander={
			traits={ trait_engineer ranger }
			skill=3
			attack_skill=3
			defense_skill=3
			planning_skill=3
			logistics_skill=3
			legacy_id=30933
		}
	}
	ITA_roberto_vannacci={
		name="Roberto Vannacci"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Roberto_Fannacci.dds"
			}
		}
		corps_commander={
			traits={ commando }
			skill=1
			attack_skill=1
			defense_skill=1
			planning_skill=1
			logistics_skill=1
			legacy_id=30934
		}
	}
	ITA_aurelio_tassi={
		name="Aurelio Tassi"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Aurelio_Tassi.dds"
			}
		}
		corps_commander={
			traits={ ranger }
			skill=1
			attack_skill=1
			defense_skill=1
			planning_skill=1
			logistics_skill=1
			legacy_id=30935
		}
	}
	ITA_carlo_di_pasquale={
		name="Carlo Di Pasquale"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Carlo_Di_Pasquale.dds"
			}
		}
		corps_commander={
			traits={ trickster }
			skill=1
			attack_skill=1
			defense_skill=1
			planning_skill=1
			logistics_skill=1
			legacy_id=30936
		}
	}
	ITA_ugo_cillo={
		name="Ugo Cillo"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Ugo_Cillo.dds"
			}
		}
		corps_commander={
			traits={ ranger }
			skill=1
			attack_skill=1
			defense_skill=1
			planning_skill=1
			logistics_skill=1
			legacy_id=30937
		}
	}
	ITA_massimiliano_stecca={
		name="Massimiliano Stecca"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Massimiliano_Stecca.dds"
			}
		}
		corps_commander={
			traits={ naval_invader }
			skill=1
			attack_skill=1
			defense_skill=1
			planning_skill=1
			logistics_skill=1
			legacy_id=30938
		}
	}
	ITA_antonello_vespeziani={
		name="Antonello Vespeziani"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Antonello_Vespeziani.dds"
			}
		}
		corps_commander={
			traits={ panzer_leader }
			skill=1
			attack_skill=1
			defense_skill=1
			planning_skill=1
			logistics_skill=1
			legacy_id=30939
		}
	}
	ITA_stefano_angioni={
		name="Stefano Angioni"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Stefano_Angioni.dds"
			}
		}
		corps_commander={
			traits={ commando }
			skill=1
			attack_skill=1
			defense_skill=1
			planning_skill=1
			logistics_skill=1
			legacy_id=30940
		}
	}
	ITA_giovanni_rossitto={
		name="Giovanni Rossitto"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Giovanni_Rossitto.dds"
			}
		}
		corps_commander={
			traits={ trait_engineer }
			skill=1
			attack_skill=1
			defense_skill=1
			planning_skill=1
			logistics_skill=1
			legacy_id=30941
		}
	}
	ITA_agostino_biancafarini={
		name="Agostino Biancafarini"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Agostino_Biancafarina.dds"
			}
		}
		corps_commander={
			traits={ artillery_officer }
			skill=2
			attack_skill=2
			defense_skill=2
			planning_skill=2
			logistics_skill=2
			legacy_id=30942
		}
	}
	ITA_salvatore_camporeale={
		name="Salvatore Camporeale"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Salvatore_Camporeale.dds"
			}
		}
		corps_commander={
			traits={ trickster }
			skill=2
			attack_skill=2
			defense_skill=2
			planning_skill=2
			logistics_skill=2
			legacy_id=30943
		}
	}
	ITA_enzo_vecciarelli={
		name="Enzo Vecciarelli"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Enzo_Vecciarelli.dds"
			}
		}
		corps_commander={
			skill=3
			attack_skill=3
			defense_skill=3
			planning_skill=3
			logistics_skill=3
			legacy_id=30944
		}
	}
	ITA_tullio_del_sette={
		name="Tullio Del Sette"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Tullio_Del_Sette.dds"
			}
		}
		corps_commander={
			traits={ urban_assault_specialist }
			skill=4
			attack_skill=4
			defense_skill=4
			planning_skill=4
			logistics_skill=4
			legacy_id=30945
		}
	}
	ITA_vincenzo_coppola={
		name="Vincenzo Coppola"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Vincenzo_Coppola.dds"
			}
		}
		corps_commander={
			traits={ urban_assault_specialist }
			skill=3
			attack_skill=3
			defense_skill=3
			planning_skill=3
			logistics_skill=3
			legacy_id=30946
		}
	}
	ITA_giorgio_toschi={
		name="Giorgio Toschi"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Giorgio_Toschi.dds"
			}
		}
		corps_commander={
			traits={ urban_assault_specialist }
			skill=3
			attack_skill=3
			defense_skill=3
			planning_skill=3
			logistics_skill=3
			legacy_id=30947
		}
	}
	ITA_filippo_ritondale={
		name="Filippo Ritondale"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Filippo_Ritondale.dds"
			}
		}
		corps_commander={
			traits={ urban_assault_specialist }
			skill=2
			attack_skill=2
			defense_skill=2
			planning_skill=2
			logistics_skill=2
			legacy_id=30948
		}
	}
	ITA_gabriele_lupini={
		name="Gabriele Lupini"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Gabriele_Lupini.dds"
			}
		}
		corps_commander={
			traits={ trait_engineer }
			skill=2
			attack_skill=2
			defense_skill=2
			planning_skill=2
			logistics_skill=2
			legacy_id=30949
		}
	}

	ITA_claudio_mora={
		name="Claudio Mora"
		portraits={
			army={
				small="gfx/leaders/ITA/Portrait_Claudio_Mora.dds"
			}
			army={
				large="gfx/leaders/ITA/Portrait_Claudio_Mora.dds"
			}
		}
		field_marshal={
			traits={ thorough_planner }
			skill=3
			attack_skill=3
			defense_skill=3
			planning_skill=3
			logistics_skill=3
			legacy_id=30902
		}
	}

	ITA_pietro_serino={
		name="Pietro Serino"
		portraits={
			army={
				small="gfx/leaders/ITA/Portrait_Pietro_Serino.dds"
			}
			army={
				large="gfx/leaders/ITA/Portrait_Pietro_Serino.dds"
			}
		}
		field_marshal={
			traits={ defensive_doctrine }
			skill=3
			attack_skill=3
			defense_skill=3
			planning_skill=3
			logistics_skill=3
			legacy_id=30903
		}
	}
	ITA_riccardo_marchiò={
		name="Riccardo Marchiò"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Ricardo_Marchio.dds"
			}
		}
		field_marshal={
			traits={ fast_planner }
			skill=3
			attack_skill=3
			defense_skill=3
			planning_skill=3
			logistics_skill=3
			legacy_id=30905
		}
	}

#NAVY

	ITA_valter_girardelli={
		name="Valter Girardelli"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Valter_Girardelli.dds"
			}
		}
		navy_leader={
			traits={ old_guard_navy superior_tactician }
			skill=2
			attack_skill=2
			defense_skill=2
			maneuvering_skill=1
			coordination_skill=1
			legacy_id=30950
		}
	}
	ITA_vincenzo_melone={
		name="Vincenzo Melone"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Vincenzo_Melone.dds"
			}
		}
		navy_leader={
			traits={ ironside }
			skill=1
			attack_skill=2
			defense_skill=2
			maneuvering_skill=1
			coordination_skill=1
			legacy_id=30951
		}
	}
	ITA_paolo_pezzuti={
		name="Paolo Pezzuti"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Paolo_Pezzutti.dds"
			}
		}
		navy_leader={
			traits={ seawolf }
			skill=1
			attack_skill=1
			defense_skill=1
			maneuvering_skill=1
			coordination_skill=1
			legacy_id=30952
		}
	}
	ITA_donato_marzano={
		name="Donato Marzano"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Donato_Marzano.dds"
			}
		}
		navy_leader={
			traits={ fly_swatter }
			skill=1
			attack_skill=1
			defense_skill=1
			maneuvering_skill=1
			coordination_skill=1
			legacy_id=30953
		}
	}
	ITA_giuseppe_de_giorgi={
		name="Giuseppe De Giorgi"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Giuseppe_De_Giorgio.dds"
			}
		}
		navy_leader={
			traits={ old_guard_navy air_controller }
			skill=2
			attack_skill=2
			defense_skill=2
			maneuvering_skill=1
			coordination_skill=1
			legacy_id=30954
		}
	}
	ITA_stefano_tortora={
		name="Stefano Tortora"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Stefano_Tortora.dds"
			}
		}
		navy_leader={
			traits={ spotter }
			skill=1
			attack_skill=1
			defense_skill=1
			maneuvering_skill=1
			coordination_skill=1
			legacy_id=30955
		}
	}
	ITA_enrico_mascia={
		name="Enrico Mascia"
		portraits={
			army={
				small=""
			}
			army={
				large="gfx/leaders/ITA/Portrait_Enrico_Mascia.dds"
			}
		}
		navy_leader={
			traits={ blockade_runner }
			skill=1
			attack_skill=1
			defense_skill=1
			maneuvering_skill=1
			coordination_skill=1
			legacy_id=30956
		}
	}