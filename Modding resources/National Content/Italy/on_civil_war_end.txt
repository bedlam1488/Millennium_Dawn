
				ROOT = {
					load_focus_tree = italy_focus
					if = {
						limit = {
							FROM = {
								has_dynamic_modifier = ITA_party_popularity_drift_modifier
							}
						}
						add_dynamic_modifier = { modifier = ITA_party_popularity_drift_modifier }
						set_variable = { fascism_drift_var = ITA.fascism_drift_var }
						set_variable = { communism_drift_var = ITA.communism_drift_var }
						set_variable = { democratic_drift_var = ITA.democratic_drift_var }
						set_variable = { neutrality_drift_var = ITA.neutrality_drift_var }
						set_variable = { nationalist_drift_var = ITA.nationalist_drift_var }
					}
					if = {
						limit = {
							FROM = {
								has_dynamic_modifier = ITA_party_propaganda_modifier
							}
						}
						add_dynamic_modifier = { modifier = ITA_party_propaganda_modifier }
						set_variable = { fascism_propaganda_var = ITA.fascism_propaganda_var }
						set_variable = { communism_propaganda_var = ITA.communism_propaganda_var }
						set_variable = { democratic_propaganda_var = ITA.democratic_propaganda_var }
						set_variable = { neutrality_propaganda_var = ITA.neutrality_propaganda_var }
						set_variable = { nationalist_propaganda_var = ITA.nationalist_propaganda_var }
					}
					if = {
						limit = {
							FROM = {
								has_dynamic_modifier = ITA_mafia_modifier
							}
						}
						add_dynamic_modifier = { modifier = ITA_mafia_modifier }
						set_variable = { mafia_strength_stab_var = ITA.mafia_strength_stab_var }
						set_variable = { mafia_strength_construction_var = ITA.mafia_strength_construction_var }
						set_variable = { mafia_strength_pp_var = ITA.mafia_strength_pp_var }
						set_variable = { mafia_strength_lega_drift_var = ITA.mafia_strength_lega_drift_var }
					}
					if = {
						limit = {
							FROM = {
								has_dynamic_modifier = ITA_nimby_culture
							}
						}
						add_dynamic_modifier = { modifier = ITA_nimby_culture }
						set_variable = { nimby_culture_var = ITA.nimby_culture_var }
					}
					if = {
						limit = {
							FROM = {
								has_dynamic_modifier = ITA_multipurpose_modifier
							}
						}
						add_dynamic_modifier = { modifier = ITA_multipurpose_modifier }
						set_variable = { ITA_tax_gain_multiplier_modifier_var = ITA.ITA_tax_gain_multiplier_modifier_var }
						set_variable = { ITA_interest_rate_multiplier_modifier_var = ITA.ITA_interest_rate_multiplier_modifier_var }
						set_variable = { ITA_bureaucracy_cost_multiplier_modifier_var = ITA.ITA_bureaucracy_cost_multiplier_modifier_var }
						set_variable = { ITA_police_cost_multiplier_modifier_var = ITA.ITA_police_cost_multiplier_modifier_var }
						set_variable = { ITA_education_cost_multiplier_modifier_var = ITA.ITA_education_cost_multiplier_modifier_var }
						set_variable = { ITA_health_cost_multiplier_modifier_var = ITA.ITA_health_cost_multiplier_modifier_var }
						set_variable = { ITA_social_cost_multiplier_modifier_var = ITA.ITA_social_cost_multiplier_modifier_var }
						set_variable = { ITA_army_personnel_cost_multiplier_modifier_var = ITA.ITA_army_personnel_cost_multiplier_modifier_var }
						set_variable = { ITA_navy_personnel_cost_multiplier_modifier_var = ITA.ITA_navy_personnel_cost_multiplier_modifier_var }
						set_variable = { ITA_airforce_personnel_cost_multiplier_modifier_var = ITA.ITA_airforce_personnel_cost_multiplier_modifier_var }
						set_variable = { ITA_equipment_cost_multiplier_modifier_var = ITA.ITA_equipment_cost_multiplier_modifier_var }
						set_variable = { ITA_global_building_slots_factor_var = ITA.ITA_global_building_slots_factor_var }
						set_variable = { ITA_MONTHLY_POPULATION_var = ITA.ITA_MONTHLY_POPULATION_var }
						set_variable = { ITA_production_speed_buildings_factor_var = ITA.ITA_production_speed_buildings_factor_var }
						set_variable = { ITA_stability_factor_var = ITA.ITA_stability_factor_var }
						set_variable = { ITA_stability_weekly_var = ITA.ITA_stability_weekly_var }
						set_variable = { ITA_research_speed_factor_var = ITA.ITA_research_speed_factor_var }
						set_variable = { ITA_production_factory_max_efficiency_factor_var = ITA.ITA_production_factory_max_efficiency_factor_var }
						set_variable = { ITA_production_factory_efficiency_gain_factor_var = ITA.ITA_production_factory_efficiency_gain_factor_var }
						set_variable = { ITA_supply_consumption_factor_var = ITA.ITA_supply_consumption_factor_var }
						set_variable = { ITA_army_org_regain_var = ITA.ITA_army_org_regain_var }
						set_variable = { ITA_army_org_Factor_var = ITA.ITA_army_org_Factor_var }
						set_variable = { ITA_repair_speed_factor_var = ITA.ITA_repair_speed_factor_var }
						set_variable = { ITA_refit_speed_var = ITA.ITA_refit_speed_var }
						set_variable = { ITA_org_loss_when_moving_var = ITA.ITA_org_loss_when_moving_var }
						set_variable = { ITA_army_attack_factor_var = ITA.ITA_army_attack_factor_var }
						set_variable = { ITA_army_defence_factor_var = ITA.ITA_army_defence_factor_var }
						set_variable = { ITA_training_time_factor_var = ITA.ITA_training_time_factor_var }
						set_variable = { ITA_attrition_var = ITA.ITA_attrition_var }
						set_variable = { ITA_conscription_var = ITA.ITA_conscription_var }
						set_variable = { ITA_justify_war_goal_time_var = ITA.ITA_justify_war_goal_time_var }
						set_variable = { ITA_send_volunteer_divisions_required_var = ITA.ITA_send_volunteer_divisions_required_var }
						set_variable = { ITA_send_volunteer_factor_var = ITA.ITA_send_volunteer_factor_var }
						set_variable = { ITA_air_volunteer_cap_var = ITA.ITA_air_volunteer_cap_var }
						set_variable = { ITA_experience_gain_army_var = ITA.ITA_experience_gain_army_var }
						set_variable = { ITA_navy_fuel_consumption_factor_var = ITA.ITA_navy_fuel_consumption_factor_var }
						set_variable = { ITA_navy_max_range_factor_var = ITA.ITA_navy_max_range_factor_var }
						set_variable = { ITA_cas_damage_reduction_var = ITA.ITA_cas_damage_reduction_var }
						set_variable = { ITA_air_superiority_bonus_in_combat_var = ITA.ITA_air_superiority_bonus_in_combat_var }
						set_variable = { ITA_air_strategic_bomber_bombing_factor_var = ITA.ITA_air_strategic_bomber_bombing_factor_var }
						set_variable = { ITA_industry_air_damage_factor_var = ITA.ITA_industry_air_damage_factor_var }
						set_variable = { ITA_foreign_influence_defense_modifier_var = ITA.ITA_foreign_influence_defense_modifier_var }
						set_variable = { ITA_foreign_influence_modifier_var = ITA.ITA_foreign_influence_modifier_var }
						set_variable = { ITA_subjects_autonomy_gain_var = ITA.ITA_subjects_autonomy_gain_var }
					}
				}