ideas = {
	country = {
		##Milf System
		PHI_milf_effect_one = {
			on_add = { log = "[GetDateText]: [Root.GetName]: idea PHI_milf_effect_one" }
			picture = Ethnic_Insurgencies
			modifier = {
				stability_factor = -0.02
				political_power_gain = -0.05
				fascism_drift = 0.01
			}
		}

		PHI_milf_effect_two = {
			on_add = { log = "[GetDateText]: [Root.GetName]: idea PHI_milf_effect_two" }
			picture = Ethnic_Insurgencies
			modifier = {
				stability_factor = -0.04
				political_power_gain = -0.10
				fascism_drift = 0.03
			}
		}

		PHI_milf_effect_three = {
			on_add = { log = "[GetDateText]: [Root.GetName]: idea PHI_milf_effect_three" }
			picture = Ethnic_Insurgencies
			modifier = {
				stability_factor = -0.06
				political_power_gain = -0.15
				fascism_drift = 0.05
			}
		}

		PHI_milf_effect_four = {
			on_add = { log = "[GetDateText]: [Root.GetName]: idea PHI_milf_effect_four" }
			picture = Ethnic_Insurgencies
			modifier = {
				stability_factor = -0.08
				political_power_gain = -0.20
				fascism_drift = 0.075
			}
		}

		PHI_milf_effect_five = {
			on_add = { log = "[GetDateText]: [Root.GetName]: idea PHI_milf_effect_five" }
			picture = Ethnic_Insurgencies
			modifier = {
				stability_factor = -0.10
				political_power_gain = -0.25
				fascism_drift = 0.10
			}
		}

		##Filipino Syncretism
		PHI_syncretism_one_focus = {
			picture = filipino_syncretism
			on_add = { log = "[GetDateText]: [Root.GetName]: idea PHI_syncretism_one_focus" }
			removal_cost = -1

			modifier = {
				political_power_factor = 0.05
				stability_factor = 0.02
				consumer_goods_factor = 0.01
				production_factory_max_efficiency_factor = -0.05
			}
		}

		PHI_syncretism_two_focus = {
			picture = filipino_syncretism
			on_add = { log = "[GetDateText]: [Root.GetName]: idea PHI_syncretism_two_focus" }
			removal_cost = -1

			modifier = {
				political_power_factor = 0.10
				stability_factor = 0.03
				consumer_goods_factor = 0.03
				production_factory_max_efficiency_factor = -0.10
			}
		}

		PHI_syncretism_three_focus = {
			picture = filipino_syncretism
			on_add = {
				log = "[GetDateText]: [Root.GetName]: idea PHI_syncretism_three_focus"
			}
			removal_cost = -1

			modifier = {
				political_power_factor = 0.15
				stability_factor = 0.05
				consumer_goods_factor = 0.05
				production_factory_max_efficiency_factor = -0.15
			}
		}
	}
}
