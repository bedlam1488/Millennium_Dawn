focus_tree = {
	id = Rojava_Focus

	country = {
		factor = 0

		modifier = {
			add = 10
			tag = ROJ
		}
	}

	default = no

	#Main SDF branch
	focus = {
		id = ROJ_recognize_minorities

		cost = 5

		x = 6
		y = 0

		bypass = {
			has_government = nationalist
		}

		cancel_if_invalid = yes
		available_if_capitulated = no

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ROJ_recognize_minorities"
			add_stability = 0.05
		}
	}

	focus = {
		id = ROJ_raqqa_offensive

		cost = 5

		x = -4
		y = 0

		bypass = {
			has_war = no
		}

		relative_position_id = ROJ_recognize_minorities

		cancel_if_invalid = yes
		available_if_capitulated = no

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ROJ_raqqa_offensive"
			191 = { add_core_of = ROJ }
			995 = { add_core_of = ROJ }
			add_timed_idea = {
				idea = rojava_raqqa_offensive
				days = 365
			}
		}
	}

	#Army sub-branch
	focus = {
		id = ROJ_centralize_militias

		cost = 10

		x = 0
		y = 1

		relative_position_id = ROJ_raqqa_offensive

		prerequisite = {
			focus = ROJ_raqqa_offensive
		}

		mutually_exclusive = {
			focus = ROJ_decentralize_militias
		}

		cancel_if_invalid = yes
		available_if_capitulated = no

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ROJ_centralize_militias"
			add_ideas = rojava_centralized_militias
		}
	}

	focus = {
		id = ROJ_decentralize_militias

		cost = 10

		x = -2
		y = 1

		relative_position_id = ROJ_recognize_minorities

		prerequisite = {
			focus = ROJ_recognize_minorities
		}

		mutually_exclusive = {
			focus = ROJ_centralize_militias
		}

		cancel_if_invalid = yes
		available_if_capitulated = no

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ROJ_decentralize_militias"
			#TODO spawn units
		}
	}

	focus = {
		id = ROJ_decentralized_warfare

		cost = 10

		x = 0
		y = 1

		relative_position_id = ROJ_decentralize_militias

		prerequisite = {
			focus = ROJ_decentralize_militias
		}

		cancel_if_invalid = yes
		available_if_capitulated = no

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ROJ_decentralized_warfare"
			add_tech_bonus = {
				bonus = 1
				uses = 2
				category = Cat_DECENTRALIZED_WARFARE
			}
		}
	}

	focus = {
		id = ROJ_officer_corps

		cost = 10

		x = 0
		y = 1

		relative_position_id = ROJ_centralize_militias

		prerequisite = {
			focus = ROJ_centralize_militias
			focus = ROJ_decentralize_militias
		}

		cancel_if_invalid = yes
		available_if_capitulated = no

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ROJ_officer_corps"
			army_experience = 40
			add_ideas = rojava_officer_corps
		}
	}

	focus = {
		id = ROJ_military_industry

		cost = 10

		x = -2
		y = 1

		relative_position_id = ROJ_centralize_militias

		prerequisite = {
			focus = ROJ_centralize_militias
		}

		cancel_if_invalid = yes
		available_if_capitulated = no

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ROJ_military_industry"
			random_owned_state = {
				add_building_construction = {
					type = arms_factory
					level = 1
					instant_build = yes
				}
			}
			custom_effect_tooltip = 7.5_billion_expense_tt
			subtract_from_variable = { treasury = 7.5 }
		}
	}

	#Internal sub-branch
	focus = {
		id = ROJ_reverse_arabization

		cost = 10

		x = 2
		y = 1

		relative_position_id = ROJ_recognize_minorities

		prerequisite = {
			focus = ROJ_recognize_minorities
		}

		cancel_if_invalid = yes
		available_if_capitulated = no

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ROJ_reverse_arabization"
			generic_lower_40_percent_influence_of_SYR = yes
		}
	}

	focus = {
		id = ROJ_fix_education

		cost = 10

		x = -2
		y = 1

		relative_position_id = ROJ_reverse_arabization

		prerequisite = {
			focus = ROJ_reverse_arabization
		}

		cancel_if_invalid = yes
		available_if_capitulated = no

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ROJ_fix_education"

			add_ideas = rojava_education_bonus
			if = {
				limit = { has_idea = edu_01 }
				add_ideas = edu_02
			}
		}
	}

	focus = {
		id = ROJ_bring_emigrants_back

		cost = 10

		x = 0
		y = 1

		relative_position_id = ROJ_reverse_arabization

		prerequisite = {
			focus = ROJ_reverse_arabization
		}

		cancel_if_invalid = yes
		available_if_capitulated = no

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ROJ_bring_emigrants_back"

			add_timed_idea = {
				idea = rojava_kurds_immigrating
				days = 730
			}
		}
	}

	focus = {
		id = ROJ_found_ypg_international

		cost = 10

		x = 2
		y = 1

		relative_position_id = ROJ_reverse_arabization

		cancel_if_invalid = yes
		available_if_capitulated = no

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ROJ_found_ypg_international"

			add_ideas = rojava_international_volunteers
		}
	}

	focus = {
		id = ROJ_promote_ypg_international

		cost = 10

		x = 0
		y = 1

		relative_position_id = ROJ_found_ypg_international

		prerequisite = {
			focus = ROJ_found_ypg_international
		}
		prerequisite = {
			focus = ROJ_bring_emigrants_back
		}

		cancel_if_invalid = yes
		available_if_capitulated = no

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ROJ_found_ypg_international"

			swap_ideas = {
				remove_idea = rojava_international_volunteers
				add_idea = rojava_international_volunteers2
			}
		}
	}

	#External branch

	focus = {
		id = ROJ_look_to_USA
		icon = american_partnerships

		cost = 10

		x = 8
		y = 0
		relative_position_id = ROJ_recognize_minorities

		available = {
			NOT = {
				has_government = fascism
				has_war_with = USA
			}
		}

		cancel_if_invalid = yes
		available_if_capitulated = no

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus ROJ_look_to_USA"
			if = {
				limit = {
					NOT = { has_idea = USA_usaid }
				}
				country_event = {
					id = rojava.1
				}
			}
		}

		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.1
				OR = {
					has_government = fascist
					has_government = nationalist
				}
			}
			modifier = {
				factor = 0.25
				has_government = emerging
			}
		}
	}
}