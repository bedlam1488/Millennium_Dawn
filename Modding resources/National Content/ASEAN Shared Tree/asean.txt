# asean_voting_category = {
# 	asean_voting_change_vote_of_nation = {
# 		icon = decision
# 		allowed = { always = no }
# 		target_trigger = {
# 			FROM = {
# 				has_idea = ASEAN_Member
# 				has_country_flag = voted_no_asean
# 			}
# 		}
# 		visible = {
# 			OR = {
# 				has_country_flag = set_proposer_nation_ASEAN_mutual_defense
# 			}
# 		}
# 		complete_effect = {
# 			log = "[GetDateText]: [Root.GetName]: Decision asean_voting_change_vote_of_nation target: [From.GetName]"
# 			##Bribes target nation
# 			FROM = {
# 				country_event = { id = asean_voting_events.1000 days = 1 }
# 			}
# 			subtract_from_variable = { treasury = 5 }
# 			custom_effect_tooltip = 5_billion_expense_tt
# 			effect_tooltip = {
# 				FROM = {
# 					add_to_variable = { treasury = 5 }
# 					custom_effect_tooltip = 5_billion_revenue_tt
# 					custom_effect_tooltip = asean_vote_changes_to_yes_tt
# 				}
# 			}
# 		}
# 	}
# 	asean_mutual_defense_proposal_mission = {
# 		icon = gfx_decision
# 		allowed = { always = no }
# 		days_mission_timeout = 30
# 		is_good = no
# 		activation = {
# 			has_idea = ASEAN_Member
# 			has_global_flag = ASEAN_active_voting
# 			has_global_flag = ASEAN_mutual_defense_proposing_flag
# 		}
# 		available = {
# 			NOT = {
# 				has_global_flag = ASEAN_active_voting
# 				has_global_flag = ASEAN_mutual_defense_proposing_flag
# 			}
# 		}
# 		visible = {
# 			has_idea = ASEAN_Member
# 			has_global_flag = ASEAN_active_voting
# 			has_global_flag = ASEAN_mutual_defense_proposing_flag
# 		}
# 		complete_effect = {
# 			if = {
# 				limit = {
# 					has_country_flag = set_proposer_nation_ASEAN_mutual_defense
# 				}
# 				if = {
# 					limit = {
# 						BRU = { has_idea = ASEAN_Member }
# 					}
# 					BRU = { country_event = { id = asean_voting_events.2 } }
# 				}
# 				else_if = {
# 					limit = {
# 						NOT = { BRU = { has_idea = ASEAN_Member } }
# 					}
# 					random_country = {
# 						limit = {
# 							OR = {
# 								check_variable = {
# 									var = yes_votes
# 									value > 0
# 								}
# 								check_variable = {
# 									var = no_votes
# 									value > 0
# 								}
# 							}
# 						}
# 						country_event = { id = asean_voting_events.2 }
# 					}
# 				}
# 			}
# 			else_if = {
# 				limit = {
# 					NOT = {
# 						has_country_flag = set_proposer_nation_ASEAN_mutual_defense
# 					}
# 				}
# 				effect_tooltip = {
# 					if = {
# 						limit = {
# 							BRU = { has_idea = ASEAN_Member }
# 						}
# 						BRU = { country_event = { id = asean_voting_events.2 } }
# 					}
# 					else_if = {
# 						limit = {
# 							NOT = { BRU = { has_idea = ASEAN_Member } }
# 						}
# 						random_country = {
# 							limit = {
# 								OR = {
# 									check_variable = {
# 										var = yes_votes
# 										value > 0
# 									}
# 									check_variable = {
# 										var = no_votes
# 										value > 0
# 									}
# 								}
# 							}
# 							country_event = { id = asean_voting_events.2 }
# 						}
# 					}
# 				}
# 			}
# 		}
# 		timeout_effect = {
# 			if = {
# 				limit = {
# 					has_country_flag = set_proposer_nation_ASEAN_mutual_defense
# 				}
# 				if = {
# 					limit = {
# 						BRU = { has_idea = ASEAN_Member }
# 					}
# 					BRU = { country_event = { id = asean_voting_events.2 } }
# 				}
# 				else_if = {
# 					limit = {
# 						NOT = { BRU = { has_idea = ASEAN_Member } }
# 					}
# 					random_country = {
# 						limit = {
# 							OR = {
# 								check_variable = {
# 									var = yes_votes
# 									value > 0
# 								}
# 								check_variable = {
# 									var = no_votes
# 									value > 0
# 								}
# 							}
# 						}
# 						country_event = { id = asean_voting_events.2 }
# 					}
# 				}
# 			}
# 			else_if = {
# 				limit = {
# 					NOT = {
# 						has_country_flag = set_proposer_nation_ASEAN_mutual_defense
# 					}
# 				}
# 				effect_tooltip = {
# 					if = {
# 						limit = {
# 							BRU = { has_idea = ASEAN_Member }
# 						}
# 						BRU = { country_event = { id = asean_voting_events.2 } }
# 					}
# 					else_if = {
# 						limit = {
# 							NOT = { BRU = { has_idea = ASEAN_Member } }
# 						}
# 						random_country = {
# 							limit = {
# 								OR = {
# 									check_variable = {
# 										var = yes_votes
# 										value > 0
# 									}
# 									check_variable = {
# 										var = no_votes
# 										value > 0
# 									}
# 								}
# 							}
# 							country_event = { id = asean_voting_events.2 }
# 						}
# 					}
# 				}
# 			}
# 		}
# 		fire_only_once = yes
# 	}
# }