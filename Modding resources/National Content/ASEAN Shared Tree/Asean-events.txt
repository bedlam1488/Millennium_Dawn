add_namespace = asean
add_namespace = asean_news
add_namespace = asean_voting_events

#ASEAN Voting Events
country_event = {
	id = asean_voting_events.0
	is_triggered_only = yes
	hidden = yes
	trigger = {
		has_idea = ASEAN_Member
		has_global_flag = ASEAN_active_voting
	}
	immediate = {
		log = "[GetDateText]: [Root.GetName]: event asean_voting_events.0"
		##Smallest Nation in the ASEAN least likely to leave
		if = {
			limit = {
				BRU = { has_idea = ASEAN_Member }
			}
			BRU = {
				clear_variable = yes_votes
				clear_variable = no_votes
				clear_variable = total_vote_percentage
				set_variable = { var = yes_votes value = 0 }
				set_variable = { var = no_votes value = 0 }
			}
		}
		else_if = {
			limit = {
				NOT = { BRU = { has_idea = ASEAN_Member } }
			}
			random_country = {
				limit = {
					NOT = { has_idea = ASEAN_Member }
					has_country_flag = voter_counter_nation
				}
				clr_country_flag = voter_counter_nation
			}
			random_country = {
				limit = {
					has_idea = ASEAN_Member
					OR = { ##Typically the voter nation just a fail safe code line
						check_variable = {
							var = yes_votes
							value > 0
						}
						check_variable = {
							var = no_votes
							value > 0
						}
					}
				}
				clear_variable = no_votes
				clear_variable = yes_votes
				clear_variable = total_vote_percentage
				if = {
					limit = {
						NOT = { has_country_flag = voter_counter_nation }
					}
					set_country_flag = voter_counter_nation
				}
				set_variable = {
					var = yes_votes
					value = 0
				}
				set_variable = {
					var = no_votes
					value = 0
				}
			}
		}
		every_country = {
			limit = {
				has_idea = ASEAN_Member
			}
			country_event = { id = asean_voting_events.1 days = 1 }
		}
	}
}

#Voting Event - One sized fits all. All AI will be determined via scripted triggers check ASEAN_voting_scripted_triggers.txt
country_event = {
	id = asean_voting_events.1
	title = asean_voting_events.1.t
	desc = asean_voting_events.1.d
	immediate = { log = "[GetDateText]: [Root.GetName]: event asean_voting_events.1" }
	is_triggered_only = yes
	trigger = {
		has_idea = ASEAN_Member
	}
	picture = GFX_election_day

	#Yes Vote
	option = {
		name = asean_voting_events.1.o1
		log = "[GetDateText]: [Root.GetName]: event asean_voting_events.1.o1"
		if = {
			limit = {
				BRU = { has_idea = ASEAN_Member }
			}
			BRU = {
				add_to_variable = {
					var = yes_votes
					value = 1
				}
				hidden_effect = {
					set_temp_variable = { total_votes = yes_votes }
					add_to_temp_variable = { total_votes = no_votes }
					set_temp_variable = { yes_votes_temp = yes_votes }
					divide_temp_variable = {
						var = yes_votes_temp
						value = total_votes
					}
					set_variable = { total_vote_percentage = yes_votes_temp }
				}
			}
		}
		else_if = {
			limit = {
				NOT = { BRU = { has_idea = ASEAN_Member } }
			}
			random_country = {
				limit = {
					has_idea = ASEAN_Member
					has_country_flag = voter_counter_nation
				}
				add_to_variable = {
					var = yes_votes
					value = 1
				}
				hidden_effect = {
					set_temp_variable = { total_votes = yes_votes }
					add_to_temp_variable = { total_votes = no_votes }
					set_temp_variable = { yes_votes_temp = yes_votes }
					divide_temp_variable = {
						var = yes_votes_temp
						value = total_votes
					}
					set_variable = { total_vote_percentage = yes_votes_temp }
				}
			}
		}

		ai_chance = {
			factor = 1
		}
	}

	#No Vote
	option = {
		name = asean_voting_events.1.o2
		log = "[GetDateText]: [Root.GetName]: event asean_voting_events.1.o2"
		set_country_flag = voted_no_asean
		if = {
			limit = {
				BRU = { has_idea = ASEAN_Member }
			}
			BRU = {
				add_to_variable = {
					var = no_votes
					value = 1
				}
				hidden_effect = {
					set_temp_variable = { total_votes = yes_votes }
					add_to_temp_variable = { total_votes = no_votes }
					set_temp_variable = { yes_votes_temp = yes_votes }
					divide_temp_variable = {
						var = yes_votes_temp
						value = total_votes
					}
					set_variable = { total_vote_percentage = yes_votes_temp }
				}
			}
		}
		else_if = {
			limit = {
				NOT = { BRU = { has_idea = ASEAN_Member } }
			}
			random_country = {
				limit = {
					has_idea = ASEAN_Member
					has_country_flag = voter_counter_nation
				}
				add_to_variable = {
					var = no_votes
					value = 1
				}
				hidden_effect = {
					set_temp_variable = { total_votes = yes_votes }
					add_to_temp_variable = { total_votes = no_votes }
					set_temp_variable = { yes_votes_temp = yes_votes }
					divide_temp_variable = {
						var = yes_votes_temp
						value = total_votes
					}
					set_variable = { total_vote_percentage = yes_votes_temp }
				}
			}
		}

		ai_chance = {
			factor = 1
		}
	}
}

#Voting Counter
country_event = {
	id = asean_voting_events.2
	title = asean_voting_events.2.t
	desc = asean_voting_events.2.d
	picture = GFX_election_day
	immediate = { log = "[GetDateText]: [Root.GetName]: event asean_voting_events.2" }
	trigger = {
		has_idea = ASEAN_Member
	}
	is_triggered_only = yes

	option = {
		name = asean_voting_events.2.a
		log = "[GetDateText]: [Root.GetName]: event asean_voting_events.2 mutual defense voting"
		clr_global_flag = ASEAN_active_voting
		if = {
			limit = {
				BRU = { has_idea = ASEAN_Member }
			}
			hidden_effect = {
				BRU = {
					set_temp_variable = { total_votes = yes_votes }
					add_to_temp_variable = { total_votes = no_votes }
					set_temp_variable = { yes_votes_temp = yes_votes }
					divide_temp_variable = {
						var = yes_votes_temp
						value = total_votes
					}
					set_variable = { total_vote_percentage = yes_votes_temp }
				}
				if = {
					limit = {
						has_global_flag = ASEAN_mutual_defense_proposing_flag
					}
					if = {
						limit = {
							BRU = { check_variable = { total_vote_percentage > 0.50 } }
						}
						set_global_flag = mutual_defense_passed
					}
					else_if = {
						limit = {
							BRU = { check_variable = { total_vote_percentage < 0.50 } }
						}
						set_global_flag = mutual_defense_failed
					}
					random_country = {
						limit = {
							has_country_flag = set_proposer_nation_ASEAN_mutual_defense
							has_idea = ASEAN_Member
						}
						country_event = { id = asean_voting_events.4 days = 1 }
					}
				}
			}
		}
	}
}

#ASEAN Mutual Defense
country_event = {
	id = asean_voting_events.3
	title = asean_voting_events.3.t
	desc = asean_voting_events.3.d
	picture = GFX_election_day
	immediate = { log = "[GetDateText]: [Root.GetName]: event asean_voting_events.3" }
	trigger = {
		has_idea = ASEAN_Member
	}
	is_triggered_only = yes

	option = {
		name = asean_voting_events.3.o1
		log = "[GetDateText]: [Root.GetName]: event asean_voting_events.3.o2"
		set_country_flag = set_proposer_nation_ASEAN_mutual_defense
		random_country = {
			limit = {
				has_idea = ASEAN_Member
			}
			country_event = { id = asean_voting_events.0 days = 1 }
		}
	}
}

#Results for Mutual Defense
country_event = {
	id = asean_voting_events.4
	title = {
		trigger = {
			has_global_flag = mutual_defense_passed
		}
		text = asean_voting_events.4.t1
	}
	title = {
		trigger = {
			has_global_flag = mutual_defense_failed
		}
		text = asean_voting_events.4.t2
	}
	desc = {
		trigger = {
			has_global_flag = mutual_defense_passed
		}
		text = asean_voting_events.4.d1
	}
	desc = {
		trigger = {
			has_global_flag = mutual_defense_failed
		}
		text = asean_voting_events.4.d2
	}
	picture = GFX_election_day
	is_triggered_only = yes
	immediate = { log = "[GetDateText]: [Root.GetName]: event asean_voting_events.4" }

	option = {
		name = asean_voting_events.4.a
		trigger = {
			has_global_flag = mutual_defense_passed
		}
		every_country = {
			limit = {
				has_idea = ASEAN_Member
			}
			give_guarantee = ROOT
			ROOT = {
				give_guarantee = PREV
			}
			PREV = {
				every_country = {
					limit = {
						has_idea = ASEAN_Member
					}
					give_guarantee = PREV.PREV
				}
			}
		}
		clr_global_flag = ASEAN_active_voting
		clr_global_flag = ASEAN_mutual_defense_proposing_flag
	}

	option = {
		name = asean_voting_events.4.b
		trigger = {
			has_global_flag = mutual_defense_failed
		}
		add_political_power = -50
		clr_global_flag = ASEAN_active_voting
		clr_global_flag = ASEAN_mutual_defense_proposing_flag
	}
}

##Changing Target Country Votes
country_event = {
	id = asean_voting_events.1000
	title = asean_voting_events.1000.t
	desc = asean_voting_events.1000.d
	picture = x
	immediate = { log = "[GetDateText]: [Root.GetName]: event asean_voting_events.1000" }
	is_triggered_only = yes

	#Accept Bribe
	option = {
		name = asean_voting_events.1000.o1
		log = "[GetDateText]: [Root.GetName]: event asean_voting_events.1000.o1"

	}
}
