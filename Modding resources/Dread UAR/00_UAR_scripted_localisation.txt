defined_text = {
	name = UAR_LC_gov_title

	text = {
		trigger = {
			OR = {
				check_variable = { ROOT.UAR_tag_LC_GOV > 0 }
				check_variable = { ROOT.UAR_tag_LC_GOV = 0 }
			}
		}
		localization_key = UAR_gov_rel_title_TT
	}
	text = {
		trigger = { check_variable = { ROOT.UAR_tag_LC_GOV < 0 } }
		localization_key = UAR_gov_sec_title_TT
	}
}

defined_text = {
	name = UAR_LC_soc_title

	text = {
		trigger = {
			OR = {
				check_variable = { ROOT.UAR_tag_LC_SOC > 0 }
				check_variable = { ROOT.UAR_tag_LC_SOC = 0 }
			}
		}
		localization_key = UAR_soc_rel_title_TT
	}
	text = {
		trigger = { check_variable = { ROOT.UAR_tag_LC_SOC < 0 } }
		localization_key = UAR_soc_sec_title_TT
	}
}

defined_text = {
	name = UAR_BS_title

	text = {
		trigger = { check_variable = { ROOT.UAR_tag_BS < -0.2 } }
		localization_key = UAR_baathism_ASSAD_TT
	}
	text = {
		trigger = {
			check_variable = { ROOT.UAR_tag_BS > -0.2 }
			check_variable = { ROOT.UAR_tag_BS < 0.2 }
		}
		localization_key = UAR_baathism_NASSER_TT
	}
	text = {
		trigger = { check_variable = { ROOT.UAR_tag_BS > 0.2 } }
		localization_key = UAR_baathism_SADDAM_TT
	}
}

defined_text = {
	name = UAR_tourist_alcohol_laws_GUI
	text = {
		trigger = { check_variable = { ROOT.UAR_tag_tourist_alcohol = 2 } }
		localization_key = UAR_law_BANNED
	}
	text = {
		trigger = { check_variable = { ROOT.UAR_tag_tourist_alcohol = 1 } }
		localization_key = UAR_law_RESTRICTED
	}
	text = {
		trigger = { check_variable = { ROOT.UAR_tag_tourist_alcohol = 0 } }
		localization_key = UAR_law_LEGALISED
	}
}

defined_text = {
	name = UAR_citizen_alcohol_laws_GUI
	text = {
		trigger = { check_variable = { ROOT.UAR_tag_citizen_alcohol = 2 } }
		localization_key = UAR_law_BANNED
	}
	text = {
		trigger = { check_variable = { ROOT.UAR_tag_citizen_alcohol = 1 } }
		localization_key = UAR_law_RESTRICTED
	}
	text = {
		trigger = { check_variable = { ROOT.UAR_tag_citizen_alcohol = 0 } }
		localization_key = UAR_law_LEGALISED
	}
}

defined_text = {
	name = UAR_dress_code_laws_GUI
	text = {
		trigger = { check_variable = { ROOT.UAR_tag_dress_code = 2 } }
		localization_key = UAR_law_ENFORCED
	}
	text = {
		trigger = { check_variable = { ROOT.UAR_tag_dress_code = 1 } }
		localization_key = UAR_law_RELAXED
	}
	text = {
		trigger = { check_variable = { ROOT.UAR_tag_dress_code = 0 } }
		localization_key = UAR_law_NO_RESTRICTIONS
	}
}

defined_text = {
	name = UAR_veil_laws_GUI
	text = {
		trigger = { check_variable = { ROOT.UAR_tag_veils = 2 } }
		localization_key = UAR_law_ENFORCED
	}
	text = {
		trigger = { check_variable = { ROOT.UAR_tag_veils = 1 } }
		localization_key = UAR_law_PERMITTED
	}
	text = {
		trigger = { check_variable = { ROOT.UAR_tag_veils = 0 } }
		localization_key = UAR_law_BANNED
	}
}

defined_text = {
	name = UAR_headscarf_laws_GUI
	text = {
		trigger = { check_variable = { ROOT.UAR_tag_headscarves = 2 } }
		localization_key = UAR_law_ENFORCED
	}
	text = {
		trigger = { check_variable = { ROOT.UAR_tag_headscarves = 1 } }
		localization_key = UAR_law_PERMITTED
	}
	text = {
		trigger = { check_variable = { ROOT.UAR_tag_headscarves = 0 } }
		localization_key = UAR_law_BANNED
	}
}

defined_text = {
	name = UAR_same_sex_laws_GUI
	text = {
		trigger = { check_variable = { ROOT.UAR_tag_same_sex = 2 } }
		localization_key = UAR_law_BANNED
	}
	text = {
		trigger = { check_variable = { ROOT.UAR_tag_same_sex = 1 } }
		localization_key = UAR_law_DECRIMINALISED
	}
	text = {
		trigger = { check_variable = { ROOT.UAR_tag_same_sex = 0 } }
		localization_key = UAR_law_LEGALISED
	}
}

defined_text = {
	name = UAR_tourist_drug_laws_GUI
	text = {
		trigger = { check_variable = { ROOT.UAR_tag_tourist_drugs = 0 } }
		localization_key = UAR_law_BANNED
	}
	text = {
		trigger = { check_variable = { ROOT.UAR_tag_tourist_drugs = 1 } }
		localization_key = UAR_law_DEFACTO
	}
	text = {
		trigger = { check_variable = { ROOT.UAR_tag_tourist_drugs = 2 } }
		localization_key = UAR_law_DECRIMINALISED
	}
	text = {
		trigger = { check_variable = { ROOT.UAR_tag_tourist_drugs = 3 } }
		localization_key = UAR_law_SOFT_DRUGS
	}
	text = {
		trigger = { check_variable = { ROOT.UAR_tag_tourist_drugs = 4 } }
		localization_key = UAR_law_HARD_DRUGS
	}
}

defined_text = {
	name = UAR_citizen_drug_laws_GUI
	text = {
		trigger = { check_variable = { ROOT.UAR_tag_citizen_drugs = 0 } }
		localization_key = UAR_law_BANNED
	}
	text = {
		trigger = { check_variable = { ROOT.UAR_tag_citizen_drugs = 1 } }
		localization_key = UAR_law_DEFACTO
	}
	text = {
		trigger = { check_variable = { ROOT.UAR_tag_citizen_drugs = 2 } }
		localization_key = UAR_law_DECRIMINALISED
	}
	text = {
		trigger = { check_variable = { ROOT.UAR_tag_citizen_drugs = 3 } }
		localization_key = UAR_law_SOFT_DRUGS
	}
	text = {
		trigger = { check_variable = { ROOT.UAR_tag_citizen_drugs = 4 } }
		localization_key = UAR_law_HARD_DRUGS
	}
}

defined_text = {
	name = UAR_pan_arabism_title

	text = {
		trigger = { NOT = { has_country_flag = UAR_TESTING } }
		localization_key = UAR_pan_arab_TT
	}
	text = {
		trigger = { has_country_flag = UAR_TESTING }
		localization_key = ""
	}
}

defined_text = {
	name = UAR_theocracy_name
	text = {
		trigger = { has_idea = shia }
		localization_key = UAR_theocracy_name_SHIA
	}
	text = {
		trigger = { NOT = { has_idea = shia } }
		localization_key = UAR_theocracy_name_SUNNI
	}
}

defined_text = {
	name = UAR_iraqi_alignment

	text = {
		trigger = { NOT = { has_global_flag = success_of_operation_iraqi_freedom } }
		localization_key = ARAB_SADDAM_pre_war
	}
	text = {
		trigger = {
			has_global_flag = success_of_operation_iraqi_freedom
			NOT = { has_global_flag = saddam_hussein_captured }
		}
		localization_key = ARAB_SADDAM_post_war_no_capture
	}
	text = {
		trigger = {
			has_global_flag = success_of_operation_iraqi_freedom
			has_global_flag = saddam_hussein_captured
		}
		localization_key = ARAB_SADDAM_post_war_saddam_captured
	}
}


# LAWS

# defined_text = {
# 	name = UAR_tourist_alcohol
# 	text = {
# 		trigger = { THIS = { has_country_flag = limited_alcohol } }
# 		localization_key = UAR_law_BANNED
# 	}
# 	text = {
# 		trigger = { THIS = { NOT = { has_country_flag = limited_alcohol } } }
# 		localization_key = UAR_law_LEGALISED
# 	}
# }