scripted_gui = {

	UAR_menu_open_button = {
		# context_type = player_context
		parent_window_token = politics_tab
		window_name = "UAR_menu_open_button"

		visible = { is_potential_UAR_candidate = yes }
		ai_enabled = { always = no }

		effects = {
			UAR_menu_open_button_click = {
				if = {
					limit = { NOT = { has_country_flag = UAR_main_menu_opened } }
					set_country_flag = UAR_main_menu_opened
				}
				else = {
					clr_country_flag = UAR_main_menu_opened
				}
			}
		}
	}

	UAR_menu_MAIN = {
		# context_type = player_context
		window_name = "UAR_menu_MAIN"

		properties = {
			#change flag textures to actual flag texture
			flag = { image = "[THIS.GetFlag]" }
		}

		visible = {
			has_country_flag = UAR_main_menu_opened
		}

		effects = {
			flag_click = {
				set_variable = { ROOT.UAR_tag_selected = THIS.id }
				set_variable = { ROOT.UAR_tag_LC_GOV = THIS.social_conservatism_government }
				set_variable = { ROOT.UAR_tag_BS = THIS.baathist_outlook }
				set_variable = { ROOT.UAR_tag_tourist_alcohol = THIS.ARAB_tourist_alcohol }
				set_variable = { ROOT.UAR_tag_citizen_alcohol = THIS.ARAB_citizen_alcohol }
				set_variable = { ROOT.UAR_tag_dress_code = THIS.ARAB_dress_code }
				set_variable = { ROOT.UAR_tag_veils = THIS.ARAB_veils }
				set_variable = { ROOT.UAR_tag_headscarves = THIS.ARAB_headscarves }
				set_variable = { ROOT.UAR_tag_same_sex = THIS.ARAB_same_sex_relations }
				set_variable = { ROOT.UAR_tag_tourist_drugs = THIS.ARAB_tourist_drugs }
				set_variable = { ROOT.UAR_tag_citizen_drugs = THIS.ARAB_citizen_drugs }
				UAR_gov_LC_display_math = yes
				UAR_soc_LC_display_math = yes
				UAR_baathist_display_math = yes
			}
			UAR_menu_close_button_click = {
				clr_country_flag = UAR_main_menu_opened
				clr_country_flag = UAR_unity_menu_opened
				clr_country_flag = UAR_alignment_menu_opened
				clr_country_flag = UAR_secularism_menu_opened
			}
			UAR_menu_unity_on_button_click = {
				set_country_flag = UAR_unity_menu_opened
				clr_country_flag = UAR_main_menu_opened
				clr_country_flag = UAR_alignment_menu_opened
				clr_country_flag = UAR_secularism_menu_opened
			}
			UAR_menu_alignment_on_button_click = {
				set_country_flag = UAR_alignment_menu_opened
				clr_country_flag = UAR_main_menu_opened
				clr_country_flag = UAR_unity_menu_opened
				clr_country_flag = UAR_secularism_menu_opened
			}
			UAR_menu_secularism_on_button_click = {
				set_country_flag = UAR_secularism_menu_opened
				clr_country_flag = UAR_main_menu_opened
				clr_country_flag = UAR_unity_menu_opened
				clr_country_flag = UAR_alignment_menu_opened
			}
			UAR_menu_baathist_split_click = {
				UAR_baathism_split_recall = yes
			}
			UAR_menu_secularism_bop_click = {
				UAR_LC_split_recall = yes
			}
		}

		triggers = {
			UAR_major_overlay_visible = {
				is_major = yes
			}
		}

		dynamic_lists = {
			UAR_dynamic_tag_list = {
				change_scope = yes # NEEDED TO GET THE TAG RIGHT
				array = global.UAR_existing_tag
				entry_container = "UAR_country_entry"
			}
		}
	}

	#UAR_status_map = {
	#	context_type = player_context
	#	window_name = "UAR_menu_map"

	#	properties = {}

	#	visible = {
	#		has_country_flag = UAR_main_menu_opened
	#	}

	#	effects = {}
	#}

	UAR_menu_unity = {
		context_type = player_context
		window_name = "UAR_menu_unity"

		properties = {
		}

		visible = {
			has_country_flag = UAR_unity_menu_opened
		}

		effects = {
			UAR_menu_close_button_click = {
				clr_country_flag = UAR_main_menu_opened
				clr_country_flag = UAR_unity_menu_opened
				clr_country_flag = UAR_alignment_menu_opened
				clr_country_flag = UAR_secularism_menu_opened
			}
			UAR_menu_status_on_button_click ={
				set_country_flag = UAR_main_menu_opened
				clr_country_flag = UAR_unity_menu_opened
				clr_country_flag = UAR_alignment_menu_opened
				clr_country_flag = UAR_secularism_menu_opened
			}
			UAR_menu_alignment_on_button_click = {
				set_country_flag = UAR_alignment_menu_opened
				clr_country_flag = UAR_unity_menu_opened
				clr_country_flag = UAR_secularism_menu_opened
			}
			UAR_menu_secularism_on_button_click = {
				set_country_flag = UAR_secularism_menu_opened
				clr_country_flag = UAR_unity_menu_opened
				clr_country_flag = UAR_alignment_menu_opened
			}
		}
	}

	UAR_menu_alignment = {
		context_type = player_context
		window_name = "UAR_menu_alignment"

		properties = {
		}

		visible = {
			has_country_flag = UAR_alignment_menu_opened
		}

		effects = {
			UAR_menu_close_button_click = {
				clr_country_flag = UAR_main_menu_opened
				clr_country_flag = UAR_unity_menu_opened
				clr_country_flag = UAR_alignment_menu_opened
				clr_country_flag = UAR_secularism_menu_opened
			}
			UAR_menu_status_on_button_click ={
				set_country_flag = UAR_main_menu_opened
				clr_country_flag = UAR_unity_menu_opened
				clr_country_flag = UAR_alignment_menu_opened
				clr_country_flag = UAR_secularism_menu_opened
			}
			UAR_menu_unity_on_button_click = {
				set_country_flag = UAR_unity_menu_opened
				clr_country_flag = UAR_alignment_menu_opened
				clr_country_flag = UAR_secularism_menu_opened
			}
			UAR_menu_secularism_on_button_click = {
				set_country_flag = UAR_secularism_menu_opened
				clr_country_flag = UAR_unity_menu_opened
				clr_country_flag = UAR_alignment_menu_opened
			}
		}
	}

	UAR_menu_secularism = {
		context_type = player_context
		window_name = "UAR_menu_secularism"

		properties = {
		}

		visible = {
			has_country_flag = UAR_secularism_menu_opened
		}

		effects = {
			UAR_menu_close_button_click = {
				clr_country_flag = UAR_main_menu_opened
				clr_country_flag = UAR_unity_menu_opened
				clr_country_flag = UAR_alignment_menu_opened
				clr_country_flag = UAR_secularism_menu_opened
			}
			UAR_menu_status_on_button_click ={
				set_country_flag = UAR_main_menu_opened
				clr_country_flag = UAR_unity_menu_opened
				clr_country_flag = UAR_alignment_menu_opened
				clr_country_flag = UAR_secularism_menu_opened
			}
			UAR_menu_unity_on_button_click = {
				set_country_flag = UAR_unity_menu_opened
				clr_country_flag = UAR_alignment_menu_opened
				clr_country_flag = UAR_secularism_menu_opened
			}
			UAR_menu_alignment_on_button_click = {
				set_country_flag = UAR_alignment_menu_opened
				clr_country_flag = UAR_unity_menu_opened
				clr_country_flag = UAR_secularism_menu_opened
			}
		}
	}


}