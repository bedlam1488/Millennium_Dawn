UAR_LC_split_recall = {
	meta_effect = { # LIBERALISM/CONSERVATISM
		text = {
			[ROOTTAG] = {
				remove_power_balance = { id = ARAB_assadist_saddamist }
				set_power_balance = {
					id = ARAB_liberalism_conservatism
					left_side = ARAB_liberalism_side
					right_side = ARAB_conservative_side
					set_value = [ROOTLCVALUE]
				}
			}
		}
		ROOTTAG = "[ROOT.GetTag]"
		ROOTLCVALUE = "[?ROOT.social_conservatism_government]"
		debug = yes
	}
}

UAR_baathism_split_recall = {
	meta_effect = { # ASSADISM/SADDAMISM
		text = {
			[ROOTTAG] = {
				remove_power_balance = { id = ARAB_liberalism_conservatism }
				set_power_balance = {
					id = ARAB_assadist_saddamist
					left_side = ARAB_assadism_side
					right_side = ARAB_saddamism_side
					set_value = [ROOTBAATHVALUE]
				}
			}
		}
		ROOTTAG = "[ROOT.GetTag]"
		ROOTBAATHVALUE = "[?ROOT.baathist_outlook]"
		debug = yes
	}
}

modify_baathism = {
	add_to_variable = { baathist_outlook = baathism_change }
	clamp_variable = {
		var = baathist_outlook
		min = -1
		max = 1
	}
}

UAR_gov_LC_display_math = {
	set_variable = { ROOT.GOV_CON_display = ROOT.UAR_tag_LC_GOV }
	if = {
		limit = { check_variable = { ROOT.GOV_CON_display > -0.001 } }
		multiply_variable = { ROOT.GOV_CON_display = 1 }
	}
	else = { multiply_variable = { ROOT.GOV_CON_display = -1 } }
}
UAR_soc_LC_display_math = {
	set_variable = { ROOT.SOC_CON_display = ROOT.UAR_tag_LC_SOC }
	if = {
		limit = { check_variable = { ROOT.SOC_CON_display > -0.001 } }
		multiply_variable = { ROOT.SOC_CON_display = 1 }
	}
	else = {
		multiply_variable = { ROOT.SOC_CON_display = -1 }
	}
}
UAR_baathist_display_math = {
	set_variable = { ROOT.baathist_display = ROOT.UAR_tag_BS }
	if = {
		limit = { check_variable = { ROOT.baathist_display > -0.001 } }
		multiply_variable = { ROOT.baathist_display = 1 }
	}
	else = { multiply_variable = { ROOT.baathist_display = -1 } }
}

# TOURIST DRINKING
	UAR_let_tourists_drink_banned = {
		set_variable = { ARAB_tourist_alcohol = 2 }
		custom_effect_tooltip = UAR_tourist_alcohol_laws_2_TT
	}
	UAR_let_tourists_drink_restricted = {
		set_variable = { ARAB_tourist_alcohol = 1 }
		custom_effect_tooltip = UAR_tourist_alcohol_laws_1_TT
	}
	UAR_let_tourists_drink_free = {
		set_variable = { ARAB_tourist_alcohol = 0 }
		custom_effect_tooltip = UAR_tourist_alcohol_laws_0_TT
	}
# CITIZEN DRINKING
	UAR_let_citizens_drink_banned = {
		set_variable = { ARAB_citizen_alcohol = 2 }
		custom_effect_tooltip = UAR_citizen_alcohol_laws_2_TT
	}
	UAR_let_citizens_drink_restricted = {
		set_variable = { ARAB_citizen_alcohol = 1 }
		custom_effect_tooltip = UAR_citizen_alcohol_laws_1_TT
	}
	UAR_let_citizens_drink_free = {
		set_variable = { ARAB_citizen_alcohol = 0 }
		custom_effect_tooltip = UAR_citizen_alcohol_laws_0_TT
	}
# DRESS CODE
	UAR_dress_code_enforced = {
		set_variable = { ARAB_dress_code = 2 }
		custom_effect_tooltip = UAR_dress_code_laws_2_TT
	}
	UAR_dress_code_relaxed = {
		set_variable = { ARAB_dress_code = 1 }
		custom_effect_tooltip = UAR_dress_code_laws_1_TT
	}
	UAR_dress_code_removed = {
		set_variable = { ARAB_dress_code = 0 }
		custom_effect_tooltip = UAR_dress_code_laws_0_TT
	}
# VEIL LAWS
	UAR_veils_enforced = {
		set_variable = { ARAB_veils = 2 }
		custom_effect_tooltip = UAR_veil_laws_2_TT
	}
	UAR_veils_permitted = {
		set_variable = { ARAB_veils = 1 }
		custom_effect_tooltip = UAR_veil_laws_1_TT
	}
	UAR_veils_banned = {
		set_variable = { ARAB_veils = 0 }
		custom_effect_tooltip = UAR_veil_laws_0_TT
	}
# HEADSCARF LAWS
	UAR_headscarves_enforced = {
		set_variable = { ARAB_headscarves = 2 }
		custom_effect_tooltip = UAR_headscarf_laws_2_TT
	}
	UAR_headscarves_permitted = {
		set_variable = { ARAB_headscarves = 1 }
		custom_effect_tooltip = UAR_headscarf_laws_1_TT
	}
	UAR_headscarves_banned = {
		set_variable = { ARAB_headscarves = 0 }
		custom_effect_tooltip = UAR_headscarf_laws_0_TT
	}
# SAME SEX RELATIONS LAWS
	UAR_same_sex_ban = {
		set_variable = { ARAB_same_sex_relations = 2 }
		custom_effect_tooltip = UAR_same_sex_laws_2_TT
	}
	UAR_same_sex_decriminalise = {
		set_variable = { ARAB_same_sex_relations = 1 }
		custom_effect_tooltip = UAR_same_sex_laws_1_TT
	}
	UAR_same_sex_legalise = {
		set_variable = { ARAB_same_sex_relations = 0 }
		custom_effect_tooltip = UAR_same_sex_laws_0_TT
	}
# TOURIST DRUGS
	UAR_let_tourists_do_drugs_enforced_ban = {
		set_variable = { ARAB_tourist_drugs = 0 }
		custom_effect_tooltip = UAR_tourist_drug_laws_0_TT
	}
	UAR_let_tourists_do_drugs_unenforced_ban = {
		set_variable = { ARAB_tourist_drugs = 1 }
		custom_effect_tooltip = UAR_tourist_drug_laws_1_TT
	}
	UAR_let_tourists_do_drugs_decriminalise = {
		set_variable = { ARAB_tourist_drugs = 2 }
		custom_effect_tooltip = UAR_tourist_drug_laws_2_TT
	}
	UAR_let_tourists_do_drugs_soft_drugs_legal = {
		set_variable = { ARAB_tourist_drugs = 3 }
		custom_effect_tooltip = UAR_tourist_drug_laws_3_TT
	}
	UAR_let_tourists_do_drugs_hard_drugs_legal = {
		set_variable = { ARAB_tourist_drugs = 4 }
		custom_effect_tooltip = UAR_tourist_drug_laws_4_TT
	}
# CITIZEN DRUGS
	UAR_let_citizens_do_drugs_enforced_ban = {
		set_variable = { ARAB_citizen_drugs = 0 }
		custom_effect_tooltip = UAR_citizen_drug_laws_0_TT
	}
	UAR_let_citizens_do_drugs_unenforced_ban = {
		set_variable = { ARAB_citizen_drugs = 1 }
		custom_effect_tooltip = UAR_citizen_drug_laws_1_TT
	}
	UAR_let_citizens_do_drugs_decriminalise = {
		set_variable = { ARAB_citizen_drugs = 2 }
		custom_effect_tooltip = UAR_citizen_drug_laws_2_TT
	}
	UAR_let_citizens_do_drugs_soft_drugs_legal = {
		set_variable = { ARAB_citizen_drugs = 3 }
		custom_effect_tooltip = UAR_citizen_drug_laws_3_TT
	}
	UAR_let_citizens_do_drugs_hard_drugs_legal = {
		set_variable = { ARAB_citizen_drugs = 4 }
		custom_effect_tooltip = UAR_citizen_drug_laws_4_TT
	}