puppet_rework_category = {
	icon = GFX_decision_category_generic_democracy
	priority = 100

	visible = {
		num_subjects > 0
	}

	visible_when_empty = yes
}