scripted_gui = {

	open_subject_interaction_menu = {
		context_type = player_context
		parent_window_name = manage_subjects_window
		window_name = "open_subject_interaction_menu_GUI"

		effects = {
			open_subject_interaction_menu_button_click = {
				set_country_flag = subject_interaction_menu_opened
				set_country_flag = subject_interactions_main_menu
			}
		}
	}

	subject_interaction_menu_MAIN = {
		context_type = player_context
		window_name = "subject_interaction_menu_MAIN_window"

		visible = {
			has_country_flag = subject_interaction_menu_opened
			has_country_flag = subject_interactions_main_menu
		}

		effects = {
			subject_interaction_menu_close_button_click = {
				clr_country_flag = subject_interaction_menu_opened
				clr_country_flag = subject_interactions_main_menu
			}
			puppet_refresh_click = {
				clr_country_flag = subject_interaction_1
				clr_country_flag = subject_interaction_2
				clr_country_flag = interacting_with_subjects
			}
			AA_slot_click = {
				set_country_flag = interacting_with_subjects
				clr_country_flag = subject_interaction_2
				set_country_flag = subject_interaction_1
			}
			ZZ_slot_click = {
				set_country_flag = interacting_with_subjects
				clr_country_flag = subject_interaction_1
				set_country_flag = subject_interaction_2
			}
		}

		properties = {
			AA_slot = { image = "[?ROOT.subjects^0.GetFlag]" }
			ZZ_slot = { image = "[?var:ROOT.fucking_bullshit.GetFlag]" }
		}
	}
}