eac_clear_vote = {
	if = {
		limit = {
			eac_has_vote = yes
		}
		clr_global_flag = eac_kiswahili_commission_vote
		clr_global_flag = eac_official_language_vote
		clr_global_flag = eac_religious_autonomy_vote
		clr_global_flag = eac_one_identity_vote
		clr_global_flag = eac_development_fund_vote
		clr_global_flag = eac_competition_authority_vote
		clr_global_flag = eac_development_bank_vote
		clr_global_flag = eac_international_investment_committee_vote
		clr_global_flag = eac_central_bank_vote
		clr_global_flag = eac_customs_union_vote
		clr_global_flag = eac_eu_eac_vote
		clr_global_flag = eac_growth_pact_vote
		clr_global_flag = eac_china_agreement_vote
		clr_global_flag = eac_tripartite_treaty_vote
		clr_global_flag = eac_common_market_vote
		clr_global_flag = eac_economic_convergence_vote
		clr_global_flag = eac_passport_vote
		clr_global_flag = eac_monetary_union_vote
		clr_global_flag = eac_cassoa_vote
		clr_global_flag = eac_science_technology_vote
		clr_global_flag = eac_fund_merge_bills_vote
		clr_global_flag = eac_lake_act_vote
		clr_global_flag = eac_health_research_vote
		clr_global_flag = eac_conservation_act_vote
		clr_global_flag = eac_tourism_agency_vote
		clr_global_flag = eac_central_managment_vote
		clr_global_flag = eac_higher_education_vote
		clr_global_flag = eac_education_agency_vote
		clr_global_flag = eac_immigration_agency_vote
		clr_global_flag = eac_legislative_assembly_vote
		clr_global_flag = eac_court_of_justice_vote
		clr_global_flag = eac_police_agency_vote
		clr_global_flag = eac_common_defence_vote
		clr_global_flag = eac_east_african_navy_vote
		clr_global_flag = eac_proto_parliament_vote
		clr_global_flag = eac_parliament_election_vote
		clr_global_flag = eac_constitution_vote
		clr_global_flag = eac_lake_states_vote
		clr_global_flag = eac_south_sudan_vote
		clr_global_flag = eac_indian_ocean_vote
		set_global_flag = eac_nothing_vote
	}
}






eac_decrease_corruption = {

	custom_effect_tooltip = EAC_increased_integration_tt


			if = {
				limit = {
					check_variable = { EAC_corruption > 0 }
					check_variable = { EAC_corruption < 2 }
				}
				random_list = {
					95 = {
						subtract_from_variable = { var = EAC_influence value = 1 }
					}
					5 = {
						subtract_from_variable = { var = EAC_corruption value = 1 }
					}
				}
			}
			if = {
				limit = {
					check_variable = { EAC_corruption > 1 }
					check_variable = { EAC_corruption < 3 }
				}
				random_list = {
					90 = {
						subtract_from_variable = { var = EAC_influence value = 1 }
					}
					10 = {
						subtract_from_variable = { var = EAC_corruption value = 1 }
					}
				}
			}
			if = {
				limit = {
					check_variable = { EAC_corruption > 2 }
					check_variable = { EAC_corruption < 4 }
				}
				random_list = {
					85 = {
						subtract_from_variable = { var = EAC_influence value = 1 }
					}
					15 = {
						subtract_from_variable = { var = EAC_corruption value = 1 }
					}
				}
			}
			if = {
				limit = {
					check_variable = { EAC_corruption > 3 }
					check_variable = { EAC_corruption < 5 }
				}
				random_list = {
					80 = {
						subtract_from_variable = { var = EAC_influence value = 1 }
					}
					20 = {
						subtract_from_variable = { var = EAC_corruption value = 1 }
					}
				}
			}
			if = {
				limit = {
					check_variable = { EAC_corruption > 4 }
					check_variable = { EAC_corruption < 6 }
				}
				random_list = {
					65 = {
						subtract_from_variable = { var = EAC_influence value = 1 }
					}
					35 = {
						subtract_from_variable = { var = EAC_corruption value = 1 }
					}
				}
			}
			if = {
				limit = {
					check_variable = { EAC_corruption > 5 }
					check_variable = { EAC_corruption < 7 }
				}
				random_list = {
					55 = {
						subtract_from_variable = { var = EAC_influence value = 1 }
					}
					45 = {
						subtract_from_variable = { var = EAC_corruption value = 1 }
					}
				}
			}
			if = {
				limit = {
					check_variable = { EAC_corruption > 6 }
					check_variable = { EAC_corruption < 8 }
				}
				random_list = {
					55 = {
						subtract_from_variable = { var = EAC_influence value = 1 }
					}
					45 = {
						subtract_from_variable = { var = EAC_corruption value = 1 }
					}
				}
			}
			if = {
				limit = {
					check_variable = { EAC_corruption > 7 }
					check_variable = { EAC_corruption < 9 }
				}
				random_list = {
					40 = {
						subtract_from_variable = { var = EAC_influence value = 1 }
					}
					60 = {
						subtract_from_variable = { var = EAC_corruption value = 1 }
					}
				}
			}
			if = {
				limit = {
					check_variable = { EAC_corruption > 8 }
					check_variable = { EAC_corruption < 10 }
				}
				random_list = {
					30 = {
						subtract_from_variable = { var = EAC_influence value = 1 }
					}
					70 = {
						subtract_from_variable = { var = EAC_corruption value = 1 }
					}
				}
			}
			if = {
				limit = {
					check_variable = { EAC_corruption > 9 }
					check_variable = { EAC_corruption < 11 }
				}
				random_list = {
					10 = {
						subtract_from_variable = { var = EAC_influence value = 1 }
					}
					90 = {
						subtract_from_variable = { var = EAC_corruption value = 1 }
					}
				}
			}

}

adopts_english = {

	custom_effect_tooltip = TNZ_adopting_english_tt

	hidden_effect = {
		ENG = {
			add_opinion_modifier = {
				target = ROOT
				modifier = english_speaking
			}
		}
		USA = {
			add_opinion_modifier = {
				target = ROOT
				modifier = english_speaking
			}
		}
		SCO = {
			add_opinion_modifier = {
				target = ROOT
				modifier = english_speaking
			}
		}
		IRE = {
			add_opinion_modifier = {
				target = ROOT
				modifier = english_speaking
			}
		}
		WAS = {
			add_opinion_modifier = {
				target = ROOT
				modifier = english_speaking
			}
		}
		CAN = {
			add_opinion_modifier = {
				target = ROOT
				modifier = english_speaking
			}
		}
		AST = {
			add_opinion_modifier = {
				target = ROOT
				modifier = english_speaking
			}
		}
		NZL = {
			add_opinion_modifier = {
				target = ROOT
				modifier = english_speaking
			}
		}
		BLZ = {
			add_opinion_modifier = {
				target = ROOT
				modifier = english_speaking
			}
		}
		BAH = {
			add_opinion_modifier = {
				target = ROOT
				modifier = english_speaking
			}
		}
		BAR = {
			add_opinion_modifier = {
				target = ROOT
				modifier = english_speaking
			}
		}
		TRI = {
			add_opinion_modifier = {
				target = ROOT
				modifier = english_speaking
			}
		}
		DMI = {
			add_opinion_modifier = {
				target = ROOT
				modifier = english_speaking
			}
		}
		STK = {
			add_opinion_modifier = {
				target = ROOT
				modifier = english_speaking
			}
		}
		JAM = {
			add_opinion_modifier = {
				target = ROOT
				modifier = english_speaking
			}
		}
		GUY = {
			add_opinion_modifier = {
				target = ROOT
				modifier = english_speaking
			}
		}
		GAH = {
			add_opinion_modifier = {
				target = ROOT
				modifier = english_speaking
			}
		}
		NIG = {
			add_opinion_modifier = {
				target = ROOT
				modifier = english_speaking
			}
		}
		ROOT = {
			add_opinion_modifier = {
				target = ENG
				modifier = english_speaking
			}
		}
		ROOT = {
			add_opinion_modifier = {
				target = USA
				modifier = english_speaking
			}
		}
		ROOT = {
			add_opinion_modifier = {
				target = SCO
				modifier = english_speaking
			}
		}
		ROOT = {
			add_opinion_modifier = {
				target = IRE
				modifier = english_speaking
			}
		}
		ROOT = {
			add_opinion_modifier = {
				target = WAS
				modifier = english_speaking
			}
		}
		ROOT = {
			add_opinion_modifier = {
				target = CAN
				modifier = english_speaking
			}
		}
		ROOT = {
			add_opinion_modifier = {
				target = AST
				modifier = english_speaking
			}
		}
		ROOT = {
			add_opinion_modifier = {
				target = NZL
				modifier = english_speaking
			}
		}
		ROOT = {
			add_opinion_modifier = {
				target = BLZ
				modifier = english_speaking
			}
		}
		ROOT = {
			add_opinion_modifier = {
				target = BAH
				modifier = english_speaking
			}
		}
		ROOT = {
			add_opinion_modifier = {
				target = BAR
				modifier = english_speaking
			}
		}
		ROOT = {
			add_opinion_modifier = {
				target = TRI
				modifier = english_speaking
			}
		}
		ROOT = {
			add_opinion_modifier = {
				target = DMI
				modifier = english_speaking
			}
		}
		ROOT = {
			add_opinion_modifier = {
				target = STK
				modifier = english_speaking
			}
		}
		ROOT = {
			add_opinion_modifier = {
				target = JAM
				modifier = english_speaking
			}
		}
		ROOT = {
			add_opinion_modifier = {
				target = GUY
				modifier = english_speaking
			}
		}
		ROOT = {
			add_opinion_modifier = {
				target = GAH
				modifier = english_speaking
			}
		}
		ROOT = {
			add_opinion_modifier = {
				target = NIG
				modifier = english_speaking
			}
		}
	}
}