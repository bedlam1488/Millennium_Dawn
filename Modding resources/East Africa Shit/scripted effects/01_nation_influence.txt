#Bluehunter

tnz_root_gain_5_influence = {

	hidden_effect = {
		ROOT = {
			set_temp_variable = { influence_gain = 5 }
			for_loop_effect = {
				end = influence_array^num
				value = v
				if = {
					limit = {
						check_variable = { influence_array^v = TNZ }
					}
					add_to_variable = { influence_array_val^v = influence_gain }
					set_country_flag = found
				}
			}
			if = {
				limit = { NOT = { has_country_flag = found } }
					add_to_array = { influence_array = TNZ.id }
					add_to_array = { influence_array_val = influence_gain }
			}
			clr_country_flag = found
			recalculate_influence = yes
		}
	}
}

fra_root_gain_5_influence = {

	hidden_effect = {
		ROOT = {
			set_temp_variable = { influence_gain = 5 }
			for_loop_effect = {
				end = influence_array^num
				value = v
				if = {
					limit = {
						check_variable = { influence_array^v = FRA }
					}
					add_to_variable = { influence_array_val^v = influence_gain }
					set_country_flag = found
				}
			}
			if = {
				limit = { NOT = { has_country_flag = found } }
					add_to_array = { influence_array = FRA.id }
					add_to_array = { influence_array_val = influence_gain }
			}
			clr_country_flag = found
			recalculate_influence = yes
		}
	}
}

ger_root_gain_5_influence = {

	hidden_effect = {
		ROOT = {
			set_temp_variable = { influence_gain = 5 }
			for_loop_effect = {
				end = influence_array^num
				value = v
				if = {
					limit = {
						check_variable = { influence_array^v = GER }
					}
					add_to_variable = { influence_array_val^v = influence_gain }
					set_country_flag = found
				}
			}
			if = {
				limit = { NOT = { has_country_flag = found } }
					add_to_array = { influence_array = GER.id }
					add_to_array = { influence_array_val = influence_gain }
			}
			clr_country_flag = found
			recalculate_influence = yes
		}
	}
}

eng_root_gain_5_influence = {

	hidden_effect = {
		ROOT = {
			set_temp_variable = { influence_gain = 5 }
			for_loop_effect = {
				end = influence_array^num
				value = v
				if = {
					limit = {
						check_variable = { influence_array^v = ENG }
					}
					add_to_variable = { influence_array_val^v = influence_gain }
					set_country_flag = found
				}
			}
			if = {
				limit = { NOT = { has_country_flag = found } }
					add_to_array = { influence_array = ENG.id }
					add_to_array = { influence_array_val = influence_gain }
			}
			clr_country_flag = found
			recalculate_influence = yes
		}
	}
}

chi_root_gain_5_influence = {

	hidden_effect = {
		ROOT = {
			set_temp_variable = { influence_gain = 5 }
			for_loop_effect = {
				end = influence_array^num
				value = v
				if = {
					limit = {
						check_variable = { influence_array^v = CHI }
					}
					add_to_variable = { influence_array_val^v = influence_gain }
					set_country_flag = found
				}
			}
			if = {
				limit = { NOT = { has_country_flag = found } }
					add_to_array = { influence_array = CHI.id }
					add_to_array = { influence_array_val = influence_gain }
			}
			clr_country_flag = found
			recalculate_influence = yes
		}
	}
}

raj_root_gain_5_influence = {

	hidden_effect = {
		ROOT = {
			set_temp_variable = { influence_gain = 5 }
			for_loop_effect = {
				end = influence_array^num
				value = v
				if = {
					limit = {
						check_variable = { influence_array^v = RAJ }
					}
					add_to_variable = { influence_array_val^v = influence_gain }
					set_country_flag = found
				}
			}
			if = {
				limit = { NOT = { has_country_flag = found } }
					add_to_array = { influence_array = RAJ.id }
					add_to_array = { influence_array_val = influence_gain }
			}
			clr_country_flag = found
			recalculate_influence = yes
		}
	}
}

jap_root_gain_5_influence = {

	hidden_effect = {
		ROOT = {
			set_temp_variable = { influence_gain = 5 }
			for_loop_effect = {
				end = influence_array^num
				value = v
				if = {
					limit = {
						check_variable = { influence_array^v = JAP }
					}
					add_to_variable = { influence_array_val^v = influence_gain }
					set_country_flag = found
				}
			}
			if = {
				limit = { NOT = { has_country_flag = found } }
					add_to_array = { influence_array = JAP.id }
					add_to_array = { influence_array_val = influence_gain }
			}
			clr_country_flag = found
			recalculate_influence = yes
		}
	}
}

tnz_root_gain_10_influence = {

	hidden_effect = {
		ROOT = {
			set_temp_variable = { influence_gain = 10 }
			for_loop_effect = {
				end = influence_array^num
				value = v
				if = {
					limit = {
						check_variable = { influence_array^v = TNZ }
					}
					add_to_variable = { influence_array_val^v = influence_gain }
					set_country_flag = found
				}
			}
			if = {
				limit = { NOT = { has_country_flag = found } }
					add_to_array = { influence_array = TNZ.id }
					add_to_array = { influence_array_val = influence_gain }
			}
			clr_country_flag = found
			recalculate_influence = yes
		}
	}
}

ast_root_gain_10_influence = {

	hidden_effect = {
		ROOT = {
			set_temp_variable = { influence_gain = 10 }
			for_loop_effect = {
				end = influence_array^num
				value = v
				if = {
					limit = {
						check_variable = { influence_array^v = AST }
					}
					add_to_variable = { influence_array_val^v = influence_gain }
					set_country_flag = found
				}
			}
			if = {
				limit = { NOT = { has_country_flag = found } }
					add_to_array = { influence_array = AST.id }
					add_to_array = { influence_array_val = influence_gain }
			}
			clr_country_flag = found
			recalculate_influence = yes
		}
	}
}

eng_root_gain_10_influence = {

	hidden_effect = {
		ROOT = {
			set_temp_variable = { influence_gain = 10 }
			for_loop_effect = {
				end = influence_array^num
				value = v
				if = {
					limit = {
						check_variable = { influence_array^v = ENG }
					}
					add_to_variable = { influence_array_val^v = influence_gain }
					set_country_flag = found
				}
			}
			if = {
				limit = { NOT = { has_country_flag = found } }
					add_to_array = { influence_array = ENG.id }
					add_to_array = { influence_array_val = influence_gain }
			}
			clr_country_flag = found
			recalculate_influence = yes
		}
	}
}

usa_root_gain_10_influence = {

	hidden_effect = {
		ROOT = {
			set_temp_variable = { influence_gain = 10 }
			for_loop_effect = {
				end = influence_array^num
				value = v
				if = {
					limit = {
						check_variable = { influence_array^v = USA }
					}
					add_to_variable = { influence_array_val^v = influence_gain }
					set_country_flag = found
				}
			}
			if = {
				limit = { NOT = { has_country_flag = found } }
					add_to_array = { influence_array = USA.id }
					add_to_array = { influence_array_val = influence_gain }
			}
			clr_country_flag = found
			recalculate_influence = yes
		}
	}
}

chi_root_gain_10_influence = {

	hidden_effect = {
		ROOT = {
			set_temp_variable = { influence_gain = 10 }
			for_loop_effect = {
				end = influence_array^num
				value = v
				if = {
					limit = {
						check_variable = { influence_array^v = CHI }
					}
					add_to_variable = { influence_array_val^v = influence_gain }
					set_country_flag = found
				}
			}
			if = {
				limit = { NOT = { has_country_flag = found } }
					add_to_array = { influence_array = CHI.id }
					add_to_array = { influence_array_val = influence_gain }
			}
			clr_country_flag = found
			recalculate_influence = yes
		}
	}
}

raj_root_gain_10_influence = {

	hidden_effect = {
		ROOT = {
			set_temp_variable = { influence_gain = 10 }
			for_loop_effect = {
				end = influence_array^num
				value = v
				if = {
					limit = {
						check_variable = { influence_array^v = RAJ }
					}
					add_to_variable = { influence_array_val^v = influence_gain }
					set_country_flag = found
				}
			}
			if = {
				limit = { NOT = { has_country_flag = found } }
					add_to_array = { influence_array = RAJ.id }
					add_to_array = { influence_array_val = influence_gain }
			}
			clr_country_flag = found
			recalculate_influence = yes
		}
	}
}

tnz_root_gain_20_influence = {

	hidden_effect = {
		ROOT = {
			set_temp_variable = { influence_gain = 20 }
			for_loop_effect = {
				end = influence_array^num
				value = v
				if = {
					limit = {
						check_variable = { influence_array^v = TNZ }
					}
					add_to_variable = { influence_array_val^v = influence_gain }
					set_country_flag = found
				}
			}
			if = {
				limit = { NOT = { has_country_flag = found } }
					add_to_array = { influence_array = TNZ.id }
					add_to_array = { influence_array_val = influence_gain }
			}
			clr_country_flag = found
			recalculate_influence = yes
		}
	}
}

chi_root_gain_20_influence = {

	hidden_effect = {
		ROOT = {
			set_temp_variable = { influence_gain = 20 }
			for_loop_effect = {
				end = influence_array^num
				value = v
				if = {
					limit = {
						check_variable = { influence_array^v = CHI }
					}
					add_to_variable = { influence_array_val^v = influence_gain }
					set_country_flag = found
				}
			}
			if = {
				limit = { NOT = { has_country_flag = found } }
					add_to_array = { influence_array = CHI.id }
					add_to_array = { influence_array_val = influence_gain }
			}
			clr_country_flag = found
			recalculate_influence = yes
		}
	}
}

usa_root_gain_20_influence = {

	hidden_effect = {
		ROOT = {
			set_temp_variable = { influence_gain = 20 }
			for_loop_effect = {
				end = influence_array^num
				value = v
				if = {
					limit = {
						check_variable = { influence_array^v = USA }
					}
					add_to_variable = { influence_array_val^v = influence_gain }
					set_country_flag = found
				}
			}
			if = {
				limit = { NOT = { has_country_flag = found } }
					add_to_array = { influence_array = USA.id }
					add_to_array = { influence_array_val = influence_gain }
			}
			clr_country_flag = found
			recalculate_influence = yes
		}
	}
}