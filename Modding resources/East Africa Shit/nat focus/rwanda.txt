focus_tree = {

	id = rwanda_focus

	country = {
		factor = 0
		
		modifier = {
			add = 20
			tag = RWA
		}
	}

	shared_focus = EAC_east_african_community

	continuous_focus_position = { x = 500 y = 720 }

	focus = {
		id = RWA_great_african_war
		icon = african_conflict


		x = 4
		y = 0
		
		cost = 5

		bypass = {
			has_war = no
		}

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_great_african_war executed"
			add_war_support = 0.05
		}

		prerequisite = {

		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_show_no_weakness
		icon = conscription


		x = 1
		y = 1
		
		cost = 10

		available = {
			has_war_with = DRC
		}

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_show_no_weakness executed"
			add_political_power = 100
			army_experience = 10
			add_war_support = 0.02
		}

		prerequisite = {
			focus = RWA_great_african_war
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_infiltrate_kinshasa
		icon = infiltration


		x = 0
		y = 2
		
		cost = 10

		available = {
			has_war_with = DRC
		}

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_infiltrate_kinshasa executed"
			country_event = rwa.2
		}

		prerequisite = {
			focus = RWA_show_no_weakness
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_decisive_breakthrough
		icon = army_logistics


		x = 2
		y = 2
		
		cost = 10

		available = {
			has_war_with = DRC
		}

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_decisive_breakthrough executed"
			add_timed_idea = { 
				idea = rwa_decisive_breakthrough  days = 120
			}
		}

		prerequisite = {
			focus = RWA_show_no_weakness
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_seek_international_support_to_our_cause
		icon = improve_relations_blue


		x = 2
		y = 4
		
		cost = 10


		available = {
			DRC = {
				has_war = no
			}
		  	OR = {
				RCD = {
					is_subject_of = RWA
				}
				has_full_control_of_state = 308
			}
		}	

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_seek_international_support_to_our_cause executed"
			custom_effect_tooltip = RWA_lift_sanctions_tt
			hidden_effect = {
				remove_ideas = Western_Sanctions 
				add_timed_idea = {
					idea = Western_Sanctions 
					days = 1200
				}
			}
		}

		prerequisite = {
			focus = RWA_affirmative_role_in_internal_politics	focus = RWA_balkanize_the_congo
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_balkanize_the_congo
		icon = balkan_conflict


		x = 1
		y = 3
		
		cost = 10

		available = {
			DRC = {
				is_subject_of = RWA
			}
		}

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_balkanize_the_congo executed"
			country_event = rwa.23
		}

		prerequisite = {
			focus = RWA_infiltrate_kinshasa	focus = RWA_decisive_breakthrough
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_integrate_the_kivu_region
		icon = war2_green


		x = 2
		y = 5
		
		cost = 15

		available = {
			has_full_control_of_state = 308
			has_stability > 0.5
		}

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_integrate_the_kivu_region executed"
			308 = {
				add_core_of = RWA
			}
		}

		prerequisite = {
			focus = RWA_seek_international_support_to_our_cause
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_greater_rwanda
		icon = political_pressure_dark_green


		x = 2
		y = 6
		
		cost = 10

		available = {
			is_puppet = no
			OR = {
				has_full_control_of_state = 308
				has_full_control_of_state = 251
			}
		}

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_greater_rwanda executed"
			
			add_stability = 0.10
		}

		prerequisite = {
			focus = RWA_integrate_the_kivu_region
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_mineral_exploitation
		icon = war2_green


		x = 4
		y = 1
		
		cost = 10

		available = {
			has_full_control_of_state = 308
			has_full_control_of_state = 250
		}
				

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_mineral_exploitation executed"
			add_resource = { 
				type = tungsten
				amount = 5
				state = 308
			}
			add_resource = { 
				type = tungsten
				amount = 2
				state = 250
			}		
		}

		prerequisite = {
			focus = RWA_great_african_war
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_peace_talks
		icon = peace


		x = 7
		y = 1
		
		cost = 10

		bypass = {
			RWA = {
				has_war = no
			}
		}

		available = {
			has_war_with = DRC
		}

		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_peace_talks executed"
			country_event = rwa.3
		}

		prerequisite = {
			focus = RWA_great_african_war
		}


		ai_will_do = {
			base = 1
			modifier = {
				factor = 8
				date > 2001.01.01
 			}
			modifier = {
				factor = 16
				date > 2002.01.01
 			}
		}
	}

	focus = {
		id = RWA_recognizing_our_new_neighbour
		icon = dangerous_deal2


		x = 6
		y = 2
		
		cost = 10

		available = {
			DRC = {
				has_war = no
			}
			OR = {
				country_exists = MLA
				country_exists = RCD
			}	
		}

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_recognizing_our_new_neighbour executed"
			MLC = {
			swap_ideas = { remove_idea = Non_State_Actor add_idea = Lacks_International_Recognition }
			}
			RCD = {
			swap_ideas = { remove_idea = Non_State_Actor add_idea = Lacks_International_Recognition }
			}
		}

		prerequisite = {
			focus = RWA_peace_talks
		}

		mutually_exclusive = {
			focus = RWA_continued_support_for_insurections
		}

		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_affirmative_role_in_internal_politics
		icon = gunpoint_diplomacy


		x = 4
		y = 3
		
		cost = 10

		available = {
			RCD = {
				is_subject_of = RWA
			}
		}

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_affirmative_role_in_internal_politics executed"
			hidden_effect = {
				subtract_from_variable = { domestic_influence_amount = 30 }
				recalculate_influence = yes
			}
			RCD = {
				custom_effect_tooltip = RWA_RCD_loose_influence_tt
				add_country_leader_trait = puppet
			}
		}

		mutually_exclusive = {
			focus = RWA_relinquish_our_grip
		}

		prerequisite = {
			focus = RWA_recognizing_our_new_neighbour
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_relinquish_our_grip
		icon = self_management


		x = 6
		y = 3
		
		cost = 10

		available = {
			RCD = {
				is_subject_of = RWA
			}
		}

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_relinquish_our_grip executed"
			end_puppet = RCD
		}

		prerequisite = {
			focus = RWA_recognizing_our_new_neighbour
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_mutual_defensive_pact
		icon = support_the_left_right


		x = 7
		y = 4
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_mutual_defensive_pact executed"
			custom_effect_tooltip = RWA_defence_pact_rcd_tt
			hidden_effect = {
			ROOT = {
				diplomatic_relation = {
					country = RCD
					relation = guarantee
					active = yes
				}
			}
			RCD = {
				diplomatic_relation = {
					country = ROOT
					relation = guarantee
					active = yes
				}
			}
			}
		}

		prerequisite = {
			focus = RWA_relinquish_our_grip
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_central_african_league
		icon = national_unity


		x = 5
		y = 4
		
		cost = 10


		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_central_african_league executed"
			create_faction = CENTRAL_AFRICAN_LEAGUE
			set_faction_name = CENTRAL_AFRICAN_LEAGUE
			if = {
				limit = {
					country_exists = RCD
				}
			add_to_faction = RCD
			}
		}

		prerequisite = {
			focus = RWA_relinquish_our_grip
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_continued_support_for_insurections
		icon = espionage2


		x = 8
		y = 2
		
		cost = 10

		available = {
			DRC = {
				OR = {
					has_full_control_of_state = 308
					has_full_control_of_state = 309
				}
			}
			has_war = no
		}

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_continued_support_for_insurections executed"
			DRC = {
				add_ideas = RWA_kivu_insurrections
				add_opinion_modifier = {
					target = RWA
					modifier = supported_opposition
				}
			}
		}

		mutually_exclusive = {
			focus = RWA_recognizing_our_new_neighbour
		}

		prerequisite = {
			focus = RWA_peace_talks
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_border_smugglers_paradise
		icon = infiltration


		x = 8
		y = 3
		
		cost = 10

		available = {
			DRC = {
				has_full_control_of_state = 308
				has_full_control_of_state = 308
				has_war = no
			}
		}

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_border_smugglers_paradise executed"
			DRC = {
			add_resource = { 
				type = tungsten
				amount = -1
				state = 308
			}
			add_resource = { 
				type = tungsten
				amount = -1
				state = 309
			}
			}
			add_resource = { 
				type = tungsten
				amount = 2
				state = 250
			}
		}

		prerequisite = {
			focus = RWA_continued_support_for_insurections
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_the_president_steps_down
		icon = anti_establishment2


		x = 13
		y = 0
		
		cost = 5

		available = {
			has_country_leader = { name = "Pasteur Bizimungu" ruling_only = yes }
		}

		bypass = {
			NOT = { has_country_leader = { name = "Pasteur Bizimungu" ruling_only = yes }
			}
		}
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_the_president_steps_down executed"
			custom_effect_tooltip = RWA_president_steps_down_tt
			hidden_effect = {
				meta_effect = {
					text = {
						set_country_flag = [META_SET_RULING_PARTY]
					}
					META_SET_RULING_PARTY = "[meta_set_ruling_leader]"
				}
				set_leader = yes
			}
		}

		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_rise_of_kagame
		icon = kagame


		x = 13
		y = 1
		
		cost = 10

		available = {
			has_country_leader = { name = "Paul Kagame" ruling_only = yes }
		}

		bypass = {
			date >  2000.06.01
			NOT = { has_country_leader = { name = "Paul Kagame" ruling_only = yes }
			}
		}

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_rise_of_kagame executed"
			country_event = rwa.22
		}

		prerequisite = {
			focus = RWA_the_president_steps_down
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_begin_internal_consolidation
		icon = national_unity_red


		x = 12
		y = 2
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_begin_internal_consolidation executed"
			add_ideas = RWA_patriotic_front
		}

		prerequisite = {
			focus = RWA_rise_of_kagame
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_new_constitution
		icon = dangerous_laws


		x = 10
		y = 3
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_new_constitution executed"
			add_political_power = 200
			add_country_leader_trait = dictator
		}

		prerequisite = {
			focus = RWA_begin_internal_consolidation
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_2003_election
		icon = Election


		x = 13
		y = 4
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_2003_election executed"
			swap_ideas = { remove_idea = RWA_patriotic_front add_idea = RWA_patriotic_front2 }
		}

		prerequisite = {
			focus = RWA_new_constitution	focus = RWA_court_genocide_backlogs	focus = RWA_new_flag_new_anthem
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_court_genocide_backlogs
		icon = deadly_deal


		x = 12
		y = 3
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_court_genocide_backlogs executed"
			add_stability = 0.04
		}

		prerequisite = {
			focus = RWA_begin_internal_consolidation
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_all_inclusive_cabinet
		icon = union_negotiations


		x = 14
		y = 2
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_all_inclusive_cabinet executed"
			add_stability = 0.05
		}

		prerequisite = {
			focus = RWA_rise_of_kagame
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_new_flag_new_anthem
		icon = forces_rwa


		x = 14
		y = 3
		
		cost = 10

		available = {
			has_political_power > 100
		}

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_new_flag_new_anthem executed"
			visually_display_opinion_rise_the_military = yes
			set_country_flag = current_the_military
			increase_internal_faction_opinion = yes
			add_political_power = -100
		}

		prerequisite = {
			focus = RWA_all_inclusive_cabinet
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_easing_sentences_and_persecutions
		icon = neutrality


		x = 13
		y = 5
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_easing_sentences_and_persecutions executed"
			add_stability = 0.05
		}

		prerequisite = {
			focus = RWA_2003_election
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_a_new_rwanda
		icon = national_unity


		x = 15
		y = 6
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_a_new_rwanda executed"
			swap_ideas = { remove_idea = hutu_tutsi_tensions add_idea = multi_ethnic_state_idea }
		}

		prerequisite = {
			focus = RWA_easing_sentences_and_persecutions	focus = RWA_joining_the_commonwealth
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_rwandan_diplomacy
		icon = diplomatic_treaty


		x = 17
		y = 0
		
		cost = 10

		available = {
			has_war = no
		}

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_rwandan_diplomacy executed"
		}

		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_open_embassies
		icon = improve_relations_blue


		x = 17
		y = 1
		
		cost = 10

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_open_embassies executed"
		}

		prerequisite = {
			focus = RWA_rwandan_diplomacy
		}

		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_breaking_relations_with_france
		icon = war_against_france


		x = 17
		y = 2
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_breaking_relations_with_france executed"
		}

		prerequisite = {
			focus = RWA_open_embassies
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_joining_the_eac
		icon = align_to_east_africa


		x = 17
		y = 3
		
		cost = 10

		available = {
			has_idea = idea_eac_member_state
		}

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_joining_the_eac executed"
			custom_effect_tooltip = EAC_community_influence_2_tt
			add_to_variable = { var = EAC_influence value = 2 }
		}

		prerequisite = {
			focus = RWA_breaking_relations_with_france
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_adopting_english
		icon = british_africa


		x = 17
		y = 4
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_adopting_english executed"
			custom_effect_tooltip = TNZ_adopting_english_tt
			hidden_effect = {
				adopts_english = yes
			}
		}

		prerequisite = {
			focus = RWA_joining_the_eac focus = RWA_reinvesting_in_education
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_joining_the_commonwealth
		icon = align_to_britain2


		x = 17
		y = 5
		
		cost = 10


		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_joining_the_commonwealth executed"
			ENG = {
				country_event = { days = 1 id = rwa.21 }
			}
		}

		prerequisite = {
			focus = RWA_adopting_english
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_war_economy
		icon = economic_crisis

		x = 22
		y = 0
		
		cost = 2


		bypass = { 
		has_war = no
		}

		available = {
			has_full_control_of_state = 250
		 }
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_war_economy executed"
			custom_effect_tooltip = RWA_war_economy_tt
			hidden_effect = {
				increase_military_spending = yes
				250 = {
					add_building_construction = {
						type = arms_factory
						level = 1
						instant_build = yes
					}
					remove_building = {
						type = industrial_complex
						level = 1
					}
				}
			}
		}

		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_reconverting_to_civilian_economy
		icon = consumer_goods


		x = 22
		y = 1

		cost = 10

		bypass = {
			date > 2004.01.01
			250 = {
				arms_factory < 1
			}
		}

		available = {
			has_war = no
			has_full_control_of_state = 250
		}

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_reconverting_to_civilian_economy executed"
			custom_effect_tooltip = RWA_reconverting_to_civilian_economy_tt
			hidden_effect = {
				decrease_military_spending = yes
				250 = {
					add_building_construction = {
						type = industrial_complex
						level = 1
						instant_build = yes
					}
					remove_building = {
						type = arms_factory
						level = 1
					}
				}
			}
		}

		prerequisite = {
			focus = RWA_war_economy
		}

		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_cash_crop_economy
		icon = agriculture


		x = 22
		y = 2

		cost = 10

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_cash_crop_economy executed"
			visually_display_opinion_rise_farmers = yes
			set_country_flag = current_farmers
			increase_internal_faction_opinion = yes
		}

		prerequisite = {
			focus = RWA_reconverting_to_civilian_economy
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_reinvesting_in_education
		icon = research


		x = 20
		y = 3
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_reinvesting_in_education executed"
			increase_education_budget = yes
			add_research_slot = 1
		}

		prerequisite = {
			focus = RWA_cash_crop_economy
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_welfare_and_recovery_programs
		icon = generic_healthcare


		x = 19
		y = 4
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_welfare_and_recovery_programs executed"
			increase_social_spending = yes
			remove_ideas = great_lakes_aftermath
		}

		prerequisite = {
			focus = RWA_reinvesting_in_education
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_national_parks
		icon = mountain_gorilla


		x = 19
		y = 5
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_national_parks executed"
			add_stability = 0.02
			add_political_power = 50
		}

		prerequisite = {
			focus = RWA_welfare_and_recovery_programs
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_booming_tourism_sector
		icon = u


		x = 19
		y = 6
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_booming_tourism_sector executed"
			add_stability = 0.03
			add_political_power = 100
		}

		prerequisite = {
			focus = RWA_national_parks
		}


		prerequisite = {
			focus = RWA_investing_in_the_capital
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_path_to_growth
		icon = economic_prosperity2


		x = 22
		y = 3
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_path_to_growth executed"
			increase_economic_growth = yes
		}

		prerequisite = {
			focus = RWA_cash_crop_economy
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_lessons_from_the_asians
		icon = race_relations2


		x = 21
		y = 4
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_lessons_from_the_asians executed"
			add_political_power = 150
		}

		prerequisite = {
			focus = RWA_path_to_growth
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_investing_in_the_capital
		icon = construction

		x = 21
		y = 5
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_investing_in_the_capital executed"
			250 = {
				add_extra_state_shared_building_slots = 1
			}
			custom_effect_tooltip = 2_billion_expense_tt
			hidden_effect = {
				add_to_variable = { treasury = -2.0 }
			}
		}

		prerequisite = {
			focus = RWA_lessons_from_the_asians
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_ban_plastics
		icon = plastic_ban


		x = 21
		y = 6
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_ban_plastics executed"
			add_stability = 0.02
		}

		prerequisite = {
			focus = RWA_investing_in_the_capital
		}

		prerequisite = {
			focus = RWA_national_parks
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_small_arms_facility
		icon = industry_military


		x = 23
		y = 4
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_small_arms_facility executed"
			250 = {
				add_extra_state_shared_building_slots = 1
				add_building_construction = {
					type = arms_factory
					level = 1
					instant_build = yes
				}
			}
			custom_effect_tooltip = 8_billion_expense_tt
			hidden_effect = {
				add_to_variable = { treasury = -8.0 }
			}
		}

		prerequisite = {
			focus = RWA_path_to_growth
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_investing_in_infrastructure
		icon = infrastructure


		x = 23
		y = 5
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_investing_in_infrastructure executed"
			250 = {
				add_extra_state_shared_building_slots = 1
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
			custom_effect_tooltip = 6_billion_expense_tt
			hidden_effect = {
				add_to_variable = { treasury = -6.0 }
			}
		}

		prerequisite = {
			focus = RWA_small_arms_facility
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_rwand_air
		icon = rwand_airline


		x = 23
		y = 6
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_rwand_air executed"
			add_political_power = 100
			custom_effect_tooltip = 1_billion_expense_tt
			hidden_effect = {
				subtract_from_variable = { treasury = 1.0 }
			}
		}

		prerequisite = {
			focus = RWA_investing_in_infrastructure
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_small_arms_tech
		icon = small_arms_west


		x = 25
		y = 5
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_small_arms_tech executed"
			add_tech_bonus = {
				name = infantry_weapons
				bonus = 0.30
				technology = infantry_weapons
				technology = infantry_weapons1
				technology = infantry_weapons2
				technology = infantry_weapons3
				technology = infantry_weapons4
				technology = infantry_weapons5
				technology = infantry_weapons6
				technology = infantry_weapons7
			}
		}

		prerequisite = {
			focus = RWA_small_arms_facility
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_small_airforce
		icon = modern_fighter


		x = 25
		y = 6
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_small_airforce executed"
			add_tech_bonus = {
				name = fighter_bonus
				bonus = 0.25
				technology = MR_Fighter1
				technology = MR_Fighter2
				technology = MR_Fighter3
				technology = MR_Fighter4
				technology = MR_upgrade_1
			}
		}

		prerequisite = {
			focus = RWA_small_arms_tech
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_speak_at_aipac
		icon = align_to_israel


		x = 17
		y = 6
		
		cost = 10

		available = {
			has_country_leader = { name = "Paul Kagame" ruling_only = yes }
		}

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_speak_at_aipac executed"
			ISR = {
				add_opinion_modifier = {
					target = RWA
					modifier = declaration_of_friendship
				}
				reverse_add_opinion_modifier = {
					target = RWA
					modifier = declaration_of_friendship
				}
			}
			add_country_leader_trait = pro_israel
		}

		prerequisite = {
			focus = RWA_joining_the_commonwealth
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = RWA_africas_singapore
		icon = u


		x = 19
		y = 7
		
		cost = 15
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus RWA_africas_singapore executed"
			add_political_power = 300
		}

		prerequisite = {
			focus = RWA_booming_tourism_sector
		}

		prerequisite = {
			focus = RWA_rwand_air
		}

		prerequisite = {
			focus = RWA_a_new_rwanda
		}

		ai_will_do = {
			base = 10
		}
	}
}
