add_namespace = UGA

news_event = {
	id = UGA.0
	title = UGA.0.t
	desc = UGA.0.d
	picture = GFX_TNZ_startup_2000

	immediate = { log = "[GetDateText]: [Root.GetName]: event UGA.0" }


	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = UGA.0.a
	}
}

country_event = {
	id = UGA.1
	immediate = { log = "[GetDateText]: [Root.GetName]: event UGA.1" }
	title = UGA.1.t
	desc = UGA.1.d
	is_triggered_only = yes
	option = {
		name = UGA.1.a
		log = "[GetDateText]: [Root.GetName]: event UGA.1.a"
		increase_healthcare_budget = yes
		add_stability = 0.03
		add_political_power = 25
	}

	option = {
		name = UGA.1.b
		log = "[GetDateText]: [Root.GetName]: event UGA.1.b"
		increase_social_spending = yes
		add_stability = 0.02
		add_political_power = 50
	}

	option = {
		name = UGA.1.c
		log = "[GetDateText]: [Root.GetName]: event UGA.1.c"
		increase_education_budget = yes
		add_stability = 0.01
		add_political_power = 75
	}
}

country_event = { 
	id = UGA.2
	immediate = { log = "[GetDateText]: [Root.GetName]: event UGA.2" }
	title = UGA.2.t
	desc = UGA.2.d
	is_triggered_only = yes
	option = {
		name = UGA.2.a
		log = "[GetDateText]: [Root.GetName]: event UGA.2.a"
		add_resource = { 
			type = oil
			amount = 5
			state = 248
		}
		DRC = {
			add_opinion_modifier = {
				target = UGA
				modifier = trespassing
			}
		}
		custom_effect_tooltip = 1_billion_expense_tt
		add_to_variable = { treasury = -1.0 }
	}

	option = {
		name = UGA.2.b
		log = "[GetDateText]: [Root.GetName]: event UGA.2.b"
		add_resource = { 
			type = oil
			amount = 3
			state = 248
		}
		add_resource = { 
			type = oil
			amount = 2
			state = 20
		}
		DRC = {
			add_opinion_modifier = {
				target = UGA
				modifier = eased_border_tensions
			}
		}
		custom_effect_tooltip = 1_billion_expense_tt
		add_to_variable = { treasury = -1.0 }
	}
}

country_event = { 
	id = UGA.3
	immediate = { log = "[GetDateText]: [Root.GetName]: event UGA.3" }
	title = UGA.3.t
	desc = UGA.3.d
	is_triggered_only = yes

	option = {
		name = UGA.3.a
		log = "[GetDateText]: [Root.GetName]: event UGA.3.a"
		random_owned_state = { 
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = industrial_complex
				level = 1
				instant_build = yes
			}
		}
		custom_effect_tooltip = 4_billion_expense_tt
		add_to_variable = { treasury = -4 }
		add_stability = 0.03
		add_political_power = 50
	}

	option = {
		name = UGA.3.b
		log = "[GetDateText]: [Root.GetName]: event UGA.3.b"
		random_owned_state = { 
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
		custom_effect_tooltip = 4_billion_expense_tt
		add_to_variable = { treasury = -4 }
		if = {
			limit = {
				has_idea = international_bankers
				NOT = { has_country_flag = enthusiastic_international_bankers }
			}
			visually_display_opinion_rise_international_bankers = yes
			set_country_flag = current_international_bankers
			increase_internal_faction_opinion = yes
		}
		else_if = {
			add_stability = 0.02
			add_political_power = 75
		}
	}

	option = {
		name = UGA.3.c
		log = "[GetDateText]: [Root.GetName]: event UGA.3.c"
		random_owned_state = { add_resource = { type = steel amount = 4 } add_building_construction = { type = infrastructure level = 1 instant_build = yes } }
		custom_effect_tooltip = 4_billion_expense_tt
		add_to_variable = { treasury = -4 }
		if = {
			limit = {
				has_idea = industrial_conglomerates
				NOT = { has_country_flag = enthusiastic_industrial_conglomerates }
			}
			visually_display_opinion_rise_industrial_conglomerates = yes
			set_country_flag = current_industrial_conglomerates
			increase_internal_faction_opinion = yes
		}
		else_if = {
			add_stability = 0.01
			add_political_power = 100
		}
	}
}

country_event = { 
	id = UGA.4
	immediate = { log = "[GetDateText]: [Root.GetName]: event UGA.4" }
	title = UGA.4.t
	desc = UGA.4.d
	is_triggered_only = yes

	option = {
		name = UGA.4.a
		log = "[GetDateText]: [Root.GetName]: event UGA.4.a"
		add_political_power = 20
	}
	option = {
		name = UGA.4.b
		log = "[GetDateText]: [Root.GetName]: event UGA.4.b"
		remove_ideas = USA_usaid
	}
}

country_event = {
	id = UGA.5
	immediate = { log = "[GetDateText]: [Root.GetName]: event UGA.5" }
	title = UGA.5.t
	desc = UGA.5.d
	is_triggered_only = yes

	option = {
		name = UGA.5.a
		log = "[GetDateText]: [Root.GetName]: event UGA.5.a"
		RWA = {
			country_event = UGA.6
		}
	}
}

country_event = {
	id = UGA.6
	immediate = { log = "[GetDateText]: [Root.GetName]: event UGA.6" }
	title = UGA.6.t
	desc = UGA.6.d
	is_triggered_only = yes

	option = {
		name = UGA.6.a
		log = "[GetDateText]: [Root.GetName]: event UGA.6.a"
		UGA = {
			country_event = UGA.7
		}
	}
	option = {
		name = UGA.6.b
		log = "[GetDateText]: [Root.GetName]: event UGA.6.b"
		UGA = {
			country_event = UGA.7
		}
	}
}

country_event = {
	id = UGA.7
	title = UGA.7.t
	desc = UGA.7.d
	picture = GFX_EAF_event_ken_tnz_UGA

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = UGA.7.a
		DRC = {
			country_event = rwa.6
		}
		ai_chance = {
			factor = 90
			modifier = {
				factor = 8
				DRC = {
					has_country_leader = { name = "Joseph Kabila" ruling_only = yes }
				}
			}
		}
	}

	option = {
		name = UGA.7.b
		DRC = {
			country_event = rwa.7
		}
		ai_chance = {
			factor = 10
			modifier = {
				factor = 4000
				DRC = {
					NOT = {
						has_full_control_of_state = 303
					}
				}
			}
			modifier = {
				factor = 2
				DRC = {
					has_country_leader = { name = "Joseph Kabila" ruling_only = yes }
				}
			}
		}
	}

	option = {
		name = UGA.7.c
		DRC = {
			country_event = rwa.8
		}
		ai_chance = {
			factor = 10
			modifier = {
				factor = 4000
				DRC = {
					NOT = {
						has_full_control_of_state = 302
					}
				}
			}
			modifier = {
				factor = 2
				DRC = {
					has_country_leader = { name = "Joseph Kabila" ruling_only = yes }
				}
			}
		}
	}
}

country_event = {
	id = UGA.9
	title = UGA.9.t
	desc = UGA.9.d
	picture = GFX_EAF_event_ken_tnz_UGA


	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = UGA.9.a
		RCD = {
			transfer_state = 306
			transfer_state = 307
			set_capital = 306
		}
		DRC = {
			transfer_state = 302
			transfer_state = 303
			transfer_state = 304
			transfer_state = 313
			transfer_state = 314
		}
		MLC = {
			transfer_state = 312
			transfer_state = 311
			transfer_state = 20
			transfer_state = 309
			transfer_state = 310
			transfer_state = 308
		}



		ai_chance = { factor = 100 }
	}
}

country_event = {
	id = UGA.20
	title = UGA.20.t
	desc = UGA.20.d
	picture = GFX_EAF_event_ken_tnz_UGA
	immediate = { log = "[GetDateText]: [Root.GetName]: event UGA.20" }

	is_triggered_only = yes

	option = {
		name = UGA.20.a
		log = "[GetDateText]: [This.GetName]: UGA.20.a executed"
		country_event = UGA.21
		ai_chance = { factor = 1 }
	}
	option = {
		name = UGA.20.b
		log = "[GetDateText]: [This.GetName]: UGA.20.b executed"
		add_political_power = -5
		ai_chance = { factor = 1 }
	}
}

country_event = {
	id = UGA.21
	title = UGA.21.t
	desc = UGA.21.d
	picture = GFX_EAF_event_ken_tnz_UGA
	immediate = { log = "[GetDateText]: [Root.GetName]: event UGA.21" }

	is_triggered_only = yes

	option = {
		name = UGA.21.a
		log = "[GetDateText]: [This.GetName]: UGA.21.a executed"
		KEN = {
			country_event = UGA.12
		}
		ai_chance = { factor = 1 }
	}
	option = {
		name = UGA.21.b
		log = "[GetDateText]: [This.GetName]: UGA.21.b executed"
		TNZ = {
			country_event = UGA.13
		}
		ai_chance = { factor = 1 }
	}
}

country_event = {
	id = UGA.12
	title = UGA.12.t
	desc = UGA.12.d
	picture = GFX_EAF_event_ken_tnz_UGA
	immediate = { log = "[GetDateText]: [Root.GetName]: event UGA.12" }

	is_triggered_only = yes

	option = {
		name = UGA.12.a
		log = "[GetDateText]: [This.GetName]: UGA.12.a executed"
		hidden_effect = {
			UGA = {
				set_temp_variable = { influence_gain = 30 }
				for_loop_effect = {
					end = influence_array^num
					value = v
					if = {
						limit = {
							check_variable = { influence_array^v = TNZ }
						}
						add_to_variable = { influence_array_val^v = influence_gain }
						set_country_flag = found
					}
				}
				if = {
					limit = { NOT = { has_country_flag = found } }
						add_to_array = { influence_array = TNZ.id }
						add_to_array = { influence_array_val = influence_gain }
				}
				clr_country_flag = found
				recalculate_influence = yes
			}
		}
		ai_chance = { factor = 1 }
	}
	option = {
		name = UGA.12.b
		log = "[GetDateText]: [This.GetName]: UGA.12.b executed"
		add_political_power = -5
		ai_chance = { factor = 1 }
	}
}

country_event = {
	id = UGA.13
	title = UGA.13.t
	desc = UGA.13.d
	picture = GFX_EAF_event_ken_tnz_UGA
	immediate = { log = "[GetDateText]: [Root.GetName]: event UGA.13" }

	is_triggered_only = yes

	option = {
		name = UGA.13.a
		log = "[GetDateText]: [This.GetName]: UGA.13.a executed"
		hidden_effect = {
			UGA = {
				set_temp_variable = { influence_gain = 30 }
				for_loop_effect = {
					end = influence_array^num
					value = v
					if = {
						limit = {
							check_variable = { influence_array^v = TNZ }
						}
						add_to_variable = { influence_array_val^v = influence_gain }
						set_country_flag = found
					}
				}
				if = {
					limit = { NOT = { has_country_flag = found } }
						add_to_array = { influence_array = TNZ.id }
						add_to_array = { influence_array_val = influence_gain }
				}
				clr_country_flag = found
				recalculate_influence = yes
			}
		}
		ai_chance = { factor = 1 }
	}
	option = {
		name = UGA.13.b
		log = "[GetDateText]: [This.GetName]: UGA.13.b executed"
		add_political_power = -5
		ai_chance = { factor = 1 }
	}
}