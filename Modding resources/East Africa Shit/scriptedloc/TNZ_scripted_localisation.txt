defined_text = {
	name = TNZ_congo_saf

	text = { trigger = { SAF = { has_country_leader { name = "Thabo Mbeki" ruling_only = yes } } } localization_key = TNZ_congo_saf_tt }
}

defined_text = {
	name = TNZ_congo_saf_backs

	text = { trigger = { SAF = { has_country_leader { name = "Thabo Mbeki" ruling_only = yes } } } localization_key = TNZ_congo_saf_backs_tt }
}

defined_text = {
	name = TNZ_west_anti_pirates

	text = { localization_key = TNZ_combat_pirates_tt }

	text = { trigger = { has_completed_focus = TNZ_american_bases } localization_key = TNZ_nato_combat_pirates_tt }

	text = { trigger = { has_completed_focus = TNZ_pro_eu } localization_key = TNZ_eu_combat_pirates_tt }

	text = { trigger = { has_completed_focus = TNZ_chinese_mission } localization_key = TNZ_china_combat_pirates_tt }
}

defined_text = {
	name = get_current_party_leader_portrait

	text = {
		trigger = {
			has_political_power > 10
		}
		localization_key = GFX_amina_salum_ali_small
	}
}

defined_text = {
	name = current_party_leader

	text = { localization_key = gui_party_leader }

	text = { trigger = { has_country_flag = TNZ_new_ccm_candidate } localization_key = gui_party_candidate }

}