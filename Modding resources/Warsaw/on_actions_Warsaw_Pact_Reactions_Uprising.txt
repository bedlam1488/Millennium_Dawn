##########Uprising in Poland (Warsaw Pact) №1
		if = {
			limit = {
				original_tag = POL
				NOT = {has_country_flag = POL_uprising_anti_wp}
				POL = {
					NOT = {
						AND = {
							has_country_flag = WP_normal_puppet
							has_country_flag = WP_escaped
						}
					}
					has_civil_war = yes
					has_country_flag = warsaw_pact_reintegrate
					has_country_flag = target_wp_pl
				}
			}
			set_country_flag = POL_uprising_anti_wp
			####Czechia reaction
			if = {
				limit = {
					country_exists = CZE
					CZE = {
						NOT = {
							AND = {
								has_country_flag = WP_normal_puppet
								has_country_flag = WP_escaped
							}
						}
						has_civil_war = no
						AND = {
							has_autonomy_state = autonomy_warsaw
							has_country_flag = warsaw_pact_reintegrate
						}
					}
				}
				CZE = { country_event = SOV_Global.20 }
			}
			####Slovakia reaction
			if = {
				limit = {
					country_exists = SLO
					SLO = {
						NOT = {
							AND = {
								has_country_flag = WP_normal_puppet
								has_country_flag = WP_escaped
							}
						}
						has_civil_war = no
						AND = {
							has_autonomy_state = autonomy_warsaw
							has_country_flag = warsaw_pact_reintegrate
						}
					}
				}
				SLO = { country_event = SOV_Global.20 }
			}
			####Hungary reaction
			if = {
				limit = {
					country_exists = HUN
					HUN = {
						NOT = {
							AND = {
								has_country_flag = WP_normal_puppet
								has_country_flag = WP_escaped
							}
						}
						has_civil_war = no
						AND = {
							has_autonomy_state = autonomy_warsaw
							has_country_flag = warsaw_pact_reintegrate
						}
					}
				}
				HUN = { country_event = SOV_Global.20 }
			}
			####Romania reaction
			if = {
				limit = {
					country_exists = ROM
					ROM = {
						NOT = {
							AND = {
								has_country_flag = WP_normal_puppet
								has_country_flag = WP_escaped
							}
						}
						has_civil_war = no
						AND = {
							has_autonomy_state = autonomy_warsaw
							has_country_flag = warsaw_pact_reintegrate
						}
					}
				}
				ROM = { country_event = SOV_Global.20 }
			}
			####Bulgaria reaction
			if = {
				limit = {
					country_exists = BUL
					BUL = {
						NOT = {
							AND = {
								has_country_flag = WP_normal_puppet
								has_country_flag = WP_escaped
							}
						}
						has_civil_war = no
						AND = {
							has_autonomy_state = autonomy_warsaw
							has_country_flag = warsaw_pact_reintegrate
						}
					}
				}
				BUL = { country_event = SOV_Global.20 }
			}
			####Albania reaction
			if = {
				limit = {
					country_exists = ALB
					ALB = {
						NOT = {
							AND = {
								has_country_flag = WP_normal_puppet
								has_country_flag = WP_escaped
							}
						}
						has_civil_war = no
						AND = {
							has_autonomy_state = autonomy_warsaw
							has_country_flag = warsaw_pact_reintegrate
						}
					}
				}
				ALB = { country_event = SOV_Global.20 }
			}
			####USSR reaction
			if = {
				limit = {
					country_exists = SOV
					SOV = {
						NOT = {
							has_government = neutral
							has_government = western
							has_government = nationalist
						}
						has_civil_war = no
						AND = {
							has_government = communism
							has_idea = SOV_warsaw_pact_idea
						}
					}
				}
				SOV = { country_event = SOV_Global.20 }
			}






##########Uprising in Czechia (Warsaw Pact) №2
		if = {
			limit = {
				original_tag = CZE
				NOT = {has_country_flag = CZE_uprising_anti_wp}
				CZE = {
					NOT = {
						AND = {
							has_country_flag = WP_normal_puppet
							has_country_flag = WP_escaped
						}
					}
					has_civil_war = yes
					has_country_flag = warsaw_pact_reintegrate
					has_country_flag = target_wp_cz
				}
			}
			set_country_flag = CZE_uprising_anti_wp
			####Poland reaction
			if = {
				limit = {
					country_exists = POL
					POL = {
						NOT = {
							AND = {
								has_country_flag = WP_normal_puppet
								has_country_flag = WP_escaped
							}
						}
						has_civil_war = no
						AND = {
							has_autonomy_state = autonomy_warsaw
							has_country_flag = warsaw_pact_reintegrate
						}
					}
				}
				POL = { country_event = SOV_Global.21 }
			}
			####Slovakia reaction
			if = {
				limit = {
					country_exists = SLO
					SLO = {
						NOT = {
							AND = {
								has_country_flag = WP_normal_puppet
								has_country_flag = WP_escaped
							}
						}
						has_civil_war = no
						AND = {
							has_autonomy_state = autonomy_warsaw
							has_country_flag = warsaw_pact_reintegrate
						}
					}
				}
				SLO = { country_event = SOV_Global.21 }
			}
			####Hungary reaction
			if = {
				limit = {
					country_exists = HUN
					HUN = {
						NOT = {
							AND = {
								has_country_flag = WP_normal_puppet
								has_country_flag = WP_escaped
							}
						}
						has_civil_war = no
						AND = {
							has_autonomy_state = autonomy_warsaw
							has_country_flag = warsaw_pact_reintegrate
						}
					}
				}
				HUN = { country_event = SOV_Global.21 }
			}
			####Romania reaction
			if = {
				limit = {
					country_exists = ROM
					ROM = {
						NOT = {
							AND = {
								has_country_flag = WP_normal_puppet
								has_country_flag = WP_escaped
							}
						}
						has_civil_war = no
						AND = {
							has_autonomy_state = autonomy_warsaw
							has_country_flag = warsaw_pact_reintegrate
						}
					}
				}
				ROM = { country_event = SOV_Global.21 }
			}
			####Bulgaria reaction
			if = {
				limit = {
					country_exists = BUL
					BUL = {
						NOT = {
							AND = {
								has_country_flag = WP_normal_puppet
								has_country_flag = WP_escaped
							}
						}
						has_civil_war = no
						AND = {
							has_autonomy_state = autonomy_warsaw
							has_country_flag = warsaw_pact_reintegrate
						}
					}
				}
				BUL = { country_event = SOV_Global.21 }
			}
			####Albania reaction
			if = {
				limit = {
					country_exists = ALB
					ALB = {
						NOT = {
							AND = {
								has_country_flag = WP_normal_puppet
								has_country_flag = WP_escaped
							}
						}
						has_civil_war = no
						AND = {
							has_autonomy_state = autonomy_warsaw
							has_country_flag = warsaw_pact_reintegrate
						}
					}
				}
				ALB = { country_event = SOV_Global.21 }
			}
			####USSR reaction
			if = {
				limit = {
					country_exists = SOV
					SOV = {
						NOT = {
							has_government = neutral
							has_government = western
							has_government = nationalist
						}
						has_civil_war = no
						AND = {
							has_government = communism
							has_idea = SOV_warsaw_pact_idea
						}
					}
				}
				SOV = { country_event = SOV_Global.21 }
			}









##########Uprising in Slovakia (Warsaw Pact) №3
		if = {
			limit = {
				original_tag = SLO
				NOT = {has_country_flag = SLO_uprising_anti_wp}
				SLO = {
					NOT = {
						AND = {
							has_country_flag = WP_normal_puppet
							has_country_flag = WP_escaped
						}
					}
					has_civil_war = yes
					has_country_flag = warsaw_pact_reintegrate
					has_country_flag = target_wp_sl
				}
			}
			set_country_flag = SLO_uprising_anti_wp
			####Poland reaction
			if = {
				limit = {
					country_exists = POL
					POL = {
						NOT = {
							AND = {
								has_country_flag = WP_normal_puppet
								has_country_flag = WP_escaped
							}
						}
						has_civil_war = no
						AND = {
							has_autonomy_state = autonomy_warsaw
							has_country_flag = warsaw_pact_reintegrate
						}
					}
				}
				POL = { country_event = SOV_Global.22 }
			}
			####Czechia reaction
			if = {
				limit = {
					country_exists = CZE
					CZE = {
						NOT = {
							AND = {
								has_country_flag = WP_normal_puppet
								has_country_flag = WP_escaped
							}
						}
						has_civil_war = no
						AND = {
							has_autonomy_state = autonomy_warsaw
							has_country_flag = warsaw_pact_reintegrate
						}
					}
				}
				CZE = { country_event = SOV_Global.22 }
			}
			####Hungary reaction
			if = {
				limit = {
					country_exists = HUN
					HUN = {
						NOT = {
							AND = {
								has_country_flag = WP_normal_puppet
								has_country_flag = WP_escaped
							}
						}
						has_civil_war = no
						AND = {
							has_autonomy_state = autonomy_warsaw
							has_country_flag = warsaw_pact_reintegrate
						}
					}
				}
				HUN = { country_event = SOV_Global.22 }
			}
			####Romania reaction
			if = {
				limit = {
					country_exists = ROM
					ROM = {
						NOT = {
							AND = {
								has_country_flag = WP_normal_puppet
								has_country_flag = WP_escaped
							}
						}
						has_civil_war = no
						AND = {
							has_autonomy_state = autonomy_warsaw
							has_country_flag = warsaw_pact_reintegrate
						}
					}
				}
				ROM = { country_event = SOV_Global.22 }
			}
			####Bulgaria reaction
			if = {
				limit = {
					country_exists = BUL
					BUL = {
						NOT = {
							AND = {
								has_country_flag = WP_normal_puppet
								has_country_flag = WP_escaped
							}
						}
						has_civil_war = no
						AND = {
							has_autonomy_state = autonomy_warsaw
							has_country_flag = warsaw_pact_reintegrate
						}
					}
				}
				BUL = { country_event = SOV_Global.22 }
			}
			####Albania reaction
			if = {
				limit = {
					country_exists = ALB
					ALB = {
						NOT = {
							AND = {
								has_country_flag = WP_normal_puppet
								has_country_flag = WP_escaped
							}
						}
						has_civil_war = no
						AND = {
							has_autonomy_state = autonomy_warsaw
							has_country_flag = warsaw_pact_reintegrate
						}
					}
				}
				ALB = { country_event = SOV_Global.22 }
			}
			####USSR reaction
			if = {
				limit = {
					country_exists = SOV
					SOV = {
						NOT = {
							has_government = neutral
							has_government = western
							has_government = nationalist
						}
						has_civil_war = no
						AND = {
							has_government = communism
							has_idea = SOV_warsaw_pact_idea
						}
					}
				}
				SOV = { country_event = SOV_Global.22 }
			}
			##########Uprising in Hungary (Warsaw Pact) №4
		if = {
			limit = {
				original_tag = HUN
				NOT = {has_country_flag = HUN_uprising_anti_wp}
				HUN = {
					NOT = {
						AND = {
							has_country_flag = WP_normal_puppet
							has_country_flag = WP_escaped
						}
					}
					has_civil_war = yes
					has_country_flag = warsaw_pact_reintegrate
					has_country_flag = target_wp_hn
				}
			}
			set_country_flag = HUN_uprising_anti_wp
			####Poland reaction
			if = {
				limit = {
					country_exists = POL
					POL = {
						NOT = {
							AND = {
								has_country_flag = WP_normal_puppet
								has_country_flag = WP_escaped
							}
						}
						has_civil_war = no
						AND = {
							has_autonomy_state = autonomy_warsaw
							has_country_flag = warsaw_pact_reintegrate
						}
					}
				}
				POL = { country_event = SOV_Global.23 }
			}
			####Czechia reaction
			if = {
				limit = {
					country_exists = CZE
					CZE = {
						NOT = {
							AND = {
								has_country_flag = WP_normal_puppet
								has_country_flag = WP_escaped
							}
						}
						has_civil_war = no
						AND = {
							has_autonomy_state = autonomy_warsaw
							has_country_flag = warsaw_pact_reintegrate
						}
					}
				}
				CZE = { country_event = SOV_Global.23 }
			}
			####Slovakia reaction
			if = {
				limit = {
					country_exists = SLO
					SLO = {
						NOT = {
							AND = {
								has_country_flag = WP_normal_puppet
								has_country_flag = WP_escaped
							}
						}
						has_civil_war = no
						AND = {
							has_autonomy_state = autonomy_warsaw
							has_country_flag = warsaw_pact_reintegrate
						}
					}
				}
				HUN = { country_event = SOV_Global.23 }
			}
			####Romania reaction
			if = {
				limit = {
					country_exists = ROM
					ROM = {
						NOT = {
							AND = {
								has_country_flag = WP_normal_puppet
								has_country_flag = WP_escaped
							}
						}
						has_civil_war = no
						AND = {
							has_autonomy_state = autonomy_warsaw
							has_country_flag = warsaw_pact_reintegrate
						}
					}
				}
				ROM = { country_event = SOV_Global.23 }
			}
			####Bulgaria reaction
			if = {
				limit = {
					country_exists = BUL
					BUL = {
						NOT = {
							AND = {
								has_country_flag = WP_normal_puppet
								has_country_flag = WP_escaped
							}
						}
						has_civil_war = no
						AND = {
							has_autonomy_state = autonomy_warsaw
							has_country_flag = warsaw_pact_reintegrate
						}
					}
				}
				BUL = { country_event = SOV_Global.23 }
			}
			####Albania reaction
			if = {
				limit = {
					country_exists = ALB
					ALB = {
						NOT = {
							AND = {
								has_country_flag = WP_normal_puppet
								has_country_flag = WP_escaped
							}
						}
						has_civil_war = no
						AND = {
							has_autonomy_state = autonomy_warsaw
							has_country_flag = warsaw_pact_reintegrate
						}
					}
				}
				ALB = { country_event = SOV_Global.23 }
			}
			####USSR reaction
			if = {
				limit = {
					country_exists = SOV
					SOV = {
						NOT = {
							has_government = neutral
							has_government = western
							has_government = nationalist
						}
						has_civil_war = no
						AND = {
							has_government = communism
							has_idea = SOV_warsaw_pact_idea
						}
					}
				}
				SOV = { country_event = SOV_Global.23 }
			}
}