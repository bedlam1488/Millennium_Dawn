officer_corps_training_category = {

	army_officer_training_course = {

		custom_cost_trigger = {
			command_power > 20
		}

		custom_cost_text = command_power_more_than_20
	
		available = {
			has_war = no
			OR = {
				AND = {
					has_idea = officer_basic_training
					any_army_leader = {
						skill < 2
					}
				}
				AND = {
					has_idea = officer_advanced_training
					any_army_leader = {
						skill < 3
					}
				}
				AND = {
					has_idea = officer_military_school
					any_army_leader = {
						skill < 4
					}
				}
				AND = {
					has_idea = officer_military_academy
					any_army_leader = {
						skill < 5
					}
				}
				AND = {
					has_idea = officer_international_education
					any_army_leader = {
						skill < 6
					}
				}
			}
		}
		
		days_remove = 120
		
		days_re_enable = 7
		
		remove_effect = {
			random_army_leader = {
				limit = {
					OR = {
						AND = {
							ROOT = { has_idea = officer_basic_training }
							skill < 2
						}
						AND = {
							ROOT = { has_idea = officer_advanced_training }
							skill < 3
						}
						AND = {
							ROOT = { has_idea = officer_military_school }
							skill < 4
						}
						AND = {
							ROOT = { has_idea = officer_military_academy }
							skill < 5
						}
						AND = {
							ROOT = { has_idea = officer_international_education }
							skill < 6
						}
					}
				}
				add_skill_level = 1
			}
		}
	
	}

	navy_officer_training_course = {

		custom_cost_trigger = {
			command_power > 20
		}

		custom_cost_text = command_power_more_than_20
	
		available = {
			has_war = no
			OR = {
				AND = {
					has_idea = officer_basic_training
					any_navy_leader = {
						skill < 2
					}
				}
				AND = {
					has_idea = officer_advanced_training
					any_navy_leader = {
						skill < 3
					}
				}
				AND = {
					has_idea = officer_military_school
					any_navy_leader = {
						skill < 4
					}
				}
				AND = {
					has_idea = officer_military_academy
					any_navy_leader = {
						skill < 5
					}
				}
				AND = {
					has_idea = officer_international_education
					any_navy_leader = {
						skill < 6
					}
				}
			}
		}
		
		days_remove = 120
		
		days_re_enable = 7
		
		remove_effect = {
			random_navy_leader = {
				limit = {
					OR = {
						AND = {
							ROOT = { has_idea = officer_basic_training }
							skill < 2
						}
						AND = {
							ROOT = { has_idea = officer_advanced_training }
							skill < 3
						}
						AND = {
							ROOT = { has_idea = officer_military_school }
							skill < 4
						}
						AND = {
							ROOT = { has_idea = officer_military_academy }
							skill < 5
						}
						AND = {
							ROOT = { has_idea = officer_international_education }
							skill < 6
						}
					}
				}
				add_skill_level = 1
			}
		}
	
	}
	
}