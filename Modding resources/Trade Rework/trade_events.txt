add_namespace = trade
country_event = {
	id = trade.1
	immediate = {log = "[GetDateText]: [Root.GetTag]: id = trade.1"}
	hidden = yes
	is_triggered_only = yes

	immediate = {
		trade_check = yes
	}
}
country_event = {	#Oil Check country_event = trade.2
	id = trade.2
	immediate = {log = "[GetDateText]: [Root.GetTag]: id = trade.2"}
	hidden = yes
	is_triggered_only = yes

	immediate = {
		oilDeliveryCuts = yes
	}
}
country_event = {	#Steel Check country_event = trade.3
	id = trade.3
	immediate = {log = "[GetDateText]: [Root.GetTag]: id = trade.3"}
	hidden = yes
	is_triggered_only = yes

	immediate = {
		steelDeliveryCuts = yes
	}
}
country_event = {	#Rare Earth Check country_event = trade.4
	id = trade.4
	immediate = {log = "[GetDateText]: [Root.GetTag]: id = trade.4"}
	hidden = yes
	is_triggered_only = yes

	immediate = {
		rareEarthDeliveryCuts = yes
	}
}
country_event = {	#Aluminium Check country_event = trade.5
	id = trade.5
	immediate = {log = "[GetDateText]: [Root.GetTag]: id = trade.5"}
	hidden = yes
	is_triggered_only = yes

	immediate = {
		aluminiumDeliveryCuts = yes
	}
}
country_event = {	#Nuclear Check country_event = trade.6
	id = trade.6
	immediate = {log = "[GetDateText]: [Root.GetTag]: id = trade.6"}
	hidden = yes
	is_triggered_only = yes

	immediate = {
		chromiumDeliveryCuts = yes
	}
}
country_event = {	#Nuclear Check country_event = trade.7
	id = trade.7
	immediate = {log = "[GetDateText]: [Root.GetTag]: id = trade.7"}
	hidden = yes
	is_triggered_only = yes

	immediate = {
		foodDeliveryCuts = yes
	}
}