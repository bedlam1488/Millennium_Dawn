cz_idk_category = {

	icon = czechia_cat

	priority = 25

	visible = {
		#OR = {
		#	#original_tag = CZE
		#	#original_tag = MRV
		#	#original_tag = SLO
		#	#original_tag = POL
		#	#original_tag = HUN
		#	#original_tag = NOR
		#	#original_tag = GER
		#	#original_tag = SWE
		#	#original_tag = DEN
		#	is_ai = no
		#}
		is_ai = no
	}
}
czechia_category = {
	
	icon = czechia_cat

	priority = 25

	visible = {
		original_tag = CZE
	}
}
cz_czechoslovakia_category = {
	
	icon = czechia_cat

	picture = GFX_decision_cat_csr

	priority = 25

	visible = {
		OR = {
			original_tag = CZE
			original_tag = SLO
		}
	}
}
cz_togo_category = {
	
	icon = czechia_cat

	picture = GFX_decision_cat_togo

	priority = 25

	visible = {
		original_tag = CZE
	}
}
cz_visegrad_group_category = {

	icon = cz_v4_cat

	picture = GFX_decision_cat_v4

	priority = 25

	visible = {
		original_tag = CZE
	}
}
cz_TSI_group_category = {

	icon = cz_3si_cat

	picture = GFX_decision_cat_3si

	priority = 26

	visible = {
		#OR = {
			original_tag = CZE
			#original_tag = SLO
		#}
	}
}
cz_TSI_group_diplomacy_category = {

	icon = cz_3si_cat

	picture = GFX_decision_cat_3si

	priority = 26

	visible = {
		always = yes
	}
}
cz_TSI_group_invites_category = {

	icon = cz_3si_cat

	picture = GFX_decision_cat_3si

	priority = 26

	visible = {
		always = yes
	}
}
cz_TSI_group_kicks_category = {

	icon = cz_3si_cat

	picture = GFX_decision_cat_3si

	priority = 26

	visible = {
		always = yes
	}
}
cz_TSI_group_join_leave_category = {

	icon = cz_3si_cat

	picture = GFX_decision_cat_3si

	priority = 26

	visible = {
		always = yes
	}
}
cz_get_locked_ideo_ideas_category = {

	icon = czechia_cat

	priority = 25

	visible = {
		original_tag = CZE
	}
}
cz_AZ_category = {

	icon = cz_AZ_cat

	picture = GFX_decision_cat_AZ

	priority = 25

	visible = {
		OR = {
			original_tag = CZE
			original_tag = MRV
		}
	}
}
czechoslovak_group_category = {

	icon = czechia_cat

	priority = 25

	visible = {
		OR = {
			original_tag = CZE
			original_tag = SLO
		}
		NOT = {
			CZE = {
				has_war_with = SLO
			}
		}
		has_country_flag = czechoslovak_group
		NOT = { has_communist_government = yes }
	}

	visible_when_empty = yes
}
cz_highway_category = {

	icon = cz_highway_cat

	picture = GFX_decision_cat_highway

	priority = 25

	visible = {
		always = yes
	}
}