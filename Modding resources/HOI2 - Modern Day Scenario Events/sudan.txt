#########################################################
#							#
# Events for Sudan (SUD) by Zokan                       #
#      			                                #
#########################################################

#########
# Index #
#########################################################
#From	- To	#	Name 			 
#########################################################
#77501 - 77800# Main events
#77801 - 77950# Political events
#77951 - 78000# Election events	 		
#########################################################

############################################################################################################################

#############################################
###### Main events
#############################################

#############################################
###### Humanitarian Ceasefire Agreement
#############################################
event = {
         id = 77501
         random = no
         country = SUD
         trigger = {			
		   atwar = no

         }
 
         name = "EVT_77501_NAME"
         desc = "EVT_77501_DESC"
         style = 0
	 picture = "no"
 
         date = { day = 8 month = april year = 2004 }
 
           action_a = {
                  name = "Sign the cease fire agreement"
		  ai_chance = 95
                  command = { type = dissent value = -1 }
		  command = { type = belligerence value = -1 }
		  command = { type = relation which = OTT value = 20 }
		  command = { type = trigger which = 77502 }
		  command = { type = local_setflag which = sud_ceasefire }
           }
           action_b = {
                  name = "No treaties with terrorists!"
                  ai_chance = 5
                  command = { type = relation which = OTT value = -20 }
		  command = { type = dissent value = 5 }
           }

}
#############################################
###### Humanitarian Ceasefire Agreement
#############################################
event = {
         id = 77502
         random = no
         country = OTT
 
         name = "EVT_77502_NAME"
         desc = "EVT_77502_DESC"
         style = 0
	 picture = "no"
 
           action_a = {
                  name = "Great"
                  command = { type = dissent value = -1 }
           }

}

#############################################
###### Militant attacks continue
#############################################
event = {
         id = 77503
         random = no
         country = SUD
         trigger = {
		atwar = no
		local_flag = sud_ceasefire

         }
 
         name = "EVT_77503_NAME"
         desc = "EVT_77503_DESC"
         style = 0
	 picture = "no"
 
         date = { day = 25 month = april year = 2004 }
 
           action_a = {
                  name = "We will stop them"
                  command = { type = dissent value = 2 }
           }

}
#############################################
###### Comprehensive Ceasefire Agreement
#############################################
event = {
         id = 77504
         random = no
         country = SUD
         trigger = {			
		   atwar = no
		   NOT = {
			exists = U24
		   }

         }
 
         name = "EVT_77504_NAME"
         desc = "EVT_77504_DESC"
         style = 0
	 picture = "sudan_cw"
 
         date = { day = 8 month = april year = 2004 }
 
           action_a = {
                  name = "Sign the cease fire agreement"
		  ai_chance = 100
                  command = { type = dissent value = -1 }
		  command = { type = belligerence value = -1 }
		  command = { type = relation which = ETH value = 10 }
		  command = { type = relation which = ENG value = 10 }
		  command = { type = relation which = USA value = 10 }
		  command = { type = local_setflag which = comp_ceasefire }
           }
           action_b = {
                  name = "No treaties with terrorists!"
                  ai_chance = 0
                  command = { type = independence which = U24 }
		  command = { type = dissent value = 5 }
		  command = { type = belligerence value = 3 }
		  command = { type = relation which = ETH value = -10 }
		  command = { type = relation which = ENG value = -10 }
		  command = { type = relation which = USA value = -10 }
		  command = { type = trigger which = 98501 }
           }

}
#############################################
###### Vice President dies in a plane crash
#############################################
event = {
         id = 77505
         random = no
         country = SUD
         trigger = {			
		   atwar = no
		   local_flag = comp_ceasefire
		   NOT = {
			exists = U24
		   }

         }
 
         name = "EVT_77505_NAME"
         desc = "EVT_77505_DESC"
         style = 0
	 picture = "no"
 
         date = { day = 29 month = july year = 2005 }
 
           action_a = {
                  name = "Oh no"
                  command = { type = dissent value = 2 }
           }

}
#############################################
###### Southern Sudanese independence referendum - independence
#############################################
event = {
         id = 77506
         random = no
         country = SUD
         trigger = {			
		   atwar = no
		   random = 70
		   local_flag = comp_ceasefire
		   NOT = {
			exists = U24
		   }

         }
 
         name = "EVT_77506_NAME"
         desc = "EVT_77506_DESC"
         style = 0
	 picture = "sudan_cw"
 
         date = { day = 3 month = january year = 2011 }
 
           action_a = {
                  name = "Ok"
		  command = { type = independence which = U24 }
		  command = { type = local_clrflag which = comp_ceasefire }
		  command = { type = trigger which = 98502 }
		  command = { type = sleepevent which = 77507 }
		  command = { type = domestic which = democratic value = 1 }
		  command = { type = belligerence value = -3 }

		  command = { type = removecore which = 1069 }
		  command = { type = removecore which = 1065 }
           }

}
#############################################
###### Southern Sudanese independence referendum - no independence
#############################################
event = {
         id = 77507
         random = no
         country = SUD
         trigger = {			
		   atwar = no
		   local_flag = comp_ceasefire
		   NOT = {
			exists = U24
		   }

         }
 
         name = "EVT_77507_NAME"
         desc = "EVT_77507_DESC"
         style = 0
	 picture = "sudan_cw"
 
         date = { day = 5 month = january year = 2011 }
 
           action_a = {
                  name = "Ok"
		  command = { type = dissent value = -1 }
		  command = { type = domestic which = democratic value = 1 }
		  command = { type = belligerence value = -3 }
           }

}