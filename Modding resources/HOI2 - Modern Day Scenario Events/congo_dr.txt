#########################################################
#							#
# Events for Democratic Republic of Congo (U11)         #
#                                			#
#########################################################

#########
# Index #
#########################################################
#From	- To	#	Name 			 
#########################################################
#20501 - 20800# Main events
#20801 - 20950# Political events
#20951 - 21000# Election events	 		
#########################################################

############################################################################################################################

#############################################
###### Main events
#############################################

#############################################
###### Transitional Government of DR Congo
#############################################
event = {
	id = 20501
	random = no
	country = U11
	trigger = {
		war = { country = U11 country = RSI }
		war = { country = U11 country = U18 }

	}

	name = "EVT_20501_NAME"
	desc = "EVT_20501_DESC"
	style = 0
	picture = "congowar"

	date = { day = 2 month = july year = 2003 }

	action_a = {
		name = "Form the transitional government"
		ai_chance = 99
		command = { type = peace which = RSI }
		command = { type = peace which = U18 }
		command = { type = trigger which = 20502 }
		command = { type = trigger which = 20503 }
		command = { type = foreignminister which = 20520 }
		command = { type = armamentminister which = 20523 }
		command = { type = ministerofsecurity which = 20525 }
	}
	action_b = {
		name = "No transitional government, we will continue to fight!"
		ai_chance = 1
		command = { type = dissent value = 10 }
		command = { type = relation which = USA value = -100 }
		command = { type = relation which = FRA value = -100 }
		command = { type = relation which = GER value = -100 }
		command = { type = relation which = ENG value = -100 }
	}

}
#############################################
###### Transitional Government of DR Congo formed
#############################################
event = {
	id = 20502
	random = no
	country = RSI

	name = "EVT_20502_NAME"
	desc = "EVT_20502_DESC"
	style = 0
	picture = "congowar"

	action_a = {
		name = "Good"
		command = { }
	}

}
#############################################
###### Transitional Government of DR Congo formed
#############################################
event = {
	id = 20503
	random = no
	country = U18

	name = "EVT_20503_NAME"
	desc = "EVT_20503_DESC"
	style = 0
	picture = "congowar"

	action_a = {
		name = "Good"
		command = { }
	}

}
