#########################################################
#							#
# Events for Armenia (ARM) by Praetyre and Zokan        #
#                                			#
#########################################################

#########
# Index #
#########################################################
#From	- To	#	Name 			 
#########################################################
#3001 - 3300# Main events
#3301 - 3450# Political events
#3451 - 3500# Election events	 		
#########################################################

############################################################################################################################

#############################################
###### Main events
#############################################

#############################################
###### Vahan Aroyan detained
#############################################
event = {
         id = 3001
         random = no
         country = ARM
         trigger = {
                   headofstate = 3001
                   atwar = no

         }
 
         name = "EVT_3001_NAME"
         desc = "EVT_3001_DESC"
         style = 0
	 picture = "no"
 
         date = { day = 1 month = january year = 2007 }
 
           action_a = {
                  name = "Ok"
                  command = { type = dissent value = 2 }
           }

}
#############################################
###### Death of Hrant Dink
#############################################
event = {
         id = 3002
         random = no
         country = TUR
         trigger = {
                   atwar = no
		   exists = ARM

         }
 
         name = "EVT_3002_NAME"
         desc = "EVT_3002_DESC"
         style = 0
	 picture = "no"
 
         date = { day = 19 month = january year = 2007 }
 
           action_a = {
                  name = "Not good"
                  command = { type = relation which = ARM value = -5 }
		  command = { type = trigger which = 3003 }
           }

}
#############################################
###### Death of Hrant Dink
#############################################
event = {
         id = 3003
         random = no
         country = ARM
 
         name = "EVT_3003_NAME"
         desc = "EVT_3003_DESC"
         style = 0
	 picture = "no"
 
           action_a = {
                  name = "Not good"
                  command = { type = relation which = TUR value = -5 }
           }

}
#############################################
###### Ogun Samast arrested
#############################################
event = {
         id = 3004
         random = no
         country = TUR
         trigger = {
                   atwar = no
		   event = 3002
		   exists = ARM

         }
 
         name = "EVT_3004_NAME"
         desc = "EVT_3004_DESC"
         style = 0
	 picture = "no"
 
         date = { day = 20 month = january year = 2007 }
 
           action_a = {
                  name = "Good"
		  command = { type = trigger which = 3005 }
           }

}
#############################################
###### Ogun Samast arrested
#############################################
event = {
         id = 3005
         random = no
         country = ARM
 
         name = "EVT_3005_NAME"
         desc = "EVT_3005_DESC"
         style = 0
	 picture = "no"
 
           action_a = {
                  name = "Good"
                  command = { type = relation which = TUR value = 5 }
           }

}
#############################################
###### Mass mournings of Hrant Dink
#############################################
event = {
         id = 3006
         random = no
         country = TUR
         trigger = {
                   atwar = no
		   event = 3002

         }
 
         name = "EVT_3006_NAME"
         desc = "EVT_3006_DESC"
         style = 0
	 picture = "no"
 
         date = { day = 24 month = january year = 2007 }
 
           action_a = {
                  name = "Ok"
		  command = { type = dissent value = 1 }
           }

}
#############################################
###### US stance on the Armenian genocide
#############################################
event = {
         id = 3007
         random = no
         country = USA
         trigger = {
                   atwar = no
		   government = democratic

         }
 
         name = "EVT_3007_NAME"
         desc = "EVT_3007_DESC"
         style = 0
	 picture = "no"
 
         date = { day = 29 month = january year = 2007 }
 
           action_a = {
                  name = "Oppose the Armenian genocide bill"
		  ai_chance = 90
		  command = { type = trigger which = 3008 }
		  command = { type = trigger which = 3009 }
		  command = { type = relation which = ARM value = -10 }
		  command = { type = relation which = TUR value = 5 }
		  command = { type = sleepevent which = 3020 }
           }
	   action_b = {
                  name = "Support the Armenian genocide bill"
		  ai_chance = 10
		  command = { type = trigger which = 3010 }
		  command = { type = trigger which = 3011 }
		  command = { type = relation which = ARM value = 20 }
		  command = { type = relation which = TUR value = -20 }
		  command = { type = sleepevent which = 3017 }
           }

}
#############################################
###### US opposes the Armenian Genocide bill
#############################################
event = {
         id = 3008
         random = no
         country = ARM
 
         name = "EVT_3008_NAME"
         desc = "EVT_3008_DESC"
         style = 0
	 picture = "no"
 
           action_a = {
                  name = "They are wrong"
		  command = { type = relation which = USA value = -20 }
           }

}
#############################################
###### US opposes the Armenian Genocide bill
#############################################
event = {
         id = 3009
         random = no
         country = TUR
 
         name = "EVT_3009_NAME"
         desc = "EVT_3009_DESC"
         style = 0
	 picture = "no"
 
           action_a = {
                  name = "Good"
		  command = { type = relation which = USA value = 5 }
           }

}
#############################################
###### US supports the Armenian Genocide bill
#############################################
event = {
         id = 3010
         random = no
         country = ARM
 
         name = "EVT_3010_NAME"
         desc = "EVT_3010_DESC"
         style = 0
	 picture = "no"
 
           action_a = {
                  name = "Excellent"
		  command = { type = relation which = USA value = 20 }
           }

}
#############################################
###### US supports the Armenian Genocide bill
#############################################
event = {
         id = 3011
         random = no
         country = TUR
 
         name = "EVT_3011_NAME"
         desc = "EVT_3011_DESC"
         style = 0
	 picture = "no"
 
           action_a = {
                  name = "This will harm our relations"
		  command = { type = relation which = USA value = -30 }
           }

}
#############################################
###### Dogu Perincek convicted of genocide denial
#############################################
event = {
         id = 3012
         random = no
         country = SCH
         trigger = {
                   atwar = no
		   government = democratic

         }
 
         name = "EVT_3012_NAME"
         desc = "EVT_3012_DESC"
         style = 0
	 picture = "no"
 
         date = { day = 9 month = march year = 2007 }
 
           action_a = {
                  name = "Ok"
		  command = { type = trigger which = 3013 }
		  command = { type = relation which = ARM value = 5 }
           }

}
#############################################
###### Dogu Perincek convicted of genocide denial
#############################################
event = {
         id = 3013
         random = no
         country = ARM
 
         name = "EVT_3013_NAME"
         desc = "EVT_3013_DESC"
         style = 0
	 picture = "no"
 
           action_a = {
                  name = "Good"
		  command = { type = relation which = SCH value = 5 }
           }

}
#############################################
###### Prosperous Armenia offices bombed
#############################################
event = {
         id = 3014
         random = no
         country = ARM
         trigger = {
                   atwar = no
		   headofstate = 3001

         }
 
         name = "EVT_3014_NAME"
         desc = "EVT_3014_DESC"
         style = 0
	 picture = "no"
 
         date = { day = 16 month = april year = 2007 }
 
           action_a = {
                  name = "Not good"
		  command = { type = dissent value = 1 }
           }

}
#############################################
###### Turkey recalls its ambassador to US
#############################################
event = {
         id = 3015
         random = no
         country = TUR
         trigger = {
                   atwar = no
		   government = democratic

         }
 
         name = "EVT_3015_NAME"
         desc = "EVT_3015_DESC"
         style = 0
	 picture = "no"
 
         date = { day = 11 month = october year = 2007 }
 
           action_a = {
                  name = "Ok"
		  command = { type = relation which = USA value = -10 }
		  command = { type = trigger which = 3016 }
           }

}
#############################################
###### Turkey recalls its ambassador to US
#############################################
event = {
         id = 3016
         random = no
         country = USA
 
         name = "EVT_3016_NAME"
         desc = "EVT_3016_DESC"
         style = 0
	 picture = "no"
 
           action_a = {
                  name = "Their loss"
		  command = { type = relation which = TUR value = -10 }
           }

}
#############################################
###### Supporters of genocide resolution drop vote calls
#############################################
event = {
         id = 3017
         random = no
         country = USA
         trigger = {
                   atwar = no
		   government = democratic
		   event = 3007

         }
 
         name = "EVT_3017_NAME"
         desc = "EVT_3017_DESC"
         style = 0
	 picture = "no"
 
         date = { day = 25 month = october year = 2007 }
 
           action_a = {
                  name = "Ok"
		  command = { type = relation which = ARM value = -5 }
		  command = { type = relation which = TUR value = 5 }
		  command = { type = trigger which = 3018 }
		  command = { type = trigger which = 3019 }
           }

}
#############################################
###### Supporters of genocide resolution drop vote calls
#############################################
event = {
         id = 3018
         random = no
         country = ARM
 
         name = "EVT_3018_NAME"
         desc = "EVT_3018_DESC"
         style = 0
	 picture = "no"
 
           action_a = {
                  name = "Ok"
		  command = { type = relation which = USA value = -5 }
           }

}
#############################################
###### Supporters of genocide resolution drop vote calls
#############################################
event = {
         id = 3019
         random = no
         country = TUR
 
         name = "EVT_3019_NAME"
         desc = "EVT_3019_DESC"
         style = 0
	 picture = "no"
 
           action_a = {
                  name = "Ok"
		  command = { type = relation which = USA value = 5 }
           }

}
#############################################
###### Congress passes the Armenian Genocide resolution
#############################################
event = {
         id = 3020
         random = no
         country = USA
         trigger = {
                   atwar = no
		   government = democratic
		   event = 3007

         }
 
         name = "EVT_3020_NAME"
         desc = "EVT_3020_DESC"
         style = 0
	 picture = "no"
 
         date = { day = 25 month = october year = 2007 }
 
           action_a = {
                  name = "Ok"
		  command = { type = relation which = ARM value = 20 }
		  command = { type = relation which = TUR value = -20 }
		  command = { type = trigger which = 3021 }
		  command = { type = trigger which = 3021 }
           }

}
#############################################
###### US Congress passes the Armenian Genocide resolution
#############################################
event = {
         id = 3021
         random = no
         country = ARM
 
         name = "EVT_3021_NAME"
         desc = "EVT_3021_DESC"
         style = 0
	 picture = "no"
 
           action_a = {
                  name = "Ok"
		  command = { type = relation which = USA value = 20 }
           }

}
#############################################
###### US Congress passes the Armenian Genocide resolution
#############################################
event = {
         id = 3022
         random = no
         country = TUR
 
         name = "EVT_3022_NAME"
         desc = "EVT_3022_DESC"
         style = 0
	 picture = "no"
 
           action_a = {
                  name = "Ok"
		  command = { type = relation which = USA value = -20 }
           }

}
#############################################
###### Election protests
#############################################
event = {
         id = 3023
         random = no
         country = ARM
         trigger = {
                   atwar = no
		   event = 3451

         }
 
         name = "EVT_3023_NAME"
         desc = "EVT_3023_DESC"
         style = 0
	 picture = "no"
 
         date = { day = 20 month = february year = 2008 }
 
           action_a = {
                  name = "Not good"
		  command = { type = dissent value = 10 }
           }

}



#############################################
###### Political events
#############################################

#############################################
###### Andranik Magaryan dies
#############################################
event = {
         id = 3301
         random = no
         country = ARM
         trigger = {
                   headofgovernment = 3002

         }
 
         name = "EVT_3301_NAME"
         desc = "EVT_3301_DESC"
         style = 0
	 picture = "no"
 
         date = { day = 25 month = january year = 2007 }
 
           action_a = {
                  name = "Rest in peace"
		  command = { type = sleepminister which = 3002 }
		  command = { type = headofgovernment which = 3012 }
		  command = { type = armamentminister which = 3014 }
           }

}



#############################################
###### Election events
#############################################

#############################################
###### 2008 Armenian presidential election
#############################################
event = {
         id = 3451
         random = no
         country = ARM
         trigger = {
                   headofstate = 3001
                   atwar = no

         }
 
         name = "EVT_3451_NAME"
         desc = "EVT_3451_DESC"
         style = 0
	 picture = "elections"
 
         date = { day = 19 month = february year = 2008 }
 
           action_a = {
                  name = "Serzh Sargsyan - Republican Party of Armenia"
		  ai_chance = 100
                  command = { type = headofstate = 3011 }
		  command = { type = headofgovernment = 3013 }
		  command = { type = foreignminister = 3018 }
		  command = { type = armamentminister = 3015 }
		  command = { type = ministerofsecurity which = 3019 }
		  command = { type = chiefofstaff which = 3032 }
		  command = { type = set_domestic which = democratic value = 7 }
		  command = { type = set_domestic which = political_left value = 2 }
		  command = { type = set_domestic which = freedom value = 5 }
		  command = { type = set_domestic which = free_market value = 6 }
           }
	   action_b = {
                  name = "Levon Ter-Petrossian - independent"
		  ai_chance = 0
                  command = { type = headofstate = 3037 }
		  command = { type = headofgovernment = 3038 }
		  command = { type = foreignminister = 3027 }
		  command = { type = armamentminister = 3015 }
		  command = { type = set_domestic which = democratic value = 8 }
		  command = { type = set_domestic which = political_left value = 3 }
		  command = { type = set_domestic which = freedom value = 7 }
		  command = { type = set_domestic which = free_market value = 7 }
		  command = { type = sleepevent which = 3023 }
           }
	   action_c = {
                  name = "Artur Baghdasarian - Rule of Law"
		  ai_chance = 0
                  command = { type = headofstate = 3020 }
		  command = { type = headofgovernment = 3038 }
		  command = { type = foreignminister = 3023 }
		  command = { type = armamentminister = 3015 }
		  command = { type = set_domestic which = democratic value = 8 }
		  command = { type = set_domestic which = political_left value = 6 }
		  command = { type = set_domestic which = freedom value = 8 }
		  command = { type = set_domestic which = free_market value = 8 }
		  command = { type = sleepevent which = 3023 }
           }

}