#########################################################
#							#
# Events for Transamur Republic (U32)                   #
#                                			#
#########################################################

#########
# Index #
#########################################################
#From	- To	#	Name 			 
#########################################################
#97001 - 97300# Main events
#97301 - 97450# Political events
#97451 - 97500# Election events	 		
#########################################################

############################################################################################################################

#############################################
###### Main events
#############################################

#############################################
###### Foreign policy direction
#############################################
event = {
	id = 97001
	random = no
	country = U32
	trigger = {
		atwar = no
		exists = JAP
		exists = CHC
		NOT = {
			war = { country = JAP country = CHC }
			war = { country = JAP country = KOR }
			war = { country = JAP country = U09 }
			war = { country = JAP country = USA }
			war = { country = USA country = CHC }
			war = { country = RUS country = CHC }
			war = { country = IND country = CHC }
		}

	}

	name = "EVT_97001_NAME"
	desc = "EVT_97001_DESC"
	style = 0
	picture = "no"

	date = { day = 1 month = january year = 2004 }
	offset = 25 # Check for this every 25 days
	deathdate = { day = 30 month = december year = 2020 }

	action_a = {
		name = "Focus on China"
		ai_chance = 20
		command = { type = setflag which = transamur_china }
		command = { type = relation which = CHC value = 20 }
	}
	action_b = {
		name = "Focus on Japan"
		ai_chance = 80
		command = { type = setflag which = transamur_japan }
		command = { type = relation which = JAP value = 20 }
	}

}
#############################################
###### Foreign policy initiative yields results - China
#############################################
event = {
	id = 97002
	random = no
	country = U32
	trigger = {
		atwar = no
		flag = transamur_china
		exists = CHC
		NOT = {
			war = { country = JAP country = CHC }
			war = { country = USA country = CHC }
			war = { country = RUS country = CHC }
			war = { country = IND country = CHC }
		}

	}

	name = "EVT_97002_NAME"
	desc = "EVT_97002_DESC"
	style = 0
	picture = "no"

	date = { day = 1 month = january year = 2004 }
	offset = 360
	deathdate = { day = 30 month = december year = 2020 }

	action_a = {
		name = "Excellent"
		command = { type = relation which = CHC value = 30 }
		command = { type = construct which = ic where = 1528 value = 1 }
		command = { type = construct which = ic where = 1367 value = 1 }
	}

}
#############################################
###### Foreign policy initiative yields results - Japan
#############################################
event = {
	id = 97003
	random = no
	country = U32
	trigger = {
		atwar = no
		flag = transamur_japan
		exists = JAP
		NOT = {
			war = { country = JAP country = CHC }
			war = { country = JAP country = KOR }
			war = { country = JAP country = U09 }
			war = { country = JAP country = USA }
		}

	}

	name = "EVT_97003_NAME"
	desc = "EVT_97003_DESC"
	style = 0
	picture = "no"

	date = { day = 1 month = january year = 2004 }
	offset = 360
	deathdate = { day = 30 month = december year = 2020 }

	action_a = {
		name = "Excellent"
		command = { type = relation which = JAP value = 30 }
		command = { type = research_mod value = 3 }
		command = { type = construct which = ic where = 1369 value = 1 }
	}

}
