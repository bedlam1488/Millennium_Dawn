#########################################################
#							#
# Events for NATO military alliance                     #
# 100001-100500                                         #
# 			                                #
#########################################################

############################################################################################################################

#############################################
###### Estonia joins NATO
#############################################
event = {
         id = 100001
         random = no
         country = EST
         trigger = {
                   atwar = no
                   government = democratic
                   NOT = { 
                   	war = { country = RUS country = USA }
			alliance = { country = EST country = USA }
			alliance = { country = EST country = U06 }
		   }


         }
 
         name = "EVT_100001_NAME"
         desc = "EVT_100001_DESC"
         style = 0
         picture = "nato"
 
         date = { day = 28 month = march year = 2004 }
 
           action_a = {
                  name = "Ok"
		  command = { type = dissent value = -2 }
                  command = { type = relation which = USA value = 20 }
                  command = { type = relation which = GER value = 20 }
                  command = { type = relation which = FRA value = 20 }
                  command = { type = relation which = ENG value = 20 }
		  command = { type = relation which = RUS value = -20 }
		  command = { type = alliance which = U06 }
                  command = { type = trigger which = 100002 }
           }

}
#############################################
###### Estonia joins NATO
#############################################
event = {
         id = 100002
         random = no
         country = USA
 
         name = "EVT_100002_NAME"
         desc = "EVT_100002_DESC"
         style = 0
 
           action_a = {
                  name = "Ok"
                  command = { type = guarantee which = USA where = EST }
		  command = { type = access which = EST }
           }

}
#############################################
###### Latvia joins NATO
#############################################
event = {
         id = 100003
         random = no
         country = LAT
         trigger = {
                   atwar = no
                   government = democratic
                            NOT = { 
                                     war = { country = RUS country = USA }
				     alliance = { country = LAT country = USA }
			alliance = { country = LAT country = U06 }
				  }


         }
 
         name = "EVT_100003_NAME"
         desc = "EVT_100003_DESC"
         style = 0
         picture = "nato"
 
         date = { day = 28 month = march year = 2004 }
 
           action_a = {
                  name = "Ok"
	 	  command = { type = dissent value = -2 }
                  command = { type = relation which = USA value = 20 }
                  command = { type = relation which = GER value = 20 }
                  command = { type = relation which = FRA value = 20 }
                  command = { type = relation which = ENG value = 20 }
		  command = { type = relation which = RUS value = -20 }
		  command = { type = alliance which = U06 }
                  command = { type = trigger which = 100004 }
           }

}
#############################################
###### Latvia joins NATO
#############################################
event = {
         id = 100004
         random = no
         country = USA
 
         name = "EVT_100004_NAME"
         desc = "EVT_100004_DESC"
         style = 0
 
           action_a = {
                  name = "Ok"
                  command = { type = guarantee which = USA where = LAT }
		  command = { type = access which = LAT }
           }

}
#############################################
###### Lithuania joins NATO
#############################################
event = {
         id = 100005
         random = no
         country = LIT
         trigger = {
                   atwar = no
                   government = democratic
                            NOT = { 
                                     war = { country = RUS country = USA }
				     alliance = { country = LIT country = USA }
			alliance = { country = LIT country = U06 }
				  }


         }
 
         name = "EVT_100005_NAME"
         desc = "EVT_100005_DESC"
         style = 0
         picture = "nato"
 
         date = { day = 28 month = march year = 2004 }
 
           action_a = {
                  name = "Ok"
		  command = { type = dissent value = -2 }
                  command = { type = relation which = USA value = 20 }
                  command = { type = relation which = GER value = 20 }
                  command = { type = relation which = FRA value = 20 }
                  command = { type = relation which = ENG value = 20 }
		  command = { type = relation which = RUS value = -20 }
		  command = { type = alliance which = U06 }
                  command = { type = trigger which = 100006 }
           }

}
#############################################
###### Lithuania joins NATO
#############################################
event = {
         id = 100006
         random = no
         country = USA
 
         name = "EVT_100006_NAME"
         desc = "EVT_100006_DESC"
         style = 0
 
           action_a = {
                  name = "Ok"
                  command = { type = guarantee which = USA where = LIT }
		  command = { type = access which = LIT }
           }

}
#############################################
###### Slovenia joins NATO
#############################################
event = {
         id = 100007
         random = no
         country = SLV
         trigger = {
                   atwar = no
                   government = democratic
                            NOT = { 
                                     war = { country = RUS country = USA }
				alliance = { country = SLV country = USA }
			alliance = { country = SLV country = U06 }
				  }


         }
 
         name = "EVT_100007_NAME"
         desc = "EVT_100007_DESC"
         style = 0
         picture = "nato"
 
         date = { day = 28 month = march year = 2004 }
 
           action_a = {
                  name = "Ok"
		  command = { type = dissent value = -2 }
                  command = { type = relation which = USA value = 20 }
                  command = { type = relation which = GER value = 20 }
                  command = { type = relation which = FRA value = 20 }
                  command = { type = relation which = ENG value = 20 }
		  command = { type = relation which = RUS value = -20 }
		  command = { type = alliance which = U06 }
                  command = { type = trigger which = 100008 }
           }

}
#############################################
###### Slovenia joins NATO
#############################################
event = {
         id = 100008
         random = no
         country = USA
 
         name = "EVT_100008_NAME"
         desc = "EVT_100008_DESC"
         style = 0
 
           action_a = {
                  name = "Ok"
                  command = { type = guarantee which = USA where = SLV }
		  command = { type = access which = SLV }
           }

}
#############################################
###### Slovakia joins NATO
#############################################
event = {
         id = 100009
         random = no
         country = SLO
         trigger = {
                   atwar = no
                   government = democratic
                            NOT = { 
                                     war = { country = RUS country = USA }
					alliance = { country = SLO country = USA }
			alliance = { country = SLO country = U06 }
				  }


         }
 
         name = "EVT_100009_NAME"
         desc = "EVT_100009_DESC"
         style = 0
         picture = "nato"
 
         date = { day = 28 month = march year = 2004 }
 
           action_a = {
                  name = "Ok"
		  command = { type = dissent value = -2 }
                  command = { type = relation which = USA value = 20 }
                  command = { type = relation which = GER value = 20 }
                  command = { type = relation which = FRA value = 20 }
                  command = { type = relation which = ENG value = 20 }
		  command = { type = relation which = RUS value = -20 }
		  command = { type = alliance which = U06 }
                  command = { type = trigger which = 100010 }
           }

}
#############################################
###### Slovakia joins NATO
#############################################
event = {
         id = 100010
         random = no
         country = USA
 
         name = "EVT_100010_NAME"
         desc = "EVT_100010_DESC"
         style = 0
 
           action_a = {
                  name = "Ok"
                  command = { type = guarantee which = USA where = SLO }
		  command = { type = access which = SLO }
           }

}
#############################################
###### Bulgaria joins NATO
#############################################
event = {
         id = 100011
         random = no
         country = BUL
         trigger = {
                   atwar = no
                   government = democratic
                            NOT = { 
                                     war = { country = RUS country = USA }
				alliance = { country = BUL country = USA }
			alliance = { country = BUL country = U06 }
				  }


         }
 
         name = "EVT_100011_NAME"
         desc = "EVT_100011_DESC"
         style = 0
         picture = "nato"
 
         date = { day = 28 month = march year = 2004 }
 
           action_a = {
                  name = "Ok"
		  command = { type = dissent value = -2 }
                  command = { type = relation which = USA value = 20 }
                  command = { type = relation which = GER value = 20 }
                  command = { type = relation which = FRA value = 20 }
                  command = { type = relation which = ENG value = 20 }
		  command = { type = relation which = RUS value = -20 }
		  command = { type = alliance which = U06 }
                  command = { type = trigger which = 100012 }
           }

}
#############################################
###### Bulgaria joins NATO
#############################################
event = {
         id = 100012
         random = no
         country = USA
 
         name = "EVT_100012_NAME"
         desc = "EVT_100012_DESC"
         style = 0
 
           action_a = {
                  name = "Ok"
                  command = { type = guarantee which = USA where = BUL }
		  command = { type = access which = BUL }
           }

}
#############################################
###### Romania joins NATO
#############################################
event = {
         id = 100013
         random = no
         country = ROM
         trigger = {
                   atwar = no
                   government = democratic
                            NOT = { 
                                     war = { country = RUS country = USA }
				alliance = { country = ROM country = USA }
			alliance = { country = ROM country = U06 }
				  }


         }
 
         name = "EVT_100013_NAME"
         desc = "EVT_100013_DESC"
         style = 0
         picture = "nato"
 
         date = { day = 28 month = march year = 2004 }
 
           action_a = {
                  name = "Ok"
		  command = { type = dissent value = -2 }
                  command = { type = relation which = USA value = 20 }
                  command = { type = relation which = GER value = 20 }
                  command = { type = relation which = FRA value = 20 }
                  command = { type = relation which = ENG value = 20 }
		  command = { type = relation which = RUS value = -20 }
		  command = { type = alliance which = U06 }
                  command = { type = trigger which = 100014 }
           }

}
#############################################
###### Romania joins NATO
#############################################
event = {
         id = 100014
         random = no
         country = USA
 
         name = "EVT_100014_NAME"
         desc = "EVT_100014_DESC"
         style = 0
 
           action_a = {
                  name = "Ok"
                  command = { type = guarantee which = USA where = ROM }
		  command = { type = access which = ROM }
           }

}
#############################################
###### Croatia joins NATO
#############################################
event = {
         id = 100015
         random = no
         country = CRO
         trigger = {
                   atwar = no
                   government = democratic
                            NOT = { 
                                     war = { country = RUS country = USA }
				alliance = { country = CRO country = USA }
			alliance = { country = CRO country = U06 }
				  }


         }
 
         name = "EVT_100015_NAME"
         desc = "EVT_100015_DESC"
         style = 0
         picture = "nato"
 
         date = { day = 3 month = april year = 2009 }
 
           action_a = {
                  name = "Ok"
		  command = { type = dissent value = -2 }
                  command = { type = relation which = USA value = 20 }
                  command = { type = relation which = GER value = 20 }
                  command = { type = relation which = FRA value = 20 }
                  command = { type = relation which = ENG value = 20 }
		  command = { type = relation which = RUS value = -20 }
		  command = { type = alliance which = U06 }
                  command = { type = trigger which = 100016 }
           }

}
#############################################
###### Croatia joins NATO
#############################################
event = {
         id = 100016
         random = no
         country = USA
 
         name = "EVT_100016_NAME"
         desc = "EVT_100016_DESC"
         style = 0
 
           action_a = {
                  name = "Ok"
                  command = { type = guarantee which = USA where = CRO }
		  command = { type = access which = CRO }
           }

}
#############################################
###### Albania joins NATO
#############################################
event = {
         id = 100017
         random = no
         country = ALB
         trigger = {
                   atwar = no
                   government = democratic
                            NOT = { 
                                     war = { country = RUS country = USA }
				alliance = { country = ALB country = USA }
			alliance = { country = ALB country = U06 }
				  }


         }
 
         name = "EVT_100017_NAME"
         desc = "EVT_100017_DESC"
         style = 0
         picture = "nato"
 
         date = { day = 3 month = april year = 2009 }
 
           action_a = {
                  name = "Ok"
		  command = { type = dissent value = -2 }
                  command = { type = relation which = USA value = 20 }
                  command = { type = relation which = GER value = 20 }
                  command = { type = relation which = FRA value = 20 }
                  command = { type = relation which = ENG value = 20 }
		  command = { type = relation which = RUS value = -20 }
		  command = { type = alliance which = U06 }
                  command = { type = trigger which = 100018 }
           }

}
#############################################
###### Albania joins NATO
#############################################
event = {
         id = 100018
         random = no
         country = USA
 
         name = "EVT_100018_NAME"
         desc = "EVT_100018_DESC"
         style = 0
 
           action_a = {
                  name = "Ok"
                  command = { type = guarantee which = USA where = ALB }
		  command = { type = access which = ALB }
           }

}
#############################################
###### Macedonia joins NATO
#############################################
event = {
         id = 100019
         random = no
         country = SCA
         trigger = {
                   atwar = no
                   government = democratic
                            NOT = { 
                                     war = { country = RUS country = USA }
				alliance = { country = SCA country = USA }
			alliance = { country = SCA country = U06 }
				  }


         }
 
         name = "EVT_100019_NAME"
         desc = "EVT_100019_DESC"
         style = 0
         picture = "nato"
 
         date = { day = 3 month = april year = 2009 }
 
           action_a = {
                  name = "Ok"
		  command = { type = dissent value = -2 }
                  command = { type = relation which = USA value = 20 }
                  command = { type = relation which = GER value = 20 }
                  command = { type = relation which = FRA value = 20 }
                  command = { type = relation which = ENG value = 20 }
		  command = { type = relation which = RUS value = -20 }
		  command = { type = alliance which = U06 }
                  command = { type = trigger which = 100020 }
           }

}
#############################################
###### Macedonia joins NATO
#############################################
event = {
         id = 100020
         random = no
         country = USA
 
         name = "EVT_100020_NAME"
         desc = "EVT_100020_DESC"
         style = 0
 
           action_a = {
                  name = "Ok"
                  command = { type = guarantee which = USA where = SCA }
		  command = { type = access which = SCA }
           }

}
#############################################
###### Macedonian NATO membership
#############################################
event = {
         id = 100021
         random = no
         country = GRE
         trigger = {
                   atwar = no
		   exists = SCA
                            NOT = { 
                                     war = { country = RUS country = USA }
					war = { country = GRE country = SCA }
				alliance = { country = SCA country = USA }
			alliance = { country = SCA country = U06 }
			alliance = { country = SCA country = GRE }
				  }


         }
 
         name = "EVT_100021_NAME"
         desc = "EVT_100021_DESC"
         style = 0
         picture = "nato"
 
         date = { day = 3 month = april year = 2008 }
 
           action_a = {
                  name = "Refuse Macedonia as a NATO member"
		  ai_chance = 99
		  command = { type = dissent value = -1 }
                  command = { type = relation which = SCA value = -50 }
                  command = { type = relation which = GER value = -20 }
                  command = { type = relation which = FRA value = -20 }
                  command = { type = relation which = ENG value = -20 }
		  command = { type = relation which = USA value = -20 }
                  command = { type = sleepevent which = 100019 }
		  command = { type = sleepevent which = 100020 }
           }
	   action_b = {
                  name = "Allow them in, we'll settle the dispute later"
		  ai_chance = 1
		  command = { type = dissent value = 3 }
                  command = { type = relation which = SCA value = 50 }
		  command = { type = domestic which = freedom value = 1 }
           }

}