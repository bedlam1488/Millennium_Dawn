ideas = {
	country = {
		#### start
		TRK_corruption = {
			allowed_civil_war = { always = yes }
			picture = puppet_industry
			modifier = {
				corruption_cost_factor = 0.25
				local_resources_factor = 0.10
				political_power_gain = -0.10
				neutrality_drift = 0.10
				stability_factor = -0.3
				tax_gain_multiplier_modifier = -0.2
				economic_cycles_cost_factor = 0.5				
			}
		}
		TRK_low_army = { 
			allowed_civil_war = { always = yes }
			picture = disorganized_army
			modifier = {
				army_attack_factor = -0.10
				army_defence_factor = -0.10
				industrial_capacity_factory = -0.25			
			}
		}
		TRK_shelkov_put = { 
			allowed_civil_war = { always = yes }
			picture = map_turkmen
			modifier = {
				local_resources_factor = 0.10
				stability_factor = 0.1		
			}
		}
		TRK_zoomer_closed = { 
			allowed_civil_war = { always = yes }
			picture = turkmen_zoomer_closed
			modifier = {
				stability_factor = -0.1
				MONTHLY_POPULATION = -0.25	
			}
		}
		TRK_turkmen_police = { 
			allowed_civil_war = { always = yes }
			picture = turkmen_police
			modifier = {
				stability_factor = 0.1
		    	crime_fighting_cost_factor = 0.5
			}
		}
		TRK_turkmen_neitalitet = { 
			allowed_civil_war = { always = yes }
			picture = neutrality
			modifier = {
			    justify_war_goal_time = 0.90
		    	Foreign_Intervention_Law_cost_factor = 2
			}
		}	
		TRK_goodbay_worker = { 
			allowed_civil_war = { always = yes }
			picture = migrant_crisis_europe
			modifier = {
		    	stability_factor = -0.05
				MONTHLY_POPULATION = -0.25				
			}			
		}
		TRK_smi_closed = { 
			allowed_civil_war = { always = yes }
			picture = political_censorship
			modifier = {
				democratic_drift = -0.02
				communism_drift = -0.02
				stability_factor = -0.02
			}						
		}
		TRK_decommunisation = { 
			allowed_civil_war = { always = yes }
			picture = anti_soviet_pact
			modifier = {
			    war_support_factor = 0.05
				communism_drift = -0.02
				stability_factor = 0.05
			}						
		}
		TRK_bitoyoi_nazi = { 
			allowed_civil_war = { always = yes }
			picture = morale_bonus
			modifier = {
			    war_support_factor = 0.05
				nationalist_drift = 0.01
				stability_factor = 0.05
				army_morale_factor = 0.05
				ground_attack_factor = 0.03				
			}						
		}
		TRK_assimiliacia = { 
			allowed_civil_war = { always = yes }
			picture = turkmen_unit
			modifier = {
			    war_support_factor = 0.05
				stability_factor = 0.10
				nationalist_drift = 0.01
                conscription_factor = 0.10			
			}						
		}
		TRK_civilwar_baff = { 
			allowed_civil_war = { always = yes }
			picture = defense
			modifier = {
        		army_attack_factor = -0.2	
				army_core_defence_factor = 0.8
			}						
		}	
		TRK_civilwar_baff_niyazov = { 
			allowed_civil_war = { always = yes }
			picture = national_mobilization
			modifier = {
        		army_attack_factor = 0.5	
				army_core_defence_factor = 0.5
			}						
		}
		TRK_russia_language = { 
			allowed_civil_war = { always = yes }
			picture = turkmen_russia_people
			modifier = {
        		monthly_population = 0.05
				communism_drift = 0.01
			}						
		}
		TRK_escape_from_turkmen = { 
			allowed_civil_war = { always = yes }
			picture = turkmen_uzbek_angry
			modifier = {
        		monthly_population = -0.09
			}						
		}
		TRK_opposition_rebellion = {  
			allowed_civil_war = { always = yes }
			picture = turkmen_people_opposition
			modifier = {
        		monthly_population = -0.3
                production_speed_buildings_factor = -0.1
                war_support_factor = -0.2
                stability_weekly  = -0.3
                political_power_factor = -0.1                				
			}						
		}
		TRK_border_closedsss = {  
			allowed_civil_war = { always = yes }
			picture = turkmen_borderclosed
			modifier = {
                stability_weekly  = 0.02
				army_defence_factor = 0.05
				conscription_factor = -0.05
				justify_war_goal_time = 0.05                              				
			}						
		}
		TRK_big_build_plan = {  
			allowed_civil_war = { always = yes }
			picture = resource_production
			modifier = {
                stability_weekly  = 0.01
                production_speed_buildings_factor = 0.2
                production_speed_industrial_complex_factor = 0.2 				
			}						
		}
		TRK_great_khan = {  
			allowed_civil_war = { always = yes }
			picture = ottoman_spirit
			modifier = {
                stability_weekly  = 0.05
                army_attack_factor = 0.1
                army_core_attack_factor = 0.15
                army_attack_speed_factor = 0.1				
			}						
		}
		TRK_arkadag_vmeste = { 
			allowed_civil_war = { always = yes }
			picture = pos_braindr
			modifier = {
			stability_factor = 0.1
			monthly_population = 0.05				
			}			
		}
		TRK_separatisma_netu_niyazov = {  
			allowed_civil_war = { always = yes }
			picture = ottoman_spirit
			modifier = {
                army_core_defence_factor  = 0.05				
			}						
		}
		TRK_volontures_group_afgan = {  
			allowed_civil_war = { always = yes }
			picture = idea_por_volunteers_in_the_war3
			modifier = {
                send_volunteers_tension = -0.9
               	army_attack_factor = 0.05			
			}						
		}
		TRK_strong_armya = {   
			allowed_civil_war = { always = yes }
			picture = GFX_idea_turkmen_strong_armia
			modifier = {
                army_core_defence_factor = 0.1
               	army_attack_factor = 0.05
                army_org = 0.15
                army_speed_factor = 0.05				
			}						
		}
		TRK_people_home = {   
			allowed_civil_war = { always = yes }
			picture = GFX_idea_turkmen_people_homeland
			modifier = {
             	monthly_population = 0.15
                conscription = 0.05	
                recruitable_population = 0.03
               	stability_factor = 0.05
                nationalist_drift = 0.05				
			}						
		}
		TRK_angry_politics = {   
			allowed_civil_war = { always = yes }
			picture = GFX_idea_turkmen_people_opposition
			modifier = {
                production_speed_bunker_factor = 0.05
                nationalist_drift = 0.05
			    justify_war_goal_time = -0.20
		    	Foreign_Intervention_Law_cost_factor = -2				
			}						
		}
		TRK_dead_russia = {   
			allowed_civil_war = { always = yes }
			picture = GFX_idea_SOV_factionalism_modifier
			modifier = {
                production_speed_bunker_factor = 0.15
                army_leader_start_attack_level = 1 
                army_attack_factor = 0.15
                army_org = 0.10
                planning_speed = -0.05				
			}						
		}
		TRK_molotov_cocktail = {   
			allowed_civil_war = { always = yes }
			picture = GFX_idea_SWI_spirit_of_helvetia_aggressive_war
			modifier = {
                terrain_penalty_reduction = 0.1	
                experience_loss_factor = 0.1				
			}						
		}
		TRK_gase_attack = {   
			allowed_civil_war = { always = yes }
			picture = GFX_idea_gas_mask_cope
			modifier = {
                max_dig_in_factor = -0.1
				breakthrough_factor = 0.1
			}						
		}
		TRK_bombandirovka_danger = {   
			allowed_civil_war = { always = yes }
			picture = combined_bomber_offensive
			modifier = {
            army_attack_factor = -0.2 
			army_attack_speed_factor = -0.15
			army_defence_factor = -0.2
			army_morale = -0.1
			stability_weekly = -0.05
			war_support_factor = -0.05 
			}						
		}
		TRK_army_desert = {   
			allowed_civil_war = { always = yes }
			picture = turkmen_karalpak_knight
			modifier = { 
            army_attack_factor = 0.15 
			army_attack_speed_factor = 0.15
			army_defence_factor = 0.15
			army_morale = 0.05
			stability_weekly = 0.02
			war_support_factor = 0.05 
			}						
		}
		TRK_army_uzbek_baff = {   
			allowed_civil_war = { always = yes }
			picture = army_problems
			modifier = { 
			coordination_bonus = 0.20
			army_attack_speed_factor = -0.15
			army_core_defence_factor = 0.55
			army_morale = 0.05
			stability_weekly = -0.02
			war_support_weekly = 0.03
			production_speed_buildings_factor = -0.5
			army_attack_factor = -0.30
			}						
		}		
	}
}	