
##Development Projects
BRA_major_development_projects_decision_category = {
	
	##Seek investments from foreign companies
	BRA_decision_seek_foreign_investment = {
		cost = 100
		
		fixed_random_seed = no

		complete_effect = {
			hidden_effect = {
				random_list ={
					100 = {
					var:influence_array^0 = {
						##some effect
					}  
					}
					75 = {
						var:influence_array^1 = {
						##some effect
						}  
					}
					50 = {
						var:influence_array^2 = {
						##some effect
						}  
					}
					25 = {
						var:influence_array^3 = {
						##some effect
						}  
					}
					5 = {
						var:influence_array^4 = {
						##some effect
						}  
					}
				}
		  }
		}
		ai_will_do = {
			factor = 40
			modifier = {
				political_power_daily < 0.5
				factor = 0
			}
		}

			
		
	}
	
	
	#MAIN TRANSPOSITION

	BRA_transposition_saofrancisco_Main_infrastructure_1 = {							#This is the main prideposition (huehue get it? see, germans can be funni)

		icon = resource_production

		allowed = {
			original_tag = BRA
			
		}

		available = {
			has_full_control_of_state = 889				# Ceara
			owns_state = 889
			has_full_control_of_state = 888				# Pernambuco
			owns_state = 888
			has_full_control_of_state = 1028			# Paraiba
			owns_state = 1028
			has_full_control_of_state = 1027			# Rio grande do Norte  
			owns_state = 1027
		}

		cost = 150
		days_remove = 150
		fire_only_once = yes

		ai_will_do = {
			factor = 40
			modifier = {
				political_power_daily < 0.5
				factor = 0
			}
		}

		visible = {
			has_completed_focus = BRA_major_development_projects
			original_tag = BRA
			has_tech = construction2
		}

		complete_effect = {
			add_manpower = -5000
			add_command_power = -20
			subtract_from_variable = { treasury = 20.0 }
			custom_effect_tooltip = BRA_decision_transposition_saofrancisco_Main_infrastructure_1_tt
		}

		remove_effect = {

			889 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
			888 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
			1028 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
			1027 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
			add_manpower = 5000			
		}
	}
	BRA_transposition_saofrancisco_main_civ_1 = {							

		icon = resource_production

		allowed = {
			original_tag = BRA
		}

		available = {
			has_full_control_of_state = 889				# Ceara
			owns_state = 889
			has_full_control_of_state = 888				# Pernambuco
			owns_state = 888
		}

		cost = 100
		days_remove = 100
		fire_only_once = yes

		ai_will_do = {
			factor = 40
			modifier = {
				political_power_daily < 0.5
				factor = 0
			}
		}

		visible = {
			has_completed_focus = BRA_major_development_projects
			has_decision = BRA_transposition_saofrancisco_Main_infrastructure_1
			original_tag = BRA
		}

		complete_effect = {
			subtract_from_variable = { treasury = 15.0 }
			custom_effect_tooltip = BRA_decision_transposition_saofrancisco_main_civ_1_tt
		}

		remove_effect = {

			889 = {
				add_building_construction = {
					type = industrial_complex
					level = 1
					instant_build = yes
				}
			}
			888 = {
				add_building_construction = {
					type = industrial_complex
					level = 1
					instant_build = yes
				}
			}
		}
	}	
	BRA_transposition_saofrancisco_main_mil_1 = {							
		icon = resource_production

		allowed = {
			original_tag = BRA
		}

		available = {
			has_full_control_of_state = 889				# Ceara
			owns_state = 889
			
		}

		cost = 70
		days_remove = 100
		fire_only_once = yes

		ai_will_do = {
			factor = 40
			modifier = {
				political_power_daily < 0.5
				factor = 0
			}
		}

		visible = {
			has_completed_focus = BRA_major_development_projects
			has_decision = BRA_transposition_saofrancisco_Main_infrastructure_1
			original_tag = BRA
		}

		complete_effect = {
			subtract_from_variable = { treasury = 8.0 }
			custom_effect_tooltip = BRA_decision_transposition_saofrancisco_main_mil_1_tt
		}

		remove_effect = {

			889 = {
				add_building_construction = {
					type = arms_factory
					level = 1
					instant_build = yes
				}
			}
		}
	}

	#WESTERN AXIS

	BRA_transposition_saofrancisco_infrastructure_2 = {				

		icon = resource_production

		allowed = {
			original_tag = BRA
		}

		available = {
			has_full_control_of_state = 1025				# Piaui
			owns_state = 1025
			has_full_control_of_state = 890				# Maranhao
			owns_state = 890
			has_decision = BRA_transposition_saofrancisco_Main_infrastructure_1
		}

		cost = 100
		days_remove = 100
		fire_only_once = yes

		ai_will_do = {
			factor = 40
			modifier = {
				political_power_daily < 0.5
				factor = 0
			}
		}

		visible = {
			has_completed_focus = BRA_major_development_projects
			original_tag = BRA
		}

		complete_effect = {
			add_manpower = -5000
			add_command_power = -10
			subtract_from_variable = { treasury = 10.0 }
			custom_effect_tooltip = BRA_decision_transposition_sao_francisco_2_tt
		}

		remove_effect = {

			890 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
			1025 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
			add_manpower = 5000				
		}
	}	BRA_transposition_saofrancisco_infrastructure_3 = {				 #this is the southern axis I think 

		icon = resource_production

		allowed = {
			original_tag = BRA
		}

		available = {
			has_full_control_of_state = 1029				# Alagoas-sergipe
			owns_state = 1029
			has_full_control_of_state = 887					# Bahia
			owns_state = 887
			has_decision = BRA_transposition_saofrancisco_Main_infrastructure_1 
		}

		cost = 100
		days_remove = 100
		fire_only_once = yes

		ai_will_do = {
			factor = 40
			modifier = {
				political_power_daily < 0.5
				factor = 0
			}
		}

		visible = {
			has_completed_focus = BRA_major_development_projects
			original_tag = BRA
		}

		complete_effect = {
			add_manpower = -5000
			add_command_power = -10
			subtract_from_variable = { treasury = 10.0 }
			custom_effect_tooltip = BRA_decision_transposition_sao_francisco_2_tt
		}

		remove_effect = {

			887 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
			1029 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
			add_manpower = 5000				
		}
	}
	BRA_bacia_tocantins = {

		icon = generic_construction

		allowed = {
			original_tag = BRA
		}

		available = {
			has_full_control_of_state = 890
			owns_state = 890
			has_full_control_of_state = 885
			owns_state = 885
		}

		cost = 150
		days_remove = 150 fire_only_once = yes

		ai_will_do = {
			factor = 40
			modifier = {
				political_power_daily < 0.5
				factor = 0
			}
		}

		visible = {
			has_completed_focus = BRA_major_development_projects
			original_tag = BRA
		}

		complete_effect = {
			add_manpower = -5000
			add_command_power = -5
			subtract_from_variable = { treasury = 10.0 }
			custom_effect_tooltip = BRA_decision_bacia_tocantins_tt
		}

		remove_effect = {

			890 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}

			885 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
		}
	}
	BRA_army_projects = {

		icon = generic_construction

		allowed = {
			original_tag = BRA
		}

		available = {
			has_full_control_of_state = 890
			owns_state = 890
			has_full_control_of_state = 885
			owns_state = 885
		}

		cost = 150
		days_remove = 150 fire_only_once = yes

		ai_will_do = {
			factor = 40
			modifier = {
				political_power_daily < 0.5
				factor = 0
			}
		}

		visible = {
			has_completed_focus = BRA_major_development_projects
			original_tag = BRA
		}

		modifier = { }

		complete_effect = {
			add_manpower = -5000
			add_command_power = -5
			subtract_from_variable = { treasury = 10.0 }
			custom_effect_tooltip = BRA_decision_bacia_tocantins_tt
		}

		remove_effect = {

			890 = {
				add_extra_state_shared_building_slots = 2
				add_building_construction = {
					type = arms_factory
					level = 2
					instant_build = yes
				}
			}

			891 = {
				add_extra_state_shared_building_slots = 2
				add_building_construction = {
					type = arms_factory
					level = 2
					instant_build = yes
				}
			}
		}
	}
	BRA_improve_dockyards = {

		icon = generic_construction

		allowed = {
			original_tag = BRA
		}

		available = {
			has_full_control_of_state = 890
			owns_state = 890
			has_full_control_of_state = 885
			owns_state = 885
		}

		cost = 150
		days_remove = 150 fire_only_once = yes

		ai_will_do = {
			factor = 40
			modifier = {
				political_power_daily < 0.5
				factor = 0
			}
		}

		visible = {
			has_completed_focus = BRA_major_development_projects
			original_tag = BRA
		}

		complete_effect = {
			add_manpower = -5000
			add_command_power = -5
			subtract_from_variable = { treasury = 10.0 }
			custom_effect_tooltip = BRA_decision_bacia_tocantins_tt
		}

		remove_effect = {

			883 = {
				add_extra_state_shared_building_slots = 2
				add_building_construction = {
					type = dockyard
					level = 2
					instant_build = yes
				}
			}

			889 = {
				add_extra_state_shared_building_slots = 2
				add_building_construction = {
					type = dockyard
					level = 2
					instant_build = yes
				}
			}
		}
	}
	BRA_amazonian_projects = {

		icon = generic_construction

		allowed = {
			original_tag = BRA
		}

		available = {
			has_full_control_of_state = 892
			owns_state = 892
		}

		cost = 150
		days_remove = 150
		fire_only_once = yes

		visible = {
			has_completed_focus = BRA_major_development_projects
			original_tag = BRA
		}

		complete_effect = {
			add_manpower = -5000
			add_command_power = -5
			set_temp_variable = { treasury_change = -10 }
			modify_treasury_effect = yes
		}

		remove_effect = {

			892 = {
				add_extra_state_shared_building_slots = 2
				add_building_construction = {
					type = industrial_complex
					level = 1
					instant_build = yes
				}
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
		}

		ai_will_do = {
			factor = 40
			modifier = {
				political_power_daily < 0.5
				factor = 0
			}
		}
	}
}


BRA_workers_party_alignment_decision_category = {

	BRA_decision_support_radicals = {
		cost = 50
		icon = unknown
		available = { has_government = communism }

		days_remove = 7
		days_re_enable = 45
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BRA_decision_support_radicals"
			subtract_from_variable = { BRA_brazilian_party_alignment = 5.0 }
			custom_effect_tooltip = BRA_lowers_party_alignment_by_5_tt
		}

		ai_will_do = {
			factor = 1

			modifier = {
				add = 3
				is_historical_focus_on = Yes
			}
		}
	}

	BRA_decision_support_reformists = {
		cost = 50
		icon = unknown

		available = { has_government = communism }

		days_remove = 7
		days_re_enable = 45
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BRA_decision_support_reformists"
			add_to_variable = { BRA_brazilian_party_alignment = 5.0 }
			custom_effect_tooltip = BRA_raise_party_alignment_by_5_tt
		}

		ai_will_do = {
			factor = 2

			modifier = {
				add = 3
				is_historical_focus_on = No
			}
		}
	}
}

##Migrate into either Development or Devestation
BRA_amazon_resource_development_decision_category = {

	BRA_decision_intial_investment = {

		cost = 75

		available = {
			check_variable = { treasury	> 7.5 }
		}


		days_remove = 30
		days_re_enable = 1
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BRA_decision_intial_investment"
			set_temp_variable = { treasury_change = -7.5 }
			modify_treasury_effect = yes
		}

		ai_will_do = {
			factor = 2

			modifier = {
				add = 4
				is_historical_focus_on = Yes
			}
		}
	}

	BRA_decision_the_road_to_el_dorado = {
		cost = 75

		available = {
			has_full_control_of_state = 892
			has_decision = BRA_decision_intial_investment
		}

		days_remove = 45
		days_re_enable = 0
		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BRA_decision_the_road_to_el_dorado"
			892 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
			set_temp_variable = { temp_opinion = 5 }
			change_industrial_conglomerates_opinion = yes
			set_temp_variable = { treasury_change = -10 }
			modify_treasury_effect = yes
		}

		ai_will_do = {
			factor = 2

			modifier = {
				add = 4
				is_historical_focus_on = Yes
			}
		}
	}

	BRA_decision_expand_the_mining_industry = {

		cost = 100

		available = {
			has_full_control_of_state = 892
			has_decision = BRA_decision_intial_investment
			# has_idea = BRA_idea_initial_investiment
		}

		days_remove = 100
		days_re_enable = 0
		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BRA_decision_expand_the_mining_industry"
			add_resource = {
				type = aluminium
				amount = 6
				state = 892
			}
			set_temp_variable = { treasury_change = -7.5 }
			modify_treasury_effect = yes
		}

		ai_will_do = {
			factor = 6

			modifier = {
				add = 4
				is_historical_focus_on = Yes
			}
		}
	}

	BRA_decision_increase_the_oil_production = {

		cost = 75

		available = {
			has_full_control_of_state = 891
			has_decision = BRA_decision_intial_investment
			# has_idea = BRA_idea_initial_investiment
		}


		days_remove = 90
		days_re_enable = 0
		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BRA_decision_increase_the_oil_production"
			add_resource = {
				type = oil
				amount = 6
				state = 891
			}
			set_temp_variable = { treasury_change = -7.5 }
			modify_treasury_effect = yes
		}

		ai_will_do = {
			factor = 6

			modifier = {
				add = 4
				is_historical_focus_on = Yes
			}
		}
	}

	BRA_decision_increase_military_production = {

		cost = 150

		available = {
			has_full_control_of_state = 892
			has_decision = BRA_decision_intial_investment
			# has_idea = BRA_idea_initial_investiment
		}

		days_remove = 90
		days_re_enable = 0
		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BRA_decision_increase_military_production"
			892 = {
				add_building_construction = {
					type = arms_factory
					level = 1
					instant_build = yes
				}
			}
			set_temp_variable = { treasury_change = -7.5 }
			modify_treasury_effect = yes
		}

		ai_will_do = {
			factor = 6

			modifier = {
				add = 4
				is_historical_focus_on = Yes
			}
		}
	}

	BRA_decision_tech_metal_industry_in_the_amazon = {

		cost = 125

		available = {
			has_full_control_of_state = 892
			has_decision = BRA_decision_intial_investment
			# has_idea = BRA_idea_initial_investiment
		}


		days_remove = 100
		days_re_enable = 0
		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BRA_decision_tech_metal_industry_in_the_amazon"
			set_temp_variable = { treasury_change = -7.5 }
			modify_treasury_effect = yes
			add_resource = {
				type = tungsten
				amount = 6
				state = 892
			}
		}

		ai_will_do = {
			factor = 6

			modifier = {
				add = 4
				is_historical_focus_on = Yes
			}
		}
	}
}

BRA_car_wash_decision_category = {

}

##Migrate into either Development or Devestation
#Amazon Protection
BRA_amazon_protection_decision_category = {

	BRA_decision_reduce_fires = {
		cost = 75

		available = {
			has_full_control_of_state = 892
			has_full_control_of_state = 891
		}


		days_remove = 45
		days_re_enable = 0
		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BRA_decision_the_road_to_el_dorado"
			set_temp_variable = { treasury_change = -3.5 }
			modify_treasury_effect = yes
		}

		remove_effect = {
			892 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
			add_stability = 0.05
			log = "[GetDateText]: [Root.GetName]: Decision remove BRA_decision_reduce_fires"
 		}
		highlight_states = { }

		ai_will_do = {
			factor = 1

			modifier = {
				add = 4
				is_historical_focus_on = no
			}
		}
	}

	BRA_decision_safe_mining_expansion = {

		cost = 100

		available = {
			has_full_control_of_state = 892
			has_full_control_of_state = 891
		}


		days_remove = 100
		days_re_enable = 0
		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BRA_decision_expand_the_mining_industry"
			set_temp_variable = { treasury_change = -7.5 }
			modify_treasury_effect = yes
		}

		remove_effect = {
			add_resource = {
				type = aluminium
				amount = 3
				state = 892
			}
			add_resource = {
				type = aluminium
				amount = 3
				state = 891
			}
			log = "[GetDateText]: [Root.GetName]: Decision remove BRA_decision_expand_the_mining_industry"
 		}
	}
}

#Bolsonaro Alignment
BRA_bolsonaro_alignment_decision_category = {

	BRA_decision_gatter_conservative_support = {

		cost = 50
		icon = generic_civil_support

		available = {
			has_government = nationalist
			NOT = {
				has_idea = BRA_idea_bolsonaro_conservative_influence
			}
		}


		days_remove = 7
		days_re_enable = 45
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BRA_decision_gatter_conservative_support"
			add_timed_idea = {
				idea = BRA_idea_bolsonaro_conservative_influence
				days = 365
			}
			custom_effect_tooltip = BRA_bolsonaro_influence_tt
		}

		ai_will_do = {
			factor = 1

			modifier = {
				add = 3
				is_historical_focus_on = Yes
			}
		}
	}

	BRA_decision_gatter_liberal_support = {

		cost = 50
		icon = eng_propaganda_campaigns

		available = {
			has_government = nationalist
			NOT = {
				has_idea = BRA_idea_bolsonaro_liberal_influence
			}
		}

		days_remove = 7
		days_re_enable = 45
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BRA_decision_gatter_liberal_support"
			add_timed_idea = {
				idea = BRA_idea_bolsonaro_liberal_influence
				days = 365
			}
			custom_effect_tooltip = BRA_bolsonaro_influence_tt
		}


		ai_will_do = {
			factor = 2

			modifier = {
				add = 3
				is_historical_focus_on = No
			}
		}
	}

	BRA_decision_support_private_sector = {

		cost = 50
		icon = generic_factory

		available = {
			has_government = nationalist
			NOT = { has_idea = BRA_idea_supporting_private_sector }
		}


		days_remove = 7
		days_re_enable = 45
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BRA_decision_support_private_sector"
			add_timed_idea = {
				idea = BRA_idea_supporting_private_sector
				days = 730
			}
			custom_effect_tooltip = BRA_supporting_private_sector_tt
		}

		ai_will_do = {
			factor = 1

			modifier = {
				add = 3
				is_historical_focus_on = No
			}
		}
	}

	BRA_decision_support_militaries = {

		cost = 50
		icon = brazilian_guns

		available = {
			has_government = nationalist
			NOT = {
				has_idea = BRA_idea_supporting_military
			}
		}


		days_remove = 7
		days_re_enable = 45
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BRA_decision_support_militaries"
			add_timed_idea = {
				idea = BRA_idea_supporting_military
				days = 730
			}
			# custom_effect_tooltip = BRA_supporting_militaries_tt
		}

		ai_will_do = {
			factor = 1

			modifier = {
				add = 3
				is_historical_focus_on = No
			}
		}
	}
}