	##############################internetpartyingamerule
	}
	focus = {
		id = UKR_internet_party
		icon = Internet_party
		x = -28
		y = 1
		cost = 7
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_internet_party"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_choice_leader
		icon = Darth_Vader
		x = 0
		y = 2
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_internet_party }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_choice_leader"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_free_pc_language
		icon = freepcandforeignukr
		x = 0
		y = 3
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_choice_leader }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_free_pc_language"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_elim_bureucracy
		icon = break_the_budapest_memorandum
		x = -2
		y = 3
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_choice_leader }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_elim_bureucracy"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_reduce_number_tax
		icon = reducenumberoftaxes
		x = 2
		y = 3
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_choice_leader }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: UKR_reduce_number_tax"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_abolition_of_vat
		icon = vatukr
		x = 0
		y = 4
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_free_pc_language }
		prerequisite = { focus = UKR_elim_bureucracy }
		prerequisite = { focus = UKR_reduce_number_tax }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: UKR_abolition_of_vat"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_transition_in_dig
		icon = transitiontomedia
		x = 0
		y = 5
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_abolition_of_vat }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: UKR_transition_in_dig"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_ban_gmo
		icon = sov_ban_gmo
		x = -2
		y = 5
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_abolition_of_vat }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_ban_gmo"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_computerization
		icon = computerisationukr
		x = 2
		y = 5
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_abolition_of_vat }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_computerization"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_single_tax
		icon = gen_coins_n
		x = 0
		y = 6
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_ban_gmo }
		prerequisite = { focus = UKR_computerization }
		prerequisite = { focus = UKR_transition_in_dig }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_single_tax"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_offhores
		icon = blr_offshore_heaven
		x = 0
		y = 7
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_ban_gmo }
		prerequisite = { focus = UKR_computerization }
		prerequisite = { focus = UKR_transition_in_dig }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_offhores"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_nationalize_gsc
		icon = nationalizegsc
		x = -4
		y = 11
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_it_industry }
		mutually_exclusive = { focus = UKR_funding_gsc}
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_nationalize_gsc"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_funding_gsc
		icon = investingsc
		x = -6
		y = 11
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_it_industry }
		mutually_exclusive = { focus = UKR_nationalize_gsc}
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_funding_gsc"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_ties_with_cis
		icon = russian_cooperation
		x = -8
		y = 10
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_foreign_politics }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_ties_with_cis"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_deal_with_torrent
		icon = pirate_flag
		x = -10
		y = 11
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_ties_with_cis }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_deal_with_torrent"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_buy_WOT
		icon = blr_wot
		x = -8
		y = 11
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_ties_with_cis }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_buy_WOT"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_form_galactic_empire
		icon = russian_cooperation
		x = 5
		y = 8
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_offhores }
		mutually_exclusive = { focus = UKR_foreign_politics }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_ties_with_russia"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_foreign_politics
		icon = peace
		x = -8
		y = 8
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_offhores }
		mutually_exclusive = { focus = UKR_form_galactic_empire}
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_ties_with_russia"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_military_plan
		icon = russian_cooperation
		x = 5
		y = 10
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_form_galactic_empire }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_military_plan"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_armour_for_inf
		icon = russian_cooperation
		x = 5
		y = 11
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_military_plan }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_armour_for_inf"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_tie_fighter
		icon = russian_cooperation
		x = 7
		y = 11
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_military_plan }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_ties_with_russia"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_il2_players
		icon = russian_cooperation
		x = 7
		y = 12
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_tie_fighter }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_ties_with_russia"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_WOT_players
		icon = russian_cooperation
		x = 5
		y = 12
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_armour_for_inf }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_ties_with_russia"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_we_only_one_empire
		icon = russian_cooperation
		x = 5
		y = 13
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_WOT_players }
		prerequisite = { focus = UKR_cs_players }
		prerequisite = { focus = UKR_il2_players }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_ties_with_russia"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_war_usa
		icon = russian_cooperation
		x = 5
		y = 14
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_we_only_one_empire }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_ties_with_russia"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_war_china
		icon = russian_cooperation
		x = 7
		y = 14
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_we_only_one_empire }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_ties_with_russia"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_war_russia
		icon = russian_cooperation
		x = 3
		y = 14
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_we_only_one_empire }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_ties_with_russia"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_coup_belarus
		icon = russian_cooperation
		x = 2
		y = 15
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_we_only_one_empire }
		prerequisite = { focus = UKR_cyber_attack }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_ties_with_russia"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_build_death_star
		icon = russian_cooperation
		x = 3
		y = 11
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_military_plan }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_build_death_star"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_cyber_attack
		icon = russian_cooperation
		x = 1
		y = 13
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_military_plan }
		prerequisite = { focus = UKR_cyber_def }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_build_death_star"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_cs_players
		icon = russian_cooperation
		x = 3
		y = 12
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_build_death_star }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_build_death_star"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_it_industry
		icon = russian_cooperation
		x = -2
		y = 9
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_form_galactic_empire focus = UKR_foreign_politics }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_it_industry"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_it_schools
		icon = russian_cooperation
		x = -4
		y = 10
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_it_industry }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_it_industry"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_it_foreig_progg
		icon = russian_cooperation
		x = -2
		y = 10
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_it_industry }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_it_industry"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_cyber_def
		icon = russian_cooperation
		x = 0
		y = 10
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_it_industry }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_it_industry"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_neural
		icon = russian_cooperation
		x = 0
		y = 11
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_cyber_def }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_it_industry"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_summer_camps
		icon = russian_cooperation
		x = -2
		y = 11
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_it_foreig_progg }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_it_industry"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_it_tour
		icon = russian_cooperation
		x = -2
		y = 12
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_summer_camps }
		prerequisite = { focus = UKR_neural }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_it_industry"
		}
		ai_will_do = {
			factor = 1
		}
	}
	focus = {
		id = UKR_it_empire
		icon = russian_cooperation
		x = -2
		y = 13
		cost = 7
		relative_position_id = UKR_internet_party
		prerequisite = { focus = UKR_it_tour }
		search_filters = { FOCUS_FILTER_UKR_STABILITY }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus UKR_it_industry"
		}
		ai_will_do = {
			factor = 1
		}
	}

###InternetPartyOfUkraine
 UKR_internet_party: "Internet Party of Ukraine"
 UKR_internet_party_desc: "Political party of Ukraine, created in 2007. Chairman - Dmitry Ivanovich Golubov."
 UKR_choice_leader: "Choice Our Leader"
 UKR_choice_leader_desc: "We need to choose our leader, we have several suitable candidates"
 UKR_free_pc_language: "Free PC and Foreign Language Courses for Everyone"
 UKR_free_pc_language_desc: "Our citizens need free courses in using PCs and foreign languages."
 UKR_elim_bureucracy: "Elimination of Bureaucracy in its Negative Sense"
 UKR_elim_bureucracy_desc: "By reducing the amount of bureaucracy, we will speed up some processes in our state."
 UKR_reduce_number_tax: "Reduce the Number of Taxes"
 UKR_reduce_number_tax_desc: "To optimize bureaucracy, you need to combine taxes into one, or cancel some."
 UKR_abolition_of_vat: "Abolition of VAT"
 UKR_abolition_of_vat_desc: "VAT needs to be abolished, it harms the purchasing power of our citizens."
 UKR_transition_in_dig: "Transition to Digital Media and Avoidance of Paperwork"
 UKR_transition_in_dig_desc: "Paper media is outdated, we need to switch to digital."
 UKR_ban_gmo: "Ban GMO"
 UKR_ban_gmo_desc: "GMOs should be banned, they harm the health of our citizens"
 UKR_computerization: "Computerization"
 UKR_computerization_desc: "We need to computerize our state as much as possible."
 UKR_single_tax: "Single Income Tax 25"
 UKR_single_tax_desc: "By creating a single tax, we will make life easier for our citizens."
 UKR_offhores: "Creation of Offshore Zones in Ukraine"
 UKR_offhores_desc: "Let's create offshore zones and do our dirty deeds there."
 UKR_nationalize_gsc: "Nationalization of GSC"
 UKR_nationalize_gsc_desc: "We will nationalize GSC in order to get their income."
 UKR_funding_gsc: "Investments in GSC"
 UKR_funding_gsc_desc: "We will fund them to promote our influence through their games in the future."
 UKR_ties_with_russia: "Friendship with Russia"
 UKR_ties_with_russia_desc: "Let's make friends with Russia! They are good!"
 UKR_ties_with_belarus: "Friendship with Belarus"
 UKR_ties_with_belarus_desc: "Let's make friends with Belarus! They are good!"
 UKR_deal_with_torrent: "Deals with uTorrent"
 UKR_deal_with_torrent_desc: ""
 UKR_buy_WOT: "Buy Belarusian World of Tanks"
 UKR_buy_WOT_desc: "Let's buy World of Tanks in order to become a gaming empire."
 UKR_foreign_politics: "Democratic Path"

