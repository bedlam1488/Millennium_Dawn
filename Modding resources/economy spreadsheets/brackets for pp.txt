﻿Brackets:

#small to largest [num_factories]
### bracket_1 ### [0-2]
political_power_factor = -0.50
corruption_cost_factor = -0.50
gdp_capita_cost_factor = -0.50
economic_cycles_cost_factor = -0.50
corruption_cost_factor = -0.50
internal_factions_cost_factor = -0.50
bureaucracy_cost_factor = -0.50
Military_Spending_cost_factor = -0.50
crime_fighting_cost_factor = -0.50
education_budget_cost_factor = -0.50
health_budget_cost_factor = -0.50
social_budget_cost_factor = -0.50
trade_laws_cost_factor = -0.50
Conscription_Law_cost_factor = -0.50
Military_Status_Women_cost_factor = -0.50
Foreign_Intervention_Law_cost_factor = -0.50
Officer_Training_Law_cost_factor = -0.50
Infantry_Weapon_Company_cost_factor = -0.50
Vehicle_Company_cost_factor = -0.50
Aircraft_Company_cost_factor = -0.50
Helicopter_Company_cost_factor = -0.50
Ship_Company_cost_factor = -0.50
Submarine_Company_cost_factor = -0.50

### bracket_2 ### [2-5]
#Non-power:
political_power_factor = -0.35
#hidden ideas with tooltip "Laws Cost: -35%"
corruption_cost_factor = -0.35
gdp_capita_cost_factor = -0.35
economic_cycles_cost_factor = -0.35
corruption_cost_factor = -0.35
internal_factions_cost_factor = -0.35
bureaucracy_cost_factor = -0.35
Military_Spending_cost_factor = -0.35
crime_fighting_cost_factor = -0.35
education_budget_cost_factor = -0.35
health_budget_cost_factor = -0.35
social_budget_cost_factor = -0.35
trade_laws_cost_factor = -0.35
Conscription_Law_cost_factor = -0.35
Military_Status_Women_cost_factor = -0.35
Foreign_Intervention_Law_cost_factor = -0.35
Officer_Training_Law_cost_factor = -0.35
Infantry_Weapon_Company_cost_factor = -0.35
Vehicle_Company_cost_factor = -0.35
Aircraft_Company_cost_factor = -0.35
Helicopter_Company_cost_factor = -0.35
Ship_Company_cost_factor = -0.35
Submarine_Company_cost_factor = -0.35

### bracket_3 ### [5-9]
political_power_factor = -0.30
corruption_cost_factor = -0.30
gdp_capita_cost_factor = -0.30
economic_cycles_cost_factor = -0.30
corruption_cost_factor = -0.30
internal_factions_cost_factor = -0.30
bureaucracy_cost_factor = -0.30
Military_Spending_cost_factor = -0.30
crime_fighting_cost_factor = -0.30
education_budget_cost_factor = -0.30
health_budget_cost_factor = -0.30
social_budget_cost_factor = -0.30
trade_laws_cost_factor = -0.30
Conscription_Law_cost_factor = -0.30
Military_Status_Women_cost_factor = -0.30
Foreign_Intervention_Law_cost_factor = -0.30
Officer_Training_Law_cost_factor = -0.30
Infantry_Weapon_Company_cost_factor = -0.30
Vehicle_Company_cost_factor = -0.30
Aircraft_Company_cost_factor = -0.30
Helicopter_Company_cost_factor = -0.30
Ship_Company_cost_factor = -0.30
Submarine_Company_cost_factor = -0.30

### bracket_4 ### [9-14]
political_power_factor = -0.20
corruption_cost_factor = -0.20
gdp_capita_cost_factor = -0.20
economic_cycles_cost_factor = -0.20
corruption_cost_factor = -0.20
internal_factions_cost_factor = -0.20
bureaucracy_cost_factor = -0.20
Military_Spending_cost_factor = -0.20
crime_fighting_cost_factor = -0.20
education_budget_cost_factor = -0.20
health_budget_cost_factor = -0.20
social_budget_cost_factor = -0.20
trade_laws_cost_factor = -0.20
Conscription_Law_cost_factor = -0.20
Military_Status_Women_cost_factor = -0.20
Foreign_Intervention_Law_cost_factor = -0.20
Officer_Training_Law_cost_factor = -0.20
Infantry_Weapon_Company_cost_factor = -0.20
Vehicle_Company_cost_factor = -0.20
Aircraft_Company_cost_factor = -0.20
Helicopter_Company_cost_factor = -0.20
Ship_Company_cost_factor = -0.20
Submarine_Company_cost_factor = -0.20


### bracket_5 ### [14-21]
political_power_factor = -0.10
corruption_cost_factor = -0.10
gdp_capita_cost_factor = -0.10
economic_cycles_cost_factor = -0.10
corruption_cost_factor = -0.10
internal_factions_cost_factor = -0.10
bureaucracy_cost_factor = -0.10
Military_Spending_cost_factor = -0.10
crime_fighting_cost_factor = -0.10
education_budget_cost_factor = -0.10
health_budget_cost_factor = -0.10
social_budget_cost_factor = -0.10
trade_laws_cost_factor = -0.10
Conscription_Law_cost_factor = -0.10
Military_Status_Women_cost_factor = -0.10
Foreign_Intervention_Law_cost_factor = -0.10
Officer_Training_Law_cost_factor = -0.10
Infantry_Weapon_Company_cost_factor = -0.10
Vehicle_Company_cost_factor = -0.10
Aircraft_Company_cost_factor = -0.10
Helicopter_Company_cost_factor = -0.10
Ship_Company_cost_factor = -0.10
Submarine_Company_cost_factor = -0.10

### bracket_6 ###[21-32]
political_power_factor = 0
corruption_cost_factor = 0
gdp_capita_cost_factor = 0
economic_cycles_cost_factor = 0
corruption_cost_factor = 0
internal_factions_cost_factor = 0
bureaucracy_cost_factor = 0
Military_Spending_cost_factor = 0
crime_fighting_cost_factor = 0
education_budget_cost_factor = 0
health_budget_cost_factor = 0
social_budget_cost_factor = 0
trade_laws_cost_factor = 0
Conscription_Law_cost_factor = 0
Military_Status_Women_cost_factor = 0
Foreign_Intervention_Law_cost_factor = 0
Officer_Training_Law_cost_factor = 0
Infantry_Weapon_Company_cost_factor = 0
Vehicle_Company_cost_factor = 0
Aircraft_Company_cost_factor = 0
Helicopter_Company_cost_factor = 0
Ship_Company_cost_factor = 0
Submarine_Company_cost_factor = 0

### bracket_7 ### [32-50]
political_power_gain = 0.2
political_power_factor = 0.2
corruption_cost_factor = 0.2
gdp_capita_cost_factor = 0.2
economic_cycles_cost_factor = 0.2
corruption_cost_factor = 0.2
internal_factions_cost_factor = 0.2
bureaucracy_cost_factor = 0.2
Military_Spending_cost_factor = 0.2
crime_fighting_cost_factor = 0.2
education_budget_cost_factor = 0.2
health_budget_cost_factor = 0.2
social_budget_cost_factor = 0.2
trade_laws_cost_factor = 0.2
Conscription_Law_cost_factor = 0.2
Military_Status_Women_cost_factor = 0.2
Foreign_Intervention_Law_cost_factor = 0.2
Officer_Training_Law_cost_factor = 0.2
Infantry_Weapon_Company_cost_factor = 0.2
Vehicle_Company_cost_factor = 0.2
Aircraft_Company_cost_factor = 0.2
Helicopter_Company_cost_factor = 0.2
Ship_Company_cost_factor = 0.2
Submarine_Company_cost_factor = 0.2

### Regional Power: ###
political_power_gain = 0.4
political_power_factor = 0.4
#hidden effects with tooltip "Laws Cost: +30%" ;
corruption_cost_factor = 0.4
gdp_capita_cost_factor = 0.4
economic_cycles_cost_factor = 0.4
corruption_cost_factor = 0.4
internal_factions_cost_factor = 0.4
bureaucracy_cost_factor = 0.4
Military_Spending_cost_factor = 0.4
crime_fighting_cost_factor = 0.4
education_budget_cost_factor = 0.4
health_budget_cost_factor = 0.4
social_budget_cost_factor = 0.4
trade_laws_cost_factor = 0.4
Conscription_Law_cost_factor = 0.4
Military_Status_Women_cost_factor = 0.4
Foreign_Intervention_Law_cost_factor = 0.4
Officer_Training_Law_cost_factor = 0.4
Infantry_Weapon_Company_cost_factor = 0.4
Vehicle_Company_cost_factor = 0.4
Aircraft_Company_cost_factor = 0.4
Helicopter_Company_cost_factor = 0.4
Ship_Company_cost_factor = 0.4
Submarine_Company_cost_factor = 0.4

### Regional Power: ###
political_power_gain = 0.4
political_power_factor = 0.4
#hidden effects with tooltip "Laws Cost: +30%" ;
corruption_cost_factor = 0.4
gdp_capita_cost_factor = 0.4
economic_cycles_cost_factor = 0.4
corruption_cost_factor = 0.4
internal_factions_cost_factor = 0.4
bureaucracy_cost_factor = 0.4
Military_Spending_cost_factor = 0.4
crime_fighting_cost_factor = 0.4
education_budget_cost_factor = 0.4
health_budget_cost_factor = 0.4
social_budget_cost_factor = 0.4
trade_laws_cost_factor = 0.4
Conscription_Law_cost_factor = 0.4
Military_Status_Women_cost_factor = 0.4
Foreign_Intervention_Law_cost_factor = 0.4
Officer_Training_Law_cost_factor = 0.4
Infantry_Weapon_Company_cost_factor = 0.4
Vehicle_Company_cost_factor = 0.4
Aircraft_Company_cost_factor = 0.4
Helicopter_Company_cost_factor = 0.4
Ship_Company_cost_factor = 0.4
Submarine_Company_cost_factor = 0.4

### Large Regional Power: ###
political_power_gain = 0.6
political_power_factor = 0.6
#hidden effects with tooltip "Laws Cost: +60%" ;
corruption_cost_factor = 0.6
gdp_capita_cost_factor = 0.6
economic_cycles_cost_factor = 0.6
corruption_cost_factor = 0.6
internal_factions_cost_factor = 0.6
bureaucracy_cost_factor = 0.6
Military_Spending_cost_factor = 0.6
crime_fighting_cost_factor = 0.6
education_budget_cost_factor = 0.6
health_budget_cost_factor = 0.6
social_budget_cost_factor = 0.6
trade_laws_cost_factor = 0.6
Conscription_Law_cost_factor = 0.6
Military_Status_Women_cost_factor = 0.6
Foreign_Intervention_Law_cost_factor = 0.6
Officer_Training_Law_cost_factor = 0.6
Infantry_Weapon_Company_cost_factor = 0.6
Vehicle_Company_cost_factor = 0.6
Aircraft_Company_cost_factor = 0.6
Helicopter_Company_cost_factor = 0.6
Ship_Company_cost_factor = 0.6
Submarine_Company_cost_factor = 0.6

### Large Power: ###
political_power_gain = 0.8
political_power_factor = 0.8
#hidden effects with tooltip "Laws Cost: +80%" ;
corruption_cost_factor = 0.8
gdp_capita_cost_factor = 0.8
economic_cycles_cost_factor = 0.8
corruption_cost_factor = 0.8
internal_factions_cost_factor = 0.8
bureaucracy_cost_factor = 0.8
Military_Spending_cost_factor = 0.8
crime_fighting_cost_factor = 0.8
education_budget_cost_factor = 0.8
health_budget_cost_factor = 0.8
social_budget_cost_factor = 0.8
trade_laws_cost_factor = 0.8
Conscription_Law_cost_factor = 0.8
Military_Status_Women_cost_factor = 0.8
Foreign_Intervention_Law_cost_factor = 0.8
Officer_Training_Law_cost_factor = 0.8
Infantry_Weapon_Company_cost_factor = 0.8
Vehicle_Company_cost_factor = 0.8
Aircraft_Company_cost_factor = 0.8
Helicopter_Company_cost_factor = 0.8
Ship_Company_cost_factor = 0.8
Submarine_Company_cost_factor = 0.8

### Great Power: ###
political_power_gain = 1.2
political_power_factor = 1.2
#hidden effects with tooltip "Laws Cost: +100%" ;
corruption_cost_factor = 1.2
gdp_capita_cost_factor = 1.2
economic_cycles_cost_factor = 1.2
corruption_cost_factor = 1.2
internal_factions_cost_factor = 1.2
bureaucracy_cost_factor = 1.2
Military_Spending_cost_factor = 1.2
crime_fighting_cost_factor = 1.2
education_budget_cost_factor = 1.2
health_budget_cost_factor = 1.2
social_budget_cost_factor = 1.2
trade_laws_cost_factor = 1.2
Conscription_Law_cost_factor = 1.2
Military_Status_Women_cost_factor = 1.2
Foreign_Intervention_Law_cost_factor = 1.2
Officer_Training_Law_cost_factor = 1.2
Infantry_Weapon_Company_cost_factor = 1.2
Vehicle_Company_cost_factor = 1.2
Aircraft_Company_cost_factor = 1.2
Helicopter_Company_cost_factor = 1.2
Ship_Company_cost_factor = 1.2
Submarine_Company_cost_factor = 1.2

### Superpower: ###
political_power_gain = 2
political_power_factor = 2
#hidden effects with tooltip "Laws Cost: +200%" ;
corruption_cost_factor = 2
gdp_capita_cost_factor = 2
economic_cycles_cost_factor = 2
corruption_cost_factor = 2
internal_factions_cost_factor = 2
bureaucracy_cost_factor = 2
Military_Spending_cost_factor = 2
crime_fighting_cost_factor = 2
education_budget_cost_factor = 2
health_budget_cost_factor = 2
social_budget_cost_factor = 2
trade_laws_cost_factor = 2
Conscription_Law_cost_factor = 2
Military_Status_Women_cost_factor = 2
Foreign_Intervention_Law_cost_factor = 2
Officer_Training_Law_cost_factor = 2
Infantry_Weapon_Company_cost_factor = 2
Vehicle_Company_cost_factor = 2
Aircraft_Company_cost_factor = 2
Helicopter_Company_cost_factor = 2
Ship_Company_cost_factor = 2
Submarine_Company_cost_factor = 2



