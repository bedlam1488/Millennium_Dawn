﻿division_template = {
	name = "Garisson"

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
	}
	support = {
	}
}
division_template = {
	name = "Pirates"

	regiments = {
		Militia_Bat = { x = 0 y = 0 }
		Militia_Bat = { x = 0 y = 1 }
		Militia_Bat = { x = 0 y = 2 }
	}
	support = {
	}
}

units = {
	division = {
		name = "Pirates from Bosaso"
		location = 1966
		division_template = "Pirates"
		start_experience_factor = 0
		start_equipment_factor = 0.6
	}
	division = {
		name = "Pirates from Harardhere"
		location = 11088
		division_template = "Pirates"
		start_experience_factor = 0
		start_equipment_factor = 0.6
	}
	division = {
		name = "Pirates from Eyl"
		location = 11090
		division_template = "Pirates"
		start_experience_factor = 0
		start_equipment_factor = 0.6
	}
	division = {
		name = "Loyal Garrison Force"
		location = 11090
		division_template = "Garisson"
		start_experience_factor = 0.1
		start_equipment_factor = 0.8
	}
}
