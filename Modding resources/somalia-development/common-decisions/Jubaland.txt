JUB_prepare_for_return = {
	JUB_train_militias = {
		cost = 0

		available = {
			has_country_flag = JUB_training_camps_established_flg
		}

		days_remove = 30
		fire_only_once = yes

		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove JUB_train_militias"
			add_to_variable = {
				var = jub_training_level
				value = 10
			}
			add_to_variable = {
				var = jub_equipment_level
				value = -5
			}
			clamp_variable = {
				var = jub_training_level
				min = 0
				max = 120
			}
			clamp_variable = {
				var = jub_equipment_level
				min = -100
				max = 100
			}
			custom_effect_tooltip = jub_equipment_level_decrease_5_tt
			custom_effect_tooltip = JUB_training_increases_10_tt
		}

		ai_will_do = {
			factor = 2
		}
	}

	JUB_training_camps = {
		cost = 0

		available = {
			has_country_flag = JUB_train_fc_flg
		}

		days_remove = 120
		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision JUB_training_camps"
		}

		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove JUB_training_camps"
			set_country_flag = JUB_training_camps_established_flg
		}

		ai_will_do = {
			factor = 2
		}
	}

	JUB_smuggle_kenya = {
		icon = GFX_decision_generic_prepare_civil_war

		cost = 0

		available = {
			has_country_flag = JUB_smuggle_fc_flg
		}

		days_remove = 30
		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision JUB_smuggle_kenya_1"
		}

		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove JUB_smuggle_kenya_1"
			add_to_variable = {
				var = jub_equipment_level
				value = 10
			}
			clamp_variable = {
				var = jub_equipment_level
				min = -100
				max = 100
			}
			custom_effect_tooltip = jub_equipment_level_increase_10_tt
		}

		ai_will_do = {
			factor = 2
		}
	}

	JUB_contact_loyalists = {
		icon = GFX_decision_generic_political_discourse

		cost = 0

		available = {
			has_country_flag = JUB_contact_fc_flg
		}

		Is_good = no
		days_remove = 30
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision JUB_contact_loyalists"
		}

		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove JUB_contact_loyalists"
			add_to_variable = {
				var = jub_manpower
				value = 10
			}
			add_to_variable = {
				var = jub_equipment_level
				value = -5
			}
			clamp_variable = {
				var = jub_manpower
				min = -0
				max = 100000
			}
			clamp_variable = {
				var = jub_equipment_level
				min = -100
				max = 100
			}
			custom_effect_tooltip = jub_equipment_level_decrease_5_tt
			custom_effect_tooltip = jub_manpower_increase_500_tt
		}

		ai_will_do = {
			factor = 2
		}
	}

}

JUB_destroy_spm = {

	JUB_spm_gedo = {

		icon = GFX_decision_generic_prepare_civil_war

		cost = 25

		available = {
			NOT = { has_country_flag = JUB_spm_active_mission_flg }
		}

		Is_good = no
		days_remove = 30
		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision JUB_spm_gedo"
			set_country_flag = JUB_spm_active_mission_flg
		}

		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove JUB_spm_gedo"
			clr_country_flag = JUB_spm_active_mission_flg
			if = {
				limit = {
					has_idea = butchers_threat
				}
				swap_ideas = {
					remove_idea = butchers_threat
					add_idea = butchers_threat1
				}
			}
			else_if = {
				limit = { has_idea = butchers_threat1 }
				swap_ideas = { remove_idea = butchers_threat1 add_idea = butchers_threat2 }
			}
			else_if = {
				limit = { has_idea = butchers_threat2 }
				swap_ideas = { remove_idea = butchers_threat2 add_idea = butchers_threat3 }
			}
			else_if = {
				limit = { has_idea = butchers_threat3 }
				remove_idea = butchers_threat3
			}
		}

		ai_will_do = {
			factor = 2
		}
	}

	JUB_spm_middle_juba = {

		icon = GFX_decision_generic_prepare_civil_war

		cost = 25

		available = {
			NOT = { has_country_flag = JUB_spm_active_mission_flg }
		}

		Is_good = no
		days_remove = 30
		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision JUB_spm_gedo"
			set_country_flag = JUB_spm_active_mission_flg
		}

		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove JUB_spm_gedo"
			clr_country_flag = JUB_spm_active_mission_flg
			if = {
				limit = {
					has_idea = butchers_threat
				}
				swap_ideas = {
					remove_idea = butchers_threat
					add_idea = butchers_threat1
				}
			}
			else_if = {
				limit = { has_idea = butchers_threat1 }
				swap_ideas = { remove_idea = butchers_threat1 add_idea = butchers_threat2 }
			}
			else_if = {
				limit = { has_idea = butchers_threat2 }
				swap_ideas = { remove_idea = butchers_threat2 add_idea = butchers_threat3 }
			}
			else_if = {
				limit = { has_idea = butchers_threat3 }
				remove_idea = butchers_threat3
			}
		}

		ai_will_do = {
			factor = 2
		}
	}

	JUB_spm_lower_juba = {

		icon = GFX_decision_generic_prepare_civil_war

		cost = 25

		available = {
			NOT = { has_country_flag = JUB_spm_active_mission_flg }
		}

		Is_good = no
		days_remove = 30
		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision JUB_spm_gedo"
			set_country_flag = JUB_spm_active_mission_flg
		}

		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove JUB_spm_gedo"
			clr_country_flag = JUB_spm_active_mission_flg
			if = {
				limit = {
					has_idea = butchers_threat
				}
				swap_ideas = {
					remove_idea = butchers_threat
					add_idea = butchers_threat1
				}
			}
			else_if = {
				limit = { has_idea = butchers_threat1 }
				swap_ideas = { remove_idea = butchers_threat1 add_idea = butchers_threat2 }
			}
			else_if = {
				limit = { has_idea = butchers_threat2 }
				swap_ideas = { remove_idea = butchers_threat2 add_idea = butchers_threat3 }
			}
			else_if = {
				limit = { has_idea = butchers_threat3 }
				remove_idea = butchers_threat3
			}
		}

		ai_will_do = {
			factor = 2
		}
	}

}
