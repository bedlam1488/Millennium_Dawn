################
##### PUN ######
################

PUN_plunder = {
	icon = GFX_decision_pirates_hat
	priority = 10
	visible_when_empty = yes
	allowed = { original_tag = PUN }
	visible = {
		original_tag = PUN
	}
}

PUN_treasure_hunt = {
	icon = generic_economy
	priority = 10
	visible_when_empty = yes
	allowed = { original_tag = PUN }
	visible = {
		original_tag = PUN
		has_country_flag = PUN_is_pirate
		NOT = { has_country_flag = PUN_aborted_treasure }
	}
}

PUN_decisions = {
	icon = GFX_decision_generic_puntland
	priority = 10
	visible_when_empty = yes
	allowed = { original_tag = PUN }
	visible = {
		original_tag = PUN
	}
}

PUN_deferral_system = {
	icon = GFX_decision_generic_puntland
	priority = 10
	allowed = { original_tag = PUN }
	visible = {
		has_country_flag = PUN_deferred_money_flag
	}
}

PUN_pirates = {
	icon = GFX_decision_pirates_hat
	priority = 10
	visible_when_empty = yes
	allowed = { original_tag = PUN }
	visible = {
		original_tag = PUN
		has_country_flag = PUN_pirates_started_flg
	}
}
