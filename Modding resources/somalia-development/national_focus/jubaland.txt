### Jubaland's Focuses
##Angriest Bird
focus_tree = {
	id = JUB_focus
	country = {
		factor = 0
		modifier = {
			add = 25
			tag = JUB
		}
	}
	shared_focus = SOM_a_nation_torn

	continuous_focus_position = { x = 3000 y = 3000 }
	### Land In The Jubba-Valley
	focus = {
		id = JUB_jubba_valley
		icon = jubba_valley
		x = 8
		y = 0
		cost = 1

		completion_reward = {
			add_political_power = 10
			country_event = { id = jubaland.1 days = 1}
		}

		ai_will_do = {
			factor = 1
		}
	}

	### The Old Order Is Gone
	focus = {
		id = JUB_old_order_gone
		icon = barre_hiiraale
		relative_position_id = JUB_jubba_valley
		x = -3
		y = 1
		cost = 5

		prerequisite = { focus = JUB_jubba_valley }
		mutually_exclusive = { focus = JUB_butcher_returns }

		available = {

		}

		completion_reward = {
			add_stability = 0.05
		}

		ai_will_do = {
			factor = 1
		}
	}

	### Patrol The Border
	focus = {
		id = JUB_united_jubaland
		icon = jubaland_border_patrol
		relative_position_id = JUB_old_order_gone
		x = -1
		y = 1
		cost = 10

		prerequisite = { focus = JUB_old_order_gone }

		available = {

		}

		completion_reward = {
			if = {
				limit = {
					has_idea = butchers_threat
				}
				swap_ideas = {
					remove_idea = butchers_threat
					add_idea = butchers_threat1
				}
			}
			else_if = {
				limit = { has_idea = butchers_threat1 }
				swap_ideas = { remove_idea = butchers_threat1 add_idea = butchers_threat2 }
			}
			else_if = {
				limit = { has_idea = butchers_threat2 }
				swap_ideas = { remove_idea = butchers_threat2 add_idea = butchers_threat3 }
			}
			else_if = {
				limit = { has_idea = butchers_threat3 }
				remove_idea = butchers_threat3
			}
		}

		ai_will_do = {
			factor = 1
		}
	}

	### Destroy The Last SPM Pockets
	focus = {
		id = JUB_destroy_pockets
		icon = infiltration
		relative_position_id = JUB_old_order_gone
		x = 1
		y = 1
		cost = 10

		ai_will_do = { factor = 1 }

		prerequisite = {
			focus = JUB_old_order_gone
		}

		available = {

		}

		completion_reward = {
			unlock_decision_category_tooltip = JUB_destroy_spm
		}

		ai_will_do = {
			factor = 1
		}
	}

	### Jubaland Is Safe Again
	focus = {
		id = JUB_safe_again
		icon = jubaland_safe
		relative_position_id = JUB_old_order_gone
		x = 0
		y = 2
		cost = 10

		ai_will_do = { factor = 1 }

		prerequisite = {
			focus = JUB_united_jubaland
		}
		prerequisite = {
			focus = JUB_destroy_pockets
		}

		available = {

		}

		completion_reward = {
			add_stability = 0.05
		}

		ai_will_do = {
			factor = 1
		}
	}

	##The Butcher Tree
	focus = {
		id = JUB_butcher_returns
		icon = the_butcher
		relative_position_id = JUB_jubba_valley
		x = 3
		y = 1
		cost = 5

		ai_will_do = { factor = 1 }

		prerequisite = {
			focus = JUB_jubba_valley
		}

		mutually_exclusive = { focus = JUB_old_order_gone }

		available = {

		}

		completion_reward = {
			add_stability = -0.05
			unlock_decision_category_tooltip = JUB_prepare_for_return
			set_variable = { jub_training_level = 10 }
			set_variable = { jub_equipment_level = 10 }
			set_variable = { jub_manpower = 500 }
		}

		ai_will_do = {
			factor = 1
		}
	}

	### Establish Contact With Former Loyalists
	focus = {
		id = JUB_contact_former_loyalists
		icon = jubaland_establish_contact
		relative_position_id = JUB_butcher_returns
		x = -2
		y = 1
		cost = 10

		ai_will_do = { factor = 1 }

		prerequisite = {
			focus = JUB_butcher_returns
		}

		available = {

		}

		completion_reward = {
			unlock_decision_tooltip = JUB_contact_loyalists
			set_country_flag = JUB_contact_fc_flg
		}

		ai_will_do = {
			factor = 1
		}
	}

	### Train The Militias
	focus = {
		id = JUB_train_militias
		icon = militias_train
		relative_position_id = JUB_butcher_returns
		x = 0
		y = 1
		cost = 10

		ai_will_do = { factor = 1 }

		prerequisite = {
			focus = JUB_butcher_returns
		}

		available = {

		}

		completion_reward = {
			unlock_decision_tooltip = JUB_train_militias
			set_country_flag = JUB_train_fc_flg
		}

		ai_will_do = {
			factor = 1
		}
	}

	### Smuggle Small Arms
	focus = {
		id = JUB_smuggle_small_arms
		icon = smuggle_truck
		relative_position_id = JUB_butcher_returns
		x = 2
		y = 1
		cost = 10

		prerequisite = { focus = JUB_butcher_returns }

		available = {

		}

		completion_reward = {
			unlock_decision_tooltip = JUB_smuggle_kenya
			set_country_flag = JUB_smuggle_fc_flg
		}

		ai_will_do = {
			factor = 1
		}
	}

	### Retake Kismayo
	focus = {
		id = JUB_retake_kismayo
		icon = retake_kismayo
		relative_position_id = JUB_butcher_returns
		x = 0
		y = 2
		cost = 10

		prerequisite = { focus = JUB_contact_former_loyalists }
		prerequisite = { focus = JUB_smuggle_small_arms }
		prerequisite = { focus = JUB_train_militias }

		available = {

		}

		completion_reward = {
			remove_ideas = butchers_threat
		}

		ai_will_do = {
			factor = 1
		}
	}

	##Somalia Civil War Action
	focus = {
		id = JUB_a_united_somalia
		icon = x
		x = -4
		y = 1
		relative_position_id = JUB_safe_again
		cost = 1

		prerequisite = { focus = JUB_safe_again }
		mutually_exclusive = { focus = JUB_stay_on_our_own }
		completion_reward = {

		}

		ai_will_do = {
			factor = 1
		}
	}

	focus = {
		id = JUB_stay_on_our_own
		icon = x
		x = -2
		y = 1
		relative_position_id = JUB_safe_again
		cost = 1

		prerequisite = { focus = JUB_safe_again }
		mutually_exclusive = { focus = JUB_a_united_somalia }
		completion_reward = {

		}

		ai_will_do = {
			factor = 1
		}
	}

	focus = {
		id = JUB_central_government_talks
		icon = x
		x = -4
		y = 2
		relative_position_id = JUB_safe_again
		cost = 1

		prerequisite = { focus = JUB_a_united_somalia }
		completion_reward = {

		}

		ai_will_do = {
			factor = 1
		}
	}

	focus = {
		id = JUB_isolate_ourselves
		icon = x
		x = -4
		y = 2
		relative_position_id = JUB_safe_again
		cost = 1

		prerequisite = { focus = JUB_stay_on_our_own }
		completion_reward = {

		}

		ai_will_do = {
			factor = 1
		}
	}
}
