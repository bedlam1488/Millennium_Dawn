set_leader_PUN = {
	if = { limit = { has_country_flag = set_Neutral_Autocracy }
		if = { limit = { check_variable = { Neutral_Autocracy_leader = 0 } }
			add_to_variable = { Neutral_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Abdullahi Yusuf Ahmed"
				desc = "ABDULLAHI_YUSUF_AHMED_DESC"
				picture = "PUN_Abdullahi_Yusuf_Ahmed.dds"
				ideology = Neutral_Autocracy
				traits = {
					neutrality_Neutral_Autocracy
					ruthless
					military_career
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Autocracy_leader = 1 } }
			if = { limit = { date < 2004.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { Neutral_Autocracy_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mohamud Muse Hersi"
				picture = "mohamud_adde_muse_hersi.dds"
				ideology = Neutral_Autocracy
				traits = {
					neutrality_Neutral_Autocracy
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Autocracy_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { Neutral_Autocracy_leader = 2 } }
			add_to_variable = { Neutral_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Abdirahman Farole"
				picture = "abdirahman_farole.dds"
				ideology = Neutral_Autocracy
				traits = {
					neutrality_Neutral_Autocracy
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Autocracy_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { Neutral_Autocracy_leader = 3 } }
			add_to_variable = { Neutral_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Abdiweli Gaas"
				picture = "abdiweli_gaas.dds"
				ideology = Neutral_Autocracy
				traits = {
					neutrality_Neutral_Autocracy
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Autocracy_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}
}