ideas = {
	country = {
		JUB_butchers_threat = {
			picture = JUB_butchers_threat
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea JUB_butchers_threat" }
			allowed_civil_war = { always = no }
			modifier = {
				stability_factor = -0.2
			}
		}

		JUB_butchers_threat1 = {
			picture = JUB_butchers_threat
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea JUB_butchers_threat1" }
			allowed_civil_war = { always = no }
			modifier = {
				stability_factor = -0.15
			}
		}

		JUB_butchers_threat2 = {
			picture = JUB_butchers_threat
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea JUB_butchers_threat2" }
			allowed_civil_war = { always = no }
			modifier = {
				stability_factor = -0.1
			}
		}

		JUB_butchers_threat3 = {
			picture = JUB_butchers_threat
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea JUB_butchers_threat3" }
			allowed_civil_war = { always = no }
			modifier = {
				stability_factor = -0.05
			}
		}
	}
}
